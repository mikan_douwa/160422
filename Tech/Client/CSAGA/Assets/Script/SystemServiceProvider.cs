﻿using UnityEngine;
using System.Collections;
using Mikan;
using Mikan.Net;
using System;
using System.Threading;

public class SystemServiceProvider : ISystemServiceProvider
{
	#region ISystemServiceProvider implementation

	public event Action<byte[]> OnReadEnd;
	
	public IHttpServiceProvider GetHttpServiceProvider() { return HttpServiceProvider.Instance; }

	public IWebSocketService CreateWebSocketService() { throw new NotImplementedException(); }

	public void CleanNotification()
	{
#if UNITY_IPHONE
		/*
		var notify = new LocalNotification();
		notify.applicationIconBadgeNumber = -1;

		NotificationServices.PresentLocalNotificationNow(notify);
		NotificationServices.CancelAllLocalNotifications();
		NotificationServices.ClearLocalNotifications();
		*/
#endif
	}

	public void AddNotification(string message, DateTime time, int number)
	{
#if UNITY_IPHONE
		/*
		var notify = new LocalNotification();
		notify.fireDate = time;
		notify.alertBody = message;
		notify.hasAction = true;
		notify.applicationIconBadgeNumber = number;
		notify.soundName = LocalNotification.defaultSoundName;
		NotificationServices.ScheduleLocalNotification(notify);
		*/
#endif
	}

	public void SaveLocalData (string key, string value)
	{
		PlayerPrefs.SetString (key, value);
	}
	
	public void SaveLocalData (string key, int value)
	{
		PlayerPrefs.SetInt (key, value);
	}
	
	public void SaveLocalData (string key, float value)
	{
		PlayerPrefs.SetFloat (key, value);
	}
	
	public string LoadString (string key)
	{
		return PlayerPrefs.GetString (key);
	}
	
	public int LoadInt (string key)
	{
		return PlayerPrefs.GetInt (key);
	}
	
	public float LoadFloat (string key)
	{
		return PlayerPrefs.GetFloat (key);
	}

	public void ClearLocalData(string key)
	{
		PlayerPrefs.DeleteKey(key);
	}

	public void ClearLocalData()
	{
		PlayerPrefs.DeleteAll();
	}
	
	public void ShowLog (int priority, string log)
	{
		if (priority <= 0)
			Debug.LogWarning (log);
		else
			Debug.Log (log);
	}
	
	public string UUID {
		get {
			return Guid.NewGuid().ToString();
		}
	}
	
	public string DeviceID {
		get {
			return SystemInfo.deviceUniqueIdentifier;
		}
	}

	public string IAPName {
		get {
			return UIDefine.IAPName;
		}
	}

	public void Read (params string[] args)
	{
		Coroutine.Start(ReadRoutine(args));
	}

	#endregion

	private IEnumerator ReadRoutine(string[] args)
	{
		foreach(var item in args)
		{
			var www = new WWW(Application.streamingAssetsPath + System.Text.Encoding.UTF8.GetString(System.Convert.FromBase64String(item)));
			yield return www;

			if(!string.IsNullOrEmpty(www.error))
				OnReadEnd(System.Text.Encoding.UTF8.GetBytes(item));
			else
				OnReadEnd(www.bytes);

			www.Dispose();
		}
	}
}

