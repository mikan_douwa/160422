﻿using UnityEngine;
using System.Collections;

public class ChallengeRewardMsg : MessageBox {

	public UIGrid grid;
	public GameObject iconPrefab;

	protected override void OnReady ()
	{
		GameSystem.Service.OnTrophyList += HandleOnTrophyList;

		GameSystem.Service.TrophyList();
	}

	void HandleOnTrophyList ()
	{
		foreach(Mikan.CSAGA.ConstData.Trophy data in ConstData.Tables.Trophy.Select(o=> { return o.n_LEGACY_TYPE == Mikan.CSAGA.TrophyType.Challenge
																							&& o.n_EFFECT[0] == Context.Instance.MainQuest.MainQuestID; }))
		{
			ChallengeRewardIcon icon = NGUITools.AddChild(grid.gameObject, iconPrefab).GetComponent<ChallengeRewardIcon>();

			var trophy = GameSystem.Service.TrophyInfo[(int)data.n_IDSET];

			icon.Init(data, (trophy != null && data.n_ID <= trophy.Current));
		}

		grid.Reposition();
	}

	protected override void OnDestroy ()
	{
		if(GameSystem.IsAvailable)
			GameSystem.Service.OnTrophyList -= HandleOnTrophyList;
		base.OnDestroy ();
	}
}
