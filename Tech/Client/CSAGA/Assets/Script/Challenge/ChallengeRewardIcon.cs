﻿using UnityEngine;
using System.Collections;

public class ChallengeRewardIcon : MonoBehaviour {

	public Icon icon;
	public UILabel count;
	public UISprite check;

	public void Init(Mikan.CSAGA.ConstData.Trophy tData, bool isGet)
	{
		if(tData != null)
		{
			if(tData.n_BONUS_LINK > 0)
			{
				//icon
				Mikan.CSAGA.ConstData.Drop data = ConstData.Tables.Drop[tData.n_BONUS_LINK];
				if(data != null)
				{
					icon.IconType = data.n_DROP_TYPE;
					icon.IconID = data.n_DROP_ID;
					icon.Count = data.n_COUNT;
					icon.ApplyChange();
				}
			}
		}

		count.text = "x " + tData.n_EFFECT[1].ToString();
		NGUITools.SetActive(check.gameObject, isGet);
	}
}
