﻿using UnityEngine;
using System.Collections;

public class GalleryPage : CardListPage {

	protected override bool ShowSpaceInfo { get { return false; } }

	protected override void Initialize ()
	{
		titleText = Context.Instance.SetupData.Text;
		base.Initialize ();
	}
}
