﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using Mikan.CSAGA.Data;

public class SocialListView : View {
	
	public ItemList itemList;
	public UILabel countLabel;
	public GameObject countObj;
	public UILabel NoFriendLabel;
	public GameObject NoFriendObj;
	
	
	List<string> lstId = new List<string>();
	
	protected override void Initialize ()
	{
		GameSystem.Service.OnSocialQuery += SocialQueryHandler;
		GameSystem.Service.OnSocialRandomQuery += SocialRandomQueryHandler;
		GameSystem.Service.OnSocialUpdate += UpdateList;
		
		NGUITools.SetActive(NoFriendObj, false);

		//title
		switch(Context.Instance.SocialAction)
		{
		case SocialMenuData.SOCIAL_ACTION.FOLLOW_LIST:
			appendixTextID = SystemTextID._402_FANS_LIST;
			break;
		case SocialMenuData.SOCIAL_ACTION.FANS_LIST:
			appendixTextID = SystemTextID._401_FOLLOW_LIST;
			break;
		case SocialMenuData.SOCIAL_ACTION.CHOOSE_COMRADE:
		{
			appendixTextID = SystemTextID._412_OTHER_SUPPORT;
			/*
			disableNextButton = false;
			titleText = ConstData.GetSystemText(SystemTextID._411_FELLOW_SUPPORT);
			
			nextButtonText = ConstData.GetSystemText(SystemTextID._411_FELLOW_SUPPORT);
			
			if(Context.Instance.StageID == StageID.FollowSupport)
			{
				titleText = ConstData.GetSystemText(SystemTextID._411_FELLOW_SUPPORT);
				nextButtonText = ConstData.GetSystemText(SystemTextID._412_OTHER_SUPPORT);
			}
			else if(Context.Instance.StageID == StageID.RandomSupport)
			{
				titleText = ConstData.GetSystemText(SystemTextID._412_OTHER_SUPPORT);
				nextButtonText = ConstData.GetSystemText(SystemTextID._411_FELLOW_SUPPORT);
				GameSystem.Service.SocialRandomQuery();
				return;
			}
			*/
			break;
		}
		case SocialMenuData.SOCIAL_ACTION.RANDOM:
			appendixTextID = SystemTextID._411_FELLOW_SUPPORT;
			GameSystem.Service.SocialRandomQuery();
			return;
		}
		
		GetSocialListData();
	}

	void GetSocialListData()
	{
		List<Fellow> lstFellow = new List<Fellow>();
		lstFellow = GameSystem.Service.SocialInfo.List;
		Mikan.CSAGA.Relation relation = Mikan.CSAGA.Relation.None;
		switch(Context.Instance.SocialAction)
		{
		case SocialMenuData.SOCIAL_ACTION.FOLLOW_LIST:
		case SocialMenuData.SOCIAL_ACTION.CHOOSE_COMRADE:
			relation = Mikan.CSAGA.Relation.Follow;
			break;
		case SocialMenuData.SOCIAL_ACTION.FANS_LIST:
			relation = Mikan.CSAGA.Relation.Fans;
			break;
		}
		
		for(int i = 0; i < lstFellow.Count; i++)
		{
			Fellow fellow = lstFellow[i];
			if(fellow.Relation == relation || fellow.Relation == Mikan.CSAGA.Relation.CrossFollow)
			{
				if(!lstId.Contains(fellow.PublicID))
					lstId.Add(fellow.PublicID);
			}
		}
		
		GameSystem.Service.SocialQuery(lstId);
	}

	void SocialRandomQueryHandler(List<string> lst)
	{
		lstId = lst;
		GameSystem.Service.SocialQuery(lstId);
	}
	
	void SocialQueryHandler()
	{
		UpdateList();
		if(!IsInitialized) InitializeComplete();
	}
	
	void UpdateList(string publicId = "")
	{
		List<SocialListData> lst = new List<SocialListData>();
		for(int i = 0; i < lstId.Count; i++)
		{
			string id = lstId[i];
			Fellow fellow = GameSystem.Service.SocialInfo[id];
			if(fellow != null)
			{
				lst.Add(new SocialListData(){ Origin = fellow });
			}
		}
		
		if(lst.Count > 0)
			itemList.Setup(lst);
		else if(Context.Instance.SocialAction == SocialMenuData.SOCIAL_ACTION.RANDOM)
			NGUITools.SetActive(NoFriendObj, false);
		else
		{
			NGUITools.SetActive(NoFriendObj, true);
			switch(Context.Instance.SocialAction)
			{
			case SocialMenuData.SOCIAL_ACTION.FOLLOW_LIST:
			case SocialMenuData.SOCIAL_ACTION.CHOOSE_COMRADE:
				NoFriendLabel.text = ConstData.GetSystemText(SystemTextID._426_FOLLOW_LIST_EMPTY);
				break;
				
			case SocialMenuData.SOCIAL_ACTION.FANS_LIST:
				NoFriendLabel.text = ConstData.GetSystemText(SystemTextID._427_FANS_LIST_EMPTY);
				break;
			}
		}
		
		//show count
		switch(Context.Instance.SocialAction)
		{
		case SocialMenuData.SOCIAL_ACTION.FOLLOW_LIST:
			NGUITools.SetActive(countObj, true);
			countLabel.text = lstId.Count.ToString() + "/" + GameSystem.Service.PlayerInfo.Data.n_FOLLOW.ToString();
			break;			
		case SocialMenuData.SOCIAL_ACTION.FANS_LIST:
			NGUITools.SetActive(countObj, true);
			countLabel.text = ConstData.GetSystemText(SystemTextID._428_FANS_COUNT_V1, lstId.Count);
			break;
		case SocialMenuData.SOCIAL_ACTION.CHOOSE_COMRADE:
		case SocialMenuData.SOCIAL_ACTION.RANDOM:	
		default:
			NGUITools.SetActive(countObj, false);
			break;
		}
	}
	/*
	protected override void OnBack ()
	{
		switch(Context.Instance.StageID)
		{
		case StageID.SocialList:
			Message.Send(MessageID.SwitchStage, StageID.SocialMenu);
			break;
		case StageID.FollowSupport:
		case StageID.RandomSupport:
		{
			Context.Instance.CachedFellow = new SocialListData();
			var members = GameSystem.Service.TeamInfo[Context.Instance.TeamIndex].Members;
			for(int i = 0; i < members.Count; ++i)
			{
				var serial = members[i];
				if(serial.IsExternal)
				{
					Context.Instance.CachedFellow.Origin = GameSystem.Service.CardInfo[serial].Owner;
					Context.Instance.CachedFellow.CachedTeamIndex = Context.Instance.TeamIndex;
					Context.Instance.CachedFellow.CachedTeamMemberIndex = i;
					break;
				}
			}
			
			Message.Send(MessageID.SwitchStage, StageID.Quest);
			break;
		}
		}
		
	}
	*/
	/*
	protected override void OnNext ()
	{
		if(Context.Instance.StageID == StageID.FollowSupport)
			Message.Send(MessageID.SwitchStage, StageID.RandomSupport);
		else if(Context.Instance.StageID == StageID.RandomSupport)
			Message.Send(MessageID.SwitchStage, StageID.FollowSupport);
	}
	*/
	protected override void OnDestroy ()
	{
		if(GameSystem.IsAvailable) 
		{
			GameSystem.Service.OnSocialQuery -= SocialQueryHandler;
			GameSystem.Service.OnSocialRandomQuery -= SocialRandomQueryHandler;
			GameSystem.Service.OnSocialUpdate -= UpdateList;
		}
		base.OnDestroy ();
	}
}