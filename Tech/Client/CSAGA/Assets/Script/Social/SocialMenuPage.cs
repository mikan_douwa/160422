﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SocialMenuPage : Stage {

	public ItemList itemList;

	protected override void Initialize ()
	{
		GameSystem.Service.OnSocialInfo += OnSocialInfo;
		GameSystem.Service.GetSocialInfo();
	}
	
	protected override void OnDestroy ()
	{
		if(GameSystem.IsAvailable) GameSystem.Service.OnSocialInfo -= OnSocialInfo;
		base.OnDestroy ();
	}
	
	void OnSocialInfo ()
	{
		List<SocialMenuData> lst = new List<SocialMenuData>();
		lst.Add(new SocialMenuData(){ Action = SocialMenuData.SOCIAL_ACTION.FOLLOW_LIST, Text = ConstData.GetSystemText(SystemTextID._401_FOLLOW_LIST) });
		lst.Add(new SocialMenuData(){ Action = SocialMenuData.SOCIAL_ACTION.FANS_LIST, Text = ConstData.GetSystemText(SystemTextID._402_FANS_LIST) });
		lst.Add(new SocialMenuData(){ Action = SocialMenuData.SOCIAL_ACTION.ADD_FOLLOW, Text = ConstData.GetSystemText(SystemTextID._403_FOLLOW) });
		lst.Add(new SocialMenuData(){ Action = SocialMenuData.SOCIAL_ACTION.MISSION, Text = ConstData.GetSystemText( SystemTextID._263_CO_MISSION) });
		lst.Add(new SocialMenuData(){ Action = SocialMenuData.SOCIAL_ACTION.SHOP, Text = ConstData.GetSystemText(SystemTextID._415_SOCIAL_SHOP) });
		itemList.Setup(lst);

		base.Initialize ();
	}
}


public class SocialMenuData
{
	public enum SOCIAL_ACTION
	{
		FOLLOW_LIST,
		FANS_LIST,
		ADD_FOLLOW,
		MISSION,
		SHOP,
		CHOOSE_COMRADE,
		RANDOM,
	}

	public SOCIAL_ACTION Action;

	public string Text;


}