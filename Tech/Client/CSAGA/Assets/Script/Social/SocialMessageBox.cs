﻿using UnityEngine;
using System.Collections;
using Mikan.CSAGA.Data;

public class SocialMessageBox : MessageBox {

	public Unit unit;
	public UILabel nameLabel;
	public UILabel rankLabel;
	public UILabel pointLabel;
	public UISprite typeSprite;

	SocialListData data;

	protected override void OnReady ()
	{
		data = (SocialListData)context.args[0];

		Debug.Log("Open id:" + data.Origin.PublicID);

		CardProperty prop = data.Origin.Card.Calc();
		//icon
		unit.CardID = data.Origin.Card.ID;
		unit.LV = prop.LV;
		unit.Rare = data.Origin.Card.Rare;
		unit.ApplyChange();
		//name
		nameLabel.text = data.Origin.Name;
		//rank
		rankLabel.text = ConstData.GetSystemText(SystemTextID.RANK) + " " + Mikan.CSAGA.PlayerUtils.CalcLV(data.Origin.Snapshot.Exp).ToString();
		//point
		if(Context.Instance.SocialAction == SocialMenuData.SOCIAL_ACTION.CHOOSE_COMRADE && data.Point > 0)
			pointLabel.text = ConstData.Tables.GetSystemText(SystemTextID._414_FP) + "+" + data.Point.ToString();
		else
			pointLabel.text = "";
		//type
		NGUITools.SetActive(typeSprite.gameObject, true);
		switch(data.Origin.Relation)
		{
		case Mikan.CSAGA.Relation.Follow:
			typeSprite.spriteName = "friend_01";
			break;
		case Mikan.CSAGA.Relation.Fans:
			typeSprite.spriteName = "friend_02";
			break;
		case Mikan.CSAGA.Relation.CrossFollow:
			typeSprite.spriteName = "friend_03";
			break;
		default:
			NGUITools.SetActive(typeSprite.gameObject, false);
			break;
		}


		base.OnReady ();
	}

	public void ShowDetail(GameObject go)
	{
		if(data.CardDetail)
		{
			Context.Instance.Card = new CardData{ Origin = data.Origin.Card, CardID = data.Origin.Card.ID };
			Message.Send(MessageID.SwitchStage, StageID.CardInfo);
			Destroy(gameObject);
		}
	}
}

