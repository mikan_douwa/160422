﻿using UnityEngine;
using System;
using System.Collections;
using Mikan.CSAGA.Data;

public class SocialListItemRenderer : ItemRenderer<SocialListData> {

	public UnitLoader unit;
	public UILabel nameLabel;
	public UILabel rankLabel;
	public UILabel pointLabel;
	public UISprite typeSprite;

	SocialListData Data;
	bool comradeChoosing = false;

	protected override void ApplyChange (SocialListData data)
	{
		Data = data;

		CardProperty prop = data.Origin.Card.Calc();
		//icon
		unit.prefabPath = "GameScene/Card/Unit";
		unit.CardID = data.Origin.Card.ID;
		unit.LV = prop.LV;
		unit.Rare = data.Origin.Card.Rare;
		unit.ApplyChange();
		//name
		nameLabel.text = data.Origin.Name;
		//rank
		rankLabel.text = ConstData.GetSystemText(SystemTextID.RANK) + " " + Mikan.CSAGA.PlayerUtils.CalcLV(data.Origin.Snapshot.Exp).ToString();
		//point
		if(Context.Instance.SocialAction == SocialMenuData.SOCIAL_ACTION.CHOOSE_COMRADE && data.Point > 0)
			pointLabel.text = ConstData.Tables.GetSystemText(SystemTextID._414_FP) + "+" + data.Point.ToString();
		else
			pointLabel.text = "";
		//type
		NGUITools.SetActive(typeSprite.gameObject, true);
		switch(data.Origin.Relation)
		{
		case Mikan.CSAGA.Relation.Follow:
			typeSprite.spriteName = "friend_01";
			break;
		case Mikan.CSAGA.Relation.Fans:
			typeSprite.spriteName = "friend_02";
			break;
		case Mikan.CSAGA.Relation.CrossFollow:
			typeSprite.spriteName = "friend_03";
			break;
		default:
			NGUITools.SetActive(typeSprite.gameObject, false);
			break;
		}
	}

	public void ShowCardInfo()
	{
		Context.Instance.Card = new CardData{ Origin = data.Origin.Card, CardID = data.Origin.Card.ID };
		Message.Send(MessageID.SwitchStage, StageID.CardInfo);
	}

	void OnClick()
	{
		ShowInfo();
		//MessageBox.Show("GameScene/Crusade/CrusadeSubmitWindow", "", new SystemTextID[]{ SystemTextID.BTN_CONFIRM, SystemTextID.BTN_CANCEL });
	}
	/*
	void OnLongPress()
	{
		if(Context.Instance.SocialAction == SocialMenuData.SOCIAL_ACTION.CHOOSE_COMRADE)
			ShowInfo();
	}
	*/
	void CancelFollowHandler(int res)
	{
		if(res == 0)
			GameSystem.Service.SocialRemove(Data.Origin.PublicID);
	}

	void AddFollowHandler(int res)
	{
		if(res == 0)
			GameSystem.Service.SocialAdd(Data.Origin.PublicID);
	}

	void ChooseComradeWithCancelHandler(int res)
	{
		if(res == 0)
		{
			ConfirmComrade();
		}
		else if(res == 1)
		{
			GameSystem.Service.SocialRemove(Data.Origin.PublicID);
		}
	}
	void ChooseComradeWithAddHandler(int res)
	{
		if(res == 0)
		{
			ConfirmComrade();
		}
		else if(res == 1)
		{
			GameSystem.Service.SocialAdd(Data.Origin.PublicID);
		}
	}

	private void ConfirmComrade()
	{
		//Choose Comrade Confirmed.
		data.CachedTeamIndex = Context.Instance.TeamIndex;
		data.CachedTeamMemberIndex = Context.Instance.TeamMemberIndex;
		Context.Instance.CachedFellow = data;
		Message.Send(MessageID.ConfirmSupport);
	}

	void ShowInfo()
	{
		switch(Data.Origin.Relation)
		{
			// Cancel Follow.
		case Mikan.CSAGA.Relation.Follow:
		case Mikan.CSAGA.Relation.CrossFollow:
		{
			if(Context.Instance.SocialAction != SocialMenuData.SOCIAL_ACTION.CHOOSE_COMRADE && Context.Instance.SocialAction != SocialMenuData.SOCIAL_ACTION.RANDOM)
			{
				MessageBoxContext msg = MessageBox.ShowConfirmWindow("GameScene/Social/SocialMessageBox"
				                                        , ConstData.GetSystemText(SystemTextID._407_TIP_CANCLE_FOLLOW_CONFIRM, Data.Origin.Name)
				                                        , CancelFollowHandler); 
				msg.args = new object[]{ Data };
			}
			else
			{
				MessageBoxContext msg = MessageBox.Show("GameScene/Social/SocialMessageBox", ""
				                                        , new SystemTextID[]{ SystemTextID._424_SELECT_SUPPORT, SystemTextID._423_CANCEL_FOLLOW, SystemTextID.BTN_CANCEL }
				                                       ); 
				msg.args = new object[]{ Data };
				msg.callback = ChooseComradeWithCancelHandler;
				msg.colors = new ButtonColor[]{ ButtonColor.Green, ButtonColor.None, ButtonColor.Red };
			}
			break;
		}
			// Add Follow.
		case Mikan.CSAGA.Relation.None:
		case Mikan.CSAGA.Relation.Fans:
		{
			if(Context.Instance.SocialAction != SocialMenuData.SOCIAL_ACTION.CHOOSE_COMRADE && Context.Instance.SocialAction != SocialMenuData.SOCIAL_ACTION.RANDOM)
			{
				MessageBoxContext msg = MessageBox.ShowConfirmWindow("GameScene/Social/SocialMessageBox"
			                                                     , ConstData.GetSystemText(SystemTextID._408_TIP_FOLLOW_CONFIRM, Data.Origin.Name)
			                                                     , AddFollowHandler); 
				msg.args = new object[]{ Data };
			}
			else
			{
				MessageBoxContext msg = MessageBox.Show("GameScene/Social/SocialMessageBox", ""
				                                        , new SystemTextID[]{ SystemTextID._424_SELECT_SUPPORT, SystemTextID._422_FOLLOW, SystemTextID.BTN_CANCEL }
				); 
				msg.args = new object[]{ Data };
				msg.callback = ChooseComradeWithAddHandler;
				msg.colors = new ButtonColor[]{ ButtonColor.Green, ButtonColor.None, ButtonColor.Red };
			}
			break;
		}
		}
	}
}
