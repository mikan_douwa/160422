﻿using UnityEngine;
using System.Collections;
using Mikan.CSAGA.Data;

public class SearchPlayerStage : Stage {

	public UIInput playerId;

	string id;
	SocialListData data;

	protected override void Initialize ()
	{
		GameSystem.Service.OnSocialQuery += SocialQueryHandler;



		base.Initialize ();
	}

	protected override void OnGetBack ()
	{
		data = Context.Instance.SearchPlayerTempData;
		if(data != null)
		{
			MessageBoxContext msg = MessageBox.ShowConfirmWindow("GameScene/Social/SocialMessageBox"
			                                                     , ConstData.GetSystemText(SystemTextID._408_TIP_FOLLOW_CONFIRM, data.Origin.Name)
			                                                     , AddFollowHandler); 
			msg.args = new object[]{ data };

		}
	}

	void SocialQueryHandler()
	{
		Fellow info = GameSystem.Service.SocialInfo[id];
		if(id == GameSystem.Service.Profile.PublicID || info == null)
		{
			MessageBox.Show(SystemTextID._410_TIP_PLAYER_ID_INVALID);
		}
		else
		{
			playerId.value = "";
			switch(info.Relation)
			{
			case Mikan.CSAGA.Relation.Follow:
			case Mikan.CSAGA.Relation.CrossFollow:
				MessageBox.Show(SystemTextID._404_TIP_ALREADY_FOLLOWED);
				break;

			default:
				data = new SocialListData(){ Origin = info, CardDetail = true };
				Context.Instance.SearchPlayerTempData = data;
				MessageBoxContext msg = MessageBox.ShowConfirmWindow("GameScene/Social/SocialMessageBox"
				                                                     , ConstData.GetSystemText(SystemTextID._408_TIP_FOLLOW_CONFIRM, data.Origin.Name)
				                                                     , AddFollowHandler); 
				msg.args = new object[]{ data };
				break;
			}
		}
	}

	public void SearchConfirm(GameObject go)
	{
		id = playerId.value;

		GameSystem.Service.SocialQuery(id);
	}

	public void SearchCancel(GameObject go)
	{
		OnBack();
	}

	public void AddFollowHandler(int res)
	{
		Context.Instance.SearchPlayerTempData = null;
		if(res == 0)
		{
			GameSystem.Service.SocialAdd(data.Origin.PublicID);
		}
	}

	protected override void OnDestroy ()
	{
		if(GameSystem.IsAvailable)
			GameSystem.Service.OnSocialQuery -= SocialQueryHandler;
		base.OnDestroy ();
	}
}
