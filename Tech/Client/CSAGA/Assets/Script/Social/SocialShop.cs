﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SocialShop : Stage {

	public UILabel donate;

	public UILabel fp;

	public ArticleList itemList;

	private List<int> purchaseList;

	protected override void Initialize ()
	{
		GameSystem.Service.OnSocialPurchaseHistory += OnSocialPurchaseHistory;
		GameSystem.Service.OnRandomShopPurchase += OnRandomShopPurchase;

		GameSystem.Service.GetSocialPurchaseHistory();
	}

	void OnSocialPurchaseHistory (List<int> list)
	{
		Context.Instance.SocialPurchaseList = list;

		itemList.Rebuild();

		donate.text = ConstData.GetSystemText(SystemTextID._419_TODAY_DONATE) + " : " + GameSystem.Service.CooperateMissionInfo.TotalPoint.ToString();
		fp.text = ConstData.GetSystemText(SystemTextID._416_FP_VI_V2, GameSystem.Service.PlayerInfo.FP, ConstData.Tables.FPMax);

		if(!IsInitialized) InitializeComplete();
	}

	protected override void OnDestroy ()
	{
		if(GameSystem.IsAvailable) 
		{
			GameSystem.Service.OnSocialPurchaseHistory -= OnSocialPurchaseHistory;
			GameSystem.Service.OnRandomShopPurchase -= OnRandomShopPurchase;
		}
		base.OnDestroy ();
	}

	void OnRandomShopPurchase (int dropId)
	{
		QuestDrop.Show(dropId);

		GameSystem.Service.GetSocialPurchaseHistory();
	}
}
