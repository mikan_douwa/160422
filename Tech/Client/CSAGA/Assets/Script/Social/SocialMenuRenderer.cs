﻿using UnityEngine;
using System.Collections;

public class SocialMenuRenderer : ItemRenderer<SocialMenuData> {
	
	public UILabel label;
	
	private SocialMenuData data;
	
	#region implemented abstract members of ItemRenderer
	
	protected override void ApplyChange (SocialMenuData data)
	{
		this.data = data;
		label.text = data.Text;
	}
	
	#endregion
	
	
	void OnClick()
	{
		switch(data.Action)
		{
		case SocialMenuData.SOCIAL_ACTION.FOLLOW_LIST:
		case SocialMenuData.SOCIAL_ACTION.FANS_LIST:
		case SocialMenuData.SOCIAL_ACTION.CHOOSE_COMRADE:
			Context.Instance.SocialAction = data.Action;
			Message.Send(MessageID.SwitchStage, StageID.SocialList);
			break;

		case SocialMenuData.SOCIAL_ACTION.MISSION:
			Message.Send(MessageID.SwitchStage, StageID.CoMission);
			break;

		case SocialMenuData.SOCIAL_ACTION.ADD_FOLLOW:
			Context.Instance.SearchPlayerTempData = null;
			Message.Send(MessageID.SwitchStage, StageID.SearchPlayer);
			break;
		case SocialMenuData.SOCIAL_ACTION.SHOP:
			Message.Send(MessageID.SwitchStage, StageID.SocialShop);
			break;
		}	
	}
	

}
