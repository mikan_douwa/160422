﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;


public class ItemList : ItemListBase
{
	public UIScrollView scrollView;

	public bool autoScrollToEnd;
	
	private int lineCount;
	
	private List<PlaceHolder> placeholders;
	
	private List<ListItem> items;
	
	private int offset;
	
	private int newOffset;
	
	private int pendingCount = 0;

	#region implemented abstract members of ItemListBase

	public override int RendererCount { get { return items.Count; } }
	
	public override ItemRenderer this[int index] { get { return items[index].Renderer; } }

	#endregion

	public override void OnItemRendererStart (ItemRenderer renderer)
	{
		base.OnItemRendererStart (renderer);

		if(scrollView == null) return;

		foreach(var item in renderer.GetComponentsInChildren<BoxCollider>())
		{
			item.gameObject.AddMissingComponent<UIDragScrollView>().scrollView = scrollView;
		}
	}

	public void ResetPosition()
	{
		if(scrollView.verticalScrollBar) scrollView.verticalScrollBar.value = 0;
		if(scrollView.horizontalScrollBar) scrollView.horizontalScrollBar.value = 0;
	}

	protected override void OnStart ()
	{
		if(scrollView == null) return;
		

		if(scrollView.verticalScrollBar != null)
		{
			scrollView.verticalScrollBar.value = 0;
			EventDelegate.Add(scrollView.verticalScrollBar.onChange, OnScroll);
		}

		if(scrollView.horizontalScrollBar != null)
		{
			scrollView.horizontalScrollBar.value = 0;
			EventDelegate.Add(scrollView.horizontalScrollBar.onChange, OnScroll);
		}
	}

	protected override void OnDestroy ()
	{
		if(scrollView != null) 
		{
			if(scrollView.verticalScrollBar != null) EventDelegate.Remove(scrollView.verticalScrollBar.onChange, OnScroll);
			
			if(scrollView.horizontalScrollBar != null) EventDelegate.Remove(scrollView.horizontalScrollBar.onChange, OnScroll);
		}
		
		base.OnDestroy ();
	}

	override protected void OnDataProviderChange ()
	{
		lineCount = (int)Math.Ceiling((float)(DataProvider.Count - viewCount) / (container.maxPerLine > 0 ? (float)container.maxPerLine : 1));
		base.OnDataProviderChange ();
	}

	protected override void OnChange ()
	{
		if(DataProvider == null) return;
		
		base.OnChange ();

		RebuildPlaceHolder();
		
		RebuildListItem();
		
		if(scrollView != null) scrollView.UpdateScrollbars(true);

		ApplyOffset();

		if(autoScrollToEnd)
		{
			if(scrollView.verticalScrollBar) scrollView.verticalScrollBar.value = 1;
			if(scrollView.horizontalScrollBar) scrollView.horizontalScrollBar.value = 1;
		}

		if(scrollView != null) SafeStartCoroutine(AdjusctPanelRoutine());

	}

	private IEnumerator AdjusctPanelRoutine()
	{
		scrollView.panel.widgetsAreStatic = true;

		while(UIBase.IsChanging) yield return new WaitForEndOfFrame();

		scrollView.panel.widgetsAreStatic = false;
		scrollView.panel.SetDirty();
	}


	private void OnScroll()
	{
		if(UIScrollBar.current != null)
			OnScroll(UIScrollBar.current.value);
	}

	private void OnScroll(float val)
	{
		val = float.IsNaN(val) ? 0 : Math.Max(0, Math.Min(val, 1));
		
		var firstLine = val < 0.5f ? (int)Math.Floor(lineCount * val) : (int)Math.Ceiling(lineCount * val);

		if(container.maxPerLine > 0)
			newOffset = firstLine * container.maxPerLine;
		else
			newOffset = firstLine;
	}
	
	private void LateUpdate()
	{
		if(offset == newOffset) return;
		offset = newOffset;
		
		ApplyOffset();
	}
	
	private void ApplyOffset()
	{
		var offset = Math.Max(0, Math.Min(this.offset, DataProvider.Count - viewCount));
		
		var count = Math.Min(DataProvider.Count, viewCount);
		
		for(int i = 0; i < count; ++i)
		{
			var index = i + offset;

			if(index >= placeholders.Count) break;

			var placeholder = placeholders[index];
			
			var item = items[index % viewCount];
			
			if(item.Parent != null) item.Parent.ListItem = null;
			
			item.Parent = placeholder;
			
			placeholder.ListItem = item;

			item.Renderer.Index = index;
		}
	}
	
	private void RebuildPlaceHolder()
	{
		bool hasChange = false;
		
		if(placeholders == null) placeholders = new List<PlaceHolder>(DataProvider.Count);
		
		while(placeholders.Count < DataProvider.Count)
		{
			var widget = CreatePlaceHolder(placeholders.Count);
			
			placeholders.Add(widget.gameObject.AddComponent<PlaceHolder>());
			
			hasChange = true;
		}
		
		while(placeholders.Count > DataProvider.Count)
		{
			var indexLast = placeholders.Count - 1;
			
			var item = placeholders[indexLast];
			
			if(item.ListItem != null)
			{
				placeholders[indexLast % items.Count].ListItem = item.ListItem;
				item.ListItem = null;
			}
			placeholders.Remove(item);
			container.RemoveChild(item.gameObject);
			
			hasChange = true;
		}
		
		if(hasChange) container.ApplyChange(true);
	}
	
	private void RebuildListItem()
	{
		var count = Math.Min(viewCount, DataProvider.Count);
		
		if(items == null) items = new List<ListItem>(count);
		
		while(items.Count < count)
		{
			var item = new ListItem();

			item.Renderer = CreateItemRenderer(items.Count);

			if(scrollView != null)
			{
				var collider = item.Renderer.GetComponent<BoxCollider>();
				if(collider != null) item.Renderer.gameObject.AddMissingComponent<UIDragScrollView>().scrollView = scrollView;
			}

			
			item.Parent = placeholders[items.Count];
			
			item.Parent.ListItem = item;
			
			items.Add(item);
		}

		while(items.Count > count)
		{
			var indexLast = items.Count - 1;
			
			var item = items[indexLast];
			
			items.Remove(item);
			
			if(item.Parent != null) item.Parent.ListItem = null;
			
			NGUITools.Destroy(item.Renderer);
		}
	}

	public void ClearAll()
	{
		if(placeholders == null) return;
		while(placeholders.Count > 0)
		{
			var indexLast = placeholders.Count - 1;
			
			var item = placeholders[indexLast];
			
			if(item.ListItem != null)
			{
				placeholders[indexLast % items.Count].ListItem = item.ListItem;
				item.ListItem = null;
			}
			placeholders.Remove(item);
			container.RemoveChild(item.gameObject);
		}

		while(items.Count > 0)
		{
			var indexLast = items.Count - 1;
			
			var item = items[indexLast];
			
			items.Remove(item);
			
			if(item.Parent != null) item.Parent.ListItem = null;
			
			NGUITools.Destroy(item.Renderer);
		}
	}

	private UIWidget CreatePlaceHolder(int index)
	{
		var widget = container.AddChild<UIWidget>();
		
		widget.name = "placeholder_" + placeholders.Count.ToString("000");

		widget.pivot = UIWidget.Pivot.TopLeft;
		widget.width = (int)container.cellWidth;
		widget.height = (int)container.cellHeight;

		if(scrollView != null)
		{
			//widget.gameObject.AddComponent<BoxCollider>().size = new Vector3(container.cellWidth, container.cellHeight);
			//widget.gameObject.AddComponent<UIDragScrollView>().scrollView = scrollView;
		}
		
		return widget;
	}
}

public class ListItem
{
	public PlaceHolder Parent;
	
	public ItemRenderer Renderer;
	
}

public class PlaceHolder : UIBase
{
	private ListItem item;
	public ListItem ListItem 
	{
		get { return item; }
		set
		{
			if(item == value) return;
			
			item = value;
			
			if(item == null || item.Renderer == null) return;
			
			Transform t = item.Renderer.gameObject.transform;
			t.parent = gameObject.transform;
			t.localPosition = Vector3.zero;
			t.localRotation = Quaternion.identity;
			t.localScale = Vector3.one;
			item.Renderer.gameObject.layer = gameObject.layer;
		}
	}
}
