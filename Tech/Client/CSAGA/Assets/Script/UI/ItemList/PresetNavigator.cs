﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class PresetNavigator : UIBase {

	public event Action<PresetNavigator> OnCurrentChange;

	public GameObject[] prefabs;

	public GameObject container;

	public GameObject blocker;

	public ComplexIcon icon;

	public float delay = 0f;

	public bool loop = false;

	public bool seamless = false;

	public int current;

	private int next;

	public GameObject currentObj;

	private Dictionary<int, GameObject> preloadObjs = new Dictionary<int, GameObject>();

	private int preloadingCount = 0;

	private bool isSlipOuting = false;

	public bool IsPreloading { get { return preloadingCount > 0; } }

	protected override void OnStart ()
	{
		if(container == null) container = gameObject;

		base.OnStart ();
	}

	public void Next()
	{
		next = current + 1;
		ApplyChange();
	}

	public void Prev()
	{
		next = current - 1;
		ApplyChange();
	}

	public void Next(int next)
	{
		this.next = next;
		ApplyChange();
	}

	protected override void OnChange ()
	{
		if(blocker != null) blocker.SetActive(true);

		var originNext = next;

		if(next < 0)
			next = prefabs.Length - 1;
		else if(prefabs.Length <= next) 
			next = 0;

		var from = 0f;

		if(loop)
		{
			if(current < originNext)
				from = 800;
			else
				from = -800;

			SlipOut(new Vector3(-from, 0));
		}
		else
		{
			if(current < next)
				from = 800;
			else
				from = -800;

			if(currentObj != null)
				SlipOut(currentObj.GetComponent<SlipEffect>().from);
		}

		StartCoroutine(SlipIn(next, new Vector3(from, 0)));
	}

	private void SlipOut(Vector3 to)
	{
		if(currentObj != null)
		{
			var obj = currentObj;

			var view = obj.GetComponent<View>();

			var effect = obj.GetComponent<SlipEffect>();
			effect.delay = 0f;
			effect.from = Vector3.zero;
			effect.to = to;
			effect.destroyOnEnd = true;
			effect.resetOnPlay = true;

			isSlipOuting = true;

			var cached = preloadObjs.ContainsValue(obj);

			effect.destroyOnEnd = !cached;

			effect.onEnd = ()=>
			{ 
				if(cached)
				{
					if(view != null) view.OnHideEnd(); 
					obj.SetActive(false);
				}
				isSlipOuting = false;
			};

			effect.Play();
			currentObj = null;
		}
	}

	public void Preload(int index)
	{
		var obj = UIManager.Instance.AddChild(container, prefabs[index]);

		StartCoroutine(PreloadRoutine (index, obj));
	}

	private IEnumerator PreloadRoutine(int index, GameObject obj)
	{
		++preloadingCount;

		preloadObjs.Add(index, obj);

		var effect = obj.AddMissingComponent<SlipEffect>();
		effect.resetOnStart = true;
		effect.from = UIDefine.OutScreen;

		var ui = obj.GetComponent<UIBase>();

		if(ui != null)
		{
			while(!ui.IsStarted) yield return new WaitForEndOfFrame();
		}
		else
			yield return new WaitForEndOfFrame();

		--preloadingCount;
	}

	private IEnumerator SlipIn(int next, Vector3 from)
	{
		yield return new WaitForFixedUpdate();

		if(delay > 0) yield return new WaitForSeconds(delay);

		while(preloadingCount > 0 || (isSlipOuting && !seamless)) yield return new WaitForFixedUpdate();

		if(preloadObjs.ContainsKey(next))
		{
			currentObj = preloadObjs[next];
			currentObj.SetActive(true);
		}
		else
			currentObj = UIManager.Instance.AddChild(container, prefabs[next]);

		currentObj.transform.localPosition = UIDefine.OutScreen;

		var view = currentObj.GetComponent<View>();

		if(view != null)
		{
			if(preloadObjs.ContainsKey(next))
				view.OnGetBack();
			else if(view.useCache)
				preloadObjs.Add(next, currentObj);
		}
		
		var effect = currentObj.GetComponent<SlipEffect>();

		if(effect == null)
		{
			effect = currentObj.AddComponent<SlipEffect>();
			effect.resetOnStart = true;
		}

		effect.onEnd = null;
		effect.from = from;
		effect.to = Vector3.zero;
		
		if(icon != null) 
		{
			icon.Index = next;
			icon.ApplyChange();
		}

		if(blocker != null) blocker.SetActive(false);
		
		if(current == next) 
		{
			effect.from = effect.to;
			while(!effect.IsStarted) yield return new WaitForSeconds(0.1f);
			effect.from = from;

			if(view != null)
			{
				while(!view.IsInitialized) yield return new WaitForSeconds(0.1f);
				view.OnShowEnd();
			}

			yield break;
		}
		
		current = next;

		var loading = UIManager.Instance.AddChild(null, Essentials.Instance.loading);

		yield return new WaitForFixedUpdate();

		while(UIBase.IsChanging) yield return new WaitForSeconds(0.1f);

		if(view != null && view.IsInitialized) view.OnGetBack();

		if(current != next) 
		{
			currentObj.transform.localPosition = Vector3.zero;
			yield break;
		}

		effect.resetOnPlay = true;

		if(view != null) 
		{
			effect.onEnd = ()=>view.OnShowEnd();

			while(!view.IsReady) yield return new WaitForEndOfFrame();
		}

		Destroy(loading);

		effect.Play();
		
		if(OnCurrentChange != null) OnCurrentChange(this);
	}

	protected override void OnDestroy ()
	{
		OnCurrentChange = null;

		base.OnDestroy ();
	}
}
