﻿using UnityEngine;
using System.Collections;

public class IndicatorItem : ItemRenderer<IndicatorItemData> {

	public SimpleToggle toggle;

	#region implemented abstract members of ItemRenderer

	protected override void ApplyChange (IndicatorItemData data)
	{
		toggle.value = (Index == data.Owner.current);
	}

	#endregion

	void OnClick()
	{
		data.Owner.OnItemClick(Index);
	}
}

public class IndicatorItemData
{
	public Indicator Owner;
}