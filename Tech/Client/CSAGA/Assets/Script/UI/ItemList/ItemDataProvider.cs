﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public interface IItemDataProvider
{
	event Action OnChange;
	int Count { get; }
}

public interface IItemDataProvider<T> : IItemDataProvider
{
	T this[int index] { get; }
}

public class ListItemDataProvider<T> : IItemDataProvider<T>
{
	#region IItemDataProvider implementation

	public event Action OnChange;

	protected List<T> list;

	public int Count {
		get {
			return list != null ? list.Count : 0;
		}
	}

	#endregion

	#region IItemDataProvider implementation

	public T this [int index] {
		get {
			if(list == null || index >= list.Count) return default(T);
			return list[index];
		}
	}

	#endregion

	public void Setup(List<T> list, bool triggerChange = true)
	{
		this.list = list;
		if(triggerChange) TriggerOnChange();
	}

	public void TriggerOnChange()
	{
		if(OnChange != null) OnChange();
	}

	public static ListItemDataProvider<T> Create(List<T> list)
	{
		var inst  =new ListItemDataProvider<T>();
		inst.Setup(list);
		return inst;
	}

}