﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PresetItemList : ItemListBase {
	public List<ItemRenderer> items;

	#region implemented abstract members of ItemListBase
	public override int RendererCount { get { return items.Count; } }
	
	public override ItemRenderer this[int index] { get { return items[index]; } }
	#endregion

	protected override void OnStart ()
	{
		foreach(var item in items) item.Owner = this;
	}

	protected override ItemRenderer CreateItemRenderer (int index)
	{
		throw new System.NotSupportedException();
	}

	protected override void OnChange ()
	{
		if(DataProvider == null) return;

		var count = Mathf.Min(RendererCount, DataProvider.Count);
		for(int i = 0; i < count; ++i) this[i].Index = i;
	}
}
