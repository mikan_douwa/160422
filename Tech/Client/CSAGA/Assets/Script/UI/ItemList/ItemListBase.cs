﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;


abstract public class ItemListBase : UIBase
{
	#region Unity
	
	public GameObject prefab;
	
	public int viewCount;
	
	public ArrangedContainer container;

	public bool autoReposition = true;
	
	#endregion

	public Func<GameObject, ItemRenderer> GetItemRendererFunc;
	
	abstract public int RendererCount { get ; }
	
	abstract public ItemRenderer this[int index] { get; }
	
	public IItemDataProvider DataProvider { get; private set; }

	public void Setup<T>(List<T> dataProvider)
	{
		Setup(ListItemDataProvider<T>.Create(dataProvider));
	}
	
	public void Setup(IItemDataProvider dataProvider)
	{
		if(DataProvider == dataProvider) return;

		if(DataProvider != null) DataProvider.OnChange -= OnDataProviderChange;

		DataProvider = dataProvider;
		DataProvider.OnChange += OnDataProviderChange;
		
		OnDataProviderChange();
	}

	public void ForceRefresh()
	{
		for(int i = 0; i < RendererCount; ++i) 
		{
			var renderer = this[i];
			renderer.ApplyChange();
		}
	}

	public void Refresh()
	{
		for(int i = 0; i < RendererCount; ++i) 
		{
			var renderer = this[i];
			renderer.Index = renderer.Index;
		}
	}

	virtual public void OnItemRendererStart(ItemRenderer renderer) {}
	
	virtual protected void OnDataProviderChange ()
	{
		ApplyChange();
	}

	virtual protected ItemRenderer GetItemRenderer(GameObject item) 
	{ 
		if(GetItemRendererFunc != null) return GetItemRendererFunc(item);
		return item.GetComponent<ItemRenderer>(); 
	}

	virtual protected ItemRenderer CreateItemRenderer(int index)
	{
		var renderer = GetItemRenderer((GameObject)GameObject.Instantiate(prefab));

		UIUtils.SetIdentity(gameObject, renderer.gameObject);
		
		renderer.Owner = this;
		
		renderer.name = "renderer_" + index.ToString("000");

		return renderer;
	}

	protected override void OnCommitChange ()
	{
		if(autoReposition) SafeStartCoroutine(RepositionRoutine());
	}

	private IEnumerator RepositionRoutine()
	{
		while(UIBase.IsChanging) yield return new WaitForEndOfFrame();

		yield return new WaitForEndOfFrame();

		foreach(var item in GetComponentsInChildren<UIScrollView>()) item.ResetPosition();
	}

	protected override void OnDestroy ()
	{
		if(DataProvider != null) DataProvider.OnChange -= OnDataProviderChange;

		base.OnDestroy ();
	}


}


