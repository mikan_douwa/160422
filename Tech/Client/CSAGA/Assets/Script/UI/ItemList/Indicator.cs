﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class Indicator : ProxyObject<Indicator> {

	public event Action<Indicator> OnCurrentChange;

	public ItemList itemList;
	
	public int count;

	public int current = -1;

	private bool isInitialized = false;

	#region implemented abstract members of ProxyObject
	protected override void Inherite (Indicator proxy){}
	protected override void InheriteChange (Indicator proxy)
	{
		count = proxy.count;
	}
	protected override void CommitChange ()
	{
		var list = new List<IndicatorItemData>(count);
		for(int i = 0; i < count; ++i) list.Add(new IndicatorItemData{ Owner = proxy != null ? proxy : this });
		
		itemList.viewCount = count;
		itemList.Setup(list);

		isInitialized = true;
	}
	#endregion

	public void GoTo(int index)
	{
		if(index == current) return;

		current = index;

		if(target == null) return;

		if(target.isInitialized && target.itemList != null) target.itemList.ForceRefresh();
	}

	public void OnItemClick(int index)
	{
		if(index == current) return;

		GoTo(index);

		if(OnCurrentChange != null) OnCurrentChange(this);
	}
}
