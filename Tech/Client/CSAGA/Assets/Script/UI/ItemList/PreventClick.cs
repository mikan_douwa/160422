﻿using UnityEngine;
using System.Collections;

public class PreventClick : MonoBehaviour {
	
	void OnPress(bool isPressed)
	{
		if(!isPressed && UICamera.currentTouch.dragStarted)
			UICamera.currentTouch.clickNotification = UICamera.ClickNotification.None;
	}
}
