﻿using UnityEngine;
using System.Collections.Generic;
using Mikan;
using System;
using System.Collections;

public class Navigator : ItemListBase {

	public enum BEHAVIOR
	{
		Default,
		SpringPanel,
	}

	public GameObject prevBtn;

	public GameObject nextBtn;

	public BEHAVIOR behavior = BEHAVIOR.Default;

	public event Action<Navigator> OnCurrentChange;

	public Indicator indicator;

	public bool repeat = false;

	public float translateDuration = 0.2f;

	public bool autoPlay = false;

	public float autoPlayInterval = 10;

	public FingerGesture fingerGesture;

	private List<NavigatorItem> items;

	private int realViewCount;

	private INaviAnimation naviAnimation;

	private bool isEmpty = true;

	public int current;

	public int Current { get { return current; } }

	#region implemented abstract members of ItemListBase

	public override int RendererCount { get { return items.Count; } }

	public override ItemRenderer this [int index] { get { return items[index].Renderer; } }

	public T GetData<T>(int index)
	{
		return (DataProvider as IItemDataProvider<T>)[index];
	}

	#endregion

	void OnEnable()
	{
		if(!isEmpty && autoPlay) AutoPlay();
	}
	
	protected override void OnAwake ()
	{
		if(behavior == BEHAVIOR.SpringPanel)
			naviAnimation = new SpringPanelAnimation(this);
		else
			naviAnimation = new DefaultAnimation(this);
	}
	
	protected override void OnDataProviderChange ()
	{
		base.OnDataProviderChange ();
		
		if(autoPlay) AutoPlay();
	}

	public ItemRenderer GetRelativeItemRenderer(int dataIndex)
	{
		if(items == null) return null;

		foreach(var item in items) 
		{
			if(item != null && item.Renderer != null && item.Renderer.Index == dataIndex) return item.Renderer;
		}
		return null;
	}

	public void GoTo(int index)
	{
		naviAnimation.GoTo(index);
	}
	public void Prev()
	{
		if(DataProvider == null || DataProvider.Count <= 1) return;

		naviAnimation.Prev();
	}
	
	public void Next()
	{
		if(DataProvider == null || DataProvider.Count <= 1) return;

		naviAnimation.Next();
	}

	private void UpdateButtons(int current)
	{
		if(DataProvider == null || DataProvider.Count <= 1)
		{
			if(prevBtn != null) prevBtn.SetActive(false);
			if(nextBtn != null) nextBtn.SetActive(false);
		}
		else
		{
			if(prevBtn != null) prevBtn.SetActive(repeat || current > 0);
			if(nextBtn != null) nextBtn.SetActive(repeat || current < DataProvider.Count - 1);
		}
	}

	protected override void OnStart ()
	{
		if(fingerGesture == null) fingerGesture = GetComponent<FingerGesture>();
		if(fingerGesture != null) EventDelegate.Add(fingerGesture.onSwip, OnFingerGestureSwip);

		if(indicator != null) indicator.OnCurrentChange += OnIndicatorCurrentChange;
	}

	private void OnIndicatorCurrentChange(Indicator sender)
	{
		GoTo(sender.current);
	}

	private void OnFingerGestureSwip()
	{
		if(container.arrangement == ArrangedContainer.ARRANGEMENT.Horizontal)
		{
			if(fingerGesture.Data.SwipDirection.x > 0)
				Prev ();
			else if(fingerGesture.Data.SwipDirection.x < 0)
				Next ();
		}
		else
		{
			if(fingerGesture.Data.SwipDirection.y < 0)
				Prev ();
			else if(fingerGesture.Data.SwipDirection.y > 0)
				Next ();
		}
	}

	private void AutoPlay()
	{
		if(naviAnimation != null) naviAnimation.AutoPlay();
	}

	virtual protected void OnForwardBegin(ItemRenderer renderer) { }

	virtual protected void OnBackwardBegin(ItemRenderer renderer) { }

	virtual protected void OnForwardEnd(ItemRenderer renderer) 
	{
		if(OnCurrentChange != null) OnCurrentChange(this);

		if(indicator != null) indicator.GoTo(Current);
	}
	
	virtual protected void OnBackwardEnd(ItemRenderer renderer) { }

	protected override void OnChange ()
	{
		if(DataProvider == null) return;

		RebuildItemRenderer();

		isEmpty = false;

		if(indicator != null)
		{
			indicator.count = DataProvider.Count;
			indicator.ApplyChange();
		}
	}

	protected override void OnDestroy ()
	{
		OnCurrentChange = null;

		if(naviAnimation != null)
		{
			naviAnimation.Dispose();
			naviAnimation = null;
		}

		if(indicator != null) indicator.OnCurrentChange -= OnIndicatorCurrentChange;

		base.OnDestroy ();
	}

	private void RebuildItemRenderer()
	{
		if(behavior == BEHAVIOR.SpringPanel)
			viewCount = DataProvider.Count;
		else
		//	viewCount = Mathf.Max(viewCount, 2);
			viewCount = 3;

		realViewCount = viewCount;

		//if(DataProvider.Count == 1) realViewCount = 1;

		//container.RemoveAllChildren();
		//items = new List<NavigatorItem>(realViewCount);

		if(items == null) items = new List<NavigatorItem>();

		while(items.Count < realViewCount)
		{
			var renderer = CreateItemRenderer(items.Count) as ItemRenderer;
			container.AddChild(renderer.gameObject);
			var item = new NavigatorItem{ Renderer = renderer };
			item.Widget = renderer.GetComponent<UIWidget>();
			items.Add(item);
		}

		naviAnimation.Reset();
	}

	class NavigatorItem
	{
		public int Position;

		public UIWidget Widget;
		
		public ItemRenderer Renderer;

		public bool IsFocused;
		
	}

	interface INaviAnimation : IDisposable
	{
		void Prev();
		void Next();
		void GoTo(int index);
		void AutoPlay();
		void Reset();
	}
	
	class DefaultAnimation : INaviAnimation
	{
		private int autoPlayNum = 0;

		private bool isTriggerAutoPlay = false;
		
		private Vector3 zeroPos;

		private Dictionary<GameObject, NavigatorItem> map;
		
		private LiveOperationQueue opQueue = new LiveOperationQueue();
		
		private Navigator navigator;
		
		public DefaultAnimation(Navigator navigator)
		{
			this.navigator = navigator;
			zeroPos = navigator.container.transform.localPosition;
		}
		
		#region INaviAnimation implementation
		public void GoTo(int index)
		{
			if(navigator.current == index) return;

			var oldValue = navigator.current;

			navigator.current = index;

			PlaySwipEffect(oldValue, navigator.current, oldValue < index);
		}
		public void Prev()
		{
			if(!opQueue.IsEmpty) return;
			if(!navigator.repeat && navigator.current <= 0) return;
			
			var oldValue = navigator.current--;
			
			if(navigator.current < 0) 
			{
				navigator.current = navigator.DataProvider.Count - 1;
			}
			
			PlaySwipEffect(oldValue, navigator.current, false);
		}
		
		public void Next()
		{
			if(!opQueue.IsEmpty) return;
			if(!navigator.repeat && navigator.current + 1 >= navigator.DataProvider.Count) return;
			
			var oldValue = navigator.current++;
			
			if(navigator.current >= navigator.DataProvider.Count) 
			{
				navigator.current = 0;
			}
			
			PlaySwipEffect(oldValue, navigator.current, true);
		}
		
		public void AutoPlay ()
		{			
			navigator.StartCoroutine(AutoPlayRoutine(++autoPlayNum));
		}
		
		public void Reset()
		{
			map = new Dictionary<GameObject, NavigatorItem>(navigator.realViewCount);
			
			foreach(var item in navigator.items) map.Add(item.Renderer.gameObject, item);

			/*
			if(navigator.container.arrangement == ArrangedContainer.ARRANGEMENT.Horizontal)
				navigator.container.startPos.x = -navigator.container.cellWidth * origin;
			else
				navigator.container.startPos.y = navigator.container.cellHeight * origin;
			*/

			opQueue.Enqueue(new CallbackOperation<int>(Reset, navigator.current));
		
			navigator.UpdateButtons(navigator.current);
		}
		#endregion
		#region IDisposable implementation
		public void Dispose ()
		{
			opQueue.Dispose();
		}
		#endregion
		
		private IEnumerator AutoPlayRoutine(int num)
		{
			yield return new WaitForSeconds(navigator.autoPlayInterval);

			if(num != autoPlayNum) yield break;

			isTriggerAutoPlay = true;
			if(navigator.autoPlay && opQueue.IsEmpty) navigator.Next ();
			isTriggerAutoPlay = false;
		}
		
		private void PlaySwipEffect(int oldValue, int newValue, bool forward)
		{
			opQueue.Enqueue(new CallbackOperation<int>(navigator.UpdateButtons, newValue));

			if(!isTriggerAutoPlay) opQueue.Enqueue(new CallbackOperation(PlaySwipSound));

			opQueue.Enqueue(new CallbackOperation<int, int>(BeginSwip, oldValue, newValue));
			
			Vector2 diff;
			
			if(navigator.container.arrangement == ArrangedContainer.ARRANGEMENT.Horizontal)
				diff = new Vector2(forward ? -navigator.container.cellWidth : navigator.container.cellWidth, 0);
			else
				diff = new Vector2(0, forward ? navigator.container.cellHeight : -navigator.container.cellHeight);
			
			opQueue.Enqueue(new TweenPositionOperation(navigator.container.gameObject, navigator.translateDuration, new Vector3(zeroPos.x + diff.x, zeroPos.y + diff.y)));
			
			opQueue.Enqueue(new CallbackOperation<int, int>(EndSwip, oldValue, newValue));
			
			if(navigator.autoPlay) opQueue.Enqueue(new CallbackOperation(AutoPlay));
		}

		private void PlaySwipSound()
		{
			Message.Send(MessageID.PlaySound, SoundID.SWITCH);
		}
		
		private void BeginSwip(int oldValue, int newValue)
		{			
			foreach(var item in navigator.items)
			{
				if(item.IsFocused)
					navigator.OnBackwardBegin(item.Renderer);
				else if(item.Renderer.Index == newValue)
					navigator.OnForwardBegin(item.Renderer);
			}
		}
		
		private void EndSwip(int oldValue, int newValue)
		{
			foreach(var item in navigator.items)
			{
				if(item.IsFocused)
				{
					navigator.OnBackwardEnd(item.Renderer);
					item.IsFocused = false;
				}
				else if(item.Renderer.Index == newValue)
				{
					navigator.OnForwardEnd(item.Renderer);
					item.IsFocused = true;
				}
			}
			Reposition(newValue);
		}

		class ItemCache
		{
			public int Index;
			public int Position;
			public NavigatorItem Item;
		}
		private void Reset(int current)
		{
			Reposition(current);
			foreach(var item in navigator.items)
			{
				item.Renderer.Index = item.Renderer.Index;

				if(item.Renderer.Index == current)
				{
					navigator.OnForwardEnd(item.Renderer);
					SetFocus(item, true);
				}
			}
		}
		private void Reposition(int current)
		{
			if(navigator.DataProvider.Count == 0) return;

			var caches = new List<ItemCache>();
			caches.Add(new ItemCache{ Position = 1, Index = (navigator.DataProvider.Count + current - 1) % navigator.DataProvider.Count });
			caches.Add(new ItemCache{ Position = 2, Index = current });
			caches.Add(new ItemCache{ Position = 3, Index = (current + 1) % navigator.DataProvider.Count });

			foreach(var item in navigator.items) item.Position = -1;
			
			foreach(var cache in caches)
			{
				foreach(var item in navigator.items)
				{
					if(item.Renderer.Index == cache.Index)
					{
						item.Position = cache.Position;
						cache.Item = item;
						break;
					}
				}
			}

			foreach(var cache in caches)
			{
				if(cache.Item != null) continue;

				foreach(var item in navigator.items)
				{
					if(item.Position == -1)
					{
						item.Position = cache.Position;
						item.Renderer.Index = cache.Index;
						cache.Item = item;
						break;
					}
				}
			}

			navigator.container.transform.localPosition = zeroPos;
			navigator.container.Sort(Compare);
		}
		
		
		private void SetFocus(NavigatorItem item, bool focused)
		{
			item.IsFocused = focused;
		}
		
		private int Compare (GameObject lhs, GameObject rhs)
		{
			return map[lhs].Position.CompareTo(map[rhs].Position);
		}
	}

	class SpringPanelAnimation : INaviAnimation
	{
		private UICenterOnChildLite centerChild;
		private Navigator navigator;

		public SpringPanelAnimation(Navigator navigator)
		{
			this.navigator = navigator;
		}
		#region INaviAnimation implementation
		public void Prev ()
		{
			if(!navigator.repeat && navigator.current == 0) return;
			if(--navigator.current < 0) navigator.current = navigator.RendererCount - 1;

			CenterCurrent();
		}
		public void Next ()
		{
			if(!navigator.repeat && navigator.current == navigator.RendererCount - 1) return;
			if(++navigator.current >= navigator.RendererCount) navigator.current = 0;

			CenterCurrent();
		}
		public void GoTo(int index)
		{
			navigator.current = index;
			CenterCurrent();
		}
		public void AutoPlay ()
		{

		}
		public void Reset ()
		{
			for(int i = 0; i < navigator.realViewCount; ++i)
			{
				var item = navigator.items[i];

				item.Renderer.Index = i;	
				
			}

			navigator.container.Sort(Compare);
			CenterCurrent();
		}
		#endregion
		#region IDisposable implementation
		public void Dispose ()
		{

		}
		#endregion

		private void CenterCurrent()
		{
			var renderer = navigator[navigator.current];
			
			var centerObj = renderer.gameObject.AddMissingComponent<UICenterOnChildLite>();

			centerObj.onCenter = OnCenter;
			centerObj.Recenter();
		}

		private int Compare (GameObject lhs, GameObject rhs)
		{
			return lhs.GetComponent<ItemRenderer>().Index.CompareTo(rhs.GetComponent<ItemRenderer>().Index);
		}

		private void OnCenter(GameObject obj)
		{
			navigator.OnForwardEnd(obj.GetComponent<ItemRenderer>());
		}
	}

	
}


