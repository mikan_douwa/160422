﻿using UnityEngine;
using System.Collections;

abstract public class ItemRenderer : MonoBehaviour
{
	private int index = -1;

	private bool isStarted = false;
	private bool isChanged = false;
	
	public ItemListBase Owner { get; set; }

	public bool IsStarted { get { return isStarted; } }
	
	public int Index {
		get {
			return index;
		}
		set {
			index = value;

			isChanged = OnIndexChange(index) | isChanged;

			OnChange();
		}
	}

	public void Start()
	{
		gameObject.AddComponent<PreventClick>();

		if(Owner != null) Owner.OnItemRendererStart(this);

		isStarted = true;

		OnStart();

		OnChange();
	}
	virtual protected void OnStart() {}

	private void OnChange()
	{
		if(isChanged && isStarted) 
		{
			ApplyChange();
			isChanged = false;
		}
	}

	protected void TriggerOnChange()
	{
		isChanged = true;
	}
	
	virtual protected bool OnIndexChange(int newIndex) { return true; }

	abstract public void ApplyChange();
	
}

abstract public class ItemRenderer<T> : ItemRenderer
	where T : class
{
	protected T data;
	
	#region implemented abstract members of ItemRenderer
	
	protected override bool OnIndexChange (int newIndex)
	{
		var dataProvider = Owner.DataProvider as IItemDataProvider<T>;

		var newData = dataProvider[newIndex];
		
		if(data == newData) return false;
		
		data = newData;
		
		return true;
	}
	
	sealed public override void ApplyChange ()
	{
		if(IsStarted)
			ApplyChange(data);
		else
			TriggerOnChange();
	}
	
	#endregion

	public void SetData(T data)
	{
		this.data = data;
	}
	
	abstract protected void ApplyChange(T data);
}
