﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SimpleToggle : MonoBehaviour {

	public int group;
	
	private bool _value;

	public GameObject obj;

	private static Dictionary<int, List<SimpleToggle>> instances = new Dictionary<int, List<SimpleToggle>>();

	protected List<SimpleToggle> list
	{
		get
		{
			var _list = default(List<SimpleToggle>);
			if(!instances.TryGetValue(group, out _list))
			{
				_list = new List<SimpleToggle>();
				instances.Add(group, _list);
			}

			return _list;
		}
	}

	public bool value 
	{
		get{ return _value; }
		set
		{
			if(_value == value) return;
			
			_value = value;
			
			if(obj == null) obj = gameObject;
			
			obj.SetActive(_value);
			
			if(group == 0 || !_value) return;
			
			foreach(var item in list)
			{
				if(item != this) item.value = false;
			}
		}
	}

	void Start () 
	{
		if(group != 0) list.Add(this);
	}
	
	void OnDestroy()
	{
		if(group == 0) return;

		list.Remove(this);
	}
	
}
