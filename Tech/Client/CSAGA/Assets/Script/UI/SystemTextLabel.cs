﻿using UnityEngine;
using System.Collections;

public class SystemTextLabel : UIBase {

	public UILabel label;

	public string bbcode = "[-]";

	public SystemTextID textId = SystemTextID.NONE;

	protected override void OnStart ()
	{
		ApplyChange();
	}

	protected override void OnChange ()
	{
		if(!label) label = gameObject.AddMissingComponent<UILabel>();

		if(textId == SystemTextID.NONE)
			label.text = "";
		else
			label.text = bbcode + ConstData.GetSystemText(textId);
	}
}
