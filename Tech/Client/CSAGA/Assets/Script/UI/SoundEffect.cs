﻿using UnityEngine;
using System.Collections;

public class SoundEffect : MonoBehaviour {

	public SoundID sound = SoundID.CLICK;

	void OnClick()
	{
		Message.Send(MessageID.PlaySound, sound);
	}
}
