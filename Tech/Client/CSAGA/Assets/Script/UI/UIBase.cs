﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class UIBase : MonoBehaviour {
	
	private static List<Type> ignore_list = new List<Type>(new Type[]{ });
	
	public static bool IsChanging
	{
		get { return changeList.size > 0 || pendingCount > 0; }
	}
	
	private static int pendingCount = 0;
	
	private static BetterList<UIBase> changeList = new BetterList<UIBase>();
	
	private bool isStarted;
	private bool isChanged;
	private bool isChangePending;
	private bool isDestroyed;
	
	private bool isActive = true;
	
	private bool isActiveChange = false;
	
	private GameObject cachedGameObject;
	
	public bool IsStarted { get { return isStarted; } }
	
	public bool IsActive { get { return isActive; } }
	
	public bool IsDestroyed { get { return isDestroyed || cachedGameObject == null; } }
	
	private DateTime changeTime;
	
	private bool isPending;
	
	protected bool IsPending
	{
		get { return isPending; }
		
		set
		{
			if(isPending == value) return;
			
			isPending = value;
			
			if(isPending)
				++pendingCount;
			else
				--pendingCount;
		}
	}
	
	protected void Awake()
	{
		cachedGameObject = gameObject;
		OnAwake();
	}
	
	virtual protected void OnAwake(){}
	
	protected void Start () {
		
		isStarted = true;
		
		OnStart();
		
		if (isActiveChange)	OnActiveChange ();
		
		if (isChanged) ApplyChange();
	}
	
	virtual protected void OnDestroy() 
	{
		CommitChange(false);
		
		isDestroyed = true;
		
		cachedGameObject = null;
		
		if(isPending) --pendingCount;
	}
	
	virtual protected void OnDisable()
	{
		if(isChanged && changeList.Remove(this)) 
		{ 
			Debug.LogWarning("De-Monitor - " + this);
		}
	}
	
	virtual protected void OnChange() { }
	
	virtual protected void OnStart() { }
	
	[ContextMenu("ApplyChange")]
	public void ApplyChange()
	{
		ApplyChange(false);
	}
	
	public void ApplyChange(bool immediately)
	{
		isChanged = true;
		changeTime = DateTime.Now;
		
		if(isStarted)
		{
			if(immediately)
			{
				OnChange();
				CommitChange(true);
			}
			else if(!isChangePending)
				SafeStartCoroutine(OnChangeRoutine());
		}
	}
	
	protected void SafeStartCoroutine(IEnumerator routine)
	{
		Coroutine.Start(DelayStartCoroutine(routine));
	}
	
	private IEnumerator DelayStartCoroutine(IEnumerator routine)
	{
		while(!IsDestroyed && !gameObject.activeInHierarchy) yield return new WaitForEndOfFrame();
		
		if(!IsDestroyed) StartCoroutine(routine);
	}
	
	public void SetActive(bool isActive)
	{
		if(this.isActive == isActive) return;
		this.isActive = isActive;
		this.isActiveChange = true;
		
		if (isStarted) OnActiveChange ();
	}
	
	virtual protected void OnActiveChange()
	{
		gameObject.SetActive(isActive);
		isActiveChange = false;
	}
	
	virtual protected void OnCommitChange() { }
	
	private void CommitChange(bool trigger)
	{
		isChanged = false;  
		changeList.Remove(this);
		
		if(trigger) OnCommitChange();
	}
	
	IEnumerator OnChangeRoutine()
	{
		if(!isChanged) yield break;
		
		if(!changeList.Contains(this) && !ignore_list.Contains(GetType())) 
		{
			changeList.Add(this);
			Coroutine.Start(MonitorChange(this));
		}
		
		isChangePending = true;
		
		yield return new WaitForEndOfFrame();
		
		isChangePending = false;
		
		if(!isChanged) yield break;
		
		OnChange();
		
		CommitChange(true);
	}
	
	private static IEnumerator MonitorChange(UIBase item)
	{
		while(item.isChanged && changeList.Contains(item))
		{
			if(DateTime.Now.Subtract(item.changeTime).TotalSeconds > 5)
			{
				Debug.LogWarning("change timeout, obj = " + item.GetType());
				changeList.Remove(item);
				yield break;
			}
			
			yield return new WaitForSeconds(1);
			
		}
	}
}
