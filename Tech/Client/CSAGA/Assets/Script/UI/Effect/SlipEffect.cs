﻿using UnityEngine;
using System.Collections;

public class SlipEffect : MonoBehaviour {
	
	public System.Action onEnd;
	
	public bool playOnStart;

	public bool destroyOnEnd;

	public bool resetOnPlay;

	public bool resetOnStart;

	public Vector3 from = Vector3.zero;

	public Vector3 to = Vector3.zero;

	public float delay;

	public float duration = 0.2f;

	private GameObject cachedGameObject;

	public bool IsStarted { get; private set; }

	void Awake()
	{
		cachedGameObject = gameObject;
	}

	void Start()
	{
		if(resetOnStart) transform.localPosition = from;

		if(playOnStart) Play();

		IsStarted = true;
	}

	public void Play()
	{
		if(cachedGameObject == null) return;

		var callback = new EventDelegate(OnTweenFinished);
		callback.oneShot = true;

		if(resetOnPlay) transform.localPosition = from;

		var tweener = TweenPosition.Begin(gameObject, duration, to);

		tweener.delay = delay;
		tweener.AddOnFinished(callback);
	}

	private void OnTweenFinished()
	{
		if(onEnd != null) onEnd();

		if(destroyOnEnd) 
		{
			cachedGameObject = null;
			Destroy(gameObject);
		}
	}

}
