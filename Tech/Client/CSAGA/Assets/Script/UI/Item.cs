﻿using UnityEngine;
using System.Collections;

public class Item : UIBase {


	public TextureLoader icon;
	public UILabel count;

	public int ItemID;
	public int Count = -1;

	public string CountFormat;

	protected override void OnChange ()
	{
		if(ItemID != 0)
		{
			var data = ConstData.Tables.Item[ItemID];
			icon.Load("ICON/" + data.s_ICON);

			if(Count == -1)
			{
				var _count = GameSystem.Service.ItemInfo.Count(ItemID);
				UpdateCount(_count.ToString());
			}
			else if(Count != 0)
				UpdateCount(Count.ToString());
			else
				UpdateCount("");
		}
		else
		{
			icon.Unload();
			UpdateCount("");
		}
	}

	private void UpdateCount(string text)
	{
		if(count != null) count.text = string.IsNullOrEmpty(CountFormat) ? text : string.Format(CountFormat, text);
	}

}
