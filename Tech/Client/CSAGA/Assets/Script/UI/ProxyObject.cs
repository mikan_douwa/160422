﻿using UnityEngine;
using System.Collections;

public class ProxyObject : UIBase 
{
	public enum BEHAVIOR
	{
		Target, 
		Proxy
	}
	
	public BEHAVIOR behavior = BEHAVIOR.Target;
	public GameObject prefab;
	public GameObject container;
	

}

abstract public class ProxyObject<T> : ProxyObject 
	where T : ProxyObject<T>
{
	
	public T target;
	protected T proxy;
	private T thisCache;
	
	abstract protected void Inherite(T proxy);
	abstract protected void InheriteChange(T proxy);
	abstract protected void CommitChange();
	
	sealed protected override void OnChange ()
	{
		if(target == null)
		{
			if(behavior == BEHAVIOR.Target)
				target = (T)this;
			else
			{
				thisCache = (T)this;
				if(container == null) container = gameObject;
				target = UIManager.Instance.AddChild(container, prefab).GetComponent<T>();
				target.proxy = thisCache;
				target.Inherite(thisCache);
			}
		}

		if(behavior == BEHAVIOR.Target)
			CommitChange();
		else
		{
			target.InheriteChange(thisCache);
			target.ApplyChange(target.IsStarted);
		}
	}
	
}
