﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Stars : ItemList {

	public enum PIVOT
	{
		LEFT,
		CENTER,
		RIGHT,
	}

	public int itemDepth = 0;

	public PIVOT pivot = PIVOT.CENTER;


	protected override ItemRenderer CreateItemRenderer (int index)
	{
		var renderer = base.CreateItemRenderer (index);

		if(itemDepth > 0)
		{
			var _renderer = renderer as StarItemRenderer;
			if(pivot == PIVOT.RIGHT)
				_renderer.depth = itemDepth - index;
			else
				_renderer.depth = itemDepth + index;
		}

		return renderer;
	}

	protected override ItemRenderer GetItemRenderer (GameObject item)
	{
		return item.AddComponent<StarItemRenderer>();
	}

	protected override void OnChange ()
	{
		switch(pivot)
		{
		case PIVOT.LEFT:
			container.startPos.x = 0;
			break;
		case PIVOT.CENTER:
			container.startPos.x = -(container.cellWidth * (viewCount - 1) * 0.5f);
			break;
		case PIVOT.RIGHT:
			container.startPos.x = -container.cellWidth * (viewCount - 1);
			break;
		}


		Setup(new Provider{ Count = viewCount });

		base.OnChange ();
	}

	class Provider : IItemDataProvider
	{
		#region IItemDataProvider implementation
		public event System.Action OnChange;
		public int Count { get; set; }
		#endregion

	}
}

public class StarItemRenderer : ItemRenderer
{
	private UIWidget widget;
	public int depth = 0;
	protected override void OnStart ()
	{
		if(depth > 0)
		{
			var widget = GetComponent<UIWidget>();
			if(widget != null) widget.depth = depth;
		}
	}

	#region implemented abstract members of ItemRenderer
	protected override bool OnIndexChange (int newIndex) { return true; }
	public override void ApplyChange () { }
	#endregion
}
