﻿using UnityEngine;
using System.Collections;

public class AnchorRoot : MonoBehaviour {

	public enum BEHAVIOR
	{
		ROOT,
		PARENT,
	}

	public GameObject root;

	public float left = 0;
	public float right = 1;
	public float bottom = 0;
	public float top = 1;

	public float leftOffset = 0;
	public float rightOffset = 0;
	public float bottomOffset = 0;
	public float topOffset = 0;

	public bool realtime = false;

	public BEHAVIOR behavior = BEHAVIOR.ROOT;

	void Awake()
	{
		if(enabled && behavior == BEHAVIOR.ROOT) ApplyChange();
	}

	void Start()
	{
		if(behavior == BEHAVIOR.PARENT) ApplyChange();
	}


	[ContextMenu("ApplyChange")]
	public void ApplyChange()
	{
		var root = this.root;

		if(root == null)
		{
			switch(behavior)
			{
			case BEHAVIOR.PARENT:
				if(transform.parent != null) root = transform.parent.gameObject;
				break;
			case BEHAVIOR.ROOT:
			default:
				break;
			}

			if(root == null) root = UIManager.Instance.GetRootObj(gameObject.layer);
		}

		var rect = GetComponent<UIRect>();

		if(root != null && rect != null)
		{
			var trans = root.transform;
			rect.leftAnchor.target = trans;
			rect.rightAnchor.target = trans;
			rect.topAnchor.target = trans;
			rect.bottomAnchor.target = trans;

#if UNITY_STANDALONE

			if(behavior == BEHAVIOR.ROOT)
			{
				if(left >= 0) rect.leftAnchor.Set(0.5f, -(UIDefine.ScreenWidth / 2 + leftOffset));
				if(right >= 0) rect.rightAnchor.Set (0.5f, UIDefine.ScreenWidth / 2 + rightOffset);
			}
			else
			{
				if(left >= 0) rect.leftAnchor.Set(left, leftOffset);
				if(right >= 0) rect.rightAnchor.Set (right, rightOffset);
			}
#else
			if(left >= 0) rect.leftAnchor.Set(left, leftOffset);
			if(right >= 0) rect.rightAnchor.Set (right, rightOffset);
#endif

			if(bottom >= 0) rect.bottomAnchor.Set(bottom, bottomOffset);
			if(top >= 0) rect.topAnchor.Set(top, topOffset);
			
		}
	}

	void Update()
	{
		if(realtime) ApplyChange();
	}
}
