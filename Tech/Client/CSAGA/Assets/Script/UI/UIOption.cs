﻿using UnityEngine;
using System.Collections;

public class UIOption : MonoBehaviour {

	public OptionID option;

	public UIToggle toggle;

	private bool isInitialized = false;
	void Start()
	{
		toggle.value = Context.Instance.Option[option];
		isInitialized = true;
	}

	public void OnToggle()
	{
		if(!isInitialized) return;

		Context.Instance.Option[option] = toggle.value;
	}
}
