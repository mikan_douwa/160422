﻿using UnityEngine;
using System.Collections;

public class SelectObject : MonoBehaviour {
	
	private CardChosenTip tip;
	
	public object data;
	
	public bool Selected { get { return Context.Instance.Selections.Contains(data); } }
	
	public void Select(int maxCount, bool replaceZero)
	{
		var selected = Selected;
		
		if(selected)
		{
			if(replaceZero)
			{
				var pos = Context.Instance.Selections.IndexOf(data);
				if(pos > -1) Context.Instance.Selections[pos] = null;
			}
			else
				Context.Instance.Selections.Remove(data);
			
			if(tip != null) Destroy(tip.gameObject);
			tip = null;
			return;
		}
		
		if(replaceZero)
		{
			var replaced = false;
			for(int i = 0; i < Context.Instance.Selections.Count; ++i)
			{
				if(Context.Instance.Selections[i] != null) continue;
				Context.Instance.Selections[i] = data;
				replaced = true;
				break;
			}
			
			if(!replaced) 
			{
				if(Context.Instance.Selections.Count >= maxCount) return;
				Context.Instance.Selections.Add(data);
			}
		}
		else
		{
			if(Context.Instance.Selections.Count >= maxCount) return;
			Context.Instance.Selections.Add(data);
		}
		
		SetSelected();
		
	}
	
	public void UpdateInitSelected()
	{
		if(Selected) 
			SetSelected();
		else if(tip != null) 
		{
			Destroy(tip.gameObject);
			tip = null;
		}
		
	}
	
	private void SetSelected()
	{
		if(tip != null) return;
		
		ResourceManager.Instance.CreatePrefabInstance(gameObject, "GameScene/Card/ChosenTip", (obj)=>{
			
			
			if(!Context.Instance.Selections.Contains(data))
			{
				Destroy(obj);
				if(tip != null) Destroy(tip.gameObject);
				tip = null;
			}
			else
			{
				tip = obj.GetComponent<CardChosenTip>();
				tip.data = data;
				tip.transform.localPosition = new Vector3(34f,34f);
			}
		});	
	}

	void OnDestroy()
	{
		if(tip != null) Destroy(tip.gameObject);
		tip = null;
	}
}
