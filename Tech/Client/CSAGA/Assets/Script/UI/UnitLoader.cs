﻿using UnityEngine;
using System.Collections;

public class UnitLoader : UIBase {

	public string prefabPath = "Template/Unit";

	public int CardID;

	public int LV;

	public int Rare;

	private Unit unit;

	protected override void OnStart ()
	{
		ResourceManager.Instance.CreatePrefabInstance(gameObject, prefabPath, OnCreateComplete);
	}
	
	private void OnCreateComplete(GameObject obj)
	{
		unit = obj.GetComponent<Unit>();

		var parent = gameObject.GetComponent<UIWidget>();

		if(parent != null)
		{
			var widget = obj.GetComponent<UIWidget>();
			widget.width = parent.width;
			widget.height = parent.height;
		}

		ApplyChange();
	}

	protected override void OnChange ()
	{
		if(unit == null) return;
		unit.CardID = CardID;
		unit.LV = LV;
		unit.Rare = Rare;
		unit.ApplyChange();
	}
}
