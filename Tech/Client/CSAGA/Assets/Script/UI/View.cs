﻿using UnityEngine;
using System.Collections;

public class View : UIBase {

	public GameObject appendixPrefab;

	public string appendixText;

	public SystemTextID appendixTextID;

	public bool useCache;

	private GameObject appendix;

	private bool isVisible;

	public bool IsInitialized { get; private set; }

	virtual public bool IsReady { get { return IsInitialized; } }
		
	virtual public void OnShowEnd()
	{
		isVisible = true;
		if(string.IsNullOrEmpty(appendixText) && appendixTextID != SystemTextID.NONE)
			appendixText = ConstData.GetSystemText(appendixTextID);

		if(appendixPrefab == null && string.IsNullOrEmpty(appendixText)) return;

		if(appendix == null) appendix = Headline.Append(appendixPrefab, appendixText);

		appendix.SetActive(true);
	}

	virtual public void OnHideEnd()
	{
		isVisible = false;
		if(appendix != null) appendix.SetActive(false);
	}

	virtual public void OnRecycle(){}

	virtual public void OnGetBack() { }

	protected void UpdateAppendix(GameObject prefab, string text)
	{
		if(appendixPrefab == prefab && appendixText == text) return;

		appendixPrefab = prefab;
		appendixText = text;

		if(appendix != null) 
		{
			Destroy(appendix);
			appendix = null;
		}

		if(appendixPrefab == null && string.IsNullOrEmpty(appendixText)) return;
		
		appendix = Headline.Append(appendixPrefab, appendixText);
	}

	protected override void OnStart ()
	{
		Message.AddListener(MessageID.AppendStage, OnAppendStage);
		Message.AddListener(MessageID.RemoveStage, OnRemoveStage);
		ApplyChange();
	}

	protected override void OnDestroy ()
	{
		Message.RemoveListener(MessageID.AppendStage, OnAppendStage);
		Message.RemoveListener(MessageID.RemoveStage, OnRemoveStage);

		if(appendix != null) Destroy(appendix);

		base.OnDestroy ();
	}

	private void OnAppendStage()
	{
		if(!isVisible) return;

		if(appendix != null) appendix.SetActive(false);
	}

	private void OnRemoveStage()
	{
		if(!isVisible) return;

		OnShowEnd();
	}
	
	protected override void OnChange ()
	{
		Initialize();
	}
	
	virtual protected void Initialize() 
	{
		InitializeComplete();
	}

	virtual protected void OnInitializeComplete(){}

	protected void InitializeComplete()
	{
		SafeStartCoroutine(SetInitialized());
	}
	
	private IEnumerator SetInitialized()
	{
		yield return new WaitForEndOfFrame();
		
		while(UIBase.IsChanging) yield return new WaitForEndOfFrame();
		
		IsInitialized = true;

		OnInitializeComplete();
	}
}
