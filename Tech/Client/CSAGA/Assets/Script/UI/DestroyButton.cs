﻿using UnityEngine;
using System.Collections;

public class DestroyButton : MonoBehaviour {

	public GameObject target;
	void OnClick()
	{
		if(target == null) target = gameObject;

		Destroy(target);
	}
}
