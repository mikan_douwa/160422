﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(UIWidget))]
public class FitParentWidget : MonoBehaviour {

	public UIWidget parent;
	public UIWidget target; 
	// Use this for initialization
	void Start () {
		if(target == null) target = GetComponent<UIWidget>();
		if(parent == null) parent = target.transform.parent.GetComponent<UIWidget>();

		if(target.width != parent.width) target.width = parent.width;
		if(target.height != parent.height) target.height = parent.height;
	}
}
