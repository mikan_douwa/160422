﻿using UnityEngine;
using System.Collections;

public class ComplexIcon : UIBase {

	public string[] sprites;
	
	public SystemTextID[] texts;
	
	public UISprite sprite;
	public UILabel text;
	
	public int Index;
	
	protected override void OnChange ()
	{
		text.text = ConstData.GetSystemText(texts[Index]);
		sprite.spriteName = sprites[Index];
	}
}
