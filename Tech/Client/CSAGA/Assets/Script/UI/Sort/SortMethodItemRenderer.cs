﻿using UnityEngine;
using System.Collections;

public class SortMethodItemRenderer : ItemRenderer<SortMethodData> {
	public UILabel label;
	#region implemented abstract members of ItemRenderer

	protected override void ApplyChange (SortMethodData data)
	{
		var selected = SortMethod.Serial;
		switch(Context.Instance.SortTarget)
		{
		case SortTarget.Card:
			selected = Context.Instance.SortCard.Value;
			break;
		case SortTarget.Equipment:
			selected = Context.Instance.SortEquipment.Value;
			break;
		}
		label.text = (data.Method == selected ? "[ffff00]" : "[ffffff]") + ConstData.GetSortMethodText(data.Method);
	}

	#endregion

	void OnClick()
	{
		Message.Send(MessageID.SortMethod, data.Method);
	}

}

public class SortMethodData
{
	public SortMethod Method;
}
