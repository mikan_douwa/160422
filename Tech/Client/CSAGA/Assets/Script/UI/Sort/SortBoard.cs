﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SortBoard : Stage {

	private static SortMethod[] CARD_COLLECTION = { SortMethod.Serial, SortMethod.Rare, SortMethod.Kizuna, SortMethod.Type, SortMethod.Group, SortMethod.LV, SortMethod.HP, SortMethod.Atk, SortMethod.Rev, SortMethod.Chg, SortMethod.Luck, SortMethod.ID };
	private static SortMethod[] EQUIPMENT_COLLECTION = { SortMethod.Serial, SortMethod.Rare, SortMethod.HP, SortMethod.Atk, SortMethod.Rev, SortMethod.Chg, SortMethod.Counter01, SortMethod.Counter02, SortMethod.Counter04, SortMethod.Counter08, SortMethod.Counter16 };

	public ItemList itemList;

	protected override void Initialize ()
	{
		switch(Context.Instance.SortTarget)
		{
		case SortTarget.Card:
			Initialize(CARD_COLLECTION);
			break;
		case SortTarget.Equipment:
			Initialize(EQUIPMENT_COLLECTION);
			break;
		}

		Message.AddListener(MessageID.SortMethod, OnSortMethod);

		base.Initialize ();
	}

	protected override void OnDestroy ()
	{
		Message.RemoveListener(MessageID.SortMethod, OnSortMethod);
		base.OnDestroy ();
	}

	private void Initialize(SortMethod[] methods)
	{
		var list = new List<SortMethodData>(methods.Length);
		foreach(var item in methods)
		{
			var data = new SortMethodData{ Method = item };
			list.Add(data);
		}
		itemList.Setup(list);
	}

	private void OnSortMethod(IMessage msg)
	{
		var method = (SortMethod)msg.Data;
		
		switch(Context.Instance.SortTarget)
		{
		case SortTarget.Card:
			Context.Instance.SortCard.Value = method;
			break;
		case SortTarget.Equipment:
			Context.Instance.SortEquipment.Value = method;
			break;
		}
		
		Message.Send(MessageID.Sort, method);
		
		Destroy(gameObject);
	}
}
