﻿using UnityEngine;
using System.Collections;

public class SortButton : MonoBehaviour {

	public SortTarget target;

	public UILabel method;


	// Use this for initialization
	void Start () {
		Message.AddListener(MessageID.SelectItem, OnSelectItem);

		switch(target)
		{
		case SortTarget.Card:
			method.text = ConstData.GetSortMethodText(Context.Instance.SortCard.Value);
			break;
		case SortTarget.Equipment:
			method.text = ConstData.GetSortMethodText(Context.Instance.SortEquipment.Value);
			break;
		}
	}

	void OnDestroy()
	{
		Message.RemoveListener(MessageID.SelectItem, OnSelectItem);
	}

	void OnClick()
	{
		Context.Instance.SortTarget = target;
		
		Message.Send(MessageID.SwitchStage, StageID.SortBoard);
	}

	private void OnSelectItem(IMessage msg)
	{
		if(msg.Data is SortMethod && target == Context.Instance.SortTarget)
		{
			method.text = ConstData.GetSortMethodText((SortMethod)msg.Data);
		}
	}
	
}
