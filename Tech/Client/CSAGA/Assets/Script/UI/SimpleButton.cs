﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SimpleButton : MonoBehaviour {

	public List<EventDelegate> onClick = new List<EventDelegate>();

	void OnClick()
	{
		EventDelegate.Execute(onClick);
	}
}
