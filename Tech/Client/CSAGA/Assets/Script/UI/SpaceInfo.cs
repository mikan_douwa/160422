﻿using UnityEngine;
using System.Collections;

public class SpaceInfo : UIBase {

	public enum TYPE
	{
		CARD,
		EQUIPMENT
	}

	public TYPE type;

	public UILabel label;

	protected override void OnChange ()
	{
		switch(type)
		{
		case TYPE.CARD:
		{
			var count = ConstData.Tables.CardSpaceCalc.Count(GameSystem.Service.PurchaseInfo.Count(Mikan.CSAGA.ArticleID.CardSpace));
			UpdateLabel(GameSystem.Service.CardInfo.List.Count, count);
			break;
		}
		case TYPE.EQUIPMENT:
		{
			var count = ConstData.Tables.Player[GameSystem.Service.PlayerInfo.LV].n_EQUIP_SPACE;
			var current = 0;
			foreach(var item in GameSystem.Service.EquipmentInfo.List) 
			{
				if(item.CardSerial.IsEmpty) ++current;
			}
			UpdateLabel(current, count);
			break;
		}
		default:
			label.text = "";
			break;
		}
	}

	private void UpdateLabel(int current, int max)
	{
		
		label.text = string.Format(current > max ? "[ff0000]{0}[ffffff]/{1}" : "[ffffff]{0}/{1}", current, max);
	}
}
