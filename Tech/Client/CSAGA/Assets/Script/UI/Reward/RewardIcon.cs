﻿using UnityEngine;
using System.Collections;

public class RewardIcon : MonoBehaviour {

	public Icon icon;

	public void Hide()
	{
		icon.IconType = Mikan.CSAGA.DropType.None;
		icon.manualIconName = "";
		icon.ApplyChange();
	}

	public void ApplyChange(Mikan.CSAGA.Data.Gift gift)
	{
		icon.IconType = gift.Type;
		icon.IconID = gift.Value2;
		
		if(gift.Type == Mikan.CSAGA.DropType.Card)
		{
			icon.Count = gift.Value1 / 1000;
			icon.Rare = (gift.Value1 % 1000) / 100;
			icon.Luck = gift.Value1 % 100 + 1;
		}
		else
		{
			icon.Count = gift.Value1;
			icon.Rare = 0;
			icon.Luck = 0;
		}
		
		icon.ApplyChange();
	}

	public void ApplyChange(Mikan.CSAGA.ConstData.Drop drop)
	{
		icon.IconType = drop.n_DROP_TYPE;
		icon.IconID = drop.n_DROP_ID;
		icon.Count = drop.n_COUNT;
		icon.Rare = drop.n_RARE;
		icon.Luck = drop.n_LUCK + 1;
		icon.ApplyChange();
	}
}
