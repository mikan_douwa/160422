﻿using UnityEngine;
using System.Collections;

public class RewardItemRenderer : ItemRenderer<RewardData> {
	
	public RewardIcon icon;
	public UILabel description;
	public UILabel progress;
	public UISprite getSprite;
	
	protected override void ApplyChange (RewardData data)
	{
		if(data.Trophy != null)
			ApplyChange(data.Trophy);
		else if(data.Gift != null)
			ApplyChange(data.Gift);
		else if(data.TrophyConstObj != null)
			ApplyChange(data.TrophyConstObj);
	}

	private void ApplyChange(Mikan.CSAGA.Data.Gift gift)
	{
		description.text = gift.Description;

		icon.ApplyChange(gift);
	}

	private void ApplyChange(Mikan.CSAGA.Data.Trophy trophy)
	{
		var data = ConstData.Tables.Trophy[trophy.Current];
		progress.text = string.Format("{0}/{1}", trophy.Progress, data.n_EFFECT[0]);

		description.text = ConstData.Tables.TrophyText[trophy.Current].s_NAME;
		
		var drop = ConstData.Tables.Drop[data.n_BONUS_LINK];

		icon.ApplyChange(drop);
	}

	private void ApplyChange(Mikan.CSAGA.ConstData.Trophy trophy)
	{
		int score = trophy.n_EFFECT[0];

		if(data.IsPVPReward)
			progress.text = ConstData.GetSystemText(SystemTextID._203_EVENT_POINT) + (score + 1000).ToString();
		else
			progress.text = ConstData.GetSystemText(SystemTextID._203_EVENT_POINT) + score.ToString();
		
		Mikan.CSAGA.ConstData.Drop drop = ConstData.Tables.Drop[data.TrophyConstObj.n_BONUS_LINK];
		if(drop != null) icon.ApplyChange(drop);
		//get or not
		bool isGet = false;
		if(GameSystem.Service.RankingInfo != null)
		{
			Mikan.CSAGA.Data.Ranking info = GameSystem.Service.RankingInfo[Context.Instance.EventScheduleID];
			if(info != null)
			{
				isGet = (score <= info.Point);
			}
		}
		if(getSprite != null)
			NGUITools.SetActive(getSprite.gameObject, isGet);
	}

	void OnClick()
	{
		if(data.Gift != null)
		{
			switch(data.Gift.Type)
			{
			case Mikan.CSAGA.DropType.Card:
				if(!ShopManager.Instance.CheckCardSpace(1)) return;
				break;
			case Mikan.CSAGA.DropType.Item:
			{
				var itemData = ConstData.Tables.Item[data.Gift.Value2];
				if(itemData.n_TYPE == Mikan.CSAGA.ItemType.Equipment && !QuestManager.Instance.CheckEquipmentSpace(1)) return;
				break;
			}
			}
			
			Context.Instance.GiftClaimCount = 1;
			GameSystem.Service.GiftClaim(data.Gift.Serial);
		}
	}
}
