﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

abstract public class RewardListView : View {

	public GameObject container;

	public ItemList itemList;
	
	public GameObject itemPrefab;

	
	protected override void Initialize ()
	{
		Rebuild();
	}
	
	protected void Rebuild()
	{
		if(container == null) container = gameObject;

		if(itemList == null)
			ResourceManager.Instance.CreatePrefabInstance(container, "GameScene/Reward/RewardList", OnCreateComplete);
		else
			Setup();
	}
	
	abstract protected void Setup();

	protected void Setup(List<RewardData> list)
	{
		itemList.Setup(list);
	}
	
	private void OnCreateComplete(GameObject obj)
	{
		if(itemList != null) Destroy(itemList.gameObject);
		
		itemList = obj.GetComponent<ItemList>();
		
		if(itemPrefab != null) itemList.prefab = itemPrefab;
		
		Setup();

		if(!IsInitialized) InitializeComplete();
		
	}
}

public class RewardData
{
	public Mikan.CSAGA.Data.Gift Gift;
	public Mikan.CSAGA.Data.Trophy Trophy;
	public Mikan.CSAGA.ConstData.Trophy TrophyConstObj;
	public bool IsPVPReward;
}