﻿using UnityEngine;
using System.Collections;

public class MessageButton : MonoBehaviour {

	public MessageID messageId;

	public int param;

	public bool enableLongPress;

	void Start()
	{
		var longPress = gameObject.AddMissingComponent<LongPressDetector>();

		longPress.clickFunctionName = "onClick";

		if(enableLongPress) 
		{
			longPress.longPressFunctionName = "onLongPress";
			longPress.longReleaseFunctionName = "onLongPressRelease";
		}
		else
		{
			longPress.longPressFunctionName = "";
			longPress.longReleaseFunctionName = "";
		}
	}
	void onClick()
	{
		Message.Send(messageId, param);
	}

	void onLongPress()
	{
		StartCoroutine(SendContinous());
	}

	void onLongPressRelease()
	{
		isSending = false;
	}

	private bool isSending = false;

	IEnumerator SendContinous()
	{
		if(isSending) yield break;

		isSending = true;

		while(isSending)
		{
			onClick();
			yield return new WaitForSeconds(0.05f);
		}
	}

}
