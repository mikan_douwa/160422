﻿using UnityEngine;
using System.Collections;

public class ScrollableLabel : UIBase {

	public UILabel label;
	public UIWidget container;

	public string Text;

	public float leftSpace = 20f;

	public float rightSpace = 10f;

	public float speed = 1f;

	private Vector2 origin;

	public UILabel Label 
	{
		get
		{
			if(label == null) label = GetComponent<UILabel>();
			return label;
		}
	}

	public UIWidget Container 
	{
		get
		{
			if(container == null) container = transform.parent.GetComponent<UIWidget>();
			return container;
		}
	}

	protected override void OnChange ()
	{
		if(Label.text == Text) return;

		Label.text = Text;

		StopAllCoroutines();
		StartCoroutine(BeginScroll());
	
	}

	IEnumerator BeginScroll()
	{
		yield return StartCoroutine(Reset());

		var overflow = Label.width - Container.width - rightSpace;

		if(overflow > 0)
		{
			while(true)
			{
				var seconds = speed * (overflow / 100f);

				TweenX.Begin(Label.gameObject, seconds, -overflow);

				yield return new WaitForSeconds(seconds + 1.5f);

				yield return StartCoroutine(Reset());
			}

		}
	}

	private IEnumerator Reset()
	{
		Label.transform.localPosition = new Vector3(Container.width - rightSpace, 0);
		TweenX.Begin(Label.gameObject, 0.3f, leftSpace);
		yield return new WaitForSeconds(1.8f);
	}
}
