﻿using UnityEngine;
using System.Collections;

public class SwitchTexture : MonoBehaviour {

	public float duration = 0.2f;

	private bool isTweening = false;

	private string current;

	private string next;

	private TextureLoader loader;
	private UITexture texture;

	void Awake()
	{
		texture = GetComponent<UITexture>();
		loader = GetComponent<TextureLoader>();
	}

	public void Switch(string path)
	{
		if(next == path) return;
		next = path;
		
		if(isTweening) return;

		isTweening = true;

		LoadNext();
	}

	private void LoadNext()
	{
		if(string.IsNullOrEmpty(current))
		{
			texture.alpha = 0;
			OnFadeOutFinished();
		}
		else
		{
			var tweener = TweenAlpha.Begin(texture.gameObject, duration, 0);
			AddOnTweenFinished(tweener, OnFadeOutFinished);
		}
	}
	
	private void OnFadeOutFinished()
	{
		loader.Unload();

		current = next;

		if(!string.IsNullOrEmpty(current) && !current.EndsWith("/null"))
			loader.Load(current);

		var tweener = TweenAlpha.Begin(texture.gameObject, duration, 1);
		AddOnTweenFinished(tweener, OnFadeInFinished);
	}



	private void OnFadeInFinished()
	{
		if(next != current)
			LoadNext();
		else
			isTweening = false;
	}

	private void AddOnTweenFinished(UITweener tween, EventDelegate.Callback callback) 
	{
		var del = new EventDelegate(callback);
		del.oneShot = true;
		tween.AddOnFinished(del);
	}
}
