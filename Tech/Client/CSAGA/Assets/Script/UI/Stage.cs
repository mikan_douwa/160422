﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Stage : UIBase {

	private static List<Stage> instances = new List<Stage>();
	private static Stage current = null;

	public bool IsInitialized { get; private set; }

	public enum TitleFormat
	{
		None,
		Static,
		Dynamic,
		Custom,
		DontCare,
	}

	public TitleFormat titleFormat = TitleFormat.None;

	public SystemTextID titleTextID;

	public StageID backToStage = StageID.Home;

	public string titleText;

	public GameObject titlePrefab;

	public bool disableBackButton = false;

	public bool disableNextButton = true;

	public GameObject nextButtonPrefab;

	public string nextButtonText;

	public int nextButtonWidth = 135;

	public bool manualOffset = false;

	public Vector4 offset = Vector4.zero;

	public StageObject parent;

	public bool IsManaged { get; set; }
	
	protected bool IsAppendixHidden { get; set; }

	virtual public HideType CustemHideType { get { return HideType.None; } }

	virtual public bool SwitchPrepare() { return true; }

	protected override void OnStart ()
	{
		Message.AddListener(MessageID.Back, onBack);
		Message.AddListener(MessageID.PhysicalBack, onPhysicalBack);
		Message.AddListener(MessageID.Next, onNext);

		instances.Add(this);
		current = this;

		Initialize();
	}

	virtual protected void Initialize()
	{
		InitializeComplete();
	}

	protected override void OnDestroy ()
	{
		base.OnDestroy ();

		if(!GameSystem.IsAvailable) return;

		Message.RemoveListener(MessageID.Back, onBack);
		Message.RemoveListener(MessageID.PhysicalBack, onPhysicalBack);
		Message.RemoveListener(MessageID.Next, onNext);

		instances.Remove(this);

		if(current == this)
		{
			current = null;
			if(instances.Count > 0)
				current = instances[instances.Count - 1];

			if(!IsManaged)
			{
				if(titleFormat != TitleFormat.DontCare)
				{
					if(current != null) current.UpdateHeadline();
				}

				if(current != null) Coroutine.DelayInvoke(current.GetBack);
			}
		}

		if(!IsManaged && parent != null) Destroy(parent.gameObject);
	}

	private void onPhysicalBack()
	{
		if(Popup.InstanceCount == 0 && (!GameSystem.IsAvailable || !GameSystem.Service.IsBusy)) onBack();
	}

	private void onBack()
	{
		if(current == this) OnBack();
	}

	private void onNext()
	{
		if(current == this) OnNext();
	}

	virtual protected void OnBack()
	{
		var isLastInstance = instances.Count == 1;
		
		if(isLastInstance) 
			Message.Send(MessageID.SwitchStage, backToStage);
		else if(parent != null) 
			Destroy(parent.gameObject);
		else if(!IsDestroyed)
			Destroy(gameObject);
	}

	virtual protected void OnNext()
	{
	}

	virtual public void OnShowEnd() {}

	protected void InitializeComplete()
	{
		ApplyChange();
		//UpdateHeadline();
		SafeStartCoroutine(SetInitialized());
	}

	public void GetBack()
	{
		OnGetBack();
	}

	virtual protected void OnInitializeComplete() { }

	virtual protected void OnGetBack() { }

	virtual protected void UpdateHeadlinePrepare() { }

	public void UpdateHeadline()
	{
		UpdateHeadline(false);
	}

	public void UpdateHeadline(bool temporary)
	{
		if(current != this) return;

		UpdateHeadlinePrepare();

		var behavior = new Headline.Behavior{ IsTemporary = temporary, IsAppendixHidden = IsAppendixHidden };

		if(!temporary)
		{
			behavior.DisableBackButton = disableBackButton;
			behavior.DisableNextButton = disableNextButton;

			if(!behavior.DisableNextButton)
			{
				behavior.NextButtonWidth = nextButtonWidth;
				behavior.NextButtonText = nextButtonText;
				behavior.NextButtonPrefab = nextButtonPrefab;
			}
		}
		
		switch(titleFormat)
		{
		case TitleFormat.Static:
			titleText = ConstData.GetSystemText(titleTextID);
			break;
		case TitleFormat.Dynamic:
			break;
		case TitleFormat.None:
			if(temporary)
				titleText = "";
			else
			{
				Message.Send(MessageID.HideHeadline);
				return;
			}
			break;
		case TitleFormat.Custom:
			behavior.TitlePrefab = titlePrefab;
			break;
		case TitleFormat.DontCare:
			return;
		}
		
		behavior.Title = titleText;
		
		Message.Send(MessageID.ShowHeadline, behavior);
	}

	private IEnumerator SetInitialized()
	{
		yield return new WaitForEndOfFrame();

		while(UIBase.IsChanging) yield return new WaitForEndOfFrame();

		IsInitialized = true;

		OnInitializeComplete();
	}
}
