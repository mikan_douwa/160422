﻿using UnityEngine;
using System.Collections;

public class Blocker : MonoBehaviour {

	void Awake()
	{
		gameObject.AddComponent<BoxCollider>();

		var widget = gameObject.AddComponent<UIWidget>();
		widget.depth = -1;
		widget.autoResizeBoxCollider = true;

		UIUtils.FitRoot(widget);
	}

	void OnEnable()
	{
		FingerGestureManager.Instance.OnBlockerActive(gameObject, true);
	}

	void OnDisable()
	{
		if(FingerGestureManager.Instance != null)
			FingerGestureManager.Instance.OnBlockerActive(gameObject, false);
	}

	void OnDestroy()
	{
		OnDisable();
	}
}
