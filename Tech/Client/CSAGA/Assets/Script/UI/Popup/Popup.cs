﻿using UnityEngine;
using System.Collections.Generic;

public class Popup : MonoBehaviour {

	private static List<Popup> instances = new List<Popup>();

	private static GameObject blocker;

	private static GameObject manualBlock;

	private static int instanceCount = 0;

	private static int blockCount = 0;

	public static bool IsBlocking { get { return blockCount > 0; } }

	public static bool IsEmpty { get{ return instances.Count == 0; } }

	public static int InstanceCount { get { return instanceCount; } }


	public static void Block()
	{
		++blockCount;

		if(manualBlock != null) return;

		manualBlock = new GameObject();
		manualBlock.name = "block";
		manualBlock.layer = (int)UILayer.Popup;
		UIManager.Instance.AddToRoot(manualBlock);
	}

	public static void Unblock()
	{
		if(blockCount == 0) return;

		--blockCount;

		if(blockCount > 0 || manualBlock == null) return;

		NGUITools.Destroy(manualBlock);

		manualBlock = null;
	}

	public static void Clear()
	{
		if(blocker) 
		{
			NGUITools.Destroy(blocker);
			blocker = null;
		}

		foreach(var item in instances)
		{
			if(item) NGUITools.Destroy(item);
		}


		instances = new List<Popup>();
	}

	void Awake()
	{
		++instanceCount;
	}

	// Use this for initialization
	void Start () {

		if(gameObject.layer == (int)UILayer.Popup)
		{
			if(gameObject == blocker) return;

			if(instances.Count == 0) SetBlockerActive(true);

			instances.Add(this);
		}
		else
		{
			AddBlocker(gameObject);
		}


		BringFront(gameObject);
	}
	

	void OnDestroy()
	{
		--instanceCount;

		if(gameObject.layer == (int)UILayer.Popup)
		{
			instances.Remove(this);

			if(instances.Count == 0) SetBlockerActive(false);
		}
	}

	private static void SetBlockerActive(bool active)
	{
		if(blocker == null) 
		{
			if(!active) return;

			blocker = AddBlocker(UIManager.Instance.GetRootObj(UILayer.Popup));
		}

		blocker.SetActive(active);

	}

	public static void BringFront(GameObject go)
	{
		var panel = go.AddMissingComponent<UIPanel>();

		var add = 0;
		
		foreach(var item in UIManager.Instance.GetRootObj(go.layer).GetComponentsInChildren<UIPanel>())
		{
			if(item == panel) continue;
			if(item.depth >= panel.depth + add) add = item.depth - panel.depth + 1;
		}
		
		if(add > 0)
		{
			foreach(var item in go.GetComponentsInChildren<UIPanel>()) item.depth += add;
		}
	}

	public static GameObject AddBlocker(GameObject go)
	{
		var blocker = new GameObject();
		blocker.name = "_blocker";
		UIManager.Instance.SetIdentity(go, blocker, false);
		blocker.AddComponent<Blocker>();

		return blocker;
	}
}
