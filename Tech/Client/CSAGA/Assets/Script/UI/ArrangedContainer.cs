﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class ArrangedContainer : UIBase
{
	public enum ARRANGEMENT
	{
		Horizontal,
		Vertical,
	}

	public enum DIRECTION
	{
		TopLeft,
		TopRight,
		BottomLeft,
		BottomRight,
	}
	
	public float cellWidth;
	public float cellHeight;
	
	public ARRANGEMENT arrangement;
	
	public int maxPerLine;

	public Vector2 startPos = Vector2.zero;

	public DIRECTION direction = DIRECTION.TopLeft;

	public bool alignCenter = false;

	public bool flexible = false;
	
	private List<GameObject> children = new List<GameObject>();

	public T AddChild<T>() where T : Component
	{
		var obj = NGUITools.AddChild<T>(gameObject);
		children.Add(obj.gameObject);
		return obj;
	}

	public void AddChild(GameObject go)
	{
		UIUtils.SetIdentity(gameObject, go);
		children.Add(go);
	}

	public void RemoveChild(GameObject obj)
	{
		if(children.Remove(obj)) NGUITools.Destroy(obj);
	}

	public void RemoveAllChildren()
	{
		if(children.Count == 0) return;
		foreach(var item in children) NGUITools.Destroy(item);
		children = new List<GameObject>();
	}

	public void Sort(Comparison<GameObject> comparison)
	{
		children.Sort(comparison);
		ApplyChange(true);
	}

	public void Sort(IComparer<GameObject> comparison)
	{
		children.Sort(comparison);
		ApplyChange(true);
	}
	
	protected override void OnChange ()
	{
		base.OnChange ();

		var startPos = new Vector2(this.startPos.x, this.startPos.y);

		if(alignCenter)
		{
			var offset = 0;

			if(maxPerLine > 0)
			{
				if(flexible)
					offset = Mathf.Min(maxPerLine, children.Count);
				else
					offset = maxPerLine;
			}
			else
				offset = children.Count;

			if(offset > 0) --offset;

			if(arrangement == ARRANGEMENT.Horizontal)
			{
				startPos.x -= (cellWidth * offset) / 2;
			}
			else
			{
				startPos.y += (cellHeight * offset) / 2;
			}
		}

		int x = 0;
		int y = 0;

		var gap = CalcCellGap();
		
		foreach(var item in children)
		{
			Transform t = item.transform;
			
			//if (!NGUITools.GetActive(t.gameObject)) continue;
			
			float depth = t.localPosition.z;
			Vector3 pos = (arrangement == ARRANGEMENT.Horizontal) ?
				new Vector3(gap.x * x + startPos.x, gap.y * y + startPos.y, depth) :
					new Vector3(gap.x * y + startPos.x, gap.y * x + startPos.y, depth);
			
			t.localPosition = pos;
			
			if (++x >= maxPerLine && maxPerLine > 0)
			{
				x = 0;
				++y;
			}
		}
	}

	private Vector2 CalcCellGap()
	{
		switch(direction)
		{
		case DIRECTION.TopLeft:
			return new Vector2(cellWidth, -cellHeight);	
		case DIRECTION.TopRight:
			return new Vector2(-cellWidth, -cellHeight);
		case DIRECTION.BottomLeft:
			return new Vector2(cellWidth, cellHeight);
		case DIRECTION.BottomRight:
			return new Vector2(-cellWidth, cellHeight);
		default:
			return new Vector2(cellWidth, -cellHeight);
		}

	}
}