﻿using UnityEngine;
using System.Collections;

public class SceneTransition : MonoBehaviour {

	public static SceneTransition Instance { get; private set; }
	public bool IsFading { get; private set; }
	public bool fadeIn = false;
	
	// Use this for initialization
	void Start () {

		Instance = this;

		if(fadeIn) FadeIn();
	}

	void OnDestroy()
	{
		if(Instance == this) Instance = null;
	}
	
	private void FadeIn()
	{
		IsFading = true;
		GetComponent<UIWidget>().alpha = 0f;
		var tweener = TweenAlpha.Begin(gameObject, 0.2f, 1f);
		EventDelegate.Add(tweener.onFinished, OnTweenFinished);
	}

	public void FadeOut()
	{
		IsFading = true;
		var tweener = TweenAlpha.Begin(gameObject, 0.2f, 0f);
		EventDelegate.Add(tweener.onFinished, OnTweenFinished);
	}

	private void OnTweenFinished()
	{
		IsFading = false;
	}

}
