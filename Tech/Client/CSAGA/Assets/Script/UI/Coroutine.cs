﻿using UnityEngine;
using System.Collections;
using System;

public class Coroutine : MonoBehaviour {

	private static Coroutine instance;
	private static Coroutine Instance 
	{
		get
		{
			if(instance == null)
			{
				var go = new GameObject();
				go.name = "[AUTO]Coroutine";
				instance = go.AddComponent<Coroutine>();
			}
			return instance;
		}
	}
	public static void Start(IEnumerator routine)
	{
		Instance.StartCoroutine(routine);
	}

	public static void DelayInvoke(Action callback)
	{
		Start(InvokeRoutine(callback));
	}

	public static void DelayInvoke(Action callback, float seconds)
	{
		Start(InvokeRoutine(callback, seconds));
	}

	private static IEnumerator InvokeRoutine(Action callback)
	{
		yield return new WaitForEndOfFrame();
		callback();
	}

	private static IEnumerator InvokeRoutine(Action callback, float seconds)
	{
		yield return new WaitForSeconds(seconds);
		callback();
	}

	public static void StopAll()
	{
		if(instance != null) Destroy(instance.gameObject);
		instance = null;
	}
}
