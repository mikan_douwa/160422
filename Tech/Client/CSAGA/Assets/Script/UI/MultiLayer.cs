﻿using UnityEngine;
using System.Collections;

public class MultiLayer : MonoBehaviour {

	void Start()
	{
		foreach(var item in GetComponentsInChildren<UIPanel>()) 
		{
			UIManager.Instance.AddCamera(item.gameObject.layer);
		}
	}
}
