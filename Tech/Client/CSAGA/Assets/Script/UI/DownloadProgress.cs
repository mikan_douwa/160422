﻿using UnityEngine;
using System.Collections;

public class DownloadProgress : MonoBehaviour {

	public UIProgressBar progressBar;

	private float progress = 0;

	private float current = 0;

	void Start ()
	{
		Message.AddListener(MessageID.DownloadEnd, OnDownloadEnd);
		Message.AddListener(MessageID.DownloadProgress, OnDownloadProgress);
	}

	void FixedUpdate()
	{
		var value = 1f;
		
		if(PatchManager.Instance.DownloadedFileCount < PatchManager.Instance.TotalFileCount) 
			value = ((float)PatchManager.Instance.DownloadedFileCount + progress) / (float)PatchManager.Instance.TotalFileCount;
		
		current = value;
		
		if(current <= progressBar.value) return;
		
		var diff = current - progressBar.value;
		
		var tick = Time.deltaTime * 2;
		
		if(diff < tick)
			progressBar.value = current;
		else
			progressBar.value += tick;
	}

	void OnDestroy()
	{
		Message.RemoveListener(MessageID.DownloadEnd, OnDownloadEnd);
		Message.RemoveListener(MessageID.DownloadProgress, OnDownloadProgress);
	}

	private void OnDownloadProgress(IMessage msg)
	{
		progress = (float)msg.Data;
	}
	
	private void OnDownloadEnd()
	{
		Destroy(gameObject);
	}
}
