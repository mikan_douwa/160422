﻿using UnityEngine;
using System.Collections;

public class SwitchStage : MonoBehaviour {

	public StageID stageId;
	
	void OnClick()
	{
		if(stageId != StageID.None)
			Message.Send(MessageID.SwitchStage, stageId);
	}
}
