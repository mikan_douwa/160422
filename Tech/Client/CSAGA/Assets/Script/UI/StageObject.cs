﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class StageObject : MonoBehaviour {

	public StageContext Context;

	public Stage stage;
	
	public UIWidget container;

	private UIWidget stageWidget;

	private UIWidget rect;

	private HideType hideType;

	public HideType HideType { get { return hideType; } }

	public bool IsManaged { get { return stage != null && stage.IsManaged; } }

	public bool IsStarted { get; private set; }

	public bool IsInitialized { get; private set; }

	public bool IsComplete{ get; private set; }

	public bool IsDestroyed { get; private set; }

	public void AddStage(GameObject prefab)
	{
		stage = UIManager.Instance.AddChild(container.gameObject, prefab).GetComponent<Stage>();
		stage.parent = this;

		if(Context.HideType == HideType.Custom)
			hideType = stage.CustemHideType;
		else
			hideType = Context.HideType;

		stageWidget = stage.GetComponent<UIWidget>();

		if(stage.manualOffset)
			ApplyOffset(rect, stage.offset);
		else
			ApplyOffset(rect, new Vector4(0, 0, 65, -185));

		StartCoroutine(DelayUpdateAnchors());
	}

	private IEnumerator DelayUpdateAnchors()
	{
		yield return new WaitForEndOfFrame();

		rect.ResetAndUpdateAnchors();

		container.width = rect.width;
		container.height = rect.height;

		if(stageWidget != null)
		{
			var fitParent = stage.gameObject.AddComponent<FitParent>();
			fitParent.behavior = FitParent.BEHAVIOR.WIDGET;

			while(!fitParent.IsComplete) yield return new WaitForEndOfFrame();
		}

		if(stage != null)
		{
			while(!stage.IsInitialized) yield return new WaitForEndOfFrame();
		}

		IsInitialized = true;
	}

	private void ApplyOffset(UIWidget rect, Vector4 offset)
	{
#if UNITY_STANDALONE
		rect.leftAnchor.Set(0.5f, -(UIDefine.ScreenWidth / 2 + offset.x));
		rect.rightAnchor.Set (0.5f, UIDefine.ScreenWidth / 2 + offset.y);
#else
		rect.leftAnchor.Set(0, offset.x);
		rect.rightAnchor.Set (1, offset.y);
#endif
		
		rect.bottomAnchor.Set(0, offset.z);
		rect.topAnchor.Set(1, offset.w);
	}

	void Awake()
	{
		rect = gameObject.AddComponent<UIWidget>();
	}

	void Start () {

		var root = UIManager.Instance.GetRootObj(gameObject.layer);
		
		var trans = root.transform;
		
		rect.leftAnchor.target = trans;
		rect.rightAnchor.target = trans;
		rect.topAnchor.target = trans;
		rect.bottomAnchor.target = trans;

		container = UIManager.Instance.AddChild<UIWidget>(gameObject);
		container.transform.localPosition = new Vector3(UIDefine.ScreenWidth + 70f, 0f);

		IsStarted = true;
	}

	public void Show(bool smooth)
	{
		StartCoroutine(ShowRoutine(smooth));
	}

	private IEnumerator ShowRoutine(bool smooth)
	{
		if(Context.IsAppend) 
		{
			yield return new WaitForEndOfFrame();

			ShowImmediately();

			IsComplete = true;

			yield break;
		}

		if(stage != null)
		{
			if(stage.titleFormat == Stage.TitleFormat.None) stage.UpdateHeadline();
		}

		if(smooth)
		{
			var callback = new EventDelegate(OnTweenerFinished);
			callback.oneShot = true;
			TweenX.Begin(container.gameObject, 0.1f, 0).AddOnFinished(callback);

			for(int i = 0; i < 10; ++i)
			{
				foreach(var scrollView in stage.GetComponentsInChildren<UIScrollView>()) 
				{
					scrollView.panel.SetDirty();
					scrollView.ResetPosition();
				}

				yield return new WaitForSeconds(0.02f);
			}
		}
		else
		{
			yield return new WaitForEndOfFrame();

			ShowImmediately();
		}

		IsComplete = true;

		if(stage != null) stage.OnShowEnd();
	}

	private void ShowImmediately()
	{
		container.transform.localPosition = Vector3.zero;

		foreach(var scrollView in stage.GetComponentsInChildren<UIScrollView>()) 
		{
			scrollView.panel.SetDirty();
			scrollView.ResetPosition();
		}
		
		if(stage != null) stage.UpdateHeadline();
		
		TryShowTeach();
	}

	private void OnTweenerFinished()
	{
		if(stage != null) stage.UpdateHeadline();
		TryShowTeach();
	}

	private void TryShowTeach()
	{
		if(Context.Tutorials == null || Context.Tutorials.Count == 0 || !global::Context.Instance.IsTutorialComplete || global::Context.Instance.TeachFlags[Context.ID]) return;

		var list = new List<TeachContent>(); 
		
		foreach(var item in Context.Tutorials) list.Add(new TeachContent("", item.s_FILENAME));
		
		ResourceManager.Instance.CreatePrefabInstance("Teach/Teach", o=>
		                                              {
			var clip = o.GetComponent<TeachClip>();
				clip.Init(list, ()=>{ global::Context.Instance.TeachFlags[Context.ID] = true; });
		});

	}

	

	void OnDestroy()
	{ 
		IsDestroyed = true;
		if(GameSystem.IsAvailable && StageManager.Instance != null) StageManager.Instance.Remove(this); 
	}

	private int hideCount = 0;
	public void SetActive(bool active)
	{
		if(active)
			--hideCount;
		else
			++hideCount;

		if(hideCount < 0) hideCount = 0;

		if(!IsDestroyed)
			gameObject.SetActive(hideCount == 0);
	}

	public void ForceActive()
	{
		if(hideCount == 0) return;

		hideCount = 0;

		if(!IsDestroyed)
			gameObject.SetActive(true);
	}
}

