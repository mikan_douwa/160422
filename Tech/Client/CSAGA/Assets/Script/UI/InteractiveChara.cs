﻿using UnityEngine;
using System.Collections;
using Mikan.CSAGA.Calc;
using System;

public class InteractiveChara : UIBase {

	public TextureLoader pic;
	
	public InteractiveLabel label;

	public int InteractiveID;

	public int EventID;

	public Mikan.Serial CardSerial = Mikan.Serial.Empty;

	protected override void OnChange ()
	{
		var card = !CardSerial.IsEmpty ? GameSystem.Service.CardInfo[CardSerial] :  Context.Instance.Card.Origin;
		
		pic.Load(ConstData.GetCardPicPath(card.ID));

		label.EventID = EventID;

		if(EventID == 0)
		{
			if(InteractiveID != 0)
				label.InteractiveID = InteractiveID;
			else
				label.InteractiveID = CardEvent.Next(CardEvent.Filter.Common, card.ID, card.Kizuna, GameSystem.Service.CardInfo.IDs);
		}
		label.ApplyChange();
	}

}
