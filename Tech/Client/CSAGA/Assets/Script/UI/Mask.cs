﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Mask : UIBase {

	public UIWidget target;

	public int depthLimit = int.MaxValue;

	public Color color;

	public float alpha = 0.6f;

	protected override void OnStart ()
	{
		if(target == null) target = GetComponent<UIWidget>();

		var children = new List<UIWidget>(GetComponentsInChildren<UIWidget>());

		foreach(var item in transform.parent.GetComponentsInChildren<UIWidget>())
		{
			if(children.Contains(item) || item == target || item.depth < target.depth) continue;
			target.depth = item.depth + 1;
		}

		foreach(var item in children)
		{
			if(item == target) continue;
			item.depth = target.depth + item.depth;
		}

		ApplyChange();
	}

	protected override void OnChange ()
	{
		if(target.depth >= depthLimit) target.depth = depthLimit - 1;

		target.color = new Color(color.r, color.g, color.b, alpha);
	}
}
