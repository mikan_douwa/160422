﻿using UnityEngine;
using System.Collections;
using Mikan.CSAGA;

public class InteractiveLabel : UIBase {

	public int InteractiveID;

	public int EventID;

	public string EventText;

	public UILabel label;

	protected override void OnStart ()
	{
		if(label == null) label = GetComponent<UILabel>();

	}

	protected override void OnChange ()
	{
		Mikan.CSAGA.ConstData.EventShow data = null;

		if(EventID != 0)
			data = ConstData.Tables.EventShow[EventID];
		else if(InteractiveID != 0)
			data = EventUtils.CallEvent(InteractiveID);

		if(data == null)
			label.text = string.IsNullOrEmpty(EventText) ? "" : EventText;
		else
			label.text = Mikan.CSAGA.ConstData.Tables.Format(ConstData.GetEventText(data.n_ID), GameSystem.Service.PlayerInfo.Name);
	}
}
