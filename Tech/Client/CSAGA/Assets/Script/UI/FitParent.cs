﻿using UnityEngine;
using System.Collections;

public class FitParent : MonoBehaviour {

	public enum BEHAVIOR
	{
		ROOT,
		WIDGET,
	}
	
	public BEHAVIOR behavior = BEHAVIOR.ROOT;

	public UIWidget parent;
	public UIWidget target; 

	public bool once = false;

	public bool IsComplete{ get; private set; }

	// Use this for initialization
	void Start () {
		
		if(target == null) target = GetComponent<UIWidget>();

		if(parent == null && behavior == BEHAVIOR.WIDGET) 
			parent = target.transform.parent.GetComponent<UIWidget>();

		var trans = default(Transform);

		switch(behavior)
		{
		case BEHAVIOR.ROOT:
			trans = UIManager.Instance.GetRootObj(gameObject.layer).transform;
			break;
			
		case BEHAVIOR.WIDGET:
		default:
			if(parent != null) trans = parent.transform;
			break;
		}

		if(trans == null)
		{
			//GameSystem.Report(typeof(FitParent), "FitParent not work, name = {0}, behavior = {1}, parent = {2}", gameObject.name, behavior, parent);
			IsComplete = true;
			return;
		}

		target.leftAnchor.target = trans;
		target.rightAnchor.target = trans;
		target.topAnchor.target = trans;
		target.bottomAnchor.target = trans;

#if UNITY_STANDALONE
		if(behavior == BEHAVIOR.ROOT)
		{
			target.leftAnchor.Set(0.5f, -UIDefine.ScreenWidth / 2);
			target.rightAnchor.Set (0.5f, UIDefine.ScreenWidth / 2);
		}
		else
		{
			target.leftAnchor.Set(0, 0);
			target.rightAnchor.Set (1, 0);
		}
#else
		target.leftAnchor.Set(0, 0);
		target.rightAnchor.Set (1, 0);
#endif

		target.bottomAnchor.Set(0, 0);
		target.topAnchor.Set(1, 0);
		
		target.ResetAndUpdateAnchors();

		if(once)
		{
			target.leftAnchor.target = null;
			target.rightAnchor.target = null;
			target.topAnchor.target = null;
			target.bottomAnchor.target = null;
		}

		IsComplete = true;
	}
}
