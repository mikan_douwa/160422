﻿using UnityEngine;
using System.Collections;

public class Icon : UIBase {

	public Mikan.CSAGA.DropType IconType;
	public int IconID;
	public int Count;
	public int Rare;
	public int Luck;
	
	public UILabel iconName;

	public string manualIconSprite;

	public string manualIconName;

	public Vector2 manualIconSize = Vector2.zero;

	public bool fitParent = true;

	public int baseDepth;

	private GameObject content;

	private int serial;

	public void ShowInfo()
	{
		switch(IconType)
		{
		case Mikan.CSAGA.DropType.Card:
		{
			var exp = Mikan.CSAGA.CardUtils.CalcExp(IconID, Count);
			Context.Instance.Card = new CardData{ CardID = IconID, Origin = Mikan.CSAGA.CardUtils.GenerateDummy(IconID, Rare, exp) };
			if(Luck > 0) Context.Instance.Card.Luck = Luck;
			Message.Send(MessageID.SwitchStage, StageID.CardInfo);
			break;
		}
		case Mikan.CSAGA.DropType.Item:
		{
			var data = ConstData.Tables.Item[IconID];
			if(data.n_TYPE == Mikan.CSAGA.ItemType.Equipment) 
			{
				Message.Send(MessageID.ShowMessageBox, MessageBoxContext.Create("Common/EquipmentInfo3", new EquipmentData{ ItemID = IconID }));
			}
			else
				ItemInfo.Show(IconID);
			break;
		}
		case Mikan.CSAGA.DropType.Tactics:
		{
			var data = ConstData.Tables.Tactics[IconID];
			TacticInfo.Show(data);
			break;
		}
		}
	}

	protected override void OnChange ()
	{
		if(content != null)
		{
			Destroy(content);
			content = null;
		}

		++serial;

		switch(IconType)
		{
		case Mikan.CSAGA.DropType.Card:
			ShowCard();
			break;
		case Mikan.CSAGA.DropType.Item:
			ShowItem();
			break;
		case Mikan.CSAGA.DropType.Coin:
			ShowCoin();
			break;
		case Mikan.CSAGA.DropType.Crystal:
			ShowCrystal();
			break;
		case Mikan.CSAGA.DropType.Gem:
			ShowGem();
			break;
		case Mikan.CSAGA.DropType.FP:
			ShowFP();
			break;
		case Mikan.CSAGA.DropType.None:
			ShowManualIcon();
			break;
		case Mikan.CSAGA.DropType.Quest:
			ShowQuest();
			break;
		case Mikan.CSAGA.DropType.Tactics:
			ShowTactics();
			break;
		}
	}

	protected override void OnDestroy ()
	{
		this.serial = -1;
		base.OnDestroy ();
	}

	private void ShowCard()
	{
		var cardData = ConstData.Tables.Card[IconID];
		if(iconName != null) iconName.text = string.Format("{0} {1}", ConstData.GetCardName(cardData.n_ID), ConstData.GetSystemText(SystemTextID.LV_V1,  Count));

		var serial = this.serial;

		ResourceManager.Instance.CreatePrefabInstance(gameObject, "Template/Unit", o=>
		{
			if(IsDestroyed) return;

			if(serial != this.serial) 
			{
				Destroy(o);
				return;
			}
			content = o;
			var unit = o.GetComponent<Unit>();
			unit.CardID = IconID;
			unit.Rare = Rare;
			unit.ApplyChange();
			FitParent(o);
		});
	}
	
	private void ShowItem()
	{
		UpdateDrop(ConstData.GetItemName(IconID));

		var serial = this.serial;

		ResourceManager.Instance.CreatePrefabInstance(gameObject, "Template/Item", o=>
		{
			if(IsDestroyed) return;

			if(serial != this.serial) 
			{
				Destroy(o);
				return;
			}
			content = o;
			var item = o.GetComponent<Item>();
			item.ItemID = IconID;
			item.Count = 0;
			item.ApplyChange();
			if(iconName != null)
				iconName.depth = item.GetComponent<UIWidget>().depth + 3;
			FitParent(o);
		});
	}

	public void ShowQuest()
	{
		content = AddTexture(gameObject, "UI/ICON_DAILY_DISCOVER", 3);
		UpdateDrop(ConstData.GetSubQuestName(IconID), true);
	}

	private void ShowCrystal()
	{
		UpdateDrop(ConstData.GetSystemText(SystemTextID.CRYSTAL));
		
		AddIcon("img_crystal", 1);
	}

	private void ShowCoin()
	{
		UpdateDrop(ConstData.GetSystemText(SystemTextID.COIN));
		
		AddIcon("img_token", 2);
	}

	private void ShowGem()
	{
		UpdateDrop(ConstData.GetSystemText(SystemTextID.GEM));
		
		AddIcon("img_diamond", 3);
	}

	private void ShowFP()
	{
		UpdateDrop(ConstData.GetSystemText(SystemTextID._414_FP));
		
		AddIcon("button_friend");
	}

	private void ShowTactics()
	{
		var data = ConstData.Tables.Tactics[IconID];
		content = AddTexture(gameObject, "ICON/" + data.s_ICON, 3);

		var textData = ConstData.Tables.TacticsText[IconID];

		UpdateDrop(textData.s_NAME, true);
	}

	private void ShowManualIcon()
	{
		if(iconName != null) iconName.text = manualIconName;
		var obj = AddIcon(manualIconSprite);

		if(obj != null && manualIconSize != Vector2.zero)
		{
			var widget = obj.GetComponent<UIWidget>();
			widget.width = (int)manualIconSize.x;
			widget.height = (int)manualIconSize.y;
		}
	}
	
	private GameObject AddIcon(string spriteName, int borderIndex = 0)
	{
		if(string.IsNullOrEmpty(spriteName)) return null;

		content = new GameObject();
		UIManager.Instance.SetIdentity(gameObject, content);

		var obj = AddIcon(content, spriteName, 3);

		if(borderIndex > 0)
		{
			AddIcon(content, string.Format("IC_UNDER_{0:000}", borderIndex), 2);
			obj.transform.localScale = new Vector3(0.8f, 0.8f);
		}

		/*
		content.AddComponent<UISprite>().depth = 2;

		var sprite = content.AddComponent<SpriteLoader>();
		sprite.spriteName = spriteName;
		sprite.Load("UI_COMMON/UI_COMMON");

		FitParent(content);
		*/

		return obj;
	}

	private GameObject AddIcon(GameObject parent, string spriteName, int depth)
	{
		var obj = new GameObject();
		UIManager.Instance.SetIdentity(parent, obj);
		
		obj.AddComponent<UISprite>().depth = baseDepth > 0 ? baseDepth + depth : depth;
		
		var sprite = obj.AddComponent<SpriteLoader>();
		sprite.spriteName = spriteName;
		sprite.Load("UI_COMMON/UI_COMMON");
		
		FitParent(obj);

		return obj;
	}

	private GameObject AddTexture(GameObject parent, string path, int depth)
	{
		var obj = new GameObject();
		UIManager.Instance.SetIdentity(parent, obj);
		var texture = obj.AddComponent<UITexture>();

		texture.depth = baseDepth > 0 ? baseDepth + depth : depth;

		ResourceManager.Instance.LoadTexture(path, texture);

		return obj;
	}

	public void RefreshIconName()
	{
		UpdateDrop(manualIconName);
	}
	
	private void UpdateDrop(string name, bool ignoreCount = false)
	{
		if(iconName != null) 
		{
			if(string.IsNullOrEmpty(manualIconName))
			{
				if(ignoreCount)
					iconName.text = name;
				else
					iconName.text = string.Format("{0} x {1}", name, Count);
			}
			else
				iconName.text = manualIconName;
		}
	}

	private void FitParent(GameObject obj)
	{
		if(!fitParent) return;

		var fitParentObj = obj.AddMissingComponent<FitParent>();
		fitParentObj.behavior = global::FitParent.BEHAVIOR.WIDGET;
		fitParentObj.parent = gameObject.GetComponent<UIWidget>();
		fitParentObj.target = obj.GetComponent<UIWidget>();
	}
}
