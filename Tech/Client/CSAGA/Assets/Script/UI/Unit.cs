﻿using UnityEngine;
using System;
using System.Collections;

public class Unit : UIBase {
	
	public int CardID;
	
	public string borderPrefix;
	
	public string headerPrefix;
	
	public UISprite border;
	
	public TextureLoader header;

	public GameObject lvContainer;
	public UILabel lvLabel;
	public UILabel luckLabel;

	public UIWidget lckObj;

	public UISprite prefixIcon;

	public Stars stars;

	public GameObject externalObj;
	
	private int curCardId = int.MinValue;

	public float LV;
	
	public int Rare;

	public SystemTextID LVFormat = SystemTextID.LV_V1;

	public string TextFormat;

	public bool IsLocked;

	public bool ShowPrefixIcon;

	public bool IsExternal = false;

	public string PrefixIconSpriteName = "image_star";

	protected override void OnAwake ()
	{
		base.OnAwake ();
		border.spriteName = null;
	}
	
	protected override void OnChange ()
	{
		base.OnChange ();

		if(lvLabel != null) 
		{
			var text = string.IsNullOrEmpty(TextFormat) ? LV.ToString() : LV.ToString(TextFormat);

			if(LVFormat == SystemTextID.NONE)
				lvLabel.text = text;
			else
				lvLabel.text = ConstData.GetSystemText(LVFormat, text);
		}

		if(lckObj != null) lckObj.alpha = IsLocked ? 1 : 0;

		if(prefixIcon != null)
		{
			if(ShowPrefixIcon) prefixIcon.spriteName = PrefixIconSpriteName;
			prefixIcon.gameObject.SetActive(ShowPrefixIcon);
		}

		if(stars != null) 
		{
			stars.viewCount = Rare;
			stars.ApplyChange();
		}

		if(externalObj != null)
			externalObj.SetActive(IsExternal);
		
		if(curCardId != CardID) ChangeCard();
	}
	
	protected void ChangeCard()
	{
		curCardId = CardID;
		
		var data = ConstData.Tables.Card[curCardId];
		
		if(data != null)
		{
			border.spriteName = borderPrefix + data.n_TYPE.ToString("000");
			header.Load("ICON/" + headerPrefix + data.s_FILENAME);
			if(lvContainer != null) lvContainer.SetActive(true);
		}
		else
		{
			if(curCardId == UIDefine.EmptyCardID)
				border.spriteName = "IC_Q";
			else if(curCardId == UIDefine.RemoveCardID)
				border.spriteName = "IC_REMOVE";
			else if(curCardId == UIDefine.FellowCardID)
				border.spriteName = "button_friend";
			else
				border.spriteName = "";
			header.Unload();

			if(Context.Instance.StageID == StageID.Gallery) return;

			if(lvContainer != null) lvContainer.SetActive(false);
		}

		if(luckLabel != null) luckLabel.text = GameSystem.Service.CardInfo.GetLuck(CardID).ToString();
	}
	
}
