﻿using UnityEngine;
using System.Collections;

public class CDButton : MonoBehaviour {

	public UIButton button;
	public float duration = 1f;

	public void ChangeState(bool active)
	{
		ChangeState(active, duration);
	}
	public void ChangeState(bool active, float duration)
	{
		StopAllCoroutines();

		if(button != null) 
		{
			if(active)
				button.isEnabled = true;
			else
			{
				button.isEnabled = false;
				StartCoroutine(DelayReactiveRoutine(duration));
			}

		}
	}

	// Use this for initialization
	void Start () {
		if(button == null) button = GetComponent<UIButton>();
		if(button != null) EventDelegate.Add(button.onClick, OnButtonClick);
	}

	private void OnButtonClick()
	{
		ChangeState(false);
	}

	IEnumerator DelayReactiveRoutine(float duration)
	{
		var beginTime = System.DateTime.Now;

		while(true) 
		{
			yield return new WaitForSeconds(0.1f);
			if(System.DateTime.Now.Subtract(beginTime).TotalSeconds > duration) break;
		}

		button.isEnabled = true;
	}
	

}
