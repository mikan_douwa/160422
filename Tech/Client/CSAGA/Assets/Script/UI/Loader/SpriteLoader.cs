﻿using UnityEngine;
using System.Collections;

public class SpriteLoader : LoaderBase<UIAtlas> {

	public UISprite sprite;

	public string spriteName;

	protected override IAsset<UIAtlas> BeginLoad (string path)
	{
		return ResourceManager.Instance.LoadAtlas(path);
	}
	
	protected override void EndLoad (UIAtlas content)
	{
		if(sprite == null) sprite = gameObject.AddMissingComponent<UISprite>();
		
		sprite.atlas = content;

		sprite.spriteName = spriteName;
	}
}
