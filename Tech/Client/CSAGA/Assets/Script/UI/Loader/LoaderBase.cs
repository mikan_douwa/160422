﻿using UnityEngine;
using System.Collections;

abstract public class LoaderBase<T> : MonoBehaviour {

	private IAsset<T> asset;

	private bool isStarted = false;
	private bool isLoading = false;

	public bool showLoadingIcon = true;

	public string path;

	private string curPath;

	private GameObject loadingIcon;

	public bool IsLoading { get{ return isLoading; } }

	virtual protected void Start()
	{
		isStarted = true;

		if(string.IsNullOrEmpty(path)) return;

		Load(path);
	}

	[ContextMenu("LoadAsset")]
	public void Load()
	{
		Load(path);
	}
	public void Load(string path)
	{
		this.path = path;

		if(!isStarted) return;

		if(asset != null)
		{
			if(curPath == path)
			{
				if(!isLoading) OnLoadEnd(asset.Content);
				return;
			}
			else
			{
				asset.DecreaseReferenceCount();
				asset = null;
			}
		}

		if(string.IsNullOrEmpty(path)) return;

		asset = BeginLoad(path);

		curPath = path;

		BeginLoad();
	}

	public void Unload()
	{
		if(asset != null) asset.DecreaseReferenceCount();
		asset = null;
		EndLoad(default(T));
		isLoading = false;
	}

	private void BeginLoad()
	{
		isLoading = true;

		Coroutine.Start(WaitForLoadComplete(asset));
	}

	private void OnLoadEnd(T content)
	{
		if(isLoading) EndLoad(content);
	}

	private IEnumerator WaitForLoadComplete(IAsset<T> asset)
	{
		int retryCount = 0;

		var origin = GetComponent<UIWidget>();

		yield return new WaitForEndOfFrame();

		while(true)
		{
			if(asset != this.asset) yield break;

			if(asset.IsComplete) break;

			if(++retryCount == 5)
			{
				OnLoadEnd(default(T));
				if(showLoadingIcon && loadingIcon == null)
				{
					loadingIcon = CreateLoadingIcon(origin);

					UIManager.Instance.SetIdentity(gameObject, loadingIcon, false);

					if(gameObject.GetComponent<UIRect>() == null)
					{
						var parent = NGUITools.FindInParents<UIPanel>(loadingIcon);
						if(parent != null) 
						{
							loadingIcon.AddComponent<UIPanel>().depth = parent.depth;
						}
					}
				}

				if(origin != null) origin.enabled = false;
			}
			//yield return new WaitForEndOfFrame();
			yield return new WaitForSeconds(0.1f);
		}

		if(loadingIcon != null) 
		{
			if(origin != null) origin.enabled = true;
			Destroy(loadingIcon);
			loadingIcon = null;
		}
		else
		{
			if(origin != null) origin.enabled = true;
		}

		OnLoadEnd(asset.Content);
		isLoading = false;
	}

	abstract protected IAsset<T> BeginLoad(string path);

	virtual protected void EndLoad(T content){ }

	virtual protected void OnDestroy()
	{
		if(asset != null) asset.DecreaseReferenceCount();
		asset = null;
		isLoading = false;
	}

	private GameObject CreateLoadingIcon(UIWidget origin)
	{
		var icon = new GameObject();
		icon.name = "loading_icon";
		var sprite = icon.AddComponent<UISprite>();
		sprite.atlas = Resources.Load<UIAtlas>("Atlases/UI_COMMON/UI_COMMON");
		sprite.spriteName = "loading";
		sprite.MakePixelPerfect();

		if(origin != null)
		{
			sprite.depth = origin.depth;
			var size = Mathf.Min(origin.width, origin.height);
			if(size < sprite.width || size < sprite.height)
			{
				sprite.width = size;
				sprite.height = size;
			}
		}
		
		
		var tweener = TweenClock.Begin(icon, 1f, 360);
		tweener.style = UITweener.Style.Loop;

		return icon;
	}
}
