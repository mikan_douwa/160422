﻿using UnityEngine;
using System.Collections;

public class TextureLoader : LoaderBase<Texture2D> {

	public UITexture texture;

	public bool makePixelPerfect = true;

	protected override IAsset<Texture2D> BeginLoad (string path)
	{
		return ResourceManager.Instance.LoadTexture(path);
	}

	protected override void EndLoad (Texture2D content)
	{
		if(texture == null) texture = gameObject.AddMissingComponent<UITexture>();
		
		texture.mainTexture = content;

		if(makePixelPerfect) 
		{
			var size = PatchManager.Instance.GetOriginTextureSize(path);
			if(size != Vector2.zero)
			{
				texture.width = (int)size.x;
				texture.height = (int)size.y;
			}
			else
				texture.MakePixelPerfect();
		}
	}

}
