﻿using UnityEngine;
using System.Collections;

public class PrefabLoader : LoaderBase<GameObject> {

	public GameObject prefabInst;

	protected override IAsset<GameObject> BeginLoad (string path)
	{
		if(prefabInst != null) 
		{
			NGUITools.Destroy(prefabInst);
			prefabInst = null;
		}

		return ResourceManager.Instance.LoadPrefab(path);
	}
	
	protected override void EndLoad (GameObject content)
	{
		if(content == null) return;
		prefabInst = UIManager.Instance.AddChild(gameObject, content);
	}
}
