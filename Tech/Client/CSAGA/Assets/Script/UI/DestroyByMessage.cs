﻿using UnityEngine;
using System.Collections;

public class DestroyByMessage : MonoBehaviour {

	public MessageID[] msgList;

	// Use this for initialization
	void Start () {
		foreach(var msg in msgList)
			Message.AddListener(msg, OnMessage);
	}

	void OnDestroy()
	{
		foreach(var msg in msgList)
			Message.RemoveListener(msg, OnMessage);
	}

	void OnMessage()
	{
		Destroy(gameObject);
	}
}
