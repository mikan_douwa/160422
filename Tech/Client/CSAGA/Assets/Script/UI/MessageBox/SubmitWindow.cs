﻿using UnityEngine;
using System.Collections;

abstract public class SubmitWindow : MessageBox {

	protected override void OnClickButton (int index)
	{
		if(index == 0) 
		{
			if(!OnSubmit()) return;
		}

		StartCoroutine(WaitForSubmit(index));
	}
	
	abstract protected bool OnSubmit();

	IEnumerator WaitForSubmit (int index)
	{
		while(IsSubmiting) yield return 0;

		if(!IsSucc) yield return new WaitForEndOfFrame();

		if(IsSucc) base.OnClickButton (index);
	}

	virtual protected bool IsSubmiting { get { return GameSystem.Service.IsBusy; } }
	virtual protected bool IsSucc { get { return true; } }
}
