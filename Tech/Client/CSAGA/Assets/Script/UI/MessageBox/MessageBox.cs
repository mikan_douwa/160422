﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class MessageBox : MonoBehaviour {

	private const string DEAULT_PREFAB = "Template/MessageBox";

	protected class ButtonContext
	{
		public UIButton Button;
		public int Index;

		private Color defaultColor;
		private bool grayScale = false;
		public bool GrayScale
		{
			get	{ return grayScale; }
			set
			{
				if(value == grayScale) return;

				if(!grayScale) defaultColor = Button.defaultColor;
				grayScale = value;

				Button.defaultColor = value ? Color.gray : defaultColor;
				Button.UpdateColor(Button.isEnabled);//, true); //Modified by Severus, 2015.6.14
			}
		}
		
		public bool IsEnabled
		{
			get { return  Button.isEnabled; }
			
			set
			{
				Button.isEnabled = value;
				GrayScale = !value;
			}
		}
	}

	private static List<MessageBoxContext> instances = new List<MessageBoxContext>();

	//public static MessageBox current { get { return instances.Count > 0 ? instances[instances.Count - 1].messageBox : null; } }

	public float buttonGap = 90f;

	public float buttonWidth = 0f;

	private UIWidget container;

	public UILabel contentLabel;
	public UILabel titleLabel;
	public GameObject buttons;
	public GameObject buttonPrefab;

	public bool smooth = true;

	public MessageBoxContext context;

	private List<ButtonContext> btnList;

	private static string[] ToStrings(SystemTextID[] ids)
	{
		string[] strings = new string[ids.Length];
		for(int i = 0; i < ids.Length; ++i)
			strings[i] = ConstData.GetSystemText(ids[i]);
		return strings;
	}

	public static MessageBoxContext Show(SystemTextID content, params object[] args)
	{
		return Show (ConstData.GetSystemText(content, args));
	}

	public static MessageBoxContext Show(string content)
	{
		return Show (content, SystemTextID.BTN_CONFIRM);
	}

	public static MessageBoxContext Show(string content, params SystemTextID[] buttons)
	{
		return Show (null, content, buttons);
	}
	
	public static MessageBoxContext Show(string content, params string[] buttons)
	{
		return Show (null, content, buttons);
	}

	public static MessageBoxContext Show(string prefabPath, string content)
	{
		return Show (prefabPath, content, SystemTextID.BTN_CONFIRM);
	}

	public static MessageBoxContext Show(string prefabPath, string content, params SystemTextID[] buttons)
	{
		return Show (prefabPath, content, ToStrings(buttons));
	}

	// can't use 'params', or be confused
	public static MessageBoxContext Show(string prefabPath, string content, string[] buttons)
	{
		var context = new MessageBoxContext();
		context.prefabPath = prefabPath;
		context.content = content;
		context.buttons = buttons;
		Show (context);

		return context;
	}

	public static void Show(MessageBoxContext context)
	{
		var prefab = string.IsNullOrEmpty(context.prefabPath) ? DEAULT_PREFAB : context.prefabPath;

		ResourceManager.Instance.CreatePrefabInstance (prefab, o=>
		                                               {
			context.messageBox = o.GetComponent<MessageBox> ();
			context.messageBox.context = context;
			
			if(context.onMessageBoxCreated!=null)
			{
				context.onMessageBoxCreated(context.messageBox);
			}
		});
	}

	#region extends
	
	static public MessageBoxContext ShowPrompt(string prefabPath, string content)
	{
		return MessageBox.Show (prefabPath, content, new string[]{});
	}
	static public MessageBoxContext ShowConfirmWindow(string content, Action<int> callback)
	{
		return ShowConfirmWindow("Template/MessageBox", content, callback);
	}

	static public MessageBoxContext ShowConfirmWindow(string prefabPath, string content, Action<int> callback)
	{
		var msgBox = Show(prefabPath, content, SystemTextID.BTN_CONFIRM, SystemTextID.BTN_CANCEL);
		msgBox.callback = callback;
		msgBox.colors = new ButtonColor[]{ ButtonColor.Green, ButtonColor.Red };
		return msgBox;
	}

	#endregion

	virtual protected void OnClickButton(int index)
	{
		if(context.callback != null) context.callback(index);
		
		Destroy(gameObject);
	}

	void Awake()
	{
		instances.Add(context);
	}
	
	virtual protected void OnDestroy()
	{
		instances.Remove(context);
	}

	void Start()
	{
		transform.localPosition = new Vector3(0, -1600);

		container = GetComponent<UIWidget>();

		if(context.height > 0) container.height = context.height;
		if(context.width > 0) container.width = context.width;

		if(context.buttonGap > 0) buttonGap = context.buttonGap;
		if(context.buttonWidth > 0) buttonWidth = context.buttonWidth;

		if(contentLabel != null) 
		{
			contentLabel.pivot = context.pivot;
			contentLabel.text = string.IsNullOrEmpty(context.content) ? "" : context.content;
		}
		if(titleLabel != null) titleLabel.text = string.IsNullOrEmpty(context.title) ? "" : context.title;

		CreateButton();
	}

	void OnClick()
	{
		OnCloseByClick();
		Destroy(gameObject);
	}

	virtual protected void OnReady() {}

	virtual protected void OnCloseByClick() {} 

	public bool HasButton { get { return btnList != null && btnList.Count > 0; } }
	
	protected ButtonContext GetButton(int index)
	{
		if(index >= btnList.Count) return null;
		return btnList[index];
	}

	protected void SetButtonEnabled(int index, bool enabled)
	{
		var item = GetButton(index);
		if(item != null) item.IsEnabled = enabled;
	}

	protected void SetButtonEnabled(bool enabled)
	{
		foreach(var item in btnList) item.IsEnabled = enabled;
	}

	private void CreateButton()
	{
		if(buttonPrefab != null || context.buttons == null)
			CreateButton(buttonPrefab);
		else
			CreateButton(Essentials.Instance.button);
	}

	private void CreateButton(GameObject prefab)
	{
		if(context.buttons != null && buttons != null)
		{
			btnList = new List<ButtonContext>(context.buttons.Length);

			for(int i = 0; i < context.buttons.Length; ++i)
			{
				var button = new ButtonContext();
				button.Index = i;
				
				var go = UIManager.Instance.AddChild(buttons, prefab);
				go.AddMissingComponent<SoundEffect>().sound = (i == 0 ? SoundID.CONFIRM : SoundID.CANCEL);

				var widget = go.GetComponent<UIWidget>();
				widget.depth = 1;
				if(buttonWidth > 0) widget.width = (int)buttonWidth;

				button.Button = go.GetComponent<UIButton>();
				EventDelegate.Add(button.Button.onClick, onClickButton);
				
				var label = go.GetComponentInChildren<UILabel>();
				if(label != null) label.text = context.buttons[i];

				if(context.colors != null)
				{
					var sprite = go.GetComponentInChildren<UISprite>();
					switch(context.colors[i])
					{
					case ButtonColor.Green:
						sprite.spriteName = "button_04";
						break;
					case ButtonColor.Red:
						sprite.spriteName = "button_05";
						break;
					}
				}
				
				btnList.Add(button);
				
				if(context.buttons.Length>1)
				{
					go.transform.localPosition=new Vector3(buttonGap*(2*i-context.buttons.Length+1),0f,0f);
				}
			}

			if(btnList != null && btnList.Count > 0 && container != null)
			{
				if(container.width < btnList.Count * buttonGap * 2 + 60)
					container.width = (int)(btnList.Count * buttonGap * 2 + 60);
			}

			if(context.enableCloseByClick) UIUtils.AddFullScreenBoxCllider(gameObject);

		}
		else
			UIUtils.AddFullScreenBoxCllider(gameObject);
		
		OnReady();
		StartCoroutine(WaitForReadyRoutine());
	}
	
	private IEnumerator WaitForReadyRoutine()
	{
		while(UIBase.IsChanging) yield return new WaitForEndOfFrame();
		
		transform.localPosition = Vector3.zero;
		
		if(smooth)
		{
			transform.localScale = new Vector3(0.05f, 0.05f);
			var tween = TweenScale.Begin(gameObject, 0.1f, Vector3.one);
			tween.method = UITweener.Method.EaseOut;
		}
	}

	private void onClickButton()
	{
		foreach(var button in btnList)
		{
			if(UIButton.current != button.Button) continue;
			OnClickButton(button.Index);
			break;
		}
	}
}

public enum ButtonColor
{
	None,Green,Red
}

public class MessageBoxContext
{
	public bool enableCloseByClick;

	public MessageBox messageBox;
	public string prefabPath;
	
	public UIWidget.Pivot pivot = UIWidget.Pivot.Center;
	public int height;
	public int width;

	public string title;
	public string content;
	public string[] buttons;
	public ButtonColor[] colors;
	public int buttonGap = 0;
	public int buttonWidth = 0;

	public Action<MessageBox> onMessageBoxCreated;
	public Action<int> callback;
	public object[] args;
	public static MessageBoxContext Create(string prefab, params object[] args)
	{
		return new MessageBoxContext{ prefabPath = prefab, args = args };
	}
}