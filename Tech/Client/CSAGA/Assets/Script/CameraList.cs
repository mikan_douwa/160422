﻿using UnityEngine;
using System.Collections.Generic;

public class CameraList : MonoBehaviour {

	private static CameraList instance;

	public static CameraList Instance
	{
		get
		{
			if(instance == null)
			{
				var go = new GameObject();
				go.name = "[AUTO] CameraList";
				instance = go.AddComponent<CameraList>();
				DontDestroyOnLoad(go);
			}

			return instance;
		}
	}


	private Dictionary<int, Camera> list = new Dictionary<int, Camera>();
	public void Add(int layer)
	{
		if(list.ContainsKey(layer)) return;

		var camera = UIUtils.FindCamera(layer);

		if(camera == null) camera = UIUtils.CreateCamera(layer, gameObject);

		list.Add(layer, camera);
	}

	public Camera Get(int layer)
	{
		var camera = default(Camera);

		if(list.TryGetValue(layer, out camera)) return camera;

		return null;
	}

	void OnLevelWasLoaded(int level)
	{
		if(level != 0) return;
		Destroy(instance.gameObject);
		instance = null;
	}
}
