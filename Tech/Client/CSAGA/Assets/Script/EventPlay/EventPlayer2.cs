﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Mikan.CSAGA;

public class EventPlayer2 : UIBase {

	public TextureLoader background;

	public UITable table;
	public UIPanel panel;
	
	public GameObject container;

	private EventChara2[] charas = new EventChara2[]{ null, null }; 
	
	private int interactiveId;
	
	private Queue<Mikan.CSAGA.ConstData.EventShow> events;
	
	private int pendingCount = -1;
	
	private string nexBG;
	
	private bool isLoading = false;

	private Queue<EventLine> lines;

	private Mikan.CSAGA.ConstData.EventShow current;

	private EventDialog dialog;

	private string playerName;

	private IAsset<GameObject> charaAsset;

	private bool isFirstLine = true;

	private float offset = 0f;

	public bool IsReady { get{ return pendingCount == 0; } }
	
	public bool IsComplete { get; private set; }

	private EventTransition transition;

	public void OnClick()
	{
		if(isLoading) return;
		
		Next ();
	}

	protected override void OnDestroy ()
	{
		base.OnDestroy ();

		Message.RemoveListener(MessageID.DownloadEnd, OnDownloadEnd);

		if(charaAsset != null)
		{
			charaAsset.DecreaseReferenceCount();
			charaAsset = null;
		}

		if(Application.loadedLevelName == SceneName.GAME)
			Message.Send(MessageID.PlayMusic, "bgm_normal");
		Message.Send(MessageID.EndEvent, interactiveId);

		if(transition != null)
		{
			EventDelegate.Set(transition.tweener.onFinished, ()=> Destroy(transition.gameObject));
			
			transition.tweener.duration = 0.5f;
			transition.tweener.PlayReverse();
		}
	}
	
	public void Skip()
	{
		if(isLoading) return;
		EndEvent();
	}

	private void EndEvent()
	{
		if(IsComplete) return;

		IsComplete = true;

		if(StageManager.Instance.Current.ID != StageID.QuestStage) 
		{
			if(!Context.Instance.IsTutorialComplete)
			{
				if(interactiveId == ConstData.Tables.TutorialEventID1)
				{
					ShopManager.Instance.DrawGacha(ConstData.Tables.FirstGachaID);
				}
				else if(interactiveId == ConstData.Tables.TutorialEventID2)
				{
					BeginTransition(()=>
					{
						Context.Instance.IsTutorialComplete = true;
						Message.Send(MessageID.SwitchStage, StageID.Home);
					});
				}
			}
			else
			{
				BeginTransition(()=>
				{
					Destroy(gameObject);
				});

			}
		}
	}

	private void BeginTransition(EventDelegate.Callback callback)
	{
		if(transition != null)
		{
			transition.gameObject.SetActive(true);
			EventDelegate.Set(transition.tweener.onFinished, callback);
			
			transition.tweener.duration = 0.3f;
			transition.tweener.PlayForward();
		}
		else
			callback();
	}

	protected override void OnStart ()
	{
		transition = FindObjectOfType<EventTransition>();

		if(transition != null)
		{
			EventDelegate.Set(transition.tweener.onFinished, ()=>
			{
				transition.gameObject.SetActive(false);
			});

			transition.tweener.duration = 0.5f;
			transition.tweener.PlayReverse();

		}

		offset = -panel.gameObject.transform.position.y - 0.5f + ((panel.gameObject.transform.position.y / 0.5f) * 0.06f);

		playerName = GameSystem.IsAvailable ? GameSystem.Service.PlayerInfo.Name : "{Player}";

		charaAsset = ResourceManager.Instance.LoadPrefab("EventPlay/EventChara2", o=>{
			charaAsset = o;
			if(Context.Instance.InteractiveID != 0) 
			{
				isLoading = true;
				Message.AddListener(MessageID.DownloadEnd, OnDownloadEnd);
				ApplyChange();
			}
		});
	}

	protected override void OnChange ()
	{
		interactiveId = Context.Instance.InteractiveID;
		var data = ConstData.Tables.Interactive[interactiveId];
		
		if(data == null) 
		{
			Debug.LogWarning(string.Format("interactive not exists, id={0}", interactiveId));
			return;
		}
		
		events = new Queue<Mikan.CSAGA.ConstData.EventShow>();
		
		foreach(var item in ConstData.Tables.EventShow.Select((o)=>{ return o.n_GROUP == data.n_CALL_EVENT; })) events.Enqueue(item);
		
		PatchManager.Instance.DownloadEventPatch(data);
	}
	
	private void OnDownloadEnd()
	{
		isLoading = false;
		pendingCount = 0;
		Message.RemoveListener(MessageID.DownloadEnd, OnDownloadEnd);

		Next ();
	}
	
	private bool Next()
	{
		if(IsComplete) return false;

		if(pendingCount > 0) return true;

		if(dialog != null && dialog.Next()) return true;

		if(lines != null && lines.Count > 0)
		{
			var line = lines.Dequeue();

			var postfix = "0";

			if(line.Origin.n_TALK == 1 || line.Origin.n_TALK == 2)
				postfix = "1";
			else if(line.Origin.n_TALK == 3)
				postfix = "2";

			++pendingCount;

			ResourceManager.Instance.CreatePrefabInstance(table.gameObject, "EventPlay/EventDialog" + postfix, o=>{
				dialog = o.GetComponent<EventDialog>();
				dialog.screen = container;
				dialog.table = table;
				dialog.panel = panel;
				dialog.line = line;
				dialog.offset = offset;

				if(isFirstLine)
				{
					dialog.delayShow = 0.3f;
					isFirstLine = false;
				}

				dialog.ApplyChange();
				--pendingCount;
			});

			return true;
		}

		if(events.Count == 0)
		{
			EndEvent();
			return false;
		}

		current = events.Dequeue();
		
		background.gameObject.AddMissingComponent<SwitchTexture>().Switch("BG/" + current.s_BG);
		
		Message.Send(MessageID.PlayMusic, current.s_BGM);

		SwitchChara(0);
		SwitchChara(1);

		lines = Split(current);

		return Next ();
	}

	private void SwitchChara(int index)
	{
		var charaId = current.n_CHARA[index];

		var chara = charas[index];

		if(chara == null || chara.CharaID != charaId) 
		{
			if(chara != null) chara.Hide();

			charas[index] = null;

			if(charaId == 0) return;

			var obj = UIManager.Instance.AddChild(container, charaAsset.Content);

			chara = obj.GetComponent<EventChara2>();
			charas[index] = chara;
			chara.CharaID = charaId;
			chara.ApplyChange();
		}

		chara.transform.localPosition = new Vector3(current.n_POSITIONX[index], -current.n_POSITIONY[index]);
		var scale = current.n_SIZE[index] / 100f;
		chara.transform.localScale = new Vector3(scale, scale);

		chara.texture.flip = current.n_FLIP[index] != 0 ? UIBasicSprite.Flip.Horizontally : UIBasicSprite.Flip.Nothing;

		if(current.n_TALK == index + 1)
		{
			chara.texture.depth = 3;
			chara.texture.color = Color.white;
		}
		else
		{
			chara.texture.depth = 2;
			chara.texture.color = new Color(0.4f, 0.4f, 0.4f);
		}
	}
	
	private Queue<EventLine> Split(Mikan.CSAGA.ConstData.EventShow data)
	{
		var text = ConstData.GetEventText(data.n_ID);

		var split = text.Split(new string[]{"\\s"}, System.StringSplitOptions.RemoveEmptyEntries);
		var list = new Queue<EventLine>(split.Length);

		foreach(var item in split) 
		{
			if(!string.IsNullOrEmpty(item)) 
			{
				var obj = new EventLine();
				obj.Origin = data;
				obj.Line = list.Count;

				var begin = item.IndexOf(NameSectionBegin);
				if(begin == 0)
				{
					var end = item.IndexOf(NameSectionEnd, NameSectionBegin.Length);
					if(end > -1)
					{
						obj.Name = Format(item.Substring(NameSectionBegin.Length, end - NameSectionBegin.Length));
						obj.Text = Format(item.Substring(end + NameSectionEnd.Length));
					}
					else
					{
						obj.Name = "";
						obj.Text = Format(item);
					}
				}
				else
				{
					obj.Name = "";
					obj.Text = Format(item);
				}
				list.Enqueue(obj);
			}
		}
		return list;
	}

	private string Format(string text)
	{
		return Mikan.CSAGA.ConstData.Tables.Format(text, playerName);
	}
	
	private const string NameSectionBegin = "[namecolor]";
	private const string NameSectionEnd = "[-]";

}

public class EventLine
{
	public Mikan.CSAGA.ConstData.EventShow Origin;
	public string Name;
	public string Text;
	public int Line;
}
