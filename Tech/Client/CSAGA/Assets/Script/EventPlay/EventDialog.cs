﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EventDialog : UIBase {

	public GameObject screen;
	public GameObject anchor;
	public GameObject banner;
	public UITable table;
	public UIPanel panel;

	public GameObject container;
	public UISprite background;
	public UILabel charaName;
	public UILabel text;
	
	public EventLine line;

	public float delayShow = 0f;

	public float offset;

	private int panding = 0;

	private UIWidget parent;

	private Queue<string> lines;

	private float diffs = 0f;

	protected override void OnStart ()
	{
		base.OnStart ();

		parent = GetComponent<UIWidget>();

		parent.alpha = 0;

		TweenAlpha.Begin(gameObject, 0.2f, 1f).delay = delayShow;

		panding = parent.height - text.height + 40;
	}

	public bool Next()
	{
		if(lines == null) return true;

		if(lines.Count == 0) 
		{
			if(diffs != 0)
			{
				table.Reposition();
				
				panel.transform.localPosition = new Vector3(panel.transform.localPosition.x, panel.transform.localPosition.y - diffs);
			}
			return false;
		}

		text.text += lines.Dequeue();

		var originHeight = parent.height;

		parent.height = text.height + panding;

		var diff = parent.height - originHeight;

		if(diff != 0)
		{
			foreach(var item in table.GetChildList())
			{
				if(item != this.transform) item.localPosition = new Vector3(item.localPosition.x, item.localPosition.y - diff);
			}

			/*
			table.Reposition();

			panel.transform.localPosition = new Vector3(panel.transform.localPosition.x, panel.transform.localPosition.y - diff);
			*/

			diffs += diff;
		}

		return true;
	}
	protected override void OnChange ()
	{
		if(anchor != null)
		{
			if(line.Origin.n_TALK == 1 || line.Origin.n_TALK == 2)
			{
				var posX = line.Origin.n_POSITIONX[line.Origin.n_TALK - 1];
				anchor.transform.localPosition = new Vector3(posX, anchor.transform.localPosition.y);
			}
			else
				anchor.SetActive(false);
		}

		//if(line.Origin.n_TALK == 1) background.flip = UIBasicSprite.Flip.Both;

		charaName.text = line.Name;
		if(banner != null) banner.SetActive(!string.IsNullOrEmpty(line.Name));
		//text.text = line.Text;

		var split = line.Text.Split(new string[]{"\\p"}, System.StringSplitOptions.RemoveEmptyEntries);

		lines = new Queue<string>(split);

		text.text = lines.Dequeue();

		parent.height = text.height + panding;

		Coroutine.DelayInvoke(()=>{
			if(IsDestroyed) return;
			table.Reposition();
			gameObject.AddComponent<UIFocusOnChild>().panelCenter = new Vector3(0f, offset);

			if(line.Line == 0 && line.Origin.f_QUAKE > 0) TweenShake.Begin(screen, line.Origin.f_QUAKE, 20, 2);
		});

	}
}
