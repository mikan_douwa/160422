﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EventDialog3 : UIBase {
	
	public GameObject screen;
	public GameObject anchor;
	public GameObject banner;
	public UITable table;
	public UIPanel panel;

	public UILabel charaName;
	public UILabel text;
	
	public EventLine line;
	
	public float delayShow = 0f;
	
	public float offset;
	
	public int panding = 0;
	
	private UIWidget parent;
	
	private Queue<string> lines;
	
	private float diffs = 0f;

	protected override void OnStart ()
	{
		base.OnStart ();
		
		parent = GetComponent<UIWidget>();
		
		parent.alpha = 0;
		
		TweenAlpha.Begin(gameObject, 0.2f, 1f).delay = delayShow;
	}
	
	public bool Next()
	{
		if(lines == null) return true;
		
		if(lines.Count == 0) return false;
		
		text.text += lines.Dequeue();
		
		parent.height = text.height + panding;
		
		table.repositionNow = true;

		panel.GetComponent<UIScrollView>().MoveRelative(new Vector3(0, -32));

		StartCoroutine(DelayReposition());
		
		return true;
	}
	protected override void OnChange ()
	{
		if(anchor != null)
		{
			if(line.Origin.n_TALK == 1 || line.Origin.n_TALK == 2)
			{
				var posX = line.Origin.n_POSITIONX[line.Origin.n_TALK - 1];
				anchor.transform.localPosition = new Vector3(posX, anchor.transform.localPosition.y);
			}
			else
				anchor.SetActive(false);
		}
		
		//if(line.Origin.n_TALK == 1) background.flip = UIBasicSprite.Flip.Both;
		
		charaName.text = line.Name;
		if(banner != null) banner.SetActive(!string.IsNullOrEmpty(line.Name));
		//text.text = line.Text;
		
		var split = line.Text.Split(new string[]{"\\p"}, System.StringSplitOptions.RemoveEmptyEntries);
		
		lines = new Queue<string>(split);
		
		text.text = lines.Dequeue();
		
		parent.height = text.height + panding;

		table.repositionNow = true;

		StartCoroutine(DelayReposition());
	}

	private IEnumerator DelayReposition()
	{
		yield return new WaitForFixedUpdate();
		yield return new WaitForSeconds(0.05f);

		var tweener = TweenFloatValue.Begin(panel.gameObject, 0.2f, panel.GetComponent<UIScrollView>().verticalScrollBar.value, 0f);

		tweener.OnValueChanged = OnValueChange;

		if(line.Line == 0 && line.Origin.f_QUAKE > 0) TweenShake.Begin(screen, line.Origin.f_QUAKE, 20, 2);
	}

	private void OnValueChange(float val)
	{
		panel.GetComponent<UIScrollView>().verticalScrollBar.value = val;
	}
}
