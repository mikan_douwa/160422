﻿using UnityEngine;
using System.Collections;

public class EventChara2 : UIBase {

	public TextureLoader loader;

	public UITexture texture;

	public int CharaID;

	public void Hide()
	{
		if(!IsDestroyed)
		{
			TweenAlpha.Begin(texture.gameObject, 0.1f, 0f);

			Coroutine.DelayInvoke(()=>{ if(!IsDestroyed) Destroy(gameObject); }, 0.2f);
		}
	}

	protected override void OnStart ()
	{
		texture.alpha = 0;
	}

	protected override void OnChange ()
	{
		StartCoroutine(LoadRoutine());
	}

	private IEnumerator LoadRoutine()
	{
		loader.Load(ConstData.GetCardPicPath(CharaID));

		while(true)
		{
			yield return new WaitForSeconds(0.05f);
			if(!loader.IsLoading) break;
		}

		TweenAlpha.Begin(texture.gameObject, 0.2f, 1f);
	}
}
