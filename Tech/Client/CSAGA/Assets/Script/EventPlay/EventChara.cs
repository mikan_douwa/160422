﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EventChara : UIBase {

	public UIPanel panel;
	public TextureLoader pic;
	public GameObject dialog;
	public GameObject background;
	public InteractiveLabel label;
	public UILabel charaName;

	private int index;

	private Mikan.CSAGA.ConstData.EventShow ev;

	private int line = 0;

	private int baseDepth;

	private int lastTalk = 0;

	private int lastCharaId = 0;

	private bool isEmpty = true;

	private List<Line> lines;

	private SwitchTexture switchObj;

	protected override void OnAwake ()
	{
		base.OnAwake ();
		baseDepth = panel.depth;
		charaName.text = "";
		dialog.SetActive(false);
		SetVisible(false, true);

		if(pic != null) 
		{
			switchObj = pic.gameObject.AddMissingComponent<SwitchTexture>();
			switchObj.duration = 0.2f;
		}
	}

	public void Play(int index, Mikan.CSAGA.ConstData.EventShow ev)
	{
		this.index = index;
		this.ev = ev;

		var text = ConstData.GetEventText(ev.n_ID);
		lines = Split(text);
		line = 0;


		ApplyChange();
	}

	public bool Next()
	{
		if(lines == null) return false;

		if(line + 1 >= lines.Count) return false;

		++line;

		ApplyChange();

		return true;
	}

	protected override void OnChange ()
	{
		var cachedLastCharaId = lastCharaId;
		var isCharaChanged = false;

		if(index == 1 || index == 2)
		{
			var id = ev.n_CHARA[index - 1];
			
			if(id == 0)
			{
				SetVisible(false);
				//pic.Unload();
				switchObj.Switch(null);
			}
			else
			{
				SetVisible(true);

				switchObj.Switch(ConstData.GetCardPicPath(id));
				//pic.Load(ConstData.GetCardPicPath(id));
				pic.transform.localPosition = new Vector3(-UIDefine.ScreenWidth/2 + ev.n_POSITIONX[index-1], UIDefine.ScreenHeight/2 - ev.n_POSITIONY[index-1]);
			}

			if(lastCharaId != id)
			{
				lastCharaId = id;
				isCharaChanged = true;
			}
		}
		else if(ev.n_TALK != 2)
			SetVisible(ev.n_TALK == index);

		if(index == 2)
			dialog.transform.localPosition = new Vector3(0, 395);
		else
			dialog.transform.localPosition = new Vector3(0, -320);


		if(ev.n_TALK == index)
		{
			dialog.SetActive(true);

			StartCoroutine(SetPicActive(true, baseDepth + 3, cachedLastCharaId, isCharaChanged));
			/*
			panel.depth = baseDepth + 3;

			if(pic != null) 
			{
				var texture = pic.GetComponent<UITexture>();
				texture.color = Color.white;
				texture.flip = ev.n_FLIP[index - 1] != 0 ? UIBasicSprite.Flip.Horizontally : UIBasicSprite.Flip.Nothing;
			}
*/

			charaName.text = GetCharaName();
			label.EventText = GetEventText();
			label.ApplyChange();
			isEmpty = false;
		}
		else
		{
			var depth = baseDepth;

			if(index != 1 && index != 2)
			{
				depth = baseDepth + 2;
			}
			else
			{
				if(index == lastTalk)
					depth = baseDepth + 1;
				else
					depth = baseDepth;
			}


			StartCoroutine(SetPicActive(false, depth, cachedLastCharaId, isCharaChanged));
			/*
			panel.depth = depth;
			if(pic != null) 
			{
				var texture = pic.GetComponent<UITexture>();
				texture.color = new Color(0.4f, 0.4f, 0.4f);
				texture.flip = ev.n_FLIP[index - 1] != 0 ? UIBasicSprite.Flip.Horizontally : UIBasicSprite.Flip.Nothing;
			}
*/

			if(isCharaChanged || isEmpty || ev.n_TALK != 2 && index != 2) 
			{
				dialog.SetActive(false);
				ClearText();
			}
		}

		if((ev.n_TALK == 1 || ev.n_TALK == 2) & ev.n_TALK != lastTalk) lastTalk = ev.n_TALK;
	}

	private IEnumerator SetPicActive(bool active, int depth, int lastCharaId, bool isCharaChanged)
	{
		if(pic == null) 
		{
			panel.depth = depth;
			yield break;
		}

		if(active && isCharaChanged && lastCharaId != 0)
		{
			yield return new WaitForSeconds(0.2f);
		}

		panel.depth = depth;

		var texture = pic.GetComponent<UITexture>();
		texture.color = active ? Color.white : new Color(0.4f, 0.4f, 0.4f);
		texture.flip = ev.n_FLIP[index - 1] != 0 ? UIBasicSprite.Flip.Horizontally : UIBasicSprite.Flip.Nothing;
	}



	private void SetVisible(bool visible, bool imediately = false)
	{
		var alpha = visible ? 1f : 0f;

		var duration = (imediately || alpha == panel.alpha) ? 0f : 0.1f;

		TweenAlpha.Begin(panel.gameObject, duration, alpha);

		//panel.alpha = visible ? 1 : 0;

		if(!visible) ClearText();

	}

	private void ClearText()
	{
		charaName.text = "";
		label.EventText = "";
		label.ApplyChange();

		isEmpty = true;
	}

	private string GetEventText()
	{
		if(lines == null || line >= lines.Count) return "";
		return Format(lines[line].Text);
	}

	private string GetCharaName()
	{
		if(lines == null || line >= lines.Count) return "";
		return Format(lines[line].Name);
	}

	private string Format(string text)
	{
		return Mikan.CSAGA.ConstData.Tables.Format(text, GameSystem.Service.PlayerInfo.Name);
	}

	private List<Line> Split(string text)
	{
		var split = text.Split('\\', 's');
		var list = new List<Line>(split.Length);
		foreach(var item in split) 
		{
			if(!string.IsNullOrEmpty(item)) 
			{
				var obj = new Line();
				var begin = item.IndexOf(NameSectionBegin);
				if(begin == 0)
				{
					var end = item.IndexOf(NameSectionEnd, NameSectionBegin.Length);
					if(end > -1)
					{
						obj.Name = item.Substring(NameSectionBegin.Length, end - NameSectionBegin.Length);
						obj.Text = item.Substring(end + NameSectionEnd.Length);
					}
					else
					{
						obj.Name = "";
						obj.Text = item;
					}
				}
				else
				{
					obj.Name = "";
					obj.Text = item;
				}
				list.Add(obj);
			}
		}
		return list;
	}

	private const string NameSectionBegin = "[namecolor]";
	private const string NameSectionEnd = "[-]";
	class Line
	{

		public string Name;
		public string Text;
	}
}
