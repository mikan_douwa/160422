﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Mikan.CSAGA;

public class EventPlayer : UIBase {
	
	public TextureLoader background;

	public GameObject container;

	private List<EventChara> charaList;

	private int interactiveId;

	private Queue<Mikan.CSAGA.ConstData.EventShow> events;

	private int pendingCount = -1;

	private string nexBG;

	private bool isLoading = false;

	public bool IsReady { get{ return pendingCount == 0; } }

	public bool IsComplete { get; private set; }

	void OnClick()
	{
		if(isLoading) return;

		Next ();
	}

	protected override void OnDestroy ()
	{
		base.OnDestroy ();
		if(Application.loadedLevelName == SceneName.GAME)
			Message.Send(MessageID.PlayMusic, "bgm_normal");
		Message.Send(MessageID.EndEvent, interactiveId);
	}

	public void Save()
	{
		if(isLoading) return;

		MessageBox.Show(SystemTextID.TIP_NOT_AVAILABLE);
	}

	public void Skip()
	{
		if(isLoading) return;

		while(Next ()){}
	}

	protected override void OnStart ()
	{
		if(Context.Instance.InteractiveID != 0) 
		{
			isLoading = true;
			Message.AddListener(MessageID.DownloadEnd, OnDownloadEnd);
			ApplyChange();
		}
	}

	protected override void OnChange ()
	{
		interactiveId = Context.Instance.InteractiveID;
		var data = ConstData.Tables.Interactive[interactiveId];

		if(data == null) 
		{
			Debug.LogWarning(string.Format("interactive not exists, id={0}", interactiveId));
			return;
		}

		events = new Queue<Mikan.CSAGA.ConstData.EventShow>();

		foreach(var item in ConstData.Tables.EventShow.Select((o)=>{ return o.n_GROUP == data.n_CALL_EVENT; })) events.Enqueue(item);

		PatchManager.Instance.DownloadEventPatch(data);
	}

	private void OnDownloadEnd()
	{
		isLoading = false;
		Message.RemoveListener(MessageID.DownloadEnd, OnDownloadEnd);

		if(charaList == null)
			CreateChara();
		else if(events.Count > 0)
			Next ();
	}

	private bool Next()
	{
		if(IsComplete) return false;

		if(pendingCount > 0) return true;

		var isFinished = true;
		foreach(var chara in charaList)
		{
			if(chara.Next()) isFinished = false;
		}

		if(!isFinished) return true;

		if(events.Count == 0)
		{
			IsComplete = true;
			if(StageManager.Instance.Current.ID != StageID.QuestStage) 
			{
				if(!Context.Instance.IsTutorialComplete)
				{
					if(interactiveId == ConstData.Tables.TutorialEventID1)
						ShopManager.Instance.DrawGacha(ConstData.Tables.FirstGachaID);
					else if(interactiveId == ConstData.Tables.TutorialEventID2)
					{
						Context.Instance.IsTutorialComplete = true;
						Message.Send(MessageID.SwitchStage, StageID.Home);
					}
				}
				else
					Destroy(gameObject);
			}
			return false;
		}

		var ev = events.Dequeue();

		background.gameObject.AddMissingComponent<SwitchTexture>().Switch("BG/" + ev.s_BG);
		//SwitchBG(ev.s_BG);

		Message.Send(MessageID.PlayMusic, ev.s_BGM);

		for(int i = 0; i < charaList.Count; ++i) charaList[i].Play(i, ev);

		if(ev.f_QUAKE > 0) TweenShake.Begin(container, ev.f_QUAKE, 20, 2);

		return true;
	}

	private void CreateChara()
	{
		if(pendingCount > 0) return;
		
		pendingCount = 4;

		charaList = new List<EventChara>(pendingCount);
		
		ResourceManager.Instance.CreatePrefabInstance(container, "EventPlay/EventDialog", OnCharaCreateComplete);
		ResourceManager.Instance.CreatePrefabInstance(container, "EventPlay/EventChara", OnCharaCreateComplete);
		ResourceManager.Instance.CreatePrefabInstance(container, "EventPlay/EventCharaUpset", OnCharaCreateComplete);
		ResourceManager.Instance.CreatePrefabInstance(container, "EventPlay/EventPlayerDialog", OnCharaCreateComplete);
	}

	private void OnCharaCreateComplete(GameObject obj)
	{
		charaList.Add(obj.GetComponent<EventChara>());
		--pendingCount;
		if(events.Count > 0) Next ();
	}


}
