﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpecialQuestItemList : QuestListBase {

	protected override void OnStart ()
	{
		var list = new List<QuestItemData>();
		
		var tmp = new Dictionary<int, QuestItemData>();
		
		foreach(var item in GameSystem.Service.QuestInfo.SelectLimited())
		{
			var obj = default(QuestItemData);
			
			if(tmp.TryGetValue(item.MainQuestID, out obj))
			{
				if(obj.EndTime > item.EndTime) obj.EndTime = item.EndTime;
				continue;
			}
			
			var origin = ConstData.Tables.QuestDesign[item.QuestID];
			
			if(origin.n_COUNT > 0 && origin.n_COUNT % 10 == 0 && item.PlayCount >= (origin.n_COUNT / 10)) continue;
			
			var quest = QuestManager.Instance.GetSpecial(item.MainQuestID);
			
			if(quest == null) 
			{
				Debug.LogWarning(string.Format ("quest not exists, main_quest_id = {0}", item.MainQuestID));
				continue;
			}
			
			var isEmpty = true;
			
			foreach(var sub in quest.List)
			{
				if(sub.n_PRE_QUEST != 0 && !GameSystem.Service.QuestInfo[sub.n_PRE_QUEST].IsCleared) continue;
				
				isEmpty = false;
				break;
			}
			
			if(isEmpty) continue;
			
			obj = new QuestItemData();
			obj.IsMainQuest = true;
			obj.EventID = item.EventID;
			obj.ID = item.MainQuestID;
			obj.EndTime = item.EndTime;
			obj.Name = ConstData.Tables.QuestName[quest.FirstQuestID].s_MAIN_NAME;
			
			list.Add(obj);
			tmp.Add(item.MainQuestID, obj);
		}
		
		list.Sort(Compare);
		
		Setup(list);
	}
	

	private static int Compare(QuestItemData lhs, QuestItemData rhs)
	{
		return lhs.EndTime.CompareTo(rhs.EndTime);
	}
	
	
}
