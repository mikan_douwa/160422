﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class QuestListBase : UIBase {

	public ItemList itemList;

	private List<QuestItemData> list;

	public bool IsInitialized { get; private set; }

	virtual public bool ShowTip { get { return false; } }

	virtual public bool DynamicAppendix { get { return false; } }

	public bool HasChallenge { get; private set; }

	public void Setup(List<QuestItemData> list)
	{
		this.list = list;

		foreach(var item in list)
		{
			if(item.ChallengeList != null && item.ChallengeList.Count > 0)
			{
				HasChallenge = true;
				break;
			}
		}

		ApplyChange();
	}

	protected override void OnChange ()
	{
		itemList.Setup(list);

		IsInitialized = true;
	}
}
