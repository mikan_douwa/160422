using UnityEngine;
using System.Collections;

public class QuestNode : UIBase {

	public GameObject icon;

	public UILabel nodeName;

	public int Index;
	public QuestList Data;

	public GameObject Origin;

	protected override void OnChange ()
	{
		var data = ConstData.Tables.QuestDesign[Data.FirstQuestID];

		if(data == null || data.n_PRE_QUEST != 0 && !GameSystem.Service.QuestInfo[data.n_PRE_QUEST].IsCleared)
		{
			collider.enabled = false;
			icon.SetActive(false);
		}
		else
		{
			collider.enabled = true;
			icon.SetActive(true);
			
			var origin = Origin.transform.localPosition;
			
			transform.localPosition = new Vector3(origin.x + data.n_POS_X, origin.y - data.n_POS_Y);
			
			nodeName.text = (Index + 1).ToString();
		}
	}

	void OnClick()
	{
		Context.Instance.MainQuest = Data;
		Message.Send(MessageID.ShowQuestView, QuestPage.SUB_QUEST);
	}

}
