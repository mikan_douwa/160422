﻿using UnityEngine;
using System.Collections;

public class ChallengeInfo : UIBase {

	public GameObject[] icons;

	public UILabel[] labels;


	protected override void OnChange ()
	{
		var data = Context.Instance.Quest;

		if(data.ChallengeList == null || data.ChallengeList.Count == 0) return;

		var state = GameSystem.Service.QuestInfo[data.ID];

		for(int i = 0; i < icons.Length; ++i)
		{
			icons[i].SetActive(state.IsChallegeFinish(i));
			labels[i].text = ConstData.GetChallengeText(data.ChallengeList[i].n_ID);
		}
	}
}
