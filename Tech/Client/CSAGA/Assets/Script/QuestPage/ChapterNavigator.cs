﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ChapterNavigator : Navigator {
	
	protected override void OnForwardEnd (ItemRenderer renderer)
	{
		var data = GetData<Chapter>(Current);

		Message.Send(MessageID.NextChapter, data.No);
	}
}
