﻿using UnityEngine;
using System.Collections;

public class DifficultySwitch : MonoBehaviour {

	public UILabel label;
	public UISprite background;
	// Use this for initialization
	void Start () {
		QuestManager.Instance.SetupDifficulty();

		RefreshButton();
	}
	
	void OnClick()
	{
		++Context.Instance.Difficulty;
		if(Context.Instance.Difficulty > 3) Context.Instance.Difficulty = 1;

		RefreshButton();

		Message.Send(MessageID.NextDifficulty);
	}

	private void RefreshButton()
	{
		switch(Context.Instance.Difficulty)
		{
		case 2:
			label.text = ConstData.GetSystemText(SystemTextID._451_NORMAL_MODE);
			background.spriteName = "button_08";
			break;
		case 3:
			label.text = ConstData.GetSystemText(SystemTextID._452_HARD_MODE);
			background.spriteName = "button_05";
			break;
		case 1:
		default:
			label.text = ConstData.GetSystemText(SystemTextID._450_EASY_MODE);
			background.spriteName = "button_04";
			break;
		}
	}
}
