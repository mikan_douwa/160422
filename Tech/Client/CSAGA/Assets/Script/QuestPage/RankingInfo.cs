﻿using UnityEngine;
using System.Collections;

public class RankingInfo : MonoBehaviour {

	private static int showCount = 0;
	private static GameObject instance;

	public UILabel point;
	public UILabel rank;
	public UILabel next;

	public SystemTextLabel rankPrefix;
	public SystemTextLabel nextPrefix;
	public SystemTextLabel btnText;

	public TextureLoader banner;

	public UILabel condition1;
	public UILabel condition2;
	public UILabel percent1;
	public UILabel percent2;
	public UILabel today;

	public bool isSummary = false;

	private Mikan.CSAGA.ConstData.EventSchedule schedule;

	private int cachedPoint;

	public void ShowReward()
	{
		Message.Send(MessageID.ShowQuestView, schedule.n_RANKING > 0 ? QuestPage.RANKING_REWARD : QuestPage.POINT_REWARD);
	}

	public void ShowRanking()
	{
		if(schedule.n_TYPE == Mikan.CSAGA.EventScheduleType.Crusade)
		{
			MessageBox.ShowConfirmWindow("GameScene/Crusade/CrusadeSubmitWindow", "", null).title = ConstData.GetSystemText(SystemTextID._443_CRUSADE_SELECT);
		}
		else
		{
			Context.Instance.RankingTag = RankingListView.TAG.Top10;
			Message.Send(MessageID.ShowQuestView, QuestPage.RANKING_TOP10);
		}
	}

	void Awake()
	{
		schedule = ConstData.Tables.EventSchedule[Context.Instance.EventScheduleID];
		
		if(schedule != null)
		{
			if(rankPrefix != null)
			{
				if(schedule.n_TYPE == Mikan.CSAGA.EventScheduleType.Crusade)
				{
					rankPrefix.textId = isSummary ? SystemTextID._445_CRUSADE_POINT_OTHER : SystemTextID._440_CRUSADE_DISCOVER;
				}
				else
					rankPrefix.textId = SystemTextID._204_CURRENT_RANKING;

				rankPrefix.ApplyChange();
			}
			
			if(nextPrefix != null)
			{
				if(schedule.n_TYPE == Mikan.CSAGA.EventScheduleType.Crusade)
				{
					nextPrefix.textId = isSummary ? SystemTextID._446_CRUSADE_TOTAL_POINT : SystemTextID._441_CRUSADE_COUNT;
				}
				else
					nextPrefix.textId = SystemTextID._205_NEXT_RANKING;

				nextPrefix.ApplyChange();
			}

			if(btnText != null)
			{
				btnText.textId = schedule.n_TYPE == Mikan.CSAGA.EventScheduleType.Crusade ? SystemTextID._442_BTN_CRUSADE :  SystemTextID._202_EVENT_RANKING;
				btnText.ApplyChange();
			}
		}
	}

	// Use this for initialization
	void Start () {

		if(banner != null)
		{
			var data = ConstData.Tables.EventSchedule[Context.Instance.EventScheduleID];
			if(data.n_SHOWBANNER != 0)
			{
				var bannerData = ConstData.Tables.Setup[data.n_SHOWBANNER];
				if(bannerData != null) banner.Load(bannerData.s_STRING);
			}
		}

		if(today != null)
		{
			today.text = "[ffff00]" + ConstData.GetSystemText(SystemTextID._288_TOWER_BEST_SCORE_V1, "[-]--");
		}

		GameSystem.Service.PlayerInfo.OnPropertyChange += OnPropertyChange;
		GameSystem.Service.OnPurchase += OnPurchase;
		GameSystem.Service.OnRankingInfo += OnRankingInfo;
		GameSystem.Service.GetRankingInfo();
	}

	void OnPurchase (Mikan.CSAGA.ArticleID id)
	{
		if(id == Mikan.CSAGA.ArticleID.CrusadePoint && !isSummary) UpdateCrusade();
	}

	void OnPropertyChange (Mikan.CSAGA.Data.PlayerInfo sender, Mikan.CSAGA.PropertyID id, int oldValue, int newValue)
	{
		if(id == Mikan.CSAGA.PropertyID.Discover && !isSummary)
			rank.text = GameSystem.Service.QuestInfo.Crusade.Discover.ToString();
	}

	void OnDestroy()
	{
		if(gameObject == instance) 
		{
			instance = null;
			showCount = 0;
		}

		if(GameSystem.IsAvailable)
		{
			GameSystem.Service.PlayerInfo.OnPropertyChange -= OnPropertyChange;
			GameSystem.Service.OnPurchase -= OnPurchase;
			GameSystem.Service.OnRankingInfo -= OnRankingInfo;
		}
	}

	void OnRankingInfo ()
	{
		var ranking = GameSystem.Service.RankingInfo[Context.Instance.EventScheduleID];

		if(ranking != null)
		{
			if(schedule != null && schedule.n_TYPE == Mikan.CSAGA.EventScheduleType.PVP)
				point.text = (ranking.Point + 1000).ToString();
			else
				point.text = ranking.Point.ToString();

			cachedPoint = ranking.Point;


			if(ranking.Point > 0 && ranking.PlayerCount >= ConstData.Tables.RankingPlayerLimit)
			{
				if(ranking.PlayerCount > 0 && ranking.Ranking >= 0)
					rank.text = Format(ranking.Ranking, ranking.PlayerCount) + "%"; 
				else
					rank.text = "--";

				if(ranking.Next > 0)
					next.text = ranking.Next.ToString();
				else
					next.text = "--";
			}
			else
			{
				rank.text = "--";
				next.text = "--";
			}
		}
		else
		{
			point.text = "--";
			rank.text = "--";
			next.text = "--";
		}

		if(schedule != null && schedule.n_TYPE == Mikan.CSAGA.EventScheduleType.Crusade)
		{
			UpdateCrusade();
		}

		if(Context.Instance.MainQuest.IsInfinity)
		{
			var bonus = GameSystem.Service.InfinityInfo.Bonus(GameSystem.Service.CurrentTime);

			if(bonus == null) return;

			if(condition1 != null)
			{
				condition1.gameObject.SetActive(true);
				condition1.text = ConstData.GetSystemText(SystemTextID._290_TOWER_CONDITION1_V1, ConstData.GetTypeText(bonus.Value1));

				percent1.text = string.Format("{0}%", ConstData.Tables.TowerBonus);
			}

			if(condition2 != null)
			{
				condition2.gameObject.SetActive(true);
				condition2.text = ConstData.GetSystemText(SystemTextID._291_TOWER_CONDITION2_V1, ConstData.GetGroupText(bonus.Value2));

				percent2.text = string.Format("{0}%", ConstData.Tables.TowerBonus);
			}

			if(today != null)
			{
				var point = GameSystem.Service.InfinityInfo.Point(GameSystem.Service.CurrentTime);
				today.text = "[ffff00]" + ConstData.GetSystemText(SystemTextID._288_TOWER_BEST_SCORE_V1, "[-]" + (point > 0 ? point.ToString() : "--"));
			}
		}
	}

	private void UpdateCrusade()
	{
		if(isSummary)
		{
			var subtotal = 0;
			foreach(var item in GameSystem.Service.QuestInfo.Crusade.QuestList)
			{
				if(!item.IsSelf) continue;
				var data = ConstData.Tables.QuestDesign[item.QuestID];
				var pt = Mathf.FloorToInt(data.n_POINT * ConstData.Tables.CrusadeOwner);
				subtotal += pt * item.PlayCount;
			}

			point.text = (cachedPoint - subtotal).ToString();
			rank.text = subtotal.ToString();
			next.text = cachedPoint.ToString();
		}
		else
		{
			rank.text = GameSystem.Service.QuestInfo.Crusade.Discover.ToString();
			var current = GameSystem.Service.QuestInfo.Crusade.CalcPoint(GameSystem.Service.CurrentTime);
			var max = ConstData.Tables.CrusadeMax;
			next.text = string.Format("{0}/{1}", current, max);
		}
	}

	public static void Show()
	{
		if(++showCount == 1) 
		{
			if(instance != null)
				instance.SetActive(true);
			else
				ResourceManager.Instance.CreatePrefabInstance("GameScene/RankReward/RankingInfo", o=>{ instance = o; });
		}
	}

	public static void Hide()
	{
		if(--showCount == 0 && instance != null) 
		{
			Coroutine.DelayInvoke(DelayHide);
		}
	}

	private static void DelayHide()
	{
		if(showCount == 0 && instance != null)
		{
			instance.SetActive(false);
			Coroutine.DelayInvoke(DelayDestroy, 3f);
		}
	}
	private static void DelayDestroy()
	{
		if(showCount == 0 && instance != null)
		{
			Destroy(instance);
			instance = null;
		}
	}

	public static string Format(int current, int total)
	{
		if(current == total - 1) return "99.9";
		var value = (float)current / (float)total * 100f;
		var value1 = Mathf.Floor(value);
		if(value1 < value && value - value1 < 0.1f) value = value1 + 0.1f;
		
		return value.ToString("F1"); 
	}

}
