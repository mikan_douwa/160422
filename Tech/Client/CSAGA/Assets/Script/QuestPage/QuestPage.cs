﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class QuestPage : Stage {

	public const int EMPTY = 0;
	public const int SUB_QUEST = 1;
	public const int EVENT_LIST = 2;
	public const int EVENT_QUEST = 3;
	public const int SCHEDULED_QUEST = 4;
	public const int QUEST_PREPARE = 5;
	public const int SELECT_ONE = 6;
	public const int SELECT_SUPPORT = 7;
	public const int RANDOM_SUPPORT = 8;

	public const int RANKING_TOP10 = 9;
	public const int RANKING_SELF = 10;
	public const int RANKING_REWARD = 11;
	public const int POINT_REWARD = 12;

	public GameObject nodePrefab;

	public VirtualCardTeamView teamView;

	public PresetNavigator questList;

	public Navigator navigator;

	public TextureLoader background;

	public TextureLoader backgroundExtra;
	public GameObject backgorundExtraObj;

	public GameObject origin;

	public GameObject mask;

	public GameObject difficulty;

	public MapMover mover;

	private int chapterNo;

	private string map = "BG/bg_map";

	private QuestNode focusNode;

	private List<QuestNode> nodes = new List<QuestNode>();

	protected override void Initialize ()
	{
		QuestManager.Instance.SetupDifficulty();
		
		mask.SetActive(false);
		
		Menu.Show();
		
		questList.OnCurrentChange += OnQuestListCurrentChange;
		
		Message.AddListener(MessageID.ShowQuestView, OnShowQuestView);
		Message.AddListener(MessageID.NextChapter, OnNextChapter);
		Message.AddListener(MessageID.NextDifficulty, OnNextDifficulty);
		Message.AddListener(MessageID.ShowChallengeReward, OnShowChallengeReward);
		
		if(Context.Instance.QuestType == Mikan.CSAGA.QuestType.Extra)
		{
			map = "BG/bg_map2";
			backgorundExtraObj.SetActive(true);
		}
		
		ResourceManager.Instance.PreloadTexture(map, OnPreloadComplete);
		ResourceManager.Instance.CreatePrefabInstance(gameObject, "GameScene/Quest/RescueInfo");
	}

	protected override void OnDestroy ()
	{
		Message.RemoveListener(MessageID.ShowQuestView, OnShowQuestView);
		Message.RemoveListener(MessageID.NextChapter, OnNextChapter);
		Message.RemoveListener(MessageID.NextDifficulty, OnNextDifficulty);
		Message.RemoveListener(MessageID.ShowChallengeReward, OnShowChallengeReward);

		questList.OnCurrentChange -= OnQuestListCurrentChange;
		
		base.OnDestroy ();
	}

	private void OnPreloadComplete(IAsset<Texture2D> asset)
	{
		if(IsDestroyed) return;
		
		background.Load(map);
		
		SetupChapter();
		
		InitializeComplete();
	}
	
	protected override void OnInitializeComplete ()
	{
		if(Context.Instance.TriggerQuestID > 0)
		{
			MessageBox.Show(ConstData.GetSubQuestName(Context.Instance.TriggerQuestID)).title = ConstData.GetSystemText(SystemTextID.TIP_SPECIAL_QUEST_ACTIVATED);
			Context.Instance.TriggerQuestID = 0;
		}
		
		var current = EMPTY;
		
		if(Context.Instance.RestoreQuestEntrance)
		{
			var data = ConstData.Tables.QuestDesign[Context.Instance.QuestID];
			if(data.n_QUEST_TYPE != Mikan.CSAGA.QuestType.Regular && data.n_QUEST_TYPE != Mikan.CSAGA.QuestType.Extra)
			{
				var showList = true;
				var limited = GameSystem.Service.QuestInfo.GetLimited(data.n_ID);
				
				if(limited != null)
				{
					if(data.n_COUNT > 0 && data.n_COUNT % 10 == 0 && limited.PlayCount >= (data.n_COUNT / 10))
						showList = false;
				}
				
				if(showList) current = Context.Instance.IsScheduledQuest ? SCHEDULED_QUEST : EVENT_QUEST;
			}
			Context.Instance.RestoreQuestEntrance = false;
		}
		else if(Context.Instance.CachedFellow != null)
		{
			current = QUEST_PREPARE;
		}
		else if(Context.Instance.SwitchByNavigator)
		{
			current = EVENT_LIST;
		}
		
		questList.Next (current);
	}
	
	private void SetupChapter()
	{
		var questId = Context.Instance.QuestCache.GetLastQuestID(Context.Instance.QuestType);
		
		var data = ConstData.Tables.QuestDesign[questId];
		
		var current = data != null ? data.n_CHAPTER : 0;
		
		//var current = Context.Instance.LastQuestID.Value != 0 ? ConstData.Tables.QuestDesign[Context.Instance.LastQuestID.Value] : null;
		
		var chapterList = QuestManager.Instance.ChaperList[Context.Instance.QuestType];
		
		var list = new List<Chapter>();
		foreach(var item in chapterList)
		{
			var first = ConstData.Tables.QuestDesign[item.List[0].FirstQuestID];
			if(first.n_PRE_QUEST == 0 || GameSystem.Service.QuestInfo[first.n_PRE_QUEST].IsCleared) 
			{
				if(item.No == current) navigator.current = list.Count;
				//if(current != null && item.No == current.n_CHAPTER) navigator.current = list.Count;
				list.Add(item);
				
			}
		}
		
		navigator.Setup(list);
	}

	protected override void OnChange ()
	{
		if(chapterNo <= 0) return;

		focusNode = null;

		foreach(var item in nodes) Destroy(item.gameObject);

		if(Context.Instance.QuestType == Mikan.CSAGA.QuestType.Extra) 
		{
			difficulty.SetActive(true);
			backgroundExtra.Load(string.Format("UI/extra_stage_{0}", chapterNo));
		}

		nodes = new List<QuestNode>();

		var chapter = QuestManager.Instance.GetChapter(chapterNo);

		var lastQuestId = Context.Instance.QuestCache.Get(Context.Instance.QuestType, chapterNo);

		if(lastQuestId > 0)
		{
			var lastQuest = ConstData.Tables.QuestDesign[lastQuestId];
			if(lastQuest != null) lastQuestId = lastQuest.n_MAIN_QUEST;
		}
		
		if(lastQuestId == 0) 
			lastQuestId = chapter.FirstMainQuestID;

		for(int i = 0; i < chapter.List.Count; ++i)
		{
			var item  = chapter.List[i];
			if(Context.Instance.QuestType == Mikan.CSAGA.QuestType.Extra && item.Difficulty != Context.Instance.Difficulty) continue;

			var node = UIManager.Instance.AddChild(background.gameObject, nodePrefab).GetComponent<QuestNode>();
			node.Origin = origin;
			node.Index = nodes.Count;
			node.Data = item;
			node.ApplyChange();
			nodes.Add(node);

			if(item.MainQuestID == lastQuestId) focusNode = node;
		}

		SafeStartCoroutine(DelayRecenter(focusNode));
	}

	private IEnumerator DelayRecenter(QuestNode node)
	{
		if(Context.Instance.QuestType != Mikan.CSAGA.QuestType.Regular || node == null) yield break;

		while(UIBase.IsChanging) yield return new WaitForEndOfFrame();

		if(node != focusNode) yield break;

		node.GetComponent<UIToggle>().value  = true;
		node.gameObject.AddMissingComponent<UICenterOnChildLite>().Recenter();

		mover.Init();
		mover.MoveTo(0f, -node.transform.localPosition.y);
	}

	public override void OnShowEnd ()
	{
		SafeStartCoroutine(DelayRecenter(focusNode));
	}

	protected override void OnBack ()
	{
		if(teamView.Back()) return;

		switch(questList.current)
		{
		case EVENT_LIST:
		case SUB_QUEST:
			questList.Next(EMPTY);
			break;
		case EVENT_QUEST:
		case SCHEDULED_QUEST:
			questList.Next(EVENT_LIST);
			break;
		case QUEST_PREPARE:
			if(Context.Instance.Quest.Type == Mikan.CSAGA.QuestType.Regular)
				questList.Next (SUB_QUEST);
			else
				questList.Next (Context.Instance.IsScheduledQuest ? SCHEDULED_QUEST : EVENT_QUEST);
			break;
		case RANKING_TOP10:
		case RANKING_SELF:
		case RANKING_REWARD:
		case POINT_REWARD:
			questList.Next (Context.Instance.IsScheduledQuest ? SCHEDULED_QUEST : EVENT_QUEST);
			break;
		case EMPTY:
		default:
			base.OnBack();
			break;
		}
	}

	protected override void OnNext ()
	{
		if(teamView.Next()) return;

		switch(questList.current)
		{
		case EMPTY:
		case SUB_QUEST:
			questList.Next (EVENT_LIST);
			break;
		case SCHEDULED_QUEST:
		{
			var text = ConstData.GetSystemText(Context.Instance.MainQuest.IsInfinity ? SystemTextID._436_INFINITY_DESCRIPTION : (Context.Instance.MainQuest.QuestType == Mikan.CSAGA.QuestType.Crusade ? SystemTextID._438_CRUSADE_DESCRIPTION : SystemTextID._433_EVENT_DESCRIPTION));
			var msgBox = MessageBox.Show("GameScene/Quest/EventQuestInfoMessageBox", text);
			msgBox.title = ConstData.GetSystemText(Context.Instance.MainQuest.IsInfinity ? SystemTextID._435_INFINITY : (Context.Instance.MainQuest.QuestType == Mikan.CSAGA.QuestType.Crusade ? SystemTextID._437_CRUSADE : SystemTextID._432_EVENT));
			msgBox.pivot = UIWidget.Pivot.Left;
			break;
		}
		case RANKING_TOP10:
			Context.Instance.RankingTag = RankingListView.TAG.MyRanking;
			questList.Next(RANKING_SELF);
			break;
		case RANKING_SELF:
			Context.Instance.RankingTag = RankingListView.TAG.Top10;
			questList.Next(RANKING_TOP10);
			break;
		case RANKING_REWARD:
			questList.Next(POINT_REWARD);
			break;
		case POINT_REWARD:
			questList.Next(RANKING_REWARD);
			break;
		}
	}


	void OnQuestListCurrentChange (PresetNavigator obj)
	{
		if(questList.current == EMPTY)
		{
			mask.SetActive(false);
			navigator.SetActive(true);

			titleFormat = TitleFormat.Static;
		}
		else
		{
			mask.SetActive(true);
			navigator.SetActive(false);

			titleFormat = TitleFormat.Dynamic;

			switch(questList.current)
			{
			case EVENT_LIST:
				titleText = ConstData.GetSystemText(SystemTextID.SPECIAL_QUEST);
				break;
			case SUB_QUEST:
			case EVENT_QUEST:
			case SCHEDULED_QUEST:
				titleText = ConstData.GetQuestName(Context.Instance.MainQuest.FirstQuestID);
				break;
			case QUEST_PREPARE:
				titleText = ConstData.GetSystemText(SystemTextID.TEAM_CONFIRM);
				break;
			case SELECT_ONE:
				titleText = ConstData.GetSystemText(SystemTextID.BTN_CARD_TEAM);
				break;
			case SELECT_SUPPORT:
				titleText = ConstData.GetSystemText(SystemTextID._411_FELLOW_SUPPORT);
				break;
			case RANDOM_SUPPORT:
				titleText = ConstData.GetSystemText(SystemTextID._412_OTHER_SUPPORT);
				break;
			case RANKING_TOP10:
				titleText = ConstData.GetSystemText(SystemTextID._202_EVENT_RANKING);
				break;
			case RANKING_SELF:
				titleText = ConstData.GetSystemText(SystemTextID._209_MY_RANKING);
				break;
			case RANKING_REWARD:
				titleText = ConstData.GetSystemText(SystemTextID._206_RANKING_REWARD);
				break;
			case POINT_REWARD:
				titleText = ConstData.GetSystemText(SystemTextID._207_POINT_REWARD);
				break;
			}
		}

		UpdateHeadline();
	}

	private void OnShowQuestView(IMessage msg)
	{
		questList.Next((int)msg.Data);
	}

	private void OnNextChapter(IMessage msg)
	{
		chapterNo = (int)msg.Data;

		Context.Instance.ChapterNo = chapterNo;

		ApplyChange();
	}

	private void OnNextDifficulty()
	{
		ApplyChange();
	}

	private void OnShowChallengeReward()
	{
		MessageBox.Show("GameScene/Challenge/ChallengeRewardMsg", "").title = ConstData.GetSystemText(SystemTextID._482_MISSION_REWARD);
	}

}
