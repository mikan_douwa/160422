﻿using UnityEngine;
using System.Collections;

public class QuestNavigator : Stage {

	public BoxCollider extraCollider;

	public UIWidget extraWidget;

	public GameObject faithObj;

	protected override void Initialize ()
	{
		if(ConstData.Tables.GetVariable(Mikan.CSAGA.VariableID.EXSTAGE_ENABLE) == 0)
		{
			extraCollider.enabled = false;
			extraWidget.color = Color.gray;
		}
#if !DEBUG
		if(ConstData.Tables.GetVariable(Mikan.CSAGA.VariableID.FAITH_ENABLE) != 0)
#endif
		{
			faithObj.SetActive(true);
		}

		base.Initialize ();
	}

	public void OnRegular()
	{
		Context.Instance.QuestType = Mikan.CSAGA.QuestType.Regular;

		Message.Send(MessageID.SwitchStage, StageID.Quest);
	}

	public void OnExtra()
	{

		Context.Instance.QuestType = Mikan.CSAGA.QuestType.Extra;

		Message.Send(MessageID.SwitchStage, StageID.Quest);
	}

}
