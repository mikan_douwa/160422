﻿using UnityEngine;
using System.Collections;

public class QuestListView : View {

	public GameObject prefab;

	private GameObject challengeObj;

	private QuestListBase list;

	protected override void Initialize ()
	{
		list = UIManager.Instance.AddChild(gameObject, prefab).GetComponent<QuestListBase>();

		StartCoroutine(WaitFor(list));
	}

	private IEnumerator WaitFor(QuestListBase list)
	{
		while(list.IsInitialized) yield return new WaitForEndOfFrame();

		if(list.DynamicAppendix)
		{
			if(list.ShowTip) 
				appendixTextID = SystemTextID._431_EVENT_TIP;
			else
				appendixTextID = SystemTextID.NONE;
		}

		InitializeComplete();
	}

	public override void OnShowEnd ()
	{
		if(list != null && list.HasChallenge)
		{
			ResourceManager.Instance.CreatePrefabInstance("GameScene/Challenge/ChallengeRewardButton", (go)=>
			{
				challengeObj = go;
			});
		}
		base.OnShowEnd ();
	}

	public override void OnHideEnd ()
	{
		if(challengeObj != null)
		{
			Destroy(challengeObj);
			challengeObj = null;
		}
		base.OnHideEnd ();
	}

	protected override void OnDestroy ()
	{
		if(challengeObj != null)
		{
			Destroy(challengeObj);
			challengeObj = null;
		}

		base.OnDestroy ();
	}
}
