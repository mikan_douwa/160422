﻿using UnityEngine;
using System.Collections;

public class CardGrowItemRenderer : ItemRenderer<CardGrowData> {

	public Unit unit;

	public UIProgressBar exp;
	public UIProgressBar kizuna;

	public GameObject icon;

	private TweenFloatValue tweener;

	private float currentProgress;

	protected override void OnStart ()
	{
		ResourceManager.Instance.CreatePrefabInstance(gameObject, "GameScene/Card/Unit", OnUnitCreateComplete);
		Message.AddListener(MessageID.PlayQuestSettlement, OnPlay);
		Message.AddListener(MessageID.SkipQuestSettlement, OnSkip);

	}

	void OnDestroy()
	{
		Message.RemoveListener(MessageID.PlayQuestSettlement, OnPlay);
		Message.RemoveListener(MessageID.SkipQuestSettlement, OnSkip);
	}

	private void OnPlay()
	{
		if(data.Origin == null) return;

		currentProgress = 0;

		tweener = TweenFloatValue.Begin(gameObject, UIDefine.TWEEN_DURATION, 1f);
		tweener.OnValueChanged = OnTweenValueChange;
	}

	private void OnTweenValueChange(float val)
	{
		if(data.Exp != null)
		{
			var _exp = data.Exp.OriginValue + (int)(data.Exp.Diff * val);
			var prop = data.Origin.Calc(_exp);
			
			unit.LV = prop.LV;
			exp.value = prop.ExpProgress;
			unit.ApplyChange();

			if(prop.ExpProgress < currentProgress)
			{
				icon.SetActive(true);
				icon.GetComponent<UIWidget>().alpha = 0;
				TweenAlpha.Begin(icon, 0.1f, 1f);
			}

			currentProgress = prop.ExpProgress;
		}
		
		if(data.Kizuna != null)
		{
			var _kizuna = data.Kizuna.OriginValue + (int)(data.Kizuna.Diff * val);
			kizuna.value = (_kizuna % 100) / 100f;
		}
	}

	private void OnSkip()
	{
		if(data.Origin == null) return;

		tweener.enabled = false;

		OnTweenValueChange(1f);

	}

	private void OnUnitCreateComplete(GameObject obj)
	{
		unit = obj.GetComponent<Unit>();
		ApplyChange();
	}

	#region implemented abstract members of ItemRenderer

	protected override void ApplyChange (CardGrowData data)
	{
		if(unit == null) return;
		if(data.Origin == null)
		{
			unit.CardID = 0;
			unit.IsExternal = false;
			exp.gameObject.SetActive(false);
			kizuna.gameObject.SetActive(false);
		}
		else
		{
			unit.CardID = data.Origin.ID;
			unit.IsExternal = data.Origin.Serial.IsExternal;

			var prop = data.Origin.Calc(data.Exp != null ? data.Exp.OriginValue : data.Origin.Exp);

			var _kizuna = data.Kizuna != null ? data.Kizuna.OriginValue : data.Origin.Kizuna;

			unit.LV = prop.LV;
			exp.value = prop.ExpProgress;
			kizuna.value = (_kizuna % 100) / 100f;
		}

		unit.ApplyChange();
	}

	#endregion


}

public class CardGrowData
{
	public Mikan.CSAGA.Data.Card Origin;
	public Mikan.ValuePair Exp;
	public Mikan.ValuePair Kizuna;
}