﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class MapMover : MonoBehaviour {

	public const float MAP_WIDTH = 2564f;
	public float MAP_HEIGHT = 2272f;

	//public GameObject bigMap;

	protected Transform mTrans;
	protected Transform mParent;
	protected UIRoot mRoot;
	public UIWidget parentWidget;

	float RootHeight = 960f;
	float RootWidth = 640f;
	float widthBound;
	float heightBound;
	UITexture map;
	List<GameObject> smallMapList;
	List<GameObject> dotList;

	public void Init()
	{
		mTrans = transform;
		mParent = mTrans.parent;
		mRoot = NGUITools.FindInParents<UIRoot>(mParent);

		RootHeight = parentWidget.height;
		map = GetComponent<UITexture>();
		MAP_HEIGHT = map.height;
		heightBound = (MAP_HEIGHT - RootHeight) / 2f;
	}

	void OnDrag(Vector2 delta)
	{
		if(Context.Instance.QuestType == Mikan.CSAGA.QuestType.Extra) return;
		if(mRoot != null && mTrans != null)
		{
			Vector2 movePos = delta * mRoot.pixelSizeAdjustment;
			mTrans.localPosition = RestrictRegion(mTrans.localPosition + new Vector3(0f, movePos.y, 0f));
		}
	}

	Vector3 RestrictRegion(Vector3 pos)
	{
		//float x = pos.x;
		float y = pos.y;

		//if(x > widthBound)
		//	x = widthBound;
		//else if(x < -widthBound)
		//	x = -widthBound;

		if(y > heightBound)
			y = heightBound;
		else if(y < -heightBound)
			y = -heightBound;

		return new Vector3(0f, y, 0f);
	}

	public void MoveTo(float x, float y)
	{
		Vector3 pos = RestrictRegion(new Vector3(x, y, 0f));
		TweenPosition tp = TweenPosition.Begin(gameObject, 0.1f, pos);
	}
}
