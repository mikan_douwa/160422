﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class QuestItemList : QuestListBase {
	
	public bool isEventQuestList;

	public RankingInfoLoader rankingInfo;

	private List<QuestItemData> list;

	private GameObject prompt;

	private bool bIsPending = false;

	public override bool ShowTip {
		get {
			return isEventQuestList;
		}
	}

	public override bool DynamicAppendix {
		get {
			return isEventQuestList;
		}
	}
	/*
	protected override void OnNext ()
	{
		if(isEventQuestList)
		{
			var text = ConstData.GetSystemText(Context.Instance.MainQuest.IsInfinity ? SystemTextID._436_INFINITY_DESCRIPTION : (Context.Instance.MainQuest.QuestType == Mikan.CSAGA.QuestType.Crusade ? SystemTextID._438_CRUSADE_DESCRIPTION : SystemTextID._433_EVENT_DESCRIPTION));
			var msgBox = MessageBox.Show("GameScene/Quest/EventQuestInfoMessageBox", text);
			msgBox.title = ConstData.GetSystemText(Context.Instance.MainQuest.IsInfinity ? SystemTextID._435_INFINITY : (Context.Instance.MainQuest.QuestType == Mikan.CSAGA.QuestType.Crusade ? SystemTextID._437_CRUSADE : SystemTextID._432_EVENT));
			msgBox.pivot = UIWidget.Pivot.Left;
		}
		else
			Message.Send(MessageID.SwitchStage, StageID.SpecialQuestList);
	}
	*/

	protected override void OnStart ()
	{
		//nextButtonText = ConstData.GetSystemText(SystemTextID.SPECIAL_QUEST);
		
		var list = new List<QuestItemData>();
		
		if(Context.Instance.MainQuest == null) 
		{
			var chapter = QuestManager.Instance.GetChapter(Context.Instance.ChapterNo);
			
			if(chapter != null)
			{
				var lastQuestId = Context.Instance.QuestCache.Get(Context.Instance.QuestType, Context.Instance.ChapterNo);
				var data = ConstData.Tables.QuestDesign[lastQuestId];
				if(data != null)
				{
					foreach(var item in chapter.List)
					{
						if(item.MainQuestID == data.n_MAIN_QUEST)
						{
							Context.Instance.MainQuest = item;
							break;
						}
					}
				}
			}
			
			if(Context.Instance.MainQuest == null)
				Context.Instance.MainQuest = QuestManager.Instance.GetChapter(1).List[0];
		}
		
		//titleText = ConstData.GetQuestName(Context.Instance.MainQuest.FirstQuestID);
		
		//if(isEventQuestList)
		//	nextButtonText = ConstData.GetSystemText(SystemTextID._431_EVENT_TIP);
		
		ShowQuestList();
	}

	protected override void OnDestroy ()
	{
		base.OnDestroy ();

		if(GameSystem.IsAvailable) 
		{
			GameSystem.Service.OnQuestOpen -= OnQuestOpen;
			GameSystem.Service.OnCrusade -= OnCrusade;
			GameSystem.Service.OnSocialQuery -= OnSocialQuery;
		}
	}
	#region Crusade
	private List<QuestItemData> GetCrusadeList()
	{
		var list = new List<QuestItemData>();
		
		foreach(var item in GameSystem.Service.QuestInfo.Crusade.SelectQuest())
		{
			var data = ConstData.Tables.QuestDesign[item.QuestID];
			
			var obj = QuestItemData.Create(data);
			obj.IsCrusade = true;
			obj.SharedQuest = item;
			obj.EndTime = item.EndTime;

			obj.IsCleared = GameSystem.Service.QuestInfo.Crusade.PlayList.Contains(item.Serial);
			
			list.Add(obj);
		}

		return list;
	}

	void OnQuestOpen (int questId)
	{
		if(prompt != null)
		{
			Destroy(prompt);
			prompt = null;
		}

		list = GetCrusadeList();

		Setup(list);
	}

	private void OnCrusade()
	{
		GameSystem.Service.OnCrusade -= OnCrusade;

		list = GetCrusadeList();

		var players = new List<string>();

		foreach(var item in list)
		{
			players.Add(item.SharedQuest.PlayerID);
		}

		if(list.Count == 0)
		{
			ResourceManager.Instance.CreatePrefabInstance(gameObject, "GameScene/Quest/EventEmptyPrompt", OnPromptCreateComplete);

			if(isEventQuestList && rankingInfo != null) rankingInfo.Show();
			//InitializeComplete();
		}
		else
		{
			GameSystem.Service.OnSocialQuery += OnSocialQuery;
			GameSystem.Service.SocialQuery(players);
		}
	}

	void OnSocialQuery ()
	{
		GameSystem.Service.OnSocialQuery -= OnSocialQuery;
		
		Setup(list);
		
		if(isEventQuestList && rankingInfo != null) rankingInfo.Show();
		//InitializeComplete();

	}

	#endregion

	private void OnPromptCreateComplete(GameObject go)
	{
		prompt = go;
	}

	private void ShowQuestList()
	{
		if(Context.Instance.MainQuest.QuestType == Mikan.CSAGA.QuestType.Crusade)
		{
			GameSystem.Service.OnQuestOpen += OnQuestOpen;
			GameSystem.Service.OnCrusade += OnCrusade;
			GameSystem.Service.QueryCrusade();
		}
		else
		{
			var list = new List<QuestItemData>();

			foreach(var item in Context.Instance.MainQuest.List)
			{
				if(item.n_PRE_QUEST != 0 && !GameSystem.Service.QuestInfo[item.n_PRE_QUEST].IsCleared) continue;
				
				var data = QuestItemData.Create(item);
				
				var limited = default(Mikan.CSAGA.Data.LimitedQuest);
				
				if(item.n_QUEST_TYPE != Mikan.CSAGA.QuestType.Regular && item.n_QUEST_TYPE != Mikan.CSAGA.QuestType.Extra)
				{
					limited = GameSystem.Service.QuestInfo.GetLimited(item.n_ID);
					
					if(limited != null)
					{
						if(limited.IsOverdue) continue;
						data.PlayCount = limited.PlayCount;
						data.EndTime = limited.EndTime;
					}
					else if(item.n_QUEST_TYPE == Mikan.CSAGA.QuestType.Limited)
						continue;
				}
				
				if(data.IsInfinity)
				{
					if(limited != null)
					{
						var current = GameSystem.Service.InfinityInfo.Current(Context.Instance.MainQuest.MainQuestID, limited.BeginTime);
						if(current == data.ID|| (current == 0 && data.ID == Context.Instance.MainQuest.FirstQuestID))
							list.Add(data);
					}
				}
				else if(data.TotalCount == 0 || data.PlayCount < data.TotalCount || item.n_COUNT % 10 == 1)
					list.Add(data);
			}
			
			//provider = ListItemDataProvider<QuestItemData>.Create(list);	
			
			if(Context.Instance.MainQuest.QuestType != Mikan.CSAGA.QuestType.Regular && Context.Instance.MainQuest.QuestType != Mikan.CSAGA.QuestType.Extra) 
			{
				//if(!isEventQuestList || (ConstData.Tables.GetVariable(Mikan.CSAGA.VariableID.POINT_BUTTON_ENABLE) == 0)) disableNextButton = true;
				
				if(list.Count == 0) ResourceManager.Instance.CreatePrefabInstance(gameObject, "GameScene/Quest/EventOverduePrompt");
			}
			
			Setup(list);
			
			if(isEventQuestList && rankingInfo != null) rankingInfo.Show();
			
			//InitializeComplete();
		}

	}
	
	/*
	protected override void OnGetBack ()
	{
		if(parent != null && parent.container != null)
			parent.container.transform.localPosition = Vector3.zero;

		if(bIsPending) 
		{
			ShowQuestList();
			bIsPending = false;
		}
	}
	*/
}


public class QuestItemData
{
	public int ID;
	public string Name;
	public int Level;
	public int Rounds;
	public bool IsCleared;
	public Mikan.CSAGA.QuestType Type;

	public int PlayCount;
	public int TotalCount;
	public System.DateTime EndTime;

	public bool IsMainQuest = false;
	public int EventID;
	public bool IsInfinity = false;
	public bool IsCrusade = false;

	public Mikan.CSAGA.Data.SharedQuest SharedQuest;

	public List<Mikan.CSAGA.ConstData.Challenge> ChallengeList;

	public static QuestItemData Create(Mikan.CSAGA.ConstData.QuestDesign data)
	{
		var obj = new QuestItemData();
		obj.ID = data.n_ID;
		obj.Type = data.n_QUEST_TYPE;
		obj.Level = data.n_LEVEL;
		obj.Rounds = data.n_STEP;
		var text = ConstData.Tables.QuestName[data.n_ID];
		obj.Name = text.s_SUB_NAME;
		obj.TotalCount = data.n_COUNT / 10;
		obj.IsInfinity = Context.Instance.MainQuest.IsInfinity;
		obj.IsCleared = GameSystem.Service.QuestInfo[data.n_ID].IsCleared;

		obj.ChallengeList = new List<Mikan.CSAGA.ConstData.Challenge>(ConstData.Tables.Challenge.Select(o=>o.n_STAGEID == data.n_ID));

		return obj;
	}
}