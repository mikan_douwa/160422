﻿using UnityEngine;
using System.Collections;

public class ChapterItemRenderer : ItemRenderer<Chapter> {

	public UILabel label;

	#region implemented abstract members of ItemRenderer

	protected override void ApplyChange (Chapter data)
	{
		label.text = ConstData.GetChaperName(data.No, data.Type);
	}

	#endregion





}
