﻿using UnityEngine;
using System.Collections;

public class EventQuestInfo : MonoBehaviour {
	
	public TextureLoader texture;

	public UILabel point;
	
	void Start()
	{
		if(Context.Instance.MainQuest.MainQuestID == ConstData.Tables.PVPMainQuestID)
		{
			var schedule = ConstData.Tables.EventSchedule[GameSystem.Service.PVPInfo.EventID];

			var setupId = (schedule != null) ? schedule.n_SHOWBANNER : 0;

			var setupData = ConstData.Tables.Setup[setupId];
			if(setupData != null) 
				texture.Load(setupData.s_STRING);
			else
				texture.Load(ConstData.GetSystemText(ConstData.Tables.PVPBannerID));
			
			point.gameObject.SetActive(false);
		}
		else
		{
			var setupId = 0;
			var prevBeginTime = System.DateTime.MaxValue;
			if(Context.Instance.MainQuest.QuestType == Mikan.CSAGA.QuestType.Crusade)
			{
				foreach(var item in ConstData.Tables.EventSchedule.Select(o=> o.n_TYPE == Mikan.CSAGA.EventScheduleType.Crusade))
				{
					var beginTime = Mikan.CSAGA.Calc.Quest.ParseBeginTime(item.n_START_DAY, item.n_START_TIME);
					var endTime = beginTime.AddMinutes(item.n_DURATION);
					if(endTime < GameSystem.Service.CurrentTime || beginTime > GameSystem.Service.CurrentTime || beginTime > prevBeginTime) continue;
					
					prevBeginTime = beginTime;
					setupId = item.n_SHOWBANNER;
				}
			}
			else
			{
				foreach(var item in ConstData.Tables.EventSchedule.Select(o=> o.n_TYPE == Mikan.CSAGA.EventScheduleType.Quest))
				{
					if(Context.Instance.MainQuest.IsInfinity != (item.n_MAIN_POINT == 0)) continue;
					var beginTime = Mikan.CSAGA.Calc.Quest.ParseBeginTime(item.n_START_DAY, item.n_START_TIME);
					var endTime = beginTime.AddMinutes(item.n_DURATION);
					if(endTime < GameSystem.Service.CurrentTime || beginTime > GameSystem.Service.CurrentTime || beginTime > prevBeginTime) continue;
					
					prevBeginTime = beginTime;
					setupId = item.n_SHOWBANNER;
				}
			}
			
			var setupData = ConstData.Tables.Setup[setupId];
			
			if(setupData != null) texture.Load(setupData.s_STRING);

			if(Context.Instance.MainQuest.QuestType == Mikan.CSAGA.QuestType.Crusade)
				StartCoroutine(UpdateRemainTime());
			else if(Context.Instance.MainQuest.IsInfinity)
				point.gameObject.SetActive(false);
			else
				point.text = ConstData.GetSystemText(SystemTextID._434_TODAY_BONUS_V1_V2, GameSystem.Service.DailyEventPoint, GameSystem.Service.PlayerInfo.Data.n_BONUS_POINT);
		}
	}

	IEnumerator UpdateRemainTime()
	{
		var crusade = GameSystem.Service.QuestInfo.Crusade;
		var rescue = default(Mikan.CSAGA.Data.Rescue);
		
		var prefix = ConstData.GetSystemText(SystemTextID._439_CRUSADE_POINT_REMAIN_TIME);
		
		while(true)
		{
			var remains = crusade.PointRemainSeconds(GameSystem.Service.CurrentTime);
			
			point.text = string.Format("{0} {1:00}:{2:00}:{3:00}", prefix, remains / 3600, (remains / 60) % 60, remains % 60);
			
			yield return new WaitForSeconds(1);
		}
	}

}