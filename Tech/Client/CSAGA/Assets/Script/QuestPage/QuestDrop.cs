﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class QuestDrop : MonoBehaviour {

	public Icon icon;

	private Mikan.CSAGA.ConstData.Drop data;

	public static void Show(Mikan.CSAGA.Reward reward)
	{
		Show (new Mikan.CSAGA.Reward[]{ reward });
	}

	public static void Show(IEnumerable<Mikan.CSAGA.Reward> rewards)
	{
		Context.Instance.Rewards = new Queue<Mikan.CSAGA.Reward>(rewards);
		Message.Send(MessageID.ShowMessageBox, MessageBoxContext.Create("GameScene/Quest/QuestDrop"));
	}

	public static void Show(int dropId)
	{
		Show (new int[]{ dropId });
	}

	public static void Show(IEnumerable<int> drops)
	{
		Context.Instance.Drops = new Queue<int>(drops);
		Message.Send(MessageID.ShowMessageBox, MessageBoxContext.Create("GameScene/Quest/QuestDrop"));
	}

	// Use this for initialization
	void Start () {

		if(Context.Instance.Drops.Count > 0)
		{
			var dropId = Context.Instance.Drops.Dequeue();

			data = ConstData.Tables.Drop[dropId];

			icon.IconType = data.n_DROP_TYPE;
			icon.IconID = data.n_DROP_ID;
			icon.Count = data.n_COUNT;
			icon.Rare = data.n_RARE;
		}
		else if(Context.Instance.Rewards.Count > 0)
		{
			var reward = Context.Instance.Rewards.Dequeue();
			icon.IconType = reward.Type;
			icon.IconID = reward.Value2;

			if(reward.Type == Mikan.CSAGA.DropType.Card)
			{
				icon.Count = reward.Value1 / 1000;
				icon.Rare = reward.Value1 % 1000 / 100;
			}
			else
				icon.Count = reward.Value1;
		}
		icon.ApplyChange();
	}

	void OnDestroy()
	{
		if(Context.Instance.Drops.Count > 0 || Context.Instance.Rewards.Count > 0)
			Message.Send(MessageID.ShowMessageBox, MessageBoxContext.Create("GameScene/Quest/QuestDrop"));
	}
}
