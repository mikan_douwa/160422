﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class QuestSettlement : Stage {

	enum STEP
	{
		Initialiing,
		Ready,
		Playing,
		End,
	}

	public UILabel mainStage;
	public UILabel subStage;
	public UILabel rankExp;
	public UILabel crystal;
	public GameObject pointObj;
	public UILabel point;
	public GameObject discoverObj;
	public UILabel discover;
	public UIGrid grid;
	public UIProgressBar expProgress;
	public GameObject icon;
	public GameObject manualIcon;

	public ItemList itemList;

	public InteractiveChara chara;

	public GameObject retryObj;

	private STEP step = STEP.Initialiing;

	private Mikan.CSAGA.QuestSettlement settlement;
	private Mikan.ValuePair gainExp;
	private Mikan.ValuePair gainCrystal;

	private TweenFloatValue tweener;

	private float currentProgress = 0;

	private bool isTutorialComplete = true;

	private bool isPVP;

	protected override void Initialize ()
	{
		Menu.Hide();

		isTutorialComplete = Context.Instance.IsTutorialComplete;

		isPVP = (Context.Instance.QuestID == ConstData.Tables.PVPQuestID);

		var questName = ConstData.Tables.QuestName[GameSystem.Service.QuestInfo.Current.QuestID];
		mainStage.text = questName.s_MAIN_NAME;
		subStage.text = questName.s_SUB_NAME;

		var team = GameSystem.Service.TeamInfo.Current;

		var map = Mikan.Serial.CreateDictinary<CardGrowData>();
		var provider = new List<CardGrowData>();
		var serials = new List<Mikan.Serial>();
		foreach(var item in team.Members) 
		{
			var obj = new CardGrowData();

			if(!item.IsEmpty)
			{
				obj.Origin = GameSystem.Service.CardInfo[item];
				map.Add(item, obj);

				var data = ConstData.Tables.Card[obj.Origin.ID];
				if(data.n_GIFT != 0) serials.Add(item);
			}

			provider.Add(obj);
		}

		var selection = GameSystem.Service.CardInfo[Mikan.Serial.Create(Context.Instance.FavoriteCard.Value)];

		if(serials.Count > 0)
			selection = GameSystem.Service.CardInfo[serials[Random.Range(0, serials.Count)]];
		else if(selection == null)
		{
			selection = GameSystem.Service.CardInfo.List.Find(o=>{ return o.Data.n_GIFT != 0; });
			
			if(selection == null) selection = GameSystem.Service.CardInfo.List[0];
		}

		chara.CardSerial = selection.Serial;
		chara.InteractiveID = Mikan.CSAGA.Calc.CardEvent.Next(Mikan.CSAGA.Calc.CardEvent.Filter.Settlement, selection.ID, selection.Kizuna, GameSystem.Service.CardInfo.IDs);
		chara.ApplyChange();

		settlement = GameSystem.Service.QuestInfo.Settlement;

		if(settlement.SyncPlayer != null)
		{
			foreach(var item in settlement.SyncPlayer.Diff.Select())
			{
				switch(item.Key)
				{
				case Mikan.CSAGA.PropertyID.Exp:
					gainExp = item.Value;
					break;
				case Mikan.CSAGA.PropertyID.Crystal:
					gainCrystal = item.Value;
					break;
				}
			}
		}

		if(settlement.SyncCard != null && settlement.SyncCard.UpdateList != null)
		{
			foreach(var item in settlement.SyncCard.UpdateList)
			{
				var obj = default(CardGrowData);
				if(map.TryGetValue(item.Serial, out obj))
				{
					foreach(var diff in item.Select())
					{
						switch(diff.Key)
						{
						case Mikan.CSAGA.PropertyID.Exp:
							obj.Exp = diff.Value;
							break;
						case Mikan.CSAGA.PropertyID.Kizuna:
							obj.Kizuna = diff.Value;
							break;
						}
					}
				}
			}
		}

		itemList.Setup(provider);

		crystal.text = "0";
		rankExp.text = "0";

		if(gainExp != null)
			expProgress.value = PlayerUIUtils.GetRankExpProgress(gainExp.OriginValue);
		else
			expProgress.value = PlayerUIUtils.GetRankExpProgress(GameSystem.Service.PlayerInfo[Mikan.CSAGA.PropertyID.Exp]);

		if(settlement.Point != 0)
		{
			pointObj.SetActive(true);
			point.text = settlement.Point.ToString();
			if(settlement.Point < 0) point.color = Color.red;
		}

		if(settlement.Discover > 0)
		{
			discoverObj.SetActive(true);
			discover.text = settlement.Discover.ToString();
		}

		grid.Reposition();

		step = STEP.Ready;

		base.Initialize ();
	}

	protected override void OnDestroy ()
	{
		if(GameSystem.IsAvailable && isTutorialComplete && !isPVP)
			Context.Instance.RestoreQuestEntrance = true;

		Menu.Show();
		base.OnDestroy ();
	}

	void OnClick()
	{
		switch(step)
		{
		case STEP.Ready:
			Play ();
			break;
		case STEP.Playing:
			SkipPlay();
			break;
		case STEP.End:
			if(!isTutorialComplete)
				Message.Send(MessageID.PlayEvent, ConstData.Tables.TutorialEventID1);
			else if(QuestManager.Instance.IsTriggerViaCardEvent)
				Message.Send(MessageID.SwitchStage, StageID.Card);
			else if(isPVP)
			{
				if(Context.Instance.PVPWaves.IsSimulating) Context.Instance.PVPWaves.IsSimulateComplete = true;
				Message.Send(MessageID.SwitchStage, StageID.PVP);
			}
			else
				Message.Send(MessageID.SwitchStage, StageID.Quest);
			break;
		}
	}

	private void Play()
	{
		Message.Send(MessageID.PlayQuestSettlement);

		if(!isPVP && !settlement.IsAutoBattleEnabled) manualIcon.SetActive(true);

		if(gainCrystal == null && gainExp == null)
			OnTweenFinish();
		else
		{
			tweener = TweenFloatValue.Begin(gameObject, UIDefine.TWEEN_DURATION, 1f);
			tweener.OnValueChanged = OnTweenValueChange;
			tweener.AddOnFinished(OnTweenFinish);
			step = STEP.Playing;
		}
	}

	private void OnTweenValueChange(float val)
	{
		if(gainCrystal != null)
		{
			var _crystal = (int)(gainCrystal.Diff * val);
			crystal.text = _crystal.ToString();
		}
		
		if(gainExp != null)
		{
			var expDiff = (int)(gainExp.Diff * val);
			var _exp = gainExp.OriginValue + expDiff;
			rankExp.text = expDiff.ToString();
			expProgress.value = PlayerUIUtils.GetRankExpProgress(_exp);

			if(currentProgress > expProgress.value)
			{
				icon.SetActive(true);
				icon.GetComponent<UIWidget>().alpha = 0;
				TweenAlpha.Begin(icon, 0.1f, 1f);
			}

			currentProgress = expProgress.value;
		}
	}

	private void SkipPlay()
	{
		Message.Send(MessageID.SkipQuestSettlement);

		if(tweener != null) tweener.enabled = false;

		OnTweenValueChange(1f);

		OnTweenFinish();
	}

	private void OnTweenFinish()
	{
		step = STEP.End;

		if(retryObj != null)
		{
			var data = ConstData.Tables.QuestDesign[GameSystem.Service.QuestInfo.Current.QuestID];

			if(data.n_ID != ConstData.Tables.TutorialQuestID && (data.n_QUEST_TYPE == Mikan.CSAGA.QuestType.Regular || data.n_QUEST_TYPE == Mikan.CSAGA.QuestType.Extra))
				retryObj.SetActive(true);
		}

		if(settlement.Drops.Count > 0) QuestDrop.Show(settlement.Drops);
	}

	public void Retry()
	{
		MessageBox.ShowConfirmWindow(ConstData.GetSystemText(SystemTextID._485_TIP_RETRY_CONFIRM), (index)=> { if(index == 0) Message.Send(MessageID.BeginQuest); });
	}
}
