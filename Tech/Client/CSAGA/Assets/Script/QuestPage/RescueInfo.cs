﻿using UnityEngine;
using System.Collections;

public class RescueInfo : MonoBehaviour {

	public GameObject icon;
	public UILabel label;
	// Use this for initialization
	void Start () {
		StartCoroutine(RefreshRoutine());
	}

	IEnumerator RefreshRoutine()
	{
		while(true)
		{
			var rescue = GameSystem.Service.QuestInfo.Rescue;
			
			if(rescue == null || rescue.EndTime < GameSystem.Service.CurrentTime)
			{
				icon.SetActive(false);
				break;
			}
			else
				label.text = string.Format("{0}/{1}", rescue.Count, rescue.Max);
			yield return new WaitForSeconds(1);
		}
	}

	public void PopInfo()
	{
		var rescue = GameSystem.Service.QuestInfo.Rescue;

		if(rescue == null) return;

		Message.Send(MessageID.PlaySound, SoundID.CLICK);

		if(rescue.Count == 0)
		{
			var msgBox = MessageBox.Show("GameScene/Quest/RescueInfoMessageBox", "", SystemTextID._224_RESCUE_NOW, SystemTextID.BTN_DISABLE);
			msgBox.callback = OnMessageBoxClose;
			msgBox.colors = new ButtonColor[]{ ButtonColor.Green, ButtonColor.None };
		}
		else
			MessageBox.Show("GameScene/Quest/RescueInfoMessageBox", "", SystemTextID.BTN_DISABLE);
			
	}

	private void OnMessageBoxClose(int index)
	{
		if(index != 0) return;
		var rescue = GameSystem.Service.QuestInfo.Rescue;
		if(rescue.Count == 0) Message.Send(MessageID.ChargeRescuePoint);
	}
}
