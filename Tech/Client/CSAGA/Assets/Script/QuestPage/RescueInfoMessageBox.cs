﻿using UnityEngine;
using System.Collections;

public class RescueInfoMessageBox : MonoBehaviour {

	public TextureLoader texture;

	public UILabel remainTime;

	void Start()
	{
		var setupId = 0;
		var prevBeginTime = System.DateTime.MaxValue;
		foreach(var item in ConstData.Tables.EventSchedule.Select(o=>{ return o.n_TYPE == Mikan.CSAGA.EventScheduleType.Rescue; }))
		{
			var beginTime = Mikan.CSAGA.Calc.Quest.ParseBeginTime(item.n_START_DAY, item.n_START_TIME);
			var endTime = beginTime.AddMinutes(item.n_DURATION);
			if(endTime < GameSystem.Service.CurrentTime || beginTime > GameSystem.Service.CurrentTime || beginTime > prevBeginTime) continue;

			prevBeginTime = beginTime;
			setupId = item.n_SHOWBANNER;
		}

		var setupData = ConstData.Tables.Setup[setupId];

		if(setupData != null) texture.Load(setupData.s_STRING);

		StartCoroutine(UpdateRemainTime());

	}

	IEnumerator UpdateRemainTime()
	{
		var rescue = default(Mikan.CSAGA.Data.Rescue);

		var prefix = ConstData.GetSystemText(SystemTextID._221_RESCUE_REMAIN_TIME);

		while(true)
		{
			if(rescue == null || rescue.NextUpdateTime < GameSystem.Service.CurrentTime)
				rescue = GameSystem.Service.QuestInfo.Rescue;

			if(rescue == null)
			{
				remainTime.text = prefix + " --:--:--";
				break;
			}
			if(rescue.Count == rescue.Max)
			{
				remainTime.text = prefix + " 00:00:00";
				break;
			}

			var remains = rescue.NextUpdateTime.Subtract(GameSystem.Service.CurrentTime);

			remainTime.text = string.Format("{0} {1:00}:{2:00}:{3:00}", prefix, remains.Hours, remains.Minutes, remains.Seconds);

			yield return new WaitForSeconds(1);
		}
	}
}
