﻿using UnityEngine;
using System.Collections;

public class QuestItemRenderer : ItemRenderer<QuestItemData> {

	public UILabel questName;
	public UILabel level;
	public UILabel rounds;
	public UILabel remains;
	public UILabel count;
	public GameObject clearIcon;

	public GameObject remainsObj;

	public GameObject countObj;

	public GameObject infoObj;

	public GameObject challengeObj;

	public GameObject[] challengeList;

	private bool IsUpdateRoutineRunning = false;

	protected override void OnStart ()
	{
		GameSystem.Service.OnPurchase += OnPurchase;
	}

	void OnDestroy()
	{
		if(GameSystem.IsAvailable) GameSystem.Service.OnPurchase -= OnPurchase;
	}


	void OnPurchase (Mikan.CSAGA.ArticleID id)
	{
		if(data == null || data != Context.Instance.Quest) return;

		switch(id)
		{
		case Mikan.CSAGA.ArticleID.QuestCount:
		{
			data.PlayCount = 0;
			ApplyChange();
			break;
		}
		case Mikan.CSAGA.ArticleID.CrusadePoint:
		{

			break;
		}
		default:
			return;
		}


		QuestManager.Instance.PrepareQuest(data);

	}

	protected override void ApplyChange (QuestItemData data)
	{
		questName.text = data.Name;

		if(data.IsMainQuest)
		{
			infoObj.SetActive(false);
			clearIcon.SetActive(false);
			challengeObj.SetActive(false);
		}
		else
		{
			infoObj.SetActive(true);

			remains.text = "";
			level.text = ConstData.GetSystemText(SystemTextID.MODE_V1, data.Level);
			rounds.text = ConstData.GetSystemText(SystemTextID.TURN_V1, data.Rounds);
			clearIcon.SetActive(data.IsCleared);
			challengeObj.SetActive(data.ChallengeList.Count > 0);

			if(data.ChallengeList.Count > 0)
			{
				if(data.IsCleared)
				{
					var state = GameSystem.Service.QuestInfo[data.ID];

					for(int i = 0; i < 3; ++i)
						challengeList[i].SetActive(state[i+1]);
				}
				else
				{
					foreach(var obj in challengeList) obj.SetActive(false);
				}
			}
			else
				challengeObj.SetActive(false);
		}

		switch(data.Type)
		{
		case Mikan.CSAGA.QuestType.Regular:
		case Mikan.CSAGA.QuestType.Extra:
		{
			remainsObj.SetActive(false);
			countObj.SetActive(false);
			remains.text = "";
			count.text = "";
			break;
		}
		case Mikan.CSAGA.QuestType.Crusade:
		{
			var prefix = ConstData.GetSystemText(SystemTextID._448_CRUSADE_OWNER);

			if(data.SharedQuest.PlayerID == GameSystem.Service.Profile.PublicID)
			{
				questName.text = prefix + GameSystem.Service.PlayerInfo.Name;
			}
			else
			{
				var player = GameSystem.Service.SocialInfo[data.SharedQuest.PlayerID];
				if(player != null) 
					questName.text = prefix + player.Name;
				else
					questName.text = prefix + "(unknown)";
			}
			countObj.SetActive(false);
			remainsObj.SetActive(true);
			UpdateRemainTime();
			break;
		}
		default:
		{
			remainsObj.SetActive(true);
			countObj.SetActive(!data.IsMainQuest);
			UpdateRemainTime();
			break;
		}
		}

	}

	private void UpdateRemainTime()
	{
		if(!data.IsMainQuest)
		{	
			if(data.IsInfinity)
				count.text = "∞";
			else
				count.text = string.Format("{0}/{1}", data.TotalCount - data.PlayCount, data.TotalCount);
		}
		if(!IsUpdateRoutineRunning) StartCoroutine(UpdateRemainTimeRoutine());
	}

	void OnClick()
	{
		Context.Instance.Quest = data;

		if(data.IsMainQuest)
		{
			Context.Instance.MainQuest = QuestManager.Instance.GetSpecial(data.ID);
			Context.Instance.EventScheduleID = data.EventID;

			var schedule = ConstData.Tables.EventSchedule[data.EventID];
			if(schedule != null && (schedule.n_POINT > 0 || schedule.n_RANKING > 0))
			{
				Context.Instance.IsScheduledQuest = true;
				Message.Send(MessageID.ShowQuestView, QuestPage.SCHEDULED_QUEST);
			}
			else
			{
				Context.Instance.IsScheduledQuest = false;
				Message.Send(MessageID.ShowQuestView, QuestPage.EVENT_QUEST);
			}
		}
		else
		{
			if(data.Type == Mikan.CSAGA.QuestType.Crusade)
			{
				if(GameSystem.Service.QuestInfo.Crusade.CalcPoint(GameSystem.Service.CurrentTime) < 1)
				{
					Context.Instance.QuestID = data.ID;
					Message.Send(MessageID.ChargeCrusadePoint);
					return;
				}

			}
			else if(data.Type != Mikan.CSAGA.QuestType.Regular && data.Type != Mikan.CSAGA.QuestType.Extra)
			{

				if(!data.IsInfinity && data.PlayCount >= data.TotalCount)
				{
					Context.Instance.QuestID = data.ID;
					Message.Send(MessageID.ChargeQuestCount);
					return;
				}

			}

			QuestManager.Instance.PrepareQuest(data);
			Message.Send(MessageID.ShowQuestView, QuestPage.QUEST_PREPARE);
		}
	}

	IEnumerator UpdateRemainTimeRoutine()
	{
		IsUpdateRoutineRunning = true;

		while(true)
		{
			if(data == null || data.Type == Mikan.CSAGA.QuestType.Regular || data.Type == Mikan.CSAGA.QuestType.Extra) break;

			remains.text = UIUtils.FormatRemainTime(data.EndTime, false);

			yield return new WaitForSeconds(1f);
		}

		IsUpdateRoutineRunning = false;
	}
}
