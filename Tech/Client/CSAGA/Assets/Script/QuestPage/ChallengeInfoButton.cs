﻿using UnityEngine;
using System.Collections;

public class ChallengeInfoButton : MonoBehaviour {
	
	public GameObject obj;
	public UILabel label;

	// Use this for initialization
	void Start () {

		obj.SetActive(Context.Instance.Quest.ChallengeList.Count > 0);

		Message.AddListener(MessageID.SwicthChallengeInfoEnd, OnSwicthChallengeInfoEnd);

		UpdateText();
	}

	void OnEnable()
	{
		obj.SetActive(Context.Instance.Quest.ChallengeList.Count > 0);
	}

	void OnDestroy()
	{
		Message.RemoveListener(MessageID.SwicthChallengeInfoEnd, OnSwicthChallengeInfoEnd);
	}

	private void OnSwicthChallengeInfoEnd()
	{
		UpdateText();
	}

	private void UpdateText()
	{
		label.text = ConstData.GetSystemText(Context.Instance.ChallengeInfoButtonState == 0 ? SystemTextID._480_QUEST_MISSION : SystemTextID._481_TEAM_SKILL);
	}
}
