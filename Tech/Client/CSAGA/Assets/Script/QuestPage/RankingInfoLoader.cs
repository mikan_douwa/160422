﻿using UnityEngine;
using System.Collections;

public class RankingInfoLoader : MonoBehaviour {

	public GameObject container;

	public bool auto = true;
	// Use this for initialization
	void Start () {
		if(auto) Show();
	}

	public void Show()
	{
		if(container == null) container = gameObject;

		var limited = GameSystem.Service.QuestInfo.GetLimited(Context.Instance.MainQuest.FirstQuestID);
		
		var isOverdue = limited != null && limited.EndTime < GameSystem.Service.CurrentTime;
		
		var prefab = (Context.Instance.MainQuest.IsInfinity && !isOverdue) ? "GameScene/Quest/InfinityInfo" : "GameScene/Quest/RankingInfo";
		
		ResourceManager.Instance.CreatePrefabInstance(container, prefab);
	}
}
