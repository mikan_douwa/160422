﻿using UnityEngine;
using System.Collections;

public static class UIDefine
{
#if UNITY_ANDROID
	public const string AppVersion = "2.0.6";
#elif UNITY_IPHONE
	public const string AppVersion = "2.0.6";
#else
	public const string AppVersion = "0.9.0";
#endif

	public const float ScreenWidth = 640;
	public const float ScreenHeight = 1024;
	public const float CardWidth = 640;
	public const float CardHeight = 480;

	public const float TWEEN_DURATION = 0.3f;

#if DEBUG
	public const string FORMAT_FLOAT = "0.00";
#else
	public const string FORMAT_FLOAT = "0.##";
#endif

	public const int EmptyCardID = -1;
	public const int RemoveCardID = -1002;
	public const int FellowCardID = -1001;

	public static Vector3 OutScreen = new Vector3(3000, 0);

	public static string IAPName;
}

public enum UILayer
{
	Background = 8,
	Stage = 10,
	HighLevel = 15,
	Foreground = 20,
	Overlay = 21,
	EventPlay = 22,
	Title = 23,
	Popup = 25,
	Topmost = 30,
}


public static class SceneName
{
	public const string APPSTARTUP = "1AppStartup";
	public const string PRELOAD = "2Preload";
	public const string GAME = "3Game";
	public const string QUEST = "4Quest";
	public const string TESTFIGHT = "TestFight";
}