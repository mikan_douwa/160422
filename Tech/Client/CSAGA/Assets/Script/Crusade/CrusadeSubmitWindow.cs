﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CrusadeSubmitWindow : SubmitWindow {

	public UILabel diffcultTitle;
	public UILabel discoverLabel;
	public UILabel diffcultLabel;
	public UILabel pointLabel;
	public Navigator navigator;

	protected override void OnReady ()
	{
		navigator.OnCurrentChange += NavigatorChangeHandler;

		diffcultTitle.text = ConstData.GetSystemText(SystemTextID.MODE_V1, "");

		List<CrusadeQuestOpenData> lstData = new List<CrusadeQuestOpenData>();
		List<int> lst = GameSystem.Service.QuestInfo.Crusade.Candidates;
		for(int i = 0; i < lst.Count; i++)
		{
			int id = lst[i];
			Mikan.CSAGA.ConstData.QuestDesign data = ConstData.Tables.QuestDesign[id];
			if(data != null)
			{
				lstData.Add(new CrusadeQuestOpenData(){ Index = i, QuestData = data });
			}
		}
		if(lstData.Count > 0)
			navigator.Setup(lstData);

		base.OnReady ();
	}

	protected override bool OnSubmit ()
	{
		CrusadeQuestOpenData data = navigator.GetData<CrusadeQuestOpenData>(navigator.Current);
		if(GameSystem.Service.QuestInfo.Crusade.Discover >= data.QuestData.n_DISCOVER)
		{
			GameSystem.Service.OpenQuest(data.QuestData.n_ID);
			return true;
		}
		else
		{
			MessageBox.Show(ConstData.Tables.GetErrorText(Mikan.CSAGA.ErrorCode.QuestDiscoverNotEnough));
			return false;
		}
	}

	protected override void OnDestroy ()
	{
		navigator.OnCurrentChange -= NavigatorChangeHandler;
		base.OnDestroy ();
	}
	

	void NavigatorChangeHandler(Navigator nav)
	{
		CrusadeQuestOpenData data = nav.GetData<CrusadeQuestOpenData>(nav.Current);
		discoverLabel.text = GameSystem.Service.QuestInfo.Crusade.Discover.ToString() + "(-" + data.QuestData.n_DISCOVER.ToString() + ")";
		diffcultLabel.text = data.QuestData.n_LEVEL.ToString();
		pointLabel.text = data.QuestData.n_POINT.ToString();

	}
}
