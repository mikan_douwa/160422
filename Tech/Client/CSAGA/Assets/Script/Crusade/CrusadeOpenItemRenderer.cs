﻿using UnityEngine;
using System.Collections;

public class CrusadeOpenItemRenderer : ItemRenderer<CrusadeQuestOpenData> {

	public UISprite sprite;
	public UILabel label;

	protected override void ApplyChange (CrusadeQuestOpenData data)
	{
		string name = "";
		Mikan.CSAGA.ConstData.QuestName text = ConstData.Tables.QuestName[data.QuestData.n_ID];
		if(text != null)
			name = text.s_SUB_NAME;
		label.text = name;

		//bg
		switch(data.Index)
		{
		case 0:
			sprite.spriteName = "ui_sub4_05";
			break;
		case 1:
			sprite.spriteName = "ui_sub4_02";
			break;
		case 2:
			sprite.spriteName = "ui_sub4_01";
			break;
		case 3:
			sprite.spriteName = "ui_sub4_04";
			break;
		case 4:
			sprite.spriteName = "ui_sub4_03";
			break;

		}
	}
}

public class CrusadeQuestOpenData
{
	public int Index;
	public Mikan.CSAGA.ConstData.QuestDesign QuestData;
}