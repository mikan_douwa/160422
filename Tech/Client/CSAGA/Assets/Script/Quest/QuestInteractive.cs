﻿using UnityEngine;
using System.Collections;

public class QuestInteractiveEvent : QuestEventBase
{
	private Mikan.CSAGA.QuestInstance quest;
	private int round;
	private int interactiveId;
	private Mikan.IQuestController controller;
	private EventPlayer3 eventPlayer;
	private bool skip = false;

	public QuestInteractiveEvent(Mikan.CSAGA.QuestInstance quest, int round, int interactiveId)
	{
		this.quest = quest;
		this.round = round;
		this.interactiveId = interactiveId;
		if (GameSystem.Service.QuestInfo [quest.QuestID].IsCleared) skip = true;

	}

	public override void Prepare (Mikan.IQuestController controller)
	{
		Debug.Log (string.Format ("interacvie, {0} . Prepare()", interactiveId));
		if (skip) 
			base.Prepare(controller);
		else 
		{
			this.controller = controller;
			Message.AddListener(MessageID.EndEvent, OnEndEvent);
			Message.Send (MessageID.PlayEvent, interactiveId);
			Coroutine.Start (WaitForEventPlayerReady ());
		}
	}

	public override void Dispose ()
	{
		Message.RemoveListener(MessageID.EndEvent, OnEndEvent);

		base.Dispose ();
	}
	#region implemented abstract members of QuestEventBase
	
	public override void Start ()
	{
		if (skip) 
		{
			Debug.Log (string.Format ("interacvie, {0} . Skip", interactiveId));
			onFinish(true);
		} 
		else 
		{
			Debug.Log (string.Format ("interacvie, {0} . Start()", interactiveId));
		}
	}
	
	#endregion

	private IEnumerator WaitForEventPlayerReady()
	{
		while(true)
		{
			if(eventPlayer == null) eventPlayer = GameObject.FindObjectOfType<EventPlayer3>();

			if(eventPlayer == null || !eventPlayer.IsInitialized)
			{
				yield return new WaitForSeconds(0.1f);
				continue;
			}
			break;
		}

		base.Prepare(controller);
	}

	private void OnEndEvent()
	{
		onFinish(true);
	}


	
	
}