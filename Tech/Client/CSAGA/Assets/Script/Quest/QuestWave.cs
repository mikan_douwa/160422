﻿using UnityEngine;
using System.Collections;

public class QuestWave : UIBase {

	public static QuestWave current { get; private set; }

	public UILabel label;

	public int Wave;
	public int TotalWave;

	protected override void OnStart ()
	{
		current = this;
		base.OnStart ();
	}

	protected override void OnDestroy ()
	{
		if(current == this) current = null;

		base.OnDestroy ();
	}
	protected override void OnChange ()
	{
		label.text = string.Format("{0}/{1}", Wave, TotalWave);
	}
	
	// Update is called once per frame
	void Update () {
		//QuestManager.Instance.Controller.
	}
}
