﻿using UnityEngine;
using System.Collections;

public class TacticConfirm : MessageBox {

	public TextureLoader icon;
	public UILabel tacticName;
	public UILabel detail;
	public UILabel rank;
	public Item[] itemArray;

	public UIPanel panel;
	public FightResLoader fxLoader;
	public GameObject fxRoot;

	int TacticId;

	protected override void OnReady ()
	{
		GameSystem.Service.OnTacticsEnable += HandleOnTacticsEnable;
		//preload fx
		fxLoader.Preload("StrengthenFX");

		RefreshInfo();
		StartCoroutine(SetDirty());
	}

	void HandleOnTacticsEnable (int tacticsId)
	{
		RefreshInfo();
		if(fxRoot != null)
		{
			fxLoader.PlayFX("StrengthenFX", Vector3.zero, fxRoot);
		}
		//Sound
		Message.Send(MessageID.PlaySound, SoundID.POWERUP);

		Message.Send(MessageID.RefreshTactics);
	}

	void RefreshInfo()
	{
		bool isMaxLv = true;
		int Lv = 0;
		Mikan.CSAGA.ConstData.Tactics beforeTactic = null;
		foreach(Mikan.CSAGA.ConstData.Tactics tacticData in ConstData.Tables.Tactics.Select(o => { return o.n_GROUP == Context.Instance.TacticsGroup; }))
		{
			if(GameSystem.Service.TacticsInfo.List.Contains(tacticData.n_ID))
			{
				if(tacticData.n_LV > Lv)
				{
					Lv = tacticData.n_LV;
					beforeTactic = tacticData;
				}
			}
		}

		Mikan.CSAGA.ConstData.Tactics data = ConstData.Tables.Tactics.SelectFirst(o=> { return o.n_GROUP == Context.Instance.TacticsGroup && o.n_LV == Lv + 1; });
		if(data == null)
		{
			data = beforeTactic;
			isMaxLv = true;
			TacticId = 0;
		}
		else
		{
			isMaxLv = false;
			TacticId = data.n_ID;
		}
		
		if(data != null)
		{
			string name = "";
			string description = "";
			Mikan.CSAGA.ConstData.TacticsText textData = ConstData.Tables.TacticsText[data.n_ID];
			if(textData != null)
			{
				name = textData.s_NAME;
				description = textData.s_TEXT;
			}
			tacticName.text = name + " " + ConstData.GetSystemText(SystemTextID.LV) + data.n_LV.ToString();
			detail.text = description;
			icon.Load("ICON/" + data.s_ICON);
			
			rank.text = ConstData.GetSystemText(SystemTextID.RANK) + " " + data.n_RANK.ToString();
			
			//Materials
			bool materialEnough = true;
			for(int i = 0; i < 5; i++)
			{
				Item item = itemArray[i];
				int id = 0;
				if(i < data.n_MATERIAL.Length)
					id = data.n_MATERIAL[i];
				
				if(id > 0)
				{
					item.ItemID = id;
					item.Count = -1;
					int count = data.n_COUNT[i];
					item.CountFormat = count.ToString() + "({0})";
					item.ApplyChange();
					//Check
					if(count > GameSystem.Service.ItemInfo.Count(item.ItemID))
						materialEnough = false;
				}
				else
				{
					item.ItemID = 0;
					item.Count = -1;
					item.CountFormat = "";
					item.ApplyChange();
				}
			}
			
			bool btnEnable = !isMaxLv && (data.n_RANK <= GameSystem.Service.PlayerInfo.LV) && materialEnough;
			SetButtonEnabled(0, btnEnable);
		}

	}

	IEnumerator SetDirty()
	{
		for(int i = 0; i < 10; i++)
		{
			panel.SetDirty();
			yield return new WaitForSeconds(0.2f);
		}
	}

	protected override void OnClickButton (int index)
	{
		if(index == 0)
			GameSystem.Service.TacticsEnable(TacticId);
		else
			Destroy(gameObject);

		//base.OnClickButton (index);
	}

	protected override void OnDestroy ()
	{
		if(GameSystem.IsAvailable)
			GameSystem.Service.OnTacticsEnable -= HandleOnTacticsEnable;
		base.OnDestroy ();
	}

#region On Material Item Long Press
	public void OnLongPress1()
	{
		Item item = itemArray[0];
		if(item.ItemID > 0)
		{
			ItemInfo.Show(item.ItemID);
		}
	}
	public void OnLongPress2()
	{
		Item item = itemArray[1];
		if(item.ItemID > 0)
		{
			ItemInfo.Show(item.ItemID);
		}
	}
	public void OnLongPress3()
	{
		Item item = itemArray[2];
		if(item.ItemID > 0)
		{
			ItemInfo.Show(item.ItemID);
		}
	}
	public void OnLongPress4()
	{
		Item item = itemArray[3];
		if(item.ItemID > 0)
		{
			ItemInfo.Show(item.ItemID);
		}
	}
	public void OnLongPress5()
	{
		Item item = itemArray[4];
		if(item.ItemID > 0)
		{
			ItemInfo.Show(item.ItemID);
		}
	}
#endregion

}
