﻿using UnityEngine;
using System;
using System.Collections;

public class TacticItemRenderer : ItemRenderer<TacticItemData> {

	public TextureLoader iconLoader;
	public UISprite noIconSprite;
	public UILabel name;
	public UILabel lv;
	
	protected override void ApplyChange (TacticItemData data)
	{
		string tName = "";
		if(data.constData == null)
		{
			tName = ConstData.Tables.GetSystemText(SystemTextID._472_NOT_OBTAINED_TACTIC);
			lv.text = "";

			NGUITools.SetActive(noIconSprite.gameObject, true);
			NGUITools.SetActive(iconLoader.gameObject, false);
		}
		else
		{
			Mikan.CSAGA.ConstData.TacticsText textData = ConstData.Tables.TacticsText[data.constData.n_ID];
			if(textData != null)
				tName = textData.s_NAME;
			lv.text = ConstData.GetSystemText(SystemTextID.LV) + data.constData.n_LV.ToString();

			NGUITools.SetActive(noIconSprite.gameObject, false);
			NGUITools.SetActive(iconLoader.gameObject, true);
			iconLoader.Load("ICON/" + data.constData.s_ICON);
		}
		name.text = tName;
	}

	void OnClick()
	{
		if(data.constData != null && data.nextID > 0)
		{
			Mikan.CSAGA.ConstData.Tactics tactic = ConstData.Tables.Tactics[data.nextID];
			if(tactic != null)
			{
				//Context.Instance.Tactics = tactic;
				Context.Instance.TacticsGroup = tactic.n_GROUP;
				MessageBoxContext msg = MessageBox.ShowConfirmWindow("GameScene/Tactic/TacticConfirm", "", LevelUp);
				msg.title = ConstData.Tables.GetSystemText(SystemTextID._474_TACTIC_ENHANCE);
			}
		}
	}

	void LevelUp(int res)
	{

	}

	void OnLongPress()
	{
		if(data.constData != null)
			TacticInfo.Show(data.constData);
	}
}


public class TacticItemData
{
	public Mikan.CSAGA.ConstData.Tactics constData;
	//public Action<int> levelUpFunc;
	public int nextID;
}