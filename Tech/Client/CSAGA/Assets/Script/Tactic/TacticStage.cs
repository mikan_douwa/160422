﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TacticStage : Stage {

	public ItemList itemList;

	protected override void Initialize ()
	{
		Message.AddListener(MessageID.RefreshTactics, RefreshInfo);

		RefreshInfo();
		base.Initialize ();
	}

	void RefreshInfo()
	{
		Dictionary<int, Mikan.CSAGA.ConstData.Tactics> dict = new Dictionary<int, Mikan.CSAGA.ConstData.Tactics>();
		List<TacticItemData> lst = new List<TacticItemData>();

		foreach(Mikan.CSAGA.ConstData.Tactics tactic in ConstData.Tables.Tactics.Select())
		{
			if(GameSystem.Service.TacticsInfo.List.Contains(tactic.n_ID))
			{
				//Learned.
				if(!dict.ContainsKey(tactic.n_GROUP))
					dict.Add(tactic.n_GROUP, tactic);
				else
				{
					Mikan.CSAGA.ConstData.Tactics prevTactic = dict[tactic.n_GROUP];
					if(prevTactic == null || tactic.n_LV > prevTactic.n_LV)
						dict[tactic.n_GROUP] = tactic;
				}
			}
			else
			{
				//Not learned.
				if(!dict.ContainsKey(tactic.n_GROUP))
					dict.Add(tactic.n_GROUP, null);
			}
		}

		foreach(KeyValuePair<int, Mikan.CSAGA.ConstData.Tactics> pair in dict)
		{
			int nextLv = 1;
			if(pair.Value != null)
				nextLv = pair.Value.n_LV + 1;

			Mikan.CSAGA.ConstData.Tactics next = ConstData.Tables.Tactics.SelectFirst(o=> { return o.n_GROUP == pair.Key && o.n_LV == nextLv; });
			int nextId = 0;
			if(next != null)
				nextId = next.n_ID;

			lst.Add(new TacticItemData(){ constData = pair.Value, nextID = nextId });
		}

		itemList.Setup(lst);
	}

	protected override void OnDestroy ()
	{
		Message.RemoveListener(MessageID.RefreshTactics, RefreshInfo);
		base.OnDestroy ();
	}


}
