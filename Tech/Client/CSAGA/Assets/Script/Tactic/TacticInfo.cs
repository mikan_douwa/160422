﻿using UnityEngine;
using System.Collections;

public class TacticInfo : MessageBox {

	public TextureLoader icon;

	public static void Show(Mikan.CSAGA.ConstData.Tactics data)
	{
		Context.Instance.Tactics = data;
		MessageBox.Show("GameScene/Tactic/TacticInfo", "");
	}
	
	protected override void OnReady ()
	{
		var data = Context.Instance.Tactics;

		string name = "";
		string description = "";
		Mikan.CSAGA.ConstData.TacticsText textData = ConstData.Tables.TacticsText[data.n_ID];
		if(textData != null)
		{
			name = textData.s_NAME;
			description = textData.s_TEXT;
		}

		titleLabel.text = name + " " + ConstData.GetSystemText(SystemTextID.LV) + data.n_LV.ToString();
		contentLabel.text = description;
		icon.Load("ICON/" + data.s_ICON);
	}
}
