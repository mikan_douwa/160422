﻿using UnityEngine;
using System.Collections;

public class PVPPlayerItemRenderer : ItemRenderer<PVPPlayerItemData> {
	public UILabel playerName;
	public UILabel point;
	public UnitLoader icon;
	#region implemented abstract members of ItemRenderer

	protected override void ApplyChange (PVPPlayerItemData data)
	{
		playerName.text = data.Origin.Name;
		point.text = ConstData.GetSystemText(SystemTextID._203_EVENT_POINT) + (data.Origin.Point + 1000).ToString();
		icon.CardID = data.Origin.Boss.ID;
		icon.Rare = data.Origin.Boss.Rare;
		icon.ApplyChange();
	}

	#endregion

	void OnClick()
	{
		Context.Instance.PVPPlayer = data;
		Message.Send(MessageID.SelectPVPPlayer);
	}
}


public class PVPPlayerItemData
{
	public Mikan.CSAGA.Data.PVPPlayer Origin;
}