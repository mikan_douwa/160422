﻿using UnityEngine;
using System.Collections;

abstract public class PVPEditSubView : View {
	
	protected override void Initialize ()
	{
		Message.AddListener(MessageID.SelectPVPWave, OnSelectWave);
		OnSelectWave();
		InitializeComplete();
	}

	protected override void OnDestroy ()
	{
		Message.RemoveListener(MessageID.SelectPVPWave, OnSelectWave);
		base.OnDestroy ();
	}

	abstract protected void Refresh(PVPWaveItemData data);
	
	private void OnSelectWave()
	{
		Refresh (Context.Instance.PVPWave);
	}
}
