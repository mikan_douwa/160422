﻿using UnityEngine;
using System.Collections;

public class PVPPrologItemRenderer : ItemRenderer<PVPPrologItemData> {
	public UIInput input;

	#region implemented abstract members of ItemRenderer

	protected override void ApplyChange (PVPPrologItemData data)
	{
		input.value = data.Wave.Prologs[data.Index];
	}

	#endregion

	public void OnInputChange()
	{
		data.Wave.Prologs[data.Index] = input.value;
		Context.Instance.PVPWaves.IsChanged = true;
	}

}

public class PVPPrologItemData
{
	public PVPWaveItemData Wave;
	public int Index;
}