﻿using UnityEngine;
using System.Collections;

public class PVPSkillItemRenderer : ItemRenderer<PVPSkillItemData> {

	static Color GRAY = new Color(0.3f, 0.3f, 0.3f);
	public UILabel description;
	public TextureLoader icon;
	public GameObject mask;

	#region implemented abstract members of ItemRenderer
	protected override void ApplyChange (PVPSkillItemData data)
	{
		var skillId = data.Wave.Origin.Skills[data.Index];

		icon.Load("ICON/" + ConstData.Tables.Skill[skillId].s_EFFECT_USE);

		var selected = data.Wave.Selections.Contains(data.Index);
		TweenColor.Begin(icon.gameObject, 0.1f, selected ? Color.white : GRAY);
		mask.SetActive(false);
		if(selected)
		{
			var text = ConstData.Tables.SkillText[skillId];
			if(text != null)
				description.text = text.s_TIP;
			else
				description.text = string.Format("SKILL({0})", skillId);
		}
	}
	#endregion

	void OnClick()
	{
		Message.Send(MessageID.PlaySound, SoundID.CLICK);
		if(data.Wave.Selections[data.Index / 2] == data.Index) return;
		data.Wave.Selections[data.Index / 2] = data.Index;
		Context.Instance.PVPWaves.IsChanged = true;
		Message.Send(MessageID.SelectPVPSkill);
	}

}

public class PVPSkillItemData
{
	public PVPWaveItemData Wave;
	public int Index;
}
