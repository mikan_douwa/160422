﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PVPSkillView : PVPEditSubView {

	public ItemListBase itemList;

	#region implemented abstract members of PVPEditSubView
	
	protected override void Refresh (PVPWaveItemData data)
	{
		var list = new List<PVPSkillItemData>(data.Origin.Skills.Count);
		for(int i = 0; i < data.Origin.Skills.Count; ++i)
		{
			var obj = new PVPSkillItemData();
			obj.Wave = data;
			obj.Index = i;
			list.Add(obj);
		}

		itemList.Setup(list);
	}
	
	#endregion

	protected override void Initialize ()
	{
		Message.AddListener(MessageID.SelectPVPSkill, OnSelectSkill);
		GameSystem.Service.OnPVPRerollSkill += OnPVPRerollSkill;
		base.Initialize ();
	}

	protected override void OnDestroy ()
	{
		Message.RemoveListener(MessageID.SelectPVPSkill, OnSelectSkill);
		if(GameSystem.IsAvailable) GameSystem.Service.OnPVPRerollSkill -= OnPVPRerollSkill;
		base.OnDestroy ();
	}

	void OnPVPRerollSkill ()
	{
		itemList.ForceRefresh();
	}

	private void OnSelectSkill()
	{
		itemList.ForceRefresh();
	}


}
