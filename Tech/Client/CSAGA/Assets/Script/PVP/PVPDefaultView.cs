﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PVPDefaultView : View {

	public TextureLoader pic;

	public PVPPage parent;

	public UIButton rankingButton;
	public UIButton rewardButton;

	public UILabel bp;

	public ItemListBase itemList;

	public CDButton queryButton;

	protected override void Initialize ()
	{
		if(GameSystem.Service.QuestInfo.PVP != null)
		{
			if(GameSystem.Service.QuestInfo.PVP.EndTime.AddDays(3) < GameSystem.Service.CurrentTime)
				rewardButton.isEnabled = false;
		}
		else
		{
			rankingButton.isEnabled = false;
			rewardButton.isEnabled = false;
		}

		parent = FindObjectOfType<PVPPage>();

		var duration = (float)GameSystem.Service.CurrentTime.Subtract(GameSystem.Service.PVPInfo.QueryTime).TotalSeconds;

		if(duration < 10f) queryButton.ChangeState(false, 10f - duration);

		GameSystem.Service.OnPVPInfo += OnPVPInfo;
		GameSystem.Service.OnPVPQuery += OnPVPQuery;
		GameSystem.Service.OnPVPUpdate += OnPVPUpdate;
		
		GameSystem.Service.GetPVPInfo();
	}
	
	protected override void OnDestroy ()
	{
		if(GameSystem.IsAvailable)
		{
			GameSystem.Service.OnPVPInfo -= OnPVPInfo;
			GameSystem.Service.OnPVPQuery -= OnPVPQuery;
			GameSystem.Service.OnPVPUpdate -= OnPVPUpdate;
		}
		
		base.OnDestroy ();
	}
	
	public void OnEdit()
	{
		parent.navigator.Next(PVPPage.EDIT);
	}
	public void OnRanking()
	{
		if(Context.Instance.EventScheduleID == 0) return;
		parent.navigator.Next(PVPPage.TOP_RANKING);
	}
	public void OnReward()
	{
		if(Context.Instance.EventScheduleID == 0) return;
		parent.navigator.Next(PVPPage.RANKING_REWARD);
	}
	public void OnQuery()
	{
		GameSystem.Service.PVPQuery(true);
	}
	
	void OnPVPUpdate ()
	{
		
	}
	
	void OnPVPInfo ()
	{
		pic.Load(ConstData.GetCardPicPath(GameSystem.Service.PVPInfo.Boss.ID));

		if(Context.Instance.PVPPlayers != null)
			itemList.Setup(Context.Instance.PVPPlayers);

		StartCoroutine(RefreshBPRoutine());
		
		InitializeComplete();
	}

	void OnPVPQuery ()
	{
		GameSystem.Service.OnPVPQuery -= OnPVPQuery;

		var list = new List<PVPPlayerItemData>();
		foreach(var item in GameSystem.Service.PVPInfo.Players)
		{
			var obj = new PVPPlayerItemData{ Origin = item };
			list.Add(obj);
		}

		Context.Instance.PVPPlayers = list;

		itemList.Setup(list);
	}

	IEnumerator RefreshBPRoutine()
	{
		while(true)
		{
			bp.text = string.Format("{0}/{1}", GameSystem.Service.PVPInfo.BP, ConstData.Tables.BPLimit);
			yield return new WaitForSeconds(1f);
		}
	}
}
