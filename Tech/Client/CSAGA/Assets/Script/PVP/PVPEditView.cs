﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PVPEditView : View {

	const int SKILL = 0;
	const int PROLOG = 1;

	public ItemList itemList;

	public PresetNavigator navigator;

	public GameObject skillButton;
	public GameObject prologButton;

	protected override void Initialize ()
	{
		if(Context.Instance.PVPWaves != null && Context.Instance.PVPWaves.IsSelf)
		{
			itemList.Setup(Context.Instance.PVPWaves.List);
		}
		else
		{
			Rebuild();
		}

		if(Context.Instance.PVPWaves != null)
		{
			if(Context.Instance.PVPWaves.EditState == PROLOG)
				OnEditProlog();
			else
				OnEditSkill();
		}

		base.Initialize ();
	}

	public override void OnShowEnd ()
	{
		base.OnShowEnd();

		if(Context.Instance.PVPWaves != null && Context.Instance.PVPWaves.IsChanged && Context.Instance.PVPWaves.IsSimulateComplete)
		{
			MessageBox.ShowConfirmWindow(ConstData.GetSystemText(SystemTextID._501_TIP_SAVE_CONFIRM), (index)=>{
				if(index == 0) 
					Context.Instance.PVPWaves.Save();
				else
				{
					Context.Instance.PVPWaves.IsSimulating = false;
					Context.Instance.PVPWaves.IsSimulateComplete = false;
				}
			});
		}

	}

	public void OnRerollSkill()
	{
		MessageBox.ShowConfirmWindow(ConstData.GetSystemText(SystemTextID._503_TIP_REROLL_PVPSKILL_CONFIRM), (index)=>{
			if(index == 0)
			{
				if(GameSystem.Service.ItemInfo.Count(ConstData.Tables.PVPSkillItemID) > 0)
					GameSystem.Service.PVPRerollSkill(Context.Instance.PVPWave.Wave);
				else
					MessageBox.Show(SystemTextID.TIP_ITEM_NOT_ENOUGH);
			}
		});

	}

	public void OnEditSkill()
	{
		skillButton.SetActive(false);
		prologButton.SetActive(true);
		Context.Instance.PVPWaves.EditState = SKILL;
		navigator.Next(SKILL);
	}

	public void OnEditProlog()
	{
		skillButton.SetActive(true);
		prologButton.SetActive(false);
		Context.Instance.PVPWaves.EditState = PROLOG;
		navigator.Next(PROLOG);
	}

	public void OnSimulate()
	{
		Message.Send(MessageID.PVPSimulate);
	}

	public void Rebuild()
	{
		var waves = PVPWaves.Create(GameSystem.Service.PVPInfo.Waves);
		waves.IsSelf = true;
		
		Context.Instance.PVPWaves = waves;
		Context.Instance.PVPWave = waves.List[0];
		
		itemList.Setup(waves.List);
	}
}
