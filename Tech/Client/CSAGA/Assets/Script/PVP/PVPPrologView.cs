﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PVPPrologView : PVPEditSubView {

	public ItemListBase itemList;

	#region implemented abstract members of PVPEditSubView
	
	protected override void Refresh (PVPWaveItemData data)
	{
		var list = new List<PVPPrologItemData>(data.Prologs.Count);
		for(int i = 0; i < data.Prologs.Count; ++i)
		{
			var obj = new PVPPrologItemData();
			obj.Wave = data;
			obj.Index = i;
			list.Add(obj);
		}

		itemList.Setup(list);
	}
	
	#endregion
	
	
}

