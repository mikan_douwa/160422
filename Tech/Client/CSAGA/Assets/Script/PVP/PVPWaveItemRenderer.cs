﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PVPWaveItemRenderer : ItemRenderer<PVPWaveItemData> {

	public SimpleToggle toggle;
	public UILabel wave;
	public UnitLoader card;

	#region implemented abstract members of ItemRenderer

	protected override void ApplyChange (PVPWaveItemData data)
	{
		card.CardID = data.Card.ID;
		card.Rare = data.Card.Rare;
		card.ApplyChange();

		wave.text = "WAVE" + (data.Wave + 1).ToString();
		toggle.value = (data == Context.Instance.PVPWave);

	}

	#endregion

	void OnClick()
	{
		Message.Send(MessageID.PlaySound, SoundID.CLICK);
		if(Context.Instance.PVPWave == data) return;

		Context.Instance.PVPWave = data;

		toggle.value = true;
		
		Message.Send(MessageID.SelectPVPWave);
	}

	void OnLongPress()
	{
		OnClick();
		Message.Send(MessageID.ChangePVPWaveCard);
	}
}
