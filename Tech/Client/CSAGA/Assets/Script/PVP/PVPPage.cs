﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PVPPage : Stage {

	public const int DEFAULT = 0;
	public const int EDIT = 1;
	public const int CARD_LIST = 2;
	public const int TOP_RANKING = 3;
	public const int MY_RANKING = 4;
	public const int RANKING_REWARD = 5;
	public const int SCORING_REWARD = 6;
	public const int BATTLE_PREPARE = 7;


	public PresetNavigator navigator;

	public VirtualCardTeamView teamView;

	private bool isSwitchPending = false;

	public override bool SwitchPrepare ()
	{
		if(isSwitchPending) return false;

		if(Context.Instance.PVPWaves != null && Context.Instance.PVPWaves.IsChanged && !Context.Instance.PVPWaves.IsSimulating)
		{
			isSwitchPending = true;
			MessageBox.ShowConfirmWindow(ConstData.GetSystemText(SystemTextID._501_TIP_SAVE_CONFIRM), (index)=>{
				if(index == 0) 
					Context.Instance.PVPWaves.Save();
				else
				{
					Context.Instance.PVPWaves = null;
					Context.Instance.PVPWave = null;
					isSwitchPending = false;
				}
			});

			return false;
		}

		return true;
	}

	protected override void Initialize ()
	{
		Context.Instance.CachedFellow = null;

		Menu.Show();

		Message.AddListener(MessageID.ChangePVPWaveCard, OnChangeWaveCard);
		Message.AddListener(MessageID.PVPSimulate, OnSimulate);
		Message.AddListener(MessageID.SelectPVPPlayer, OnSelectPlayer);

		GameSystem.Service.OnPVPInfo += OnPVPInfo;

		GameSystem.Service.GetRankingInfo();
		GameSystem.Service.GetPVPInfo();
	}

	protected override void OnDestroy ()
	{
		Message.RemoveListener(MessageID.ChangePVPWaveCard, OnChangeWaveCard);
		Message.RemoveListener(MessageID.PVPSimulate, OnSimulate);
		Message.RemoveListener(MessageID.SelectPVPPlayer, OnSelectPlayer);

		if(GameSystem.IsAvailable) 
		{
			GameSystem.Service.OnPVPInfo -= OnPVPInfo;
			GameSystem.Service.OnPVPQuery -= OnPVPQuery;
			GameSystem.Service.OnPVPUpdate -= OnPVPUpdate;
		}
		base.OnDestroy ();
	}

	protected override void OnInitializeComplete ()
	{
		if(GameSystem.Service.PVPInfo.Boss == null)
		{
			MessageBox.Show(SystemTextID.TIP_NOT_AVAILABLE).callback = (index)=>
			{
				Message.Send(MessageID.Back);
			};
		}
		else
		{

			if(Context.Instance.PVPWaves != null && Context.Instance.PVPWaves.IsSimulateComplete)
				navigator.Next(EDIT);
			else
				navigator.Next(DEFAULT);
		}
	}

	protected override void OnBack ()
	{
		if(teamView.Back()) return;

		switch(navigator.current)
		{
		case DEFAULT:
			base.OnBack();
			break;
		case CARD_LIST:
			navigator.Next(EDIT);
			break;
		case EDIT:
		{
			if(Context.Instance.PVPWaves != null && Context.Instance.PVPWaves.IsChanged)
			{

				MessageBox.ShowConfirmWindow(ConstData.GetSystemText(SystemTextID._501_TIP_SAVE_CONFIRM), (index)=>{
					if(index == 0) 
						Context.Instance.PVPWaves.Save();
					else
					{
						Context.Instance.PVPWaves = null;
						navigator.Next(DEFAULT);
					}
				});
			}
			else
				navigator.Next(DEFAULT);
			break;
		}
		case BATTLE_PREPARE:
			if(Context.Instance.PVPWaves.IsSimulating)
				navigator.Next(EDIT);
			else
				navigator.Next (DEFAULT);
			break;
		default:
			navigator.Next(DEFAULT);
			break;
		}
	}

	protected override void OnNext ()
	{
		if(teamView.Next()) return;

		switch(navigator.current)
		{
		case DEFAULT:
		case EDIT:
			var text = ConstData.GetSystemText(SystemTextID._488_TIP_PVP_RULE);
			var msgBox = MessageBox.Show("GameScene/Quest/EventQuestInfoMessageBox", text);
			msgBox.title = ConstData.GetSystemText(SystemTextID._487_PVP);
			msgBox.pivot = UIWidget.Pivot.Left;
			break;
		case RANKING_REWARD:
			navigator.Next(SCORING_REWARD);
			break;
		case SCORING_REWARD:
			navigator.Next(RANKING_REWARD);
			break;
		}
	}

	private void OnChangeWaveCard()
	{
		navigator.Next(CARD_LIST);
	}

	private void OnSimulate()
	{
		Context.Instance.PVPWaves.IsSimulating = true;
		QuestManager.Instance.PrepareQuest(Context.Instance.Quest);
		navigator.Next(BATTLE_PREPARE);
	}

	private void OnSelectPlayer()
	{
		Context.Instance.PVPWaves = PVPWaves.Create(Context.Instance.PVPPlayer.Origin.Waves);
		QuestManager.Instance.PrepareQuest(Context.Instance.Quest);
		navigator.Next(BATTLE_PREPARE);
	}

	void OnPVPInfo ()
	{
		GameSystem.Service.OnPVPInfo -= OnPVPInfo;

		if(GameSystem.Service.PVPInfo.Boss != null)
		{
			GameSystem.Service.OnPVPQuery += OnPVPQuery;
			GameSystem.Service.PVPQuery();
		}
		else
		{
			if(GameSystem.Service.CardInfo.List.Count < 5)
				InitializeComplete();
			else
			{
				GameSystem.Service.OnPVPUpdate += OnPVPUpdate;

				if(!GameSystem.Service.PVPInit ())
				{
					GameSystem.Service.OnPVPUpdate -= OnPVPUpdate;
					InitializeComplete();
				}
			}
		}
	}

	void OnPVPQuery ()
	{
		GameSystem.Service.OnPVPQuery -= OnPVPQuery;

		var list = new List<PVPPlayerItemData>();
		foreach(var item in GameSystem.Service.PVPInfo.Players)
		{
			var obj = new PVPPlayerItemData{ Origin = item };
			list.Add(obj);
		}
		Context.Instance.PVPPlayers = list;

		Context.Instance.EventScheduleID = 0;
		Context.Instance.MainQuest = null;
		
		if(GameSystem.Service.QuestInfo.PVP != null)
		{
			Context.Instance.EventScheduleID = GameSystem.Service.QuestInfo.PVP.EventID;
			
			foreach(var item in GameSystem.Service.QuestInfo.SelectLimited())
			{
				if(item.EventID != Context.Instance.EventScheduleID) continue;
				Context.Instance.MainQuest = QuestManager.Instance.GetSpecial(item.MainQuestID);
				
				break;
			}
		}
		
		if(Context.Instance.MainQuest == null)
			Context.Instance.MainQuest = QuestManager.Instance.GetSpecial(ConstData.Tables.PVPMainQuestID);
		
		if(Context.Instance.MainQuest != null)
			Context.Instance.Quest = QuestItemData.Create(Context.Instance.MainQuest.List[0]);

		GameSystem.Service.OnPVPUpdate += OnPVPUpdate;

		InitializeComplete();
	}
	
	void OnPVPUpdate ()
	{
		if(!IsInitialized)
		{
			GameSystem.Service.OnPVPUpdate -= OnPVPUpdate;
			InitializeComplete();
			return;
		}

		var isSimulateComplete = Context.Instance.PVPWaves.IsSimulateComplete;

		Context.Instance.PVPWaves = null;
		Context.Instance.PVPWave = null;

		if(isSwitchPending)
		{
			isSwitchPending = false;
			return;
		}

		if(isSimulateComplete)
		{
			var view = navigator.currentObj.GetComponent<PVPEditView>();
			if(view != null) view.Rebuild();
		}
		else if(navigator.current == EDIT)
			navigator.Next(DEFAULT);
	}
	
}

public class PVPWaves
{
	public List<PVPWaveItemData> List;
	public bool IsChanged;
	public bool IsSelf;
	public bool IsSimulating;
	public bool IsSimulateComplete;
	public bool IsOverdue;

	public int EditState = 0;

	public void Save()
	{
		if(!IsSelf) return;

		var list = new List<Mikan.CSAGA.Data.PVPWave>(List.Count);
		foreach(var item in List)
		{
			var wave = new Mikan.CSAGA.Data.PVPWave();
			wave.Card = item.Card;
			wave.Prologs = item.Prologs;
			wave.Selections = item.Selections;
			list.Add(wave);
		}
		GameSystem.Service.PVPUpdate(list);
	}

	public static PVPWaves Create(List<Mikan.CSAGA.Data.PVPWave> source)
	{
		var list = new List<PVPWaveItemData>(5);
		
		for(int i = 0; i < source.Count; ++i)
		{
			var wave = source[i];
			
			var obj = PVPWaveItemData.Create(wave);
			obj.Wave = i;
			
			list.Add(obj);
		}
		
		return new PVPWaves{ List = list };
	}
}

public class PVPWaveItemData
{
	public Mikan.CSAGA.Data.PVPWave Origin;

	public int Wave;
	
	#region Cache
	public Mikan.CSAGA.Data.Card Card;
	
	public List<int> Selections;
	public List<string> Prologs;
	#endregion
	
	public PVPMob Convert()
	{
		var mob = new PVPMob();
		var balance = ConstData.Tables.CardBalance[Wave + 1];
		
		var card = Card.Calc();
		card.Append(Card.SelectEquipment());
		
		mob.CardID = Card.ID;
		mob.HP = Mathf.FloorToInt(card.HP * (balance.n_PVP_HP / 100f));
		mob.Rev = Mathf.FloorToInt(card.Rev * (balance.n_PVP_REV / 100f));
		mob.Atk = Mathf.FloorToInt(card.Atk * (balance.n_PVP_ATK / 100f));
		mob.Charge = card.Charge;
		
		mob.Skills = new List<int>(3);
		foreach(var item in Selections) mob.Skills.Add(Origin.Skills[item]);
		
		mob.Prologs = Prologs;
		
		return mob;
	}
	
	public static PVPWaveItemData Create(Mikan.CSAGA.Data.PVPWave origin)
	{
		var obj = new PVPWaveItemData{ Origin = origin };
		obj.Card = GameSystem.Service.CardInfo[origin.Card.Serial];
		obj.Selections = new List<int>(origin.Selections);
		obj.Prologs = new List<string>(origin.Prologs);
		while(obj.Prologs.Count < 3) obj.Prologs.Add("");
		return obj;
	}
}

public class PVPMob
{
	public int CardID;
	public int HP;
	public int Atk;
	public int Rev;
	public float Charge;
	public List<int> Skills;
	public List<string> Prologs;
}