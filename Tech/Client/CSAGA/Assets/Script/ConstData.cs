﻿using UnityEngine;
using System.Collections;
using Mikan;
using Mikan.CSAGA.ConstData;
using System.Collections.Generic;

public class ConstData {


	public const string CONST_DATA = "CSAGA_CONSTDATA";
	public const string TEXT_DATA = "CSAGA_TEXTDATA";

	public static Tables Tables { get { return Tables.Instance; } }

	public static bool IsReady { get{ return Tables != null; } }

	public static string GetSystemText(SystemTextID id, params object[] args)
	{
		return GetSystemText((int)id, args);
	}

	public static string GetSystemText(int id, params object[] args)
	{
		return Tables.GetSystemText(id, args);
	}

	public static string GetChaperName(int no, Mikan.CSAGA.QuestType questType)
	{
		var prefix = questType == Mikan.CSAGA.QuestType.Regular ? 5000 : 5500;
		var text = GetSystemText(prefix + no);
		return text.Replace("\\n", "\n");
	}

	public static string GetQuestName(int id)
	{
		var data = ConstData.Tables.QuestName[id];
		return data != null ? data.s_MAIN_NAME : string.Format("QuestName({0})", id);
	}

	public static string GetSubQuestName(int id)
	{
		var data = ConstData.Tables.QuestName[id];
		return data != null ? data.s_SUB_NAME : string.Format("SubQuestName({0})", id);
	}

	public static string GetEventChoicText(int id, int index)
	{
		var data = Tables.EventShowText[id];
		if(data == null) return string.Format("ChoiceText({0})", id);
		return data.s_CHOICETEXT[index];
	}

	public static string GetEventName(int id)
	{
		var data = Tables.EventName[id];
		if(data == null) return string.Format("EventName({0})", id);
		return data.s_NAME;
	}

	public static string GetEventText(int id)
	{
		var data = Tables.EventShowText[id];
		if(data == null) return string.Format("EventText({0})", id);
		return data.s_TEXT.Replace("\n", "").Replace("\\n", "\n");
	}

	public static string GetCardName(int id)
	{
		if(id == 0) return "";
		var data = Tables.CardText[id];
		if(data == null) return string.Format("CARD({0})", id);
		return data.s_NAME;
	}

	public static string GetItemName(int id)
	{
		if(id == 0) return "";
		var data = Tables.ItemText[id];
		if(data == null) return string.Format("Item({0})", id);
		return data.s_NAME;
	}

	public static string GetItemTip(int id)
	{
		if(id == 0) return "";
		var data = Tables.ItemText[id];
		if(data == null) return string.Format("ItemTip({0})", id);
		if(string.IsNullOrEmpty(data.s_SP) || data.s_SP == "null") return "";
		return data.s_SP;
	}

	public static string GetItemTip2(int id)
	{
		if(id == 0) return "";
		var data = Tables.ItemText[id];
		if(data == null) return string.Format("ItemTip2({0})", id);
		if(string.IsNullOrEmpty(data.s_SP2) || data.s_SP2 == "null") return "";
		return data.s_SP2;
	}

	public static string GetChallengeText(int id)
	{
		var data = Tables.ChallengeText[id];
		if(data == null) return string.Format("Challenge({0})", id);
		if(string.IsNullOrEmpty(data.s_TEXT) || data.s_TEXT == "null") return "";
		return data.s_TEXT;
	}

	public static string GetCardPicPath(int id)
	{
		if(id == 0) return null;
		var data = Tables.Card[id];
		if(data == null)
		{
			Debug.LogWarning(string.Format("card not exists, id={0}", id));
			return "CARD/CARD_907_000";
		}
		return "CARD/CARD_" + data.s_FILENAME;
	}

	public static string GetGroupText(int type)
	{
		var textId = GetGroupTextID(type);

		if(textId != SystemTextID.NONE) return GetSystemText(textId);

		return string.Format("GROUP({0})", type);
	}

	public static SystemTextID GetGroupTextID(int type)
	{
		const int MAX = 9;
		const SystemTextID BEGIN = SystemTextID._16_GROUP1;
		
		for(int i = 0; i < MAX; ++i)
		{
			if(type == (int)Mathf.Pow(2, i)) return BEGIN + i;
		}
		return SystemTextID.NONE;
	}

	public static string GetTypeText(int type)
	{
		var textId = GetTypeTextID(type);
		
		if(textId != SystemTextID.NONE) return GetSystemText(textId);
		
		return string.Format("TYPE({0})", type);
	}
	
	public static SystemTextID GetTypeTextID(int type)
	{
		const int MAX = 9;
		const SystemTextID BEGIN = SystemTextID._301_TYPE1;
		
		for(int i = 0; i < MAX; ++i)
		{
			if(type == (int)Mathf.Pow(2, i)) return BEGIN + i;
		}
		return SystemTextID.NONE;
	}

	public static string GetSubjectTip(Mikan.CSAGA.ConstData.CoMission data)
	{
		return Format(Tables.MissionText[data.n_ID].s_TIP, GetItemName(data, 0), GetItemName(data, 1), GetCondition(data, 1));
	}

	private static string GetItemName(Mikan.CSAGA.ConstData.CoMission data, int index)
	{
		var id = data.n_ID_[index];
		
		switch(data.n_TYPE)
		{
		case Mikan.CSAGA.MissionType.Item:
			return GetItemName(id);
		case Mikan.CSAGA.MissionType.Gift:
		case Mikan.CSAGA.MissionType.Talk:
		case Mikan.CSAGA.MissionType.Quest2:
			return ConstData.GetCardName(id);
		case Mikan.CSAGA.MissionType.Quest1:
		default:
			return string.Format("ID({0})", id);
		}
	}
	
	private static string GetCondition(Mikan.CSAGA.ConstData.CoMission data, int index)
	{
		var id = data.n_CONDITION[index];
		
		switch(data.n_TYPE)
		{
		case Mikan.CSAGA.MissionType.Quest1:
		case Mikan.CSAGA.MissionType.Quest2:
			return GetSubQuestName(id);
		case Mikan.CSAGA.MissionType.Item:
		case Mikan.CSAGA.MissionType.Gift:
		case Mikan.CSAGA.MissionType.Talk:
		default:
			return string.Format("CONDITION({0})", id);
		}
	}

	public static string Format(string origin, params object[] args)
	{
		return Mikan.CSAGA.ConstData.Tables.Format(origin, args);
	}

	public static string GetSortMethodText(SortMethod method)
	{
		switch(method)
		{
		case SortMethod.HP: 	return GetSystemText(SystemTextID.HP);
		case SortMethod.Atk: 	return GetSystemText(SystemTextID.ATK);
		case SortMethod.Rev: 	return GetSystemText(SystemTextID.REV);
		case SortMethod.Chg: 	return GetSystemText(SystemTextID.CNG);
		case SortMethod.Group: 	return GetSystemText(SystemTextID.TYPE);
		case SortMethod.Kizuna: return GetSystemText(SystemTextID.KIZUNA);
		case SortMethod.Rare: 	return GetSystemText(SystemTextID.RARE);
		case SortMethod.Serial: return GetSystemText(SystemTextID.OBTAIN_TIME);
		case SortMethod.Type: 	return GetSystemText(SystemTextID.GROUP);
		case SortMethod.LV:		return GetSystemText(SystemTextID.LV);
		case SortMethod.Luck:	return GetSystemText(SystemTextID._199_LUCKY_EFFECT);
		case SortMethod.ID:		return GetSystemText(SystemTextID.BTN_CARD);
		case SortMethod.Counter01: return GetSystemText(SystemTextID._475_COUNTER_01);
		case SortMethod.Counter02: return GetSystemText(SystemTextID._476_COUNTER_02);
		case SortMethod.Counter04: return GetSystemText(SystemTextID._477_COUNTER_04);
		case SortMethod.Counter08: return GetSystemText(SystemTextID._478_COUNTER_08);
		case SortMethod.Counter16: return GetSystemText(SystemTextID._479_COUNTER_16);
		}
		return "(default)";
	}

	public static IEnumerator Reload(MonoBehaviour owner)
	{
		var reader = Mikan.CSAGA.ConstData.Tables.CreateReader();

		yield return owner.StartCoroutine(Load(owner, reader, CONST_DATA));
		
		yield return owner.StartCoroutine(Load(owner, reader, TEXT_DATA));

		yield return owner.StartCoroutine(AsyncWork.Execute(()=>{

			var provider = reader.Combine ();
			
			Tables.Create(provider);
			
			reader.Dispose();
		}));

#if PROFILE
		AsyncWork.TraceAndReset();
#endif
	}

	private static IEnumerator Load(MonoBehaviour owner, ConstDataReader reader, string filename)
	{
#if PATCH
		var path = Application.persistentDataPath + "/" + filename + ".unity";
		
		if(!System.IO.File.Exists(path)) 
			path = Application.streamingAssetsPath+"/"+filename+".pak";

#if UNITY_EDITOR_WIN
		path = "C://" + path;
#endif
#if !UNITY_EDITOR && UNITY_ANDROID
		else 
			path = "file://" + path;
#endif
		
#else
		var path = Application.streamingAssetsPath+"/"+filename+".pak";
#endif
		
#if !UNITY_EDITOR && UNITY_ANDROID
		using(var www = new WWW(path))
#else
		using(var www = new WWW("file://" + path))
#endif		
		{
			yield return www;
			
			if(string.IsNullOrEmpty(www.error))
			{
				if(www.assetBundle != null)
				{	
					var bytes = (www.assetBundle.mainAsset as TextAsset).bytes;

					yield return owner.StartCoroutine(AsyncWork.Execute(()=>reader.Load(new System.IO.MemoryStream(bytes))));

					www.assetBundle.Unload(true);
				}
				else
				{
					var bytes = www.bytes;

					yield return owner.StartCoroutine(AsyncWork.Execute(()=>reader.Load(new System.IO.MemoryStream(bytes))));
				}
			}
			else
				Debug.LogError(string.Format ("www load failed, {0}", www.error));	


		}
	}
	
	public static IEnumerable<string> SelectRelated(object data)
	{
		yield break;
	}

	public static IEnumerable<string> SelectRelated(Mikan.CSAGA.ConstData.CardData data)
	{
		yield return "Texture/CARD/CARD_" + data.s_FILENAME + ".unity";
		yield return "Texture/ICON/IC_" + data.s_FILENAME + ".unity";
		yield return "Texture/ICON/ICT_" + data.s_FILENAME + ".unity";

		yield break;
	}

	public static IEnumerable<string> SelectRelated(Mikan.CSAGA.ConstData.Interactive data)
	{
		foreach(var item in ConstData.Tables.EventShow.Select((o)=>{ return o.n_GROUP == data.n_CALL_EVENT; })) 
		{
			if(!string.IsNullOrEmpty(item.s_BG) && item.s_BG != "null")
				yield return "Texture/BG/" + item.s_BG + ".unity";

			if(!string.IsNullOrEmpty(item.s_BGM) && item.s_BGM != "null")
				yield return "Sound/BGM/" + item.s_BGM + ".unity";

			foreach(var cardId in item.n_CHARA)
			{
				if(cardId > 0) yield return "Texture/" + GetCardPicPath(cardId) + ".unity";
			}
		}
	}

	public static IEnumerable<string> SelectRelated(Mikan.CSAGA.ConstData.QuestEvent data)
	{
		foreach(var item in data.n_VALUE_)
		{
			if(item == 0) continue;
			var mob = Tables.Mob[item];
			yield return "Texture/CARD/CARD_" + mob.s_FILENAME + ".unity";
		}
	}
}

public class WWWProxy : System.IDisposable
{
	public WWW www;

	public WWWProxy(string url)
	{
		www = new WWW(url);
		//www.threadPriority = ThreadPriority.Low;
	}

	public bool IsDone { get { return www.isDone; } }

	#region IDisposable implementation

	public void Dispose ()
	{
		if(www != null) www.Dispose();
	}

	#endregion
}

public class CallbackTask : ITask
{
	public static List<string> Ticks = new List<string>();
	public Callback callback;

	public bool isDone;

	#region ITask implementation
	public void Execute ()
	{
		var beginTime = System.DateTime.Now;
		callback();
		var endTime = System.DateTime.Now;
		Ticks.Add(endTime.Subtract(beginTime).TotalMilliseconds.ToString());
		isDone = true;

	}
	#endregion
	#region IDisposable implementation
	public void Dispose ()
	{

	}
	#endregion
}