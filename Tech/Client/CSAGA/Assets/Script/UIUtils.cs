﻿using UnityEngine;
using System.Collections;
using System;

public static class UIUtils {

	private static Camera _defaultCamera;
	private static Camera defaultCamera 
	{
		get
		{
			if(_defaultCamera == null)
			{

				foreach(var item in GameObject.FindObjectsOfType<Camera>())
				{
					if(item.gameObject.layer != (int)UILayer.Background) continue;
					_defaultCamera = item;
					return _defaultCamera;
				}
				_defaultCamera = GameObject.FindObjectOfType<Camera>();
			}

			return _defaultCamera;

		}
	}
	public static void SetIdentity(GameObject parent, GameObject go)
	{
		AddChild(parent, go);
		go.layer = parent.layer;
		if(go.GetComponent<MultiLayer>() == null)
			NGUITools.SetChildLayer(go.transform, parent.layer);
	}

	public static void AddChild(GameObject parent, GameObject go)
	{
		Transform t = go.transform;
		t.parent = parent.transform;
		t.localPosition = Vector3.zero;
		t.localRotation = Quaternion.identity;
		t.localScale = Vector3.one;
	}

	public static void FitRoot(UIWidget widget)
	{
		var size = GetRootSize(widget.gameObject);

		widget.height = (int)size.y;
		widget.width = (int)size.x;
	}

	public static BoxCollider AddFullScreenBoxCllider(GameObject go)
	{
		var collider = go.AddMissingComponent<BoxCollider>();

		collider.size = GetRootSize(go);

		return collider;
	}

	public static Vector3 GetRootSize(GameObject go)
	{
		var root = go.GetComponentInParent<UIRoot>();
		var height = root.activeHeight;
		var width = height * (UIDefine.ScreenWidth/UIDefine.ScreenHeight);
		return new Vector3(width, height);
	}

	public static Camera FindCamera(int layer)
	{
		var cullingMask = (int)Mathf.Pow(2, layer);
		foreach(var item in GameObject.FindObjectsOfType<Camera>())
		{
			if((item.cullingMask & cullingMask) != 0) return item;
		}

		return null;
	}

	public static Camera CreateCamera(int layer, GameObject owner)
	{
		var go = new GameObject();
		var camera = go.AddComponent<Camera>();
		
		camera.name = string.Format("[AUTO] Caemra({0})", layer);
		camera.depth = layer;
		camera.orthographic = true;
		camera.orthographicSize = 1;

		if(layer == (int)UILayer.Background)
		{
			camera.clearFlags = CameraClearFlags.Color;
			camera.backgroundColor = Color.black;
		}
		else
			camera.clearFlags = CameraClearFlags.Nothing;

		camera.gameObject.layer = layer;
		camera.cullingMask = (int)Mathf.Pow(2, layer);
		camera.nearClipPlane = -10;
		camera.farClipPlane = 10;

		UICamera uicamera = camera.gameObject.AddComponent<UICamera>();
		uicamera.eventReceiverMask = camera.cullingMask;
		uicamera.allowMultiTouch = false;

		if(owner != null)	SetIdentity(owner, go);
		
		return camera;
	}

	public static bool HitTest(GameObject go,  Vector2 pos)
	{
		return HitTest(go.collider, pos);
	}

	public static bool HitTest(Collider collider,  Vector2 pos)
	{
		var camera = Camera.current;
		if(camera == null) camera = defaultCamera;

		if(collider == null || camera == null) return false;
		var ray = camera.ScreenPointToRay(pos);
		RaycastHit hit;
		return collider.Raycast(ray, out hit, 100f);
	}

	public static int GetStringByteCount(string val)
	{
		var arr = val.ToCharArray();

		var count = 0;

		for(int i = 0; i < arr.Length; ++i)
		{
			if(System.Text.Encoding.UTF8.GetByteCount(arr, i, 1) > 1)
				count += 2;
			else
				++count;
		}

		return count;
	}

	public static string RestrictPlayerName(string origin)
	{
		if(GetStringByteCount(origin) > 12)
			return RestrictPlayerName(origin.Substring(0, origin.Length - 1));

		return origin;
	}

	public static string FormatRemainTime(System.DateTime endTime, bool full = true)
	{
		if(endTime < GameSystem.Service.CurrentTime) 
			return "";
		else
		{
			var text = "";
			var timeSpan = endTime.Subtract(GameSystem.Service.CurrentTime);
			if(timeSpan.TotalDays >= 1)
				text = Mathf.FloorToInt((float)timeSpan.TotalDays).ToString() + ConstData.GetSystemText(SystemTextID.DAY);
			else if(timeSpan.TotalHours >= 1)
				text = Mathf.FloorToInt((float)timeSpan.TotalHours).ToString() + ConstData.GetSystemText(SystemTextID.HOUR);
			else
				text = Mathf.FloorToInt((float)timeSpan.TotalMinutes).ToString() + ConstData.GetSystemText(SystemTextID.MINUTE);

			if(full)
				return ConstData.GetSystemText(SystemTextID.REMAIN_TIME_V1, text);
			else
				return text;
		}
	}

	public static string FormatURL(string url)
	{
		if(url.Contains("?") && url.Contains("="))
			url += "&";
		else
			url += "?";

		return url + "token=" + Uri.EscapeDataString(GameSystem.Service.Token);
	}
}
