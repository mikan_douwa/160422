﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FaithPage : Stage {

	class EmitObj
	{
		public Item Item;
		public Mikan.ValuePair Diff;
		public bool IsFirst;
		public bool IsLast;
	}

	public ItemList itemList;

	public Item[] mtrlList;

	public FightResLoader fx;

	public GameObject emitObj;

	public UIProgressBar progressBar;

	public UIWidget claimTip;
	
	private float max;
	private float current;

	private bool allowClaim = false;

	protected override void Initialize ()
	{
		max = Mikan.CSAGA.Calc.Faith.MaterialCount(System.DateTime.MinValue, GameSystem.Service.CurrentTime, 0f);
		fx.Preload("BulletFX");

		GameSystem.Service.OnFaithUpdate += OnFaithUpdate;
		GameSystem.Service.OnFaithInfo += OnFaithInfo;
		GameSystem.Service.OnFaithEndProduce += OnFaithEndProduce;
		GameSystem.Service.OnFaithClaim += OnFaithClaim;
		GameSystem.Service.GetFaithInfo();
	}
	
	protected override void OnDestroy ()
	{
		if(GameSystem.IsAvailable)
		{
			GameSystem.Service.OnFaithUpdate -= OnFaithUpdate;
			GameSystem.Service.OnFaithInfo -= OnFaithInfo;
			GameSystem.Service.OnFaithEndProduce -= OnFaithEndProduce;
			GameSystem.Service.OnFaithClaim -= OnFaithClaim;
		}

		base.OnDestroy ();
	}

	void Update()
	{
		if(GameSystem.Service.FaithInfo == null) return;

		current = Mikan.CSAGA.Calc.Faith.MaterialCount(GameSystem.Service.FaithInfo.ClaimTime, GameSystem.Service.CurrentTime, 0f);

		var value = current/max;

		var deltaTime = Time.deltaTime;

		if(progressBar.value < value)
			progressBar.value += deltaTime;
		else if(progressBar.value > value)
			progressBar.value -= deltaTime;
		else
			return;

		if(Mathf.Abs(progressBar.value - value) < deltaTime)
			progressBar.value = value;

		var _allowClaim = current >= 1;

		if(_allowClaim == allowClaim) return;

		allowClaim = _allowClaim;

		TweenAlpha.Begin(claimTip.gameObject, 0.5f, allowClaim ? 1f : 0f);
	}
	
	void OnClick()
	{
		Update();

		if(!allowClaim) return;
		
		Message.Send(MessageID.PlaySound, SoundID.BATTLE_HEAL);
		GameSystem.Service.FaithClaim();
	}
	
	void OnFaithClaim (Dictionary<int, Mikan.ValuePair> diff)
	{
		if(diff == null) return;

		var list = new List<Queue<EmitObj>>(4);

		for(int i = 0; i < ConstData.Tables.FaithItemIDs.Count; ++i)
		{
			var itemId = ConstData.Tables.FaithItemIDs[i];

			var item = default(Mikan.ValuePair);

			if(diff.TryGetValue(itemId, out item)) list.Add(Generate(mtrlList[i], item)); 
		}

		StartCoroutine(Emit (list));

	}

	private Queue<EmitObj> Generate(Item item, Mikan.ValuePair diff)
	{
		var queue = new Queue<EmitObj>();

		var current = diff.OriginValue;

		while(true)
		{
			var next = Mathf.Min(current + 10, diff.NewValue);

			var obj = new EmitObj{ Item = item, Diff = Mikan.ValuePair.Create(current, next) };

			if(queue.Count == 0) obj.IsFirst = true;

			queue.Enqueue(obj);

			if(next >= diff.NewValue) 
			{
				obj.IsLast = true;
				break;
			}

			current = next;
		}

		return queue;
	}

	private IEnumerator Emit(List<Queue<EmitObj>> list)
	{
		while(list.Count > 0)
		{
			var index = Mikan.MathUtils.Random.Next(list.Count);

			var queue = list[index];

			var obj = queue.Dequeue();

			if(queue.Count == 0) list.Remove(queue);

			StartCoroutine(UpdateCount(obj));

			fx.FlyFX("BulletFX", new Vector3(Mikan.MathUtils.Random.Next(-80,80), Mikan.MathUtils.Random.Next(30,70)), gameObject, obj.Item.gameObject, 0.3f, false);
			yield return new WaitForSeconds(0.03f + (float)(Mikan.MathUtils.Random.NextDouble() * 0.06f));
		}

		yield return new WaitForSeconds(0.3f);
		Refresh();
	}

	private IEnumerator UpdateCount(EmitObj obj)
	{
		if(obj.IsFirst) yield return new WaitForSeconds(0.3f);

		obj.Item.count.fontSize = 28;
		obj.Item.count.color = Color.yellow;

		while(true)
		{
			var origin = int.Parse(obj.Item.count.text);

			if(origin < obj.Diff.NewValue)
			{
				obj.Item.count.text = (origin + 1).ToString();
				yield return new WaitForSeconds(0.025f);
			}
			else
				break;
		}

		if(obj.IsLast)
		{
			yield return new WaitForSeconds(0.1f);

			obj.Item.count.fontSize = 24;
			obj.Item.count.color = Color.white;
			obj.Item.ApplyChange();
		}

	}

	void OnFaithEndProduce(int dropId)
	{
		QuestDrop.Show(dropId);
	}

	void OnFaithInfo ()
	{
		Refresh();

		InitializeComplete();
	}

	void OnFaithUpdate ()
	{
		Refresh();
	}

	private void Refresh()
	{
		var list = new List<FaithItemData>(4);
		
		for(int i = 0; i < 4; ++i)
		{
			var obj = new FaithItemData{ Index = i };
			
			obj.Origin = GameSystem.Service.FaithInfo.List.Find(o=>o.Index == i);
			
			list.Add(obj);
		}
		
		itemList.Setup(list);

		for(int i = 0; i < mtrlList.Length; ++i)
		{
			var itemId = ConstData.Tables.FaithItemIDs[i];
			var item = mtrlList[i];
			item.ItemID = itemId;
			item.Count = -1;
			item.ApplyChange();
		}
	}
}
