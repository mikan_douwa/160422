﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FaithProduceWindow : SubmitWindow {

	public Item[] mtrlList;

	public UILabel itemName;

	public UILabel range;

	public UILabel time;

	public UISlider slider;

	private List<int> counts = new List<int>();

	private int current = 0;

	private int count = 0;

	protected override void OnReady ()
	{
		for(int i = 0 ;i < 4; ++i)
		{
			var itemId = ConstData.Tables.FaithItemIDs[i];
			var item = mtrlList[i];
			item.ItemID = itemId;
			item.CountFormat = "0";
			item.ApplyChange();

			counts.Add(0);
		}

		Message.AddListener(MessageID.SelectItem, OnSelectItem);
		Message.AddListener(MessageID.Increase, OnIncrease);

		UpdateItemName(0);
		UpdateTime();
	}

	protected override bool OnSubmit ()
	{
		foreach(var item in counts)
		{
			if(item > 0) 
			{
				GameSystem.Service.FaithBeginProduce(Context.Instance.Faith.Index, counts);
				return true;
			}
		}

		return false;
	}

	protected override void OnDestroy ()
	{
		Message.RemoveListener(MessageID.SelectItem, OnSelectItem);
		Message.RemoveListener(MessageID.Increase, OnIncrease);

		base.OnDestroy ();
	}

	public void OnSliderValueChange()
	{
		if(isRecrusive) return;

		UpdateCount((int)(slider.value * ItemCount));
	}

	private void OnSelectItem(IMessage msg)
	{
		current = (int)msg.Data;

		isRecrusive = true;
		slider.value = (float)counts[current] / (float)ItemCount;
		isRecrusive = false;

		UpdateItemName(current);
	}

	private void OnIncrease(IMessage msg)
	{
		var param = (int)msg.Data;

		UpdateCount(count + param);
	}

	private bool isRecrusive = false;

	private void UpdateItemName(int index)
	{
		var itemId = ConstData.Tables.FaithItemIDs[index];
		itemName.text = ConstData.GetItemName(itemId);
		range.text = string.Format("(0~{0})",Mathf.Min(ConstData.Tables.FaithMax, GameSystem.Service.ItemInfo.Count(itemId)));
	}

	private void UpdateCount(int count)
	{
		this.count = Mathf.Min(ConstData.Tables.FaithMax, Mathf.Max(0, count));
		
		counts[current] = this.count;

		var item = mtrlList[current];

		if(this.count == 0)
		{
			item.CountFormat = "0";
		}
		else
		{
			item.CountFormat = "";
			item.Count = this.count;
		}

		item.ApplyChange();

		UpdateTime();
		
		isRecrusive = true;
		slider.value = (float)count / (float)ItemCount;
		isRecrusive = false;
	}

	private int ItemCount
	{
		get { return Mathf.Min(ConstData.Tables.FaithMax, GameSystem.Service.ItemInfo.Count(ConstData.Tables.FaithItemIDs[current])); }
	}

	private void UpdateTime()
	{
		var zero = true;
		foreach(var item in counts)
		{
			if(item == 0) continue;
			zero = false;
			break;
		}

		if(zero)
		{
			time.text = string.Format("{0} --:--", ConstData.GetSystemText(SystemTextID._470_FAITH_EXPECT_TIME));
			return;
		}


		var data = ConstData.Tables.FaithResult.SelectFirst((o)=> {
			for(int i = 0; i < 4; ++i)
			{
				var count = counts[i];
				var min = o.n_ITEMMIN[i];
				var max = o.n_ITEMMAX[i];
				if((min > 0 && count < min) || (max > 0 && count > max)) return false;
			}

			return true;
		});

		if(data == null) data = ConstData.Tables.FaithResult[1];

		var hour = data.n_TIME / 60;
		var minute = data.n_TIME % 60;
		time.text = string.Format("{0} {1:00}:{2:00}", ConstData.GetSystemText(SystemTextID._470_FAITH_EXPECT_TIME), hour, minute);
	}
}
