﻿using UnityEngine;
using System.Collections;

public class FaithItemRenderer : ItemRenderer<FaithItemData> {

	public UISprite background;

	public GameObject lckObj;

	public RewardIcon icon;

	public UILabel label;

	private bool needUpdate = false;

	private bool isLocked = false;
	private bool isFinished = false;

	#region implemented abstract members of ItemRenderer

	protected override void ApplyChange (FaithItemData data)
	{
		icon.icon.baseDepth = icon.GetComponent<UIWidget>().depth;

		var threshold = ConstData.Tables.FaithThresholds[data.Index];

		isFinished = false;
		isLocked = GameSystem.Service.PlayerInfo.LV < threshold;
		                                  
		lckObj.SetActive(isLocked);
		needUpdate = false;

		if(isLocked)
		{
			background.spriteName = "IC_UNDER";
			icon.Hide();
			label.text = ConstData.GetSystemText(SystemTextID._468_FAITH_RANK_V1, threshold);
		}
		else if(data.Origin == null || data.Origin.Serial == 0)
		{
			background.spriteName = "IC_UNDER";
			icon.Hide();
			label.text = ConstData.GetSystemText(SystemTextID._465_FAITH_EMPTY);
		}
		else if(data.Origin.EndTime > GameSystem.Service.CurrentTime)
		{
			background.spriteName = "IC_Q";
			icon.Hide();
			needUpdate = true;
			Update ();
		}
		else
		{
			ShowResult();
		}
	}

	#endregion

	private void ShowResult()
	{
		isFinished = true;
		background.spriteName = "IC_UNDER";
		var drop = ConstData.Tables.Drop[data.Origin.Result];
		if(drop != null) icon.ApplyChange(drop);
		label.text = ConstData.GetSystemText(SystemTextID._467_FAITH_FINISH);
	}

	void OnClick()
	{
		Context.Instance.Faith = data;

		if((data.Origin == null || data.Origin.Serial == 0) && !isLocked)
			MessageBox.ShowConfirmWindow("GameScene/Faith/FaithProduceWindow", "", null);
		else if(isFinished)
			GameSystem.Service.FaithEndProduce(data.Origin.Serial);

	}

	void Update()
	{
		if(!needUpdate) return;

		if(data.Origin.EndTime > GameSystem.Service.CurrentTime)
		{
			var diff = data.Origin.EndTime.Subtract(GameSystem.Service.CurrentTime);

			var time = "";
			if(diff.Hours > 0)
				time = UIUtils.FormatRemainTime(data.Origin.EndTime, false);
			else
				time = string.Format("{0:00}:{1:00}", diff.Minutes, diff.Seconds);
			label.text = ConstData.GetSystemText(SystemTextID._466_FAITH_BUSY) + "\n" + time;
		}
		else
		{
			needUpdate = false;
			ShowResult();
		}
	}

}

public class FaithItemData
{
	public int Index;
	public Mikan.CSAGA.Data.Faith Origin;

}
