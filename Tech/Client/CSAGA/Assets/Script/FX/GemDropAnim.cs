﻿using UnityEngine;
using System.Collections;

public class GemDropAnim : MonoBehaviour {

	public FXController light;
	public AnimationCurve dropCurve;

	// Use this for initialization
	void Start () {
		Go();
	}

	void Go()
	{
		//Animation.
		transform.localPosition = new Vector3(0f, 1100f - 480f, 0f);
		TweenY ty = TweenY.Begin(gameObject, 1f, 240f - 480f);
		ty.ignoreTimeScale = false;
		ty.animationCurve = dropCurve;
		EventDelegate.Set(ty.onFinished, DropEnd);
		
		NGUITools.SetActive(light.gameObject, false);
	}

	void DropEnd()
	{
		NGUITools.SetActive(light.gameObject, true);
		light.Play(null);
	}


	// Update is called once per frame
	void Update () {
	
	}
}
