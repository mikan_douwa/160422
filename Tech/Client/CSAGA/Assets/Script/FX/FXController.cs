﻿using UnityEngine;
using System.Collections;

public class FXController : MonoBehaviour {

	//Data
	public string FXName;
	public float AutoRecycleTime = 0f;
	public bool RandomRotate = false;
	//Reference
	public FightResLoader Loader;
	public GameObject[] ActiveArray;
	public UITweener[] tweenerArray;
	public ParticleSystem[] particleArray;
	public GameObject trailPrefab;
	public AnimationCurve flyCurve;
	public AnimationCurve flyCurveInverse;

	GameObject trailObj;

	public void Play(FightResLoader loader)
	{
		Loader = loader;
		for(int i = 0; i < ActiveArray.Length; i++)
		{
			NGUITools.SetActive(ActiveArray[i], true);
		}
		for(int i = 0; i < tweenerArray.Length; i++)
		{
			UITweener[] tweeners = tweenerArray[i].GetComponents<UITweener>();
			for(int j = 0; j < tweeners.Length; j++)
			{
				UITweener ut = tweeners[j];
				ut.ResetToBeginning();
				ut.enabled = true;
			}
		}
		for(int i = 0; i < particleArray.Length; i++)
		{
			ParticleSystem particle = particleArray[i];
			particle.Play();
		}

		//Auto Recycle
		if(AutoRecycleTime > 0)
			StartCoroutine(AutoRecycleAfterSec());
		//Random Rotate
		if(RandomRotate)
			transform.localRotation = Quaternion.Euler(0, 0, Mikan.MathUtils.Random.Next(0, 360));
	}

	public void Fly(float duration, bool upToDown)
	{
		TweenX tx = TweenX.Begin(gameObject, duration, 0f);
		tx.ignoreTimeScale = false;
		EventDelegate.Add(tx.onFinished, SelfRecycle, true);

		TweenY ty = TweenY.Begin(gameObject, duration, 0f);
		ty.ignoreTimeScale = false;
		if(upToDown)
			ty.animationCurve = flyCurveInverse;
		else
			ty.animationCurve = flyCurve;

		//Add Trail
		if(trailPrefab != null)
		{
			trailObj = NGUITools.AddChild(gameObject, trailPrefab);
			trailObj.transform.localPosition = new Vector3(0, 0, 1);
		}
	}

	public void SelfRecycle()
	{
		if(Loader == null)
			return;

		//Stop particle
		for(int i = 0; i < particleArray.Length; i++)
		{
			ParticleSystem particle = particleArray[i];
			particle.Stop();
		}

		if(trailObj != null)
		{
			Destroy(trailObj);
		}

		Loader.RecycleFX(this);
	}

	private IEnumerator AutoRecycleAfterSec()
	{
		yield return new WaitForSeconds(AutoRecycleTime);
		SelfRecycle();
	}
}
