﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class FightResLoader : MonoBehaviour {

	public GameObject fxPanel;

	//FX
	private Dictionary<string, IAsset<GameObject>> FXResourceDict = new Dictionary<string, IAsset<GameObject>>();
	private Dictionary<string, Queue<FXController>> FXCacheDict = new Dictionary<string, Queue<FXController>>();
	//Texture preload
	private Dictionary<string, IAsset<Texture2D>> TextureDict = new Dictionary<string, IAsset<Texture2D>>();


	private int FXTotalCount;
	private int FXCompletedCount;
	private int TexTotalCount;
	private int TexCompletedCount;
	private Action LoadCallback;

	public void Preload(string fx)
	{
		BatchLoad(()=>{}, new List<string>(new string[]{ fx }), new List<string>());
	}

	public void BatchLoad(Action callback, List<string> lstFX, List<string> lstTex)
	{
		Debug.Log("FightResLoader BatchLoad");
		LoadCallback = callback;
		FXTotalCount = lstFX.Count;
		TexTotalCount =	lstTex.Count;
		FXCompletedCount = 0;
		TexCompletedCount = 0;

		if(FXTotalCount == 0)
			StartCoroutine(AllComplete());
		else
		{
			//FX
			for(int i = 0; i < lstFX.Count; i++)
			{
				string name = lstFX[i];
				if(!FXResourceDict.ContainsKey(name))
				{
					IAsset<GameObject> asset = ResourceManager.Instance.LoadPrefab("FX/" + name, LoadFXComplete);
					FXResourceDict.Add(name, asset);
					FXCacheDict.Add(name, new Queue<FXController>());
				}
				else
					LoadFXComplete(null);
			}
		}
		//Texture
		for(int i = 0; i < lstTex.Count; i++)
		{
			string name = lstTex[i];
			if(!TextureDict.ContainsKey(name))
			{
				IAsset<Texture2D> asset = ResourceManager.Instance.LoadTexture(name, LoadCardComplete);
				TextureDict.Add(name, asset);
			}
			else
				LoadCardComplete(null);
		}

	}

	private void LoadFXComplete(IAsset<GameObject> asset)
	{
		FXCompletedCount++;

		if(FXCompletedCount == FXTotalCount)
		{
			Debug.Log("FightResLoader Load FX Complete");
			//load 1 child to cache queue for every prefab.
			foreach(KeyValuePair<string, IAsset<GameObject>> pair in FXResourceDict)
			{
				GameObject prefab = pair.Value.Content;
				if(prefab != null)
				{
					FXController ctrl = NGUITools.AddChild(gameObject, prefab).GetComponent<FXController>();
					NGUITools.SetActive(ctrl.gameObject, false);
					ctrl.FXName = pair.Key;
					if(FXCacheDict.ContainsKey(ctrl.FXName))
					{
						FXCacheDict[ctrl.FXName].Enqueue(ctrl);
					}
					else
					{
						Queue<FXController> queue = new Queue<FXController>();
						queue.Enqueue(ctrl);
						FXCacheDict.Add(ctrl.FXName, queue);
					}
				}
			}

			StartCoroutine(AllComplete());
		}
	}

	private void LoadCardComplete(IAsset<Texture2D> asset)
	{
		TexCompletedCount++;
	}

	IEnumerator AllComplete()
	{
		while(TexCompletedCount < TexTotalCount)
		{
			yield return 0;
		}

		LoadCallback();
	}

	public FXController PlayFX(string fxName, Vector3 pos, GameObject parent)
	{
		GameObject root = parent;
		if(root == null)
			root = fxPanel;

		FXController fx = GetFX(fxName);
		if(fx != null)
		{
			fx.transform.parent = root.transform;
			fx.transform.localPosition = pos;
			NGUITools.SetActive(fx.gameObject, true);
			fx.Play(this);
		}
		return fx;
	}

	public FXController FlyFX(string fxName, Vector3 pos, GameObject parent, GameObject target, float duration, bool upToDown)
	{
		GameObject root = parent;
		if(root == null)
			root = fxPanel;

		FXController fx = GetFX(fxName);
		if(fx != null)
		{
			fx.transform.parent = root.transform;
			fx.transform.localPosition = pos;
			fx.transform.parent = target.transform;
			NGUITools.SetActive(fx.gameObject, true);
			fx.Play(this);
			fx.Fly(duration, upToDown);
		}
		return fx;
	}

	FXController GetFX(string fxName)
	{
		if(FXResourceDict.ContainsKey(fxName))
		{
			Queue<FXController> cacheQueue = FXCacheDict[fxName];

			//get cache object
			if(cacheQueue.Count > 0)
			{
				return cacheQueue.Dequeue();
			}
			else
			{
				//no cache, addchild new one.
				GameObject prefab = FXResourceDict[fxName].Content;
				if(prefab != null)
				{
					FXController ctrl = NGUITools.AddChild(gameObject, prefab).GetComponent<FXController>();
					ctrl.FXName = fxName;
					return ctrl;
				}
				else
					Debug.Log("FX " + fxName + " is null");
			}
		}
		else
		{
			Load(fxName);
		}

		return null;
	}

	void Load(string fxName)
	{
		if(!FXResourceDict.ContainsKey(fxName))
		{
			IAsset<GameObject> asset = ResourceManager.Instance.LoadPrefab("FX/" + fxName);
			FXResourceDict.Add(fxName, asset);
			//empty cache queue
			FXCacheDict.Add(fxName, new Queue<FXController>());
		}
	}

	public void RecycleFX(FXController ctrl)
	{
		NGUITools.SetActive(ctrl.gameObject, false);
		ctrl.transform.parent = transform;
		Queue<FXController> cacheQueue = FXCacheDict[ctrl.FXName];
		cacheQueue.Enqueue(ctrl);
	}

	public Texture2D GetTexture(string name)
	{
		Texture2D texture = null;
		if(TextureDict.ContainsKey(name))
			texture = TextureDict[name].Content;

		return texture;
	}

	//Release 
	void OnDestroy()
	{
		foreach(IAsset<Texture2D> asset in TextureDict.Values)
		{
			asset.DecreaseReferenceCount();
		}
	}
}
