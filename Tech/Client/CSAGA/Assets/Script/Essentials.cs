﻿using UnityEngine;
using System.Collections;

public class Essentials : MonoBehaviour {

	public static Essentials Instance { get; private set; }

	public GameObject root;

	public GameObject nextButtonPrefab;

	public GameObject sceneTranslation;

	public GameObject sortButton;

	public GameObject loading;

	public GameObject mask;

	public GameObject unit;

	public GameObject button;

	// Use this for initialization
	void Awake () {

		Instance = this;

		DontDestroyOnLoad(gameObject);
	}
}
