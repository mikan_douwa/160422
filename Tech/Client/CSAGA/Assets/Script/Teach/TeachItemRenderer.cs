﻿using UnityEngine;
using System.Collections;

public class TeachItemRenderer : ItemRenderer<TeachContent> {

	protected override void ApplyChange (TeachContent data)
	{
		UITexture tex = GetComponent<UITexture>();
		if(tex != null)
		{
			if(data.texture != null)
				tex.mainTexture = data.texture;
			else
				ResourceManager.Instance.LoadTexture("TUTORIAL/" + data.path, tex);
		}
	}

}
