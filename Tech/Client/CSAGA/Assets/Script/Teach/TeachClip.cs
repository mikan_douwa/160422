﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class TeachContent
{
	public string text;
	public Texture2D texture;
	public string path;

	public TeachContent(string word)
	{
		text = word;
		texture = null;
	}
	public TeachContent(string word, Texture2D tex)
	{
		text = word;
		texture = tex;
	}
	public TeachContent(string word, string pic)
	{
		text = word;
		texture = null;
		path = pic;
	}
}

public class TeachClip : MonoBehaviour {

	public GameObject blocker;
	public GameObject closeBtn;
	public Navigator navigator;
	public GameObject leftArrow;
	public GameObject rightArrow;

	Action closeCallback = null;

	void Awake()
	{
		UIEventListener.Get(closeBtn).onClick = Close;
		UIEventListener.Get(leftArrow).onClick = Prev;
		UIEventListener.Get(rightArrow).onClick = Next;
		navigator.OnCurrentChange += OnChange;
	}

	public void Init(List<TeachContent> lst, Action callback)
	{
		closeCallback = callback;
		navigator.current = 0;
		navigator.Setup(lst);

		NGUITools.SetActive(leftArrow, false);
		NGUITools.SetActive(rightArrow, lst.Count > 1);
	}

	void Close(GameObject btn)
	{
		if(closeCallback != null)
			closeCallback();

		Destroy(gameObject);
	}

	void Prev(GameObject btn)
	{
		navigator.Prev();
	}

	void Next(GameObject btn)
	{
		navigator.Next();
	}

	void OnChange(Navigator nav)
	{
		int count = nav.DataProvider.Count;
		if(count > 1)
		{
			NGUITools.SetActive(leftArrow, nav.current > 0);
			NGUITools.SetActive(rightArrow, nav.current < count - 1);
		}
	}
}
