﻿using UnityEngine;
using System.Collections;

public enum MessageID {
	
	None = 0,

	SwitchStage,

	ShowMessageBox,

	PlaySound,
	PlayMusic,

	ShowHeadline,
	HideHeadline,
	Back,
	Next,
	Confirm,
	Cancel,
	SelectItem,

	AppendStage,
	RemoveStage,

	Sort,

	SelectedChange,

	Increase,

	PhysicalBack,

	NetFail,

	CancelSelect,

	SortMethod,

	RefreshTactics,

	EnableGesture = 100,
	DisableGesture,
	GestureTap,
	GestureSwip,
	GestureLongPress,
	GestureDragBegin,
	GestureDragMoved,
	GestureDragEnd,

	DownloadBegin = 200,
	DownloadEnd,
	DownloadProgress,

	NextChapter = 300,

	SwitchTeam = 400,
	SwitchTeamMember,
	SelectTeamMember,
	EditTeamConfirm,
	SelectSupport,
	ConfirmSupport,

	PlayEvent = 500,
	CharaTalk,
	CharaGift,
	CharaEvent,
	EndEvent,

	PrepareQuest = 600,
	BeginQuest,
	EndQuest,
	PlayQuestSettlement,
	SkipQuestSettlement,
	NextDifficulty,
	ShowQuestView,
	ShowChallengeReward,
	SwicthChallengeInfo,
	SwicthChallengeInfoEnd,

	EquipConfirm = 700,

	DrawGacha = 800,

	IAP = 900,
	ExpandCardSpace,
	ChargeTalkPoint,
	Continue,
	ChargeQuestCount,
	RandomShop,
	RandomShopRefresh,
	RandomShopExpand,
	ChargeRescuePoint,
	ChargeCrusadePoint,

	SystemSetting = 1000,
	SoundVolume,
	MusicVolume,

	BattleSetting = 1100,
	BattleState,
	BattleSpeed,
	BattleSkillCheck,
	BattleAuto,
	BattleCancel,
	BattleFX,

	Favorite = 1200,

	ClaimAllGift = 1300,

	NextScene = 1400,

	SelectPVPWave = 1500,
	ChangePVPWaveCard,
	SelectPVPSkill,
	PVPSimulate,
	SelectPVPPlayer,
}