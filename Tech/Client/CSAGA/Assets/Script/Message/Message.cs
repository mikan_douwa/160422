﻿using UnityEngine;
using System;
using System.Collections.Generic;

public class Message {

	private static List<Listener> EmptyList = new List<Listener>();

	private static Dictionary<MessageID, LinkedList<Listener>> listeners;
	private static Dictionary<object, Serial> serials;
	private static Dictionary<MessageID, int> senders;

	static Message()
	{
		Reset();
	}

	private static Serial NextSerial(object obj)
	{
		var serial = GetSerial(obj);

		if(serial == null)
		{
			serial = new Serial{ Obj = obj };
			serials.Add(obj, serial);
		}

		++serial.ReferenceCount;

		return serial;
	}

	private static Serial GetSerial(object obj)
	{
		Serial serial = null;

		if(serials.TryGetValue(obj, out serial)) return serial;

		return null;
	}

	public static void Reset()
	{
		listeners = new Dictionary<MessageID, LinkedList<Listener>>();
		serials = new Dictionary<object, Serial>();
		senders = new Dictionary<MessageID, int>();
	}

	public static bool IsLastSender(object obj, MessageID id)
	{
		var hashCode = default(int);
		if(!senders.TryGetValue(id, out hashCode)) return false;
		return obj.GetHashCode() == hashCode;
	}

	public static void Send(object sender, MessageID id)
	{
		Send(sender, id, null);
	}

	public static void Send(object sender, MessageID id, object data)
	{
		Send(sender, new SimpleMessage(id, data));
	}

	public static void Send(MessageID id)
	{
		Send(null, id);
	}

	public static void Send(MessageID id, object data)
	{
		Send (null, id, data);
	}

	private static LinkedList<Listener> GetListenerList(MessageID id)
	{
		LinkedList<Listener> list = null;
		
		if(listeners.TryGetValue(id, out list)) return list;
		
		return null;
	}

	public static void Send(object sender, IMessage msg)
	{
		senders[msg.MessageID] = sender != null ? sender.GetHashCode() : 0;

		var list = GetListenerList(msg.MessageID);
		
		var copy = list != null ? new List<Listener>(list) : EmptyList;
		
		foreach(var listener in copy)
		{
			if(listener.IsAvailable)
			{
				if(listener.OneShot) 
				{
					list.Remove(listener);
					listener.IsAvailable = false;
				}
				
				listener.Send(msg);
			}
			
		}
	}

	public static void Send(IMessage msg)
	{
		Send(null, msg);
	}

	public static void AddListener(MessageID id, Action<IMessage> callback, bool oneShot = false)
	{
		var listener = new ParamedListener{ Serial = NextSerial(callback), Callback = callback, OneShot = oneShot };
		AddListener(id, listener);
	}

	public static void AddListener(MessageID id, Action callback, bool oneShot = false)
	{
		var listener = new SimpleListener{ Serial = NextSerial(callback), Callback = callback, OneShot = oneShot };
		AddListener(id, listener);
	}

	private static void AddListener(MessageID id, Listener listener)
	{
		var list = GetListenerList(id);

		if(list == null)
		{
			list = new LinkedList<Listener>();
			listeners.Add(id, list);
		}

		list.AddLast(listener);
	}

	public static void RemoveListener(MessageID id, Action<IMessage> callback)
	{
		RemoveListener(id, GetSerial(callback));
	}

	public static void RemoveListener(MessageID id, Action callback)
	{
		RemoveListener(id, GetSerial(callback));
	}

	private static void RemoveListener(MessageID id, Serial serial)
	{
		var list = GetListenerList(id);

		if(list == null) return;

		Listener listener = null;
		foreach(var item in list)
		{
			if(item.Serial != serial) continue;
			listener = item;
			break;
		}

		if(listener != null)
		{
			listener.IsAvailable = false;
			list.Remove(listener);
			if(--serial.ReferenceCount == 0) serials.Remove(serial.Obj);
		}
	}

	class SimpleMessage : IMessage
	{
		public MessageID MessageID { get; private set; }
		public object Data { get; private set; }
		public SimpleMessage(MessageID id, object data)
		{
			MessageID = id;
			Data = data;
		}
	}

	#region Listener

	class Serial
	{
		public object Obj;
		public int ReferenceCount;
	}

	abstract class Listener
	{
		public Serial Serial;
		public bool OneShot = false;
		public bool IsAvailable = true;
		abstract public void Send (IMessage message);
	}

	class SimpleListener : Listener
	{
		public Action Callback;
		#region implemented abstract members of MessageSender

		public override void Send (IMessage message)
		{
			Callback();
		}
		#endregion
	}
	
	class ParamedListener : Listener
	{
		public Action<IMessage> Callback;
		#region implemented abstract members of MessageSender

		public override void Send (IMessage message)
		{
			Callback(message);
		}

		#endregion
	}

	#endregion

}

public class DownloadProgressMessage : IMessage
{
	public float Progress;
	
	#region IMessage implementation
	
	public MessageID MessageID { get { return MessageID.DownloadProgress; }	}
	
	public object Data { get { return Progress; } }
	
	#endregion
	
	
}