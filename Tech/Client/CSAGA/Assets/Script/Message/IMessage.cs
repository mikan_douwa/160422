﻿using UnityEngine;
using System.Collections;

public interface IMessage 
{
	MessageID MessageID { get; }
	object Data { get; }
}