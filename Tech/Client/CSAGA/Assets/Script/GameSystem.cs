﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using Mikan.CSAGA;

public class GameSystem : MonoBehaviour {

	private static GameSystem instance;

	private static bool isDisposing = false;

	private static bool isApplicationQuit;
	
	public static Mikan.IConfig Config { get; private set; }

	public static bool IsAvailable 
	{
		get
		{
			if(isDisposing || isApplicationQuit) return false;
			return instance != null;
		}
	}

	private static GameSystem Instance
	{
		get
		{
			if(isDisposing || isApplicationQuit) return null;
			if(instance) return instance;

			return CreateInstance();
		}
	}

	private static GameSystem CreateInstance()
	{
		instance = FindObjectOfType<GameSystem>();
		
		if(!instance)
		{
			var go = new GameObject("[AUTO]GameSystem");
			instance = go.AddComponent<GameSystem>();
		}
		
		return instance;
	}

	public static IEnumerator Restart()
	{
		if(instance != null) 
		{
			instance.Dispose();
			instance = null;
		}

		while(isDisposing) yield return 0;

		CreateInstance();

		while(!instance.isReady) yield return 0;
		while(!PluginAdapter.Cancelled && !PluginAdapter.Inited) yield return 0;
	}

	public static void EnableService()
	{
		Instance.service = new Mikan.CSAGA.Service();
		Instance.service.OnBusy += Instance.OnBusy;
		Instance.service.OnError +=Instance.OnError;
		Instance.service.OnReward += Instance.OnReward;

		Instance.service.Setup(new SystemServiceProvider());

		QuestManager.Instance.Setup();
		ShopManager.Instance.Setup();
		StageManager.Instance.Setup();

#if UNITY_IPHONE
		if(ConstData.Tables.GetVariable(VariableID.BUG_REPORT_ENABLE) != 0)
#endif
			ResourceManager.Instance.CreatePrefabInstance(Instance.gameObject, "Common/Report");
	}

	private void OnError (ErrorCode errCode, string errMsg, ErrorLevel level)
	{
		if(errCode == ErrorCode.NetFail)
			Reboot(0);
		else if(level == ErrorLevel.Fatal)
			MessageBox.Show(errMsg).callback = Reboot;
		else
			MessageBox.Show(errMsg);
	}

	private void OnBusy (bool obj)
	{
		UIManager.Instance.SetConnectingVisible(obj);
	}

	private void OnReward (List<Reward> list)
	{
		QuestDrop.Show(list);
	}

	private void Reboot(int index)
	{
		Application.LoadLevel(0);
	}

	public static Mikan.CSAGA.Service Service { get { return instance != null ? instance.service : null; } }

	private bool isReady = false;
	private Mikan.CSAGA.Service service;
	
	#region Unity

	void Awake () {
		DontDestroyOnLoad (gameObject);

#if UNITY_ANDROID
		const string CONFIG = "config_android";
#elif UNITY_IPHONE
		const string CONFIG = "config_ios";
#else
		const string CONFIG = "config";
#endif

#if PUBLISH
		Config = Mikan.ConfigParser.Parse(Resources.Load<TextAsset>(CONFIG).text);
#else
		Config = Mikan.ConfigParser.Parse(Resources.Load<TextAsset>(CONFIG + "_dev").text);
#endif

		Preload();
	}
	
	void Update () {
		if(service != null) service.TriggerEvent();
	}
	
	void OnDestroy() {
		if(service != null)
		{
			service.OnBusy -= OnBusy;
			service.Dispose();
			service = null;
		}

		isDisposing = false;
	}

	void OnApplicationPause(bool isPaused)
	{
		// Check the pauseStatus to see if we are in the foreground
		// or background
#if !UNITY_EDITOR && UNITY_ANDROID
		//FB Tracking
		/*
		if (!isPaused) {
			//app resume
			if (FB.IsInitialized) {
				FB.ActivateApp();
			} else {
				//Handle FB.Init
				FB.Init( () => {
					Debug.Log("FB Init");
					FB.ActivateApp();
				});
			}
		}
		*/
#endif
	}
	
	void OnApplicationQuit()
	{
		isApplicationQuit = true;
	}
	
	void OnLevelWasLoaded(int level)
	{
		if(level != 0) return;

		Dispose();
	}

	#endregion
	
	private void Preload()
	{
		CameraList.Instance.Add((int)UILayer.Background);

		Singleton<PatchManager>.Create();
		Singleton<ResourceManager>.Create();
		Singleton<UIManager>.Create();
		Singleton<StageManager>.Create();
		Singleton<SoundManager>.Create();
		Singleton<FingerGestureManager>.Create();

		PluginAdapter.Cancelled = false;
		PluginAdapter.Instance.InitIAB();

#if MYCARD
		MyCardAdapter.Instance.InitMyCard();
#endif

#if !UNITY_EDITOR && UNITY_ANDROID
		//FB
		/*
		if (FB.IsInitialized) {
			FB.ActivateApp();
		} else {
			//Handle FB.Init
			FB.Init( () => {
				Debug.Log("FB Init");
				FB.ActivateApp();
			});
		}
		*/
#endif

#if UNITY_IOS
		Application.targetFrameRate = 60;
#endif
		Message.AddListener(MessageID.DownloadEnd, OnDownloadEnd);
		Message.AddListener(MessageID.NetFail, OnNetFail);

		PatchManager.Instance.DownloadGeneralPatch();
	}

	private void OnNetFail()
	{
		UIManager.Instance.SetConnectingVisible(false);
		MessageBox.ShowConfirmWindow(ConstData.Tables.GetErrorText(ErrorCode.NetFail), (index)=>
		{
			UIManager.Instance.SetConnectingVisible(service.IsBusy);

			if(index == 0)
				HttpServiceProvider.Instance.Rewind();
			else
				HttpServiceProvider.Instance.Cancel();
		}
		);
	}

	private void OnDownloadEnd()
	{
		Message.RemoveListener(MessageID.DownloadEnd, OnDownloadEnd);
		isReady = true;
	}

	private void Dispose()
	{
		isDisposing = true;
		
		Message.Reset();
		
		Coroutine.StopAll();
		
		if(service != null)
		{
			service.OnBusy -= OnBusy;
			service.OnError -= OnError;
			service.OnReward -= OnReward;
			service.Dispose();
			service = null;
		}
		
		Destroy(gameObject);
		
		instance = null;
	}

	private HashSet<object> reports = new HashSet<object>();

	private void TryReport(object obj, string format, params object[] args)
	{
		if(!reports.Add(obj)) return;
		if(service != null) service.Report(ReportType.Debug, string.Format(format, args));
	}

	public static void Report(object obj, string format, params object[] args)
	{
#if UNITY_EDITOR
		LogWarning(format, args);
#else
		if(instance != null) instance.TryReport(obj, format, args);
#endif
	}

	public static void Log(object log)
	{
		Debug.Log(log);
	}

	public static void Log(string format, params object[] args)
	{
		Debug.Log(string.Format(format, args));
	}

	public static void LogWarning(object log)
	{
		Debug.LogWarning(log);
	}

	public static void LogWarning(string format, params object[] args)
	{
		Debug.LogWarning(string.Format(format, args));
	}

}
