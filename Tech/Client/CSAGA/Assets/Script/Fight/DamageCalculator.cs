﻿using UnityEngine;
using System;
using System.Collections.Generic;
using Mikan.CSAGA.ConstData;

public class DamageResult{

	public enum DAMAGE_EFFECT
	{
		NONE = 0,
		HEAL,
		CRITICAL,
		BUFF,
		DEBUFF,
		WEAK,
		RESIST,
		ABSORB,
	}

	public float Damage;
	public float RecoverHp;
	public DAMAGE_EFFECT Effect;

	public DamageResult(float damage, float recover, DAMAGE_EFFECT ef){
		Damage = damage;
		RecoverHp = recover;
		Effect = ef;
	}

}


//======================= Damage Calculator =========================
public class DamageCalculator{

	private List<IDamageFactor> FactorList;

	public Skill skillData;
	public BaseUnit attacker;
	public BaseUnit defender;
	public int actionLv;
	public float value;
	public float recover;
	public DamageResult.DAMAGE_EFFECT damageEffect;

	public DamageCalculator(Skill data, BaseUnit u1, BaseUnit u2, int lv)
	{
		skillData = data;
		attacker = u1;
		defender = u2;
		actionLv = lv;
		value = 0;
		recover = 0;
		damageEffect = DamageResult.DAMAGE_EFFECT.NONE;
		FactorList = new List<IDamageFactor> ();

	}

	public void AddFactor(IDamageFactor factor)
	{
		FactorList.Add (factor);
	}

	public DamageResult GetResult()
	{
		for(int i=0; i<FactorList.Count; i++)
		{
			FactorList[i].Calculate();
		}
		return new DamageResult (value, recover, damageEffect);
	}

	public void Reset()
	{
		value = 0;
		FactorList.Clear ();
	}

	//=================== Every Type of Skill Damage Behavior ============
	public DamageResult NormalAttack()
	{
		Reset ();
		AddFactor (new OriginATK (this));
		AddFactor (new TriggerLSkillEffect (this, TriggerLSkillEffect.TYPE.ATTACK, attacker.Index));
		AddFactor (new ElementEffect (this, attacker.Element));
		AddFactor (new AttackBuff (this));
		AddFactor (new ChainEffect (this));
		AddFactor (new CounterBonus (this));
		AddFactor (new DefenseBuff (this, attacker.Element));

		return GetResult();
	}

	public DamageResult EnemyAttack()
	{
		Reset ();
		AddFactor (new OriginATK (this));
		AddFactor (new AttackBuff (this));
		AddFactor (new DefenseBuff (this, attacker.Element));
		AddFactor (new LeaderSkillDefense(this, attacker.Element));
		
		return GetResult();
	}

	public DamageResult EnemyHeal()
	{
		Reset ();
		AddFactor (new OriginREV (this));
		AddFactor (new HealDebuff (this));
		
		return GetResult ();
	}

	public DamageResult AttackSkill()
	{
		Reset ();
		AddFactor (new OriginATK (this));
		AddFactor (new SkillBonusEffect (this));
		AddFactor (new TriggerLSkillEffect (this, TriggerLSkillEffect.TYPE.ATTACK, attacker.Index));
		//Element
		FightUnit.ELEMENT element = attacker.Element;
		if(skillData.n_EFFECT_Y > 0)
			element = (FightUnit.ELEMENT)skillData.n_EFFECT_Y;
		AddFactor (new ElementEffect (this, element));
		AddFactor (new AttackBuff (this));
		AddFactor (new ChainEffect (this));
		AddFactor (new CounterBonus (this));
		AddFactor (new DefenseBuff (this, (FightUnit.ELEMENT)skillData.n_EFFECT_Y));
		AddFactor (new LeaderSkillDefense(this, (FightUnit.ELEMENT)skillData.n_EFFECT_Y));
		//AddFactor (new Critical(this));
		//AddFactor (new RecoverHPAfterAttack (this));

		return GetResult();
	}

	public DamageResult NormalHeal()
	{
		Reset ();
		AddFactor (new OriginREV (this));
		AddFactor (new TriggerLSkillEffect (this, TriggerLSkillEffect.TYPE.HEAL, attacker.Index));
		AddFactor (new ChainEffect (this));
		AddFactor (new HealDebuff (this));
		
		return GetResult ();
	}

	public DamageResult HealSkill()
	{
		Reset ();
		AddFactor (new OriginREV (this));
		AddFactor (new SkillBonusEffect (this));
		AddFactor (new TargetMaxHPBonus (this, skillData.n_EFFECT_Y));
		AddFactor (new TriggerLSkillEffect (this, TriggerLSkillEffect.TYPE.HEAL, attacker.Index));
		AddFactor (new ChainEffect (this));
		//AddFactor (new HealDebuff (this));

		return GetResult ();
	}

	public DamageResult AddCDSkill()
	{
		Reset ();
		AddFactor (new FixNumber(this, skillData.n_EFFECT_X));
		AddFactor (new TargetCDBonus(this));

		return GetResult ();
	}

	public DamageResult AddConnectPower()
	{
		Reset ();
		AddFactor (new FixNumber(this, skillData.n_EFFECT_X));

		return GetResult ();
	}

	public DamageResult PowerAttackSkill()
	{
		Reset ();
		AddFactor(new FixNumber(this, skillData.n_EFFECT_X));
		//AddFactor(new ElementEffect(this));
		//AddFactor(new DefenseBuff(this));
		//AddFactor(new LeaderSkillDefense(this));
		AddFactor(new RecoverHPAfterAttack(this));

		return GetResult();
	}

	public DamageResult RecoverMP()
	{
		Reset ();
		AddFactor(new FixNumber(this, skillData.n_EFFECT_X));

		return GetResult();
	}

	public DamageResult GravityAttack()
	{
		Reset ();
		AddFactor (new TargetMaxHPBonus(this, skillData.n_EFFECT_X));
		AddFactor (new TargetHPBonus(this, skillData.n_EFFECT_Y));
		AddFactor (new FixNumber(this, skillData.n_EFFECT_Z));

		return GetResult();
	}

	public DamageResult OverHealAttack(int overHeal, int x)
	{
		Reset ();
		AddFactor (new OverHealDamage(this, overHeal, x));
		AddFactor (new ElementEffect (this, attacker.Element));
		AddFactor (new AttackBuff (this));
		AddFactor (new ChainEffect (this));
		AddFactor (new CounterBonus (this));
		AddFactor (new DefenseBuff (this, attacker.Element));
		
		return GetResult();

	}
}

//======================= Damage Factors ========================
public interface IDamageFactor{
	void Calculate();
}

public abstract class DamageBase : IDamageFactor
{
	protected DamageCalculator Calculator;

	public DamageBase(){}

	public DamageBase(DamageCalculator calculator)
	{
		Calculator = calculator;
	}
	public abstract void Calculate();
}

//====================== Custom Factor =========================
public class OriginATK : DamageBase
{
	public OriginATK (DamageCalculator cal) : base(cal){}

	public override void Calculate ()
	{
		float lvBonusRate = (float)(Calculator.actionLv - 1) * ConstData.Tables.GetVariablef(Mikan.CSAGA.VariableID.ATK_BOARD_POWER);
		float atk = (int)Calculator.attacker.FinalATK;
		//Buff effect.
		if(Calculator.attacker.GetBuff((int)Buff.BUFF_TYPE.ATK_ADD_DEF) != null)
			atk += (int)Calculator.attacker.FinalREV;

		Buff weakBuff = Calculator.attacker.GetBuff((int)Buff.BUFF_TYPE.WEAK);
		if(weakBuff != null)
			atk *= (weakBuff.XValue / 100f);
		Calculator.value += atk * (1 + lvBonusRate / 100f);
	}

}

public class SkillBonusEffect : DamageBase
{
	public SkillBonusEffect(DamageCalculator cal) : base(cal){}

	public override void Calculate()
	{
		float bonus = (float)Calculator.skillData.n_EFFECT_X / 100f;
		Calculator.value *= bonus;
	}
}

public class ElementEffect : DamageBase
{
	BaseUnit.ELEMENT AttackElement;

	public ElementEffect(DamageCalculator cal, FightUnit.ELEMENT element){
		Calculator = cal;
		AttackElement = element;
	}

	public override void Calculate ()
	{
		if(Calculator.defender == null) return;

		float effect = 1f;
		BaseUnit.ELEMENT element2 = Calculator.defender.Element;

		//Positive Effect
		if(AttackElement == BaseUnit.ELEMENT.FIRE && element2 == BaseUnit.ELEMENT.WIND
		   || AttackElement == BaseUnit.ELEMENT.WATER && element2 == BaseUnit.ELEMENT.FIRE
		   || AttackElement == BaseUnit.ELEMENT.WIND && element2 == BaseUnit.ELEMENT.WATER
		   || AttackElement == BaseUnit.ELEMENT.LIGHT && element2 == BaseUnit.ELEMENT.DARK
		   || AttackElement == BaseUnit.ELEMENT.DARK && element2 == BaseUnit.ELEMENT.LIGHT
		){
			effect = ConstData.Tables.GetVariablef(Mikan.CSAGA.VariableID.ATK_POWERUP);
			Calculator.damageEffect = DamageResult.DAMAGE_EFFECT.WEAK;
			//Tactic effect
			foreach(Mikan.CSAGA.ConstData.Tactics tactic in FightManager.Instance.TacticList)
			{
				if(tactic.n_EFFECT == Mikan.CSAGA.TacticsEffect.Weak)
					effect *= (100f + (float)tactic.n_EFFECT_Z) / 100f;
			}
		}
		else if(AttackElement == BaseUnit.ELEMENT.FIRE && element2 == BaseUnit.ELEMENT.WATER
		        || AttackElement == BaseUnit.ELEMENT.WATER && element2 == BaseUnit.ELEMENT.WIND
		        || AttackElement == BaseUnit.ELEMENT.WIND && element2 == BaseUnit.ELEMENT.FIRE
		){
			effect = ConstData.Tables.GetVariablef(Mikan.CSAGA.VariableID.ATK_POWERDOWN);
			Calculator.damageEffect = DamageResult.DAMAGE_EFFECT.RESIST;
			//Tactic effect
			foreach(Mikan.CSAGA.ConstData.Tactics tactic in FightManager.Instance.TacticList)
			{
				if(tactic.n_EFFECT == Mikan.CSAGA.TacticsEffect.Anti)
					effect *= (100f + (float)tactic.n_EFFECT_Z) / 100f;
			}
		}

		Calculator.value *= effect;

	}
}

public class CounterBonus : DamageBase
{
	public CounterBonus(DamageCalculator cal) : base(cal){}

	public override void Calculate ()
	{
		float bonus = 0f;
		foreach(KeyValuePair<int, float> pair in Calculator.attacker.CounterBonusDict)
		{
			if(pair.Key == (int)Calculator.defender.Element)
				bonus += pair.Value;
		}

		Calculator.value *= (bonus + 100f) / 100f;
	}
}

public class Critical : DamageBase
{
	public Critical(DamageCalculator cal) : base(cal){}

	public override void Calculate ()
	{
		/*
		int cri = Calculator.attacker.Critical;
		Buff buff = Calculator.attacker.GetBuff(4);	//Critical Buff
		if(buff != null)
			cri += buff.Value;
		if(Gamon.MathUtils.Random.Next(0, 100) < cri)
		{
			Calculator.value *= GameSystem.ConstData.GetVariable("CRITICAL_DAMAGE");
			Calculator.damageEffect = DamageResult.DAMAGE_EFFECT.CRITICAL;
		}
		*/
	}
}

public class RecoverHPAfterAttack : DamageBase
{
	public RecoverHPAfterAttack(DamageCalculator cal) : base(cal){}

	public override void Calculate ()
	{
		float healRate = (float)Calculator.skillData.n_EFFECT_Z / 100f;
		Calculator.recover = Calculator.value * healRate;
	}
}

public class OriginREV : DamageBase
{
	public OriginREV (DamageCalculator cal) : base(cal){}
	
	public override void Calculate ()
	{
		float lvBonusRate = (float)(Calculator.actionLv - 1) * ConstData.Tables.GetVariablef(Mikan.CSAGA.VariableID.REV_BOARD_POWER);
		float rev = (int)Calculator.attacker.FinalREV;
		//buff effect
		if(Calculator.attacker.GetBuff((int)Buff.BUFF_TYPE.ATK_ADD_DEF) != null)
			rev += (int)Calculator.attacker.FinalATK;

		Buff weakBuff = Calculator.attacker.GetBuff((int)Buff.BUFF_TYPE.WEAK);
		if(weakBuff != null)
			rev *= (weakBuff.YValue / 100f);
		Calculator.value += rev * (1 + lvBonusRate / 100f);
	}
}

public class HealerHPBonus : DamageBase
{
	public HealerHPBonus(DamageCalculator cal) : base(cal){}

	public override void Calculate ()
	{
		float bonus = (float)Calculator.skillData.n_EFFECT_Y / 100f;
		Calculator.value += (float)Calculator.attacker.Hp * bonus;
	}
}

public class TargetHPBonus : DamageBase
{
	int Value;
	public TargetHPBonus(DamageCalculator cal, int value)
	{
		Calculator = cal;
		Value = value;
	}
	
	public override void Calculate ()
	{
		float bonus = (float)Value / 100f;
		float hp = 0f;
		if(Calculator.defender != null)
			hp = (float)Calculator.defender.Hp;
		else
			hp = (float)FightManager.Instance.HP;
		Calculator.value += hp * bonus;
	}
}

public class TargetMaxHPBonus : DamageBase
{
	int Value;
	public TargetMaxHPBonus(DamageCalculator cal, int value)
	{
		Calculator = cal;
		Value = value;
	}

	public override void Calculate ()
	{
		float bonus = (float)Value / 100f;
		float maxHp = 0f;
		if(Calculator.defender != null)
			maxHp = (float)Calculator.defender.MaxHp;
		else
			maxHp = (float)FightManager.Instance.MaxHP;
		Calculator.value += maxHp * bonus;
	}
}

public class TargetCDBonus : DamageBase
{
	public TargetCDBonus(DamageCalculator cal) : base(cal){}

	public override void Calculate ()
	{
		throw new NotImplementedException();
		/*
		float bonus = (float)Calculator.skillData.n_EFFECT_Y / 100f;
		int cd = 0;
		if(Calculator.defender.ActiveSkills.Count >= 3)
			cd = Calculator.defender.ActiveSkills[2].n_CD;
		int fixedBonus = Mathf.CeilToInt(bonus * cd);
		Calculator.value += fixedBonus;
		*/
	}
}

public class FixNumber : DamageBase
{
	int Value;
	public FixNumber(DamageCalculator cal, int value)
	{
		Calculator = cal;
		Value = value;
	}

	public override void Calculate()
	{
		Calculator.value += Value;
	}
}

public class OverHealDamage : DamageBase
{
	int OverHeal;
	int X;

	public OverHealDamage(DamageCalculator cal, int overHeal, int x)
	{
		Calculator = cal;
		OverHeal = overHeal;
		X = x;
	}

	public override void Calculate ()
	{
		Calculator.value += (float)OverHeal * ((float)X / 100f);
	}
}

public class AttackBuff : DamageBase
{
	public AttackBuff(DamageCalculator cal) : base(cal){}

	public override void Calculate ()
	{

		Buff atkBuff = Calculator.attacker.GetBuff((int)Buff.BUFF_TYPE.POWER_UP);
		if(atkBuff != null)
		{
			Calculator.value *= (100f + (float)atkBuff.XValue) / 100f;
		}

	}
}

public class DefenseBuff : DamageBase
{
	BaseUnit.ELEMENT AttackElement;

	public DefenseBuff(DamageCalculator cal, BaseUnit.ELEMENT element){
		Calculator = cal;
		AttackElement = element;
	}
	
	public override void Calculate ()
	{
		BaseUnit.SIDE side = BaseUnit.SIDE.ALLY;
		if(Calculator.defender != null)
			side = Calculator.defender.Side;
		//element defense
		Buff defBuff = FightManager.Instance.GetGroupBuff(side, (int)Buff.BUFF_TYPE.GUARD);
		if(defBuff != null)
		{
			if(defBuff.YValue == 0 || (int)AttackElement == defBuff.YValue)
				Calculator.value *= (float)defBuff.XValue / 100f;
		}
		//element absorb
		if(Calculator.defender != null)
		{
			Buff abBuff = Calculator.defender.GetBuff((int)Buff.BUFF_TYPE.ABSORB);
			if(abBuff != null)
			{
				if(abBuff.XValue == 0 || (int)AttackElement == abBuff.XValue)
					Calculator.damageEffect = DamageResult.DAMAGE_EFFECT.ABSORB;
			}
		}
	}
}

public class HealDebuff : DamageBase
{
	public HealDebuff(DamageCalculator cal) : base(cal){}
	
	public override void Calculate ()
	{
		/*
		Buff deBuff = Calculator.defender.GetBuff((int)Buff.BUFF_TYPE.HEAL_REDUCE);
		if(deBuff != null)
		{
			Calculator.value *= (100f - (float)deBuff.Value) / 100f;
		}
		*/
	}
}

public class TriggerLSkillEffect : DamageBase
{
	public enum TYPE
	{
		ATTACK,
		HEAL,
	}

	TYPE Type;
	int AttackerIndex;

	public TriggerLSkillEffect(DamageCalculator cal, TYPE type, int atkIndex){
		Calculator = cal;
		Type = type;
		AttackerIndex = atkIndex;
	}

	public override void Calculate ()
	{
		if(Calculator.attacker.Side != BaseUnit.SIDE.ALLY)
			return;

		List<Skill> lst = FightManager.Instance.JITLeaderSkillList;
		int bonus = 0;
		for(int i = 0; i < lst.Count; i++)
		{
			Skill skill = lst[i];
			bool trigger = false;
			switch(skill.n_TRIGGER)
			{
				case (int)LSkillIcon.TYPE.TOTAL_LV:
				{
					int lv = FightManager.Instance.playManager.dashBoard.GetTotalLvByItem(skill.n_TRIGGER_Y);
					int min = (int)((float)skill.n_TRIGGER_X / 100f);
					int max = skill.n_TRIGGER_X % 100;
					if(lv <= max && lv >= min)
						trigger = true;
				}
				break;

				case (int)LSkillIcon.TYPE.COLUMN:
				{
					int min = (int)((float)skill.n_TRIGGER_X / 10f);
					int max = skill.n_TRIGGER_X % 10;
					trigger = FightManager.Instance.playManager.dashBoard.HasItemOnColumn(AttackerIndex, skill.n_TRIGGER_Y, min, max);
				}
				break;

				case (int)LSkillIcon.TYPE.TIME:
				{
					trigger = FightManager.Instance.TimePower >= (float)skill.n_TRIGGER_X / 10f;
				}
				break;

				case (int)LSkillIcon.TYPE.COLLECT:
				{
					int typeX = (int)((float)skill.n_TRIGGER_X / 10f);
					int typeY = (int)((float)skill.n_TRIGGER_Y / 10f);
					int countX = skill.n_TRIGGER_X % 10;
					int countY = skill.n_TRIGGER_Y % 10;
					List<int> lstType = FightManager.Instance.LSCollectList;
					for(int j = 0; j < lstType.Count; j++)
					{
						int t = lstType[j];
						if(t == typeX)
							countX--;
						else if(t == typeY)
							countY--;
					}
					trigger = (countX <= 0) && (countY <= 0);
				}
				break;

				case (int)LSkillIcon.TYPE.CHAIN:
				{
					trigger = (FightManager.Instance.chain >= skill.n_TRIGGER_X);
				}
				break;
			}

			//filter element & type
			bool elementTrigger = (skill.n_TARGET_X == 0 || ((skill.n_TARGET_X & (int)Calculator.attacker.Element) > 0));
			bool typeTrigger = (skill.n_TARGET_Y == 0 || ((skill.n_TARGET_Y & Calculator.attacker.Type) > 0));

			//Effect
			if(trigger && elementTrigger && typeTrigger && skill.n_EFFECT == 8)
			{
				switch(Type)
				{
					case TYPE.ATTACK:
						bonus += skill.n_EFFECT_X - 100;
						break;

					case TYPE.HEAL:
						bonus += skill.n_EFFECT_Y - 100;
						break;
				}
			}
		}

		Calculator.value *= (float)(bonus + 100) / 100f;
	}
}

public class ChainEffect : DamageBase
{
	public ChainEffect(DamageCalculator cal) : base(cal){}

	public override void Calculate ()
	{
		if(FightManager.Instance.isChainOn)
		{
			if(Calculator.attacker.Side == BaseUnit.SIDE.ALLY)
			{
				float chainActive = ConstData.Tables.GetVariablef(Mikan.CSAGA.VariableID.CHAIN_ACTIVE);
				if(FightManager.Instance.chain > chainActive)
				{
					float buff = ((float)FightManager.Instance.chain - chainActive) * ConstData.Tables.GetVariablef(Mikan.CSAGA.VariableID.CHAIN_POWERUP) / 100f;
					Calculator.value *= (1f + buff);
				}
			}
		}
	}
}

public class LeaderSkillDefense : DamageBase
{
	BaseUnit.ELEMENT Element;

	public LeaderSkillDefense(DamageCalculator cal, BaseUnit.ELEMENT element){
		Calculator = cal;
		Element = element;
	}

	public override void Calculate ()
	{
		if(Calculator.defender != null && Calculator.defender.Side == BaseUnit.SIDE.ENEMY)
			return;

		FightUnit unit = FightManager.Instance.GetUnit(1);
		if(unit != null)
		{
			for(int i = 0; i < unit.DefenseSkillList.Count; i++)
			{
				Skill leaderSkill = unit.DefenseSkillList[i];
				if(leaderSkill.n_TRIGGER_X > 0)
				{
					// Ignore Trigger Y.
					if(((int)Element & leaderSkill.n_TRIGGER_X) != 0)
					{
						Calculator.value *= (float)leaderSkill.n_EFFECT_X / 100f;
					}
				}
				else
				{
					// Use Trigger Y.
					FightUnit.ELEMENT element1 = Element;
					FightUnit.ELEMENT element2 = unit.Element;

					bool isTrigger = false;
					switch(leaderSkill.n_TRIGGER_Y)
					{
						case 1:	// Same Element
						{
							if(element1 == element2)
								isTrigger = true;
							break;
						}
						case 2:	// Negative Element
						{
							if(element1 == FightUnit.ELEMENT.FIRE && element2 == FightUnit.ELEMENT.WIND
							   || element1 == FightUnit.ELEMENT.WATER && element2 == FightUnit.ELEMENT.FIRE
							   || element1 == FightUnit.ELEMENT.WIND && element2 == FightUnit.ELEMENT.WATER
							   || element1 == FightUnit.ELEMENT.LIGHT && element2 == FightUnit.ELEMENT.DARK
							   || element1 == FightUnit.ELEMENT.DARK && element2 == FightUnit.ELEMENT.LIGHT
							)
								isTrigger = true;
							break;
						}
						case 3:	// Positive Element
						{
							if(element1 == FightUnit.ELEMENT.FIRE && element2 == FightUnit.ELEMENT.WATER
							   || element1 == FightUnit.ELEMENT.WATER && element2 == FightUnit.ELEMENT.WIND
							   || element1 == FightUnit.ELEMENT.WIND && element2 == FightUnit.ELEMENT.FIRE
							)
								isTrigger = true;
							break;
						}
					}
					if(isTrigger)
						Calculator.value *= (float)leaderSkill.n_EFFECT_X / 100f;
				}
			}
		}
	}
}
