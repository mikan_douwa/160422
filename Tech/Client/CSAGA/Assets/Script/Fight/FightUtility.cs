﻿using UnityEngine;
using System;
using System.Collections;

public static class FightUtility
{
	public enum SHAPE
	{
		CAPSULE,
		RECTANGLE,
	}

	public static IEnumerator WaitForRealSeconds(float time)
	{
		float start = Time.realtimeSinceStartup;
		while (Time.realtimeSinceStartup < start + time)
		{
			yield return 0;
		}
	}

	// Line Length.   
	public static double LineLength(double x1, double y1, double x2, double y2) {      
		double lineLength = 0;  
		if(x1 != x2 || y1 != y2)
			lineLength = Math.Sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));         
		
		return lineLength;      
		
	}    

	public static Vector2 LineMiddlePoint(Vector2 fromVector, Vector2 toVector, float forwardRate)
	{
		return new Vector2(fromVector.x + (toVector.x - fromVector.x) * forwardRate, fromVector.y + (toVector.y - fromVector.y) * forwardRate);
	}
	
	public static Vector2 LineStretchPoint(Vector2 fromVector, Vector2 toVector, float length)
	{
		Vector2 newToVector = toVector;
		if(fromVector == toVector)
		{
			//Same point with from and to, add 1f to x.
			newToVector = new Vector2(toVector.x + 1f, toVector.y);
		}
		float lineLength = (float)LineLength(fromVector.x, fromVector.y, newToVector.x, newToVector.y);
		float rate = length / lineLength;
		return LineMiddlePoint(fromVector, newToVector, rate);
	}
	
	//Point to Line Distance.   
	public static double PointToLine(double x1, double y1,  double x2, double y2,  double x0, double y0)      
	{      
		double space = 0;      
		double a, b, c;      
		a = LineLength(x1, y1, x2, y2);	// Line Length      
		b = LineLength(x1, y1, x0, y0);	// (x1,y1) to point distance.      
		c = LineLength(x2, y2, x0, y0);	// (x2,y2) to point distance.      
		if (c <= 0.000001 || b <= 0.000001) {      
			space = 0;      
			return space;      
		}      
		if (a <= 0.000001) {      
			space = b;      
			return space;      
		}      
		if (c * c >= a * a + b * b) {
			space = b;      
			return space;     
		}      
		if (b * b >= a * a + c * c) {    
			space = c;      
			return space;     
		}      
		double p = (a + b + c) / 2;	//      
		double s = Math.Sqrt(p * (p - a) * (p - b) * (p - c));	//     
		space = 2 * s / a;	//     
		return space;      
	}     
	public static double PointToRect(double x1, double y1,  double x2, double y2,  double x0, double y0, float radius)
	{
		double lineLength = LineLength(x1, y1, x2, y2);
		Vector2 strechPoint1 = LineStretchPoint(new Vector2((float)x2, (float)y2), new Vector2((float)x1, (float)y1), (float)(lineLength + radius));
		Vector2 strechPoint2 = LineStretchPoint(new Vector2((float)x1, (float)y1), new Vector2((float)x2, (float)y2), (float)(lineLength + radius));

		double space = 0;      
		double a, b, c;      
		a = LineLength(strechPoint1.x, strechPoint1.y, strechPoint2.x, strechPoint2.y);	// Line Length      
		b = LineLength(strechPoint1.x, strechPoint1.y, x0, y0);	// (x1,y1) to point distance.      
		c = LineLength(strechPoint2.x, strechPoint2.y, x0, y0);	// (x2,y2) to point distance.   

		if (c <= 0.000001 || b <= 0.000001) {      
			space = 0;      
			return space;      
		}      
		if (a <= 0.000001) {      
			space = b;      
			return space;      
		}      
		if (c * c >= a * a + b * b) {
			return double.MaxValue;     
		}      
		if (b * b >= a * a + c * c) {    
			return double.MaxValue;     
		}      
		double p = (a + b + c) / 2;	//      
		double s = Math.Sqrt(p * (p - a) * (p - b) * (p - c));	//     
		space = 2 * s / a;	//     
		return space;      
	}

	public static double Angle(double x1, double y1, double x2, double y2)
	{
		double dX = x2 - x1;
		double dY = y2 - y1;
		if(dX == 0 && dY == 0) return 0;
		
		double angle = Math.Atan(dY / dX) * 180 / Math.PI;
		
		if (dX < 0)
		{
			angle += 180;
		}
		
		if (angle < 0)
		{
			angle += 360;
		}
		
		return angle;
	}

	public static Vector2 FindPointWithAngle(double x, double y, double underLength, int angle)
	{
		int newAngle = angle % 360;
		int dirX = 1;
		if(newAngle > 90 && newAngle < 270)
			dirX = -1;
		int dirY = 1;
		if(newAngle > 180)
			dirY = -1;
		newAngle = newAngle % 90;

		double a = Math.Tan((double)newAngle * (Math.PI / 180)) * underLength;
		Vector2 pos = new Vector2((float)(x + dirX * underLength), (float)(y + dirY * a));
		return pos;
	}
}

public class Line : IComparable<Line>
{
	#region Members
	
	private Vector2 start;
	private Vector2 end;
	
	#endregion
	
	#region Constructors
	
	public Line(Vector2 start, Vector2 end)
	{
		this.start = start;
		this.end = end;
	}
	
	#endregion
	
	#region Properties
	
	public Vector2 Start
	{
		get
		{
			return start;
		}
		set
		{
			start = value;
		}
	}
	
	public Vector2 End
	{
		get
		{
			return end;
		}
		set
		{
			end = value;
		}
	}
	
	public double Angle
	{
		get
		{
			double dX = End.x - Start.x;
			double dY = End.y - Start.y;
			
			double angle = Math.Atan(dY / dX) * 180 / Math.PI;
			
			if (dX < 0)
			{
				angle += 180;
			}
			
			if (angle < 0)
			{
				angle += 360;
			}
			
			return angle;
		}
	}
	
	#endregion
	
	#region Functions
	
	public double IncludedAngleWith(Line line)
	{
		double angle = line.Angle;
		if (angle < this.Angle)
		{
			angle += 360;
		}
		
		return angle - this.Angle;
	}
	
	public int CompareTo(Line line)
	{
		if (this.Angle < line.Angle)
		{
			return -1;
		}
		else if (this.Angle == line.Angle)
		{
			return 0;
		}
		else
		{
			return 1;
		}
	}
	
	#endregion
}

public class TriangleMath
{
	TriangleMath(){}

	protected static TriangleMath instance = null;
	public static TriangleMath Instance
	{
		get{
			if(instance == null)
				instance = new TriangleMath();
			return instance;
		}
	}

	public bool Contains(Vector2[] vertexArray, Vector2 point)
	{
		Line[] lineArray = new Line[vertexArray.Length];
		for (int index = 0; index < vertexArray.Length; index++)
		{
			/*
			if (vertexArray[index] == point)        //Point to Vector covered.
			{
				return false;
			}
			*/
			lineArray[index] = new Line(point, vertexArray[index]);
		}
		
		Array.Sort(lineArray);
		
		for (int index = 0; index < lineArray.Length - 1; index++)
		{
			if (lineArray[index].IncludedAngleWith(lineArray[index + 1]) >= 180)
			{
				return false;
			}
		}
		
		if (lineArray[lineArray.Length - 1].IncludedAngleWith(lineArray[0]) >= 180)
		{
			return false;
		}
		
		return true;
	}
}