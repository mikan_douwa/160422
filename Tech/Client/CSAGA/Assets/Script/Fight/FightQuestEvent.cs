﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using Mikan.CSAGA.Data;

public class FightQuestEvent : QuestEventBase {

	int Round;
	int EventID;
	int QuestID;
	int TotalRounds;
	FightPlayManager PlayManager;

	Mikan.IQuestController prepareController;

	public FightQuestEvent(int round, int total, int eventId, int questId)
	{
		Round = round;
		EventID = eventId;
		QuestID = questId;
		TotalRounds = total;
	}

	public override void Prepare (Mikan.IQuestController controller)
	{
		prepareController = controller;

		if(Round == 0)
		{
			//First Battle, Load Prefab.
			Debug.Log("Load FightPlayManager");
			ResourceManager.Instance.CreatePrefabInstance("Setting/QuestTray");
			ResourceManager.Instance.CreatePrefabInstance("Fight/FightPlayManager", PrefabLoaded);
		}
		else
		{
			//Already loaded, reset playmanager
			FightManager.Instance.SetEnemyData(EventID);
			FightManager.Instance.Reset(Round, OnPrefabReady, onFinish, onFail);
		}
	}

	private void PrefabLoaded(GameObject obj)
	{
		PlayManager = obj.GetComponent<FightPlayManager>();
		FightManager.Instance.isPVP = (QuestID == ConstData.Tables.PVPQuestID);
		FightManager.Instance.SetPlayManager(PlayManager);
		FightManager.Instance.SetAllyData(Context.Instance.TeamIndex);
		FightManager.Instance.SetEnemyData(EventID);

		FightManager.Instance.Init(Round, TotalRounds, QuestID, OnPrefabReady, onFinish, onFail);
	}

	private void OnPrefabReady()
	{
		Debug.Log("OnPrefabReady");
		base.Prepare(prepareController);
	}
	
	public override void Start ()
	{
		Message.Send(MessageID.PlayMusic, Round < TotalRounds - 1 ? "bgm_battle" : "bgm_battle2");

		if(Round == 0)
			FightManager.Instance.playManager.OpenFight();
		else
		{
			FightManager.Instance.playManager.CheckTutorialThenStart();
		}
	}

	public override void Pause ()
	{
		FightManager.Instance.playManager.FightStop();
	}

	public override void Resume ()
	{
		FightManager.Instance.playManager.FightStart();
	}

	public override void Dispose ()
	{
		//Time.timeScale = 1f;
		//FightManager.Instance.ClearEnemyData();
		base.Dispose ();
	}
}
