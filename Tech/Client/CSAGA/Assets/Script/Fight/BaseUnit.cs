﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public abstract class BaseUnit {

	public enum ELEMENT
	{
		FIRE = 1,
		WATER = 2,
		WIND = 4,
		LIGHT = 8,
		DARK = 16,
		NONE = 32,
	}

	public enum SIDE
	{
		ALLY,
		ENEMY,
	}

	public int CardID;
	public int Index;
	public SIDE Side;

	public int Hp;
	public int MaxHp;
	public int Atk;
	public int Rev;
	public float Spd;

	public int BonusCount;
	public int FinalHP;
	public int FinalATK;
	public int FinalREV;

	public ELEMENT Element;
	public int Type;

	public string texName;

	//For Buff
	public Dictionary<int, Buff> BuffDict = new Dictionary<int, Buff>();
	//Passive effect
	public Dictionary<int, float> CounterBonusDict = new Dictionary<int, float>();
	public Dictionary<int, float> BuffExtendDict = new Dictionary<int, float>();

	//Const Variables.
	protected int MAX_BONUS_COUNT;
	protected float BONUS_EFFECT;

	public bool AddProbBonus(int count)
	{
		if(BonusCount == MAX_BONUS_COUNT) return false;
		
		BonusCount += count;
		if(BonusCount > MAX_BONUS_COUNT)
			BonusCount = MAX_BONUS_COUNT;
		
		
		float bonus = 1 + (float)BonusCount * BONUS_EFFECT / 100f;
		FinalHP = (int)((float)Hp * bonus);
		FinalATK = (int)((float)Atk * bonus);
		FinalREV = (int)((float)Rev * bonus);

		return true;
	}


#region Buff Functions.
	public void AddBuff(Buff b)
	{
		//buff time extend effect
		if(BuffExtendDict.ContainsKey(b.BuffID))
			b.TimeLeft *= (1f + BuffExtendDict[b.BuffID]);

		if(BuffDict.ContainsKey(b.BuffID))
		{
			Buff buff = BuffDict[b.BuffID];
			if(b.ReplaceValue >= buff.ReplaceValue)
				BuffDict[b.BuffID] = b;
		}
		else
			BuffDict.Add(b.BuffID, b);
	}

	public Buff GetBuff(int id)
	{
		if(BuffDict.ContainsKey(id))
			return BuffDict[id];
		else
			return null;
	}

	public bool ConsumeBuff(float sec)
	{
		bool needUpdate = false;
		List<int> lstDel = new List<int>();
		foreach(Buff b in BuffDict.Values)
		{
			b.TimeLeft -= sec;
			if(b.TimeLeft <= 0)
			{
				lstDel.Add(b.BuffID);
				needUpdate = true;
			}
		}
		//Buff finished
		for(int i = 0; i < lstDel.Count; i++)
		{
			int id = lstDel[i];
			BuffDict.Remove(id);
		}

		return needUpdate;
	}

	public bool CureStun(float sec)
	{
		bool needUpdate = false;
		List<int> lstDel = new List<int>();
		Buff stun = GetBuff((int)Buff.BUFF_TYPE.STUN);
		if(stun != null)
		{
			needUpdate = true;
			stun.TimeLeft -= sec;
			if(stun.TimeLeft <= 0)
			{
				BuffDict.Remove(stun.BuffID);
			}
		}

		return needUpdate;
	}

	public int GetBuffCount()
	{
		int n = 0;
		foreach(Buff b in BuffDict.Values)
		{
			if(b.BuffID < 100 && b.TimeLeft > 0)
				n++;
		}

		return n;
	}

	public int GetDebuffCount()
	{
		int n = 0;
		foreach(Buff b in BuffDict.Values)
		{
			if(b.BuffID >= 100 && b.TimeLeft > 0)
				n++;
		}
		
		return n;
	}
#endregion
}
