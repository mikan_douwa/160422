﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAISystem {
	
	public EnemyAISystem()
	{
	}

	public IFightCommand GetEnemyBehavior(FightEnemy enemy)
	{
		enemy.TalkID = 0;
		enemy.PVPTalk = "";
		
		if(FightManager.Instance.isPVP)
		{
			float hpRate = (float)enemy.Hp / (float)enemy.MaxHp;
			Mikan.CSAGA.ConstData.Skill skill = null;
			if(hpRate > 0.66f && enemy.PVPSkills[0] != null)
			{
				if(Mikan.MathUtils.Random.Next(0, 100) < enemy.PVPSkills[0].n_TRIGGER_RATE)
				{
					skill = enemy.PVPSkills[0];
					if(enemy.PVPTalks.Count > 0)
						enemy.PVPTalk = enemy.PVPTalks[0];
				}
			}
			else if(hpRate > 0.33f && enemy.PVPSkills[1] != null)
			{
				if(Mikan.MathUtils.Random.Next(0, 100) < enemy.PVPSkills[1].n_TRIGGER_RATE)
				{
					skill = enemy.PVPSkills[1];
					if(enemy.PVPTalks.Count > 1)
						enemy.PVPTalk = enemy.PVPTalks[1];
				}
			}
			else if(enemy.PVPSkills[2] != null)
			{
				if(Mikan.MathUtils.Random.Next(0, 100) < enemy.PVPSkills[2].n_TRIGGER_RATE)
				{
					skill = enemy.PVPSkills[2];
					if(enemy.PVPTalks.Count > 2)
						enemy.PVPTalk = enemy.PVPTalks[2];
				}
			}

			if(skill != null)
			{
				return new EnemySkillCommand(enemy, skill.n_ID, true);
			}
			else
				return new EnemyAttackCommand(enemy);
		}

		Mikan.CSAGA.ConstData.Mob data = enemy.mobData;
		if(data.n_MOBAI == 0 || enemy.AIData.Count == 0)
		{
			//Normal Attack.
			List<int> lst = new List<int>(data.n_MOBSKILL);
			lst.RemoveAll( o => { return o == 0; });
			return new EnemyAttackCommand(enemy);
		}
		else
		{
			bool findAction = false;
			int counter = 0;
			while(!findAction)
			{
				//Avoid Infinity Loop
				counter++;
				if(counter > 100)
				{
					return new EnemyIdleCommand(enemy);
				}
				
				Mikan.CSAGA.ConstData.MobAI ai = enemy.AIData[enemy.aiIndex];
				//AI behavior.
				switch(ai.n_CONDITION)
				{
				case 1:	//Self HP
					float hpRate = (float)enemy.Hp / (float)enemy.MaxHp * 100f;
					findAction = (hpRate >= ai.n_CONDITION_X && hpRate <= ai.n_CONDITION_Y);
					break;
				case 2:	//Rate
					findAction = (Mikan.MathUtils.Random.Next(0, 100) < ai.n_CONDITION_X);
					break;
				case 3:	//Buff
					findAction = ((ai.n_CONDITION_X == -1) && enemy.GetBuffCount() > 0) || enemy.GetBuff(ai.n_CONDITION_X) != null
						|| ((ai.n_CONDITION_Y == -1) && enemy.GetDebuffCount() > 0) || enemy.GetBuff(ai.n_CONDITION_Y) != null;
					break;
				case 4:	//Board
					findAction = FightManager.Instance.playManager.dashBoard.GetItemCountByType(ai.n_CONDITION_X) >= ai.n_CONDITION_Y;
					break;
				}
				
				if(findAction)
				{
					if(ai.n_ACT_TRUE == 0)
						enemy.aiIndex = enemy.firstAI;
					else
						enemy.aiIndex = ai.n_ACT_TRUE;
					
					//trigger action.
					switch(ai.n_ACT)
					{
					case 0:	// Pass, continue AI.
						findAction = false;
						break;
					case 1:	// Active Skill
						enemy.TalkID = ai.n_MOB_SPEAK;
						if(ai.n_ACT_X > 0)
						{
							int skillID = data.n_MOBSKILL[ai.n_ACT_X - 1];
							Mikan.CSAGA.ConstData.Skill skillData = ConstData.Tables.Skill[skillID];
							if(skillData != null && skillData.n_CD > 0)
							{
								enemy.inCDCommand = new EnemySkillCommand(enemy, skillID);
								enemy.CDFinished = false;
								return new EnemyCDCommand(enemy, skillID);
							}
							else
								return new EnemySkillCommand(enemy, skillID);
						}
						else
							return new EnemyAttackCommand(enemy);
						break;
					case 2:	// Idle
						enemy.TalkID = ai.n_MOB_SPEAK;
						return new EnemyIdleCommand(enemy);
						break;
					}
				}
				else
				{
					if(ai.n_ACT_FALSE == 0)
						enemy.aiIndex = enemy.firstAI;
					else
						enemy.aiIndex = ai.n_ACT_FALSE;
				}
			}
		}
		return null;
	}
	
}
