﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using Mikan;

public interface IFightCommand
{
	void Execute();
	
}

public class CharAttackCommand : IFightCommand {

	FightUnit attacker;
	FightEnemy defender;
	int ActionLV;
	ActItem.ACT_TYPE TriggerType;

	public CharAttackCommand(FightUnit unit, FightEnemy enemy, int lv, ActItem.ACT_TYPE type)
	{
		attacker = unit;
		defender = enemy;
		ActionLV = lv;
		TriggerType = type;
	}

	public void Execute()
	{
		LiveOperationQueue queue = new LiveOperationQueue();
		FightManager.Instance.ManageQueue(queue);
		List<IOperation> lstAction = new List<IOperation>();

		DamageCalculator cal = new DamageCalculator(null, attacker, defender, ActionLV);
		DamageResult result;
		result = cal.NormalAttack();
		//Passive skill
		List<Mikan.CSAGA.ConstData.Skill> lst = new List<Mikan.CSAGA.ConstData.Skill>();
		float hpRate = (float)FightManager.Instance.HP / (float)FightManager.Instance.MaxHP * 100f;
		bool isTrigger = false;

		int bonus = 0;
		for(int i = 0; i < attacker.PassiveSkillList.Count; i++)
		{
			Mikan.CSAGA.ConstData.Skill skillData = attacker.PassiveSkillList[i];
			//Trigger attack hp
			if(skillData.n_TRIGGER == (int)FightManager.PASSIVE_TRIGGER.ATTACK_HP && hpRate >= skillData.n_TRIGGER_X && hpRate <= skillData.n_TRIGGER_Y 
			   && Mikan.MathUtils.Random.Next(0, 100) < skillData.n_TRIGGER_RATE)
			{
				if(skillData.n_EFFECT == 8)
				{
					bonus += skillData.n_EFFECT_X - 100;
					isTrigger = true;
				}
				else
					lst.Add(skillData);
			}
			//Power up effect when Trigger use board item
			if(skillData.n_TRIGGER == (int)FightManager.PASSIVE_TRIGGER.ACT_ITEM && (skillData.n_TRIGGER_X & (int)TriggerType) > 0  
			   && ActionLV >= (int)((float)skillData.n_TRIGGER_Y / 10f) && ActionLV <= (skillData.n_TRIGGER_Y % 10)
			   && Mikan.MathUtils.Random.Next(0, 100) < skillData.n_TRIGGER_RATE)
			{
				if(skillData.n_EFFECT == 8)
				{
					bonus += skillData.n_EFFECT_X - 100;
					isTrigger = true;
				}
			}
			if(skillData.n_TRIGGER == (int)FightManager.PASSIVE_TRIGGER.ACT_COMBO && skillData.n_TRIGGER_X == (int)TriggerType  
			   && attacker.ActionLogList.Count > 1 && Mikan.MathUtils.Random.Next(0, 100) < skillData.n_TRIGGER_RATE)
			{
				if(skillData.n_EFFECT == 8)
				{
					bonus += skillData.n_EFFECT_X - 100;
					isTrigger = true;
				}
			}
		}
		result.Damage *= (float)(bonus + 100) / 100f;
		int damage = (int)result.Damage;

		if(isTrigger)
			lstAction.Add(new PlayFXToCharOperation(attacker.Side, attacker.Index, "PassiveFX"));
		lstAction.Add(new AttackEnemyOperation(attacker.Index));
		lstAction.Add(new ShootPlayOperation(BaseUnit.SIDE.ALLY, attacker.Index, BaseUnit.SIDE.ENEMY, defender.Index, 0.2f));
		if(result.Effect == DamageResult.DAMAGE_EFFECT.ABSORB)
		{
			lstAction.Add(new DamageTextOperation(defender.Index, DamageTextOperation.TEXT_TYPE.HEAL, damage, result.Effect));
		}
		else
		{
			lstAction.Add(new EnemyHitOperation(defender.Index, damage));
			lstAction.Add(new DamageTextOperation(defender.Index, DamageTextOperation.TEXT_TYPE.DAMAGE, damage, result.Effect));
		}

		//Bounce debuff
		if(attacker.GetBuff((int)Buff.BUFF_TYPE.BOUNCE) != null && damage > 0)
		{
			lstAction.Add(new PlayerHitOperation(damage, attacker.Element));
			lstAction.Add(new DamageTextOperation(0, DamageTextOperation.TEXT_TYPE.DAMAGE, damage, DamageResult.DAMAGE_EFFECT.NONE));
		}

		//Passive extend skill
		if(lst.Count > 0)
			lstAction.Add(new PassiveSkillOperation(attacker, lst));
		/*
		//Link Attack Buff
		List<FightUnit> lstAlly = FightManager.Instance.GetAllUnit();
		for(int i = 0; i < lstAlly.Count; i++)
		{
			FightUnit ally = lstAlly[i];
			if(ally.Index != attacker.Index)
			{
				Buff linkBuff = ally.GetBuff((int)Buff.BUFF_TYPE.LINK);
				if(linkBuff != null && Mikan.MathUtils.Random.Next(0, 100) < linkBuff.XValue)
				{
					//Link Attack
					DamageCalculator calAlly = new DamageCalculator(null, ally, defender, ActionLV);
					DamageResult resultAlly = calAlly.NormalAttack();
					int damageAlly = (int)resultAlly.Damage;

					lstAction.Add(new AttackEnemyOperation(ally.Index));
					lstAction.Add(new ShootPlayOperation(BaseUnit.SIDE.ALLY, ally.Index, BaseUnit.SIDE.ENEMY, defender.Index, 0.2f));
					if(resultAlly.Effect == DamageResult.DAMAGE_EFFECT.ABSORB)
					{
						lstAction.Add(new DamageTextOperation(defender.Index, DamageTextOperation.TEXT_TYPE.HEAL, damageAlly, resultAlly.Effect));
					}
					else
					{
						lstAction.Add(new EnemyHitOperation(defender.Index, damageAlly));
						lstAction.Add(new DamageTextOperation(defender.Index, DamageTextOperation.TEXT_TYPE.DAMAGE, damageAlly, resultAlly.Effect));
					}
				}
			}
		}
		*/

		lstAction.Add(new DetectFightEndOperation());
		queue.Enqueue(new SequentialOperationChain(lstAction));
	}
}

public class CharHealCommand : IFightCommand {

	FightUnit attacker;
	int ActionLV;
	ActItem.ACT_TYPE TriggerType;

	public CharHealCommand(FightUnit unit, int lv, ActItem.ACT_TYPE type)
	{
		attacker = unit;
		ActionLV = lv;
		TriggerType = type;
	}

	public void Execute()
	{
		LiveOperationQueue queue = new LiveOperationQueue();
		FightManager.Instance.ManageQueue(queue);
		List<IOperation> lstAction = new List<IOperation>();

		DamageCalculator cal = new DamageCalculator(null, attacker, null, ActionLV);
		DamageResult result = cal.NormalHeal();

		//Over Heal Attack Effect
		List<Mikan.CSAGA.ConstData.Skill> lstLeader = FightManager.Instance.JITLeaderSkillList;
		int overHealRate = 0;
		for(int i = 0; i < lstLeader.Count; i++)
		{
			Mikan.CSAGA.ConstData.Skill data = lstLeader[i];
			if(data.n_TRIGGER == 12)
			{
				if((data.n_TARGET_X == 0 || (data.n_TARGET_X & (int)attacker.Element) > 0)
				   && (data.n_TARGET_Y == 0 || (data.n_TARGET_Y & attacker.Type) > 0))
				{
					overHealRate += data.n_EFFECT_X;
				}
			}
		}

		//Passive skill
		List<Mikan.CSAGA.ConstData.Skill> lst = new List<Mikan.CSAGA.ConstData.Skill>();
		float hpRate = (float)FightManager.Instance.HP / (float)FightManager.Instance.MaxHP * 100f;
		bool isTrigger = false;

		int bonus = 0;
		float atkBonus = 0;
		for(int i = 0; i < attacker.PassiveSkillList.Count; i++)
		{
			Mikan.CSAGA.ConstData.Skill skillData = attacker.PassiveSkillList[i];
			//Heal effect
			if(skillData.n_TRIGGER == (int)FightManager.PASSIVE_TRIGGER.HEAL_HP && hpRate >= skillData.n_TRIGGER_X && hpRate <= skillData.n_TRIGGER_Y 
			   && Mikan.MathUtils.Random.Next(0, 100) < skillData.n_TRIGGER_RATE)
			{
				if(skillData.n_EFFECT == 8)
				{
					bonus += skillData.n_EFFECT_Y - 100;
					isTrigger = true;
				}
				else
					lst.Add(skillData);
			}
			//Power up effect when Trigger use board item
			if(skillData.n_TRIGGER == (int)FightManager.PASSIVE_TRIGGER.ACT_ITEM && (skillData.n_TRIGGER_X & (int)TriggerType) > 0  
			   && ActionLV >= (int)((float)skillData.n_TRIGGER_Y / 10f) && ActionLV <= (skillData.n_TRIGGER_Y % 10)
			   && Mikan.MathUtils.Random.Next(0, 100) < skillData.n_TRIGGER_RATE)
			{
				if(skillData.n_EFFECT == 8)
				{
					bonus += skillData.n_EFFECT_Y - 100;
					atkBonus += skillData.n_EFFECT_X - 100;
					isTrigger = true;
				}
			}
			if(skillData.n_TRIGGER == (int)FightManager.PASSIVE_TRIGGER.ACT_COMBO && skillData.n_TRIGGER_X == (int)TriggerType  
			   && attacker.ActionLogList.Count > 1 && Mikan.MathUtils.Random.Next(0, 100) < skillData.n_TRIGGER_RATE)
			{
				if(skillData.n_EFFECT == 8)
				{
					bonus += skillData.n_EFFECT_Y - 100;
					atkBonus += skillData.n_EFFECT_X - 100;
					isTrigger = true;
				}
			}

			if(overHealRate > 0)
			{
				//attack effect
				if(skillData.n_TRIGGER == (int)FightManager.PASSIVE_TRIGGER.ATTACK_HP && hpRate >= skillData.n_TRIGGER_X && hpRate <= skillData.n_TRIGGER_Y 
				   && Mikan.MathUtils.Random.Next(0, 100) < skillData.n_TRIGGER_RATE)
				{
					if(skillData.n_EFFECT == 8)
					{
						atkBonus += skillData.n_EFFECT_X - 100;
						isTrigger = true;
					}
					else
						lst.Add(skillData);
				}
			}
		}
		result.Damage *= (float)(bonus + 100) / 100f;
		int heal = (int)result.Damage;

		if(isTrigger)
			lstAction.Add(new PlayFXToCharOperation(attacker.Side, attacker.Index, "PassiveFX"));
		lstAction.Add(new DamageTextOperation(0, DamageTextOperation.TEXT_TYPE.HEAL, heal, result.Effect));

		if(overHealRate > 0)
		{
			FightEnemy enemyTarget = FightManager.Instance.GetTarget();
			int overHeal = heal - (FightManager.Instance.GetTotalHP() - FightManager.Instance.HP);
			overHeal = (int)((float)overHeal * (atkBonus + 100f) / 100f);
			if(enemyTarget != null && overHeal > 0)
			{
				DamageCalculator cal2 = new DamageCalculator(null, attacker, enemyTarget, 0);
				DamageResult result2 = cal2.OverHealAttack(overHeal, overHealRate);
				int overDamage = (int)result2.Damage;
				lstAction.Add(new AttackEnemyOperation(attacker.Index));
				lstAction.Add(new ShootPlayOperation(BaseUnit.SIDE.ALLY, attacker.Index, BaseUnit.SIDE.ENEMY, enemyTarget.Index, 0.2f));
				if(result2.Effect == DamageResult.DAMAGE_EFFECT.ABSORB)
				{
					lstAction.Add(new DamageTextOperation(enemyTarget.Index, DamageTextOperation.TEXT_TYPE.HEAL, overDamage, result2.Effect));
				}
				else
				{
					lstAction.Add(new EnemyHitOperation(enemyTarget.Index, overDamage));
					lstAction.Add(new DamageTextOperation(enemyTarget.Index, DamageTextOperation.TEXT_TYPE.DAMAGE, overDamage, result2.Effect));
				}

			}
		}

		//Passive extend skill
		if(lst.Count > 0)
			lstAction.Add(new PassiveSkillOperation(attacker, lst));

		lstAction.Add(new DetectFightEndOperation());
		queue.Enqueue(new SequentialOperationChain(lstAction));
	}
}

public class CharSkillCommand : IFightCommand {

	FightUnit attacker;
	bool isActiveSkill;
	int ActionLV;
	ActItem.ACT_TYPE TriggerType;
	bool isPassiveSkill;
	List<Mikan.CSAGA.ConstData.Skill> passiveSkillList;

	public CharSkillCommand(FightUnit unit, bool aSkill, int lv, ActItem.ACT_TYPE type)
	{
		attacker = unit;
		isActiveSkill = aSkill;
		ActionLV = lv;
		TriggerType = type;
		isPassiveSkill = false;
	}

	public CharSkillCommand(FightUnit unit, List<Mikan.CSAGA.ConstData.Skill> lst)
	{
		attacker = unit;
		isActiveSkill = false;
		passiveSkillList = lst;
		isPassiveSkill = true;
		ActionLV = 1;
		TriggerType = ActItem.ACT_TYPE.Useless;
	}

	int SkillBehavior(List<IOperation> lstAction, Mikan.CSAGA.ConstData.Skill skillData)
	{
		int bounceDamage = 0;
		BaseUnit.ELEMENT AttackElement = attacker.Element;

		//Passive Skill
		List<Mikan.CSAGA.ConstData.Skill> lstPSkillAtk = new List<Mikan.CSAGA.ConstData.Skill>();
		List<Mikan.CSAGA.ConstData.Skill> lstPassive8Atk = new List<Mikan.CSAGA.ConstData.Skill>();
		List<Mikan.CSAGA.ConstData.Skill> lstPSkillHeal = new List<Mikan.CSAGA.ConstData.Skill>();
		List<Mikan.CSAGA.ConstData.Skill> lstPassive8Heal = new List<Mikan.CSAGA.ConstData.Skill>();
		List<Mikan.CSAGA.ConstData.Skill> lstPassive8Item = new List<Mikan.CSAGA.ConstData.Skill>();
		if(!isPassiveSkill && skillData.n_TYPE != 3 && (skillData.n_EFFECT == 1 || skillData.n_EFFECT == 2))
		{
			//int trigger = (int)FightManager.PASSIVE_TRIGGER.ATTACK_HP;
			//if(skillData.n_EFFECT == 2)
			//	trigger = (int)FightManager.PASSIVE_TRIGGER.HEAL_HP;

			float hpRate = (float)FightManager.Instance.HP / (float)FightManager.Instance.MaxHP * 100f;
			for(int i = 0; i < attacker.PassiveSkillList.Count; i++)
			{
				Mikan.CSAGA.ConstData.Skill sData = attacker.PassiveSkillList[i];
				if(sData.n_TRIGGER == (int)FightManager.PASSIVE_TRIGGER.ATTACK_HP
				   && hpRate >= sData.n_TRIGGER_X && hpRate <= sData.n_TRIGGER_Y && Mikan.MathUtils.Random.Next(0, 100) < sData.n_TRIGGER_RATE)
				{
					if(sData.n_EFFECT == 8)
						lstPassive8Atk.Add(sData);
					else
						lstPSkillAtk.Add(sData);
				}
				if(sData.n_TRIGGER == (int)FightManager.PASSIVE_TRIGGER.HEAL_HP
				   && hpRate >= sData.n_TRIGGER_X && hpRate <= sData.n_TRIGGER_Y && Mikan.MathUtils.Random.Next(0, 100) < sData.n_TRIGGER_RATE)
				{
					if(sData.n_EFFECT == 8)
						lstPassive8Heal.Add(sData);
					else
						lstPSkillHeal.Add(sData);
				}
				//Power up effect when Trigger use board item
				if(!isActiveSkill)
				{
					if(sData.n_TRIGGER == (int)FightManager.PASSIVE_TRIGGER.ACT_ITEM && (sData.n_TRIGGER_X & (int)TriggerType) > 0 
					   && ActionLV >= (int)((float)sData.n_TRIGGER_Y / 10f) && ActionLV <= (sData.n_TRIGGER_Y % 10)
					   && Mikan.MathUtils.Random.Next(0, 100) < sData.n_TRIGGER_RATE)
					{
						if(sData.n_EFFECT == 8)
							lstPassive8Item.Add(sData);
					}
					if(sData.n_TRIGGER == (int)FightManager.PASSIVE_TRIGGER.ACT_COMBO && sData.n_TRIGGER_X == (int)TriggerType  
					   && attacker.ActionLogList.Count > 1 && Mikan.MathUtils.Random.Next(0, 100) < sData.n_TRIGGER_RATE)
					{
						if(sData.n_EFFECT == 8)
						{
							lstPassive8Item.Add(sData);
						}
					}
				}
			}
		}

		List<Mikan.CSAGA.ConstData.Skill> lstPSkill = new List<Mikan.CSAGA.ConstData.Skill>();
		List<Mikan.CSAGA.ConstData.Skill> lstPassive8 = new List<Mikan.CSAGA.ConstData.Skill>(lstPassive8Item);


		FightEnemy enemyTarget = FightManager.Instance.GetTarget();
		switch(skillData.n_EFFECT)
		{
		case 1:	//Attack
		case 5:	//Gravity Attack
		{
			lstAction.Add(new AttackEnemyOperation(attacker.Index));
			//Get Targets
			List<FightEnemy> lst = new List<FightEnemy>();
			if(skillData.n_TARGET == 1)	//Enemy
			{
				switch(skillData.n_TARGET_TYPE)
				{
				case 0:	//Single
					lst.Add(enemyTarget);
					break;
				case 1:	//All
					lst = FightManager.Instance.GetAllEnemy(true);
					break;
				}
			}

			lstPassive8.AddRange(lstPassive8Atk);
			lstPSkill.AddRange(lstPSkillAtk);
			//Passive skill trigger text
			if(lstPassive8.Count > 0 && lst.Count > 0)
				lstAction.Add(new PlayFXToCharOperation(attacker.Side, attacker.Index, "PassiveFX"));

			List<IOperation> lstHit = new List<IOperation>();
			for(int i = 0; i < lst.Count; i++)
			{
				FightEnemy enemy = lst[i];
				if(enemy == null || enemy.LifeStatus != FightEnemy.LIFE_STATUS.LIVE)
					continue;

				DamageCalculator cal = new DamageCalculator(skillData, attacker, enemy, ActionLV);
				DamageResult result;
				if(skillData.n_EFFECT == 5)
					result = cal.GravityAttack();
				else
				{
					result = cal.AttackSkill();
					AttackElement = (BaseUnit.ELEMENT)skillData.n_EFFECT_Y;
					//Passive effect
					int bonus = 0;
					for(int j = 0; j < lstPassive8.Count; j++)
					{
						Mikan.CSAGA.ConstData.Skill sp = lstPassive8[j];
						bonus += sp.n_EFFECT_X - 100;
					}
					result.Damage *= (float)(bonus + 100) / 100f;

				}
				int damage = (int)result.Damage;
				bounceDamage += damage;

				List<IOperation> lstSeq = new List<IOperation>();
				lstSeq.Add(new ShootPlayOperation(BaseUnit.SIDE.ALLY, attacker.Index, BaseUnit.SIDE.ENEMY, enemy.Index, 0.2f));
				if(result.Effect == DamageResult.DAMAGE_EFFECT.ABSORB)
				{
					lstSeq.Add(new DamageTextOperation(enemy.Index, DamageTextOperation.TEXT_TYPE.HEAL, damage, result.Effect));
				}
				else
				{
					lstSeq.Add(new EnemyHitOperation(enemy.Index, damage));
					lstSeq.Add(new DamageTextOperation(enemy.Index, DamageTextOperation.TEXT_TYPE.DAMAGE, damage, result.Effect));
				}
				lstHit.Add(new SequentialOperationChain(lstSeq));
			}
			if(lstHit.Count > 0)
				lstAction.Add(new ParallelOperationChain(lstHit));
		}
			break;
			
		case 2:	//Heal
		{
			//Over Heal Attack Effect
			List<Mikan.CSAGA.ConstData.Skill> lstLeader = FightManager.Instance.JITLeaderSkillList;
			int overHealRate = 0;
			for(int i = 0; i < lstLeader.Count; i++)
			{
				Mikan.CSAGA.ConstData.Skill data = lstLeader[i];
				if(data.n_TRIGGER == 12)
				{
					if((data.n_TARGET_X == 0 || (data.n_TARGET_X & (int)attacker.Element) > 0)
					   && (data.n_TARGET_Y == 0 || (data.n_TARGET_Y & attacker.Type) > 0))
					{
						overHealRate += data.n_EFFECT_X;
					}
				}
			}

			lstPassive8.AddRange(lstPassive8Heal);
			lstPSkill.AddRange(lstPSkillHeal);
			if(overHealRate > 0)
			{
				lstPassive8.AddRange(lstPassive8Atk);
				lstPSkill.AddRange(lstPSkillAtk);
			}

			//Passive skill trigger text
			if(lstPassive8.Count > 0)
				lstAction.Add(new PlayFXToCharOperation(attacker.Side, attacker.Index, "PassiveFX"));

			DamageCalculator cal = new DamageCalculator(skillData, attacker, null, ActionLV);
			DamageResult result = cal.HealSkill();
			//Passive effect
			int bonus = 0;
			float atkBonus = 0f;
			for(int i = 0; i < lstPassive8.Count; i++)
			{
				Mikan.CSAGA.ConstData.Skill sp = lstPassive8[i];
				bonus += sp.n_EFFECT_Y - 100;
				atkBonus += sp.n_EFFECT_X - 100;
			}
			result.Damage *= (float)(bonus + 100) / 100f;
			int heal = (int)result.Damage;
			
			lstAction.Add(new DamageTextOperation(0, DamageTextOperation.TEXT_TYPE.HEAL, heal, result.Effect));

			if(overHealRate > 0)
			{
				int overHeal = heal - (FightManager.Instance.GetTotalHP() - FightManager.Instance.HP);
				overHeal = (int)((float)overHeal * (atkBonus + 100f) / 100f);
				if(enemyTarget != null && overHeal > 0)
				{
					DamageCalculator cal2 = new DamageCalculator(null, attacker, enemyTarget, 0);
					DamageResult result2 = cal2.OverHealAttack(overHeal, overHealRate);
					int overDamage = (int)result2.Damage;
					lstAction.Add(new AttackEnemyOperation(attacker.Index));
					lstAction.Add(new ShootPlayOperation(BaseUnit.SIDE.ALLY, attacker.Index, BaseUnit.SIDE.ENEMY, enemyTarget.Index, 0.2f));
					if(result2.Effect == DamageResult.DAMAGE_EFFECT.ABSORB)
					{
						lstAction.Add(new DamageTextOperation(enemyTarget.Index, DamageTextOperation.TEXT_TYPE.HEAL, overDamage, result2.Effect));
					}
					else
					{
						lstAction.Add(new EnemyHitOperation(enemyTarget.Index, overDamage));
						lstAction.Add(new DamageTextOperation(enemyTarget.Index, DamageTextOperation.TEXT_TYPE.DAMAGE, overDamage, result2.Effect));
					}
					
				}
			}

		}
			break;
			
		case 3:	//Change Board
		{
			lstAction.Add(new ChangeBoardOperation(skillData.n_EFFECT_X, skillData.n_EFFECT_Y, skillData.n_TARGET_TYPE, attacker.Index));
		}
			break;
			
		case 4:	//Board Lv Up
		{
			lstAction.Add(new ItemLvUpOperation(skillData.n_EFFECT_X, skillData.n_EFFECT_Y, skillData.n_TARGET_TYPE, attacker.Index));
		}
			break;

		case 6:	//Lock Item
		{
			lstAction.Add(new LockItemOperation(skillData.n_EFFECT_X, skillData.n_TARGET_TYPE, attacker.Index));
		}
			break;

		case 7:	//Add Action Bar
		{
			float percent = (float)skillData.n_EFFECT_X / 100f;
			List<BaseUnit> lstTargets = GetTargets(skillData);
			for(int i = 0; i < lstTargets.Count; i++)
			{
				BaseUnit unit = lstTargets[i];
				if(unit.Side == BaseUnit.SIDE.ALLY)
					lstAction.Add(new AddActionOperation(unit.Index, percent));
			}
		}
			break;

		case 9:	//Cure Stun
		{
			List<IOperation> lstPara = new List<IOperation>();
			List<BaseUnit> lstTargets = GetTargets(skillData);
			for(int i = 0; i < lstTargets.Count; i++)
			{
				BaseUnit unit = lstTargets[i];
				lstPara.Add(new CureStunOperation(unit.Side, unit.Index, skillData.n_EFFECT_X));
			}
			if(lstPara.Count > 0)
				lstAction.Add(new ParallelOperationChain(lstPara));
		}
			break;

		case 10:	//Add CD Time
		{
			float percent = (float)skillData.n_EFFECT_X;
			List<BaseUnit> lstTargets = GetTargets(skillData);
			for(int i = 0; i < lstTargets.Count; i++)
			{
				BaseUnit unit = lstTargets[i];
				if(unit.Side == BaseUnit.SIDE.ALLY)
					lstAction.Add(new AddCDTimeOperation(unit.Index, percent));
			}
		}
			break;
			
		case 12:	//Bind Item
		{
			lstAction.Add(new BindItemOperation(skillData.n_EFFECT_X, skillData.n_TARGET_TYPE, attacker.Index));
		}
			break;

		case 13:	//Add Chain
		{
			int chain = skillData.n_EFFECT_X + (int)((float)skillData.n_EFFECT_Y / 100f * (float)FightManager.Instance.chain);
			lstAction.Add(new AddChainOperation(chain));
		}
			break;

		}

		//Bounce debuff
		if(attacker.GetBuff((int)Buff.BUFF_TYPE.BOUNCE) != null && bounceDamage > 0)
		{
			lstAction.Add(new PlayerHitOperation(bounceDamage, AttackElement));
			lstAction.Add(new DamageTextOperation(0, DamageTextOperation.TEXT_TYPE.DAMAGE, bounceDamage, DamageResult.DAMAGE_EFFECT.NONE));
		}

		//Buff
		if(skillData.n_STATUS_TARGET == 2)	//Self Buff
		{
			//Rate
			if(Mikan.MathUtils.Random.Next(0, 100) < skillData.n_STATUS_RATE)
			{
				Buff b = new Buff(skillData.n_STATUS_TYPE, skillData.n_STATUS_X, skillData.n_STATUS_Y, skillData.n_STATUS_Z, skillData.n_DURATION);
				lstAction.Add(new AddBuffOperation(attacker.Side, attacker.Index, b));
			}
		}
		else
		{
			List<BaseUnit> lstBuffTarget = GetTargets(skillData);
			
			//Target Buff
			if(FightManager.Instance.GroupBuffList.Contains(skillData.n_STATUS_TYPE))
			{
				//Group Buff
				if(lstBuffTarget.Count > 0 && Mikan.MathUtils.Random.Next(0, 100) < skillData.n_STATUS_RATE)
				{
					Buff b = new Buff(skillData.n_STATUS_TYPE, skillData.n_STATUS_X, skillData.n_STATUS_Y, skillData.n_STATUS_Z, skillData.n_DURATION);
					lstAction.Add(new AddBuffOperation(lstBuffTarget[0].Side, 0, b));
				}
			}
			else
			{
				//Trigger Buff
				List<IOperation> lstBuff = new List<IOperation>();
				for(int i = 0; i < lstBuffTarget.Count; i++)
				{
					BaseUnit unit = lstBuffTarget[i];
					//Rate
					if(Mikan.MathUtils.Random.Next(0, 100) < skillData.n_STATUS_RATE)
					{
						Buff b = new Buff(skillData.n_STATUS_TYPE, skillData.n_STATUS_X, skillData.n_STATUS_Y, skillData.n_STATUS_Z, skillData.n_DURATION);
						lstBuff.Add(new AddBuffOperation(unit.Side, unit.Index, b));
					}
				}
				if(lstBuff.Count > 0)
					lstAction.Add(new ParallelOperationChain(lstBuff));
			}
		}

		//Passive extend skill
		if(lstPSkill.Count > 0)
			lstAction.Add(new PassiveSkillOperation(attacker, lstPSkill));

		return skillData.n_SKILL_LINK;
	}

	List<BaseUnit> GetTargets(Mikan.CSAGA.ConstData.Skill skillData)
	{
		List<BaseUnit> lst = new List<BaseUnit>();

		if(skillData.n_TARGET == 3)
		{
			//self
			lst.Add(attacker);
			return lst;
		}
		else
		{
			if(skillData.n_TARGET == 5)
			{
				//ally except self
				lst = FightManager.Instance.GetAllBaseBySide(BaseUnit.SIDE.ALLY);	//Player side
				lst.Remove(attacker);
			}
			else
			{
				//Target Buff
				switch(skillData.n_TARGET_TYPE)
				{
				case 0:	//Single
					if(skillData.n_TARGET == 1)
						lst.Add(FightManager.Instance.GetTarget());
					else
						lst.Add(attacker);
					break;
				case 1:	//All
					if(skillData.n_TARGET == 1)
					{
						lst = FightManager.Instance.GetAllBaseBySide(BaseUnit.SIDE.ENEMY, true);	//Enemy side.
					}
					else
					{
						lst = FightManager.Instance.GetAllBaseBySide(BaseUnit.SIDE.ALLY);	//Player side
					}
					break;
				default:
					if(skillData.n_TARGET == 1)
					{
						lst = FightManager.Instance.GetAllBaseBySide(BaseUnit.SIDE.ENEMY, true);	//Enemy side.
					}
					else
					{
						lst = FightManager.Instance.GetAllBaseBySide(BaseUnit.SIDE.ALLY);	//Player side
					}

					//Random Targets.
					if(skillData.n_TARGET_TYPE > 10 && skillData.n_TARGET_TYPE < 20)
					{
						int count = skillData.n_TARGET_TYPE % 10;
						int TotalCount = lst.Count;
						if(count < TotalCount)
						{
							Mikan.MathUtils.Shuffle(lst);
							for(int i = TotalCount - 1; i >= count; i--)
							{
								lst.RemoveAt(i);
							}
						}
					}
					break;
				}
			}

			//Filter
			List<BaseUnit> lstFiltered = new List<BaseUnit>();
			for(int i = 0; i < lst.Count; i++)
			{
				BaseUnit unit = lst[i];
				if((skillData.n_TARGET_X == 0 || ((int)unit.Element & skillData.n_TARGET_X) != 0)
				   && (skillData.n_TARGET_Y == 0 || ((int)unit.Type & skillData.n_TARGET_Y) != 0))
					lstFiltered.Add(unit);
			}

			return lstFiltered;
		}
	}

	public void Execute()
	{

		LiveOperationQueue queue = new LiveOperationQueue();
		FightManager.Instance.ManageQueue(queue);
		List<IOperation> lstAction = new List<IOperation>();

		//Cut in
		if(isActiveSkill)
		{
			lstAction.Add(new CutInOperation(attacker, attacker.ActiveSkill.n_ID));
			FightManager.Instance.powerList.Add(attacker.ActiveSkill.n_ID);
		}

		Mikan.CSAGA.ConstData.Skill skillData = null;
		if(!isPassiveSkill)
		{
			if(isActiveSkill)
				skillData = attacker.ActiveSkill;
			else
				skillData = attacker.PowerSkill;

			int count = 0;
			int skillId = skillData.n_ID;
			while(skillId > 0)
			{
				skillId = SkillBehavior(lstAction, skillData);
				skillData = ConstData.Tables.Skill[skillId];
				count++;
				if(count > 10)
					break;
			}
		}
		else
		{
			lstAction.Add(new PlayFXToCharOperation(attacker.Side, attacker.Index, "PassiveFX"));
			for(int i = 0; i < passiveSkillList.Count; i++)
			{
				SkillBehavior(lstAction, passiveSkillList[i]);
			}
		}


		if(isActiveSkill)
			lstAction.Add(new TriggerSkillOperation(FightManager.PASSIVE_TRIGGER.ACTIVE_SKILL));
		lstAction.Add(new DetectFightEndOperation());
		queue.Enqueue(new SequentialOperationChain(lstAction));
	}
}

public class CharPowerUpCommand : IFightCommand {

	FightUnit attacker;
	int BonusCount;

	public CharPowerUpCommand(FightUnit unit, int count)
	{
		attacker = unit;
		BonusCount = count;
	}

	public void Execute()
	{
		LiveOperationQueue queue = new LiveOperationQueue();
		FightManager.Instance.ManageQueue(queue);
		List<IOperation> lstAction = new List<IOperation>();

		int hpBefore = FightManager.Instance.GetTotalHP();
		if(attacker.AddProbBonus(BonusCount))
		{

			int hpAfter = FightManager.Instance.GetTotalHP();
			int heal = hpAfter - hpBefore;
			FightManager.Instance.RefreshMaxHp();
			//FightManager.Instance.AddHp(heal);
			lstAction.Add(new UpdateTotalHPOperation());
			lstAction.Add(new DamageTextOperation(0, DamageTextOperation.TEXT_TYPE.HEAL, heal, DamageResult.DAMAGE_EFFECT.NONE));
		}

		lstAction.Add(new AddActionOperation(attacker.Index, BonusCount));
		queue.Enqueue(new SequentialOperationChain(lstAction));
	}
}

public class PoisonCommand : IFightCommand {

	int ItemLV;

	public PoisonCommand(int lv)
	{
		ItemLV = lv;
	}

	public void Execute()
	{
		LiveOperationQueue queue = new LiveOperationQueue();
		FightManager.Instance.ManageQueue(queue);
		List<IOperation> lstAction = new List<IOperation>();

		float rate = ConstData.Tables.GetVariablef(Mikan.CSAGA.VariableID.POISON_BOARD_BASE) 
		                   + (ItemLV - 1) * ConstData.Tables.GetVariablef(Mikan.CSAGA.VariableID.POISON_BOARD_POWER);
		int damage = (int)((rate / 100f) * (float)FightManager.Instance.MaxHP);
		lstAction.Add(new PlayerHitOperation(damage, BaseUnit.ELEMENT.NONE));
		lstAction.Add(new DamageTextOperation(0, DamageTextOperation.TEXT_TYPE.DAMAGE, damage, DamageResult.DAMAGE_EFFECT.NONE));

		lstAction.Add(new DetectFightEndOperation());
		queue.Enqueue(new SequentialOperationChain(lstAction));
	}
}

public class EnemyAttackCommand : IFightCommand {
	
	FightEnemy attacker;

	public EnemyAttackCommand(FightEnemy enemy)
	{
		attacker = enemy;
	}
	
	public void Execute()
	{
		LiveOperationQueue queue = new LiveOperationQueue();
		FightManager.Instance.ManageQueue(queue);
		List<IOperation> lstAction = new List<IOperation>();

		DamageCalculator cal = new DamageCalculator(null, attacker, null, 1);
		DamageResult result;
		result = cal.EnemyAttack();
		int damage = (int)result.Damage;
		//FightManager.Instance.AddHp(-damage);

		lstAction.Add(new EnemyTalkOperation(attacker, false));
		lstAction.Add(new AttackPlayerOperation(attacker.Index));
		lstAction.Add(new ShootPlayOperation(BaseUnit.SIDE.ENEMY, attacker.Index, BaseUnit.SIDE.ALLY, 0, 0.2f));
		lstAction.Add(new PlayerHitOperation(damage, attacker.Element));
		lstAction.Add(new DamageTextOperation(0, DamageTextOperation.TEXT_TYPE.DAMAGE, damage, result.Effect));

		lstAction.Add(new DetectFightEndOperation());
		queue.Enqueue(new SequentialOperationChain(lstAction));
	}
}

public class EnemyIdleCommand : IFightCommand {

	FightEnemy attacker;

	public EnemyIdleCommand(FightEnemy enemy)
	{
		attacker = enemy;
	}

	public void Execute()
	{
		LiveOperationQueue queue = new LiveOperationQueue();
		FightManager.Instance.ManageQueue(queue);
		List<IOperation> lstAction = new List<IOperation>();

		lstAction.Add(new EnemyTalkOperation(attacker, false));

		queue.Enqueue(new SequentialOperationChain(lstAction));
	}
}

public class EnemyHealCommand : IFightCommand {
	
	FightEnemy attacker;

	public EnemyHealCommand(FightEnemy enemy)
	{
		attacker = enemy;
	}
	
	public void Execute()
	{
		LiveOperationQueue queue = new LiveOperationQueue();
		FightManager.Instance.ManageQueue(queue);
		List<IOperation> lstAction = new List<IOperation>();
		
		DamageCalculator cal = new DamageCalculator(null, attacker, null, 1);
		DamageResult result = cal.EnemyHeal();
		int heal = (int)result.Damage;
		
		lstAction.Add(new DamageTextOperation(attacker.Index, DamageTextOperation.TEXT_TYPE.HEAL, heal, result.Effect));

		lstAction.Add(new DetectFightEndOperation());
		queue.Enqueue(new SequentialOperationChain(lstAction));
	}
}

public class EnemySkillCommand : IFightCommand {

	public enum TRIGGER
	{
		NONE,
		PRE_ATTACK,
		DEATH,
	}

	FightEnemy attacker;
	int SkillID;
	TRIGGER Trigger;
	bool goWhenEnd;
	bool isPVP;

	public EnemySkillCommand(FightEnemy unit, int sIndex)
	{
		attacker = unit;
		SkillID = sIndex;
		Trigger = TRIGGER.NONE;
		goWhenEnd = false;
		isPVP = false;
	}

	//use for pvp
	public EnemySkillCommand(FightEnemy unit, int sIndex, bool pvp)
	{
		attacker = unit;
		SkillID = sIndex;
		Trigger = TRIGGER.NONE;
		goWhenEnd = false;
		isPVP = pvp;
	}

	//use for pre attack or death skill.
	public EnemySkillCommand(FightEnemy unit, int sIndex, TRIGGER trigger, bool finalPreAttack)
	{
		attacker = unit;
		SkillID = sIndex;
		Trigger = trigger;
		goWhenEnd = finalPreAttack;
		isPVP = false;
	}

	int SkillBehavior(List<IOperation> lstAction, Mikan.CSAGA.ConstData.Skill skillData)
	{
		switch(skillData.n_EFFECT)
		{
		case 1:	//Attack
		case 5:	//Gravity Attack
		{
			lstAction.Add(new AttackPlayerOperation(attacker.Index));
			List<IOperation> lstHit = new List<IOperation>();
			BaseUnit.ELEMENT AttackElement = attacker.Element;
			DamageCalculator cal = new DamageCalculator(skillData, attacker, null, 1);
			DamageResult result;
			if(skillData.n_EFFECT == 5)
				result = cal.GravityAttack();
			else
			{
				result = cal.AttackSkill();
				AttackElement = (BaseUnit.ELEMENT)skillData.n_EFFECT_Y;
			}
			int damage = (int)result.Damage;
			//FightManager.Instance.AddHp(-damage);

			lstHit.Add(new ShootPlayOperation(BaseUnit.SIDE.ENEMY, attacker.Index, BaseUnit.SIDE.ALLY, 0, 0.2f));
			lstHit.Add(new PlayerHitOperation(damage, AttackElement));
			lstHit.Add(new DamageTextOperation(0, DamageTextOperation.TEXT_TYPE.DAMAGE, damage, result.Effect));
			lstAction.Add(new ParallelOperationChain(lstHit));
		}
			break;			
			
		case 2:	//Heal
		{
			//Get Targets
			List<BaseUnit> lst = GetTargets(skillData);

			List<IOperation> lstHit = new List<IOperation>();
			for(int i = 0; i < lst.Count; i++)
			{
				BaseUnit enemy = lst[i];
				DamageCalculator cal = new DamageCalculator(skillData, attacker, enemy, 1);
				DamageResult result = cal.HealSkill();
				int heal = (int)result.Damage;
				//enemy.AddHp(heal);
				
				lstHit.Add(new DamageTextOperation(enemy.Index, DamageTextOperation.TEXT_TYPE.HEAL, heal, result.Effect));
			}
			if(lstHit.Count > 0)
				lstAction.Add(new ParallelOperationChain(lstHit));
		}
			break;
			
		case 3:	//Change Board
		{
			lstAction.Add(new ChangeBoardOperation(skillData.n_EFFECT_X, skillData.n_EFFECT_Y, skillData.n_TARGET_TYPE, attacker.Index));
		}
		break;
			
		case 4:	//Board Lv Up
		{
			lstAction.Add(new ItemLvUpOperation(skillData.n_EFFECT_X, skillData.n_EFFECT_Y, skillData.n_TARGET_TYPE, attacker.Index));
		}
		break;

		case 6:	//Lock Item
		{
			lstAction.Add(new LockItemOperation(skillData.n_EFFECT_X, skillData.n_TARGET_TYPE, attacker.Index));
		}
		break;

		case 7:	//Add Action Bar
		{
			float percent = (float)skillData.n_EFFECT_X / 100f;
			List<BaseUnit> lstTargets = GetTargets(skillData);
			for(int i = 0; i < lstTargets.Count; i++)
			{
				BaseUnit unit = lstTargets[i];
				if(unit.Side == BaseUnit.SIDE.ALLY)
					lstAction.Add(new AddActionOperation(unit.Index, percent));
			}
		}
			break;

		case 10:	//Add CD Time
		{
			float percent = (float)skillData.n_EFFECT_X;
			List<BaseUnit> lstTargets = GetTargets(skillData);
			for(int i = 0; i < lstTargets.Count; i++)
			{
				BaseUnit unit = lstTargets[i];
				if(unit.Side == BaseUnit.SIDE.ALLY)
					lstAction.Add(new AddCDTimeOperation(unit.Index, percent));
			}
		}
			break;

		case 12:	//Bind Item
		{
			lstAction.Add(new BindItemOperation(skillData.n_EFFECT_X, skillData.n_TARGET_TYPE, attacker.Index));
		}
			break;

		case 14:	//QTE Shield
		{
			lstAction.Add(new QTEShieldOperation(skillData));
		}
			break;

		}


		//Buff
		if(skillData.n_STATUS_TARGET == 2)	//Self Buff
		{
			//Rate
			if(Mikan.MathUtils.Random.Next(0, 100) < skillData.n_STATUS_RATE)
			{
				Buff b = new Buff(skillData.n_STATUS_TYPE, skillData.n_STATUS_X, skillData.n_STATUS_Y, skillData.n_STATUS_Z, skillData.n_DURATION);
				lstAction.Add(new AddBuffOperation(attacker.Side, attacker.Index, b));
			}
		}
		else
		{
			List<BaseUnit> lstBuffTarget = GetTargets(skillData);

			if(FightManager.Instance.GroupBuffList.Contains(skillData.n_STATUS_TYPE))
			{
				//Group Buff
				if(lstBuffTarget.Count > 0 && Mikan.MathUtils.Random.Next(0, 100) < skillData.n_STATUS_RATE)
				{
					Buff b = new Buff(skillData.n_STATUS_TYPE, skillData.n_STATUS_X, skillData.n_STATUS_Y, skillData.n_STATUS_Z, skillData.n_DURATION);
					lstAction.Add(new AddBuffOperation(lstBuffTarget[0].Side, 0, b));
				}
			}
			else
			{
				//Trigger Buff
				List<IOperation> lstBuff = new List<IOperation>();
				for(int i = 0; i < lstBuffTarget.Count; i++)
				{
					BaseUnit unit = lstBuffTarget[i];
					//Rate
					if(Mikan.MathUtils.Random.Next(0, 100) < skillData.n_STATUS_RATE)
					{
						Buff b = new Buff(skillData.n_STATUS_TYPE, skillData.n_STATUS_X, skillData.n_STATUS_Y, skillData.n_STATUS_Z, skillData.n_DURATION);
						lstBuff.Add(new AddBuffOperation(unit.Side, unit.Index, b));
					}
				}
				if(lstBuff.Count > 0)
					lstAction.Add(new ParallelOperationChain(lstBuff));
			}
		}

		return skillData.n_SKILL_LINK;
	}

	List<BaseUnit> GetTargets(Mikan.CSAGA.ConstData.Skill skillData)
	{
		List<BaseUnit> lst = new List<BaseUnit>();
		
		if(skillData.n_TARGET == 3)
		{
			//self
			lst.Add(attacker);
			return lst;
		}
		else
		{
			if(skillData.n_TARGET == 5)
			{
				//ally except self
				lst = FightManager.Instance.GetAllBaseBySide(BaseUnit.SIDE.ENEMY);	//Enemy side
				lst.Remove(attacker);
			}
			else
			{
				//Target Buff
				switch(skillData.n_TARGET_TYPE)
				{
				case 0:	//Single
					if(skillData.n_TARGET == 1)
						lst.Add(FightManager.Instance.GetUnit(1));
					else
						lst.Add(attacker);
					break;
				case 1:	//All
					if(skillData.n_TARGET == 1)
					{
						lst = FightManager.Instance.GetAllBaseBySide(BaseUnit.SIDE.ALLY);	//Player side.
					}
					else
					{
						lst = FightManager.Instance.GetAllBaseBySide(BaseUnit.SIDE.ENEMY, true);	//Enemy side
					}
					break;
				default:
					if(skillData.n_TARGET == 1)
					{
						lst = FightManager.Instance.GetAllBaseBySide(BaseUnit.SIDE.ALLY);	//Player side.
					}
					else
					{
						lst = FightManager.Instance.GetAllBaseBySide(BaseUnit.SIDE.ENEMY, true);	//Enemy side
					}

					//Random Targets.
					if(skillData.n_TARGET_TYPE > 10 && skillData.n_TARGET_TYPE < 20)
					{
						int count = skillData.n_TARGET_TYPE % 10;
						int TotalCount = lst.Count;
						if(count < TotalCount)
						{
							Mikan.MathUtils.Shuffle(lst);
							for(int i = TotalCount - 1; i >= count; i--)
							{
								lst.RemoveAt(i);
							}
						}
					}
					break;
				}
			}
			
			//Filter
			List<BaseUnit> lstFiltered = new List<BaseUnit>();
			for(int i = 0; i < lst.Count; i++)
			{
				BaseUnit unit = lst[i];
				if((skillData.n_TARGET_X == 0 || ((int)unit.Element & skillData.n_TARGET_X) != 0)
				   && (skillData.n_TARGET_Y == 0 || ((int)unit.Type & skillData.n_TARGET_Y) != 0))
					lstFiltered.Add(unit);
			}
			
			return lstFiltered;
		}
	}


	public void Execute()
	{
		Mikan.CSAGA.ConstData.Skill skillData = ConstData.Tables.Skill[SkillID];
		if(skillData == null) return;

		LiveOperationQueue queue = new LiveOperationQueue();
		FightManager.Instance.ManageQueue(queue);
		List<IOperation> lstAction = new List<IOperation>();

		//Talk
		if(Trigger == TRIGGER.DEATH)
			attacker.TalkID = attacker.deathTalkID;
		lstAction.Add(new EnemyTalkOperation(attacker, (Trigger != TRIGGER.NONE)));

		//Cut in
		if(!isPVP && Trigger == TRIGGER.NONE && skillData.n_CD > 0)
			lstAction.Add(new CutInOperation(attacker, SkillID));

		int count = 0;
		int skillId = skillData.n_ID;
		while(skillId > 0)
		{
			//Stop Link when QTE Shield
			if(skillData.n_EFFECT == 14)
				count += 10;

			skillId = SkillBehavior(lstAction, skillData);
			skillData = ConstData.Tables.Skill[skillId];
			count++;
			if(count > 10)
				break;
		}

		//skill before death.
		if(Trigger == TRIGGER.DEATH)
			lstAction.Add(new EnemyForceDeadOperation(attacker.Index));
		//fight start after pre attack or death skill.
		if((Trigger == TRIGGER.PRE_ATTACK && goWhenEnd) || Trigger == TRIGGER.DEATH)
			lstAction.Add(new FightStartOperation());

		lstAction.Add(new DetectFightEndOperation());
		queue.Enqueue(new SequentialOperationChain(lstAction));
	}
}

public class EnemyCDCommand : IFightCommand {

	FightEnemy attacker;
	int SkillID;
	
	public EnemyCDCommand(FightEnemy unit, int sIndex)
	{
		attacker = unit;
		SkillID = sIndex;
	}

	public void Execute()
	{
		Mikan.CSAGA.ConstData.Skill skillData = ConstData.Tables.Skill[SkillID];
		if(skillData == null) return;

		LiveOperationQueue queue = new LiveOperationQueue();
		FightManager.Instance.ManageQueue(queue);
		List<IOperation> lstAction = new List<IOperation>();

		lstAction.Add(new EnemyTalkOperation(attacker, false));
		lstAction.Add(new EnemyCDOperation(attacker.Index, skillData.n_CD));

		lstAction.Add(new DetectFightEndOperation());
		queue.Enqueue(new SequentialOperationChain(lstAction));

	}

}


public class EndQTECommand : IFightCommand {

	public EndQTECommand(){}

	public void Execute()
	{
		LiveOperationQueue queue = new LiveOperationQueue();
		FightManager.Instance.ManageQueue(queue);
		List<IOperation> lstAction = new List<IOperation>();

		List<IOperation> lstPara = new List<IOperation>();
		List<FightEnemy> lst = FightManager.Instance.GetAllEnemy(true);
		for(int i = 0; i < lst.Count; i++)
		{
			FightEnemy enemy = lst[i];
			lstPara.Add(new EnemyForceDeadOperation(enemy.Index));
		}
		lstAction.Add(new ParallelOperationChain(lstPara));

		lstAction.Add(new DetectFightEndOperation());
		queue.Enqueue(new SequentialOperationChain(lstAction));
	}
}
