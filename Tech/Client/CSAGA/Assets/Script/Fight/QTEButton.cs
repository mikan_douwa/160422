﻿using UnityEngine;
using System.Collections;

public class QTEButton : MonoBehaviour {

	public UISprite itemSprite;
	public UISprite lvSprite;
	public UISprite maxSprite;
	public UILabel lvLabel;

	public ActItem.ACT_TYPE Type;
	public int Lv;

	public void Init(ActItem.ACT_TYPE type, int lv)
	{
		Type = type;
		Lv = lv;
		itemSprite.spriteName = "battle_board_" + ((int)Type).ToString("000");
		itemSprite.transform.localScale = new Vector3(0.8f, 0.8f, 0.8f);
		lvLabel.text = Lv.ToString();
		NGUITools.SetActive(lvSprite.gameObject, (Lv < ActItem.MAX_LV));
		NGUITools.SetActive(lvLabel.gameObject, (Lv < ActItem.MAX_LV));
		NGUITools.SetActive(maxSprite.gameObject, (Lv == ActItem.MAX_LV));

	}

	public void SetFocus()
	{
		itemSprite.transform.localScale = Vector3.one;
	}
}
