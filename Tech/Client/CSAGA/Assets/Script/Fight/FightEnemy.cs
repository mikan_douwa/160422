﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Mikan.CSAGA.ConstData;

public class FightEnemy : BaseUnit {

	public enum LIFE_STATUS
	{
		LIVE,
		DEAD,
	}

	public int EnemyID;
	public LIFE_STATUS LifeStatus;

	public float initPosX;
	//Enemy Behavior
	public Mob mobData;
	public FightUnit copyData;
	public Dictionary<int, MobAI> AIData;
	public int TalkID;
	public string PVPTalk = "";
	public int aiIndex;
	public int firstAI;
	//Pre attack
	public int preSkillID = 0;
	//attack after death
	public int deathSkillID = 0;
	public int deathTalkID = 0;
	public bool deathSkillPass = false;
	//PVP
	public List<Mikan.CSAGA.ConstData.Skill> PVPSkills = new List<Mikan.CSAGA.ConstData.Skill>();
	public List<string> PVPTalks = new List<string>();

	public bool CDFinished = false;
	public IFightCommand inCDCommand = null;

	public void Init(int index, Mob data, float initX)
	{
		mobData = data;

		EnemyID = data.n_ID;
		Index = index;
		Side = SIDE.ENEMY;

		MaxHp = data.n_HP;
		Hp = MaxHp;
		Atk = data.n_ATK;
		Spd = data.f_CHARGE;
		LifeStatus = LIFE_STATUS.LIVE;

		Element = (FightUnit.ELEMENT)data.n_TYPE;
		Type = 32767;
		texName = data.s_FILENAME;

		preSkillID = data.n_FIRST_SKILL;
		TalkID = data.n_FIRST_WORD;
		deathSkillID = data.n_DEATH_SKILL;
		deathTalkID = data.n_DEATH_WORD;

		initPosX = initX;

		//AI
		AIData = new Dictionary<int, MobAI>();
		int minID = 999999;
		foreach(MobAI ai in ConstData.Tables.MobAI.Select(o=>{ return o.n_GROUP == data.n_MOBAI; } ))
		{
			AIData.Add(ai.n_ID, ai);
			if(ai.n_ID < minID)
				minID = ai.n_ID;
		}
		aiIndex = minID;
		firstAI = minID;

		//Const Variable
		BonusCount = 0;
		FinalHP = MaxHp;
		FinalATK = Atk;
		FinalREV = Rev;
		MAX_BONUS_COUNT = 0;
		BONUS_EFFECT = 0;
	}

	public void Init(int index, FightUnit unit, float initX)
	{
		copyData = unit;
		
		Index = index;
		Side = SIDE.ENEMY;
		
		MaxHp = unit.Hp;
		Hp = MaxHp;
		Atk = unit.Atk;
		Rev = unit.Rev;
		Spd = unit.Spd;
		LifeStatus = LIFE_STATUS.LIVE;
		
		Element = unit.Element;
		Type = unit.Type;
		texName = unit.texName;

		initPosX = initX;

		//Const Variable
		BonusCount = 0;
		FinalHP = MaxHp;
		FinalATK = Atk;
		FinalREV = Rev;
		MAX_BONUS_COUNT = 0;
		BONUS_EFFECT = 0;
	}

	public void Init(int index, PVPMob mob, float initX)
	{
		Mikan.CSAGA.ConstData.CardData card = ConstData.Tables.Card[mob.CardID];

		Index = index;
		Side = SIDE.ENEMY;
		
		MaxHp = mob.HP;
		Hp = MaxHp;
		Atk = mob.Atk;
		Rev = mob.Rev;
		Spd = mob.Charge;
		LifeStatus = LIFE_STATUS.LIVE;
		
		Element = (FightUnit.ELEMENT)card.n_TYPE;
		Type = 32767;
		texName = card.s_FILENAME;
		initPosX = initX;

		PVPSkills.Clear();
		foreach(int id in mob.Skills)
		{
			PVPSkills.Add(ConstData.Tables.Skill[id]);
		}
		PVPTalks = new List<string>(mob.Prologs);

		//Const Variable
		BonusCount = 0;
		FinalHP = MaxHp;
		FinalATK = Atk;
		FinalREV = Rev;
		MAX_BONUS_COUNT = 0;
		BONUS_EFFECT = 0;
	}

	public int AddHp(int add)
	{
		Hp += add;
		if(Hp > MaxHp)
			Hp = MaxHp;
		else if(Hp <= 0)
		{
			Hp = 0;
			LifeStatus = LIFE_STATUS.DEAD;
		}

		return Hp;
	}

}
