﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using Mikan;
using Mikan.CSAGA.Data;


public class Buff {

	public enum BUFF_TYPE
	{
		POWER_UP = 1,
		GUARD = 2,
		SPEED_UP = 3,
		ABSORB = 4,
		LINK = 5,
		CHAIN_SHIELD = 6,
		ATK_ADD_DEF = 7,
		SPEED_DOWN = 101,
		STUN = 102,
		BOUNCE = 103,
		WEAK = 104,
		SILENT = 105,
		BOARD_DROP = 201,
		BOARD_UP = 202,
	}

	public List<int> ReplaceByXList = new List<int>(){ 1, 3, 101 };

	public int BuffID;
	public float XValue;
	public float YValue;
	public float ZValue;
	public float TimeLeft;
	public float ReplaceValue = 0f;
	public bool NeedRefresh = true;

	public Buff(int id, float x, float y, float z, float time)
	{
		BuffID = id;
		XValue = x;
		YValue = y;
		ZValue = z;
		TimeLeft = time;
		if(ReplaceByXList.Contains(BuffID))
			ReplaceValue = XValue;
	}
}

public class FightManager {

	public const int MAX_ELEMENT_COUNT = 6;
	public const int MAX_TYPE_COUNT = 4;

	public enum PASSIVE_TRIGGER
	{
		PLAYER_HIT = 1,
		ACT_ITEM = 2,
		WAVE_START = 3,
		ATTACK_HP = 4,
		HEAL_HP = 5,
		ACTIVE_SKILL = 12,
		ACTION_FULL = 13,
		BUFF = 17,
		ACT_COMBO = 18,
	}

	private static FightManager instance = null;
	public static FightManager Instance
	{
		get{
			if(instance == null)
				instance = new FightManager();
			return instance;
		}
	}

	public FightPlayManager playManager;

	//Data
	public int HP;
	public int MaxHP;

	public int TargetIndex = 0;

	public Dictionary<int, FightUnit> UnitDict;
	public Dictionary<int, FightEnemy> EnemyDict;

	public List<int> GroupBuffList;
	public Dictionary<int, Buff> PlayerBuffDict;
	public Dictionary<int, Buff> EnemyBuffDict;
	//Init
	public int QuestID;
	private Action FightInitHandler;
	private int TotalCount;
	private int CompleteCount;
	private bool isReset = false;

	//AI
	private EnemyAISystem aiSystem;

	//Fight Ending
	public int Round;
	public int TotalRounds;
	public bool FightEnd = false;
	private Action<bool> FightEndHandler;
	private Action<bool> FightFaildHandler;
	private bool IsWin = false;

	//Queue
	private List<LiveOperationQueue> lstAllQueue;

	//Tutorials
	public Dictionary<int, List<TeachContent>> teachDict;

	//QTE
	public bool QTEMode = false;
	public bool QTEClear = false;
	public bool QTEShield = false;

	//JIT Leader Skills
	public List<Mikan.CSAGA.ConstData.Skill> JITLeaderSkillList = new List<Mikan.CSAGA.ConstData.Skill>();
	public float TimePower = 0f;
	public List<int> LSCollectList = new List<int>();

	//Timer
	public bool needTimer = false;
	public float startTime = 0f;
	public float totalTime = 0f;

	//Chain
	public float chainTimeMax;
	public float chainTime;
	public int chain;

	//Tactics
	public List<Mikan.CSAGA.ConstData.Tactics> TacticList = new List<Mikan.CSAGA.ConstData.Tactics>();

	//Challenge
	public List<Mikan.CSAGA.ConstData.Challenge> ChallengeList = new List<Mikan.CSAGA.ConstData.Challenge>();
	public int TotalDamage;
	public int MaxOutput;
	public Dictionary<int, int> ItemUseDict = new Dictionary<int, int>();
	public int MaxChainCount;
	public int PowerSkillUse;

	//PVP
	public int PVPRound = 0;

	//Flags
	public bool isFXOn = true;
	public bool isChainOn;
	public bool isPVP = false;

	public List<int> powerList = new List<int>();

	//Constructor
	public FightManager()
	{
		//Debug.Log("********New FightManager");
		UnitDict = new Dictionary<int, FightUnit>();
		EnemyDict = new Dictionary<int, FightEnemy>();
		PlayerBuffDict = new Dictionary<int, Buff>();
		EnemyBuffDict = new Dictionary<int, Buff>();
		lstAllQueue = new List<LiveOperationQueue>();
		teachDict = new Dictionary<int, List<TeachContent>>();

		GroupBuffList = new List<int>();
		GroupBuffList.Add(2);
		GroupBuffList.Add(201);
		GroupBuffList.Add(202);

		aiSystem = new EnemyAISystem();

		Message.AddListener(MessageID.EndQuest, ClearFight);
	}


#region Init functions.

	public void SetPlayManager(FightPlayManager manager)
	{
		playManager = manager;

	}
	
	public void SetAllyData(int teamIndex)
	{
		if(UnitDict.Count > 0)
			UnitDict.Clear();

		var team = GameSystem.Service.TeamInfo[teamIndex];
		
		for(int i = 0; i < team.Members.Count; i++)
		{
			//Debug.Log("team.Members " + i + ":" + team.Members[i]);
			if(!team.Members[i].IsEmpty)
			{
				Card c = GameSystem.Service.CardInfo[team.Members[i]];
				if(c != null)
				{
					FightUnit unit = new FightUnit();
					unit.Init(i + 1, c);
					UnitDict.Add(i + 1, unit);
				}
			}
		}
		//Leader Skills
		JITLeaderSkillList.Clear();

		FightUnit leader1 = GetUnit(1);
		if(leader1 != null && leader1.LeaderSkill != null)
			ApplyLeaderSkillOfUnit(leader1.LeaderSkill);
		FightUnit leader2 = GetUnit(2);
		if(leader2 != null && leader2.LeaderSkill != null)
			ApplyLeaderSkillOfUnit(leader2.LeaderSkill);
		foreach(FightUnit unit in UnitDict.Values)
		{
			unit.CalculateTotalEffect();
		}

		MaxHP = GetTotalHP();
		HP = MaxHP;

	}

	private void ApplyLeaderSkillOfUnit(Mikan.CSAGA.ConstData.Skill leaderSkill)
	{
		//Detect is Trigger.
		bool isTrigger = false;
		bool needFilter = false;
		List<FightUnit> lstAlly = GetAllUnit();
		switch(leaderSkill.n_TRIGGER)
		{
			case 6:	//Element
			{
				needFilter = false;
				List<FightUnit.ELEMENT> lstElement = new List<FightUnit.ELEMENT>();
				for(int i = 0; i < lstAlly.Count; i++)
				{
					FightUnit unit = lstAlly[i];
					if(!lstElement.Contains(unit.Element))
						lstElement.Add(unit.Element);
				}
				//Have X kind of Element
				if(lstElement.Count >= leaderSkill.n_TRIGGER_X)
				{
					if(leaderSkill.n_TRIGGER_Y == 0)
						isTrigger = true;
					else
					{
						//Get Needed Element
						List<FightUnit.ELEMENT> lstNeed = new List<FightUnit.ELEMENT>();
						for(int i = 0; i < MAX_ELEMENT_COUNT; i++)
						{
							int element = (int)Math.Pow(2, i);
							if((leaderSkill.n_TRIGGER_Y & element) != 0)
								lstNeed.Add((FightUnit.ELEMENT)element);
						}
						//Need Specific Elements.
						bool existAllNeeded = true;
						for(int i = 0; i < lstNeed.Count; i++)
						{
							if(!lstElement.Contains(lstNeed[i]))
							{
								existAllNeeded = false;
								break;
							}
						}
						isTrigger = existAllNeeded;
					}
				}
				else
					isTrigger = false;
				break;
			}
			
			case 7:	//Type
			{
				needFilter = false;
				List<int> lstType = new List<int>();
				for(int i = 0; i < lstAlly.Count; i++)
				{
					FightUnit unit = lstAlly[i];
					if(!lstType.Contains(unit.Type))
						lstType.Add(unit.Type);
				}
				//Have X kind of Type
				if(lstType.Count >= leaderSkill.n_TRIGGER_X)
				{
					if(leaderSkill.n_TRIGGER_Y == 0)
						isTrigger = true;
					else
					{
						//Get Needed Type
						List<int> lstNeed = new List<int>();
						for(int i = 0; i < MAX_TYPE_COUNT; i++)
						{
							int type = (int)Math.Pow(2, i);
							if((leaderSkill.n_TRIGGER_Y & type) != 0)
								lstNeed.Add(type);
						}
						//Need Specific Elements.
						bool existAllNeeded = true;
						for(int i = 0; i < lstNeed.Count; i++)
						{
							if(!lstType.Contains(lstNeed[i]))
							{
								existAllNeeded = false;
								break;
							}
						}
						isTrigger = existAllNeeded;
					}
				}
				else
					isTrigger = false;
				break;
			}
				
			case 15:	//Speed Lower
			{
				needFilter = true;
				int count = 0;
				for(int i = 0; i < lstAlly.Count; i++)
				{
					FightUnit unit = lstAlly[i];
					if(unit.Spd <= (float)leaderSkill.n_TRIGGER_Y / 100f)
						count++;
				}
				isTrigger = (count >= leaderSkill.n_TRIGGER_X);
				break;
			}

			case 16:	//Speed Upper
			{
				needFilter = true;
				int count = 0;
				for(int i = 0; i < lstAlly.Count; i++)
				{
					FightUnit unit = lstAlly[i];
					if(unit.Spd >= (float)leaderSkill.n_TRIGGER_Y / 100f)
						count++;
				}
				isTrigger = (count >= leaderSkill.n_TRIGGER_X);
				break;
			}

			case 8:
			case 9:
			case 10:
			case 11:
			case 12:
			case 14:
				JITLeaderSkillList.Add(leaderSkill);
				break;

			default:
				isTrigger = true;
				needFilter = true;
				break;
		}
		
		if(isTrigger)
		{
			//Debug.Log("*****Trigger leaderskill:" + leaderSkill.n_ID);
			//Add to Ally.
			foreach(FightUnit unit in UnitDict.Values)
			{
				if(needFilter)
				{
					if((leaderSkill.n_TARGET_X == 0 || (leaderSkill.n_TARGET_X & (int)unit.Element) != 0)
					   && (leaderSkill.n_TARGET_Y == 0 || (leaderSkill.n_TARGET_Y & unit.Type) != 0))
						unit.AddLeaderSkillEffect(leaderSkill);
				}
				else
					unit.AddLeaderSkillEffect(leaderSkill);
			}
		}

		//skill link
		if(leaderSkill.n_SKILL_LINK > 0)
		{
			Mikan.CSAGA.ConstData.Skill linkSkill = ConstData.Tables.Skill[leaderSkill.n_SKILL_LINK];
			if(linkSkill != null)
				ApplyLeaderSkillOfUnit(linkSkill);
		}
	}

	public void SetEnemyData(int eventId)
	{
		if(EnemyDict.Count > 0)
			ClearEnemyData();

		QTEMode = false;
		QTEClear = false;
		if(!isPVP)
		{
			Mikan.CSAGA.ConstData.QuestEvent qEvent = ConstData.Tables.QuestEvent[eventId];
			if(qEvent != null)
			{
				int[] modIds = qEvent.n_VALUE_;
				int[] modPos = qEvent.n_POS;

				for(int i = 0; i < modIds.Length; i++)
				{
					int id = modIds[i];
					int x = modPos[i];
					if(id > 0)
					{
						Mikan.CSAGA.ConstData.Mob mobData = ConstData.Tables.Mob[id];
						if(mobData != null)
						{
							FightEnemy enemy = new FightEnemy();
							enemy.Init(i + 1, mobData, x);
							EnemyDict.Add(i + 1, enemy);
							if(mobData.n_CLASS == 3)
								QTEMode = true;
						}
					}
				}
			}
		}
		else
		{
			PVPWaveItemData pvpData = Context.Instance.PVPWaves.List[PVPRound];
			FightEnemy enemy = new FightEnemy();
			enemy.Init(1, pvpData.Convert(), 0f);
			EnemyDict.Add(1, enemy);

		}
	}

	public void Init(int round, int total_round, int questId, Action callback, Action<bool> endCallback, Action<bool> failCallback)
	{
		Debug.Log("Fight Init");

		isChainOn = (ConstData.Tables.GetVariable(Mikan.CSAGA.VariableID.CHAIN_ENABLE) > 0);

		isReset = false;
		FightEnd = false;
		QuestID = questId;
		Round = round;
		TotalRounds = total_round;
		PVPRound = round + 1;

		FightEndHandler = endCallback;
		FightFaildHandler = failCallback;
		FightInitHandler = callback;

		PlayerBuffDict.Clear();
		EnemyBuffDict.Clear();
		powerList.Clear();
		TacticList.Clear();

		//Challenge
		TotalDamage = 0;
		MaxOutput = 0;
		MaxChainCount = 0;
		PowerSkillUse = 0;
		ItemUseDict.Clear();
		ChallengeList.Clear();
		if(!isPVP)
		{
			foreach(Mikan.CSAGA.ConstData.Challenge cData in ConstData.Tables.Challenge.Select(o=> { return o.n_STAGEID == questId; }))
			{
				ChallengeList.Add(cData);
			}
		}

		TotalCount = UnitDict.Count + EnemyDict.Count + 1;	//1 for background
		CompleteCount = 0;

		for(int i = 1; i <= 5; i++)
		{
			if(UnitDict.ContainsKey(i))
				playManager.InitChar(i, UnitDict[i], TexInitComplete);
			else
				playManager.InitChar(i, null, null);
		}
		playManager.UpdateHP();

		int enemyCount = EnemyDict.Count;
		foreach(FightEnemy enemy in EnemyDict.Values)
		{
			playManager.InitEnemy(enemy, enemyCount, TexInitComplete);
		}

		string bg = "bg_battle_001";
		int boardId = 1;
		Mikan.CSAGA.ConstData.QuestDesign data = ConstData.Tables.QuestDesign[questId];
		if(data != null)
		{
			//background
			bg = data.s_BG;
			boardId = data.n_BOARD;
			//timer
			if(!isPVP && data.n_QUEST_TYPE == Mikan.CSAGA.QuestType.Schedule && data.n_COUNT == 0)
			{
				needTimer = true;
				totalTime = ConstData.Tables.GetVariablef(Mikan.CSAGA.VariableID.TOWER_COUNTDOWN);
			}
		}
		startTime = Time.realtimeSinceStartup;

		//Tactic data
		Dictionary<int, int> dict = new Dictionary<int, int>();
		//Find max lv
		foreach(int tacticId in GameSystem.Service.TacticsInfo.List)
		{
			Mikan.CSAGA.ConstData.Tactics tactic = ConstData.Tables.Tactics[tacticId];
			if(tactic != null)
			{
				if(!dict.ContainsKey(tactic.n_GROUP))
					dict.Add(tactic.n_GROUP, tactic.n_LV);
				else
				{
					int lv = dict[tactic.n_GROUP];
					if(tactic.n_LV > lv)
						dict[tactic.n_GROUP] = tactic.n_LV;
				}
			}
		}
		
		foreach(KeyValuePair<int, int> pair in dict)
		{
			Mikan.CSAGA.ConstData.Tactics tactic = ConstData.Tables.Tactics.SelectFirst(o=>{ return o.n_GROUP == pair.Key && o.n_LV == pair.Value; });
			if(tactic != null)
			{
				TacticList.Add(tactic);
			}
		}

		//Apply tactic effect to properties.
		foreach(FightUnit unit in UnitDict.Values)
		{
			unit.ApplyTacticEffect(TacticList);
		}
		MaxHP = GetTotalHP();
		HP = MaxHP;

		playManager.dashBoard.Init(boardId);
		playManager.InitLeaderSkillIcon(JITLeaderSkillList);
		playManager.InitBackground(bg);
	}

	public void TexInitComplete()
	{
		CompleteCount++;
		if(CompleteCount == TotalCount)
		{
			PreloadRes();
		}
	}

	private void PreloadRes()
	{
		List<string> lstFX = new List<string>();
		List<string> lstTex = new List<string>();
		if(!isReset)
		{
			//Preload fx.
			lstFX.Add("UpgradeFX");
			lstFX.Add("DowngradeFX");
			lstFX.Add("ActionFX");
			lstFX.Add("ChangeFX");
			lstFX.Add("BuffFX");
			lstFX.Add("DebuffFX");
			lstFX.Add("StunFX");
			lstFX.Add("HealFX");
			lstFX.Add("HitFX");
			lstFX.Add("CircleLightFX");
			lstFX.Add("QTECheckFX");
			lstFX.Add("BulletFX");
			lstFX.Add("SpeedFX");
			lstFX.Add("AddActionFX");
			lstFX.Add("PassiveFX");
			lstFX.Add("SoulFX");	//for cure stun.
			//Preload card texture.
			foreach(FightUnit unit in UnitDict.Values)
			{
				string name = "CARD/CARD_" + unit.texName;
				if(!lstTex.Contains(name))
					lstTex.Add(name);
			}
			//Preload tutorial texture.
			if(FightManager.Instance.QuestID == 1)
			{
				lstTex.Add("TUTORIAL/battle_tutorial01");
				lstTex.Add("TUTORIAL/battle_tutorial02");
				lstTex.Add("TUTORIAL/battle_tutorial03");
				lstTex.Add("TUTORIAL/battle_tutorial04");
				lstTex.Add("TUTORIAL/battle_tutorial05");
			}
		}
		foreach(FightEnemy enemy in EnemyDict.Values)
		{
			string name = "CARD/CARD_" + enemy.texName;
			if(!lstTex.Contains(name))
				lstTex.Add(name);
		}

		playManager.PreloadResources(lstFX, lstTex, FightInitHandler);
	}

	public void Reset(int round, Action callback, Action<bool> endCallback, Action<bool> failCallback)
	{
		isReset = true;
		FightEnd = false;
		Round = round;
		PVPRound = round + 1;

		FightEndHandler = endCallback;
		FightFaildHandler = failCallback;
		FightInitHandler = callback;

		playManager.ResetBackground();

		TotalCount = EnemyDict.Count;
		CompleteCount = 0;

		int enemyCount = EnemyDict.Count;
		foreach(FightEnemy enemy in EnemyDict.Values)
		{
			playManager.InitEnemy(enemy, enemyCount, TexInitComplete);
		}

	}

#endregion


#region Public Utility Functions.
	public FightUnit GetUnit(int index)
	{
		FightUnit unit = null;
		if(UnitDict.ContainsKey(index))
			unit = UnitDict[index];

		return unit;
	}

	public List<FightUnit> GetAllUnit()
	{
		List<FightUnit> lst = new List<FightUnit>();
		foreach(FightUnit unit in UnitDict.Values)
		{
			lst.Add(unit);
		}
		return lst;
	}

	public FightEnemy GetEnemy(int index)
	{
		FightEnemy enemy = null;
		if(EnemyDict.ContainsKey(index))
			enemy = EnemyDict[index];

		return enemy;
	}

	public List<FightEnemy> GetAllEnemy(bool ignoreDead)
	{
		List<FightEnemy> lst = new List<FightEnemy>();
		foreach(FightEnemy enemy in EnemyDict.Values)
		{
			if(!ignoreDead || enemy.LifeStatus == FightEnemy.LIFE_STATUS.LIVE)
				lst.Add(enemy);
		}
		return lst;
	}

	public List<BaseUnit> GetAllBaseBySide(BaseUnit.SIDE side, bool ignoreDead = false)
	{
		List<BaseUnit> lst = new List<BaseUnit>();
		if(side == BaseUnit.SIDE.ALLY)	//Player
		{
			foreach(FightUnit unit in UnitDict.Values)
			{
				lst.Add(unit);
			}
		}
		else
		{
			//Enemy
			foreach(FightEnemy enemy in EnemyDict.Values)
			{
				if(!ignoreDead || enemy.LifeStatus == FightEnemy.LIFE_STATUS.LIVE)
					lst.Add(enemy);
			}
		}

		return lst;
	}

	public int GetTotalHP()
	{
		int hp = 0;
		foreach(FightUnit unit in UnitDict.Values)
		{
			hp += unit.FinalHP;
		}
		return hp;
	}

	public FightEnemy GetTarget()
	{
		FightEnemy target = GetEnemy(TargetIndex);
		if(target != null && target.LifeStatus == FightEnemy.LIFE_STATUS.LIVE)
			return target;
		else
		{
			foreach(FightEnemy enemy in EnemyDict.Values)
			{
				target = enemy;	//Random a target for return.
				if(enemy.LifeStatus == FightEnemy.LIFE_STATUS.LIVE)
					return enemy;
			}
		}
		return target;
	}

	public void DetectFightEnd()
	{
		if(FightEnd) return;

		if(HP <= 0)
		{
			FightEnd = true;
			IsWin = false;
			playManager.EnableTouch(false);
			playManager.EnableSystemButton(false);
			playManager.FightStop();
			ClearAllQueue();
			if(isPVP)
				playManager.FightLosePlay(CallQuestEnd);
			else
				playManager.FightLosePlay(RequestRevive);
		}
		else
		{
			bool win = true;
			foreach(FightEnemy enemy in EnemyDict.Values)
			{
				if(enemy.LifeStatus == FightEnemy.LIFE_STATUS.LIVE)
				{
					win = false;
					break;
				}
			}
			if(win)
			{
				FightEnd = true;
				IsWin = true;
				playManager.EnableTouch(false);
				playManager.EnableSystemButton(false);
				playManager.FightStop();
				ClearAllQueue();
				//Challenge
				if(Round == TotalRounds - 1)
					SetChallenge();

				FightEndPlay();
			}
		}
	}

	public void RequestRevive()
	{
		//NGUITools.SetActive(playManager.endButton.gameObject, false);
		//Message.Send(MessageID.Continue);

		var data = ConstData.Tables.QuestDesign[QuestID];

		if(data.n_REVIVE == 0)
		{
			if(data.n_QUEST_TYPE == Mikan.CSAGA.QuestType.Schedule && data.n_COUNT == 0)
			{
				needTimer = false;
				MessageBox.ShowConfirmWindow(ConstData.GetSystemText(SystemTextID._430_QUEST_GET_BACK), (res)=>
				{
					if(res == 1) GameSystem.Service.StayAtCurrentLevel();
					CallQuestEnd();
				});
			}
			else
				CallQuestEnd();
		}
		else
			Message.Send(MessageID.Continue);
	}

	public void OnRevive(bool isSucc)
	{
		if(isSucc)
		{
			playManager.RevivePlay();
			Revive();
		}
		else
		{
			//playManager.LeaveFightPlay(CallQuestEnd);
			CallQuestEnd();	//Fade out by Quest system.
		}
	}

	public void ManageQueue(LiveOperationQueue queue)
	{
		lstAllQueue.Add(queue);
	}

	public FXController PlayFX(string fxName, Vector3 pos, GameObject parent = null)
	{
		if(isFXOn)
			return playManager.fxLoader.PlayFX(fxName, pos, parent);
		else
			return null;
	}

	public FXController FlyFX(string fxName, Vector3 pos, GameObject fromParent, GameObject toParent, float duration, bool upToDown)
	{
		return playManager.fxLoader.FlyFX(fxName, pos, fromParent, toParent, duration, upToDown);
	}
	
#endregion


#region Command Functions.
	public void CharAttack(int charId, int enemyId, int lv, ActItem.ACT_TYPE triggerType)
	{
		FightUnit unit = GetUnit(charId);
		FightEnemy enemy = GetEnemy(enemyId);

		SendCommand(new CharAttackCommand(unit, enemy, lv, triggerType));
	}

	public void CharHeal(int charId, int lv, ActItem.ACT_TYPE triggerType)
	{
		FightUnit unit = GetUnit(charId);

		SendCommand(new CharHealCommand(unit, lv, triggerType));
	}

	public void CharSkill(int charId, int lv, ActItem.ACT_TYPE triggerType)
	{
		FightUnit unit = GetUnit(charId);

		SendCommand(new CharSkillCommand(unit, false, lv, triggerType));
	}

	public void PassiveSkill(FightUnit unit, List<Mikan.CSAGA.ConstData.Skill> lst)
	{
		SendCommand(new CharSkillCommand(unit, lst));
	}

	public void CharPowerUp(int charId, int lv)
	{
		FightUnit unit = GetUnit(charId);

		SendCommand(new CharPowerUpCommand(unit, lv));
	}

	public void CharPoison(int lv)
	{
		SendCommand(new PoisonCommand(lv));
	}

	public void ActiveSkill(int charId)
	{
		FightUnit unit = GetUnit(charId);
		
		SendCommand(new CharSkillCommand(unit, true, 1, ActItem.ACT_TYPE.Useless));
	}

	public void RunEnemyAI(FightEnemy enemy)
	{
		if(QTEMode)
			playManager.QTEResultPlay();
		else
		{
			if(enemy.CDFinished && enemy.inCDCommand != null)
			{
				enemy.CDFinished = false;
				SendCommand(enemy.inCDCommand);
			}
			else
			{
				if(isPVP)
					SendCommand(new EnemyHealCommand(enemy));

				IFightCommand cmd = aiSystem.GetEnemyBehavior(enemy);
				if(cmd != null)
					SendCommand(cmd);
			}
		}
	}

	public void EndQTE()
	{
		SendCommand(new EndQTECommand());
	}

	public void SendCommand(IFightCommand cmd)
	{
		cmd.Execute();
	}

	public int AddHp(int add)
	{
		HP += add;
		if(HP > MaxHP)
			HP = MaxHP;
		else if(HP < 0)
			HP = 0;

		if(add < 0)
			TotalDamage -= add;

		return HP;
	}

	public void RefreshMaxHp()
	{
		MaxHP = GetTotalHP();
		//playManager.UpdateHP();
	}

	public void AddBuff(BaseUnit.SIDE side, int index, Buff b)
	{
		Dictionary<int, Buff> dict;
		BaseUnit unit;
		//Side
		if(side == BaseUnit.SIDE.ALLY)
		{
			dict = PlayerBuffDict;
			unit = GetUnit(index);
		}
		else
		{
			dict = EnemyBuffDict;
			unit = GetEnemy(index);
		}
		//Buff type
		if(GroupBuffList.Contains(b.BuffID))
		{
			if(dict.ContainsKey(b.BuffID))
			{
				Buff buff = dict[b.BuffID];
				if(b.ReplaceValue >= buff.ReplaceValue)
					dict[b.BuffID] = b;
			}
			else
				dict.Add(b.BuffID, b);
			//Update UI
			playManager.UpdateGroupBuffIcon(side);
		}
		else
		{
			unit.AddBuff(b);
			if(side == BaseUnit.SIDE.ALLY)
				TriggerPassiveSkillByBuff(index, b.BuffID);
			//Update UI
			playManager.UpdateBuffIconByChar(unit.Side, unit.Index);
		}
	}

	public Buff GetGroupBuff(BaseUnit.SIDE side, int id)
	{
		Dictionary<int, Buff> dict = PlayerBuffDict;
		if(side == BaseUnit.SIDE.ENEMY)
			dict = EnemyBuffDict;

		if(dict.ContainsKey(id))
			return dict[id];
		else
			return null;
	}

	public bool ConsumeBuff(float sec)
	{
		bool needUpdate = false;

		//Player 
		List<int> lstDelP = new List<int>();
		foreach(Buff b in PlayerBuffDict.Values)
		{
			b.TimeLeft -= sec;
			if(b.TimeLeft <= 0)
			{
				lstDelP.Add(b.BuffID);
				needUpdate = true;
			}
		}
		//Buff finished
		for(int i = 0; i < lstDelP.Count; i++)
		{
			int id = lstDelP[i];
			PlayerBuffDict.Remove(id);
		}

		//Enemy
		List<int> lstDelE = new List<int>();
		foreach(Buff b in EnemyBuffDict.Values)
		{
			b.TimeLeft -= sec;
			if(b.TimeLeft <= 0)
			{
				lstDelE.Add(b.BuffID);
				needUpdate = true;
			}
		}
		//Buff finished
		for(int i = 0; i < lstDelE.Count; i++)
		{
			int id = lstDelE[i];
			EnemyBuffDict.Remove(id);
		}

		return needUpdate;
	}

	public void Revive()
	{
		MaxHP = GetTotalHP();
		HP = MaxHP;
		playManager.UpdateHP();

		playManager.EnableSystemButton(true);
		playManager.FightStart();
		FightEnd = false;

		DetectFightEnd();
	}
	
	public void ClearEnemyData()
	{
		playManager.ClearEnemy();
		EnemyDict.Clear();
	}
	
	public void ClearFight()
	{
		Time.timeScale = 1f;
		Message.RemoveListener(MessageID.EndQuest, ClearFight);
		instance = null;
	}

	public void TriggerPassiveSkill(PASSIVE_TRIGGER trigger, BaseUnit.ELEMENT element = BaseUnit.ELEMENT.NONE)
	{
		foreach(FightUnit unit in UnitDict.Values)
		{
			List<Mikan.CSAGA.ConstData.Skill> lst = new List<Mikan.CSAGA.ConstData.Skill>();
			if(unit.GetBuff((int)Buff.BUFF_TYPE.STUN) == null)
			{
				for(int i = 0; i < unit.PassiveSkillList.Count; i++)
				{
					Mikan.CSAGA.ConstData.Skill skillData = unit.PassiveSkillList[i];
					if(skillData.n_EFFECT != 8 && skillData.n_TRIGGER == (int)trigger && Mikan.MathUtils.Random.Next(0, 100) < skillData.n_TRIGGER_RATE)
					{
						if(trigger == PASSIVE_TRIGGER.PLAYER_HIT)
						{
							if((skillData.n_TRIGGER_X & (int)element) != 0)
								lst.Add(skillData);
						}
						else
							lst.Add(skillData);
					}
				}
			}

			if(lst.Count > 0)
				PassiveSkill(unit, lst);
		}
	}

	public void TriggerPassiveSkillByItem(int index, ActItem.ACT_TYPE type, int lv)
	{
		FightUnit unit = GetUnit(index);
		if(unit != null)
		{
			//unit.LogAction(type);
			List<Mikan.CSAGA.ConstData.Skill> lst = new List<Mikan.CSAGA.ConstData.Skill>();
			for(int i = 0; i < unit.PassiveSkillList.Count; i++)
			{
				Mikan.CSAGA.ConstData.Skill skillData = unit.PassiveSkillList[i];
				if(skillData.n_EFFECT != 8 && skillData.n_TRIGGER == (int)PASSIVE_TRIGGER.ACT_ITEM && (skillData.n_TRIGGER_X & (int)type) > 0  
				   && lv >= (int)((float)skillData.n_TRIGGER_Y / 10f) && lv <= (skillData.n_TRIGGER_Y % 10)
				   && Mikan.MathUtils.Random.Next(0, 100) < skillData.n_TRIGGER_RATE)
					lst.Add(skillData);

				if(skillData.n_EFFECT != 8 && skillData.n_TRIGGER == (int)PASSIVE_TRIGGER.ACT_COMBO && skillData.n_TRIGGER_X == (int)type  
				   && unit.ActionLogList.Count > 1 && Mikan.MathUtils.Random.Next(0, 100) < skillData.n_TRIGGER_RATE)
					lst.Add(skillData);
			}
			
			if(lst.Count > 0)
				PassiveSkill(unit, lst);
		}
	}

	public void TriggerPassiveSkillById(int index, PASSIVE_TRIGGER trigger)
	{
		FightUnit unit = GetUnit(index);
		if(unit != null)
		{
			List<Mikan.CSAGA.ConstData.Skill> lst = new List<Mikan.CSAGA.ConstData.Skill>();
			for(int i = 0; i < unit.PassiveSkillList.Count; i++)
			{
				Mikan.CSAGA.ConstData.Skill skillData = unit.PassiveSkillList[i];
				if(skillData.n_EFFECT != 8 && skillData.n_TRIGGER == (int)trigger && Mikan.MathUtils.Random.Next(0, 100) < skillData.n_TRIGGER_RATE)
				{
					lst.Add(skillData);
				}
			}
			
			if(lst.Count > 0)
				PassiveSkill(unit, lst);
		}
	}

	public void TriggerPassiveSkillByBuff(int index, int buffId)
	{
		FightUnit unit = GetUnit(index);
		if(unit != null)
		{
			List<Mikan.CSAGA.ConstData.Skill> lst = new List<Mikan.CSAGA.ConstData.Skill>();
			if(unit.GetBuff((int)Buff.BUFF_TYPE.STUN) == null)
			{
				for(int i = 0; i < unit.PassiveSkillList.Count; i++)
				{
					Mikan.CSAGA.ConstData.Skill skillData = unit.PassiveSkillList[i];
					if(skillData.n_EFFECT != 8 && skillData.n_TRIGGER == (int)PASSIVE_TRIGGER.BUFF && Mikan.MathUtils.Random.Next(0, 100) < skillData.n_TRIGGER_RATE)
					{
						if(skillData.n_TRIGGER_X == buffId)
							lst.Add(skillData);
					}
				}
			}
			
			if(lst.Count > 0)
				PassiveSkill(unit, lst);
		}
	}

	public bool ResistDebuff(int index, int buffId)
	{
		FightUnit unit = GetUnit(index);
		if(unit != null)
		{
			if(!unit.ResistDebuffList.ContainsKey(buffId))
				return false;
			else
				return (unit.ResistDebuffList[buffId] > Mikan.MathUtils.Random.Next(0, 100));
		}

		return false;
	}

	public void ChainAdd(int add = 1)
	{
		if(isChainOn)
		{
			chain += add;
			playManager.ChainJump();

			//Max time
			float chainActive = ConstData.Tables.GetVariablef(Mikan.CSAGA.VariableID.CHAIN_ACTIVE);
			chainTimeMax = ConstData.Tables.GetVariablef(Mikan.CSAGA.VariableID.CHAIN_DURATION);
			//Tactic effect
			foreach(Mikan.CSAGA.ConstData.Tactics tactic in TacticList)
			{
				if(tactic.n_EFFECT == Mikan.CSAGA.TacticsEffect.Chain)
				{
					chainTimeMax += (float)tactic.n_EFFECT_Z / 10f;
				}
			}

			if(chain > chainActive)
			{
				chainTimeMax -= (chain - chainActive) * ConstData.Tables.GetVariablef(Mikan.CSAGA.VariableID.CHAIN_DURATION_LOST);
				float chainMin = ConstData.Tables.GetVariablef(Mikan.CSAGA.VariableID.CHAIN_DURATION_MIN);
				if(chainTimeMax < chainMin)
					chainTimeMax = chainMin;
			}

			chainTime = chainTimeMax;
			NGUITools.SetActive(playManager.chainObj, true);
			playManager.RefreshChain();

			if(chain > MaxChainCount)
				MaxChainCount = chain;
		}
	}
	
#endregion

#region private functions.
	private void FightEndPlay()
	{
		if(Round == TotalRounds - 1)
			playManager.StartCoroutine(playManager.FightWinPlay());
		else
			playManager.StartCoroutine(playManager.RoundChange(CallQuestEnd));
	}

	public void LeaveFight()
	{
		//playManager.LeaveFightPlay(CallQuestEnd);
		CallQuestEnd();	//Fade out by Quest system.
	}

	private void CallQuestEnd()
	{
		//if(Round == TotalRounds - 1)
		//	NGUITools.SetActive(playManager.gameObject, false);
		if(QTEMode && !QTEClear)
			FightFaildHandler(true);
		else
			FightEndHandler(IsWin);
	}

	private void ClearAllQueue()
	{
		for(int i = 0; i < lstAllQueue.Count; i++)
		{
			LiveOperationQueue queue = lstAllQueue[i];
			if(queue != null)
				queue.Clear();
		}
		lstAllQueue.Clear();
	}

	void SetChallenge()
	{
		foreach(Mikan.CSAGA.ConstData.Challenge data in ChallengeList)
		{
			if(data == null) continue;

			switch(data.n_TYPE)
			{
			case 1:	//Element
			{
				int element = data.n_TYPE_[0];
				int count = 0;
				foreach(FightUnit unit in UnitDict.Values)
				{
					if(unit == null) continue;

					if((int)unit.Element == element)
						count++;
				}
				if(count >= data.n_TYPE_[1])
					GameSystem.Service.SetChallenge(data.n_ID, count);
			}
				break;

			case 2:	//Type
			{
				int type = data.n_TYPE_[0];
				int count = 0;
				foreach(FightUnit unit in UnitDict.Values)
				{
					if(unit == null) continue;
					
					if(unit.Type == type)
						count++;
				}
				if(count >= data.n_TYPE_[1])
					GameSystem.Service.SetChallenge(data.n_ID, count);
			}
				break;

			case 3:	//Max Output
			{
				if(MaxOutput >= data.n_TYPE_[0])
					GameSystem.Service.SetChallenge(data.n_ID, MaxOutput);
			}
				break;

			case 4:	//Total Damage
			{
				if(TotalDamage <= data.n_TYPE_[0])
					GameSystem.Service.SetChallenge(data.n_ID, TotalDamage);
			}
				break;

			case 5:	//Time
			{
				float t = Time.realtimeSinceStartup - FightManager.Instance.startTime;
				if(t <= data.n_TYPE_[0])
					GameSystem.Service.SetChallenge(data.n_ID, (int)t);
			}
				break;

			case 6:	//Item Use
			{
				int bType = data.n_TYPE_[0];
				if(!ItemUseDict.ContainsKey(bType) || ItemUseDict[bType] <= data.n_TYPE_[1])
				{
					int count = 0;
					if(ItemUseDict.ContainsKey(bType))
						count = ItemUseDict[bType];
					GameSystem.Service.SetChallenge(data.n_ID, count);
				}
			}
				break;

			case 7:	//Max Chain
			{
				if(MaxChainCount >= data.n_TYPE_[0])
					GameSystem.Service.SetChallenge(data.n_ID, MaxChainCount);
			}
				break;

			case 8:	//Power Skill Use
			{
				if(PowerSkillUse <= data.n_TYPE_[0])
					GameSystem.Service.SetChallenge(data.n_ID, PowerSkillUse);
			}
				break;

			case 9:	//Special Card
			{
				List<int> lst = new List<int>();
				foreach(FightUnit unit in UnitDict.Values)
				{
					if(unit == null) continue;
					
					lst.Add(unit.CardID);
				}

				bool pass = true;
				foreach(int id in data.n_TYPE_)
				{
					if(id > 0 && !lst.Contains(id))
					{
						pass = false;
						break;
					}
				}

				if(pass)
				{
					lst.Insert(0, data.n_ID);
					GameSystem.Service.SetChallenge(lst.ToArray());
				}
			}
				break;

			case 10:	//Total Luck
			{
				int luck = 0;
				foreach(FightUnit unit in UnitDict.Values)
				{
					if(unit == null) continue;
					
					luck += unit.Luck;
				}
				if(luck >= data.n_TYPE_[0])
					GameSystem.Service.SetChallenge(data.n_ID, luck);
			}
				break;

			case 11:	//Total Rare
			{
				int rare = 0;
				foreach(FightUnit unit in UnitDict.Values)
				{
					if(unit == null) continue;
					
					rare += unit.Rare;
				}
				if(rare <= data.n_TYPE_[0])
					GameSystem.Service.SetChallenge(data.n_ID, rare);
			}
				break;

			case 12: //Leader Skill
			{
				FightUnit leader1 = GetUnit(1);
				FightUnit leader2 = GetUnit(2);
				List<FightUnit> lst = new List<FightUnit>(){ leader1, leader2 };
				bool achieve = false;
				foreach(FightUnit unit in lst)
				{
					if(unit != null)
					{
						Mikan.CSAGA.ConstData.Skill skill = unit.LeaderSkill;
						while(skill != null)
						{
							if(skill.n_TRIGGER == data.n_TYPE_[0])
							{
								GameSystem.Service.SetChallenge(data.n_ID, skill.n_ID);
								achieve = true;
								break;
							}

							skill = ConstData.Tables.Skill[skill.n_SKILL_LINK];
						}
					}

					if(achieve)
						break;
				}
			}
				break;

			}


		}
	}

#endregion

}
