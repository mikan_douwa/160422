﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using Mikan;

public class AttackEnemyOperation : OperationBase {

	int CharIndex;

	public AttackEnemyOperation(int charId)
	{
		CharIndex = charId;
	}

	public override void Execute ()
	{
		FightManager.Instance.playManager.Attack(CharIndex);
		OperationComplete();
	}

	void Step2()
	{
		FightManager.Instance.playManager.AttackEnd();
		OperationComplete();
	}
}

public class EnemyHitOperation : OperationBase {

	int Index;
	int Damage;

	public EnemyHitOperation(int id, int damage)
	{
		Index = id;
		Damage = damage;
	}

	public override void Execute ()
	{
		FightManager.Instance.playManager.HitEnemy(Index, Damage);
		OperationComplete();
	}
}

public class ShootPlayOperation : OperationBase {

	BaseUnit.SIDE FromSide;
	int FromIndex;
	BaseUnit.SIDE ToSide;
	int ToIndex;
	float Duration;

	public ShootPlayOperation(BaseUnit.SIDE fromSide, int fromIndex, BaseUnit.SIDE toSide, int toIndex, float duration)
	{
		FromSide = fromSide;
		FromIndex = fromIndex;
		ToSide = toSide;
		ToIndex = toIndex;
		Duration = duration;
	}

	public override void Execute ()
	{
		FightManager.Instance.playManager.ShootPlay(FromSide, FromIndex, ToSide, ToIndex, Duration);
		OperationComplete();
	}
}

public class EnemyForceDeadOperation : OperationBase {
	
	int Index;
	
	public EnemyForceDeadOperation(int id)
	{
		Index = id;
	}
	
	public override void Execute ()
	{
		FightManager.Instance.playManager.EnemyForceDead(Index);
		OperationComplete();
	}
}

public class EnemyTalkOperation : OperationBase {

	FightEnemy Enemy;
	bool isSync;

	public EnemyTalkOperation(FightEnemy enemy, bool sync)
	{
		Enemy = enemy;
		isSync = sync;
	}

	public override void Execute ()
	{
		if(Enemy.PVPTalk != "")
		{
			string msg = Enemy.PVPTalk;
			if(isSync)
				FightManager.Instance.playManager.EnemyTalk(Enemy.Index, msg, OperationComplete);
			else
				FightManager.Instance.playManager.EnemyTalk(Enemy.Index, msg);
			Enemy.PVPTalk = "";
			Enemy.TalkID = 0;
		}
		else if(Enemy.TalkID > 0)
		{
			string msg = "";
			Mikan.CSAGA.ConstData.MobSpeak data = ConstData.Tables.MobSpeak[Enemy.TalkID];
			if(data != null)
				msg = data.s_TEXT;

			if(isSync)
				FightManager.Instance.playManager.EnemyTalk(Enemy.Index, msg, OperationComplete);
			else
				FightManager.Instance.playManager.EnemyTalk(Enemy.Index, msg);
			Enemy.PVPTalk = "";
			Enemy.TalkID = 0;
		}

		if(!isSync)
			OperationComplete();
	}
}

public class EnemyCDOperation : OperationBase {

	int Index;
	float Second;

	public EnemyCDOperation(int index, float sec)
	{
		Index = index;
		Second = sec;
	}

	public override void Execute ()
	{
		if(Second > 0)
			FightManager.Instance.playManager.EnemyCD(Index, Second);

		OperationComplete();
	}
}

public class DamageTextOperation : OperationBase {

	public enum TEXT_TYPE
	{
		DAMAGE,
		HEAL,
	}


	DamageResult.DAMAGE_EFFECT Effect;
	int Index;
	int Damage;
	TEXT_TYPE Type;

	public DamageTextOperation(int id, TEXT_TYPE type, int damage, DamageResult.DAMAGE_EFFECT effect)
	{
		Index = id;
		Type = type;
		Damage = damage;
		Effect = effect;
	}

	public override void Execute ()
	{
		switch(Type)
		{
			case TEXT_TYPE.DAMAGE:
				FightManager.Instance.playManager.ShowDamage(Index, Damage, Effect);
				break;
			case TEXT_TYPE.HEAL:
				FightManager.Instance.playManager.ShowHeal(Index, Damage);
				break;
		}
		OperationComplete();
	}
}

public class AddActionOperation : OperationBase {

	int Index;
	int Lv;
	float Percent;

	public AddActionOperation(int id, int lv)
	{
		Index = id;
		Percent = 0f;
		Lv = lv;
	}

	public AddActionOperation(int id, float p)
	{
		Index = id;
		Percent = p;
		Lv = 0;
	}

	public override void Execute ()
	{
		FightManager.Instance.playManager.AddActionToChar(Index, Lv, Percent);
		OperationComplete();
	}
}

public class AddCDTimeOperation : OperationBase {
	
	int Index;
	float Percent;
	

	public AddCDTimeOperation(int id, float p)
	{
		Index = id;
		Percent = p;
	}
	
	public override void Execute ()
	{
		FightManager.Instance.playManager.AddCDToChar(Index, -Percent);
		OperationComplete();
	}
}


public class AttackPlayerOperation : OperationBase {
	
	int EnemyIndex;
	
	public AttackPlayerOperation(int enemyId)
	{
		EnemyIndex = enemyId;
	}
	
	public override void Execute ()
	{
		FightManager.Instance.playManager.EnemyAttack(EnemyIndex, Step2);
	}
	
	void Step2()
	{
		FightManager.Instance.playManager.EnemyAttackEnd(EnemyIndex);
		OperationComplete();
	}
}

public class PlayerHitOperation : OperationBase {

	int Damage;
	BaseUnit.ELEMENT Element;

	public PlayerHitOperation(int damage, BaseUnit.ELEMENT element)
	{
		Damage = damage;
		Element = element;
	}
	
	public override void Execute ()
	{
		FightManager.Instance.playManager.HitPlayer(Damage, Element);
		OperationComplete();
	}
}

public class AddBuffOperation : OperationBase {

	BaseUnit.SIDE Side;
	int Index;
	Buff BuffAdded;

	public AddBuffOperation(BaseUnit.SIDE side, int index, Buff b)
	{
		Side = side;
		Index = index;
		BuffAdded = b;
	}

	public override void Execute ()
	{
		bool targetDead = false;
		if(Side == BaseUnit.SIDE.ENEMY)
		{
			FightEnemy enemy = FightManager.Instance.GetEnemy(Index);
			if(enemy != null && enemy.LifeStatus == FightEnemy.LIFE_STATUS.DEAD)
				targetDead = true;
		}

		if(!targetDead)
		{
			if(FightManager.Instance.ResistDebuff(Index, BuffAdded.BuffID))
			{
				//Resist
				if(!FightManager.Instance.GroupBuffList.Contains(BuffAdded.BuffID))
				{
					string fxName = "UpgradeFX";
					FightManager.Instance.playManager.PlayFXTo(Side, Index, fxName);
				}
			}
			else
			{
				FightManager.Instance.AddBuff(Side, Index, BuffAdded);
				//Sound
				if(BuffAdded.BuffID < 100)
					Message.Send(MessageID.PlaySound, SoundID.BATTLE_BUFF);
				else
					Message.Send(MessageID.PlaySound, SoundID.BATTLE_DEBUFF);

				if(!FightManager.Instance.GroupBuffList.Contains(BuffAdded.BuffID))
				{
					string fxName = "BuffFX";
					if(BuffAdded.BuffID >= 100)
						fxName = "DebuffFX";
					FightManager.Instance.playManager.PlayFXTo(Side, Index, fxName);
				}
			}
		}

		OperationComplete();
	}
}

public class CureStunOperation : OperationBase {

	BaseUnit.SIDE Side;
	int Index;
	float Second;

	public CureStunOperation(BaseUnit.SIDE side, int index, float sec)
	{
		Side = side;
		Index = index;
		Second = sec;
	}

	public override void Execute ()
	{
		FightManager.Instance.playManager.CureStunByChar(Side, Index, Second);
		OperationComplete();
	}
}

public class ChangeBoardOperation : OperationBase {

	int FromItemType;
	int ToItemType;
	int Mode;
	int CharIndex;

	public ChangeBoardOperation(int from, int to, int mode, int index)
	{
		FromItemType = from;
		ToItemType = to;
		Mode = mode;
		CharIndex = index;
	}

	public override void Execute ()
	{
		FightManager.Instance.playManager.dashBoard.ChangeBoard(FromItemType, ToItemType, Mode, CharIndex);
		OperationComplete();
	}
}

public class ItemLvUpOperation : OperationBase {
	
	int ItemType;
	int Lv;
	int Mode;
	int CharIndex;

	public ItemLvUpOperation(int type, int lv, int mode, int index)
	{
		ItemType = type;
		Lv = lv;
		Mode = mode;
		CharIndex = index;
	}
	
	public override void Execute ()
	{
		FightManager.Instance.playManager.dashBoard.AddLvByType(ItemType, Lv, Mode, CharIndex);
		OperationComplete();
	}
}

public class LockItemOperation : OperationBase {
	
	int ItemType;
	int Mode;
	int CharIndex;
	
	public LockItemOperation(int type, int mode, int index)
	{
		ItemType = type;
		Mode = mode;
		CharIndex = index;
	}
	
	public override void Execute ()
	{
		FightManager.Instance.playManager.dashBoard.LockItemByType(ItemType, Mode, CharIndex);
		OperationComplete();
	}
}

public class BindItemOperation : OperationBase {
	
	int ItemType;
	int Mode;
	int CharIndex;
	
	public BindItemOperation(int type, int mode, int index)
	{
		ItemType = type;
		Mode = mode;
		CharIndex = index;
	}
	
	public override void Execute ()
	{
		FightManager.Instance.playManager.dashBoard.BindItemByType(ItemType, Mode, CharIndex);
		OperationComplete();
	}
}

public class AddChainOperation : OperationBase {
	
	int Chain;

	public AddChainOperation(int chain)
	{
		Chain = chain;
	}
	
	public override void Execute ()
	{
		FightManager.Instance.ChainAdd(Chain);
		OperationComplete();
	}
}

public class UpdateTotalHPOperation : OperationBase {

	public UpdateTotalHPOperation(){}

	public override void Execute ()
	{
		FightManager.Instance.RefreshMaxHp();
		OperationComplete();
	}
}

public class CutInOperation : OperationBase {

	BaseUnit Unit;
	int SkillID;

	public CutInOperation(BaseUnit unit, int sId)
	{
		Unit = unit;
		SkillID = sId;
	}

	public override void Execute ()
	{
		FightManager.Instance.playManager.AddCutIn(Unit, SkillID, OperationComplete);
	}
}

public class DetectFightEndOperation : OperationBase {

	public DetectFightEndOperation()
	{}

	public override void Execute ()
	{
		FightManager.Instance.DetectFightEnd();
		OperationComplete();
	}
}

public class FightStartOperation : OperationBase {

	public FightStartOperation()
	{}

	public override void Execute ()
	{
		FightManager.Instance.playManager.EnableSystemButton(true);
		FightManager.Instance.playManager.FightStart(true);
		OperationComplete();
	}
}

public class PassiveSkillOperation : OperationBase {

	FightUnit Unit;
	List<Mikan.CSAGA.ConstData.Skill> SkillList;

	public PassiveSkillOperation(FightUnit unit, List<Mikan.CSAGA.ConstData.Skill> lst)
	{
		Unit = unit;
		SkillList = lst;
	}

	public override void Execute ()
	{
		FightManager.Instance.PassiveSkill(Unit, SkillList);
		OperationComplete();
	}
}

public class TriggerSkillOperation : OperationBase {

	FightManager.PASSIVE_TRIGGER Trigger;

	public TriggerSkillOperation(FightManager.PASSIVE_TRIGGER trigger)
	{
		Trigger = trigger;
	}

	public override void Execute ()
	{
		FightManager.Instance.TriggerPassiveSkill(Trigger);
		OperationComplete();
	}
}

public class ShowTextToCharOperation : OperationBase {

	int Index;
	string Text;

	public ShowTextToCharOperation(int index, string text)
	{
		Index = index;
		Text = text;
	}

	public override void Execute ()
	{
		FightManager.Instance.playManager.ShowStatusText(Index, StatusText.STATUS_TYPE.TEXT, 0, Text);
		OperationComplete();
	}

}

public class PlayFXToCharOperation : OperationBase {

	BaseUnit.SIDE Side;
	int Index;
	string Name;
	
	public PlayFXToCharOperation(BaseUnit.SIDE side, int index, string name)
	{
		Side = side;
		Index = index;
		Name = name;
	}
	
	public override void Execute ()
	{
		FightManager.Instance.playManager.PlayFXTo(Side, Index, Name);
		OperationComplete();
	}
	
}

public class QTEShieldOperation : OperationBase {
	
	Mikan.CSAGA.ConstData.Skill SkillData;
	
	public QTEShieldOperation(Mikan.CSAGA.ConstData.Skill skillData)
	{
		SkillData = skillData;
	}
	
	public override void Execute ()
	{
		FightManager.Instance.playManager.InitQTEShield(SkillData);
		OperationComplete();
	}
	
}


