﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using Mikan.CSAGA.Data;

public class FightUnit : BaseUnit {

	public Int64 SeqId;
	public int Rare;
	public int Luck;

	public Mikan.CSAGA.ConstData.Skill PowerSkill;
	public Mikan.CSAGA.ConstData.Skill ActiveSkill;

	public string iconName;

	//For Leader Skill
	public Mikan.CSAGA.ConstData.Skill LeaderSkill;
	public int HpLeaderBonus = 0;
	public int AtkLeaderBonus = 0;
	public int RevLeaderBonus = 0;
	public bool PlayLeaderSkill = false;
	public List<Mikan.CSAGA.ConstData.Skill> DefenseSkillList = new List<Mikan.CSAGA.ConstData.Skill>();
	//Passive Skills
	public List<Mikan.CSAGA.ConstData.Skill> PassiveSkillList = new List<Mikan.CSAGA.ConstData.Skill>();
	public Dictionary<int, int> ResistDebuffList = new Dictionary<int, int>();
	public int SkillBoost = 0;
	public int ActionBoost = 0;
	public List<int> ActionLogList = new List<int>();

	public void Init(int index, Card card)
	{
		CardID = card.ID;
		Rare = card.Rare;
		Luck = card.Luck;
		Index = index;
		Side = SIDE.ALLY;

		CardProperty cp = card.Calc();
		cp.Append(card.SelectEquipment());
		Hp = cp.HP;
		Atk = cp.Atk;
		Rev = cp.Rev;
		Spd = cp.Charge;

		Mikan.CSAGA.ConstData.CardData data = ConstData.Tables.Card[card.ID];
		Element = (ELEMENT)data.n_TYPE;
		Type = data.n_GROUP;

		//Skills
		int psId = card.Skill1;
		int asId = card.Skill2;
		PowerSkill = ConstData.Tables.Skill[psId];
		ActiveSkill = ConstData.Tables.Skill[asId];
		LeaderSkill = ConstData.Tables.Skill[card.LeaderSkill];
		//Passive Skills
		//From Awaken
		for(int i = 1; i <= 3; i++)
		{
			int awakenLv = card.GetAwakenLV(i);
			Mikan.CSAGA.ConstData.Awaken awakenData = ConstData.Tables.Awaken.SelectFirst(o=>{ return o.n_CARDID == CardID && o.n_GROUP == i && o.n_LV == awakenLv; });
			if(awakenData != null)
			{
				int skillId = awakenData.n_SKILL[2];
				Mikan.CSAGA.ConstData.Skill skillData = ConstData.Tables.Skill[skillId];
				if(skillData != null)
					PassiveSkillList.Add(skillData);
			}
		}
		//From Equipment
		foreach(Equipment equip in card.SelectEquipment())
		{
			float counterEnhance = 1f;

			Mikan.CSAGA.ConstData.Item itemData = ConstData.Tables.Item[equip.ID];
			if(itemData != null)
			{
				switch(itemData.n_EFFECT)
				{
					case Mikan.CSAGA.ItemEffect.Skill:
						Mikan.CSAGA.ConstData.Skill skillData = ConstData.Tables.Skill[itemData.n_EFFECT_X];
						if(skillData != null)
							PassiveSkillList.Add(skillData);
						break;

					case Mikan.CSAGA.ItemEffect.Boost:
						if(SkillBoost < itemData.n_EFFECT_X)
							SkillBoost = itemData.n_EFFECT_X;
						break;

					case Mikan.CSAGA.ItemEffect.Boost2:
						if(ActionBoost < itemData.n_EFFECT_X)
							ActionBoost = itemData.n_EFFECT_X;
						break;

					case Mikan.CSAGA.ItemEffect.Resist:
						if(ResistDebuffList.ContainsKey(itemData.n_EFFECT_X))
						{
							int rate = ResistDebuffList[itemData.n_EFFECT_X];
							ResistDebuffList[itemData.n_EFFECT_X] = rate + itemData.n_EFFECT_Y;
						}
						else
						{
							ResistDebuffList.Add(itemData.n_EFFECT_X, itemData.n_EFFECT_Y);
						}
						break;

					case Mikan.CSAGA.ItemEffect.CounterEnhance:
						counterEnhance += (float)itemData.n_EFFECT_X / 100f;
						break;

					case Mikan.CSAGA.ItemEffect.BuffExtend:
						float extend = (float)itemData.n_EFFECT_Y / 100f;
						if(BuffExtendDict.ContainsKey(itemData.n_EFFECT_X))
						{
							if(BuffExtendDict[itemData.n_EFFECT_X] < extend)
								BuffExtendDict[itemData.n_EFFECT_X] = extend;
						}
						else
							BuffExtendDict.Add(itemData.n_EFFECT_X, extend);
						break;
				}

				if(CardID == itemData.n_CARD)
				{
					switch(itemData.n_EFFECT2)
					{
						case Mikan.CSAGA.ItemEffect.Skill:
							Mikan.CSAGA.ConstData.Skill skillData = ConstData.Tables.Skill[itemData.n_EFFECT2_X];
							if(skillData != null)
								PassiveSkillList.Add(skillData);
							break;
							
						case Mikan.CSAGA.ItemEffect.Boost:
							if(SkillBoost < itemData.n_EFFECT2_X)
								SkillBoost = itemData.n_EFFECT2_X;
							break;
							
						case Mikan.CSAGA.ItemEffect.Boost2:
							if(ActionBoost < itemData.n_EFFECT2_X)
								ActionBoost = itemData.n_EFFECT2_X;
							break;
							
						case Mikan.CSAGA.ItemEffect.Resist:
							if(ResistDebuffList.ContainsKey(itemData.n_EFFECT2_X))
							{
								int rate = ResistDebuffList[itemData.n_EFFECT2_X];
								ResistDebuffList[itemData.n_EFFECT2_X] = rate + itemData.n_EFFECT2_Y;
							}
							else
							{
								ResistDebuffList.Add(itemData.n_EFFECT2_X, itemData.n_EFFECT2_Y);
							}
							break;

						case Mikan.CSAGA.ItemEffect.CounterEnhance:
							counterEnhance += (float)itemData.n_EFFECT2_X / 100f;
							break;

						case Mikan.CSAGA.ItemEffect.BuffExtend:
							float extend = (float)itemData.n_EFFECT2_Y / 100f;
							if(BuffExtendDict.ContainsKey(itemData.n_EFFECT2_X))
							{
								if(BuffExtendDict[itemData.n_EFFECT2_X] < extend)
									BuffExtendDict[itemData.n_EFFECT2_X] = extend;
							}
							else
								BuffExtendDict.Add(itemData.n_EFFECT2_X, extend);
							break;
					}
				}
			}

			//counter bonus
			if(equip.CounterType > 0)
			{
				float counter = (float)equip.Counter * counterEnhance;

				if(CounterBonusDict.ContainsKey(equip.CounterType))
				{
					float bonus = CounterBonusDict[equip.CounterType];
					CounterBonusDict[equip.CounterType] = bonus + counter;
				}
				else
					CounterBonusDict.Add(equip.CounterType, counter);
			}

		}

		iconName = data.s_FILENAME;
		texName = data.s_FILENAME;

		//Const Variable
		BonusCount = 0;
		FinalHP = Hp;
		FinalATK = Atk;
		FinalREV = Rev;
		MAX_BONUS_COUNT = ConstData.Tables.GetVariable(Mikan.CSAGA.VariableID.STR_BOARD_LIMIT);
		BONUS_EFFECT = ConstData.Tables.GetVariablef(Mikan.CSAGA.VariableID.STR_BOARD_EFFECT);
	}

	public void AddLeaderSkillEffect(Mikan.CSAGA.ConstData.Skill data)
	{
		if(data == null) return;
		switch(data.n_EFFECT)
		{
			case 101:	//Properties Up.
			{
				HpLeaderBonus += Math.Max(data.n_EFFECT_X - 100, 0);
				AtkLeaderBonus += Math.Max(data.n_EFFECT_Y - 100, 0);
				RevLeaderBonus += Math.Max(data.n_EFFECT_Z - 100, 0);
				break;
			}
			case 102:
			{
				DefenseSkillList.Add(data);
				break;
			}
		}

		PlayLeaderSkill = true;
	}
	
	public void CalculateTotalEffect()
	{
		Hp = (int)((float)Hp * ((float)(100 + HpLeaderBonus) / 100f));
		Atk = (int)((float)Atk * ((float)(100 + AtkLeaderBonus) / 100f));
		Rev = (int)((float)Rev * ((float)(100 + RevLeaderBonus) / 100f));
		FinalHP = Hp;
		FinalATK = Atk;
		FinalREV = Rev;
	}

	public void ApplyTacticEffect(List<Mikan.CSAGA.ConstData.Tactics> lst)
	{
		float hpBonus = 0f;
		float atkBonus = 0f;
		float revBonus = 0f;
		foreach(Mikan.CSAGA.ConstData.Tactics tactic in lst)
		{
			if(tactic.n_EFFECT == Mikan.CSAGA.TacticsEffect.Hp || tactic.n_EFFECT == Mikan.CSAGA.TacticsEffect.Atk || tactic.n_EFFECT == Mikan.CSAGA.TacticsEffect.Rev)
			{
				if(tactic.n_EFFECT_X == (int)Element || tactic.n_EFFECT_Y == Type)
				{
					switch(tactic.n_EFFECT)
					{
					case Mikan.CSAGA.TacticsEffect.Hp:
						hpBonus += tactic.n_EFFECT_Z;
						break;
					case Mikan.CSAGA.TacticsEffect.Atk:
						atkBonus += tactic.n_EFFECT_Z;
						break;
					case Mikan.CSAGA.TacticsEffect.Rev:
						revBonus += tactic.n_EFFECT_Z;
						break;
					}
				}
			}
		}

		Hp = (int)((float)Hp * (100f + hpBonus) / 100f);
		Atk = (int)((float)Atk * (100f + atkBonus) / 100f);
		Rev = (int)((float)Rev * (100f + revBonus) / 100f);
		FinalHP = Hp;
		FinalATK = Atk;
		FinalREV = Rev;
	}

	public void LogAction(ActItem.ACT_TYPE act)
	{
		int actId = (int)act;
		if(ActionLogList.Count > 0 && ActionLogList[0] != actId)
		{
			ActionLogList.Clear();
		}

		ActionLogList.Add(actId);

	}
}

