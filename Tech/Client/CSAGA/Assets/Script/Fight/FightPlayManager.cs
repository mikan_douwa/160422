﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;


public class CutInTask {
	public BaseUnit.SIDE Side;
	public string Path;
	public string Talk;
	public string SkillName;
	public string SkillDetail;
	public Action Callback;

	public CutInTask(BaseUnit.SIDE side, string path, string talk, string name, string detail, Action callback)
	{
		Side = side;
		Path = path;
		Talk = talk;
		SkillName = name;
		SkillDetail = detail;
		Callback = callback;
	}
}


public class FightPlayManager : MonoBehaviour {

	public const float PLAYER_BUFF_ICON_X = 290f;
	public const float PLAYER_BUFF_ICON_Y = 85f;
	public const float ENEMY_BUFF_ICON_X = 290f;
	public const float ENEMY_BUFF_ICON_Y = 300f;
	public const float BUFF_ICON_HEIGHT = 55f;
	public const int BACKGROUND_DEPTH = 10;
	public const int QTE_ITEM_COUNT = 5;
	public const float LEADER_SKILL_ICON_X = -315f;
	public const float LEADER_SKILL_ICON_Y = 150f;
	public const float LEADER_SKILL_ICON_HEIGHT = 60f;

	//Data
	public CharIcon[] IconList;
	private Dictionary<int, CharEnemy> enemyDict;
	private List<BuffIcon> PlayerBuffList = new List<BuffIcon>();
	private List<BuffIcon> EnemyBuffList = new List<BuffIcon>();
	private float autoInterval = 0.1f;
	private float timeCounter;
	private float autoCD = 0f;
	private List<int> EnemyDeadList = new List<int>();
	//Cut In Data.
	private Queue<CutInTask> CutInQueue = new Queue<CutInTask>();
	private bool IsCutInPlaying = false;
	private CutInTask PlayingTask = null;
	//Flags.
	private int EnemyDeadPlaying = 0;
	private bool pause = true;
	private bool isAutoFight = false;
	private bool winPlayFinished = false;
	//QTE
	public int QTEIndex;
	//Leader skill icon
	private List<LSkillIcon> LSkillList = new List<LSkillIcon>();

	//GameObject
	public UIWidget anchorWidget;
	public UITexture bgTexture;
	public UITexture bgTextureFront;
	public GameObject charPanelObj;
	public GameObject enemyPanelObj;
	public GameObject buffPanelObj;
	public GameObject fxPanelObj;
	public GameObject overlayObj;
	public GameObject hitPlayerObj;
	public UISlider hpBar;
	public UILabel hpText;
	public DashBoard dashBoard;
	public DebugPanel debugPanel;
	public GameObject blocker;
	public GameObject boardBlocker;
	public GameObject autoFightBlocker;
	public FightResLoader fxLoader;
	public GameObject endButton;
	public GameObject autoFightBanner;
	public GameObject debugBtn;
	//Cut In References.
	public UIPanel cutInPanel;
	public UIPanel cutInSkillPanel;
	public GameObject cutInSlideObj;
	public GameObject cutInTalkBg;
	public UITexture cutInTex;
	public UILabel cutInTalkLabel;
	public UILabel cutInSkillName;
	public UILabel cutInSkillDetail;
	//End Game
	public GameObject dangerCover;
	public UISprite foreBlack;
	public UISprite backBlack;
	public UISprite endLogo;
	public GameObject endFXObj;
	//Final Wave
	public GameObject finalWaveObj;
	public UISprite finalWaveBg;
	public GameObject finalWaveTitle;
	public UITexture finalWaveBorder1;
	public UITexture finalWaveBorder2;
	EventDelegate.Callback changeRoundCallback = null;
	//QTE
	public GameObject qteObj;
	public QTEButton[] qteBtnArray;
	public GameObject qteArrowObj;
	public UISprite qteResult;
	public UILabel qteTip;
	public GameObject qteBtnRoot;
	public AnimationCurve qteTipCurve;
	//Timer
	public UILabel timerLabel;
	//Chain
	public GameObject chainObj;
	public UILabel chainLabel;
	public UISlider chainBar;
	public UISprite chainBarSprite;
	public GameObject chainContainer;
	public AnimationCurve chainCurve;

	//Prefab
	public GameObject enemyPrefab;
	public GameObject damagePrefab;
	public GameObject buffPrefab;
	public GameObject teachPrefab;
	public GameObject statusPrefab;
	public GameObject leaderSkillPrefab;

	//Temp skill confirm window
	public MessageBoxContext tempMsg = null;

	//Animation Curve
	public AnimationCurve walkCurve;

	void Awake()
	{
		enemyDict = new Dictionary<int, CharEnemy>();
		foreBlack.alpha = 1f;
#if DEBUG
		NGUITools.SetActive(debugBtn, true);
#endif
		/*
		FightManager.Instance.SetPlayManager(this);
		FightManager.Instance.SetAllyData(0);
		FightManager.Instance.SetEnemyData(101);
		*/
	}

	void Start()
	{
		//StartCoroutine(AssignPosition());
	}

	// Update is called once per frame
	void FixedUpdate() {
		if(!pause)
		{
			timeCounter += Time.fixedDeltaTime;
			if(autoCD > 0)
				autoCD -= Time.fixedDeltaTime;

			if(FightManager.Instance.ConsumeBuff(Time.deltaTime))
			{
				UpdateGroupBuffIcon(BaseUnit.SIDE.ALLY);
				UpdateGroupBuffIcon(BaseUnit.SIDE.ENEMY);
			}
			//detect cut in.
			if(!IsCutInPlaying)
				PlayCutIn();
			//light
			dashBoard.RefreshLight();

			//update by interval.
			RefreshAutoFight();
			if(timeCounter > autoInterval)
			{
				timeCounter = 0f;
				if(isAutoFight && autoCD <= 0)
					AutoFight();
			}

			//JIT Leader skill
			FightManager.Instance.TimePower += Time.fixedDeltaTime;
			UpdateLeaderSkillIcon(LSkillIcon.TYPE.TIME);

			//Chain
			if(FightManager.Instance.isChainOn)
			{
				if(FightManager.Instance.chainTime > 0)
				{
					FightManager.Instance.chainTime -= Time.fixedDeltaTime;
					if(FightManager.Instance.chainTime <= 0)
					{
						FightManager.Instance.chainTime = 0f;
						FightManager.Instance.chain = 0;
						NGUITools.SetActive(chainObj, false);
					}
					else
					{
						RefreshChain();
					}
				}
			}
		}
		//Timer
		if(FightManager.Instance.needTimer)
		{
			float t = FightManager.Instance.totalTime - (Time.realtimeSinceStartup - FightManager.Instance.startTime);
			if(t <= 0)
			{
				t = 0f;
				timerLabel.color = Color.red;
				FightManager.Instance.needTimer = false;
			}
			timerLabel.text = t.ToString("#0.0") + "s";
		}
	}

#region Init Functions

	public void AssignPosition()
	{
		//Assign all position
		float bottom = -anchorWidget.height / 2f;
		Debug.Log("bottom:" + bottom);
		float dashBoardHeight = 400f;
		float charBoardHeight = 184f;
		float enemyBoardHeight = 480f;
		float hpBarHeight = 24f;
		
		dashBoard.transform.localPosition = new Vector3(0f, bottom + dashBoardHeight / 2f, 0f);
		charPanelObj.transform.localPosition = new Vector3(0f, bottom + dashBoardHeight + charBoardHeight / 2f, 0f);
		enemyPanelObj.transform.localPosition = new Vector3(0f, bottom + dashBoardHeight + charBoardHeight + enemyBoardHeight / 2f, 0f);
		
		bgTexture.pivot = UIWidget.Pivot.Bottom;
		bgTextureFront.pivot = UIWidget.Pivot.Bottom;
		bgTexture.transform.localPosition = new Vector3(0f, bottom + dashBoardHeight + hpBarHeight / 2f, 0f);
		bgTextureFront.transform.localPosition = new Vector3(0f, bottom + dashBoardHeight + hpBarHeight / 2f, 0f);

	}

	public void InitChar(int index, FightUnit unit, Action callback)
	{
		//Player
		CharIcon icon = GetChar(index);
		icon.Init(unit, callback);
	}

	public void InitEnemy(FightEnemy enemy, int totalEnemy, Action callback)
	{
		//Enemy
		CharEnemy e = NGUITools.AddChild(enemyPanelObj, enemyPrefab).GetComponent<CharEnemy>();
		e.Init(enemy, totalEnemy, callback);
		enemyDict.Add(enemy.Index, e);
	}

	public void InitBackground(string bg)
	{
		//Set Timer
		NGUITools.SetActive(timerLabel.gameObject, FightManager.Instance.needTimer);

		//Set background
		ResourceManager.Instance.LoadTexture("BG/" + bg, LoadBGHandler);
	}
	private void LoadBGHandler(IAsset<Texture2D> asset)
	{
		bgTexture.mainTexture = asset.Content;
		bgTextureFront.mainTexture = asset.Content;
		//bgTexture.MakePixelPerfect();
		//bgTextureFront.MakePixelPerfect();
		ResetBackground();
		FightManager.Instance.TexInitComplete();
	}

	public void ResetBackground()
	{
		//Clear tweeners, avoid run after reset.
		UITweener[] tweeners = bgTextureFront.GetComponents<UITweener>();
		for(int i = 0; i < tweeners.Length; i++)
		{
			tweeners[i].enabled = false;
		}

		//reset background
		bgTextureFront.transform.localPosition = Vector3.zero;
		bgTextureFront.transform.localScale = Vector3.one;
		bgTextureFront.alpha = 1f;
		bgTextureFront.depth = BACKGROUND_DEPTH;
	}

	public void ClearEnemy()
	{
		foreach(CharEnemy enemy in enemyDict.Values)
		{
			if(enemy != null && enemy.gameObject != null)
				Destroy(enemy.gameObject);
		}
		enemyDict.Clear();

		EnemyDeadList.Clear();
		EnemyDeadPlaying = 0;
	}

	public void PreloadResources(List<string> lstFX, List<string> lstCard, Action callback)
	{
		fxLoader.BatchLoad(callback, lstFX, lstCard);
	}

	public void InitLeaderSkillIcon(List<Mikan.CSAGA.ConstData.Skill> lst)
	{
		LSkillList.Clear();
		float count = 0;
		for(int i = 0; i < lst.Count; i++)
		{
			Mikan.CSAGA.ConstData.Skill skill = lst[i];
			switch(skill.n_TRIGGER)
			{
				case 8:
				case 9:
				case 10:
				case 11:
					LSkillIcon icon = NGUITools.AddChild(buffPanelObj, leaderSkillPrefab).GetComponent<LSkillIcon>();
					icon.Init(skill);
					icon.transform.localPosition = new Vector3(LEADER_SKILL_ICON_X, LEADER_SKILL_ICON_Y + count * LEADER_SKILL_ICON_HEIGHT, 0f);

					LSkillList.Add(icon);
					count++;
				break;
			}
		}

		UpdateLeaderSkillIcon(LSkillIcon.TYPE.TOTAL_LV);
	}

#endregion


#region private Utility Functions
	private CharIcon GetChar(int index)
	{
		return IconList[index - 1];
	}

	private CharEnemy GetEnemy(int index)
	{
		return enemyDict[index];
	}

	private void AutoFight()
	{
		for(int i = 1; i <= IconList.Length; i++)
		{
			if(dashBoard.ItemClickByChar(i))
			{
				autoCD = 0.25f;
				break;
			}
		}
	}
	
#endregion


#region Public Utility Functions

	public void CheckTutorialThenStart()
	{
		int round = FightManager.Instance.Round;

		if(FightManager.Instance.QuestID == 1 && FightManager.Instance.teachDict.Count == 0)
		{
			//Test tutorial data
			TeachContent content1 = new TeachContent("", fxLoader.GetTexture("TUTORIAL/battle_tutorial01"));
			TeachContent content2 = new TeachContent("", fxLoader.GetTexture("TUTORIAL/battle_tutorial02"));
			TeachContent content3 = new TeachContent("", fxLoader.GetTexture("TUTORIAL/battle_tutorial03"));
			TeachContent content4 = new TeachContent("", fxLoader.GetTexture("TUTORIAL/battle_tutorial04"));
			TeachContent content5 = new TeachContent("", fxLoader.GetTexture("TUTORIAL/battle_tutorial05"));
			FightManager.Instance.teachDict.Add(0, new List<TeachContent>(){content1, content2});
			FightManager.Instance.teachDict.Add(1, new List<TeachContent>(){content3});
			FightManager.Instance.teachDict.Add(2, new List<TeachContent>(){content4});
			FightManager.Instance.teachDict.Add(3, new List<TeachContent>(){content5});
		}

		if(FightManager.Instance.QuestID == 1 && FightManager.Instance.teachDict.ContainsKey(round))
		{
			TeachClip teach = NGUITools.AddChild(overlayObj, teachPrefab).GetComponent<TeachClip>();
			teach.Init(FightManager.Instance.teachDict[round], CheckRunQTE);
		}
		else
		{
			CheckRunQTE();
		}

	}

	void CheckRunQTE()
	{
		if(FightManager.Instance.QTEMode)
			InitQTE(WaveStart);
		else
			WaveStart();
	}

	void WaveStart()
	{
		//Qte appear
		NGUITools.SetActive(qteTip.gameObject, false);
		for(int i = 0; i < qteBtnArray.Length; i++)
		{
			NGUITools.SetActive(qteBtnArray[i].gameObject, true);
		}
		NGUITools.SetActive(qteArrowObj, true);

		Time.timeScale = Context.Instance.BattleSetting.Speed;

		//Pre Attack
		List<FightEnemy> lst = FightManager.Instance.GetAllEnemy(true);
		List<FightEnemy> lstPre = new List<FightEnemy>();
		for(int i = 0; i < lst.Count; i++)
		{
			FightEnemy enemy = lst[i];
			if(enemy.preSkillID > 0)
			{
				lstPre.Add(enemy);
			}
		}

		if(lstPre.Count > 0)
		{
			for(int i = 0; i < lstPre.Count; i++)
			{
				FightEnemy enemy = lstPre[i];
				if(enemy.preSkillID > 0)
				{
					FightManager.Instance.SendCommand(new EnemySkillCommand(enemy, enemy.preSkillID, EnemySkillCommand.TRIGGER.PRE_ATTACK, (i == lstPre.Count - 1)));
				}
			}
		}
		else
		{
			EnableSystemButton(true);
			FightStart(true);
		}

	}

	public void OpenFight()
	{
		TweenAlpha ta = TweenAlpha.Begin(foreBlack.gameObject, 1f, 0f);
		ta.ignoreTimeScale = false;
		EventDelegate.Add(ta.onFinished, CheckTutorialThenStart, true);
	}

	public void RefreshAutoFight()
	{
		if(isAutoFight != Context.Instance.BattleSetting.Auto)
		{
			isAutoFight = Context.Instance.BattleSetting.Auto;
			NGUITools.SetActive(autoFightBanner, isAutoFight);
			NGUITools.SetActive(autoFightBlocker, isAutoFight);
		}
	}

	public void FightStart (bool waveStart = false) {
		//Set Variables
		Time.timeScale = Context.Instance.BattleSetting.Speed;
		FightManager.Instance.isFXOn = Context.Instance.BattleSetting.FXEnabled;
		isAutoFight = Context.Instance.BattleSetting.Auto;
		NGUITools.SetActive(autoFightBanner, isAutoFight);
		NGUITools.SetActive(autoFightBlocker, isAutoFight);

		pause = false;

		for(int i = 0; i < IconList.Length; i++)
		{
			CharIcon icon = IconList[i];
			icon.Go();
		}
		foreach(CharEnemy enemy in enemyDict.Values)
		{
			enemy.Go();
		}

		//if(!FightManager.Instance.FightEnd)
		//{
			//dashBoard.EnableBoard = true;
			EnableTouch(true);
		//}

		//Passive Skills
		if(waveStart && FightManager.Instance.HP > 0)
			FightManager.Instance.TriggerPassiveSkill(FightManager.PASSIVE_TRIGGER.WAVE_START);

	}
	
	public void FightStop () {
		pause = true;

		for(int i = 0; i < IconList.Length; i++)
		{
			CharIcon icon = IconList[i];
			icon.Stop();
		}
		foreach(CharEnemy enemy in enemyDict.Values)
		{
			enemy.Stop();
		}
		
		dashBoard.ItemRelease(false);
		EnableTouch(false);
	}

	public bool ActionAvailable(ActItem item, bool consume)
	{
		CharIcon icon = GetChar(item.ID + 1);
		FightUnit unit = FightManager.Instance.GetUnit(icon.Index);
		return (icon.Enable 
		        && (!consume || icon.ActionPoint > 0)
		        && unit.GetBuff((int)Buff.BUFF_TYPE.STUN) == null);
	}

	public void EnableTouch(bool enable)
	{
		NGUITools.SetActive(boardBlocker, !enable);
	}

	public void EnableSystemButton(bool enable)
	{
		NGUITools.SetActive(blocker, !enable);
	}

	public List<int> GetCharActions()
	{
		List<int> lst = new List<int>();
		for(int i = 0; i < IconList.Length; i++)
		{
			lst.Add(IconList[i].ActionPoint);
		}
		return lst;
	}

	public void UpdateLeaderSkillIcon(LSkillIcon.TYPE type)
	{
		for(int i = 0; i < LSkillList.Count; i++)
		{
			LSkillIcon icon = LSkillList[i];
			if(icon.Type == type)
				icon.UpdateInfo();
		}
	}

	public void RefreshChain()
	{
		chainLabel.text = FightManager.Instance.chain.ToString();
		chainBar.value = FightManager.Instance.chainTime / FightManager.Instance.chainTimeMax;
		if(FightManager.Instance.chainTime <= 1)
			chainBarSprite.color = Color.red;
		else
			chainBarSprite.color = Color.white;

		//chain leader skill
		Color c = Color.white;
		foreach(Mikan.CSAGA.ConstData.Skill skill in FightManager.Instance.JITLeaderSkillList)
		{
			if(skill.n_TRIGGER == (int)LSkillIcon.TYPE.CHAIN && FightManager.Instance.chain >= skill.n_TRIGGER_X)
			{
				c = Color.green;
				break;
			}
		}
		chainLabel.color = c;
	}

	public void ChainJump()
	{
		chainContainer.transform.localScale = Vector3.one;
		TweenScale ts = TweenScale.Begin(chainContainer, 0.1f, new Vector3(1.5f, 1.5f, 1.5f));
		ts.animationCurve = chainCurve;

	}

	//For Debug Info
	public float GetSpeedRate(int index)
	{
		return GetChar(index).SpeedRate;
	}

#endregion


#region Action API
	public void Action(ActItemCommand item, bool consume, ActItem.ACT_TYPE triggerType)
	{
		if(FightManager.Instance.FightEnd) return;

		//Challenge.
		Dictionary<int, int> dict = FightManager.Instance.ItemUseDict;
		if(!dict.ContainsKey((int)item.Type))
			dict.Add((int)item.Type, 1);
		else
		{
			int count = dict[(int)item.Type];
			dict[(int)item.Type] = count + 1;
		}

		CharIcon icon = GetChar(item.ID + 1);
		if(consume)
			icon.ConsumeAction();

		//use item combo
		FightUnit actUnit = FightManager.Instance.GetUnit(icon.Index);
		actUnit.LogAction(triggerType);

		bool triggerLink = false;
		switch(item.Type)
		{
			case ActItem.ACT_TYPE.TimeBoost:
				float t = ConstData.Tables.GetVariablef(Mikan.CSAGA.VariableID.TIME_BOARD_POWER) * (float)item.Lv;
				AddCDToChar(icon.Index, t);
				break;

			case ActItem.ACT_TYPE.Attack:
				FightEnemy target = FightManager.Instance.GetTarget();
				if(target != null)
				{
					FightManager.Instance.CharAttack(icon.Index, target.Index, item.Lv, triggerType);
					triggerLink = true;
					FightManager.Instance.ChainAdd();
				}
				break;

			case ActItem.ACT_TYPE.Heal:
				FightManager.Instance.CharHeal(icon.Index, item.Lv, triggerType);
				triggerLink = true;
				FightManager.Instance.ChainAdd();
				break;

			case ActItem.ACT_TYPE.Skill:
				FightManager.Instance.CharSkill(icon.Index, item.Lv, triggerType);
				triggerLink = true;
				FightManager.Instance.ChainAdd();
				break;

			case ActItem.ACT_TYPE.PowerUp:
				FightManager.Instance.CharPowerUp(icon.Index, item.Lv);
				break;

			case ActItem.ACT_TYPE.Useless:
				break;

			case ActItem.ACT_TYPE.Poison:
				FightManager.Instance.CharPoison(item.Lv);
				break;
		}

		if(FightManager.Instance.QTEMode || FightManager.Instance.QTEShield)
			QTECheck(item);
		
		//Passive skill
		FightManager.Instance.TriggerPassiveSkillByItem(icon.Index, triggerType, item.Lv);

		//Reset JIT Leader skill status.
		FightManager.Instance.TimePower = 0f;
		List<int> lst = FightManager.Instance.LSCollectList;
		if(lst.Count >= 5)
			lst.RemoveAt(0);
		lst.Add((int)triggerType);
		//update ui
		UpdateLeaderSkillIcon(LSkillIcon.TYPE.TIME);
		UpdateLeaderSkillIcon(LSkillIcon.TYPE.COLLECT);

		//Link Buff
		if(triggerLink)
		{
			List<FightUnit> lstAlly = FightManager.Instance.GetAllUnit();
			foreach(FightUnit unit in lstAlly)
			{
				if(unit.Index != icon.Index)
				{
					Buff b = unit.GetBuff((int)Buff.BUFF_TYPE.LINK);
					if(b != null && Mikan.MathUtils.Random.Next(0, 100) < b.XValue)
					{
						switch(item.Type)
						{
						case ActItem.ACT_TYPE.Attack:
							FightEnemy target = FightManager.Instance.GetTarget();
							if(target != null)
								FightManager.Instance.CharAttack(unit.Index, target.Index, item.Lv, triggerType);
							break;
							
						case ActItem.ACT_TYPE.Heal:
							FightManager.Instance.CharHeal(unit.Index, item.Lv, triggerType);
							break;
							
						case ActItem.ACT_TYPE.Skill:
							FightManager.Instance.CharSkill(unit.Index, item.Lv, triggerType);
							break;
						}
					}
				}
			}
		}
	}
	
	public void UpdateHP()
	{
		int hp = FightManager.Instance.HP;
		int maxhp = FightManager.Instance.MaxHP;
		hpBar.value = (float)hp / (float)maxhp;
		hpText.text = hp.ToString() + "/" + maxhp.ToString();

		NGUITools.SetActive(dangerCover, (hp < (float)maxhp * 0.25f));
	}

	public void SetFocus(int index)
	{
		if(index == FightManager.Instance.TargetIndex)
		{
			FightManager.Instance.TargetIndex = 0;
			foreach(CharEnemy enemy in enemyDict.Values)
			{
				enemy.SetFocus(false);
			}
		}
		else
		{
			FightManager.Instance.TargetIndex = index;
			foreach(CharEnemy enemy in enemyDict.Values)
			{
				if(enemy.Index == index)
				{
					enemy.SetFocus(true);
				}
				else
					enemy.SetFocus(false);
			}
		}
	}

	public void UpdateCharSpeed(BaseUnit.SIDE side, int index)
	{
		if(side == BaseUnit.SIDE.ALLY)
			GetChar(index).UpdateSpeed();
		else
			GetEnemy(index).UpdateSpeed();
	}

	public void CureStunByChar(BaseUnit.SIDE side, int index, float sec)
	{
		//sound
		Message.Send(MessageID.PlaySound, SoundID.BATTLE_HEAL);

		if(side == BaseUnit.SIDE.ALLY)
		{
			FightUnit unit = FightManager.Instance.GetUnit(index);
			if(unit.CureStun(sec))
			{
				CharIcon fchar = GetChar(index);
				fchar.UpdateBuffIcon();
				//FX
				FightManager.Instance.PlayFX("SoulFX", Vector3.zero, fchar.fxPanel);
			}
		}
		else
		{
			FightEnemy unit = FightManager.Instance.GetEnemy(index);
			if(unit.CureStun(sec))
			{
				CharEnemy fchar = GetEnemy(index);
				fchar.UpdateBuffIcon();
				//FX
				Vector3 fxPos = enemyPanelObj.transform.localPosition + fchar.transform.localPosition;
				FightManager.Instance.PlayFX("SoulFX", fxPos, fxPanelObj);
			}
		}
	}

	public void UpdateBuffIconByChar(BaseUnit.SIDE side, int index)
	{
		if(side == BaseUnit.SIDE.ALLY)
			GetChar(index).UpdateBuffIcon();
		else
			GetEnemy(index).UpdateBuffIcon();
	}

	public void UpdateGroupBuffIcon(BaseUnit.SIDE side)
	{

		List<BuffIcon> BuffList = PlayerBuffList;
		if(side == BaseUnit.SIDE.ENEMY)
			BuffList = EnemyBuffList;

		//Del buff icon.
		List<BuffIcon> lstDel = new List<BuffIcon>();
		for(int i = 0; i < BuffList.Count; i++)
		{
			BuffIcon icon = BuffList[i];
			if(FightManager.Instance.GetGroupBuff(side, icon.BuffID) == null)
			{
				lstDel.Add(icon);
			}
		}
		for(int i = 0; i < lstDel.Count; i++)
		{
			BuffIcon icon = lstDel[i];
			BuffList.Remove(icon);
			Destroy(icon.gameObject);
		}
		
		//Add new buff icon.
		bool iconAdded = false;
		Dictionary<int, Buff> BuffDict = FightManager.Instance.PlayerBuffDict;
		if(side == BaseUnit.SIDE.ENEMY)
			BuffDict = FightManager.Instance.EnemyBuffDict;

		foreach(Buff b in BuffDict.Values)
		{
			BuffIcon bi = null;
			for(int i = 0; i < BuffList.Count; i++)
			{
				BuffIcon icon = BuffList[i];
				if(icon.BuffID == b.BuffID)
				{
					bi = icon;
					break;
				}
			}
			if(bi == null)
			{
				//Add buff icon.
				BuffIcon icon = NGUITools.AddChild(buffPanelObj, buffPrefab).GetComponent<BuffIcon>();
				icon.Init(b);
				BuffList.Add(icon);
				iconAdded = true;
			}
			else if(b.NeedRefresh)
			{
				bi.Init(b);
			}
		}
		
		//Resort icon.
		if(lstDel.Count > 0 || iconAdded)
		{
			float x = PLAYER_BUFF_ICON_X;
			float y = PLAYER_BUFF_ICON_Y;
			if(side == BaseUnit.SIDE.ENEMY)
			{
				x = ENEMY_BUFF_ICON_X;
				y = ENEMY_BUFF_ICON_Y;
			}

			for(int i = 0; i < BuffList.Count; i++)
			{
				BuffIcon icon = BuffList[i];
				icon.transform.localPosition = new Vector3(x, y + i * BUFF_ICON_HEIGHT, 0f);
			}
		}
	}

	public void OnEndClick(GameObject btn)
	{
		if(winPlayFinished)
			FightManager.Instance.LeaveFight();
	}


#endregion


#region Fight Activities
	//Player Actions.
	public void Attack(int charIndex)
	{
		CharIcon icon = GetChar(charIndex);
		TweenY ty = TweenY.Begin(icon.gameObject, 0.1f, 25f);
		ty.ignoreTimeScale = false;
		EventDelegate.Set(ty.onFinished, AttackEnd);
	}
	public void AttackEnd()
	{
		TweenY ty = TweenY.Begin(TweenY.current.gameObject, 0.1f, -25f);
		ty.ignoreTimeScale = false;
	}

	public void HitEnemy(int enemyIndex, int damage)
	{
		//Challenge.
		if(damage > FightManager.Instance.MaxOutput)
			FightManager.Instance.MaxOutput = damage;

		//Sound
		Message.Send(MessageID.PlaySound, SoundID.BATTLE_HIT);

		CharEnemy enemy = GetEnemy(enemyIndex);
		if(!FightManager.Instance.QTEShield)
			enemy.enemyData.AddHp(-damage);

		TweenShake ts = TweenShake.Begin(enemy.texture.gameObject, 0.5f, 20f);
		ts.ignoreTimeScale = false;

		enemy.UpdateInfo();
		//FX
		FightManager.Instance.PlayFX("HitFX", enemyPanelObj.transform.localPosition + enemy.transform.localPosition, fxPanelObj);
		
		//Dead play
		if(enemy.enemyData.LifeStatus == FightEnemy.LIFE_STATUS.DEAD)
		{
			//Death skill
			if(enemy.enemyData.deathSkillID > 0 && !enemy.enemyData.deathSkillPass)
			{
				enemy.enemyData.deathSkillPass = true;
				enemy.enemyData.LifeStatus = FightEnemy.LIFE_STATUS.LIVE;
				FightStop();
				FightManager.Instance.SendCommand(new EnemySkillCommand(enemy.enemyData, enemy.enemyData.deathSkillID, EnemySkillCommand.TRIGGER.DEATH, false));
			}
			else
			{
				//dead play
				if(!EnemyDeadList.Contains(enemy.Index))
				{
					EnemyDeadPlaying++;
					EnemyDeadList.Add(enemy.Index);
				}
				enemy.Stop();

				TweenAlpha ta = TweenAlpha.Begin(enemy.gameObject, 0.5f, 0f);
				ta.ignoreTimeScale = false;
				EventDelegate.Add(ta.onFinished, EnemyDeadEnd, true);
			}
		}

	}

	public void ShootPlay(BaseUnit.SIDE fromSide, int fromIndex, BaseUnit.SIDE toSide, int toIndex, float duration)
	{
		bool upToDown;
		if(fromSide == BaseUnit.SIDE.ALLY && toSide == BaseUnit.SIDE.ENEMY)
			upToDown = false;
		else
			upToDown = true;

		GameObject fromParent;
		if(fromSide == BaseUnit.SIDE.ALLY)
			fromParent = GetChar(fromIndex).gameObject;
		else
			fromParent = GetEnemy(fromIndex).gameObject;

		GameObject toParent;
		if(toSide == BaseUnit.SIDE.ALLY)
			toParent = hitPlayerObj;
		else
			toParent = GetEnemy(toIndex).gameObject;

		FightManager.Instance.FlyFX("BulletFX", Vector3.zero, fromParent, toParent, duration, upToDown);
	}

	public void EnemyForceDead(int index)
	{
		CharEnemy enemy = GetEnemy(index);
		enemy.enemyData.AddHp(-enemy.enemyData.Hp);
		if(!EnemyDeadList.Contains(index))
		{
			EnemyDeadPlaying++;
			EnemyDeadList.Add(index);
		}
		enemy.Stop();
		
		TweenAlpha ta = TweenAlpha.Begin(enemy.gameObject, 0.5f, 0f);
		ta.ignoreTimeScale = false;
		EventDelegate.Add(ta.onFinished, EnemyDeadEnd, true);

	}

	private void EnemyDeadEnd()
	{
		EnemyDeadPlaying--;
		NGUITools.SetActive(TweenShake.current.gameObject, false);
	}

	public void ShowDamage(int index, int damage, DamageResult.DAMAGE_EFFECT ef)
	{
		Vector3 fxPos;
		if(index == 0)	//Hit Player
		{
			UpdateHP();
			Vector3 pos = new Vector3(0f, hpBar.transform.localPosition.y, 0f);
			fxPos = charPanelObj.transform.localPosition + pos;
		}
		else
		{
			//Hit Enemy
			CharEnemy enemy = GetEnemy(index);
			enemy.UpdateInfo();
			fxPos = enemyPanelObj.transform.localPosition + enemy.transform.localPosition;
		}

		GameObject go = NGUITools.AddChild(fxPanelObj, damagePrefab);
		UILabel label = go.GetComponent<UILabel>();
		label.color = Color.red;
		if(index == 0)
		{
			//Hit Player
			label.text = "-" + damage.ToString();
		}
		else
		{
			//Hit Enemy
			if(FightManager.Instance.QTEShield)
				label.text = "0";
			else
				label.text = damage.ToString();
		}
		go.transform.localPosition = fxPos;

		TweenY ty = TweenY.Begin(go, 0.5f, fxPos.y + 30f);
		ty.ignoreTimeScale = false;

		TweenAlpha ta = TweenAlpha.Begin(go, 0.5f, 0f);
		ta.ignoreTimeScale = false;
		ta.delay = 0.3f;
		EventDelegate.Add(ta.onFinished, DamageEnd, true);

		//Text Effect
		if(ef == DamageResult.DAMAGE_EFFECT.WEAK)
		{
			TweenScale ts = TweenScale.Begin(go, 0.3f, new Vector3(1.5f, 1.5f, 1.5f));
			ts.ignoreTimeScale = false;
		}
		else if(ef == DamageResult.DAMAGE_EFFECT.RESIST)
		{
			TweenScale ts = TweenScale.Begin(go, 0.3f, new Vector3(0.7f, 0.7f, 0.7f));
			ts.ignoreTimeScale = false;
		}
	}
	private void DamageEnd()
	{
		Destroy(TweenAlpha.current.gameObject);
	}

	public void ShowHeal(int index, int heal)
	{
		Vector3 fxPos = Vector3.zero;
		if(index == 0)	//Heal Player
		{
			FightManager.Instance.AddHp(heal);
			UpdateHP();

			Vector3 pos = new Vector3(0f, hpBar.transform.localPosition.y, 0f);
			fxPos = charPanelObj.transform.localPosition + pos;
		}
		else
		{
			//Heal Enemy
			CharEnemy enemy = GetEnemy(index);
			enemy.enemyData.AddHp(heal);
			enemy.UpdateInfo();

			fxPos = enemyPanelObj.transform.localPosition + enemy.transform.localPosition;
		}

		//FX
		FightManager.Instance.PlayFX("HealFX", fxPos, fxPanelObj);
		//Sound
		Message.Send(MessageID.PlaySound, SoundID.BATTLE_HEAL);

		//Number
		GameObject go = NGUITools.AddChild(fxPanelObj, damagePrefab);
		UILabel label = go.GetComponent<UILabel>();
		label.color = Color.green;
		label.text = heal.ToString();
		go.transform.localPosition = fxPos;
		
		TweenY ty = TweenY.Begin(go, 0.5f, fxPos.y + 30f);
		ty.ignoreTimeScale = false;
		
		TweenAlpha ta = TweenAlpha.Begin(go, 0.5f, 0f);
		ta.ignoreTimeScale = false;
		ta.delay = 0.3f;
		EventDelegate.Add(ta.onFinished, DamageEnd, true);
	}

	public void AddActionToChar(int index, int lv, float percent)
	{
		CharIcon icon = GetChar(index);
		if(icon != null)
		{
			if(lv == 0)
				icon.AddAction(percent);
			else
			{
				icon.AddAction(lv);
				ShowStatusText(index, StatusText.STATUS_TYPE.LV_UP, lv);
			}

			//FX
			if(percent < 0)
				PlayFXTo(BaseUnit.SIDE.ALLY, index, "DebuffFX");
			else
				PlayFXTo(BaseUnit.SIDE.ALLY, index, "AddActionFX");
			//Sound
			Message.Send(MessageID.PlaySound, SoundID.BATTLE_BUFF);
		}
	}

	public void ShowStatusText(int index, StatusText.STATUS_TYPE type, float number, string str = "")
	{
		CharIcon icon = GetChar(index);
		if(icon != null)
		{
			GameObject go = NGUITools.AddChild(fxPanelObj, statusPrefab);
			StatusText text = go.GetComponent<StatusText>();
			text.Init(type, number, str);
			Vector3 pos = charPanelObj.transform.localPosition + icon.transform.localPosition;
			go.transform.localPosition = pos;

			TweenY ty = TweenY.Begin(go, 0.5f, pos.y + 30f);
			ty.ignoreTimeScale = false;
			
			TweenAlpha ta = TweenAlpha.Begin(go, 0.5f, 0f);
			ta.ignoreTimeScale = false;
			ta.delay = 0.3f;
			EventDelegate.Add(ta.onFinished, DamageEnd, true);

		}
	}

	//Enemy Actions.
	public void EnemyAttack(int enemyIndex, EventDelegate.Callback callback)
	{
		CharEnemy enemy = GetEnemy(enemyIndex);
		TweenScale ts = TweenScale.Begin(enemy.gameObject, 0.1f, new Vector3(1.2f, 1.2f, 1.2f));
		ts.ignoreTimeScale = false;
		if(callback != null)
			EventDelegate.Add(ts.onFinished, callback, true);
	}
	public void EnemyAttackEnd(int enemyIndex)
	{
		CharEnemy enemy = GetEnemy(enemyIndex);
		TweenScale ts = TweenScale.Begin(enemy.gameObject, 0.1f, Vector3.one);
		ts.ignoreTimeScale = false;
	}

	public void HitPlayer(int hp, BaseUnit.ELEMENT element)
	{
		//Sound
		Message.Send(MessageID.PlaySound, SoundID.BATTLE_HIT);
		//FX
		Vector3 pos = charPanelObj.transform.localPosition + hpBar.transform.localPosition;
		FightManager.Instance.PlayFX("HitFX", new Vector3(0f, pos.y, 0f), fxPanelObj);

		FightManager.Instance.AddHp(-hp);
		TweenShake ts = TweenShake.Begin(hpBar.gameObject, 0.5f, 20f);
		ts.ignoreTimeScale = false;

		//Trigger
		FightManager.Instance.TriggerPassiveSkill(FightManager.PASSIVE_TRIGGER.PLAYER_HIT, element);
	}
	
	public void EnemyTalk(int enemyIndex, string msg, Action callback = null)
	{
		CharEnemy enemy = GetEnemy(enemyIndex);
		enemy.Talk(msg, callback);
	}

	public void EnemyCD(int enemyIndex, float sec, Action callback = null)
	{
		CharEnemy enemy = GetEnemy(enemyIndex);
		enemy.RunCD(sec, callback);
	}

	public void AddCDToChar(int index, float percent)
	{
		CharIcon icon = GetChar(index);

		float boost = icon.AddTime(percent);
		if(percent < 0)
		{
			ShowStatusText(icon.Index, StatusText.STATUS_TYPE.CD_UP, boost);
			//FX
			PlayFXTo(BaseUnit.SIDE.ALLY, icon.Index, "DebuffFX");
			//Sound
			Message.Send(MessageID.PlaySound, SoundID.BATTLE_DEBUFF);
		}
		else
		{
			ShowStatusText(icon.Index, StatusText.STATUS_TYPE.CD_DOWN, boost);
			//FX
			PlayFXTo(BaseUnit.SIDE.ALLY, icon.Index, "SpeedFX");
			//Sound
			Message.Send(MessageID.PlaySound, SoundID.BATTLE_BUFF);
		}
	}

	public void PlayFXTo(BaseUnit.SIDE side, int index, string fxName)
	{
		if(!FightManager.Instance.isFXOn) return;

		Vector3 pos = Vector3.zero;
		if(side == BaseUnit.SIDE.ALLY)
			pos = charPanelObj.transform.localPosition + GetChar(index).transform.localPosition;
		else
			pos = enemyPanelObj.transform.localPosition + GetEnemy(index).transform.localPosition;

		fxLoader.PlayFX(fxName, pos, fxPanelObj);
	}

	public void CutIn(CutInTask task)
	{
		//Challenge.
		if(task.Side == BaseUnit.SIDE.ALLY)
			FightManager.Instance.PowerSkillUse++;

		//Sound
		Message.Send(MessageID.PlaySound, SoundID.BATTLE_CUTIN);

		float x = 640f;
		float dest = 0f;
		if(task.Side == BaseUnit.SIDE.ENEMY)
		{
			x = -x;
			dest = -dest;
		}

		//Talk
		NGUITools.SetActive(cutInTalkBg, (task.Side == BaseUnit.SIDE.ALLY));
		cutInTalkLabel.text = task.Talk;
		//Skill Info
		cutInSkillName.text = task.SkillName;
		cutInSkillDetail.text = task.SkillDetail;
		//Texture
		cutInTex.mainTexture = fxLoader.GetTexture(task.Path);
		//Recover Tween Status.
		cutInSlideObj.transform.localPosition = new Vector3(x, 0f, 0f);
		cutInPanel.alpha = 1f;
		cutInSkillPanel.alpha = 0f;

		NGUITools.SetActive(cutInPanel.gameObject, true);

		TweenX tx = TweenX.Begin(cutInSlideObj, 0.2f, dest);
		tx.ignoreTimeScale = false;

		TweenAlpha ta = TweenAlpha.Begin(cutInSkillPanel.gameObject, 0.2f, 1f);
		ta.ignoreTimeScale = false;

		TweenAlpha ta2 = TweenAlpha.Begin(cutInPanel.gameObject, 0.5f, 0f);
		ta2.ignoreTimeScale = false;
		ta2.delay = 1f;
		EventDelegate.Add(ta2.onFinished, CutInEnd, true);
	}

	public IEnumerator RoundChange(EventDelegate.Callback callback)
	{
		changeRoundCallback = callback;

		//Waiting enemy dead.
		float waitingTime = 0f;
		while(EnemyDeadPlaying > 0 && waitingTime < 3)
		{
			waitingTime += Time.deltaTime;
			yield return 0;
		}

		//Clear enemy after dead play finished.
		FightManager.Instance.ClearEnemyData();

		float walkSec = 2f;
		bool isFinalWave = (FightManager.Instance.Round == FightManager.Instance.TotalRounds - 2);
		//final wave.
		if(isFinalWave)
		{
			//walkSec = 4f;

			NGUITools.SetActive(finalWaveObj, true);
			finalWaveBg.alpha = 0f;
			finalWaveTitle.transform.localScale = Vector3.zero;
			Vector3 pos1 = finalWaveBorder1.transform.localPosition;
			Vector3 pos2 = finalWaveBorder2.transform.localPosition;
			finalWaveBorder1.transform.localPosition = new Vector3(677f, pos1.y, pos1.z);
			finalWaveBorder2.transform.localPosition = new Vector3(-677f, pos2.y, pos2.z);

			TweenAlpha taw = TweenAlpha.Begin(finalWaveBg.gameObject, 1f, 0.8f);
			taw.ignoreTimeScale = true;
			taw.style = UITweener.Style.PingPong;

			TweenScale tsw = TweenScale.Begin(finalWaveTitle, 0.3f, Vector3.one);
			tsw.ignoreTimeScale = true;
			TweenX txw1 = TweenX.Begin(finalWaveBorder1.gameObject, 0.3f, 0f);
			txw1.ignoreTimeScale = true;
			TweenX txw2 = TweenX.Begin(finalWaveBorder2.gameObject, 0.3f, 0f);
			txw2.ignoreTimeScale = true;

			//sound
			Message.Send(MessageID.MusicVolume, Context.Instance.SystemSetting.MusicVolume * 0.2f);
			Message.Send(MessageID.PlaySound, SoundID.BATTLE_WARNING);
		}

		dashBoard.CloseAllLight();
		bgTextureFront.depth = 0;

		TweenY ty = TweenY.Begin(bgTextureFront.gameObject, walkSec, -50f);
		ty.ignoreTimeScale = true;
		ty.animationCurve = walkCurve;

		TweenScale ts = TweenScale.Begin(bgTextureFront.gameObject, walkSec, new Vector3(1.5f, 1.5f, 1.5f));
		ts.ignoreTimeScale = true;

		TweenAlpha ta = TweenAlpha.Begin(bgTextureFront.gameObject, walkSec - 1f, 0f);
		ta.ignoreTimeScale = true;
		ta.delay = 1f;
		if(isFinalWave)
		{
			EventDelegate.Add(ta.onFinished, FinalWaveFadeOut, true);
		}
		else if(callback != null)
			EventDelegate.Add(ta.onFinished, callback, true);

	}
	
	void FinalWaveFadeOut()
	{
		//Recover Music Volumn.
		Message.Send(MessageID.MusicVolume, Context.Instance.SystemSetting.MusicVolume);

		TweenAlpha ta = TweenAlpha.Begin(finalWaveBg.gameObject, 0.3f, 0f);
		ta.ignoreTimeScale = true;

		TweenScale ts = TweenScale.Begin(finalWaveTitle, 0.3f, Vector3.zero);
		ts.ignoreTimeScale = true;
		if(changeRoundCallback != null)
			EventDelegate.Add(ts.onFinished, changeRoundCallback, true);

		TweenX tx1 = TweenX.Begin(finalWaveBorder1.gameObject, 0.3f, -677f);
		tx1.ignoreTimeScale = true;
		TweenX tx2 = TweenX.Begin(finalWaveBorder2.gameObject, 0.3f, 677f);
		tx2.ignoreTimeScale = true;
	}

	public IEnumerator FightWinPlay()
	{
		Context.Instance.FightPowerList = new List<int>(FightManager.Instance.powerList);

		//Close skill confirm window
		if(tempMsg != null && tempMsg.messageBox != null && tempMsg.messageBox.gameObject != null)
			Destroy(tempMsg.messageBox.gameObject);

		//Sound
		Message.Send(MessageID.MusicVolume, 0f);
		Message.Send(MessageID.PlaySound, SoundID.BATTLE_WIN);

		float waitingTime = 0f;
		while(EnemyDeadPlaying > 0 && waitingTime < 3)
		{
			waitingTime += Time.deltaTime;
			yield return 0;
		}

		//Clear enemy after dead play finished.
		FightManager.Instance.ClearEnemyData();

		endLogo.spriteName = "stage_clear";
		endLogo.transform.localPosition = Vector3.zero;
		NGUITools.SetActive(endLogo.gameObject, true);
		TweenY ty = TweenY.Begin(endLogo.gameObject, 0.5f, 200f);
		ty.ignoreTimeScale = false;
		EventDelegate.Add(ty.onFinished, OpenEndButton, true);

		//NGUITools.SetActive(endButton.gameObject, true);

	}
	private void OpenEndButton()
	{
		//FX
		FightManager.Instance.PlayFX("CircleLightFX", Vector3.zero, endFXObj);
		winPlayFinished = true;
	}

	public void FightLosePlay(EventDelegate.Callback callback)
	{
		//Sound
		Message.Send(MessageID.MusicVolume, 0f);
		Message.Send(MessageID.PlaySound, SoundID.BATTLE_LOSE);

		backBlack.alpha = 0f;
		NGUITools.SetActive(backBlack.gameObject, true);
		TweenAlpha ta = TweenAlpha.Begin(backBlack.gameObject, 0.3f, 0.5f);
		ta.ignoreTimeScale = false;

		endLogo.spriteName = "stage_failed";
		endLogo.transform.localPosition = new Vector3(0f, 512f, 0f);
		NGUITools.SetActive(endLogo.gameObject, true);
		TweenY ty = TweenY.Begin(endLogo.gameObject, 1f, 200f);
		ty.ignoreTimeScale = false;
		ty.method = UITweener.Method.BounceIn;

		if(callback != null)
			EventDelegate.Add(ty.onFinished, callback, true);
		//NGUITools.SetActive(endButton.gameObject, true);
		EnableTouch(false);
	}

	public void RevivePlay()
	{
		//Recover BG Music volumn.
		Message.Send(MessageID.MusicVolume, Context.Instance.SystemSetting.MusicVolume);

		NGUITools.SetActive(backBlack.gameObject, false);
		NGUITools.SetActive(endLogo.gameObject, false);
	}

	public void LeaveFightPlay(EventDelegate.Callback callback)
	{
		TweenAlpha ta = TweenAlpha.Begin(foreBlack.gameObject, 1f, 1f);
		ta.ignoreTimeScale = false;
		if(callback != null)
			EventDelegate.Add(ta.onFinished, callback, true);
	}

#endregion

#region QTE Functions.
	public void InitQTE(EventDelegate.Callback qteCallback)
	{
		//Tip label
		qteTip.text = ConstData.Tables.GetSystemText(218);
		NGUITools.SetActive(qteTip.gameObject, true);
		TweenAlpha ta = TweenAlpha.Begin(qteTip.gameObject, 3f, 0f);
		ta.ignoreTimeScale = false;
		ta.animationCurve = qteTipCurve;
		if(qteCallback != null)
			EventDelegate.Add(ta.onFinished, qteCallback, true);

		List<ActItem.ACT_TYPE> lst = new List<ActItem.ACT_TYPE>(){ ActItem.ACT_TYPE.Attack, ActItem.ACT_TYPE.Heal, ActItem.ACT_TYPE.Skill };
		int MAX_TOTAL_LV = 15;
		int count = qteBtnArray.Length;
		List<int> lstLv = new List<int>();
		//random lv, max total to 15.
		for(int i = 1; i <= count; i++)
		{
			int lv = Mikan.MathUtils.Random.Next(1, Math.Min(MAX_TOTAL_LV - (count - i), ActItem.MAX_LV) + 1);
			MAX_TOTAL_LV -= lv;
			lstLv.Add(lv);
		}
		Mikan.MathUtils.Shuffle(lstLv);
		//Init qte button.
		for(int i = 0; i < count; i++)
		{
			QTEButton btn = qteBtnArray[i];
			btn.Init(lst[Mikan.MathUtils.Random.Next(0, 3)], lstLv[i]);
			NGUITools.SetActive(btn.gameObject, false);
		}
		NGUITools.SetActive(qteObj, true);
		NGUITools.SetActive(qteResult.gameObject, false);
		QTESetFocus(0);
		NGUITools.SetActive(qteArrowObj, false);
	}

	public void InitQTEShield(Mikan.CSAGA.ConstData.Skill skillData)
	{
		FightManager.Instance.QTEShield = true;
		//Tip label
		if(FightManager.Instance.QTEMode)
			qteTip.text = ConstData.Tables.GetSystemText(SystemTextID._218_TIP_UNSEAL);
		else
			qteTip.text = ConstData.Tables.GetSystemText(496);
		qteTip.alpha = 1f;
		NGUITools.SetActive(qteTip.gameObject, true);
		TweenAlpha ta = TweenAlpha.Begin(qteTip.gameObject, 3f, 0f);
		ta.ignoreTimeScale = false;
		ta.animationCurve = qteTipCurve;

		List<ActItem.ACT_TYPE> lst = new List<ActItem.ACT_TYPE>(){ ActItem.ACT_TYPE.Attack, ActItem.ACT_TYPE.Heal, ActItem.ACT_TYPE.Skill };
		List<ActItem.ACT_TYPE> lstItem = new List<ActItem.ACT_TYPE>();
		List<int> lstLv = new List<int>();
		//Data
		int maxCount = QTE_ITEM_COUNT;
		Mikan.CSAGA.ConstData.Skill skill = skillData;
		while(skill != null && maxCount > 0)
		{
			if(skill.n_EFFECT == 14)
			{
				ActItem.ACT_TYPE type = ActItem.ACT_TYPE.Attack;
				int itemId = skill.n_EFFECT_X;
				if(itemId == 0)
					type = lst[Mikan.MathUtils.Random.Next(0, 3)];
				else
					type = (ActItem.ACT_TYPE)skill.n_EFFECT_X;
				lstItem.Add(type);

				int lv = Mikan.MathUtils.Random.Next(skill.n_EFFECT_Y, skill.n_EFFECT_Z + 1);
				lstLv.Add(lv);

				maxCount--;
			}
			int linkId = skill.n_SKILL_LINK;
			skill = ConstData.Tables.Skill[linkId];
		}
		//Init qte button.
		int emptyCount = QTE_ITEM_COUNT - lstItem.Count;
		qteBtnRoot.transform.localPosition = new Vector3(-50f * (float)emptyCount, 0f, 0f);
		for(int i = QTE_ITEM_COUNT - 1; i >= 0; i--)
		{
			QTEButton btn = qteBtnArray[i];
			int itemIndex = i - emptyCount;
			if(itemIndex >= 0)
			{
				btn.Init(lstItem[itemIndex], lstLv[itemIndex]);
				NGUITools.SetActive(btn.gameObject, true);
			}
			else
			{
				NGUITools.SetActive(btn.gameObject, false);
			}
		}
		NGUITools.SetActive(qteObj, true);
		NGUITools.SetActive(qteResult.gameObject, false);
		QTESetFocus(0);
		NGUITools.SetActive(qteArrowObj, true);

		QTESetFocus(emptyCount);
	}

	public void QTESetFocus(int index)
	{
		QTEButton btn = qteBtnArray[index];
		btn.SetFocus();
		Vector3 pos = qteArrowObj.transform.localPosition;
		qteArrowObj.transform.localPosition = new Vector3(btn.transform.localPosition.x, pos.y, pos.z);
		QTEIndex = index;
	}

	public void QTECheck(ActItemCommand item)
	{
		QTEButton btn = qteBtnArray[QTEIndex];
		if(item.Type == btn.Type && item.Lv == btn.Lv)
		{
			NGUITools.SetActive(btn.gameObject, false);
			//FX
			fxLoader.PlayFX("QTECheckFX", enemyPanelObj.transform.localPosition + btn.transform.localPosition + qteBtnRoot.transform.localPosition, fxPanelObj);
			//Sound
			Message.Send(MessageID.PlaySound, SoundID.QTE_CRASH);

			if(QTEIndex == QTE_ITEM_COUNT - 1)
			{
				FightManager.Instance.QTEClear = true;	//all clear.
				FightManager.Instance.QTEShield = false;
				if(FightManager.Instance.QTEMode)
					QTEResultPlay();
				else
					QTEShieldEnd();
			}
			else
				QTESetFocus(QTEIndex + 1);
		}
	}

	public void QTEResultPlay()
	{
		//end this wave.
		//FightManager.Instance.FightEnd = true;
		EnableTouch(false);
		EnableSystemButton(false);
		FightStop();

		qteResult.transform.localPosition = new Vector3(800f, 0f, 0f);
		qteResult.alpha = 1f;
		qteResult.spriteName = (FightManager.Instance.QTEClear) ? "help_success" : "help_timeup";
		NGUITools.SetActive(qteResult.gameObject, true);
		//animation
		TweenX tx = TweenX.Begin(qteResult.gameObject, 0.3f, 0f);
		tx.ignoreTimeScale = false;
		EventDelegate.Add(tx.onFinished, QTEFadeOut, true);
	}
	private void QTEFadeOut()
	{
		TweenAlpha ta = TweenAlpha.Begin(qteResult.gameObject, 1f, 0f);
		ta.ignoreTimeScale = false;
		EventDelegate.Add(ta.onFinished, CloseQTE, true);
	}
	private void CloseQTE()
	{
		NGUITools.SetActive(qteObj, false);
		if(FightManager.Instance.QTEMode)
			FightManager.Instance.EndQTE();
	}

	public void QTEShieldEnd()
	{
		qteResult.transform.localPosition = new Vector3(800f, 0f, 0f);
		qteResult.alpha = 1f;
		qteResult.spriteName = (FightManager.Instance.QTEClear) ? "help_success" : "help_timeup";
		NGUITools.SetActive(qteResult.gameObject, true);
		//animation
		TweenX tx = TweenX.Begin(qteResult.gameObject, 0.3f, 0f);
		tx.ignoreTimeScale = false;
		EventDelegate.Add(tx.onFinished, QTEFadeOut, true);
	}
#endregion

#region Cut In Functions.

	public void AddCutIn(BaseUnit unit, int skillId, Action callback)
	{
		//Talk
		string msg = "";
		if(unit.Side == BaseUnit.SIDE.ALLY)
		{
			Mikan.CSAGA.ConstData.CardText data = ConstData.Tables.CardText[unit.CardID];
			if(data != null)
				msg = data.s_CUTIN_WORD;
		}
		//Skill Info
		string sName = "";
		string sDetail = "";
		Mikan.CSAGA.ConstData.SkillText stData = ConstData.Tables.SkillText[skillId];
		if(stData != null)
		{
			sName = stData.s_NAME;
			sDetail = stData.s_TIP;
		}

		CutInTask task = new CutInTask(unit.Side, "CARD/CARD_" + unit.texName, msg, sName, sDetail, callback);
		CutInQueue.Enqueue(task);
	}
	
	public void PlayCutIn()
	{
		if(CutInQueue.Count > 0)
		{
			IsCutInPlaying = true;
			EnableSystemButton(false);
			FightStop();
			PlayingTask = CutInQueue.Dequeue();
			CutIn(PlayingTask);
		}
	}

	private void CutInEnd()
	{
		NGUITools.SetActive(cutInPanel.gameObject, false);
		if(!FightManager.Instance.FightEnd)
		{
			EnableSystemButton(true);
			FightStart();
			PlayingTask.Callback();
		}
		IsCutInPlaying = false;
	}

#endregion

#region Button Click Functions.
	public void DebugCharInfo(GameObject btn)
	{
		bool show = !debugPanel.IsShow;
		debugPanel.Show(show);
		if(show)
			debugPanel.ShowCharInfo();
	}

#endregion
}
