﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class CharIcon : MonoBehaviour {

	public const int MAX_ACTION_POINT = 2;
	public const float BUFF_ICON_X = -40f;
	public const float BUFF_ICON_Y = 70f;
	public const float BUFF_ICON_WIDTH = 30f;

	public int Index;

	public UISprite bg;
	public UISprite bgFront;
	public UITexture iconTex;
	public UISprite elementSprite;
	public UISlider ActionBar1;
	public UISlider ActionBar2;
	public UILabel cdLabel;
	public GameObject fxPanel;
	public UILabel stunTimeLabel;
	public UISprite silentSprite;
	//Prefab
	public GameObject buffPrefab;

	public bool Enable = false;
	public int ActionPoint;
	public float SpeedRate = 1f;
	private float AccTime;
	private bool pause = true;
	private float FullTime = 3f;
	private float MaxAccTime;
	private float SecondActionRate = 1f;
	//Active Skill CD
	private float AccSkillTime;
	private float SkillCDTime;
	//FX
	private FXController stunFX = null;

	public FightUnit data;

	private List<BuffIcon> BuffList = new List<BuffIcon>();
	private Action InitCallback;
	private bool skillAvailable = false;
	private bool isStun = false;
	private bool isSilent = false;

	public void Init(FightUnit unit, Action callback)
	{
		InitCallback = callback;
		pause = true;
		data = unit;

		if(unit != null)
		{
			Enable = true;

			Index = unit.Index;
			FullTime = unit.Spd;
			MaxAccTime = FullTime * MAX_ACTION_POINT;
			AccTime = 0f;
			SecondActionRate = ConstData.Tables.GetVariablef(Mikan.CSAGA.VariableID.SECOND_ACT_TIME) 
				/ ((float)(100f + unit.ActionBoost) / 100f);

			SkillCDTime = 0f;
			if(unit.ActiveSkill != null)
				SkillCDTime = unit.ActiveSkill.n_CD;
			AccSkillTime = SkillCDTime * (float)(100f - unit.SkillBoost) / 100f;
			if(AccSkillTime < 0)
				AccSkillTime = 0;
			cdLabel.text = ((int)AccSkillTime).ToString();

			NGUITools.SetActive(elementSprite.gameObject, true);
			NGUITools.SetActive(cdLabel.gameObject, true);
			NGUITools.SetActive(bgFront.gameObject, true);
			bg.spriteName = "battle_icon_00";
			bgFront.spriteName = "battle_icon_00a";
			UpdateBar();

			elementSprite.spriteName = "attribute_" + ((int)unit.Element).ToString("000");
			ResourceManager.Instance.LoadTexture("ICON/IC_" + unit.iconName, LoadTextureComplete);
		}
		else
		{
			Enable = false;
			bg.spriteName = "battle_icon_02";
			NGUITools.SetActive(bgFront.gameObject, false);
			iconTex.mainTexture = null;
			NGUITools.SetActive(elementSprite.gameObject, false);
			ActionBar1.value = 0f;
			ActionBar2.value = 0f;
			NGUITools.SetActive(cdLabel.gameObject, false);
		}
	}

	private void LoadTextureComplete(IAsset<Texture2D> asset)
	{
		iconTex.mainTexture = asset.Content;
		InitCallback();
	}

	// Update is called once per frame
	void Update () {
		if(!pause && data != null)
		{
			//Acc Action 
			if(AccTime < MaxAccTime)
			{
				float add = Time.deltaTime * SpeedRate;
				if(AccTime > FullTime)	//Second Action Bar.
					add /= SecondActionRate;
				AccTime += add;
				if(AccTime >= MaxAccTime)
				{
					AccTime = MaxAccTime;
					if(!isStun)
						FightManager.Instance.TriggerPassiveSkillById(Index, FightManager.PASSIVE_TRIGGER.ACTION_FULL);
				}

				UpdateBar();
			}
			//Skill Action
			if(AccSkillTime > 0)
			{
				AccSkillTime -= Time.deltaTime;
				if(AccSkillTime < 0)
					AccSkillTime = 0;
				cdLabel.text = ((int)AccSkillTime).ToString();
				//bg style
				if(!skillAvailable && AccSkillTime == 0)
				{
					skillAvailable = true;
					bg.spriteName = "battle_icon_01";
					bgFront.spriteName = "battle_icon_01a";
				}
			}

			//Buff pass.
			if(data.ConsumeBuff(Time.deltaTime))
				UpdateBuffIcon();

			//Stun Time.
			if(isStun)
			{
				Buff b = data.GetBuff((int)Buff.BUFF_TYPE.STUN);
				if(b != null)
					stunTimeLabel.text = ((int)b.TimeLeft).ToString();
			}
		}
	}

	void OnClick()
	{
		if(SkillCDTime == 0) return;
#if !DEBUG
		if(AccSkillTime == 0)
#endif
		{
			//Stun.
			if(data.GetBuff((int)Buff.BUFF_TYPE.STUN) != null)
				return;
			//Silent
			if(data.GetBuff((int)Buff.BUFF_TYPE.SILENT) != null)
				return;

			if(Context.Instance.BattleSetting.SkillCheck)
			{
				string title = "";
				string tip = "";
				Mikan.CSAGA.ConstData.SkillText text = ConstData.Tables.SkillText[data.ActiveSkill.n_ID];
				if(text != null)
				{
					title = text.s_NAME;
					tip = text.s_TIP;
				}
				MessageBoxContext context = MessageBox.ShowConfirmWindow(tip, SkillConfirm);
				context.title = title;
				FightManager.Instance.playManager.tempMsg = context;
			}
			else
				SkillConfirm(0);
		}
	}

	void SkillConfirm(int res)
	{
		if(res == 0 && !FightManager.Instance.FightEnd && data.GetBuff((int)Buff.BUFF_TYPE.STUN) == null && data.GetBuff((int)Buff.BUFF_TYPE.SILENT) == null)
		{
			AccSkillTime = SkillCDTime;
			cdLabel.text = ((int)AccSkillTime).ToString();
			//bg style
			skillAvailable = false;
			bg.spriteName = "battle_icon_00";
			bgFront.spriteName = "battle_icon_00a";

			FightManager.Instance.ActiveSkill(Index);
		}
	}

	public void Go()
	{
		pause = false;
	}

	public void Stop()
	{
		pause = true;
	}

	private void UpdateBar()
	{
		ActionPoint = (int)(AccTime / FullTime);
		
		if(AccTime < FullTime)
		{
			ActionBar1.value = AccTime / FullTime;
			ActionBar2.value = 0f;
		}
		else
		{
			ActionBar1.value = 1f;
			ActionBar2.value = (AccTime - FullTime) / FullTime;
		}
	}

	public void ConsumeAction()
	{
		AccTime -= FullTime;
		UpdateBar();
	}

	public float AddTime(float t)
	{
		//t is %
		float boost = SkillCDTime * t / 100f;
		float oldCD = AccSkillTime;
		AccSkillTime -= boost;
		if(AccSkillTime < 0)
			AccSkillTime = 0;
		else if(AccSkillTime > SkillCDTime)
			AccSkillTime = SkillCDTime;
		cdLabel.text = ((int)AccSkillTime).ToString();
		return Mathf.Abs(oldCD - AccSkillTime);
	}

	public void UpdateSpeed()
	{
		SpeedRate = 1f;
		Buff up = data.GetBuff((int)Buff.BUFF_TYPE.SPEED_UP);
		if(up != null)
			SpeedRate *= 1f + (up.XValue / 100f);
		Buff down = data.GetBuff((int)Buff.BUFF_TYPE.SPEED_DOWN);
		if(down != null)
			SpeedRate *= 1f - (down.XValue / 100f);

	}

	public void UpdateBuffIcon()
	{
		UpdateSpeed();

		//Del buff icon.
		List<BuffIcon> lstDel = new List<BuffIcon>();
		for(int i = 0; i < BuffList.Count; i++)
		{
			BuffIcon icon = BuffList[i];
			if(data.GetBuff(icon.BuffID) == null)
			{
				lstDel.Add(icon);
			}
		}
		for(int i = 0; i < lstDel.Count; i++)
		{
			BuffIcon icon = lstDel[i];
			//Close Stun FX and Stun Time
			if(icon.BuffID == (int)Buff.BUFF_TYPE.STUN)
			{
				if(stunFX != null)
					stunFX.SelfRecycle();
				NGUITools.SetActive(stunTimeLabel.gameObject, false);
				isStun = false;
			}
			//Silent effect
			if(icon.BuffID == (int)Buff.BUFF_TYPE.SILENT)
			{
				NGUITools.SetActive(silentSprite.gameObject, false);
				isSilent = false;
			}
			//Delete icon.
			BuffList.Remove(icon);
			Destroy(icon.gameObject);
		}

		//Add new buff icon.
		bool iconAdded = false;
		foreach(Buff b in data.BuffDict.Values)
		{
			BuffIcon bi = null;
			for(int i = 0; i < BuffList.Count; i++)
			{
				BuffIcon icon = BuffList[i];
				if(icon.BuffID == b.BuffID)
				{
					bi = icon;
					break;
				}
			}
			if(bi == null)
			{
				//Add buff icon.
				BuffIcon icon = NGUITools.AddChild(gameObject, buffPrefab).GetComponent<BuffIcon>();
				icon.Init(b);
				BuffList.Add(icon);
				iconAdded = true;
				//Add Stun FX and stun time.
				if(icon.BuffID == (int)Buff.BUFF_TYPE.STUN)
				{
					stunFX = FightManager.Instance.PlayFX("StunFX", Vector2.zero, fxPanel);
					stunTimeLabel.text = ((int)b.TimeLeft).ToString();
					NGUITools.SetActive(stunTimeLabel.gameObject, true);
					isStun = true;
				}
				//Silent effect
				if(icon.BuffID == (int)Buff.BUFF_TYPE.SILENT)
				{
					NGUITools.SetActive(silentSprite.gameObject, true);
					isSilent = true;
				}
			}
			else if(b.NeedRefresh)
			{
				bi.Init(b);
			}
		}

		//Resort icon.
		if(lstDel.Count > 0 || iconAdded)
		{
			for(int i = 0; i < BuffList.Count; i++)
			{
				BuffIcon icon = BuffList[i];
				icon.transform.localPosition = new Vector3(BUFF_ICON_X + BUFF_ICON_WIDTH * i, BUFF_ICON_Y, 0f);
			}
		}
	}

	public void AddAction(int lv)
	{
		if(AccTime < MaxAccTime)
		{
			float bonus = 0;
			Mikan.CSAGA.ConstData.Variable data = ConstData.Tables.Variable[41];
			if(data != null)
				bonus = data.f_VALUE_VARIABLE;

			float add = (float)lv * bonus / 100f * FullTime;
			AccTime += add;
			if(AccTime > MaxAccTime)
				AccTime = MaxAccTime;
			
			UpdateBar();
		}
	}

	public void AddAction(float percent)
	{
		//if(AccTime < MaxAccTime)
		//{
			float add = FullTime * percent;
			AccTime += add;
			if(AccTime > MaxAccTime)
				AccTime = MaxAccTime;
			else if(AccTime < 0)
				AccTime = 0;
			
			UpdateBar();
		//}
	}
}
