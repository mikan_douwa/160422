﻿using UnityEngine;
using System.Collections;

public class PlayActItem : MonoBehaviour {

	public UISprite sprite;
	public UISprite lv;
	public UILabel label;
	public UISprite max;

	public DashBoard dashBoard;

	public void SetItem(ActItem item, Vector3 pos)
	{
		sprite.spriteName = "battle_board_" + ((int)item.Type).ToString("000");
		bool isMax = (item.Lv == ActItem.MAX_LV);
		if(!isMax)
			label.text = item.Lv.ToString();

		NGUITools.SetActive(lv.gameObject, !isMax);
		NGUITools.SetActive(label.gameObject, !isMax);
		NGUITools.SetActive(max.gameObject, isMax);

		transform.localPosition = pos;
	}

	public void Play()
	{
		float y = transform.localPosition.y;
		TweenY ty = TweenY.Begin(gameObject, 0.1f, y + 100f);
		ty.ignoreTimeScale = false;
		EventDelegate.Add(ty.onFinished, Recycle, true);
	}

	void Recycle()
	{
		dashBoard.RecyclePlayItem(this);
	}
}
