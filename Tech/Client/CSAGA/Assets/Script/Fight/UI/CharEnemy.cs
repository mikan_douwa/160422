﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class CharEnemy : MonoBehaviour {

	public const float ENEMY_INIT_Y = 0f;
	public const float BUFF_ICON_Y = -110f;
	public const float BUFF_ICON_WIDTH = 50f;

	public float BUFF_ICON_X = -40f;

	public int Index;

	public UITexture texture;
	public UISprite elementSprite;
	public UISlider hpBar;
	public UISlider actionBar;
	public UISlider cdBar;
	public UISprite focusSprite;
	public GameObject talkObj;
	public UILabel talkLabel;
	//Prefab
	public GameObject buffPrefab;

	public FightEnemy enemyData;

	private float AccTime;
	private bool pause = true;
	private bool actionStop = false;
	private float FullTime = 5f;
	private float SpeedRate = 1f;
	private List<BuffIcon> BuffList = new List<BuffIcon>();
	private Action InitCallback;
	private Action TalkCallback;

	//CD
	private Action CDCallback;
	private float CDMaxTime;
	private float CDProgress;
	private bool inCD = false;


	void Update () {
		if(!pause && enemyData.LifeStatus == FightEnemy.LIFE_STATUS.LIVE)
		{
			if(!actionStop)
			{
				if(AccTime <= FullTime)
				{
					if(AccTime == FullTime)
					{
						//Attack
						AccTime = 0f;
						//actionBar.value = 0f;

						if(enemyData.GetBuff((int)Buff.BUFF_TYPE.STUN) == null)	//Stun detect.
							FightManager.Instance.RunEnemyAI(enemyData);
					}
					else
					{
						//Charge
						float add = Time.deltaTime * SpeedRate;
						AccTime += add;
						if(AccTime > FullTime)
							AccTime = FullTime;
						
						actionBar.value = AccTime / FullTime;
					}
				}
			}
			//Buff pass.
			if(enemyData.ConsumeBuff(Time.deltaTime))
				UpdateBuffIcon();
			//Run CD
			if(inCD)
			{
				CDProgress += Time.deltaTime;
				cdBar.value = CDProgress / CDMaxTime;
				if(CDProgress >= CDMaxTime)
					CDEnd();
			}
		}
	}
	
	public void Go()
	{
		pause = false;
	}
	
	public void Stop()
	{
		pause = true;
	}
	
	public void Init(FightEnemy enemy, int totalEnemy, Action callback)
	{
		InitCallback = callback;
		pause = true;
		enemyData = enemy;
		AccTime = 0f;
		actionBar.value = 0f;
		cdBar.value = 0f;
		hpBar.value = 1f;
		FullTime = enemy.Spd;
		NGUITools.SetActive(focusSprite.gameObject, false);

		if(enemy != null)
		{
			Index = enemy.Index;
			UpdateInfo();
			elementSprite.spriteName = "attribute_" + ((int)enemy.Element).ToString("000");
			transform.localPosition = new Vector3(enemy.initPosX, ENEMY_INIT_Y, 0f);
			//Dynamic hp bar length
			Mikan.CSAGA.VariableID id = Mikan.CSAGA.VariableID.MOBHP_LENGTH_4;
			switch(totalEnemy)
			{
				case 1:
					id = Mikan.CSAGA.VariableID.MOBHP_LENGTH_1;
					break;
				case 2:
					id = Mikan.CSAGA.VariableID.MOBHP_LENGTH_2;
					break;
				case 3:
					id = Mikan.CSAGA.VariableID.MOBHP_LENGTH_3;
					break;
				case 4:
					id = Mikan.CSAGA.VariableID.MOBHP_LENGTH_4;
					break;
			}
			int length = ConstData.Tables.GetVariable(id);
			float x = (float)length / 2f;
			hpBar.backgroundWidget.width = length;
			hpBar.foregroundWidget.width = length - 40;
			Vector3 posHp = hpBar.transform.localPosition;
			hpBar.transform.localPosition = new Vector3(-x, posHp.y, posHp.z);
			actionBar.backgroundWidget.width = length;
			actionBar.foregroundWidget.width = length - 40;
			Vector3 posAc = actionBar.transform.localPosition;
			actionBar.transform.localPosition = new Vector3(-x, posAc.y, posAc.z);
			cdBar.foregroundWidget.width = length - 40;
			Vector3 posCd = cdBar.transform.localPosition;
			cdBar.transform.localPosition = new Vector3(-x, posCd.y, posCd.z);
			Vector3 posEl = elementSprite.transform.localPosition;
			elementSprite.transform.localPosition = new Vector3(-x + 19f, posEl.y, posEl.z);

			BUFF_ICON_X = -x;

			//Animation
			TweenY ty = TweenY.Begin(texture.gameObject, 1f, 20f);
			ty.ignoreTimeScale = false;
			ty.method = UITweener.Method.EaseIn;
			ty.style = UITweener.Style.PingPong;

			//Load
			ResourceManager.Instance.LoadTexture("CARD/CARD_" + enemy.texName, LoadHandler);
		}

	}
	private void LoadHandler(IAsset<Texture2D> asset)
	{
		texture.mainTexture = asset.Content;
		//texture.MakePixelPerfect();
		float size = 1f;
		if(FightManager.Instance.isPVP)
			size = 0.8f;
		else
			size = (float)enemyData.mobData.n_SIZE / 100f;
		texture.transform.localScale = new Vector3(size, size, size);
		InitCallback();
	}

	public void UpdateInfo()
	{
		hpBar.value = (float)enemyData.Hp / (float)enemyData.MaxHp;
	}

	public void OnClick()
	{
		FightManager.Instance.playManager.SetFocus(Index);
	}

	public void SetFocus(bool active)
	{
		NGUITools.SetActive(focusSprite.gameObject, active);
		if(active)
		{
			//Focus Animation.
			focusSprite.transform.localScale = new Vector3(10, 10, 10);
			focusSprite.transform.localRotation = Quaternion.Euler(Vector3.zero);
			focusSprite.alpha = 0f;

			TweenAlpha ta = TweenAlpha.Begin(focusSprite.gameObject, 0.2f, 1f);
			ta.ignoreTimeScale = false;

			TweenScale ts = TweenScale.Begin(focusSprite.gameObject, 0.3f, Vector3.one);
			ts.ignoreTimeScale = false;

			TweenClock tc = TweenClock.Begin(focusSprite.gameObject, 5f, 359);
			tc.ignoreTimeScale = false;
			tc.style = UITweener.Style.Loop;
		}
	}
	
	public void UpdateSpeed()
	{
		SpeedRate = 1f;
		Buff up = enemyData.GetBuff((int)Buff.BUFF_TYPE.SPEED_UP);
		if(up != null)
			SpeedRate *= 1f + (up.XValue / 100f);
		Buff down = enemyData.GetBuff((int)Buff.BUFF_TYPE.SPEED_DOWN);
		if(down != null)
			SpeedRate *= 1f - (down.XValue / 100f);
		
	}

	public void UpdateBuffIcon()
	{
		UpdateSpeed();

		//Del buff icon.
		List<BuffIcon> lstDel = new List<BuffIcon>();
		for(int i = 0; i < BuffList.Count; i++)
		{
			BuffIcon icon = BuffList[i];
			if(enemyData.GetBuff(icon.BuffID) == null)
			{
				lstDel.Add(icon);
			}
		}
		for(int i = 0; i < lstDel.Count; i++)
		{
			BuffIcon icon = lstDel[i];
			BuffList.Remove(icon);
			Destroy(icon.gameObject);
		}
		
		//Add new buff icon.
		bool iconAdded = false;
		foreach(Buff b in enemyData.BuffDict.Values)
		{
			bool hasIcon = false;
			for(int i = 0; i < BuffList.Count; i++)
			{
				BuffIcon icon = BuffList[i];
				if(icon.BuffID == b.BuffID)
				{
					hasIcon = true;
					break;
				}
			}
			if(!hasIcon)
			{
				//Add buff icon.
				BuffIcon icon = NGUITools.AddChild(gameObject, buffPrefab).GetComponent<BuffIcon>();
				icon.Init(b);
				BuffList.Add(icon);
				iconAdded = true;
			}
		}
		
		//Resort icon.
		if(lstDel.Count > 0 || iconAdded)
		{
			for(int i = 0; i < BuffList.Count; i++)
			{
				BuffIcon icon = BuffList[i];
				icon.transform.localPosition = new Vector3(BUFF_ICON_X + BUFF_ICON_WIDTH * i, BUFF_ICON_Y, 0f);
			}
		}
	}

	public void Talk(string msg, Action callback)
	{
		talkLabel.text = msg;
		TalkCallback = callback;

		talkObj.transform.localScale = Vector3.zero;
		NGUITools.SetActive(talkObj, true);
		TweenScale ts = TweenScale.Begin(talkObj, 0.3f, Vector3.one);
		ts.ignoreTimeScale = false;
		ts.delay = 0f;
		EventDelegate.Set(ts.onFinished, TalkEnd);
	}

	private void TalkEnd()
	{
		TweenScale ts = TweenScale.Begin(talkObj, 0.3f, Vector3.zero);
		ts.ignoreTimeScale = false;
		ts.delay = 1f;
		EventDelegate.Set(ts.onFinished, TalkClose);
	}
	private void TalkClose()
	{
		NGUITools.SetActive(talkObj, false);
		if(TalkCallback != null)
			TalkCallback();
	}

	public void RunCD(float sec, Action callback)
	{
		CDCallback = callback;
		actionStop = true;

		cdBar.value = 0f;
		CDMaxTime = sec;
		CDProgress = 0f;
		inCD = true;
	}

	private void CDEnd()
	{
		cdBar.value = 0f;
		inCD = false;
		actionStop = false;
		enemyData.TalkID = 0;

		if(enemyData.GetBuff((int)Buff.BUFF_TYPE.STUN) == null)	//Stun detect.
		{
			enemyData.CDFinished = true;
			FightManager.Instance.RunEnemyAI(enemyData);
		}
		else
		{
			enemyData.inCDCommand = null;
			enemyData.CDFinished = false;
		}

		//if(CDCallback != null)
		//	CDCallback();
	}
}
