﻿using UnityEngine;
using System.Collections;

public class BuffIcon : MonoBehaviour {

	public UISprite buffSprite;
	public UISprite elementSprite;
	public UISprite shieldSprite;
	public UISprite absorbSprite;
	public UISprite itemSprite1;
	public UISprite itemSprite2;
	public UISprite arrowSprite;
	public UILabel itemLv2;

	public int BuffID;

	public void Init(Buff b)
	{
		BuffID = b.BuffID;

		switch(b.BuffID)
		{
			case 2:
			{
				//element guard
				NGUITools.SetActive(elementSprite.gameObject, true);
				NGUITools.SetActive(shieldSprite.gameObject, true);
				NGUITools.SetActive(buffSprite.gameObject, false);
				NGUITools.SetActive(absorbSprite.gameObject, false);
				NGUITools.SetActive(itemSprite1.gameObject, false);
				NGUITools.SetActive(itemSprite2.gameObject, false);
				NGUITools.SetActive(arrowSprite.gameObject, false);
				NGUITools.SetActive(itemLv2.gameObject, false);

				elementSprite.spriteName = "attribute_" + b.YValue.ToString("000");
			}
			break;

			case 4:
			{
				//element absorb
				NGUITools.SetActive(elementSprite.gameObject, true);
				NGUITools.SetActive(shieldSprite.gameObject, false);
				NGUITools.SetActive(buffSprite.gameObject, false);
				NGUITools.SetActive(absorbSprite.gameObject, true);
				NGUITools.SetActive(itemSprite1.gameObject, false);
				NGUITools.SetActive(itemSprite2.gameObject, false);
				NGUITools.SetActive(arrowSprite.gameObject, false);
				NGUITools.SetActive(itemLv2.gameObject, false);

				elementSprite.spriteName = "attribute_" + b.XValue.ToString("000");
			}
			break;

			case 201:
			{
				//board drop
				NGUITools.SetActive(elementSprite.gameObject, false);
				NGUITools.SetActive(shieldSprite.gameObject, false);
				NGUITools.SetActive(buffSprite.gameObject, false);
				NGUITools.SetActive(absorbSprite.gameObject, false);
				NGUITools.SetActive(itemSprite1.gameObject, true);
				NGUITools.SetActive(itemSprite2.gameObject, true);
				NGUITools.SetActive(arrowSprite.gameObject, true);
				NGUITools.SetActive(itemLv2.gameObject, false);

				itemSprite1.spriteName = "battle_board_" + b.XValue.ToString("000");
				itemSprite2.spriteName = "battle_board_" + b.YValue.ToString("000");
			}
			break;

			case 202:
			{
				//board drop
				NGUITools.SetActive(elementSprite.gameObject, false);
				NGUITools.SetActive(shieldSprite.gameObject, false);
				NGUITools.SetActive(buffSprite.gameObject, false);
				NGUITools.SetActive(absorbSprite.gameObject, false);
				NGUITools.SetActive(itemSprite1.gameObject, true);
				NGUITools.SetActive(itemSprite2.gameObject, false);
				NGUITools.SetActive(arrowSprite.gameObject, true);
				NGUITools.SetActive(itemLv2.gameObject, true);
				
				itemSprite1.spriteName = "battle_board_" + b.XValue.ToString("000");
				itemLv2.text = (b.YValue + 1).ToString("0");
			}
			break;

			default:
			{
				NGUITools.SetActive(elementSprite.gameObject, false);
				NGUITools.SetActive(shieldSprite.gameObject, false);
				NGUITools.SetActive(buffSprite.gameObject, true);
				NGUITools.SetActive(absorbSprite.gameObject, false);
				NGUITools.SetActive(itemSprite1.gameObject, false);
				NGUITools.SetActive(itemSprite2.gameObject, false);
				NGUITools.SetActive(arrowSprite.gameObject, false);
				NGUITools.SetActive(itemLv2.gameObject, false);

				buffSprite.spriteName = "icon_status_" + b.BuffID.ToString();
			}
			break;
		}

		b.NeedRefresh = false;
	}
}
