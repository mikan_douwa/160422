﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class ActItem : MonoBehaviour {

	public const int TOP_DEPTH = 19;
	public const int BASE_DEPTH = 3;
	public const int MAX_LV = 5;

	public enum ACT_TYPE
	{
		Attack = 1,
		Heal = 2,
		Skill = 4,
		PowerUp = 8,
		TimeBoost = 16,
		Useless = 32,
		Poison = 64,
		Rand = 128,
	}
	//Reference
	public UISprite sprite;
	public UISprite fakeSprite;
	public UISprite lvTitle;
	public UILabel lvLabel;
	public UISprite max;
	public GameObject fxPanel;
	public UITexture shineTex;
	public UISprite lockSprite;
	public UISprite chain1;
	public UISprite chain2;
	public UISprite bindSprite;
	//Data
	public int ID;
	public ACT_TYPE Type;
	public int Lv;
	public bool isActive;
	public StockItem stock;
	public bool isLock = false;
	public bool isBind = false;

	private Transform trans = null;
	public Transform Trans{
		get{
			if(trans == null)
				trans = transform;
			return trans;
		}
	}

	private List<UITweener> tweenerList = new List<UITweener>();

	public void Init(StockItem item)
	{
		stock = item;
		Type = stock.Type;
		isLock = false;
		isBind = false;
		Lv = 1;

		//Buff 201 effect
		Buff b = FightManager.Instance.GetGroupBuff(BaseUnit.SIDE.ALLY, (int)Buff.BUFF_TYPE.BOARD_DROP);
		if(b != null && b.XValue == (int)stock.Type && Mikan.MathUtils.Random.Next(0, 100) < b.ZValue)
		{
			Type = (ACT_TYPE)b.YValue;
		}
		//Buff 202 effect
		Buff b2 = FightManager.Instance.GetGroupBuff(BaseUnit.SIDE.ALLY, (int)Buff.BUFF_TYPE.BOARD_UP);
		if(b2 != null && b2.XValue == (int)stock.Type && Mikan.MathUtils.Random.Next(0, 100) < b2.ZValue)
		{
			Lv += (int)b2.YValue;
		}


		Refresh();
		isActive = true;

		//Cancel Animation objects.
		for(int i = 0; i < tweenerList.Count; i++)
		{
			tweenerList[i].enabled = false;
		}
		tweenerList.Clear();
		//Restore Transform
		sprite.transform.localRotation = Quaternion.Euler(Vector3.zero);
		lvLabel.transform.localScale = Vector3.one;
		max.transform.localScale = Vector3.one;

		NGUITools.SetActive(sprite.gameObject, true);
		NGUITools.SetActive(fakeSprite.gameObject, false);
		NGUITools.SetActive(lockSprite.gameObject, false);
		NGUITools.SetActive(chain1.gameObject, false);
		NGUITools.SetActive(chain2.gameObject, false);

		ClearFX();
	}

	public void Refresh()
	{
		sprite.spriteName = "battle_board_" + ((int)Type).ToString("000");
		bool isMax = (Lv == MAX_LV);
		if(!isMax)
			lvLabel.text = Lv.ToString();

		NGUITools.SetActive(lvTitle.gameObject, !isMax);
		NGUITools.SetActive(lvLabel.gameObject, !isMax);
		NGUITools.SetActive(max.gameObject, isMax);
		NGUITools.SetActive(shineTex.gameObject, isMax);
		NGUITools.SetActive(lockSprite.gameObject, isLock);
		NGUITools.SetActive(chain1.gameObject, isLock);
		NGUITools.SetActive(chain2.gameObject, isLock);
		NGUITools.SetActive(bindSprite.gameObject, isBind);
	}

	public void Swap(ActItem item)
	{
		ACT_TYPE tempType = item.Type;
		int tempLv = item.Lv;
		int tempDepth = item.shineTex.depth;
		StockItem tempStock = item.stock;
		bool tempIsBind = item.isBind;

		item.Type = Type;
		item.Lv = Lv;
		item.isBind = isBind;
		item.Refresh();
		item.shineTex.depth = shineTex.depth;
		item.sprite.depth = shineTex.depth + 1;
		item.lvTitle.depth = shineTex.depth + 2;
		item.lvLabel.depth = shineTex.depth + 2;
		item.max.depth = shineTex.depth + 2;
		item.bindSprite.depth = shineTex.depth + 6;
		item.stock = stock;

		Type = tempType;
		Lv = tempLv;
		isBind = tempIsBind;

		//recover size
		//max.transform.localScale = Vector3.one;
		//lvLabel.transform.localScale = Vector3.one;

		Refresh();
		shineTex.depth = tempDepth;
		sprite.depth = tempDepth + 1;
		lvTitle.depth = tempDepth + 2;
		lvLabel.depth = tempDepth + 2;
		max.depth = tempDepth + 2;
		bindSprite.depth = tempDepth + 6;
		stock = tempStock;
	}

	public void Copy(ActItem item)
	{
		Type = item.Type;
		Lv = item.Lv;
		isLock = item.isLock;
		isBind = item.isBind;
		Refresh();
		isActive = item.isActive;
		stock = item.stock;

		ClearFX();
	}

	public void RandomOut()
	{
		int randBase = Mikan.MathUtils.Random.Next(0, 6);
		int type = (int)Mathf.Pow(2, randBase);
		Type = (ACT_TYPE)type;
		Refresh();
	}

	public void ChangeType(ACT_TYPE type)
	{
		fakeSprite.spriteName = "battle_board_" + ((int)Type).ToString("000");
		if(!isBind)
		{
			Type = type;
		}
		//Refresh();
		ChangeAnimation();
	}

	public void LevelUp(int lv)
	{
		Lv += lv;
		if(Lv > MAX_LV)
			Lv = MAX_LV;
		Refresh();
	}

	public void LevelUpBySkill(int lv)
	{
		bool isMax = (Lv == MAX_LV);

		Lv += lv;
		if(Lv > MAX_LV)
			Lv = MAX_LV;
		if(Lv < 1)
			Lv = 1;

		if(lv >= 0)
			LvUpAnimation(isMax);
		else
			LvDownAnimation(isMax);
	}

	public void LockItem()
	{
		isLock = true;
		isBind = false;
		LockAnimation();
		NGUITools.SetActive(bindSprite.gameObject, false);
	}

	public void BindItem()
	{
		isBind = true;
		isLock = false;
		NGUITools.SetActive(lockSprite.gameObject, isLock);
		NGUITools.SetActive(chain1.gameObject, isLock);
		NGUITools.SetActive(chain2.gameObject, isLock);
		BindAnimation();
	}

	public void SetToTop()
	{
		shineTex.depth = TOP_DEPTH;
		sprite.depth = TOP_DEPTH + 1;
		lvTitle.depth = TOP_DEPTH + 2;
		lvLabel.depth = TOP_DEPTH + 2;
		max.depth = TOP_DEPTH + 2;
		chain2.depth = TOP_DEPTH + 3;
		chain1.depth = TOP_DEPTH + 4;
		lockSprite.depth = TOP_DEPTH + 5;
		bindSprite.depth = TOP_DEPTH + 6;
	}

	public void ResetDepth()
	{
		shineTex.depth = BASE_DEPTH;
		sprite.depth = BASE_DEPTH + 1;
		lvTitle.depth = BASE_DEPTH + 2;
		lvLabel.depth = BASE_DEPTH + 2;
		max.depth = BASE_DEPTH + 2;
		chain2.depth = BASE_DEPTH + 3;
		chain1.depth = BASE_DEPTH + 4;
		lockSprite.depth = BASE_DEPTH + 5;
		bindSprite.depth = BASE_DEPTH + 6;
	}

	public void ClearFX()
	{
		FXController[] fxArray = GetComponentsInChildren<FXController>(true);
		for(int i = 0; i < fxArray.Length; i++)
		{
			fxArray[i].SelfRecycle();
		}
	}

	public ActItemCommand GetCommand()
	{
		ActItemCommand cmd = new ActItemCommand();
		cmd.ID = ID;
		cmd.Type = Type;
		cmd.Lv = Lv;
		cmd.stock = stock;
		cmd.isActive = isActive;
		cmd.isLock = isLock;
		cmd.isBind = isBind;
		return cmd;
	}

#region Animation Functions.

	private void ChangeAnimation()
	{
		fakeSprite.transform.localRotation = Quaternion.Euler(0, 0, 0);
		NGUITools.SetActive(fakeSprite.gameObject, true);

		TweenClockY tc = TweenClockY.Begin(fakeSprite.gameObject, 0.15f, 90f);
		tc.ignoreTimeScale = false;
		EventDelegate.Set(tc.onFinished, ChangeStep2);
		if(!tweenerList.Contains(tc))
			tweenerList.Add(tc);

		NGUITools.SetActive(sprite.gameObject, false);
	}
	private void ChangeStep2()
	{
		NGUITools.SetActive(fakeSprite.gameObject, false);

		Refresh();
		sprite.transform.localRotation = Quaternion.Euler(0, 90, 0);
		NGUITools.SetActive(sprite.gameObject, true);

		TweenClockY tc = TweenClockY.Begin(sprite.gameObject, 0.15f, 0f);
		tc.ignoreTimeScale = false;
		EventDelegate.Set(tc.onFinished, ChangeEnd);
		if(!tweenerList.Contains(tc))
			tweenerList.Add(tc);

		//FX
		FightManager.Instance.PlayFX("ChangeFX", Vector3.zero, fxPanel);
	}
	private void ChangeEnd()
	{
	}

	private void LvUpAnimation(bool isMax)
	{
		if(!isMax)
		{
			lvLabel.transform.localScale = Vector3.one;
			TweenScale ts = TweenScale.Begin(lvLabel.gameObject, 0.15f, new Vector3(1.5f, 1.5f, 1.5f));
			ts.ignoreTimeScale = false;
			EventDelegate.Set(ts.onFinished, LvUpStep2);
			if(!tweenerList.Contains(ts))
				tweenerList.Add(ts);
		}
		else
			max.transform.localScale = Vector3.one;

		//FX
		FightManager.Instance.PlayFX("UpgradeFX", Vector3.zero, fxPanel);
	}
	private void LvUpStep2()
	{
		Refresh();

		if(Lv < MAX_LV)
		{
			TweenScale ts = TweenScale.Begin(lvLabel.gameObject, 0.15f, Vector3.one);
			ts.ignoreTimeScale = false;
			if(!tweenerList.Contains(ts))
				tweenerList.Add(ts);
		}
		else
		{
			lvLabel.transform.localScale = Vector3.one;

			max.transform.localScale = new Vector3(1.5f, 1.5f, 1.5f);
			TweenScale ts = TweenScale.Begin(max.gameObject, 0.15f, Vector3.one);
			ts.ignoreTimeScale = false;
			if(!tweenerList.Contains(ts))
				tweenerList.Add(ts);
		}
	}

	private void LvDownAnimation(bool isMax)
	{
		if(isMax)
		{
			max.transform.localScale = Vector3.one;
			TweenScale ts = TweenScale.Begin(max.gameObject, 0.15f, new Vector3(0.5f, 0.5f, 0.5f));
			ts.ignoreTimeScale = false;
			EventDelegate.Set(ts.onFinished, LvDownStep2);
			if(!tweenerList.Contains(ts))
				tweenerList.Add(ts);
		}
		else
		{
			lvLabel.transform.localScale = Vector3.one;
			TweenScale ts = TweenScale.Begin(lvLabel.gameObject, 0.15f, new Vector3(0.5f, 0.5f, 0.5f));
			ts.ignoreTimeScale = false;
			EventDelegate.Set(ts.onFinished, LvDownStep2);
			if(!tweenerList.Contains(ts))
				tweenerList.Add(ts);
		}
		
		//FX
		FightManager.Instance.PlayFX("DowngradeFX", Vector3.zero, fxPanel);
	}
	private void LvDownStep2()
	{
		Refresh();

		max.transform.localScale = Vector3.one;
		TweenScale ts = TweenScale.Begin(lvLabel.gameObject, 0.15f, Vector3.one);
		ts.ignoreTimeScale = false;
		if(!tweenerList.Contains(ts))
			tweenerList.Add(ts);
	}


	void LockAnimation()
	{
		NGUITools.SetActive(chain2.gameObject, false);
		NGUITools.SetActive(lockSprite.gameObject, false);
		float halfWidth = (float)sprite.width / 2f;
		float halfHeight = (float)sprite.height / 2f;
		chain1.transform.localPosition = new Vector3(halfWidth, halfHeight, 0f);
		chain2.transform.localPosition = new Vector3(-halfWidth, halfHeight, 0f);
		lockSprite.transform.localScale = new Vector3(1.5f, 1.5f, 1.5f);
		//Chain1.
		NGUITools.SetActive(chain1.gameObject, true);
		TweenPosition tp = TweenPosition.Begin(chain1.gameObject, 0.1f, Vector3.zero);
		tp.ignoreTimeScale = false;
		EventDelegate.Add(tp.onFinished, LockStep2, true);
	}
	void LockStep2()
	{
		//Chain2.
		NGUITools.SetActive(chain2.gameObject, true);
		TweenPosition tp2 = TweenPosition.Begin(chain2.gameObject, 0.1f, Vector3.zero);
		tp2.ignoreTimeScale = false;
		EventDelegate.Add(tp2.onFinished, LockStep3, true);
	}
	void LockStep3()
	{
		//Lock
		NGUITools.SetActive(lockSprite.gameObject, true);
		TweenScale ts = TweenScale.Begin(lockSprite.gameObject, 0.3f, Vector3.one);
		ts.ignoreTimeScale = false;
		ts.method = UITweener.Method.BounceIn;
	}

	void BindAnimation()
	{
		bindSprite.transform.localScale = new Vector3(0f, 1f, 1f);
		NGUITools.SetActive(bindSprite.gameObject, true);
		TweenScale ts = TweenScale.Begin(bindSprite.gameObject, 0.1f, Vector3.one);
		ts.ignoreTimeScale = false;
	}

#endregion
	
}

public class ActItemCommand
{
	public int ID;
	public ActItem.ACT_TYPE Type;
	public int Lv;
	public bool isActive;
	public StockItem stock;
	public bool isLock = false;
	public bool isBind = false;
}
