﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;


public class StockItem
{
	public ActItem.ACT_TYPE Type;
	//public bool isAvailable;
	public bool Drop;

	public StockItem(ActItem.ACT_TYPE type, bool drop)
	{
		Type = type;
		//isAvailable = true;
		Drop = drop;
	}
}

public class DashBoard : MonoBehaviour {
	
	public const int ITEM_ROW_COUNT = 3;
	public const int ITEM_COL_COUNT = 5;
	public const float ITEM_HEIGHT = 100f;
	public const float ITEM_WIDTH = 100f;

	public const float SLOT_HEIGHT = 120f;
	public const float SLOT_WIDTH = 120f;
	public const float ITEM_START_X = -240f;
	public const float ITEM_START_Y = 95f;

	public const float ITEM_DRAG_MIN_DISTANCE = 20f;

	public float ITEM_HEIGHT_HALF;
	public float ITEM_WIDTH_HALF;
	public float SLOT_HEIGHT_HALF;
	public float SLOT_WIDTH_HALF;
	public float UPPER_BOUND;
	public float LOWER_BOUND;
	public float RIGHT_BOUND;
	public float LEFT_BOUND;

	//Reference
	public GameObject itemPrefab;
	public GameObject playPrefab;
	public GameObject fxPanel;
	public GameObject fxObject;
	public UISprite itemForPlay;
	public GameObject[] lightArray;
	public UISprite cancelTip;
	//Data
	private Vector3 dashBoardPos;
	public ActItem ActiveItem;
	//public bool EnableBoard = false;

	private List<Vector2> ItemPositionList;
	private List<ActItem> ItemList;
	private List<ActItem> DragList;
	private List<StockItem> StockList;
	private List<bool> LightList;
	private Queue<PlayActItem> playQueue;
	private bool isRelease = true;
	private Vector2 prevPos;


	public void Init(int stockId) {

		dashBoardPos = transform.localPosition;
		ItemList = new List<ActItem>();
		ItemPositionList = new List<Vector2>();
		DragList = new List<ActItem>();
		LightList = new List<bool>();
		playQueue = new Queue<PlayActItem>();

		for(int i = 0; i < ITEM_COL_COUNT; i++)
		{
			LightList.Add(false);
		}

		//Cache 1 play item
		PlayActItem play = NGUITools.AddChild(gameObject, playPrefab).GetComponent<PlayActItem>();
		play.dashBoard = this;
		NGUITools.SetActive(play.gameObject, false);
		playQueue.Enqueue(play);

		//Stock Data
		StockList = new List<StockItem>();
		foreach(Mikan.CSAGA.ConstData.BoardSetup data in ConstData.Tables.BoardSetup.Select(o=>{ return o.n_GROUP == stockId; }))
		{
			for(int i = 0; i < data.n_COUNT; i++)
			{
				StockList.Add(new StockItem((ActItem.ACT_TYPE)data.n_TYPE, (data.n_DROP == 1)));
			}
		}

		ITEM_HEIGHT_HALF = ITEM_HEIGHT / 2f;
		ITEM_WIDTH_HALF = ITEM_WIDTH / 2f;
		SLOT_HEIGHT_HALF = SLOT_HEIGHT / 2f;
		SLOT_WIDTH_HALF = SLOT_WIDTH / 2f;
		UPPER_BOUND = ITEM_START_Y + SLOT_HEIGHT_HALF;
		LOWER_BOUND = ITEM_START_Y - (SLOT_HEIGHT * ITEM_ROW_COUNT) + SLOT_HEIGHT_HALF + 0.1f;
		LEFT_BOUND = ITEM_START_X - SLOT_WIDTH_HALF;
		RIGHT_BOUND = ITEM_START_X + (SLOT_WIDTH * ITEM_COL_COUNT) - SLOT_WIDTH_HALF - 0.1f;

		int count = ITEM_ROW_COUNT * ITEM_COL_COUNT;
		for(int i = 0; i < count; i++)
		{
			ActItem item = NGUITools.AddChild(gameObject, itemPrefab).GetComponent<ActItem>();
			float x = ITEM_START_X + (i % ITEM_COL_COUNT) * SLOT_WIDTH;
			float y = ITEM_START_Y - (int)(i / ITEM_COL_COUNT) * SLOT_HEIGHT;
			item.transform.localPosition = new Vector3(x, y, 0f);
			item.ID = i;
			item.Init(PopStock());

			ItemList.Add(item);
			ItemPositionList.Add(new Vector2(x, y));
		}

		//EnableBoard = true;
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}


	public void ItemDragStart(Vector2 pos)
	{

		//CloseAllLight();

		//Clear fx object's tweener.
		UITweener[] tweeners = fxObject.GetComponents<UITweener>();
		for(int i = 0; i < tweeners.Length; i++)
		{
			tweeners[i].enabled = false;
		}

		ActiveItem = GetItemByPos(RestrictPosInRegion(pos));
		if(ActiveItem != null)
		{
			ActiveItem.SetToTop();
			isRelease = false;
		}

		//update leaderskill trigger status
		FightManager.Instance.playManager.UpdateLeaderSkillIcon(LSkillIcon.TYPE.TOTAL_LV);
	}

	public void ItemDragHandler(Vector2 pos)
	{
		float length = (float)FightUtility.LineLength(pos.x, pos.y, prevPos.x, prevPos.y);
		if(length > ITEM_DRAG_MIN_DISTANCE)
		{
			//Simulate draging point on path, avoid jump too far.
			int count = (int)(length / ITEM_DRAG_MIN_DISTANCE);
			for(int i = 1; i <= count; i++)
			{
				Vector2 point = FightUtility.LineStretchPoint(prevPos, pos, ITEM_DRAG_MIN_DISTANCE * (float)i);
				ItemDraging(point);
			}
			ItemDraging(pos);
		}
		else
		{
			ItemDraging(pos);
		}
		prevPos = pos;
	}

	public void ItemDraging(Vector2 pos)
	{
		//Debug.Log("Drag x:" + pos.x + ", y:" + pos.y);
		if(isRelease) return;

		if(ActiveItem != null)
		{
			//Drag up to char
			if(pos.y > UPPER_BOUND)
			{
				AutoReleaseClick();
				return;
			}

			Vector2 inPos = RestrictPosInRegion(pos);

			//Drag too far
			/*
			Vector2 startPos = GetItemPos(ActiveItem.ID);
			if(Mathf.Abs(startPos.x - inPos.x) > SLOT_WIDTH * 1.4f
			   || Mathf.Abs(startPos.y - inPos.y) > SLOT_HEIGHT * 1.4f)
			{
				ItemRelease();
				return;
			}
			*/

			//Touch other Item
			ActItem touchItem = GetItemByPos(inPos);
			if(touchItem != null)
			{
				if(!touchItem.isActive)
				{
					ItemRelease();
					return;
				}
				else 
				{
					if(touchItem != ActiveItem)
					{
						if(touchItem.isLock || ActiveItem.isLock)
						{
							ItemRelease();
							return;
						}

						ActiveItem.Swap(touchItem);
						//Different Item, Swap
						if(touchItem.Type != ActiveItem.Type || touchItem.isBind || ActiveItem.isBind)
						{
							SwapPosition(ActiveItem.transform, touchItem.transform);
							TweenPosition tp = TweenPosition.Begin(ActiveItem.gameObject, 0.1f, ItemPositionList[ActiveItem.ID]);
							tp.ignoreTimeScale = false;
							ActiveItem = touchItem;
							//Sound
							Message.Send(MessageID.PlaySound, SoundID.BATTLE_ITEM_SWAP);

							//ItemRelease();
							//return;
						}
						else
						{
							//Same Item, Lv+1.
							ActiveItem.isActive = false;
							RecycleStock(ActiveItem.stock);
							NGUITools.SetActive(ActiveItem.gameObject, false);
							touchItem.LevelUp(ActiveItem.Lv);
							ActiveItem = touchItem;
							//FX
							Vector2 itemPos = ItemPositionList[ActiveItem.ID];
							FightManager.Instance.PlayFX("UpgradeFX", new Vector3(itemPos.x, itemPos.y, 0f) + dashBoardPos);
							//Sound
							Message.Send(MessageID.PlaySound, new Sound(){ ID = SoundID.BATTLE_ITEM_UPGRADE, Pitch = 1f + 0.1f * (float)ActiveItem.Lv } );

							//update leaderskill trigger status
							FightManager.Instance.playManager.UpdateLeaderSkillIcon(LSkillIcon.TYPE.TOTAL_LV);
		
						}
					}
				}
			}
			ActiveItem.Trans.localPosition = inPos;
			fxObject.transform.localPosition = inPos;
		}
	}

	public void ItemRelease(bool showTip = true)
	{
		isRelease = true;
		if(ActiveItem != null)
		{
			if(showTip)
				ShowCancelTip(ActiveItem.transform.localPosition);

			TweenPosition tp = TweenPosition.Begin(ActiveItem.gameObject, 0.1f, ItemPositionList[ActiveItem.ID]);
			tp.ignoreTimeScale = false;
			TweenPosition tp2 = TweenPosition.Begin(fxObject, 0.1f, ItemPositionList[ActiveItem.ID]);
			tp2.ignoreTimeScale = false;

			Refill();
			ActiveItem.ResetDepth();
		}
		ActiveItem = null;

		//update leaderskill trigger status
		FightManager.Instance.playManager.UpdateLeaderSkillIcon(LSkillIcon.TYPE.TOTAL_LV);
	}

	public void AutoReleaseClick()
	{
		isRelease = true;
		if(ActiveItem != null)
		{
			ActiveItem.transform.localPosition = ItemPositionList[ActiveItem.ID];
			fxObject.transform.localPosition = ItemPositionList[ActiveItem.ID];
			ActiveItem.ResetDepth();

			if(ActiveItem.ID < ITEM_COL_COUNT && !FightManager.Instance.FightEnd)// && EnableBoard)
			{
				bool consumeAction = true;
				if(ActiveItem.Type == ActItem.ACT_TYPE.PowerUp || ActiveItem.Type == ActItem.ACT_TYPE.TimeBoost)
					consumeAction = false;
				
				if(FightManager.Instance.playManager.ActionAvailable(ActiveItem, consumeAction))
				{
					ActItem.ACT_TYPE triggerType = ActiveItem.Type;
					//Random Item.
					if(ActiveItem.Type == ActItem.ACT_TYPE.Rand)
						ActiveItem.RandomOut();
					
					//Time Boost sound.
					if(ActiveItem.Type == ActItem.ACT_TYPE.TimeBoost)
						Message.Send(MessageID.PlaySound, SoundID.BATTLE_BUFF);
					
					//Animation
					ClickAnimation(ActiveItem);
					//Fire
					ActiveItem.isActive = false;
					RecycleStock(ActiveItem.stock);
					FightManager.Instance.playManager.Action(ActiveItem.GetCommand(), consumeAction, triggerType);
				}
			}
			Refill();
		}
		ActiveItem = null;

		//update leaderskill trigger status
		FightManager.Instance.playManager.UpdateLeaderSkillIcon(LSkillIcon.TYPE.TOTAL_LV);

	}

	//For Auto Fight.
	public bool ItemClickByChar(int index)
	{
		return ItemClick(ItemList[index - 1]);
	}

	public bool ItemClick(ActItem item)
	{
		if(item == null) return false;// || !EnableBoard) return false;

		item.ResetDepth();

		bool trigger = false;
		if(item.ID < ITEM_COL_COUNT && !FightManager.Instance.FightEnd)
		{
			bool consumeAction = true;
			if(item.Type == ActItem.ACT_TYPE.PowerUp || item.Type == ActItem.ACT_TYPE.TimeBoost)
				consumeAction = false;

			if(FightManager.Instance.playManager.ActionAvailable(item, consumeAction))
			{
				trigger = true;
				ActItem.ACT_TYPE triggerType = item.Type;
				//Random Item.
				if(item.Type == ActItem.ACT_TYPE.Rand)
					item.RandomOut();

				//Time Boost sound.
				if(item.Type == ActItem.ACT_TYPE.TimeBoost)
					Message.Send(MessageID.PlaySound, SoundID.BATTLE_BUFF);

				//Animation
				ClickAnimation(item);
				//Fire
				item.isActive = false;
				RecycleStock(item.stock);
				FightManager.Instance.playManager.Action(item.GetCommand(), consumeAction, triggerType);
				Refill();
			}
		}

		if(!trigger)
		{
			//Recover
			TweenPosition tp = TweenPosition.Begin(item.gameObject, 0.1f, ItemPositionList[item.ID]);
			tp.ignoreTimeScale = false;
		}
		ActiveItem = null;
		isRelease = true;

		//update leaderskill trigger status
		FightManager.Instance.playManager.UpdateLeaderSkillIcon(LSkillIcon.TYPE.TOTAL_LV);

		return trigger;
	}

	public Vector2 GetItemPos(int id)
	{
		return ItemPositionList[id];
	}

	public void Refill()
	{
		int count = ItemList.Count;
		for(int i = 0; i < count; i++)
		{
			ActItem item = ItemList[i];
			if(!item.isActive)
			{
				ActItem fillItem = null;
				int n = i + ITEM_COL_COUNT;
				while(n < count)
				{
					if(ItemList[n].isActive)
					{
						fillItem = ItemList[n];
						break;
					}
					n += ITEM_COL_COUNT;
				}

				Vector3 pos = ItemPositionList[item.ID];
				float y = 0f;
				if(fillItem == null)
				{
					//Fill from OutSide.
					y = -300f - (int)((float)i / (float)ITEM_COL_COUNT) * SLOT_HEIGHT;
					item.Init(PopStock());
				}
				else
				{
					//Fill from Under Item.
					y = ItemPositionList[fillItem.ID].y;
					item.Copy(fillItem);
					fillItem.isActive = false;
				}
				item.Trans.localPosition = new Vector3(pos.x, y, 0f);
				NGUITools.SetActive(item.gameObject, true);
				TweenY ty = TweenY.Begin(item.gameObject, (pos.y - y) / 3000f, pos.y);
				ty.ignoreTimeScale = false;
			}
		}

	}

	public void ChangeBoard(int from, int to, int mode, int col = 0)
	{
		//Sound
		Message.Send(MessageID.PlaySound, SoundID.BATTLE_ITEM_CHANGE);

		ActItem.ACT_TYPE ToType = (ActItem.ACT_TYPE)to;

		List<ActItem> lst = ChooseItems(from, mode, col);
		for(int i = 0; i < lst.Count; i++)
		{
			ActItem item = lst[i];
			item.ChangeType(ToType);
		}
	
		//update leaderskill trigger status
		FightManager.Instance.playManager.UpdateLeaderSkillIcon(LSkillIcon.TYPE.TOTAL_LV);
	}

	public void AddLvByType(int typeId, int lv, int mode, int col = 0)
	{
		//Sound
		if(lv >= 0)
			Message.Send(MessageID.PlaySound, SoundID.BATTLE_ITEM_UPGRADE);
		else 
			Message.Send(MessageID.PlaySound, new Sound(){ ID = SoundID.BATTLE_ITEM_UPGRADE, Pitch = 0.3f } );

		List<ActItem> lst = ChooseItems(typeId, mode, col);
		for(int i = 0; i < lst.Count; i++)
		{
			ActItem item = lst[i];
			item.LevelUpBySkill(lv);
		}

		//update leaderskill trigger status
		FightManager.Instance.playManager.UpdateLeaderSkillIcon(LSkillIcon.TYPE.TOTAL_LV);

	}

	public void LockItemByType(int typeId, int mode, int col = 0)
	{
		List<ActItem> lst = ChooseItems(typeId, mode, col);
		for(int i = 0; i < lst.Count; i++)
		{
			ActItem item = lst[i];
			item.LockItem();
		}
	}

	public void BindItemByType(int typeId, int mode, int col = 0)
	{
		List<ActItem> lst = ChooseItems(typeId, mode, col);
		for(int i = 0; i < lst.Count; i++)
		{
			ActItem item = lst[i];
			item.BindItem();
		}
	}

	private List<ActItem> ChooseItems(int typeId, int mode, int col)
	{
		List<ActItem> lstFiltered = new List<ActItem>();
		List<ActItem> lstFinal = new List<ActItem>();
		//Filter type
		for(int i = 0; i < ItemList.Count; i++)
		{
			ActItem item = ItemList[i];
			if(item.isActive && (item != ActiveItem) && (typeId == 0 || ((int)item.Type & typeId) != 0))
				lstFiltered.Add(item);
		}

		//Choose item
		if(mode > 20 && mode < 30)
		{
			//Random. 
			int count = mode % 10;
			Mikan.MathUtils.Shuffle(lstFiltered);
			//Choose
			int candidate = Math.Min(count, lstFiltered.Count);
			for(int i = 0; i < candidate; i++)
			{
				lstFinal.Add(lstFiltered[i]);
			}
		}
		else if(mode > 100)
		{
			int x = (int)((float)mode / 10f) - 10;
			int y = mode % 10;
			for(int i = 0; i < lstFiltered.Count; i++)
			{
				ActItem item = lstFiltered[i];
				if(item.Lv >= x && item.Lv <= y)
					lstFinal.Add(item);
			}
		}
		else
		{
			for(int i = 0; i < lstFiltered.Count; i++)
			{
				ActItem item = lstFiltered[i];
				//Mode Condition.
				if(mode == 0 || (item.ID >= (mode - 1) * ITEM_COL_COUNT && item.ID < mode * ITEM_COL_COUNT)
				   || (mode == 11 && item.ID % ITEM_COL_COUNT == col - 1))
				{
					lstFinal.Add(item);
				}
			}
		}

		return lstFinal;
	}

	public void RefreshLight()
	{
		//if(!isRelease) return;

		List<int> lstAP = FightManager.Instance.playManager.GetCharActions();

		for(int i = 0; i < LightList.Count; i++)
		{
			bool isOpen = LightList[i];
			int ap = lstAP[i];
			if(isOpen && ap == 0)
			{
				NGUITools.SetActive(lightArray[i], false);
				LightList[i] = false;
			}
			else if(!isOpen && ap > 0)
			{
				NGUITools.SetActive(lightArray[i], true);
				LightList[i] = true;
			}
		}

	}
	public void CloseAllLight()
	{

		for(int i = 0; i < ITEM_COL_COUNT; i++)
		{
			NGUITools.SetActive(lightArray[i], false);
			LightList[i] = false;
		}

	}

#region Animation Functions.
	private void ClickAnimation(ActItem item)
	{
		PlayActItem play;
		if(playQueue.Count > 0)
			play = playQueue.Dequeue();
		else
		{
			play = NGUITools.AddChild(gameObject, playPrefab).GetComponent<PlayActItem>();
			play.dashBoard = this;
		}

		play.SetItem(item, ItemPositionList[item.ID]);
		NGUITools.SetActive(play.gameObject, true);
		play.Play();
	}
	public void RecyclePlayItem(PlayActItem play)
	{
		NGUITools.SetActive(play.gameObject, false);
		playQueue.Enqueue(play);
	}

	public void ShowCancelTip(Vector3 pos)
	{
		//Sound
		Message.Send(MessageID.PlaySound, SoundID.CANCEL);

		cancelTip.transform.localPosition = new Vector3(pos.x, pos.y + 35f, pos.z);
		cancelTip.transform.localScale = Vector3.zero;
		NGUITools.SetActive(cancelTip.gameObject, true);
		//animation
		TweenScale ts = TweenScale.Begin(cancelTip.gameObject, 0.3f, Vector3.one);
		ts.delay = 0f;
		ts.ignoreTimeScale = false;
		EventDelegate.Add(ts.onFinished, OutCancelTip, true);
	}
	public void OutCancelTip()
	{
		TweenScale ts = TweenScale.Begin(cancelTip.gameObject, 0.1f, Vector3.zero);
		ts.delay = 0.3f;
		ts.ignoreTimeScale = false;
		EventDelegate.Add(ts.onFinished, HideCancelTip, true);
	}
	public void HideCancelTip()
	{
		NGUITools.SetActive(cancelTip.gameObject, false);
	}

#endregion


#region Utility Functions.
	private Vector2 RestrictPosInRegion(Vector2 pos)
	{
		float x = pos.x;
		float y = pos.y;

		if(x > RIGHT_BOUND)
			x = RIGHT_BOUND;
		else if(x < LEFT_BOUND)
			x = LEFT_BOUND;

		if(y > UPPER_BOUND)
			y = UPPER_BOUND;
		else if(y < LOWER_BOUND)
			y = LOWER_BOUND;

		return new Vector2(x, y);
	}

	public ActItem GetItemByPos(Vector2 pos)
	{
		Vector2 touchPos = RestrictPosInRegion(pos);
		int index_x = (int)((touchPos.x - (ITEM_START_X - SLOT_WIDTH_HALF)) / SLOT_WIDTH);
		int index_y = -(int)((touchPos.y - (ITEM_START_Y + SLOT_HEIGHT_HALF)) / SLOT_HEIGHT);
		int index = index_x + index_y * ITEM_COL_COUNT;

		//Debug.Log("index:"+index+ ", Pos x:"+pos.x+", Pos y:"+pos.y);
		Vector2 itemPos = ItemPositionList[index];
		if(Mathf.Abs(itemPos.x - touchPos.x) < ITEM_WIDTH_HALF && Mathf.Abs(itemPos.y - touchPos.y) < ITEM_HEIGHT_HALF)
			return ItemList[index];
		else
			return null;
	}

	public void SwapPosition(Transform trans1, Transform trans2)
	{
		Vector3 pos = trans1.localPosition;
		trans1.localPosition = trans2.localPosition;
		trans2.localPosition = pos;
	}

	public StockItem PopStock()
	{
		StockItem stock = StockList[Mikan.MathUtils.Random.Next(0, StockList.Count)];
		StockList.Remove(stock);

		return stock;
	}
	public void RecycleStock(StockItem stock)
	{
		if(!stock.Drop)
			StockList.Add(stock);
	}

	public int GetItemCountByType(int type)
	{
		int count = 0;
		for(int i = 0; i < ItemList.Count; i++)
		{
			ActItem item = ItemList[i];
			if(item.isActive && (int)item.Type == type)
				count++;
		}

		return count;
	}

	public int GetTotalLvByItem(int type)
	{
		int lv = 0;
		for(int i = 0; i < ItemList.Count; i++)
		{
			ActItem item = ItemList[i];
			if(item.isActive && (int)item.Type == type && item != ActiveItem)
				lv += item.Lv;
		}
		
		return lv;

	}

	public bool HasItemOnColumn(int row, int type, int minLv, int maxLv)
	{
		for(int i = row - 1; i < ItemList.Count; i += ITEM_COL_COUNT)
		{
			ActItem item = ItemList[i];
			//Debug.Log("ActItem " + item.Type + "," + item.Lv);
			if(item != ActiveItem && item.isActive && (int)item.Type == type && item.Lv >= minLv && item.Lv <= maxLv)
				return true;
		}
		
		return false;

	}
#endregion
}
