﻿using UnityEngine;
using System.Collections;

public class StatusText : MonoBehaviour {

	public enum STATUS_TYPE
	{
		LV_UP,
		CD_DOWN,
		CD_UP,
		TEXT,
	}

	public UILabel label;

	public void Init(STATUS_TYPE type, float number, string text = "")
	{
		switch(type)
		{
			case STATUS_TYPE.LV_UP:
				label.text = "LV+" + number.ToString("##0");
				break;

			case STATUS_TYPE.CD_DOWN:
				label.text = "-" + number.ToString("##0.#") + "s";
				break;

			case STATUS_TYPE.CD_UP:
				label.text = "+" + number.ToString("##0.#") + "s";
				break;

			case STATUS_TYPE.TEXT:
				label.text = text;
				break;
		}
	}
}
