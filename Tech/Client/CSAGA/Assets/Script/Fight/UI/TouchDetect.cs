﻿using UnityEngine;
using System.Collections;

public class TouchDetect : MonoBehaviour {

	public const float CLICK_SECOND = 1f;
	public const float CLICK_HALF_WIDTH = 40f;
	public const float CLICK_HALF_HEIGHT = 40f;

	public DashBoard dashBoard;

	Vector2 m_F0StartPosition;
	Vector2 m_NowPosition;

	protected Transform mTrans;
	protected Transform mParent;
	protected UIRoot mRoot;
	protected int mTouchID = int.MinValue;

	protected float pressTime;
	protected bool inClick;
	protected Vector3 activeUnitPos;
	protected ActItem clickItem;

	void Start()
	{
		mTrans = transform;
		mParent = mTrans.parent;
		mRoot = NGUITools.FindInParents<UIRoot>(mParent);
	}
	/*
	void Update()
	{
	#if UNITY_EDITOR || UNITY_STANDALONE_OSX || UNITY_STANDALONE_WIN || UNITY_WEBPLAYER
		if (Input.GetMouseButtonDown (0)) {
			//TAP Event
			m_F0StartPosition = new Vector2((Input.mousePosition.x - Screen.width / 2f) * playManager.ScreenRate, 
			                                (Input.mousePosition.y - Screen.height / 2f) * playManager.ScreenRate);
			//playManager.SetCircleVisible(true);
			playManager.ConnectStart();
			playManager.CalculateCharTargeted(m_F0StartPosition);
		}
		if (Input.GetMouseButton (0)) {
			//Drag Move
			Vector2 pos = new Vector2((Input.mousePosition.x - Screen.width / 2f) * playManager.ScreenRate, 
			                          (Input.mousePosition.y - Screen.height / 2f) * playManager.ScreenRate);
			playManager.CalculateCharTargeted(pos);
		}
		if (Input.GetMouseButtonUp (0)) {
			//Drag End
			Vector2 pos = new Vector2((Input.mousePosition.x - Screen.width / 2f) * playManager.ScreenRate, 
			                          (Input.mousePosition.y - Screen.height / 2f) * playManager.ScreenRate);
			//playManager.SetCircleVisible(false);
			NewFightManager.Instance.PlayManager.Fire(pos);
		}
	#endif
	}
	*/
	void OnPress(bool isPress)
	{
		if(isPress)
		{
			pressTime = Time.time;
			mTouchID = UICamera.currentTouchID;

			Vector3 pos = transform.InverseTransformPoint(UICamera.lastHit.point);
			m_F0StartPosition = new Vector2(pos.x, pos.y);
			m_NowPosition = m_F0StartPosition;

			clickItem = dashBoard.GetItemByPos(m_NowPosition);
			inClick = false;
			if(clickItem != null)
			{
				activeUnitPos = dashBoard.GetItemPos(clickItem.ID);
				//click area detect.
				if(Mathf.Abs(pos.x - activeUnitPos.x) < CLICK_HALF_WIDTH && Mathf.Abs(pos.y - activeUnitPos.y) < CLICK_HALF_HEIGHT)
					inClick = true;
			}
			dashBoard.ItemDragStart(m_F0StartPosition);

		}
		else
		{
			//Release.
			if (!enabled || mTouchID != UICamera.currentTouchID) return;

			//click
			if(!inClick)
			{
				dashBoard.ItemRelease(false);
			}
			else
			{
				if((Mathf.Abs(m_NowPosition.x - activeUnitPos.x) < CLICK_HALF_WIDTH && Mathf.Abs(m_NowPosition.y - activeUnitPos.y) < CLICK_HALF_HEIGHT)
				   && Time.time < pressTime + CLICK_SECOND)
				{
					dashBoard.ItemClick(clickItem);
				}
				else
					dashBoard.ItemRelease();
			}
			clickItem = null;
		}
	}

	void OnDrag(Vector2 delta)
	{
		if(delta != Vector2.zero)
		{
			m_NowPosition += delta * mRoot.pixelSizeAdjustment;

			if(inClick && 
			   (Mathf.Abs(m_NowPosition.x - activeUnitPos.x) > CLICK_HALF_WIDTH || Mathf.Abs(m_NowPosition.y - activeUnitPos.y) > CLICK_HALF_HEIGHT)
			   )
			{
				inClick = false;
			}

			dashBoard.ItemDraging(m_NowPosition);
		}
	}
}
