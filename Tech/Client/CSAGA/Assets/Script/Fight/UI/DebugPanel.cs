﻿using UnityEngine;
using System.Collections;

public class DebugPanel : MonoBehaviour {

	public bool IsShow = false;

	public UILabel[] InfoArray;

	public void Show(bool isShow)
	{
		IsShow = isShow;
		NGUITools.SetActive(gameObject, isShow);
	}

	public void ShowCharInfo()
	{
		for(int i = 0; i < 5; i++)
		{
			FightUnit unit = FightManager.Instance.GetUnit(i + 1);
			if(unit != null)
			{
				UILabel label = InfoArray[i];
				label.text = "HP:" + unit.Hp + "\n"
						+ "ATK:" + unit.Atk + "\n"
						+ "REV:" + unit.Rev + "\n"
						+ "SPD:" + unit.Spd + "\n"
						+ "SPD x " + FightManager.Instance.playManager.GetSpeedRate(i + 1) + "\n"
						+ "UP LV:" + unit.BonusCount.ToString();
			}
		}
	}
}
