﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using Mikan.CSAGA.ConstData;

public class LSkillIcon : MonoBehaviour {

	public enum TYPE
	{
		TOTAL_LV = 8,
		COLUMN = 9,
		TIME = 10,
		COLLECT = 11,
		CHAIN = 14,
	}

	public UISprite ActItemSprite;
	public UISprite LvSprite;
	public UILabel CountLabel;
	public UISprite ArrowSprite;
	public UISprite LvSprite2;
	public UILabel CountLabel2;
	public UISlider Bar;
	public UISprite BarCover;
	public UISprite Queue;
	public UISprite[] ItemXY;
	public UILabel[] CountXY;
	public UISprite[] QueueItem;

	//Total lv
	public TYPE Type;
	//Column
	int ActItemId;
	int Min;
	int Max;
	//Time
	float FullTime;
	//Collect
	int CollectTypeX;
	int CollectTypeY;
	int CollectCountX;
	int CollectCountY;

	public void Init(Skill leaderSkill)
	{
		switch(leaderSkill.n_TRIGGER)
		{
			case (int)TYPE.TOTAL_LV:
			{
				Type = TYPE.TOTAL_LV;	

				NGUITools.SetActive(ActItemSprite.gameObject, true);
				NGUITools.SetActive(LvSprite.gameObject, true);
				NGUITools.SetActive(CountLabel.gameObject, true);
				NGUITools.SetActive(ArrowSprite.gameObject, false);
				NGUITools.SetActive(LvSprite2.gameObject, false);
				NGUITools.SetActive(CountLabel2.gameObject, false);
				NGUITools.SetActive(Bar.gameObject, false);
				NGUITools.SetActive(Queue.gameObject, false);

				ActItemId = leaderSkill.n_TRIGGER_Y;
				ActItemSprite.spriteName = "battle_board_" + ActItemId.ToString("000");
				CountLabel.text = "0";
				CountLabel.color = Color.white;
				Min = (int)((float)leaderSkill.n_TRIGGER_X / 100f);
				Max = leaderSkill.n_TRIGGER_X % 100;
				
			}
			break;

			case (int)TYPE.COLUMN:
			{
				Type = TYPE.COLUMN;	
				
				NGUITools.SetActive(ActItemSprite.gameObject, true);
				NGUITools.SetActive(LvSprite.gameObject, false);
				NGUITools.SetActive(CountLabel.gameObject, false);
				NGUITools.SetActive(ArrowSprite.gameObject, true);
				NGUITools.SetActive(LvSprite2.gameObject, true);
				NGUITools.SetActive(CountLabel2.gameObject, true);
				NGUITools.SetActive(Bar.gameObject, false);
				NGUITools.SetActive(BarCover.gameObject, false);
				NGUITools.SetActive(Queue.gameObject, false);
				
				ActItemId = leaderSkill.n_TRIGGER_Y;
				ActItemSprite.spriteName = "battle_board_" + ActItemId.ToString("000");
				Min = (int)((float)leaderSkill.n_TRIGGER_X / 10f);
				Max = leaderSkill.n_TRIGGER_X % 10;
				CountLabel2.text = Min.ToString() + "[00ff00]-[-]" + Max.ToString();
			}
			break;


			case (int)TYPE.TIME:
			{
				Type = TYPE.TIME;	
				
				NGUITools.SetActive(ActItemSprite.gameObject, false);
				NGUITools.SetActive(LvSprite.gameObject, false);
				NGUITools.SetActive(CountLabel.gameObject, false);
				NGUITools.SetActive(ArrowSprite.gameObject, false);
				NGUITools.SetActive(LvSprite2.gameObject, false);
				NGUITools.SetActive(CountLabel2.gameObject, false);
				NGUITools.SetActive(Bar.gameObject, true);
				NGUITools.SetActive(BarCover.gameObject, false);
				NGUITools.SetActive(Queue.gameObject, false);
				
				Bar.value = 0f;
				FullTime = (float)leaderSkill.n_TRIGGER_X / 10f;
			}
			break;

			case (int)TYPE.COLLECT:
			{
				Type = TYPE.COLLECT;	
				
				NGUITools.SetActive(ActItemSprite.gameObject, false);
				NGUITools.SetActive(LvSprite.gameObject, false);
				NGUITools.SetActive(CountLabel.gameObject, false);
				NGUITools.SetActive(ArrowSprite.gameObject, false);
				NGUITools.SetActive(LvSprite2.gameObject, false);
				NGUITools.SetActive(CountLabel2.gameObject, false);
				NGUITools.SetActive(Bar.gameObject, false);
				NGUITools.SetActive(Queue.gameObject, true);
				
				CollectTypeX = (int)((float)leaderSkill.n_TRIGGER_X / 10f);
				CollectTypeY = (int)((float)leaderSkill.n_TRIGGER_Y / 10f);
				CollectCountX = leaderSkill.n_TRIGGER_X % 10;
				CollectCountY = leaderSkill.n_TRIGGER_Y % 10;
				if(CollectTypeX > 0 && CollectCountX > 0)
				{
					ItemXY[0].spriteName = "battle_board_" + CollectTypeX.ToString("000");
					CountXY[0].text = CollectCountX.ToString();
				}
				else
				{
					NGUITools.SetActive(ItemXY[0].gameObject, false);
					NGUITools.SetActive(CountXY[0].gameObject, false);
				}
				if(CollectTypeY > 0 && CollectCountY > 0)
				{
					ItemXY[1].spriteName = "battle_board_" + CollectTypeY.ToString("000");
					CountXY[1].text = CollectCountY.ToString();
				}
				else
				{
					NGUITools.SetActive(ItemXY[1].gameObject, false);
					NGUITools.SetActive(CountXY[1].gameObject, false);
				}
				for(int i = 0; i < QueueItem.Length; i++)
				{
					NGUITools.SetActive(QueueItem[i].gameObject, false);
				}
			}
			break;

		}
	}

	public void UpdateInfo()
	{
		switch(Type)
		{
			case TYPE.TOTAL_LV:
			{
				int lv = FightManager.Instance.playManager.dashBoard.GetTotalLvByItem(ActItemId);
				CountLabel.text = lv.ToString();
				if(lv > Max)
					CountLabel.color = Color.red;
				else if(lv >= Min)
					CountLabel.color = Color.green;
				else
					CountLabel.color = Color.white;
			}
			break;

			case TYPE.TIME:
			{
				float rate = FightManager.Instance.TimePower / FullTime;
				if(rate > 1)
					rate = 1f;
				Bar.value = rate;
				NGUITools.SetActive(BarCover.gameObject, (rate == 1));
			}
			break;

			case TYPE.COLLECT:
			{
				List<int> CollectList = FightManager.Instance.LSCollectList;

				int x = 0;
				int y = 0;
				for(int i = 0; i < CollectList.Count; i++)
				{
					UISprite item = QueueItem[i];
					int type = CollectList[i];
					item.spriteName = "battle_board_" + type.ToString("000");
					NGUITools.SetActive(item.gameObject, true);

					if(type == CollectTypeX)
						x++;
					else if(type == CollectTypeY)
						y++;
				}

				UILabel xLabel = CountXY[0];
				if(x >= CollectCountX)
					xLabel.color = Color.green;
				else
					xLabel.color = Color.white;

				UILabel yLabel = CountXY[1];
				if(y >= CollectCountY)
					yLabel.color = Color.green;
				else
					yLabel.color = Color.white;

			}
			break;
		}
	}
}
