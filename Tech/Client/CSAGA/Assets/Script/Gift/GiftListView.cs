﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GiftListView : RewardListView {

	protected override void Initialize ()
	{
		GameSystem.Service.OnGiftClaim += OnGiftClaim;
		GameSystem.Service.OnGiftList += OnGiftList;
		
		Message.AddListener(MessageID.ClaimAllGift, OnClaimAllGift);

		base.Initialize ();
	}

	protected override void OnDestroy ()
	{
		if(GameSystem.IsAvailable) 
		{
			GameSystem.Service.OnGiftClaim -= OnGiftClaim;
			GameSystem.Service.OnGiftList -= OnGiftList;
		}

		Message.RemoveListener(MessageID.ClaimAllGift, OnClaimAllGift);

		base.OnDestroy ();
	}

	private void OnClaimAllGift()
	{
		Context.Instance.GiftClaimCount = Mathf.Min(GameSystem.Service.GiftInfo.List.Count, 50);
		GameSystem.Service.GiftBatchClaim(Context.Instance.GiftClaimCount);
	}
	
	protected override void Setup ()
	{
		GameSystem.Service.GiftList();
	}
	
	void OnGiftList ()
	{
		var list = new List<RewardData>(GameSystem.Service.GiftInfo.List.Count);
		
		foreach(var item in GameSystem.Service.GiftInfo.List)
		{
			var data = new RewardData{ Gift = item };
			list.Add(data);
			if(list.Count >= 50) break;
		}
		
		Setup(list);
	}
	
	void OnGiftClaim (System.Collections.Generic.List<Mikan.CSAGA.Reward> list)
	{
		if(list.Count > 1)
		{
			Context.Instance.Rewards = new Queue<Mikan.CSAGA.Reward>(list);
			MessageBox.Show("GameScene/Gift/GiftClaimResultWindow", "").title = ConstData.GetSystemText(SystemTextID._214_CLAIMED_GIFT_LIST);
		}
		else if(list.Count > 0)
		{
			QuestDrop.Show(list);
		}

		Coroutine.DelayInvoke(Rebuild, 0.2f);
	}
}
