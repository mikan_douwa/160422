﻿using UnityEngine;
using System.Collections;

public class GiftClaimResultWindow : MonoBehaviour {

	public UIWidget container;

	public UILabel label;
	public GameObject overflowTip;
	// Use this for initialization
	void Start () {

		if(Context.Instance.Rewards.Count == Context.Instance.GiftClaimCount)
			overflowTip.SetActive(false);

		var crystal = 0;
		var coin = 0;
		var gem = 0;
		var equipment = 0;
		var card = 0;
		var item = 0;

		while(Context.Instance.Rewards.Count > 0)
		{
			var reward = Context.Instance.Rewards.Dequeue();
			switch(reward.Type)
			{
			case Mikan.CSAGA.DropType.Crystal:
				crystal += reward.Value;
				break;
			case Mikan.CSAGA.DropType.Coin:
				coin += reward.Value;
				break;
			case Mikan.CSAGA.DropType.Gem:
				gem += reward.Value;
				break;
			case Mikan.CSAGA.DropType.Item:
			{
				if(ConstData.Tables.Item[reward.Value2].n_TYPE == Mikan.CSAGA.ItemType.Equipment)
					++equipment;
				else
					item += reward.Value;
				break;
			}
			case Mikan.CSAGA.DropType.Card:
				card += reward.Value;
				break;
			}
		}

		var text = "";

		if(crystal > 0)
			text = Append(text, SystemTextID.CRYSTAL, crystal);
		if(coin > 0)
			text = Append(text, SystemTextID.COIN, coin);
		if(gem > 0)
			text = Append(text, SystemTextID.GEM, gem);
		if(item > 0)
			text = Append(text, SystemTextID._215_ITEM, item);
		if(equipment > 0)
			text = Append(text, SystemTextID._216_EQUIPMENT, equipment);
		if(card > 0)
			text = Append(text, SystemTextID.BTN_CARD, card);

		label.text = text;

		container.height = label.height + 240;
	}

	private string Append(string origin, SystemTextID prefix, int count)
	{
		var append = ConstData.GetSystemText(prefix) + " x " + count.ToString();
		if(string.IsNullOrEmpty(origin)) return append;
		return origin + "\n" + append;
	}
}
