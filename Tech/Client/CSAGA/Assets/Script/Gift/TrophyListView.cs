﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TrophyListView : RewardListView {

	protected override void Initialize ()
	{
		GameSystem.Service.OnTrophyList += OnTrophyList;
		base.Initialize ();
	}
	
	protected override void OnDestroy ()
	{
		if(GameSystem.IsAvailable) GameSystem.Service.OnTrophyList -= OnTrophyList;
		base.OnDestroy ();
	}

	protected override void Setup ()
	{
		GameSystem.Service.TrophyList();
	}
	
	void OnTrophyList ()
	{
		var list = new List<RewardData>(GameSystem.Service.TrophyInfo.List.Count);
		
		foreach(var item in GameSystem.Service.TrophyInfo.List)
		{
			var data = new RewardData{ Trophy = item.Value };
			list.Add(data);
		}
		
		list.Sort(Compare);
		
		Setup(list);
	}
	
	private static int Compare(RewardData lhs, RewardData rhs)
	{
		return lhs.Trophy.Group.CompareTo(rhs.Trophy.Group);
	}
}
