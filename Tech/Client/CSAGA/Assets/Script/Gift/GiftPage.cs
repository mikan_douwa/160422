﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GiftPage : Stage {

	public const int GIFT = 0;
	public const int TROPHY = 1;

	public PresetNavigator navigator;

	protected override void Initialize ()
	{
		navigator.OnCurrentChange += OnCurrentChange;
		navigator.Next(GIFT);
		base.Initialize ();
	}

	void OnCurrentChange (PresetNavigator obj)
	{
		switch(navigator.current)
		{
		case GIFT:
			titleTextID = SystemTextID.REWARD_BOX;
			break;
		case TROPHY:
			titleTextID = SystemTextID.TROPHY;
			break;
		}

		UpdateHeadline();
	}

	protected override void OnNext ()
	{
		switch(navigator.current)
		{
		case GIFT:
			navigator.Next(TROPHY);
			break;
		case TROPHY:
			navigator.Next(GIFT);
			break;
		}
	}
}
