﻿using UnityEngine;
using System.Collections;

class CSAGAWebViewCallback : Kogarasi.WebView.IWebViewCallback
{
	public void onLoadStart( string url )
	{
		Debug.Log( "call onLoadStart : " + url );
	}
	public void onLoadFinish( string url )
	{
		Debug.Log( "call onLoadFinish : " + url );
	}
	public void onLoadFail( string url )
	{
		Debug.Log( "call onLoadFail : " + url );
	}
}

public class CSAGAWebView : Stage
{
	public GameObject closeBtn;

	CSAGAWebViewCallback m_callback;

	protected override void Initialize ()
	{
		UIEventListener.Get(closeBtn).onClick = Close;

		//Call Web View
		m_callback = new CSAGAWebViewCallback();
		
		WebViewBehavior webview = GetComponent<WebViewBehavior>();
		
		if( webview != null )
		{
			var id = 0;
			switch(Context.Instance.StageID)
			{
			case StageID.Announce:
				id = 90001;
				break;
			case StageID.UserTreaty:
				id = 90002;
				break;
			case StageID.Bahamut:
				id = 90003;
				break;
			}

			string url = ConstData.Tables.GetSystemText(id) + "?r=" + Mikan.MathUtils.Random.Next(0, 9999999).ToString();
			webview.LoadURL( url );
			webview.SetVisibility( true );
			webview.SetMargins(80, 80, 80, 200);
			webview.setCallback( m_callback );
		}

		base.Initialize ();
	}

	void Close(GameObject btn)
	{
		Destroy(gameObject);
	}

}
