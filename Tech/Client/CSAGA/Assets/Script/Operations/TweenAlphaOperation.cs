﻿using UnityEngine;
using System.Collections;
using Mikan;

public class TweenAlphaOperation : TweenOperationBase {
	
	private GameObject target;
	private float to;
	private float from;
	private float duration;

	private bool fromBeginnng = false;
	
	public TweenAlphaOperation(GameObject target, float duration, float from, float to)
	{
		
		this.target = target;
		this.from = from;
		this.to = to;
		this.duration = duration;
		
		fromBeginnng = true;
	}

	public TweenAlphaOperation(GameObject target, float duration, float to)
	{
		this.target = target;
		this.to = to;
		this.duration = duration;
	}
	
	#region implemented abstract members of TweenOperationBase
	protected override UITweener CreateTweener ()
	{
		return TweenAlpha.Begin(target, duration, to);
	}
	#endregion
	
	public override void Execute ()
	{
		if(fromBeginnng) target.GetComponent<UIWidget>().alpha = from;
		base.Execute ();
	}
}
