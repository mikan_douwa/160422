﻿using UnityEngine;
using System.Collections;

public class TweenScaleOperation : TweenOperationBase {
	
	private GameObject target;
	private Vector3 scale;
	private float duration;
	
	private UITweener tweener;
	public TweenScaleOperation(GameObject target, float duration, Vector3 scale)
	{
		this.target = target;
		this.scale = scale;
		this.duration = duration;
	}
	
	#region implemented abstract members of TweenOperationBase
	protected override UITweener CreateTweener ()
	{
		return TweenScale.Begin(target, duration, scale);
	}
	#endregion
	
	
}
