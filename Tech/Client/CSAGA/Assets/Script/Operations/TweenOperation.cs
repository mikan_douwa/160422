﻿using UnityEngine;
using System;
using System.Collections;

public class TweenOperation : TweenOperationBase {

	private UITweener tweener;
	private Func<UITweener> factory;
	public TweenOperation(UITweener tweener)
	{
		this.tweener = tweener;
		tweener.enabled = false;
	}

	public TweenOperation(Func<UITweener> factory)
	{
		this.factory = factory;
	}

	#region implemented abstract members of TweenOperationBase
	protected override UITweener CreateTweener ()
	{
		if(tweener)
		{
			tweener.enabled = true;
			return tweener;
		}

		if(factory != null) return factory();

		return null;
	}
	#endregion

}
