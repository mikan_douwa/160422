﻿using UnityEngine;
using System;
using System.Collections;
using Mikan;

public class TweenFloatValueOperation : TweenOperationBase {
	
	private GameObject target;
	private float final;
	private float duration;

	private Action<float> changeFunc;
	
	public TweenFloatValueOperation(GameObject target, float duration, float final, Action<float> changeFunc)
	{
		this.target = target;
		this.final = final;
		this.duration = duration;
		this.changeFunc = changeFunc;
	}
	
	#region implemented abstract members of TweenOperationBase
	protected override UITweener CreateTweener ()
	{
		var tween = TweenFloatValue.Begin(target, duration, final);

		tween.OnValueChanged = changeFunc;

		return tween;
	}
	#endregion
	
	
}
