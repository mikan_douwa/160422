﻿using UnityEngine;
using System.Collections;
using Mikan;

class DelayOperation : OperationBase
{
	private float seconds;
	private int serial;
	public DelayOperation(float seconds)
	{
		this.seconds = seconds;
	}
	#region implemented abstract members of OperationBase
	
	public override void Execute ()
	{
		Stop();

		Coroutine.Start(DelayRoutine(serial));
		
	}
	
	#endregion

	public override void Stop ()
	{
		base.Stop ();
		++serial;
	}

	public override void Dispose ()
	{
		Stop ();
		base.Dispose ();
	}
	
	private IEnumerator DelayRoutine(int serial)
	{
		yield return new WaitForSeconds(seconds);

		if(serial == this.serial) OperationComplete ();
	}
}
