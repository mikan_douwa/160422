﻿using UnityEngine;
using System.Collections;

public class TweenPositionOperation : TweenOperationBase {

	private GameObject target;
	private Vector3 from;
	private Vector3 to;
	private float duration;

	private bool fromBeginnng = false;

	public TweenPositionOperation(GameObject target, float duration, Vector3 from, Vector3 to)
	{

		this.target = target;
		this.from = from;
		this.to = to;
		this.duration = duration;

		fromBeginnng = true;
	}

	public TweenPositionOperation(GameObject target, float duration, Vector3 to)
	{
		this.target = target;
		this.to = to;
		this.duration = duration;
	}

	#region implemented abstract members of TweenOperationBase
	protected override UITweener CreateTweener ()
	{
		return TweenPosition.Begin(target, duration, to);
	}
	#endregion

	public override void Execute ()
	{
		if(fromBeginnng) target.transform.localPosition = from;

		base.Execute ();
	}

}
