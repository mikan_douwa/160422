﻿using UnityEngine;
using System;
using System.Collections;
using Mikan;

class InstantiatePrefabOperation : OperationBase
{
	public event Action<InstantiatePrefabOperation, GameObject> OnInstantiate;
	private GameObject parent;
	private string path;
	private IAsset<GameObject> asset;
	public InstantiatePrefabOperation(GameObject parent, string path)
	{
		this.parent = parent;
		this.path = path;
	}
	
	#region implemented abstract members of OperationBase
	
	public override void Execute ()
	{
		asset = ResourceManager.Instance.LoadPrefab(path, OnAssetLoadComplete);
	}
	
	#endregion
	
	public override void Stop ()
	{
		base.Stop ();
		if(asset != null)
		{
			asset.DecreaseReferenceCount();
			asset = null;
		}
	}
	
	public override void Dispose ()
	{
		Stop ();
		
		OnInstantiate = null;
		base.Dispose ();
	}
	
	void OnAssetLoadComplete (IAsset<GameObject> _asset)
	{		
		var obj = UIManager.Instance.AddChild(parent, asset.Content);
		
		if(OnInstantiate != null) OnInstantiate(this, obj);
		
		asset.DecreaseReferenceCount();
		
		asset = null;

		OperationComplete();
	}
}
