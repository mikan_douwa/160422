﻿using UnityEngine;
using System.Collections;
using Mikan;

abstract public class TweenOperationBase : OperationBase {

	private UITweener tweener;

	private bool isStopped;

	#region implemented abstract members of OperationBase

	public override void Execute ()
	{
		isStopped = false;

		if(tweener == null) 
		{
			tweener = CreateTweener();
			EventDelegate.Add(tweener.onFinished, OnTweenerFinished, true);
		}

		tweener.PlayForward();
	}

	#endregion


	public override void Stop ()
	{
		isStopped = true;

		if(tweener) 
		{
			tweener.enabled = false;
			EventDelegate.Remove(tweener.onFinished, OnTweenerFinished);
			tweener = null;
		}
	}

	public override void Dispose ()
	{
		Stop ();
		
		if(tweener) EventDelegate.Remove(tweener.onFinished, OnTweenerFinished);

		base.Dispose();
	}

	abstract protected UITweener CreateTweener();


	private void OnTweenerFinished()
	{
		tweener.enabled = false;
		tweener = null;

		if(!isStopped) OperationComplete();
	}
}
