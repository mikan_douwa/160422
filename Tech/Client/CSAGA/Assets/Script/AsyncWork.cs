﻿using UnityEngine;
using System.Collections;
using Mikan;
using System.Collections.Generic;

public class AsyncWork {

	private static TaskThread thread = null;

	public static IEnumerator Execute(Callback callback)
	{
		if(thread == null) thread = new TaskThread();

		var task = new Task { callback = callback };

		thread.AddTask(task);

		while(!task.isDone) yield return new WaitForFixedUpdate();
	}

	public static void Reset()
	{
#if PROFILE
		Task.Ticks = new List<string>();
#endif

		if(thread != null)
		{
			thread.Dispose();
			thread = null;
		}
	}

	class Task : ITask
	{
#if PROFILE
		public static List<string> Ticks = new List<string>();
#endif
		public Callback callback;
		
		public bool isDone;
		
		#region ITask implementation
		public void Execute ()
		{
#if PROFILE
			var beginTime = System.DateTime.Now;
#endif
			try
			{
				callback();
			}
			catch(System.Exception ex)
			{
				KernelService.Logger.Fatal(ex.ToString());
			}

#if PROFILE
			var endTime = System.DateTime.Now;
			Ticks.Add(endTime.Subtract(beginTime).TotalMilliseconds.ToString());
#endif
			isDone = true;
			
		}
		#endregion
		#region IDisposable implementation
		public void Dispose () {}
		#endregion
	}
#if PROFILE
	public static void TraceAndReset()
	{
		Debug.LogWarning(string.Join(",", Task.Ticks.ToArray()));

		Reset ();
	}
#endif
}
