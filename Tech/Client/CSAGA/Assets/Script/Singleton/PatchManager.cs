﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;

public class PatchManager : Singleton<PatchManager> {

	public int TotalFileCount;
	public int DownloadedFileCount;

	public bool IsPatching { get; private set; }

	public string Signature { get; private set; }

	public string Version { get; private set; }

	public string PatchVersion { get; private set; }

	public string ServerURL { get; private set; }

	private List<PatchFileInfo> files;

	private List<string> cache = new List<string>();

	private Dictionary<string, List<Action>> waitList = new Dictionary<string, List<Action>>();

	private Queue<PatchFileInfo> queue = new Queue<PatchFileInfo>();

	private string url;

	private string patchRoot;

	private Dictionary<string, Vector2> textureSizes;

	private HashSet<string> downloading = new HashSet<string>();

	private string FileListPath { get { return CacheRoot + "/file_list.txt"; } }

	public string CacheRoot { get{ return Application.persistentDataPath; } }

	private bool isDownloading = false;

	// Use this for initialization
	void Awake () {
		url = GameSystem.Config["patch_url"];
	}

	void Update()
	{

	}

	void OnApplicationPause(bool isPaused)
	{
		if(isPaused) SaveDownloadProgress();
	}

	void OnDestroy()
	{

	}

	public Vector2 GetOriginTextureSize(string path)
	{
		var size = default(Vector2);
		if(textureSizes == null || !textureSizes.TryGetValue(path, out size)) return Vector2.zero;
		return size;
	}

	public void DeleteAllFiles()
	{
		var files = new List<string>();
		
		if(File.Exists(FileListPath)) 
		{
			files = new List<string>(File.ReadAllLines(FileListPath));
			File.Delete(FileListPath);
		}
		else
			return;

		foreach(var item in files)
		{
			if(string.IsNullOrEmpty(item) || !item.Contains("|")) continue;
			
			var split = item.Split('|');

			var path = CacheRoot + "/" + split[0];

			if(File.Exists(path)) File.Delete(path);
		}
	}

	public void PreloadCard(List<int> ids)
	{
		foreach(var id in ids) AddToDownloadQueue(ConstData.SelectRelated(ConstData.Tables.Card[id]));
		BeginDownlaod(queue);
	}

	public void DownloadEventPatch(Mikan.CSAGA.ConstData.Interactive data)
	{
		AddToDownloadQueue(ConstData.SelectRelated(data));
		BeginDownlaod(queue);
	}


	public void DownloadQuestPatch(Mikan.CSAGA.QuestInstance quest)
	{
#if PATCH
		var data = ConstData.Tables.QuestDesign[quest.QuestID];

		AddToDownloadQueue(data.s_BG);

		for(int i = 0; i < quest.Rounds; ++i)
		{
			var interactiveId = Mikan.CSAGA.Calc.Quest.GetEvent(quest.QuestID, i + 1);

			if(interactiveId > 0) AddToDownloadQueue(ConstData.SelectRelated(ConstData.Tables.Interactive[interactiveId]));

			if(i + 1 == quest.Rounds)
			{
				interactiveId = Mikan.CSAGA.Calc.Quest.GetEvent(quest.QuestID, 99);
				
				if(interactiveId > 0) AddToDownloadQueue(ConstData.SelectRelated(ConstData.Tables.Interactive[interactiveId]));
			}

			var ev = ConstData.Tables.QuestEvent[quest.Events[i]];

			AddToDownloadQueue(ConstData.SelectRelated(ev));
		}

		if(quest.QuestID == ConstData.Tables.TutorialQuestID)
		{
			foreach(var item in files)
			{
				if(item.Path.Contains("Texture/TUTORIAL/battle_")) AddToDownloadQueue(item);
			}
		}

		BeginDownlaod(queue);
#else
		Message.Send(MessageID.DownloadEnd);
#endif
	}


	public void Download(string path, Action onComplete)
	{
		var filename = path + ".unity";
		var info = GetPatchFileInfo(filename);

		if(info == null) 
			onComplete();
		else
		{
			List<Action> list;
			if(!waitList.TryGetValue(filename, out list)) 
			{
				list = new List<Action>();
				waitList.Add(filename, list);
			}

			list.Add(onComplete);

			queue.Enqueue(info);

			if(!isDownloading) BeginDownlaod(queue);
		}
	}

	private void OnDownloadComplete(PatchFileInfo info)
	{
		List<Action> list;
		if(!waitList.TryGetValue(info.Path, out list)) return;

		waitList.Remove(info.Path);

		foreach(var item in list) item();
	}
	
	public void DownloadGeneralPatch()
	{
		StartCoroutine(DownloadFileList());
	}

	public void DownloadUserPatch()
	{
		queue = new Queue<PatchFileInfo>();

		foreach(var item in GameSystem.Service.CardInfo.List)
		{
			AddToDownloadQueue(queue, ConstData.SelectRelated(item.Data));
		}


		/*
		foreach(var item in GameSystem.Service.ItemInfo.Consumables)
		{
			var data = GameSystem.ConstData.itemShowDataTable[item.ID];
			AddToDownloadQueue(queue, ConstData.SelectRelated(data));
		}
		*/

		/*
		foreach(var item in GameSystem.Service.EquipmentInfo.List)
		{
			var data = ConstData.Tables.Item[item.ID];
			AddToDownloadQueue(queue, ConstData.SelectRelated(data));
		}
		*/

		BeginDownlaod(queue);
	}

	private void AddToDownloadQueue(PatchFileInfo file)
	{
		if(!cache.Contains(file.Origin)) return;
		if(!queue.Contains(file)) queue.Enqueue(file);
	}

	private void AddToDownloadQueue(string file)
	{
		AddToDownloadQueue(queue, new string[]{ file });
	}

	private void AddToDownloadQueue(IEnumerable<string> files)
	{
		AddToDownloadQueue(queue, files);
	}

	private void AddToDownloadQueue(Queue<PatchFileInfo> queue, IEnumerable<string> files)
	{
#if PATCH
		foreach(var file in files) 
		{
			var info = GetPatchFileInfo(file);
			if(info != null && !queue.Contains(info)) queue.Enqueue(info);
		}
#endif
	}

	private PatchFileInfo GetPatchFileInfo(string file)
	{
		if(files != null)
		{
			foreach(var item in files)
			{
				if(item.Path != file) continue;
				
				if(!cache.Contains(item.Origin)) return item;
			}
		}

		return null;
	}

	private void ParseMetaData(string text)
	{
		textureSizes = new Dictionary<string, Vector2>();

		var lines = text.Split('\n');

		foreach(var item in lines) 
		{
			var args = item.Split('|');
			if(args.Length < 2) continue;
			if(args[0] == "1")
				textureSizes.Add(args[1], new Vector2(int.Parse(args[2]), int.Parse(args[3])));

		}
	}
	
	private IEnumerator DownloadFileList()
	{
		var isPending = false;

		while(Application.internetReachability == NetworkReachability.NotReachable)
		{
			if(!isPending)
			{
				isPending = true;
				MessageBox.Show(ConstData.GetSystemText(SystemTextID._285_INTERNET_NOT_REACHABLE)).callback = (index)=>{ isPending = false; };
			}

			yield return new WaitForSeconds(1);
		}

		var www = default(WWW);

		var retryCount = 0;

		while(true)
		{
#if PUBLISH
			www = new WWW(url + "app_version" + UIDefine.AppVersion + ".txt?id=" + DateTime.Now.Ticks.ToString());
#else
			www = new WWW(url + "app_version.txt?id=" + DateTime.Now.Ticks.ToString());
#endif
			yield return www;

			if(string.IsNullOrEmpty(www.error)) break;

			Debug.LogWarning(www.error);

			yield return new WaitForSeconds(1);

			if(++retryCount > 3) break;
		}
		
		var args = www.text.Split('|');

		www.Dispose();

		Version = args[0];

		if(UIDefine.AppVersion.CompareTo(Version) < 0)
		{
			var msgBox = MessageBoxContext.Create("PreloadScene/AppNotMatchMessageBox", args[1]);
			msgBox.content = ConstData.GetSystemText(SystemTextID._179_TIP_APP_NOT_MATCH);
			msgBox.buttons = new string[]{ ConstData.GetSystemText(SystemTextID.BTN_CONFIRM) };
			MessageBox.Show(msgBox);

			yield break;
		}

		PatchVersion = patchRoot = args[2];

		if(!patchRoot.StartsWith("http")) patchRoot = url + patchRoot;

		ServerURL = args[3];

#if PATCH

		www = new WWW(patchRoot + "/signature.txt?id=" + DateTime.Now.Ticks.ToString());
		
		yield return www;
		
		Signature = www.text;
		
		www.Dispose();

		www = new WWW(patchRoot + "/file_list.txt?id=" + DateTime.Now.Ticks.ToString());

		yield return www;
		
		var files = www.text.Split('\n');

		this.files = new List<PatchFileInfo>(files.Length);
		
		www.Dispose();

		var origins = new List<string>();

		if(File.Exists(FileListPath)) origins = new List<string>(File.ReadAllLines(FileListPath));

		queue = new Queue<PatchFileInfo>();
		
		foreach(var item in files)
		{
			if(string.IsNullOrEmpty(item) || !item.Contains("|")) continue;

			var split = item.Split('|');
			var info = new PatchFileInfo();
			info.Origin = item;
			info.Path = split[0];
			info.CRC = uint.Parse(split[1]);
			info.Flag = int.Parse(split[2]);

			this.files.Add(info);

			if(origins.Contains(item) && File.Exists(CacheRoot + "/" + info.Path))
				cache.Add(item);
			else if(info.Flag == 1)
				queue.Enqueue(info);
		}
		
		BeginDownlaod(queue);

#else
		www = new WWW("file://" + Application.dataPath + "/__patch/metadata.txt");
		yield return www;

		ParseMetaData(www.text);
		www.Dispose();

		Message.Send(MessageID.DownloadEnd);
#endif
	}

	private void BeginDownlaod(Queue<PatchFileInfo> queue)
	{
		TotalFileCount = queue.Count;
		
		DownloadedFileCount = 0;
		
		if(TotalFileCount > 0)
		{
			if(!isDownloading)
			{
				isDownloading = true;
				Message.Send(MessageID.DownloadBegin);
			}
			
			for(int i = 0; i < 3; ++i) StartCoroutine(DownloadFile(queue));

			StartCoroutine(WaitForDownloadComplete());
		}
		else
		{
			StartCoroutine(OnDownloadEnd());
		}
	}
	
	private IEnumerator DownloadFile(Queue<PatchFileInfo> queue)
	{
		while(queue.Count > 0)
		{
			var item = queue.Dequeue();

			yield return StartCoroutine(DownloadFile(item));
			
			++DownloadedFileCount;
		}
	}

	bool isRetryMsgBoxClosed = false;
	private IEnumerator WaitForRetryCheck()
	{
		isRetryMsgBoxClosed = false;
		MessageBox.Show(SystemTextID._171_DOWNLOAD_FAIL).callback = OnMessageBoxClose;
		while(!isRetryMsgBoxClosed) yield return new WaitForEndOfFrame();
	}

	private void OnMessageBoxClose(int index)
	{
		isRetryMsgBoxClosed = true;
	}

	private IEnumerator DownloadFile(PatchFileInfo item)
	{
		if(item == null || string.IsNullOrEmpty(item.Path)) yield break;

		while(downloading.Contains(item.Path)) yield return new WaitForEndOfFrame();

		if(cache.Contains(item.Origin)) 
		{
			OnDownloadComplete(item);
			yield break;
		}

		downloading.Add(item.Path);

		var fileUrl = patchRoot + "/" + item.Path + "?id=" + item.CRC;
		
		Debug.Log("download file, url = " + fileUrl); 
		
		var www = new WWW(fileUrl);

		var retryCount = 0;

		while(true)
		{
			while(!www.isDone)
			{
				Message.Send(new DownloadProgressMessage{ Progress = www.progress });

				yield return new WaitForEndOfFrame();
			}

			if(string.IsNullOrEmpty(www.error)) break;
		
			www.Dispose();

			if(++retryCount < 2)
				yield return new WaitForSeconds(0.3f);
			else
				yield return StartCoroutine(WaitForRetryCheck());

			www = new WWW(fileUrl);
		}

		var endIndex = item.Path.LastIndexOf("/");

		if(endIndex >= 0)
		{
			var directory = CacheRoot + "/" + item.Path.Substring(0, endIndex);
		
			if(!Directory.Exists(directory)) Directory.CreateDirectory(directory);
		}

		var path = CacheRoot + "/" + item.Path;
		File.WriteAllBytes(path, www.bytes);

#if UNITY_IPHONE
		iPhone.SetNoBackupFlag(path);
#endif
		
		if(item.Path.EndsWith(".unity") && www.assetBundle != null) www.assetBundle.Unload(true);
		
		www.Dispose();
		
		cache.Add(item.Origin);

		downloading.Remove(item.Path);

		OnDownloadComplete(item);
	}
	
	private IEnumerator WaitForDownloadComplete()
	{
		while(DownloadedFileCount < TotalFileCount) yield return 0;

		SaveDownloadProgress();

		yield return new WaitForSeconds(0.3f);

		if(queue.Count > 0)
			BeginDownlaod(queue);
		else
		{
			isDownloading = false;

			yield return StartCoroutine(OnDownloadEnd());
		}
	}

	private IEnumerator OnDownloadEnd()
	{
		if(textureSizes == null) 
		{
			var path = string.Format("{0}/metadata.unity", CacheRoot);

#if UNITY_EDITOR_WIN
			var www = new WWW("file://C://" + path);
#else
			var www = new WWW("file://" + path);
#endif
			
			yield return www;
			
			ParseMetaData(((TextAsset)www.assetBundle.mainAsset).text);

			www.assetBundle.Unload(true);
			
			www.Dispose();
		}

		Message.Send(MessageID.DownloadEnd);
	}

	private void SaveDownloadProgress()
	{
		if(cache.Count > 0 && TotalFileCount > 0) File.WriteAllLines(FileListPath, cache.ToArray());
	}
}

class PatchFileInfo
{
	public string Origin;
	public string Path;
	public uint CRC;
	public int Flag;
}
