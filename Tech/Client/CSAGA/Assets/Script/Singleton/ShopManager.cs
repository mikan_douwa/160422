﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ShopManager : Singleton<ShopManager> {

	private bool hasNewCard = false;

	void Awake()
	{
		Message.AddListener(MessageID.IAP, OnIAP);
		Message.AddListener(MessageID.ExpandCardSpace, OnExpandCardSpace);
		Message.AddListener(MessageID.ChargeTalkPoint, OnChargeTalkPoint);
		Message.AddListener(MessageID.Continue, OnContinue);
		Message.AddListener(MessageID.ChargeQuestCount, OnChargeQuestCount);
		Message.AddListener(MessageID.RandomShop, OnRandomShop);
		Message.AddListener(MessageID.RandomShopRefresh, OnRandomShopRefresh);
		Message.AddListener(MessageID.RandomShopExpand, OnRandomShopExpand);
		Message.AddListener(MessageID.ChargeRescuePoint, OnChargeRescuePoint);
		Message.AddListener(MessageID.ChargeCrusadePoint, OnChargeCrusadePoint);
	}

	public void Setup()
	{
		GameSystem.Service.OnPurchase += OnPurchase;
		GameSystem.Service.OnGachaDraw += OnGachaDraw;
	}

	public void ShopPurchase(int index, int id)
	{
		GameSystem.Service.RandomShopPurchase(index, id);
	}

	void OnGachaDraw (Mikan.CSAGA.SyncCard syncCard)
	{
		Context.Instance.Cards = new List<Mikan.Serial>(syncCard.AddList.Count);

		foreach(var item in syncCard.AddList) 
		{
			Context.Instance.Cards.Add(item.Serial);
		}

		if(!Context.Instance.IsTutorialComplete)
		{
			var team = GameSystem.Service.TeamInfo[0];
			var list = new List<Mikan.Serial>(5);
			list.Add(Context.Instance.Cards[0]);
			foreach(var item in team.Members)
			{
				if(list.Contains(item)) continue;
				list.Add(item);
				if(list.Count >= 5) break;
			}
			GameSystem.Service.TeamUpdate(0, list);
		}

		var ids = Mikan.CSAGA.CardUtils.GetIDs(syncCard.AddList);

		hasNewCard = true;
		Message.AddListener(MessageID.DownloadEnd, OnDownloadEnd);

		PatchManager.Instance.PreloadCard(ids);
	}

	private void OnDownloadEnd()
	{
		if(!hasNewCard) return;
		Message.RemoveListener(MessageID.DownloadEnd, OnDownloadEnd);
		hasNewCard = false;
		if(Context.Instance.IsTutorialComplete)
			Message.Send(MessageID.SwitchStage, StageID.GachaDrawAnimation);
		else
			StageManager.Instance.ReplaceAppend(StageID.GachaDrawAnimation);

	}

	public void DrawGacha(int gachaId)
	{
		if(!CheckCardSpace(1)) return;
		var data = ConstData.Tables.GachaList[gachaId];
		if(!CheckCurrency(data.n_COINTYPE, data.n_COIN)) return;
		GameSystem.Service.GachaDraw(gachaId);
	}

	public bool CheckCardSpace(int min)
	{
		var count = GameSystem.Service.CardInfo.List.Count;
		var limit = ConstData.Tables.CardSpaceCalc.Count(GameSystem.Service.PurchaseInfo.Count(Mikan.CSAGA.ArticleID.CardSpace));
		if(limit - count < min)
		{
			var msg = ConstData.GetSystemText(SystemTextID.TIP_CARD_SPACE_NOT_ENOUGH_V1_V2, count, limit);
			MessageBox.Show(msg, SystemTextID.EXPAND, SystemTextID.BTN_CARD).callback = OnCardOverflow;
			return false;
		}

		return true;
	}

	public bool CheckGem(int cost, System.Action<int> callback = null)
	{
		if(GameSystem.Service.PurchaseInfo.Gem < cost)
		{
			MessageBox.Show(SystemTextID.TIP_GEM_NOT_ENOUGH).callback = callback;
			return false;
		}
		return true;
	}

	public bool CheckCurrency(Mikan.CSAGA.DropType coinType, int cost, int coinId = 0)
	{
		var isEnough = false;

		switch(coinType)
		{
		case Mikan.CSAGA.DropType.Gem:
			return CheckGem(cost);
		case Mikan.CSAGA.DropType.Coin:
			isEnough = GameSystem.Service.PlayerInfo[Mikan.CSAGA.PropertyID.Coin] >= cost;
			break;
		case Mikan.CSAGA.DropType.Crystal:
			isEnough = GameSystem.Service.PlayerInfo[Mikan.CSAGA.PropertyID.Crystal] >= cost;
			break;
		case Mikan.CSAGA.DropType.FP:
			isEnough = GameSystem.Service.PlayerInfo[Mikan.CSAGA.PropertyID.FP] >= cost;
			break;
		case Mikan.CSAGA.DropType.Item:
			if(GameSystem.Service.ItemInfo.Count(coinId) >= cost) return true;
			MessageBox.Show(SystemTextID.TIP_ITEM_NOT_ENOUGH);
			return false;
		default:
			break;
		}

		if(!isEnough) MessageBox.Show(SystemTextID._178_TIP_CURRENCY_NOT_ENOUGH);

		return isEnough;
	}

	private void OnCardOverflow(int index)
	{
		if(index  == 0)
			Message.Send(MessageID.ExpandCardSpace);
		else
			Message.Send(MessageID.SwitchStage, StageID.Card);
	}

	private void OnIAP()
	{
		if(ConstData.Tables.GetVariable(Mikan.CSAGA.VariableID.IAP_ENABLE) == 0)
			MessageBox.Show(SystemTextID.TIP_NOT_AVAILABLE);
		else
			MessageBox.Show(SystemTextID.TIP_NOT_AVAILABLE);
	}

	private void OnExpandCardSpace()
	{
		if(ConstData.Tables.CardSpaceCalc.Count(GameSystem.Service.PurchaseInfo.Count(Mikan.CSAGA.ArticleID.CardSpace)) < ConstData.Tables.CardSpaceCalc.Limit)
		{
			var msg = ConstData.GetSystemText(SystemTextID.TIP_EXPAND_CARD_SPACE_CONFIRM_V1_V2, ConstData.Tables.CardSpaceCost, ConstData.Tables.CardSpaceAdd);
			MessageBox.ShowConfirmWindow(msg, OnExpandCardSpaceConfirm);
		}
		else
			MessageBox.Show(SystemTextID.TIP_CARD_SPACE_MAX);
	}

	private void OnChargeTalkPoint()
	{
		if(GameSystem.Service.PlayerInfo.TalkPoint < ConstData.Tables.GetVariable(Mikan.CSAGA.VariableID.COMMU_LIMIT))
		{
			var msg = ConstData.GetSystemText(SystemTextID.TIP_CHARGE_TALK_POINT_CONFIRM_V1, ConstData.Tables.GetVariable(Mikan.CSAGA.VariableID.COMMU_COUNT_DIAMOND));
			MessageBox.ShowConfirmWindow(msg, OnChargeTalkPointConfirm);
		}
		else
			MessageBox.Show(SystemTextID.TIP_TALK_POINT_MAX);
	}

	private void OnContinue()
	{
		MessageBox.ShowConfirmWindow(ConstData.GetSystemText(SystemTextID.TIP_CONTINUE_V1, ConstData.Tables.ContinueCost), OnContinueConfirm); 
	}

	private void OnRandomShop()
	{
		if(ConstData.Tables.GetVariable(Mikan.CSAGA.VariableID.SHOP_ENABLE) == 0)
			MessageBox.Show(SystemTextID.TIP_NOT_AVAILABLE);
		else
			Message.Send(MessageID.SwitchStage, StageID.RandomShop);
	}

	private void OnRandomShopRefresh()
	{
		if(GameSystem.Service.ItemInfo.Count(ConstData.Tables.RandomShopRefreshItemID) > 0)
		{
			MessageBox.ShowConfirmWindow(ConstData.GetSystemText(SystemTextID.TIP_REFRESH_SHOP_CONFIRM), OnRandomShopRefreshConfirm);
		}
		else
			MessageBox.Show(SystemTextID.TIP_ITEM_NOT_ENOUGH);
	}

	private void OnRandomShopExpand()
	{
		if(ConstData.Tables.RandomShopSlotCalc.Count(GameSystem.Service.PurchaseInfo.Count(Mikan.CSAGA.ArticleID.RandomShopSlot)) < ConstData.Tables.RandomShopSlotCalc.Limit)
		{
			var msg = ConstData.GetSystemText(SystemTextID._176_TIP_EXPAND_SHOP_SLOT_CONFIRM_V1_V2, ConstData.Tables.RandomShopSlotCalc.Cost, 1);
			MessageBox.ShowConfirmWindow(msg, OnRandomShopExpandConfirm);
		}
		else
			MessageBox.Show(SystemTextID._175_TIP_SHOP_SLOT_MAX);
	}

	private void OnChargeQuestCount()
	{
		MessageBox.ShowConfirmWindow(ConstData.GetSystemText(SystemTextID.TIP_CHARGE_CHALLENGE_COUNT_V1, ConstData.Tables.ChargeQuestCost), OnChargeQuestCountConfirm); 
	}

	private void OnChargeRescuePoint()
	{
		MessageBox.ShowConfirmWindow(ConstData.GetSystemText(SystemTextID._222_TIP_RESET_RESCUE_CONFIRM, ConstData.Tables.ChargeRescueCost), OnChargeRescuePointConfirm);
	}

	private void OnChargeCrusadePoint()
	{
		MessageBox.ShowConfirmWindow(ConstData.GetSystemText(SystemTextID._444_TIP_CRUSADE_CHARGE_CONFIRM_V1, ConstData.Tables.ChargeCrusadeCost), OnChargeCrusadePointConfirm);
	}

	private void OnExpandCardSpaceConfirm(int index)
	{
		if(index == 0 && CheckGem(ConstData.Tables.CardSpaceCost)) Purcahse(Mikan.CSAGA.ArticleID.CardSpace);

	}

	private void OnChargeTalkPointConfirm(int index)
	{
		if(index == 0 && CheckGem(ConstData.Tables.ChargeTPCost)) Purcahse(Mikan.CSAGA.ArticleID.TalkPoint);
	}

	private void OnContinueConfirm(int index)
	{
		if(index == 0)
		{
			if(CheckGem(ConstData.Tables.ContinueCost, OnContinueGemNotEnough))
				Purcahse(Mikan.CSAGA.ArticleID.Continue);
		}
		else
			FightManager.Instance.OnRevive(false);
	}

	private void OnContinueGemNotEnough(int index)
	{
		OnContinue();
	}

	private void OnChargeQuestCountConfirm(int index)
	{
		if(index == 0 && CheckGem(ConstData.Tables.ChargeQuestCost)) Purcahse(Mikan.CSAGA.ArticleID.QuestCount, Context.Instance.QuestID);
	}

	private void OnChargeRescuePointConfirm(int index)
	{
		if(index == 0 && CheckGem(ConstData.Tables.ChargeRescueCost)) Purcahse(Mikan.CSAGA.ArticleID.RescuePoint);
	}

	private void OnChargeCrusadePointConfirm(int index)
	{
		if(index == 0 && CheckGem(ConstData.Tables.ChargeCrusadeCost)) Purcahse(Mikan.CSAGA.ArticleID.CrusadePoint);
	}

	private void OnRandomShopExpandConfirm(int index)
	{
		if(index == 0 && CheckGem(ConstData.Tables.RandomShopSlotCalc.Cost)) Purcahse(Mikan.CSAGA.ArticleID.RandomShopSlot);
	}

	private void OnRandomShopRefreshConfirm(int index)
	{
		if(index == 0) GameSystem.Service.RandomShopRefresh();
	}

	private void Purcahse(Mikan.CSAGA.ArticleID articleId, int param = 0)
	{
		GameSystem.Service.Purchase(articleId, param);
	}

	void OnPurchase (Mikan.CSAGA.ArticleID id)
	{
		switch(id)
		{
		case Mikan.CSAGA.ArticleID.TalkPoint:
			MessageBox.Show(SystemTextID.TIP_TALK_POINT_CHARGED);
			break;
		case Mikan.CSAGA.ArticleID.CardSpace:
			MessageBox.Show(SystemTextID.TIP_EXPAND_CARD_SPACE_SUCC_V1, ConstData.Tables.CardSpaceCalc.Count(GameSystem.Service.PurchaseInfo.Count(Mikan.CSAGA.ArticleID.CardSpace)));
			break;
		case Mikan.CSAGA.ArticleID.RandomShopSlot:
			MessageBox.Show(SystemTextID._177_TIP_EXPAND_SHOP_SLOT_SUCC_V1, ConstData.Tables.RandomShopSlotCalc.Count(GameSystem.Service.PurchaseInfo.Count(Mikan.CSAGA.ArticleID.RandomShopSlot)));
			break;
		case Mikan.CSAGA.ArticleID.Continue:
			FightManager.Instance.OnRevive(true);
			break;
		case Mikan.CSAGA.ArticleID.QuestCount:
			break;
		case Mikan.CSAGA.ArticleID.RescuePoint:
			MessageBox.Show(SystemTextID._223_TIP_RESET_RESCUE_SUCC);
			break;
		case Mikan.CSAGA.ArticleID.CrusadePoint:
			break;
		}
	}

}
