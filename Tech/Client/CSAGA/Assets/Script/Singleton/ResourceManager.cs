﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class ResourceManager : Singleton<ResourceManager> {

	private Dictionary<string, IAsset> assets = new Dictionary<string, IAsset>();
	
	private HashSet<string> embedList;

	private string lastScene;

	public static IEnumerable<string> SelectAllFile(string directory, Func<string,bool> filter)
	{
		if(!Directory.Exists(directory)) yield break;
		foreach(var item in Directory.GetFiles(directory))
		{
			if(filter != null && filter(item)) yield return item;
		}
		
		foreach(var item in Directory.GetDirectories(directory))
		{
			foreach(var obj in SelectAllFile(item, filter)) yield return obj;
		}
	}
	
	public bool IsEmbedded(string path)
	{
		if(embedList != null && embedList.Contains(path)) return true;
		if(path.StartsWith("Texture/") || path.StartsWith("Sound/")) return false;
		return true;
	}

	void Awake()
	{
		lastScene = Application.loadedLevelName;

		var asset = Resources.Load<TextAsset>("embed_list");

		var text = asset.text;

		var split = text.Split('\n');

		embedList = new HashSet<string>();

		foreach(var item in split)
		{
			var path = item.Trim();
			if(!string.IsNullOrEmpty(path)) embedList.Add(path);
		}

		StartCoroutine(TryReleaseUnused());
	}

	private IEnumerator TryReleaseUnused()
	{
		while(true)
		{
			yield return new WaitForSeconds(30);
			
			ReleaseUnused(o=>{ return o.ReferenceCount == 0 && o.LastUseTime.AddSeconds(30) < DateTime.Now; });
		}
	}

	void OnDestroy()
	{
		foreach(var item in assets) item.Value.Release();

		assets = new Dictionary<string, IAsset>();
	}

	void OnLevelWasLoaded(int level)
	{
		var tmp = lastScene;
		lastScene = Application.loadedLevelName;

		if(tmp == SceneName.PRELOAD) return;

		ReleaseUnused(o=>
		{ 
			if(o.ReferenceCount == 0 || lastScene == SceneName.APPSTARTUP) return true;
			if(o.LastUseTime.AddMinutes(5) < DateTime.Now)
			{
				Debug.LogWarning(string.Format("resource still in use, file = {0}", o.Path));
			}
			return false;
		});

		Resources.UnloadUnusedAssets();
	}

	private void ReleaseUnused(Func<IAsset, bool> filter)
	{
		var unusedList = new List<string>();
		foreach(var item in assets)
		{
			if(!filter(item.Value) || item.Value.IsLoading) continue;

			item.Value.Release();
			unusedList.Add(item.Key);
		}
		
		foreach(var item in unusedList) assets.Remove(item);
	}

	public IAsset<T> LoadAsset<T>(string path)
		where T : UnityEngine.Object
	{
		return LoadAsset<T>(path, null);
	}

	public IAsset<T> LoadAsset<T>(string path, Action<IAsset<T>> onComplete)
		where T : UnityEngine.Object
	{
#if PATCH
		if(path.StartsWith("Texture/") || path.StartsWith("Sound/"))
			return LoadAsset<IAsset<T>>(path, onComplete, LoadDynamicAsset<T>);
		else
#endif
			return LoadAsset<IAsset<T>>(path, onComplete, LoadStaticAsset<T>);
	}
	
	private IAsset<T> LoadStaticAsset<T>(string path)
		where T : UnityEngine.Object
	{
		var asset = new StaticAsset2<T>(this);
		
		asset.Load(path);	
		
		return asset;
	}
	
	private IAsset<T> LoadDynamicAsset<T>(string path)
		where T : UnityEngine.Object
	{
		var asset = new DynamicAsset<T>(this);
		
		asset.Load(path);	
		
		return asset;
	}

	private IAsset<Texture2D> LoadNetTextureAsset(string path)
	{
		var asset = new NetTextureAsset(this);
		
		asset.Load(path);	
		
		return asset;
	}

	private T LoadAsset<T>(string path, Action<T> onComplete, Func<string, T> loader)
		where T : IAsset
	{
#if DEBUG
		GameSystem.Log("begin load asset, path = {0}", path);
#endif
		IAsset asset;
		
		if(!assets.TryGetValue(path, out asset)) 
		{
			asset = loader(path);
			assets.Add(path, asset);
		}

		asset.IncreaseReferenceCount();
		
		var _asset = (T)asset;
		
		StartCoroutine(WaitForAssetLoadComplete(_asset, onComplete));
		
		return _asset;
	}

	private IEnumerator WaitForAssetLoadComplete<T>(T asset, Action<T> onComplete)
		where T : IAsset
	{
		if(onComplete == null) yield break;

		while(true) 
		{
			yield return new WaitForEndOfFrame();
			if(!asset.IsComplete) continue;
			onComplete(asset);
			break;
		}
	}

	public IAsset<GameObject> LoadPrefab(string path, Action<IAsset<GameObject>> onComplete)
	{
		return LoadAsset<GameObject>("Prefab/" + path, onComplete);
	}

	public IAsset<GameObject> LoadPrefab(string path)
	{
		return LoadPrefab(path, null);
	}

	public IAsset<Texture2D> LoadTexture(string path, Action<IAsset<Texture2D>> onComplete)
	{
		if(path.StartsWith("http://") || path.StartsWith("https://"))
			return LoadAsset<IAsset<Texture2D>>(path, onComplete, LoadNetTextureAsset);
		else
			return LoadAsset<Texture2D>("Texture/" + path, onComplete);
	}

	
	public void PreloadTexture(string path, Action<IAsset<Texture2D>> onComplete)
	{
		var asset = LoadTexture(path, o=>
		{
			onComplete(o);
			o.DecreaseReferenceCount();
		});
	}

	public IAsset<Texture2D> LoadTexture(string path)
	{
		Action<IAsset<Texture2D>> onComplete = null;
		return LoadTexture(path, onComplete);
	}

	public TextureLoader LoadTexture(string path, UITexture texture)
	{
		var loader = texture.gameObject.AddMissingComponent<TextureLoader>();
		loader.makePixelPerfect = false;
		loader.Load(path);
		return loader;
	}

	public IAsset<UIAtlas> LoadAtlas(string path, Action<IAsset<UIAtlas>> onComplete)
	{
		return LoadAsset<UIAtlas>("Atlases/" + path, onComplete);
	}

	public IAsset<UIAtlas> LoadAtlas(string path)
	{
		return LoadAtlas(path, null);
	}

	public IAsset<AudioClip> LoadSound(string path, Action<IAsset<AudioClip>> onComplete)
	{
		return LoadAsset<AudioClip>("Sound/" + path, onComplete);
	}

	public IAsset<AudioClip> LoadSound(string path)
	{
		return LoadSound(path, null);
	}

	public void CreatePrefabInstance(string path)
	{
		CreatePrefabInstance(path, null);
	}

	public void CreatePrefabInstance(string path, Action<GameObject> onComplete)
	{
		CreatePrefabInstance(null, path, onComplete);
	}

	public void CreatePrefabInstance(GameObject parent, string path)
	{
		CreatePrefabInstance(parent, path, null);
	}

	public void CreatePrefabInstance(GameObject parent, string path, Action<GameObject> onComplete)
	{
		StartCoroutine(CreatePrefabInstanceRoutine(parent, path, onComplete));
	}

	private IEnumerator CreatePrefabInstanceRoutine(GameObject parent, string path, Action<GameObject> onComplete)
	{
		var asset = LoadPrefab(path);
		while(true)
		{
			yield return new WaitForEndOfFrame();
			if(!asset.IsComplete) continue;
			var go = UIManager.Instance.AddChild(parent, asset.Content);
			if(onComplete != null) onComplete(go);
			break;
		}
	}
}

#region Asset
public interface IAsset 
{
	bool IsLoading { get; }
	bool IsComplete { get; }
	int ReferenceCount { get; }
	string Path { get; }
	UnityEngine.Object Rawdata { get; }

	DateTime LastUseTime { get; }
	
	void IncreaseReferenceCount();
	void DecreaseReferenceCount();

	void Release();
}

public interface IAsset<T> : IAsset
{
	T Content { get; }
}

class StaticAsset<T> : IAsset<T>
	where T : UnityEngine.Object
{
	#region IAsset implementation
	
	public UnityEngine.Object Rawdata { get; private set; }

	public T Content { get; private set; }

	public string Path { get; private set; }

	public int ReferenceCount { get; private set; }

	public DateTime LastUseTime { get; private set; }

	#endregion

	private ResourceManager manager;

	private Action<IAsset<T>> onComplete;

	public bool IsComplete { get; private set; }

	public bool IsLoading { get; private set; }

	public StaticAsset(ResourceManager manager)
	{
		this.manager = manager;
	}

	public void Load(string path)
	{
		Path = path;
		manager.StartCoroutine(LoadRoutine(path));
	}

	public void IncreaseReferenceCount(){ ++ReferenceCount; }

	public void DecreaseReferenceCount(){ --ReferenceCount; }

	public void Release(){  }

	private IEnumerator LoadRoutine(string path)
	{
		yield return new WaitForEndOfFrame();

		if(!path.EndsWith("/null"))
		{
			if(!ResourceManager.Instance.IsEmbedded(path)) path = "__patch/" + path;

			Rawdata = Resources.Load<T>(path);

			if(Rawdata)
			{

				Content =  Rawdata as T;

				if(!Content)
				{
					var contentType = typeof(T);
					if(Mikan.TypeUtils.IsSubClassOf<Component>(contentType))
					{
						Content = (Rawdata as GameObject).GetComponent(contentType) as T;
					}

					if(!Content) GameSystem.LogWarning("######## asset {0} can not convert to {1}, path = {2}", Rawdata, typeof(T).Name, path);
				}
			}
			else
			{
				GameSystem.LogWarning("######## load asset failed, path = {0}", path);
			}
		}

		IsComplete = true;
	}
}

class StaticAsset2<T> : IAsset<T>
	where T : UnityEngine.Object
{
	#region IAsset implementation

	public UnityEngine.Object Rawdata { get; private set; }
	
	public T Content { get; private set; }
	
	public string Path { get; private set; }
	
	public int ReferenceCount { get { return referenceCount; } }
	
	public DateTime LastUseTime { get; private set; }
	
	#endregion
	
	private ResourceManager manager;
	
	private Action<IAsset<T>> onComplete;
	
	private int referenceCount;
	
	private AssetBundle assetBundle;
	
	public AssetBundle AssetBundle { get { return assetBundle; } }
	
	public bool IsComplete { get; private set; }
	
	public bool IsLoading { get; private set; }
	
	public StaticAsset2(ResourceManager manager)
	{
		this.manager = manager;
	}
	
	public void Load(string path)
	{
		Path = path;
		manager.StartCoroutine(LoadRoutine(path));
	}
	
	public void IncreaseReferenceCount() 
	{ 
		++referenceCount; 
		LastUseTime = DateTime.Now;
	}
	
	public void DecreaseReferenceCount() { --referenceCount; }
	
	public void Release() 
	{  
		if(assetBundle != null) assetBundle.Unload(true);
		assetBundle = null;
	}


	private WWW Load(string path, string ext)
	{
		var file = "file://" + Application.dataPath + "/__patch/" + path + ext;
		Debug.Log(string.Format("load file, path={0}", file));
		return new WWW(file);
	}

	IEnumerator LoadTexture(string path)
	{
		var www = Load (path, ".png");

		yield return www;

		if(string.IsNullOrEmpty(www.error))
			Content = www.texture as T;
		else
			Debug.LogWarning(string.Format("texture load failed, path={0}, error={1}", path, www.error));
	}

	IEnumerator LoadSound(string path)
	{
		var www = Load (path, ".mp3");
		
		yield return www;

		if(string.IsNullOrEmpty(www.error))
			Content = www.audioClip as T;
		else
			Debug.LogWarning(string.Format("sound load failed, path={0}, error={1}", path, www.error));
	}

	private IEnumerator LoadRoutine(string path)
	{
		if(!path.EndsWith("/null"))
		{
			if(ResourceManager.Instance.IsEmbedded(path)) 
			{
				Content = Resources.Load<T>(path);
			}
			else
			{
				if(typeof(T) == typeof(Texture2D))
				   yield return manager.StartCoroutine(LoadTexture(path));
				else if(typeof(T) == typeof(AudioClip))
					yield return manager.StartCoroutine(LoadSound(path));
			}
		}
		
		IsComplete = true;
	}
}

class DynamicAsset<T> : IAsset<T>
	where T : UnityEngine.Object
{
	#region IAsset implementation
	
	public UnityEngine.Object Rawdata { get { return assetBundle != null ? assetBundle.mainAsset : null; } }
	
	public T Content { get; private set; }
	
	public string Path { get; private set; }

	public int ReferenceCount { get { return referenceCount; } }

	public DateTime LastUseTime { get; private set; }

	#endregion
	
	private ResourceManager manager;
	
	private Action<IAsset<T>> onComplete;
	
	private int referenceCount;

	private AssetBundle assetBundle;

	public AssetBundle AssetBundle { get { return assetBundle; } }
	
	public bool IsComplete { get; private set; }
	
	public bool IsLoading { get; private set; }

	public DynamicAsset(ResourceManager manager)
	{
		this.manager = manager;
	}
	
	public void Load(string path)
	{
		Path = path;
		manager.StartCoroutine(LoadRoutine(path));
	}
	
	public void IncreaseReferenceCount() 
	{ 
		++referenceCount; 
		LastUseTime = DateTime.Now;
	}
	
	public void DecreaseReferenceCount() { --referenceCount; }

	public void Release() 
	{  
		if(assetBundle != null) assetBundle.Unload(true);
		assetBundle = null;
	}

	private IEnumerator LoadRoutine(string path)
	{
		if(!path.EndsWith("/null"))
		{
			IsLoading = true;
			var downloader = new RealTimePatchDownloader();

			downloader.BeginDownload(path);

			while(!downloader.IsDone) yield return new WaitForEndOfFrame();

			//yield return new WaitForSeconds((float)rand.NextDouble() * 3f + 1f);

			if(downloader.AssetBundle != null)
			{
				assetBundle = downloader.AssetBundle;

				if(typeof(T) == typeof(AssetBundle))
					Content = assetBundle as T;
				else
					Content =  assetBundle.mainAsset as T;
				
				if(!Content)
				{
					var contentType = typeof(T);
					if(Mikan.TypeUtils.IsSubClassOf<Component>(contentType))
					{
						Debug.Log(Rawdata);
						Content = (Rawdata as GameObject).GetComponent(contentType) as T;
					}
					
					if(!Content) GameSystem.LogWarning("######## asset {0} can not convert to {1}, path = {2}", Rawdata, typeof(T).Name, path);
				}
			}
			else
			{
				GameSystem.LogWarning("######## load asset failed, path = {0}", path);
			}

			downloader.EndDownload();

			IsLoading = false;
		}
		
		IsComplete = true;
	}
}

class NetTextureAsset : IAsset<Texture2D>
{
	#region IAsset implementation

	public UnityEngine.Object Rawdata {
		get {
			throw new NotImplementedException ();
		}
	}

	public Texture2D Content { get; private set; }
	
	public string Path { get; private set; }
	
	public int ReferenceCount { get { return referenceCount; } }
	
	public DateTime LastUseTime { get; private set; }
	
	#endregion
	
	private ResourceManager manager;
	
	private Action<IAsset<Texture2D>> onComplete;
	
	private int referenceCount;
	
	public bool IsComplete { get; private set; }
	
	public bool IsLoading { get; private set; }

	public NetTextureAsset(ResourceManager manager)
	{
		this.manager = manager;
	}
	
	public void Load(string path)
	{
		Path = path;
		manager.StartCoroutine(LoadRoutine(path));
	}
	
	public void IncreaseReferenceCount() 
	{ 
		++referenceCount; 
		LastUseTime = DateTime.Now;
	}
	
	public void DecreaseReferenceCount() { --referenceCount; }
	
	public void Release() { }

	private IEnumerator LoadRoutine(string path)
	{
		var www = new WWW(path);

		yield return www;

		if(string.IsNullOrEmpty(www.error))
			Content = www.texture;
		else
			GameSystem.LogWarning("######## load asset failed, path = {0}, error = {1}", path, www.error);

		www.Dispose();
		
		IsComplete = true;
	}
}
#endregion

class RealTimePatchDownloader
{
	private string path;
	private string fullname;
	private bool isExists;
	private WWW www;

	public void BeginDownload(string path)
	{
		fullname = string.Format("{0}/{1}.unity", PatchManager.Instance.CacheRoot, path);

		PatchManager.Instance.Download(path, OnComplete);
	}

	private void OnComplete()
	{	
#if UNITY_EDITOR_WIN
		www = new WWW("file://C://" + fullname);
#else
		www = new WWW("file://" + fullname);
#endif
	}

	public bool IsDone 
	{
		get
		{
			if(www != null) return www.isDone;
			return false;
		}

	}


	public AssetBundle AssetBundle 
	{
		get
		{
			if(www != null && string.IsNullOrEmpty(www.error)) return www.assetBundle;

			return null;
		}
	}

	public void EndDownload()
	{
		if(www != null) www.Dispose();
	}
}