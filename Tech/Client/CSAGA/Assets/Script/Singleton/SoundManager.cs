﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class SoundManager : Singleton<SoundManager> {

	private Dictionary<SoundID, SoundPlayer> list = new Dictionary<SoundID, SoundPlayer>();

	private SoundPlayer bgm;

	void Awake()
	{
		gameObject.AddComponent<AudioListener>();

		Message.AddListener(MessageID.PlaySound, OnPlaySound);
		Message.AddListener(MessageID.PlayMusic, OnPlayMusic);
		Message.AddListener(MessageID.SoundVolume, OnSoundVolume);
		Message.AddListener(MessageID.MusicVolume, OnMusicVolume);

		RegisterSound(SoundID.CLICK, "se_click");
		RegisterSound(SoundID.BACK, "se_back");
		RegisterSound(SoundID.CONFIRM, "se_click");
		RegisterSound(SoundID.CANCEL, "se_back");
		RegisterSound(SoundID.SWITCH, "se_switch_01");
		RegisterSound(SoundID.CLICK2, "se_click2");

		RegisterSound(SoundID.BATTLE_ITEM_UPGRADE, "se_switch_03");
		RegisterSound(SoundID.BATTLE_ITEM_SWAP, "se_switch_02");
		RegisterSound(SoundID.BATTLE_ITEM_CHANGE, "se_change");
		RegisterSound(SoundID.BATTLE_HIT, "se_hit");
		RegisterSound(SoundID.BATTLE_HEAL, "se_heal");
		RegisterSound(SoundID.BATTLE_BUFF, "se_buff");
		RegisterSound(SoundID.BATTLE_DEBUFF, "se_debuff");
		RegisterSound(SoundID.BATTLE_CUTIN, "se_cutin");
		RegisterSound(SoundID.BATTLE_WIN, "se_win");
		RegisterSound(SoundID.BATTLE_LOSE, "se_lose");
		RegisterSound(SoundID.BATTLE_WARNING, "se_alert");

		RegisterSound(SoundID.CARD_OPEN, "se_cardopen");
		RegisterSound(SoundID.CARD_RIGHT, "se_correct");
		RegisterSound(SoundID.CARD_WRONG, "se_wrong");

		RegisterSound(SoundID.LOVE, "se_kizuna");
		RegisterSound(SoundID.POWERUP, "se_powerup");

		RegisterSound(SoundID.GACHA_DROP, "se_drop");
		RegisterSound(SoundID.GACHA_DROP2, "se_drop2");
		RegisterSound(SoundID.GACHA_LIGHT, "se_light");

		RegisterSound(SoundID.SOUL, "se_kizuna");

		RegisterSound(SoundID.QTE_CRASH, "se_crash");

		bgm = new SoundPlayer(true);
		bgm.Volume = Context.Instance.SystemSetting.MusicVolume;
	}

	private void RegisterSound(SoundID id, string path)
	{
		var sound = new SoundPlayer(false);
		sound.path = path;
		sound.Volume = Context.Instance.SystemSetting.SoundVolume;
		list.Add(id, sound);
	}

	private void OnPlaySound(IMessage msg)
	{
		if(msg.Data is Sound)
			Play ((Sound)msg.Data);
		else
			Play (new Sound{ ID = (SoundID)msg.Data, Pitch = 1f });
	}

	private void OnPlayMusic(IMessage msg)
	{
		var path = "BGM/" + (string)msg.Data;
		if(bgm.path != path) bgm.Volume = Context.Instance.SystemSetting.MusicVolume;

		bgm.Play(path);
	}

	private void OnSoundVolume(IMessage msg)
	{
		var volume = (float)msg.Data;

		foreach(var item in list)
		{
			item.Value.Volume = volume;
		}
	}

	private void OnMusicVolume(IMessage msg)
	{
		bgm.Volume = (float)msg.Data;
	}

	private void Play(Sound sound)
	{
		var player = default(SoundPlayer);
		if(list.TryGetValue(sound.ID, out player)) player.Play(sound.Pitch);
	}

}

public class Sound
{
	public SoundID ID;
	public float Pitch = 1f;
}


public enum SoundID
{
	CLICK = 1,
	BACK,
	CONFIRM,
	CANCEL,
	SWITCH,
	CLICK2,

	BATTLE_ITEM_UPGRADE = 100,
	BATTLE_ITEM_SWAP,
	BATTLE_ITEM_CHANGE,
	BATTLE_HIT,
	BATTLE_HEAL,
	BATTLE_BUFF,
	BATTLE_DEBUFF,
	BATTLE_CUTIN,
	BATTLE_WIN,
	BATTLE_LOSE,
	BATTLE_WARNING,

	CARD_OPEN,
	CARD_RIGHT,
	CARD_WRONG,

	LOVE,
	POWERUP,

	GACHA_DROP,
	GACHA_LIGHT,

	SOUL,

	GACHA_DROP2,
	QTE_CRASH,
}

public interface ISoundInstance
{
	float Volume { get; set; }
	void Play(AudioClip clip);
	void Play(AudioClip clip, float pitch);
	void Stop();
}

public class SoundInstance : ISoundInstance
{
	private AudioSource source;

	private DateTime timestamp = DateTime.MinValue;

	private float volume;

	public SoundInstance()
	{
		source = NGUITools.AddChild<AudioSource>(SoundManager.Instance.gameObject);
	}
	#region ISoundInstance implementation

	public float Volume
	{
		get { return volume; }
		set
		{
			volume = value;
			
			source.volume = value;
		}
	}
	public void Play(AudioClip clip, float pitch)
	{
		if(clip == null || DateTime.Now.Subtract(timestamp).TotalSeconds < 0.1) return;
		timestamp = DateTime.Now;
		source.name = "SOUND - " + clip.name;
		source.pitch = pitch;
		source.PlayOneShot(clip, volume);
	}
	public void Play (AudioClip clip)
	{
		Play (clip, 1f);
	}

	public void Stop()
	{
		source.Stop();
	}
	#endregion
}

public class MusicInstance : ISoundInstance
{
	const float FADE_IN_SCALE = 0.3f;
	const float FADE_OUT_SCALE = 0.7f;
	private AudioSource source;

	private float volume;

	private bool isPlaying = false;

	private int fadingCount = 0;
	
	public MusicInstance()
	{
		source = NGUITools.AddChild<AudioSource>(SoundManager.Instance.gameObject);
		source.loop = true;
		source.volume = 0;
	}

	#region ISoundInstance implementation
	public float Volume {
		get { return volume; }
		set {
			volume = value;
			if(source.isPlaying && fadingCount <= 0) source.volume = value;
		}
	}

	public void Play(AudioClip clip, float pitch)
	{
		Play(clip);
	}

	public void Play (AudioClip clip)
	{
		isPlaying = true;

		if(source.isPlaying) source.Stop();

		if(clip != null)
		{
			source.name = "MUSIC - " + clip.name;
			source.clip = clip;
			source.Play();
			Coroutine.Start(FadeIn());
		}
	}
	public void Stop ()
	{
		if(!isPlaying) return;
		isPlaying = false;

		if(source.isPlaying)
			Coroutine.Start(FadeOut());
	}

	#endregion

	private IEnumerator FadeIn()
	{
		++fadingCount;

		source.volume = 0f;

		while(isPlaying)
		{
			var delta = Time.deltaTime * FADE_IN_SCALE * volume;
			if(Mathf.Abs(volume - source.volume) > delta)
			{
				if(source.volume < volume)
					source.volume += delta;
				else
					source.volume -= delta;
			}
			else
			{
				source.volume = volume;
				break;
			}

			yield return new WaitForFixedUpdate();
		}

		--fadingCount;
	}

	private IEnumerator FadeOut()
	{
		++fadingCount;

		var originVolumn = source.volume;

		while(!isPlaying)
		{
			var delta = Time.deltaTime * FADE_OUT_SCALE * originVolumn;
			if(source.volume > delta) 
				source.volume -= delta;
			else
			{
				source.Stop();
				source.clip = null;
				break;
			}
			
			yield return new WaitForFixedUpdate();
		}

		--fadingCount;
	}
}

public class SoundPlayer
{
	public string path;
	public IAsset<AudioClip> asset;

	private ISoundInstance instance;

	private ISoundInstance back;

	private bool loop;

	private float volume = 1f;

	private float pitch = 1f;

	public SoundPlayer(bool loop)
	{
		this.loop = loop;

	}
	public float Volume
	{
		get{ return volume; }
		set
		{
			volume = value;

			if(instance != null) instance.Volume = value;
		}
	}

	public void Play(string path)
	{
		if(this.path == path) return;

		this.path = path;

		if(instance != null)
		{
			instance.Stop();
		}

		if(asset != null && asset.Path != path)
		{
			asset.DecreaseReferenceCount();
			asset = null;
		}

		Play ();
	}

	public void Play()
	{
		Play (pitch);
	}

	public void Play(float pitch)
	{
		this.pitch = pitch;

		if(asset == null)
		{
			ResourceManager.Instance.LoadSound(path, OnLoadComplete);
		}
		else if(asset.IsComplete)
		{
			if(loop)
			{
				var tmp = instance;

				instance = back;

				back = tmp;

				if(back != null) back.Stop();

			}

			if(instance == null) 
			{
				if(loop)
					instance = new MusicInstance();
				else
					instance = new SoundInstance();

				instance.Volume = volume;
			}
			
			instance.Play(asset.Content, pitch);
			
		}
	}

	private void OnLoadComplete(IAsset<AudioClip> asset)
	{
		this.asset = asset;
		Play ();
	}
}