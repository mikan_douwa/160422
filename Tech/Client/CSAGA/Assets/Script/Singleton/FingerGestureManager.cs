﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//====================================================
// function Update To Check Gesture And Fire Event througth GameEventManager
// and Define Event on FingerGestureEvent file
//
// by Joy
//====================================================
public class FingerGestureManager : Singleton<FingerGestureManager> {

	public float longPressDelay = 0.5f;

	private TouchPhase m_F0LastPhase;
	private TouchPhase m_F1LastPhase;
	
	private Vector2 m_F0LastPosition;
	private Vector2 m_F1LastPosition;
	
	private float m_F0StartTime;
	private float m_LastF0F1Dist;
	
	private Vector2 m_F0StartPosition;
	private Vector2 m_F1StartPosition;

	private bool isLongPressFired = false;
	
	private bool IsEnabled = true;
	
	private HashSet<int> blockers = new HashSet<int>();
	
	private int topLayer = 0;

	private Camera mainCamera;
	private GameObject rootObj;

	public int TopLayer{ get{ return topLayer; } }

	public void Awake()
	{
		Message.AddListener(MessageID.EnableGesture, OnEnableGesture);
		Message.AddListener(MessageID.EnableGesture, OnDisableGesture);
	}

	private void OnEnableGesture()
	{
		IsEnabled = true;
	}
	
	private void OnDisableGesture()
	{
		IsEnabled = false;
	}

	public void OnBlockerActive(GameObject go, bool active)
	{
		if(active)
		{
			blockers.Add(go.layer);
			if(go.layer > topLayer) topLayer = go.layer;
		}
		else
		{
			blockers.Remove(go.layer);

			if(go.layer == topLayer)
			{
				topLayer = 0;
				foreach(var item in blockers) 
				{
					if(item > topLayer) topLayer = item;
				}
			}
		}
	}

	// Update is called once per frame
	public void Update () {
		if(!IsEnabled)
		{
			return;
		}
		#if UNITY_EDITOR || UNITY_STANDALONE_OSX || UNITY_STANDALONE_WIN || UNITY_WEBPLAYER

		/*
		if(mainCamera == null) 
		{
			mainCamera = FindObjectOfType<Camera>();
			if(mainCamera == null) return;
		}

		if(rootObj == null) 
		{
			rootObj = FindObjectOfType<UIRoot>().gameObject;
			if(rootObj == null) return;
		}

		var pos = Input.mousePosition;
		pos.z = 10f;
		pos = mainCamera.ScreenToWorldPoint(pos);

		var lastHit = rootObj.transform.InverseTransformPoint(pos);
		*/
		var lastHit = Input.mousePosition;

		if (Input.GetMouseButtonDown (0)) {
			//TAP Event
			FireOnTap(lastHit);
			m_F0StartTime = Time.time;
			m_F0StartPosition = m_F0LastPosition = lastHit;
			isLongPressFired = false;
			FireOnDragBegin(m_F0StartPosition, lastHit);
		}
		if (Input.GetMouseButton (0)) {
			if(m_F0LastPosition.x != lastHit.x || m_F0LastPosition.y != lastHit.y)
			{
				isLongPressFired = true;

				m_F0LastPosition = lastHit;
				
				FireOnDragMoved(m_F0StartPosition, m_F0LastPosition);
			}

			if(!isLongPressFired && (Time.time - m_F0StartTime) > longPressDelay)
			{
				FireOnLongPress(m_F0LastPosition);
				isLongPressFired = true;
			}

		}
		if (Input.GetMouseButtonUp (0)) {

			float distance = Vector2.Distance(m_F0LastPosition, m_F0StartPosition);

			if(distance != 0)
			{
				Vector2 deltaDistance = m_F0LastPosition - m_F0StartPosition;
				float deltaTime = Time.time - m_F0StartTime;
				float velocity = distance / deltaTime;

				FireOnSwip(m_F0StartPosition, deltaDistance, velocity);
			}

			FireOnDragEnd(m_F0StartPosition, lastHit);
		}
		#elif UNITY_IPHONE || UNITY_ANDROID
		int touchCount = Input.touchCount;
		if(touchCount == 1)
		{
			//TAP Event
			Touch f0 = Input.GetTouch(0);
			
			//Swip Event
			if(f0.phase == TouchPhase.Began)
			{
				if(f0.tapCount >= 1)
				{
					FireOnTap(f0.position);
					if(f0.tapCount == 2)
						FireOnDoubleTap(f0.position);
				}
				
				m_F0StartTime = Time.time;
				m_F0StartPosition = f0.position;
				
				FireOnDragBegin(m_F0StartPosition, m_F0StartPosition);
			}
			else if(f0.phase == TouchPhase.Ended)
			{
				Vector2 deltaDistance = f0.position - m_F0StartPosition;
				float distance = Vector2.Distance(f0.position, m_F0StartPosition);
				float deltaTime = Time.time - m_F0StartTime;
				float velocity = distance / deltaTime;

				FireOnSwip(m_F0StartPosition, deltaDistance, velocity);
				
				FireOnDragEnd(m_F0StartPosition, f0.position);
			}
			else if(f0.phase == TouchPhase.Moved)
			{
				FireOnDragMoved(m_F0StartPosition, f0.position);    
			}
			else if(f0.phase == TouchPhase.Stationary)
			{
				if((m_F0LastPhase == TouchPhase.Stationary ) /*&& 
                    (m_CurrentTouchEventState == TouchEventState.Check || m_CurrentTouchEventState == TouchEventState.OneLongPress)*/)
				{
					FireOnLongPress(f0.position);
				}
			}
			
			m_F0LastPosition = f0.position;
			m_F0LastPhase = f0.phase;
			
		}
		else if(touchCount == 2)
		{
			Touch f0 = Input.GetTouch (0);
			Touch f1 = Input.GetTouch (1);
			Vector2 f0Dir = f0.deltaPosition.normalized;
			Vector2 f1Dir = f1.deltaPosition.normalized;
			
			float f0f1Dist = Vector3.Distance(f0.position, f1.position);
			// if both fingers moving
			if (f0.phase == TouchPhase.Began || f1.phase == TouchPhase.Began) 
			{
				m_F0StartPosition = f0.position;
				m_F1StartPosition = f1.position;
				
				FireOnTwoFingerDragBegin(m_F0StartPosition, m_F0StartPosition, m_F1StartPosition, m_F1StartPosition);
			}
			else if(f0.phase == TouchPhase.Moved && f1.phase == TouchPhase.Moved) 
			{
				// dot product of directions
				float dot = Vector3.Dot(f0Dir, f1Dir);
				float pinchDelta = f0f1Dist - m_LastF0F1Dist;
				
				// check is opposite directions
				if(dot < 0)
				{
					if(pinchDelta != 0)
					{
						
						FireOnPinchBegin(f0.position-f0.deltaPosition, f1.position - f1.deltaPosition);
					}
					
					FirePinchMoved(f0.position, f1.position);
					
					//rotation
					float angle = computeAngle(f0.position, f1.position, f0.position-f0.deltaPosition, f1.position - f1.deltaPosition);
					FireOnRotationMoved(f0.position, f1.position, angle);
				}
				
				FireOnTwoFingerDragMoved(m_F0StartPosition, f0.position, m_F1StartPosition, f1.position);
			}
			else if(f0.phase == TouchPhase.Ended || f1.phase == TouchPhase.Ended)
			{
				FirePinchEnd(f0.position, f1.position);    
				
				float angle = computeAngle(f0.position, f1.position, m_F0StartPosition, m_F1StartPosition);
				FireOnRotationEnd(f0.position, f1.position, angle);
				
				FireOnTwoFingerDragEnd(m_F0StartPosition, f0.position, m_F1StartPosition, f1.position);
			}
			
			m_LastF0F1Dist = f0f1Dist;
			m_F0LastPhase = f0.phase;
			m_F1LastPhase = f1.phase;
			
			m_F0LastPosition = f0.position;
			m_F1LastPosition = f1.position;
		}
		#endif
	}
	
	private float computeAngle(Vector2 f0Pos, Vector2 f1Pos, Vector2 f0LastPos, Vector2 f1LastPos)
	{
		//rotation
		Vector2 lastCenter = (f0LastPos + f1LastPos)/2;
		Vector2 currentCenter = (f0Pos + f1Pos)/2;
		Vector3 f0Cross = Vector3.Cross(f0LastPos - lastCenter, f0Pos - currentCenter);
		Vector3 f1Cross = Vector3.Cross(f1LastPos - lastCenter, f1Pos - currentCenter);
		
		float crossZ = f0Cross.z;
		Vector2 lastPosition = f0LastPos;
		Vector2 position = f0Pos;
		if(f1Cross.z  > f0Cross.z)
		{
			crossZ = f1Cross.z;        
			lastPosition = f1LastPos;
			position = f1Pos;
		}
		float angle = Vector3.Angle(lastPosition - lastCenter, position- currentCenter);
		
		if(crossZ > 0)
			angle = -angle;
		
		return angle;
	}
	
	#region FireEvent
	
	
	
	private void FireOnTap(Vector2 pos)
	{
		var data = new FingerGestureData();
		data.SetupFinger(0, pos);
		
		Message.Send(MessageID.GestureTap, data);
	}
	
	private void FireOnDoubleTap(Vector2 pos)
	{
		var data = new FingerGestureData();
		data.SetupFinger(0, pos);
		
		//GameSystem.SendMessage(MessageID.GestureDoubleTap, data);
	}
	
	private void FireOnSwip(Vector2 startPos, Vector2 distance, float velocity)
	{
		var data = new FingerGestureData();
		data.SetupFinger(0, startPos);
		data.SwipDistance = distance;
		data.SwipDirection = distance.normalized;
		data.SwipVelocity = velocity;
		
		Message.Send(MessageID.GestureSwip, data);
	}
	
	private void FireOnLongPress(Vector2 pos)    
	{
		var data = new FingerGestureData();
		data.SetupFinger(0, pos);
		
		Message.Send(MessageID.GestureLongPress, data);
	}
	
	private void FireOnPinchBegin(Vector2 finger0Pos, Vector2 finger1Pos)
	{
		//GameEventManager.DispatchEvent(new PinchGestureEvent(PinchGestureEvent.PINCH_BEGIN_GESTURE_EVENT, finger0Pos, finger1Pos ));
	}
	
	private void FirePinchMoved(Vector2 finger0Pos, Vector2 finger1Pos)
	{
		//GameEventManager.DispatchEvent(new PinchGestureEvent(PinchGestureEvent.PINCH_MOVED_GESTURE_EVENT, finger0Pos, finger1Pos ));
	}
	
	private void FirePinchEnd(Vector2 finger0Pos, Vector2 finger1Pos)
	{
		//GameEventManager.DispatchEvent(new PinchGestureEvent(PinchGestureEvent.PINCH_END_GESTURE_EVENT, finger0Pos, finger1Pos ));
	}
	
	private void FireOnDragBegin(Vector2 startPosition, Vector2 position)
	{
		var data = new FingerGestureData();
		data.SetupFinger(0, startPosition, position);
		
		Message.Send(MessageID.GestureDragBegin, data);
	}
	
	private void FireOnDragMoved(Vector2 startPosition, Vector2 position)
	{
		var data = new FingerGestureData();
		data.SetupFinger(0, startPosition, position);
		
		Message.Send(MessageID.GestureDragMoved, data);
	}
	
	private void FireOnDragEnd(Vector2 startPosition, Vector2 position)
	{
		var data = new FingerGestureData();
		data.SetupFinger(0, startPosition, position);
		
		Message.Send(MessageID.GestureDragEnd, data);
	}
	
	private void FireOnTwoFingerDragBegin(Vector2 startPosition, Vector2 position, Vector2 startPosition2, Vector2 position2)
	{
		//GameEventManager.DispatchEvent(new DragGestureEvent(DragGestureEvent.TWO_FINGER_DRAG_BEGIN_GESTURE_EVENT, startPosition, position, startPosition2, position2 ));
	}
	
	private void FireOnTwoFingerDragMoved(Vector2 startPosition, Vector2 position, Vector2 startPosition2, Vector2 position2)
	{
		//GameEventManager.DispatchEvent(new DragGestureEvent(DragGestureEvent.TWO_FINGER_DRAG_MOVED_GESTURE_EVENT, startPosition, position, startPosition2, position2 ));
	}
	
	private void FireOnTwoFingerDragEnd(Vector2 startPosition, Vector2 position, Vector2 startPosition2, Vector2 position2)
	{
		//GameEventManager.DispatchEvent(new DragGestureEvent(DragGestureEvent.TWO_FINGER_DRAG_END_GESTURE_EVENT, startPosition, position, startPosition2, position2 ));
	}
	
	private void FireOnRotationBegin(Vector2 f0Position , Vector2 f1Position)
	{
		//GameEventManager.DispatchEvent(new RotationBeginGestureEvent(RotationBeginGestureEvent.ROTATION_BEGIN_GESTURE_EVENT, f0Position, f1Position));
	}
	private void FireOnRotationMoved(Vector2 f0Position , Vector2 f1Position, float deltaAngle)
	{
		//GameEventManager.DispatchEvent(new RotationMovedGestureEvent(RotationMovedGestureEvent.ROTATION_MOVED_GESTURE_EVENT, f0Position, f1Position, deltaAngle));
	}
	
	private void FireOnRotationEnd(Vector2 f0Position , Vector2 f1Position, float totalAngle)
	{
		//GameEventManager.DispatchEvent(new RotationEndGestureEvent(RotationEndGestureEvent.ROTATION_END_GESTURE_EVENT, f0Position, f1Position, totalAngle));
	}
	#endregion
	
	/*
	private void OnEnableEvent(GameEvent evt)
	{
		if(evt.name == EnabledGestureEvent.ENABLE_GESTURE_EVENT)
		{
			IsEnabeld = true;
		}
		else if(evt.name == EnabledGestureEvent.DISABLE_GESTURE_EVENT)
		{
			IsEnabeld = false;
		}
	}
	*/
}

public class FingerGestureData
{
	public class Finger
	{
		public Vector2 StartPosition;
		public Vector2 EndPosition;
	}
	
	public Finger Finger0 { get { return fingers[0]; } }
	public Finger Finger1 { get { return fingers[1]; } }
	
	public Vector2 SwipDirection;
	public Vector2 SwipDistance;
	public float SwipVelocity;
	
	private Finger[] fingers = new Finger[2];
	
	public void SetupFinger(int index, Vector2 pos)
	{
		fingers[index] = new Finger{ StartPosition = pos };
	}
	public void SetupFinger(int index, Vector2 startPos, Vector2 endPos)
	{
		fingers[index] = new Finger{ StartPosition = startPos, EndPosition = endPos };
	}
}