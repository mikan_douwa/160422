﻿using UnityEngine;
using System.Collections.Generic;
using System;
using System.Collections;

public class UIManager : Singleton<UIManager> {

	private Dictionary<int, GameObject> roots;

	private GameObject connecting;

	private bool isConnectingVisible;

	private float timeEscapeDown = 0;

	public Camera GetCamera(int layer)
	{
		return CameraList.Instance.Get(layer);
	}

	public Camera GetCamera(UILayer layer)
	{
		return GetCamera((int)layer);
	}

	public void AddCamera(int layer)
	{
		if(GetCamera(layer) == null) CameraList.Instance.Add(layer);
	}

	public GameObject GetRootObj(UILayer layer)
	{
		return GetRootObj((int)layer);
	}
	public GameObject GetRootObj(int layer)
	{
		if(roots == null) FindCurrentRoots();

		GameObject go = null;

		if(!roots.TryGetValue(layer, out go)) go = CreateRoot(layer);

		return go.GetComponentInChildren<UIRoot>().gameObject;
	}

	public void DestroyRoot(int layer)
	{
		if(roots == null) FindCurrentRoots();

		GameObject go = null;
		
		if(!roots.TryGetValue(layer, out go)) return;

		roots.Remove(layer);

		Destroy(go);
	}

	public void AddToRoot(GameObject go)
	{
		var parent = GetRootObj(go.layer);

		SetIdentity(parent, go);
	}
	
	public GameObject AddChild(GameObject parent, GameObject prefab)
	{
		var go = GameObject.Instantiate(prefab) as GameObject;
		
		if(parent == null) parent = GetRootObj(go.layer);
		
		SetIdentity(parent, go);
		
		return go;
	}

	public T AddToRoot<T>(int layer)
		where T : MonoBehaviour
	{
		var parent = GetRootObj(layer);
		return AddChild<T>(parent);
	}

	public T AddChild<T>(GameObject parent)
		where T : MonoBehaviour
	{
		var go = new GameObject();
		go.name = typeof(T).Name;
		SetIdentity(parent, go);
		return go.AddComponent<T>();
	}

	public void SetConnectingVisible(bool isVisible)
	{
		isConnectingVisible = isVisible;

		StartCoroutine(DelaySetConnectingVisible(isVisible));
	}

	private IEnumerator DelaySetConnectingVisible(bool isVisible)
	{
		yield return new WaitForEndOfFrame();

		if(isVisible != isConnectingVisible) yield break;

		if(isVisible && connecting == null)
		{
			ResourceManager.Instance.CreatePrefabInstance("Common/Connecting", OnConnectingCreated);
		}
		else if(!isVisible && connecting != null)
		{
			NGUITools.Destroy(connecting);
			connecting = null;
		}
	}

	private void OnConnectingCreated(GameObject go)
	{
		if(isConnectingVisible)
			connecting = go;
		else
			NGUITools.Destroy(go);
	}

	public void SetIdentity(GameObject parent, GameObject go)
	{
		SetIdentity(parent, go, go.layer == (int)UILayer.Popup);
	}

	public void SetIdentity(GameObject parent, GameObject go, bool isPopup)
	{
		UIUtils.SetIdentity(parent, go);
		if(isPopup && NGUITools.FindInParents<Popup>(go) == null) go.AddMissingComponent<Popup>();
	}

	void Start()
	{
		Message.AddListener(MessageID.ShowMessageBox, OnShowMessageBox);
		Message.AddListener(MessageID.DownloadBegin, OnDownloadBegin);

		Message.AddListener(MessageID.SystemSetting, OnSystemSetting);
		Message.AddListener(MessageID.BattleSetting, OnBattleSetting);
	}

	void Update()
	{
		UpdateDebugUI();

		if(Time.time - timeEscapeDown > 0.3f)
		{
			if(Input.GetKeyDown(KeyCode.Escape))
			{
				timeEscapeDown = Time.time;
				Message.Send(MessageID.PhysicalBack);
			}
		}
	}

	void OnLevelWasLoaded(int level)
	{
		if(level == 0) return;

		Popup.Clear();
		FindCurrentRoots();

		StartCoroutine(DelayLoadEssential());

		Message.Send(MessageID.NextScene);
	}

	IEnumerator DelayLoadEssential()
	{
//#if UNITY_STANDALONE
		if(Application.loadedLevelName != SceneName.APPSTARTUP)
			ResourceManager.Instance.CreatePrefabInstance("Common/ScreenMask");
//#endif

		yield return new WaitForEndOfFrame();

		if(Application.loadedLevelName == SceneName.GAME)
		{
			ResourceManager.Instance.CreatePrefabInstance("GameScene/Background");
			ResourceManager.Instance.CreatePrefabInstance("GameScene/Banner");
			//Menu.Show();
		}

#if DEBUG
		ResourceManager.Instance.CreatePrefabInstance("Debug/DebugTray");
#endif
	}
	
	private void OnShowMessageBox(IMessage msg)
	{
		if(msg.Data is MessageBoxContext)
			MessageBox.Show((MessageBoxContext)msg.Data);
		else if(msg.Data is String)
			MessageBox.Show((string)msg.Data);
	}

	private void OnDownloadBegin()
	{
		if(FindObjectOfType<DownloadProgress>() == null)
			ResourceManager.Instance.CreatePrefabInstance("Common/DownloadProgress");
	}

	private void OnSystemSetting()
	{
		var msgBox = MessageBox.Show("Setting/SystemSetting", "");
		msgBox.enableCloseByClick = true;
	}

	private void OnBattleSetting()
	{
		MessageBox.Show("Setting/BattleSetting", "", SystemTextID.BTN_CANCEL);
	}

	private void FindCurrentRoots()
	{
		roots = new Dictionary<int, GameObject>();

		foreach(var item in FindObjectsOfType<UIRoot>())
		{
			roots.Add(item.gameObject.layer, item.gameObject);
		}
	}

	private GameObject CreateRoot(int layer)
	{
		var go = (GameObject)GameObject.Instantiate(Essentials.Instance.root);

		go.name = string.Format("UI Root({0:00}|{1})", layer, (UILayer)layer);
		go.layer = layer;
		/*
		var root = go.AddComponent<UIRoot>();

		root.fitWidth = true;
		root.fitHeight = false;
		root.manualWidth = 640;
		root.manualHeight = 1136;
		root.minimumHeight = 1136;
		root.maximumHeight = 1500;

#if UNITY_STANDALONE
		root.scalingStyle = UIRoot.Scaling.Flexible;
#else
		root.scalingStyle = UIRoot.Scaling.ConstrainedOnMobiles;
		go.AddComponent<UIRootMobile>();
#endif

		var panel = go.AddComponent<UIPanel>();
		*/
		roots.Add(layer, go);

		CameraList.Instance.Add(layer);

		return go;
	}


	#region DebugUI

	private Vector3 mousePos;
	private DateTime mouseDownTime;
	private bool isMouseDown;

	private void UpdateDebugUI () {
		/*
		if(Input.GetMouseButton(0))
		{
			if(!isMouseDown)
			{
				mousePos = Input.mousePosition;
				mouseDownTime = DateTime.Now;
				isMouseDown = true;
			}
		}
		else if(isMouseDown)
		{
			if(DateTime.Now.Subtract(mouseDownTime).TotalSeconds < 0.3)
			{
				var diff = Input.mousePosition - mousePos;
				
				if(diff.x < 60 && diff.x > -60)
				{
					var debugUI = FindObjectOfType<DebugUI>();
					
					if(diff.y < -120 && debugUI == null)
						GameSystem.ResourceManager.CreatePrefabInstance("Test/DebugUI");
					else if(diff.y > 120 && debugUI != null)
						debugUI.Close();
				}
				
			}
			isMouseDown = false;
		}
		*/
	}

	#endregion
}
