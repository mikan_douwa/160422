﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class Context : Singleton<Context> {

	public StringObj PublicID = StringObj.Create<StringObj>("game.public_id", "");

	public int QuestID;
	public int EventScheduleID;
	public bool IsScheduledQuest = false;
	public int InteractiveID;
	public Queue<int> Drops = new Queue<int>();
	public Queue<Mikan.CSAGA.Reward> Rewards = new Queue<Mikan.CSAGA.Reward>();
	public int GiftClaimCount;
	public int TeamIndex;
	public int TeamMemberIndex;
	public int SlotIndex;
	public QuestList MainQuest;
	public QuestItemData Quest;
	public CardData Card;
	public ItemData Item;
	public CardAwakenData CardAwaken;
	public SetupData SetupData;
	public CoMissionItemData CoMission;
	public CoSubjectItemData CoSubject;
	public StageID StageID = StageID.None;
	public List<Mikan.Serial> Cards = new List<Mikan.Serial>();
	public List<object> Selections = new List<object>();
	public int AwakenGroup;
	public GameObject CloseWithStage;
	public SocialMenuData.SOCIAL_ACTION SocialAction;
	public SocialListData SearchPlayerTempData;

	public Int64Obj FavoriteCard = Int64Obj.Create<Int64Obj>("game.favorite_card", 0);

	//public Int32DictinaryObj QuestCache = Int32DictinaryObj.Create<Int32DictinaryObj>("game.quest_cache", null);

	public QuestCacheList QuestCache = new QuestCacheList();

	public SortMethodObj SortCard = SortMethodObj.Create<SortMethodObj>("game.sort_card", SortMethod.Serial);
	public SortMethodObj SortEquipment = SortMethodObj.Create<SortMethodObj>("game.sort_equip", SortMethod.Serial);

	public BattleSetting BattleSetting = BattleSetting.Create<BattleSetting>("game.battle_setting_1");
	public SystemSetting SystemSetting = SystemSetting.Create<SystemSetting>("game.system_setting");

	public Int32ListObj AutoCache = Int32ListObj.Create<Int32ListObj>("game.auto_cache", new List<int>());

	public TeachFlags TeachFlags = TeachFlags.Create<TeachFlags>("game.teach_flags");

	public Int32Obj DailyCounter = Int32Obj.Create<Int32Obj>("game.daily_counter", 0);

	public Option Option = new Option();

	public SortTarget SortTarget;

	public bool RestoreQuestEntrance = false;

	public string NextScene;

	public int TutorialID;

	public int TriggerQuestID;

	public bool IsTutorialComplete = true;

	public bool IsPreloading = true;

	public bool IsCreatingPlayer = false;

	public int ChapterNo = 0;

	public SocialListData CachedFellow;

	public List<int> SocialPurchaseList;

	public List<int> FightPowerList;

	public Mikan.CSAGA.QuestType QuestType = Mikan.CSAGA.QuestType.Regular;

	public int Difficulty = 1;

	public RankingListView.TAG RankingTag = RankingListView.TAG.Top10;

	public bool SwitchByNavigator = false;

	public Mikan.CSAGA.ConstData.Tactics Tactics;

	public int TacticsGroup;

	public FaithItemData Faith;

	public int ChallengeInfoButtonState = 0;

	public PVPWaves PVPWaves;
	public PVPWaveItemData PVPWave;
	public List<PVPPlayerItemData> PVPPlayers;
	public PVPPlayerItemData PVPPlayer;

	public IEnumerable<long> SelectEquipment()
	{
		foreach(var item in Selections)
		{
			var obj = item as EquipmentData;

			if(obj == null || obj.Origin == null || obj.Origin.Serial == 0) continue;

			yield return obj.Origin.Serial;
		}
	}
}

abstract public class LocalCache : Mikan.SerializableObject
{
	private string key;
	
	public static U Create<U>(string key)
		where U : LocalCache, new()
	{
		var obj = new U();
		obj.key = key;
		
		var rawdata = PlayerPrefs.GetString(key);
		if(!string.IsNullOrEmpty(rawdata)) 
			obj.DeSerialize(rawdata);
		
		return obj;
	}
	
	public void Save()
	{
		PlayerPrefs.SetString(key, Serialize());
	}
}

abstract public class LocalCache<T> : LocalCache
{
	private string key;
	protected T val;

	public static U Create<U>(string key, T defaultValue)
		where U : LocalCache<T>, new()
	{
		var obj = Create<U>(key);

		if(obj.IsEmpty)	obj.val = defaultValue;

		return obj;
	}



	virtual public T Value
	{
		get { return val; }

		set 
		{
			if(Equals(val, value)) return;
			val = value;
			Save ();
		}
	}

	virtual protected bool Equals(T lhs, T rhs)
	{
		return lhs.Equals(rhs);
	}
}

public class Int32Obj : LocalCache<int>
{
	#region implemented abstract members of SerializableObject

	protected override void Serialize (Mikan.IOutput output)
	{
		output.Write(Value);
	}

	protected override void DeSerialize (Mikan.IInput input)
	{
		Value = input.ReadInt32();
	}

	#endregion
}

public class StringObj : LocalCache<string>
{
	#region implemented abstract members of SerializableObject
	
	protected override void Serialize (Mikan.IOutput output)
	{
		output.Write(Value);
	}
	
	protected override void DeSerialize (Mikan.IInput input)
	{
		Value = input.ReadString();
	}
	
	#endregion

	protected override bool Equals (string lhs, string rhs)
	{
		return lhs == rhs;
	}
}

public class Int64Obj : LocalCache<long>
{
	#region implemented abstract members of SerializableObject
	
	protected override void Serialize (Mikan.IOutput output)
	{
		output.Write(Value);
	}
	
	protected override void DeSerialize (Mikan.IInput input)
	{
		Value = input.ReadInt64();
	}
	
	#endregion
}

public class Int32ListObj : LocalCache<List<int>>
{

	public bool Contains(int item)
	{
		return Value.Contains(item);
	}

	public void Add(int item)
	{
		Value.Add(item);
		Save ();
	}

	public void Remove(int item)
	{
		if(Value.Remove(item)) Save();
	}
	#region implemented abstract members of SerializableObject

	protected override void Serialize (Mikan.IOutput output)
	{
		output.Write(Value);
	}

	protected override void DeSerialize (Mikan.IInput input)
	{
		Value = input.ReadInt32List();
	}

	#endregion

	protected override bool Equals (List<int> lhs, List<int> rhs)
	{
		return false;
	}
}

public class Int32DictinaryObj : LocalCache<Dictionary<int, int>>
{
	#region implemented abstract members of SerializableObject
	
	protected override void Serialize (Mikan.IOutput output)
	{
		output.Write(Value.Count);
		foreach(var item in Value)
		{
			output.Write(item.Key);
			output.Write(item.Value);
		}
	}
	
	protected override void DeSerialize (Mikan.IInput input)
	{
		var count = input.ReadInt32();
		Value = new Dictionary<int, int>(count);
		for(int i = 0; i < count; ++i)
		{
			var key = input.ReadInt32();
			var val = input.ReadInt32();
			Value.Add(key, val);
		}
	}
	
	#endregion

	public override Dictionary<int, int> Value {
		get {
			return base.Value;
		}
		set {
			val = value;
		}
	}

	public int Get(int chapter)
	{
		if(Value == null) return 0;
		var id = 0;
		if(Value.TryGetValue(chapter, out id)) return id;
		return 0;
	}

	public void Set(int key, int val)
	{
		if(Value == null) Value = new Dictionary<int, int>();
		Value[key] = val;
		Save();
	}
}


public class SortMethodObj : LocalCache<SortMethod>
{
	#region implemented abstract members of SerializableObject
	
	protected override void Serialize (Mikan.IOutput output)
	{
		output.Write((int)Value);
	}
	
	protected override void DeSerialize (Mikan.IInput input)
	{
		Value = (SortMethod)input.ReadInt32();
	}
	
	#endregion
}

public class SystemSetting : LocalCache
{
	public float SoundVolume = 1f;
	public float MusicVolume = 1f;
	
	#region implemented abstract members of SerializableObject
	
	protected override void Serialize (Mikan.IOutput output)
	{
		output.Write(SoundVolume);
		output.Write(MusicVolume);
	}
	
	protected override void DeSerialize (Mikan.IInput input)
	{
		SoundVolume = input.ReadFloat();
		MusicVolume = input.ReadFloat();
	}
	
	#endregion
}

public class BattleSetting : LocalCache
{
	public float Speed = 1f;
	public bool SkillCheck = true;
	public bool Auto = false;
	public bool FXEnabled = true;

	#region implemented abstract members of SerializableObject

	protected override void Serialize (Mikan.IOutput output)
	{
		output.Write(Speed);
		output.Write(SkillCheck);
		output.Write(FXEnabled);
	}

	protected override void DeSerialize (Mikan.IInput input)
	{
		Speed = input.ReadFloat();
		SkillCheck = input.ReadBoolean();
		FXEnabled = input.ReadBoolean();
	}

	#endregion
}

public enum TeachFlag
{
	None = 0,
	CardEnhance = 1,
	CardContact,
	TeamEdit
}

public class TeachFlags : LocalCache
{
	private List<int> list = new List<int>();
	
	public bool this[StageID stageId]
	{
		get
		{
			return list.Contains((int)stageId);
		}
		set
		{
			var val = (int)stageId;
			if(value && !list.Contains(val)) list.Add(val);
			if(!value && list.Contains(val)) list.Remove(val);
			
			Save ();
		}
	}
	#region implemented abstract members of SerializableObject
	
	protected override void Serialize (Mikan.IOutput output)
	{
		output.Write(list);
	}
	
	protected override void DeSerialize (Mikan.IInput input)
	{
		list = input.ReadInt32List();
	}
	
	#endregion
	
	
}

public enum OptionID
{
	Rescue,
}

public class Option
{
	private static List<OptionID> defaults = new List<OptionID>(new OptionID[]{ OptionID.Rescue });

	public bool this[OptionID id]
	{
		get
		{
			var key = ConvertKey(id);

			if(PlayerPrefs.HasKey(key))
				return PlayerPrefs.GetInt(key) == 1;

			return defaults.Contains(id);
		}
		set
		{
			var key = ConvertKey(id);

			PlayerPrefs.SetInt(key, value ? 1 : 0);
		}
	}
	
	private string ConvertKey(OptionID id)
	{
		return "option." + id.ToString();
	}
	
}

public class QuestCache : LocalCache
{
	private Dictionary<int, int> cache;

	public int LastQuestID { get; private set; }
	
	public int Get(int chapter)
	{
		if(cache == null) return 0;
		var id = 0;
		if(cache.TryGetValue(chapter, out id)) return id;
		return 0;
	}
	
	public void Set(int chapter, int val)
	{
		if(cache == null) cache = new Dictionary<int, int>();
		cache[chapter] = val;
		if(chapter > 0) LastQuestID = val;

		Save();
	}

	protected override void Serialize (Mikan.IOutput output)
	{
		output.Write(cache);
		output.Write(LastQuestID);
	}

	protected override void DeSerialize (Mikan.IInput input)
	{
		cache = input.ReadInt32Dictionary();
		LastQuestID = input.ReadInt32();
	}
}
public class QuestCacheList
{
	private static Mikan.CSAGA.QuestType[] TYPE_LIST = new Mikan.CSAGA.QuestType[]{ 
		Mikan.CSAGA.QuestType.Regular, 
		Mikan.CSAGA.QuestType.Extra 
	};

	private Dictionary<int, QuestCache> cacheList;

	public QuestCacheList()
	{
		cacheList = new Dictionary<int, QuestCache>();
		foreach(var type in TYPE_LIST)
		{
			cacheList.Add((int)type, QuestCache.Create<QuestCache>("game.quest_cache." + type.ToString()));
		}
	}

	public int GetLastQuestID(Mikan.CSAGA.QuestType type)
	{
		var cache = Mikan.DictionaryUtils.SafeGetValueNullable(cacheList, (int)type);
		if(cache == null) return 0;
		return cache.LastQuestID;
	}

	public int Get(Mikan.CSAGA.QuestType type, int chapter)
	{
		var cache = Mikan.DictionaryUtils.SafeGetValueNullable(cacheList, (int)type);
		if(cache == null) return 0;
		return cache.Get(chapter);
	}
	
	public void Set(Mikan.CSAGA.QuestType type, int chapter, int val)
	{
		var cache = Mikan.DictionaryUtils.SafeGetValueNullable(cacheList, (int)type);
		if(cache == null) return;
		cache.Set(chapter, val);

	}
}