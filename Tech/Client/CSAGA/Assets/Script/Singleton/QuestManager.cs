﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class QuestManager : Singleton<QuestManager> {

	private Dictionary<Mikan.CSAGA.QuestType, Dictionary<int, Chapter>> chapters;

	private Dictionary<int, QuestList> specials;

	private Mikan.CSAGA.QuestController controller;

	private Mikan.Serial cardSerial = Mikan.Serial.Empty;
	private int interactiveId;

	public Dictionary<Mikan.CSAGA.QuestType, List<Chapter>> ChaperList { get; private set; }

	public List<MemoryList> MemoryList{ get; private set; }

	public Mikan.CSAGA.QuestInstance Current { get { return GameSystem.Service.QuestInfo.Current; } }
	public Mikan.CSAGA.QuestController Controller { get { return controller; } }

	public bool IsTriggerViaCardEvent { get { return !cardSerial.IsEmpty; } }

	public Mikan.CSAGA.QuestInstance Generate(int questId)
	{
		return Mikan.CSAGA.Calc.Quest.Generate(questId, 0);
	}

	public void BeginTutorialQuest()
	{
		Context.Instance.QuestID = ConstData.Tables.TutorialQuestID;
		Message.Send(MessageID.BeginQuest);
	}

	public void PrepareQuest(QuestItemData quest)
	{
		Context.Instance.QuestID = quest.ID;

		PrepareQuest(Mikan.Serial.Empty, 0);
	}

	public void PrepareQuest(Mikan.Serial cardSerial, int interactiveId)
	{
		this.cardSerial = cardSerial;
		this.interactiveId = interactiveId;

		//Message.Send(MessageID.SwitchStage, StageID.QuestCheck);
	}

	public void PlayQuest()
	{
		if(Context.Instance.BattleSetting.Auto)
			GameSystem.Service.OnAutoBattleEnabled();

#if SKIP_QUEST
		Message.Send(MessageID.EndQuest);
#else
		controller.Prepare(Current);
#endif
	}

	public Chapter GetChapter(int no)
	{
		return chapters[Context.Instance.QuestType][no];
	}

	public QuestList GetSpecial(int questId)
	{
		var obj = default(QuestList);
		if(specials.TryGetValue(questId, out obj)) return obj;
		return null;
	}

	public void ShowQuestList()
	{
		var schedule = ConstData.Tables.EventSchedule[Context.Instance.EventScheduleID];
		if(schedule != null && (schedule.n_POINT > 0 || schedule.n_RANKING > 0))
			Message.Send(MessageID.SwitchStage, StageID.EventQuestList);
		else
			Message.Send(MessageID.SwitchStage, StageID.QuestList);
	}
	
	// Use this for initialization
	public void Setup()
	{
		controller = new Mikan.CSAGA.QuestController();
		controller.RegisterEventFactory(new QuestEventFactory());
		controller.OnReady += OnQuestControllerReady;
		controller.OnQuestEnd += OnQuestControllerEnd;

		chapters = new Dictionary<Mikan.CSAGA.QuestType, Dictionary<int, Chapter>>();
		chapters[Mikan.CSAGA.QuestType.Regular] = new Dictionary<int, Chapter>();
		chapters[Mikan.CSAGA.QuestType.Extra] = new Dictionary<int, Chapter>();

		ChaperList = new Dictionary<Mikan.CSAGA.QuestType, List<Chapter>>();
		ChaperList[Mikan.CSAGA.QuestType.Regular] = new List<Chapter>();
		ChaperList[Mikan.CSAGA.QuestType.Extra] = new List<Chapter>();

		MemoryList = new List<global::MemoryList>();

		specials = new Dictionary<int, QuestList>();

		var tmp = new Dictionary<int, QuestList>();

		foreach (var item in ConstData.Tables.QuestDesign.Select()) 
		{
			Chapter chapter = null;
			Dictionary<int, QuestList> owner = null;

			if(item.n_QUEST_TYPE == Mikan.CSAGA.QuestType.Regular || item.n_QUEST_TYPE == Mikan.CSAGA.QuestType.Extra)
			{
				if(!chapters[item.n_QUEST_TYPE].TryGetValue(item.n_CHAPTER, out chapter))
				{
					chapter = new Chapter();
					chapter.No = item.n_CHAPTER;
					chapter.Type = item.n_QUEST_TYPE;
					chapters[item.n_QUEST_TYPE].Add(item.n_CHAPTER, chapter);
					ChaperList[item.n_QUEST_TYPE].Add(chapter);
				}

				owner = tmp;
			}
			else
				owner = specials;

			QuestList list = null;
			
			if(!owner.TryGetValue(item.n_MAIN_QUEST, out list))
			{
				list = new QuestList();
				list.MainQuestID = item.n_MAIN_QUEST;
				list.QuestType = item.n_QUEST_TYPE;
				list.Difficulty = item.n_DIFFICULTY;
				if(list.QuestType == Mikan.CSAGA.QuestType.Schedule && item.n_COUNT == 0) list.IsInfinity = true;

				owner.Add(item.n_MAIN_QUEST, list);
				if(chapter != null)
				{
					list.Chapter = chapter;
					chapter.List.Add(list);
				}
			}
			
			list.List.Add(item);
		}

		var memoryList = new Dictionary<int, MemoryList>();

		foreach(var item in ConstData.Tables.Interactive.Select(o=>{ return o.n_RECALL != 0; }))
		{
			Chapter chapter = null;
			if(chapters[Mikan.CSAGA.QuestType.Regular].TryGetValue(item.n_RECALL, out chapter)) 
				chapter.MemoryList.Add(item);
			else if(chapters[Mikan.CSAGA.QuestType.Extra].TryGetValue(item.n_RECALL, out chapter)) 
				chapter.MemoryList.Add(item);
			else
			{
				var memory = default(MemoryList);
				if(!memoryList.TryGetValue(item.n_RECALL, out memory))
				{
					memory = new MemoryList();
					memory.QuestID = item.n_ID;
					memoryList.Add(item.n_RECALL, memory);
					MemoryList.Add(memory);
				}
				memory.List.Add(item);
			}
		}

		foreach(var chapterList in ChaperList)
		{
			foreach(var chapter in chapterList.Value)
			{
				foreach(var item in chapter.List) item.List.Sort(CompareSubQuest);

				chapter.List.Sort(CompareMainQuest);
			}
		}

		Message.AddListener(MessageID.BeginQuest, OnBeginQuest);
		Message.AddListener(MessageID.EndQuest, OnEndQuest);

		GameSystem.Service.OnQuestBegin += OnQuestBegin;
		GameSystem.Service.OnQuestEnd += OnQuestEnd;
	}

	private static int CompareMainQuest(QuestList lhs, QuestList rhs)
	{
		var _lhs = ConstData.Tables.QuestDesign[lhs.FirstQuestID];
		var _rhs = ConstData.Tables.QuestDesign[rhs.FirstQuestID];
		return _lhs.n_MAIN_SORT.CompareTo(_rhs.n_MAIN_SORT);
	}

	private static int CompareSubQuest(Mikan.CSAGA.ConstData.QuestDesign lhs, Mikan.CSAGA.ConstData.QuestDesign rhs)
	{
		return lhs.n_SUB_SORT.CompareTo(rhs.n_SUB_SORT);
	}
		
	void OnDestroy()
	{
		if(controller != null)
		{
			controller.Dispose();
			controller = null;
		}
	}

	void OnQuestControllerReady (Mikan.IQuest obj)
	{
		controller.Start();
	}

	void OnQuestControllerEnd (Mikan.IQuestResult obj)
	{
		Message.Send(MessageID.EndQuest, obj);
	}

	public bool CheckEquipmentSpace(int min)
	{
		var current = 0;

		foreach(var item in GameSystem.Service.EquipmentInfo.List) 
		{
			if(item.CardSerial.IsEmpty) ++current;
		}

		var max = ConstData.Tables.Player[GameSystem.Service.PlayerInfo.LV].n_EQUIP_SPACE;

		if(current + min > max)
		{
			MessageBox.Show(ConstData.GetSystemText(SystemTextID.TIP_EQUIP_SPACE_NOT_ENOUGH_V1_V2, current, max)).callback = o=>
			{
				Message.Send(MessageID.SwitchStage, StageID.Others);
			};

			return false;
		}

		return true;
	}
	private void OnBeginQuest()
	{
		if(!ShopManager.Instance.CheckCardSpace(0)) return;

		if(!CheckEquipmentSpace(0)) return;

		if(!cardSerial.IsEmpty)
			GameSystem.Service.CardEvent(cardSerial, interactiveId);
		else
		{
			var team = GameSystem.Service.TeamInfo[Context.Instance.TeamIndex];
			if(Context.Instance.QuestID == ConstData.Tables.PVPQuestID && (Context.Instance.PVPWaves != null && !Context.Instance.PVPWaves.IsSelf) && Context.Instance.PVPPlayer != null)
			{
				GameSystem.Service.PVPBegin(team.Members, Context.Instance.PVPPlayer.Origin.Ticket);
			}
			else
				GameSystem.Service.BeginQuest(Context.Instance.QuestID, team.Members, !Context.Instance.Option[OptionID.Rescue], (Context.Instance.Quest != null && Context.Instance.Quest.SharedQuest != null) ? Context.Instance.Quest.SharedQuest.Serial : 0);
		}
	}

	private void OnEndQuest(IMessage msg)
	{
		if(msg.Data == null)
			GameSystem.Service.EndQuest();
		else
			GameSystem.Service.EndQuest((Mikan.IQuestResult)msg.Data, Context.Instance.FightPowerList);
	}

	private void OnQuestBegin (Mikan.CSAGA.QuestInstance quest)
	{
		Context.Instance.QuestID = quest.QuestID;

		var data = ConstData.Tables.QuestDesign[quest.QuestID];

		Context.Instance.QuestCache.Set(data.n_QUEST_TYPE, data.n_CHAPTER, quest.QuestID);

		Context.Instance.BattleSetting.Auto = Context.Instance.AutoCache.Value.Contains(quest.QuestID);
		Message.Send(MessageID.SwitchStage, StageID.QuestStage);
	}

	private void OnQuestEnd (Mikan.CSAGA.QuestSettlement settlement)
	{
		Context.Instance.TriggerQuestID = settlement.TriggerQuestID;
		if(settlement.IsSucc)
			Message.Send(MessageID.SwitchStage, StageID.QuestSettlement);
		else if(Context.Instance.QuestID == ConstData.Tables.PVPQuestID)
		{
			if(Context.Instance.PVPWaves.IsSimulating) Context.Instance.PVPWaves.IsSimulateComplete = true;
			Message.Send(MessageID.SwitchStage, StageID.PVP);
		}
		else if(IsTriggerViaCardEvent)
			Message.Send(MessageID.SwitchStage, StageID.Card);
		else
			Message.Send(MessageID.SwitchStage, StageID.Quest);
	}

	public void SetupDifficulty()
	{
		if(Context.Instance.QuestType == Mikan.CSAGA.QuestType.Extra)
		{
			var lastQuestId = Context.Instance.QuestCache.GetLastQuestID(Mikan.CSAGA.QuestType.Extra);

			var data = ConstData.Tables.QuestDesign[lastQuestId];

			if(data != null) Context.Instance.Difficulty = data.n_DIFFICULTY;
		}
	}
}

public class Chapter
{
	public int No;
	public Mikan.CSAGA.QuestType Type;
	public List<QuestList> List = new List<QuestList>();
	public int FirstMainQuestID { get { return List.Count > 0 ? List[0].MainQuestID : 0; } }
	public List<Mikan.CSAGA.ConstData.Interactive> MemoryList = new List<Mikan.CSAGA.ConstData.Interactive>();
}

public class QuestList
{
	public Chapter Chapter;
	public int MainQuestID;
	public Mikan.CSAGA.QuestType QuestType;
	public int FirstQuestID { get { return List.Count > 0 ? List[0].n_ID : 0; } }
	public bool IsInfinity;
	public int Difficulty;
	public List<Mikan.CSAGA.ConstData.QuestDesign> List = new List<Mikan.CSAGA.ConstData.QuestDesign>();
}

public class MemoryList
{
	public int QuestID;
	public List<Mikan.CSAGA.ConstData.Interactive> List = new List<Mikan.CSAGA.ConstData.Interactive>();
}

class QuestEventFactory : Mikan.CSAGA.QuestEventFactoryBase
{
	#region implemented abstract members of QuestEventFactoryBase

	protected override Mikan.IQuestEvent CreateInteractiveEvent (Mikan.CSAGA.QuestInstance quest, int round, int interactiveId)
	{
		return new QuestInteractiveEvent(quest, round, interactiveId);
	}

	protected override Mikan.IQuestEvent CreateFightEvent (Mikan.CSAGA.QuestInstance quest, int round, int eventId)
	{
		return new FightQuestEvent(round, quest.Rounds, eventId, quest.QuestID);
	}

	#endregion
	
	protected override Mikan.IQuestEvent CreateRoundBeginEvent (Mikan.CSAGA.QuestInstance quest, int round)
	{
		return new RoundBeginEvent(round, quest.Rounds);
	}
	protected override Mikan.IQuestEvent CreateRoundEndEvent (Mikan.CSAGA.QuestInstance quest, int round)
	{
		return new RoundEndEvent();
	}

}

abstract public class QuestEventBase : Mikan.IQuestEvent
{
	#region IQuestEvent implementation

	public event System.Action<Mikan.IQuestEvent> OnPreload;

	public event System.Action<Mikan.IQuestEvent> OnReady;

	public event System.Action<Mikan.IQuestEventResult> OnFinish;

	virtual public void Preload ()
	{
		if(OnPreload != null) OnPreload(this);
	}

	virtual public void Prepare (Mikan.IQuestController controller)
	{
		if(OnReady != null) OnReady(this);
	}

	abstract public void Start ();

	protected void onFinish(bool isSucc)
	{
		if(OnFinish != null) OnFinish(new QuestEventResult{ Event = this, IsSucc = isSucc, Continue = isSucc });
	}

	protected void onSucc()
	{
		onFinish(true);
	}

	protected void onFail(bool isContinue)
	{
		if(OnFinish != null) OnFinish(new QuestEventResult{ Event = this, IsSucc = false, Continue = isContinue });
	}

	#region reserved

	virtual public void Pause () {}

	virtual public void Resume () {}

	virtual public void Cancel () {}

	#endregion

	#endregion

	#region IDisposable implementation

	virtual public void Dispose (){}

	#endregion

	#region ISerializable implementation

	public void Serialize (Mikan.IOutput output)
	{
		throw new System.NotImplementedException ();
	}

	public void DeSerialize (Mikan.IInput input)
	{
		throw new System.NotImplementedException ();
	}

	#endregion


}

class QuestEventResult : Mikan.IQuestEventResult
{
	#region IQuestEventResult implementation
	public Mikan.IQuestEvent Event { get; set; }
	public bool IsSucc { get; set; }
	public bool Continue { get; set; }
	#endregion

	#region ISerializable implementation
	public void Serialize (Mikan.IOutput output)
	{
		throw new System.NotImplementedException ();
	}
	public void DeSerialize (Mikan.IInput input)
	{
		throw new System.NotImplementedException ();
	}
	#endregion
}

class RoundBeginEvent : QuestEventBase
{
	private int round;
	private int totalRound;
	public RoundBeginEvent(int round, int totalRound)
	{
		this.round = round;
		this.totalRound = totalRound;
	}

	#region implemented abstract members of QuestEventBase
	public override void Start ()
	{
		ResourceManager.Instance.CreatePrefabInstance("GameScene/Quest/QuestWave", OnComplete);
	}
	#endregion

	private void OnComplete(GameObject go)
	{
		var obj = go.GetComponent<QuestWave>();
		obj.Wave = round + 1;
		obj.TotalWave = totalRound;
		obj.ApplyChange();

		onFinish(true);
	}
}

class RoundEndEvent : QuestEventBase
{
	#region implemented abstract members of QuestEventBase

	public override void Start ()
	{
		if(QuestWave.current != null) GameObject.Destroy(QuestWave.current.gameObject);
		onFinish(true);
	}

	#endregion


}

public class TraceEvent : QuestEventBase
{
	private string name;
	public TraceEvent(string name)
	{
		this.name = name;
	}
	#region implemented abstract members of QuestEventBase

	public override void Start ()
	{
		Debug.Log(string.Format("{0} . Start()", name));

		onFinish(true);
	}

	#endregion


}