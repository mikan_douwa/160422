﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class StageManager : Singleton<StageManager> {

	private Dictionary<StageID, StageContext> stages;

	private StageContext current = StageContext.Empty;

	private StageContext next = StageContext.Empty;

	private int serial = 0;

	private IAsset<GameObject> transistionAsset;

	private GameObject eventTransition;

	public StageContext Current { get { return current; } }
	public StageContext Next { get { return next; } }

	public bool IsSwitching { get { return !opQueue.IsEmpty; } }

	private Mikan.LiveOperationQueue opQueue = new Mikan.LiveOperationQueue();

	// Use this for initialization
	void Start () {
		transistionAsset = ResourceManager.Instance.LoadPrefab("Common/SceneTransition");
	}

	public void Setup()
	{
		RegisterStage ();
		
		Message.AddListener(MessageID.SwitchStage, OnSwitchStage);
		Message.AddListener(MessageID.ShowHeadline, OnShowHeadline);
		Message.AddListener(MessageID.HideHeadline, OnHideHeadline);
		Message.AddListener(MessageID.PlayEvent, OnPlayEvent);
	}

	private void RegisterStage()
	{
		stages = new Dictionary<StageID, StageContext> ();

		StageContext stage;

		stage = RegisterGameStage (StageID.Home, "GameScene/HomePage");

		RegisterGameStage (StageID.QuestNavigator, "GameScene/Quest/QuestNavigator");
		RegisterGameStage (StageID.Quest, "GameScene/Quest/QuestPage");
		AppendGameStage (StageID.QuestList, "GameScene/Quest/QuestList", HideType.Custom).Parent = StageID.Quest;
		AppendGameStage (StageID.QuestCheck, "GameScene/Quest/QuestTeam", HideType.Prev);
		AppendGameStage (StageID.SpecialQuestList, "GameScene/Quest/SpecialQuestList", HideType.Custom).Parent = StageID.Quest;
		AppendGameStage (StageID.EventQuestList, "GameScene/Quest/EventQuestList", HideType.Custom).Parent = StageID.Quest;

		RegisterGameStage (StageID.Card, "GameScene/Card/CardPage");
		RegisterGameStage (StageID.CardTeam, "GameScene/Card/CardTeamPage");
		RegisterGameStage (StageID.CardEnhance, "GameScene/Card/CardEnhancePage");
		RegisterGameStage (StageID.CardSell, "GameScene/Card/CardSellPage");
		RegisterGameStage (StageID.CardList, "GameScene/Card/CardListPage");
		RegisterGameStage (StageID.CardContact, "GameScene/Card/CardContactPage");
		RegisterGameStage (StageID.CardAwaken, "GameScene/Card/CardAwakenPage");

		AppendGameStage (StageID.CardInfo, "GameScene/Card/CardInfo", HideType.All).UseTransition = true;
		AppendGameStage (StageID.CardEnhancePopup, "GameScene/Card/CardEnhance", HideType.None);
		AppendGameStage (StageID.CardTeamMemberSelect, "GameScene/Card/CardTeamMemberSelect", HideType.All);
		AppendGameStage (StageID.CardTeamMemberBatchSelect, "GameScene/Card/CardTeamMemberBatchSelect", HideType.All);
		AppendGameStage (StageID.CardContactView, "GameScene/Card/CardContactView", HideType.All);
		AppendGameStage (StageID.CardEquipment, "GameScene/Card/CardEquipment", HideType.All);
		AppendGameStage (StageID.CardAwakenView, "GameScene/Card/CardAwakenView", HideType.None);
		AppendGameStage (StageID.CardAwakenConfirm, "GameScene/Card/CardAwakenConfirm", HideType.Prev);

		RegisterStage(StageID.QuestStage, SceneName.QUEST, null);
		RegisterStage(StageID.QuestSettlement, SceneName.GAME, "GameScene/Quest/QuestSettlement");

		RegisterGameStage(StageID.Summon, "GameScene/Gacha/GachaPage");
		AppendGameStage(StageID.GachaDrawAnimation, "GameScene/Gacha/GachaDrawAnimation", HideType.All).UseTransition = true;
		AppendGameStage(StageID.GachaDrawShowCard, "GameScene/Gacha/GachaDrawShowCard", HideType.None);
		RegisterGameStage(StageID.GachaDrawShowResult, "GameScene/Gacha/GachaDrawShowResult");

		RegisterGameStage(StageID.Shop, "GameScene/Shop/ShopPage");
		RegisterGameStage(StageID.RandomShop, "GameScene/Shop/RandomShopPage");
		RegisterGameStage(StageID.CommonShop, "GameScene/Shop/CommonShopPage");

		RegisterGameStage(StageID.Others, "GameScene/Setup/SetupPage");
		RegisterGameStage(StageID.Item, "GameScene/Setup/ItemPage");
		RegisterGameStage(StageID.Equipment, "GameScene/Setup/EquipmentPage");
		RegisterGameStage(StageID.Gallery, "GameScene/Setup/GalleryPage");

		AppendCommonStage(StageID.EventPlayer, "EventPlay/EventPlayer3", HideType.All).UseTransition = true;

		AppendCommonStage(StageID.SortBoard, "Common/SortBoard", HideType.None);

		RegisterGameStage(StageID.Gift, "GameScene/Gift/GiftPage");
		RegisterGameStage(StageID.Trophy, "GameScene/Gift/TrophyPage");

		AppendCommonStage(StageID.Announce, "WebView/WebView", HideType.None);
		AppendCommonStage(StageID.UserTreaty, "WebView/WebView", HideType.None);
		AppendCommonStage(StageID.Bahamut, "WebView/WebView", HideType.None);

		RegisterGameStage(StageID.ActiveAward, "GameScene/ActiveAward/ActiveAward");
		AppendCommonStage(StageID.ActiveTarget, "GameScene/ActiveAward/ActiveTarget", HideType.None);

		RegisterGameStage(StageID.FirstGacha, "GameScene/FirstGachaStage");

		AppendGameStage(StageID.RankingReward, "GameScene/RankReward/RankingReward", HideType.Prev);
		AppendGameStage(StageID.PointReward, "GameScene/RankReward/RankScoreReward", HideType.Prev);

		AppendGameStage(StageID.RankingTop10, "GameScene/RankReward/RankingList", HideType.Prev);
		AppendGameStage(StageID.RankingSelf, "GameScene/RankReward/RankingList", HideType.Prev);

		RegisterGameStage(StageID.IAP, "GameScene/Shop/IAPShopPage");

		RegisterGameStage(StageID.CoMission, "GameScene/CoMission/CoMissionPage");
		RegisterGameStage(StageID.CoMissionInfo, "GameScene/CoMission/CoMissionInfo");

		RegisterGameStage(StageID.SocialMenu, "GameScene/Social/SocialMenuPage");
		RegisterGameStage(StageID.SocialList, "GameScene/Social/SocialListPage");
		RegisterGameStage(StageID.FollowSupport, "GameScene/Social/SocialListPage");
		RegisterGameStage(StageID.RandomSupport, "GameScene/Social/SocialListPage");
		RegisterGameStage(StageID.SearchPlayer, "GameScene/Social/SearchPlayerStage");
		RegisterGameStage(StageID.SocialShop, "GameScene/Social/SocialShopStage");

		RegisterGameStage(StageID.Tactics, "GameScene/Tactic/TacticStage");

		RegisterGameStage(StageID.Faith, "GameScene/Faith/FaithPage");

		RegisterGameStage(StageID.PVP, "GameScene/PVP/PVPPage");
	}

	private StageContext RegisterStage(StageID stageId, string scene, string prefab)
	{
		var context = new StageContext ();
		context.ID = stageId;
		context.Scene = scene;
		context.Prefrab = prefab;


		foreach(var item in ConstData.Tables.Tutorial.Select(o=>{ return o.n_SCENEID == (int)stageId; }))
		{
			if(context.Tutorials == null) context.Tutorials = new List<Mikan.CSAGA.ConstData.Tutorial>();
			context.Tutorials.Add(item);
		}

		if(context.Tutorials != null) context.Tutorials.Sort((lhs, rhs)=>{ return lhs.n_PAGE.CompareTo(rhs.n_PAGE); });

		stages.Add (stageId, context);
		return context;
	}

	private StageContext RegisterGameStage(StageID stageId, string prefab)
	{
		return RegisterStage (stageId, SceneName.GAME, prefab);
	}

	private StageContext RegisterQuestStage(StageID stageId, string prefab)
	{
		return RegisterStage (stageId, SceneName.QUEST, prefab);
	}

	private StageContext AppendGameStage(StageID stageId, string prefab, HideType hideType)
	{
		var context = RegisterGameStage(stageId, prefab);
		context.IsAppend = true;
		context.HideType = hideType;
		return context;
	}

	private StageContext AppendCommonStage(StageID stageId, string prefab, HideType hideType)
	{
		var context = RegisterStage(stageId, null, prefab);
		context.IsAppend = true;
		context.HideType = hideType;
		return context;
	}

	public StageContext GetStage(StageID stageId)
	{
		StageContext stage;
		if (stages.TryGetValue (stageId, out stage)) return stage;
		return StageContext.Empty;
	}

	public bool IsAvailable(StageID id)
	{
#if !DEBUG
		switch(id)
		{
		case StageID.ActiveAward:
			return ConstData.Tables.GetVariable(Mikan.CSAGA.VariableID.ACTIVE_ENABLE) != 0;
		case StageID.CardContact:
			return ConstData.Tables.GetVariable(Mikan.CSAGA.VariableID.COMMU_ENABLE) != 0;
		case StageID.CardAwaken:
			return ConstData.Tables.GetVariable(Mikan.CSAGA.VariableID.AWAKEN_ENABLE) != 0;
		case StageID.IAP:
			return ConstData.Tables.GetVariable(Mikan.CSAGA.VariableID.IAP_ENABLE) != 0;
		case StageID.CoMission:
			return ConstData.Tables.GetVariable(Mikan.CSAGA.VariableID.COMISSION_ENABLE) != 0;
		case StageID.CoMissionRanking:
			return ConstData.Tables.GetVariable(Mikan.CSAGA.VariableID.COMISSION_RANKING_ENABLE) != 0;
		case StageID.Bahamut:
			return ConstData.Tables.GetVariable(Mikan.CSAGA.VariableID.BAHAMUT_ENABLE) != 0;
		case StageID.Faith:
			return ConstData.Tables.GetVariable(Mikan.CSAGA.VariableID.FAITH_ENABLE) != 0;
		case StageID.Tactics:
			return ConstData.Tables.GetVariable(Mikan.CSAGA.VariableID.TACTICS_ENABLE) != 0;
		}
#endif

		return true;
	}
	private void OnSwitchStage(IMessage msg)
	{
		var stageId = (StageID)msg.Data;

		next = GetStage(stageId);

		if(!opQueue.IsEmpty) 
		{
			Context.Instance.SwitchByNavigator = false;
			return;
		}

		if(!next.IsAppend) opQueue.Enqueue(new SwitchStagePrepareOperation{ Current = current });

		SwitchStage(stageId);
	}

	private void SwitchStage(StageID stageId)
	{
		opQueue.Enqueue(new SwitchStageOperation{ StageID = stageId });
	}

	public void ApplyStage(StageContext stage)
	{
		Context.Instance.SwitchByNavigator = false;

		current = stage;

		if(current != next && (current.Current == null || current.Current.Context != next)) 
			SwitchStage(next.ID);
		else
			next = StageContext.Empty;

	}
	
	private void OnShowHeadline(IMessage msg)
	{
		Headline.Show((Headline.Behavior)msg.Data);
	}

	private void OnHideHeadline()
	{
		Headline.Hide();
	}

	private void OnPlayEvent(IMessage msg)
	{
		Context.Instance.InteractiveID = (int)msg.Data;
		Message.Send(MessageID.SwitchStage, StageID.EventPlayer);                                    
	}

	public void RemoveAppend()
	{
		ReplaceAppend(StageID.None);
	}

	public void ReplaceAppend(StageID id, float delay = 0f, bool removeAll = false)
	{
		if(current.Current == null || !current.Current.Context.IsAppend) return;
		
		opQueue.Enqueue(new RemoveAppendStageOperation{ Replace = id, Delay = delay, RemoveAll = removeAll });
	}

	public void RemoveAllAppend()
	{
		ReplaceAppend(StageID.None, 0, true);
	}

	public void Remove(StageObject obj)
	{
		if(current.Current != obj || obj.IsManaged) return;

		if(!obj.Context.IsAppend) return;

		ShowPrev(obj.HideType);

		current.Objs.Pop();

		if(current.Current != null) Context.Instance.StageID = current.Current.Context.ID;

		Message.Send(MessageID.RemoveStage);
	}

	private void ShowPrev(HideType hideType)
	{
		StartCoroutine(SetActiveRoutine(current, true, hideType));
	}

	public void HidePrev(HideType hideType)
	{
		StartCoroutine(SetActiveRoutine(current, false, hideType));
	}

	private IEnumerator SetActiveRoutine(StageContext current, bool active, HideType hideType)
	{
		if(current.IsEmpty) 
		{
			if(active) Context.Instance.StageID = StageID.None;
			yield break;
		}

		if(hideType == HideType.None) yield break;

		var objs = new List<StageObject>(current.Objs.ToArray());

		if(current.Current != null) objs.Remove(current.Current);

		if(hideType == HideType.Prev)
		{
			if(objs.Count > 0) 
			{
				var obj = objs[0];
				objs.Clear();
				objs.Add(obj);
			}
		}

		yield return new WaitForEndOfFrame();

		if(current != this.current) yield break;

		foreach(var item in objs) item.SetActive(active);
	}

	private void DestroyCurrent()
	{
		DestroyStage(current);
	}

	public void DestroyStage(StageContext context)
	{
		if(context.IsEmpty) return;
		
		while(context.Objs.Count > 0)
		{
			var obj = context.Objs.Pop();
			
			if(obj != null) 
			{
				if(obj.stage != null) obj.stage.parent = null;
				Destroy(obj.gameObject);
			}
		}
	}

}

public enum StageID
{
	None = 0,
	Home = 1,
	Quest = 2,
	Card = 3,
	Summon = 4,
	Shop = 5,
	Others = 6,
	QuestList = 7,
	CardTeam = 8,
	CardEnhance = 9,
	CardSell = 10,
	CardList = 11,
	CardInfo = 12,
	CardContact = 13,
	CardAwaken = 14,
	CardEnhancePopup = 15,
	CardTeamMemberSelect = 16,
	CardContactView = 17,
	EventPlayer = 18,
	QuestCheck = 19,
	QuestStage = 20,
	QuestSettlement = 21,
	CardEquipment = 22,
	GachaDrawAnimation = 23,
	SortBoard = 24,
	CardAwakenView = 25,
	CardAwakenConfirm = 26,
	CardTeamMemberBatchSelect = 27,
	Gallery = 28,
	Item = 29,
	Equipment = 30,
	Memory = 31,
	EditName = 32,
	SpecialQuestList = 33,
	GachaDrawShowCard = 34,
	Gift = 35,
	Trophy = 36,
	Announce = 37,
	FirstGacha = 38,
	ActiveAward = 39,
	ActiveTarget = 40,
	RandomShop = 41,
	GachaDrawShowResult = 42,
	EventQuestList = 43,
	RankingReward = 44,
	PointReward = 45,
	RankingTop10 = 46,
	RankingSelf = 47,
	IAP = 48,
	UserTreaty = 49,
	CoMission = 50,
	CoMissionInfo = 51,
	CoMissionRanking = 52,
	Bahamut = 53,
	SocialMenu = 54,
	SocialList = 55,
	FollowSupport = 56,
	RandomSupport = 57,
	SearchPlayer = 58,
	SocialShop = 59,
	QuestNavigator = 60,
	Faith = 61,
	Tactics = 62,
	CommonShop = 63,
	PVP = 64,
}

public class StageContext
{
	public static StageContext Empty = new StageContext{ ID = StageID.None };

	public StageID ID;

	public StageID Parent = StageID.None;

	public string Scene;
	public string Prefrab;
	public bool IsAppend = false;
	public bool UseTransition = false;
	public HideType HideType = HideType.None;

	public Stack<StageObject> Objs = new Stack<StageObject>();

	public StageObject Current { get { return Objs.Count > 0 ? Objs.Peek() : null; } }

	public bool IsValid { get { return this.ID != StageID.None; } }

	public bool IsEmpty { get { return this == Empty; } }

	public List<Mikan.CSAGA.ConstData.Tutorial> Tutorials;
}

public enum HideType
{
	None,
	Prev,
	All,
	Custom,
}

class SwitchStagePrepareOperation : Mikan.OperationBase
{
	public StageContext Current;

	#region implemented abstract members of OperationBase

	public override void Execute ()
	{
		if(Current.IsEmpty || Current.Current == null || Current.Current.stage == null || Current.Current.stage.SwitchPrepare())
			OperationComplete();
		else
			Coroutine.Start(WaitFor());
	}

	#endregion

	IEnumerator WaitFor()
	{
		while(!Current.Current.stage.SwitchPrepare()) yield return new WaitForSeconds(0.1f);

		OperationComplete();
	}
}


class SwitchStageOperation : Mikan.OperationBase
{
	public StageID StageID;

	public bool IsTransitionExists = false;

	private Mikan.LiveOperationQueue opQueue;

	private StageContext current;
	private StageContext next;

	private bool isRecursive = false;
	
	#region implemented abstract members of OperationBase

	public override void Execute ()
	{
		if(!Prepare()) OperationComplete();

	}

	#endregion

	private bool Prepare()
	{
		if(!StageManager.Instance.IsAvailable(StageID))
		{
			MessageBox.Show(SystemTextID.TIP_NOT_AVAILABLE);
			return false;
		}

		next = StageManager.Instance.GetStage(StageID);

		if(next.IsEmpty) return false;

		current = StageManager.Instance.Current;

		if(current == next) return false;

		opQueue = new Mikan.LiveOperationQueue();

		var hasTransition = false;

		if(!next.IsAppend)
		{
			if(Application.loadedLevelName != next.Scene)
			{

				if(!Context.Instance.IsPreloading)
				{
					if(!IsTransitionExists)
					{
						opQueue.Enqueue(new BeginTransitionOperation());
						hasTransition = true;
					}
				}

				opQueue.Enqueue(new DestroyStageOperation{ Stage = current });
				opQueue.Enqueue(new SwitchSceneOpertation{ SceneName = next.Scene });
			}
			else
			{
				if(!IsTransitionExists)
					opQueue.Enqueue(new HideStageOperation{ Stage = current });
				opQueue.Enqueue(new DestroyStageOperation{ Stage = current });
			}

			current = next;
		}
		else
		{
			if(next.Parent != StageID.None)
			{
				var exists = false;
				foreach(var item in current.Objs)
				{
					if(item.Context.ID != next.Parent) continue;
					exists = true;
					break;
				}

				if(!exists)
					opQueue.Enqueue(new SwitchStageOperation{ StageID = next.Parent, isRecursive = true });
			}

			if(!IsTransitionExists && next.UseTransition)
			{
				opQueue.Enqueue(new BeginTransitionOperation());
				hasTransition = true;
			}
		}

		opQueue.Enqueue(new CreateStageOperation{ Current = current, Stage = next });

		if(hasTransition)
		{
			opQueue.Enqueue(new ShowStageOperation{ Stage = current, Smooth = false });
			opQueue.Enqueue(new EndTransitionOperation());
		}
		else
			opQueue.Enqueue(new ShowStageOperation{ Stage = current, Smooth = !Context.Instance.IsPreloading });

		opQueue.Enqueue(new Mikan.CallbackOperation(EndPoint));
		return true;

	}

	private void EndPoint()
	{
		if(!isRecursive) StageManager.Instance.ApplyStage(current);

		Coroutine.DelayInvoke(OperationComplete);
	}

}

class RemoveAppendStageOperation : Mikan.OperationBase
{
	public StageID Replace = StageID.None;

	public float Delay = 0f;

	public bool RemoveAll = false;

	private Mikan.LiveOperationQueue opQueue;

	#region implemented abstract members of OperationBase
	
	public override void Execute ()
	{
		var current = StageManager.Instance.Current.Current;
		
		opQueue = new Mikan.LiveOperationQueue();
		
		var hasTransition = false;
		
		if(current.Context.UseTransition)
		{
			opQueue.Enqueue(new BeginTransitionOperation());
			hasTransition = true;
		}

		if(RemoveAll)
			opQueue.Enqueue(new DestroyAllAppendStageOperation{ Stage = StageManager.Instance.Current });
		else
			opQueue.Enqueue(new DestroyAppendStageOperation{ Stage = current });

		if(Replace != StageID.None)
			opQueue.Enqueue(new SwitchStageOperation{ StageID = Replace, IsTransitionExists = hasTransition });

		if(Delay > 0f)
			opQueue.Enqueue(new DelayOperation(Delay));

		if(hasTransition)
			opQueue.Enqueue(new EndTransitionOperation());

		opQueue.Enqueue(new Mikan.CallbackOperation(OperationComplete));
	}
	
	#endregion
	
}

class BeginTransitionOperation : Mikan.OperationBase
{
	#region implemented abstract members of OperationBase

	public override void Execute ()
	{
		var go = (GameObject)GameObject.Instantiate(Essentials.Instance.sceneTranslation);
		go.GetComponent<SceneTransition>().fadeIn = true;
		UIManager.Instance.AddToRoot(go);
		Coroutine.Start(WaitFor());
	}

	#endregion

	private IEnumerator WaitFor()
	{
		while(SceneTransition.Instance == null || SceneTransition.Instance.IsFading) yield return new WaitForEndOfFrame();
		OperationComplete();
	}

}

class EndTransitionOperation : Mikan.OperationBase
{
	#region implemented abstract members of OperationBase
	
	public override void Execute ()
	{
		if(SceneTransition.Instance == null)
			OperationComplete();
		else
		{
			SceneTransition.Instance.FadeOut();
			Coroutine.Start(WaitFor());
		}
	}
	
	#endregion
	
	private IEnumerator WaitFor()
	{
		while(SceneTransition.Instance.IsFading) yield return new WaitForEndOfFrame();
		GameObject.Destroy(SceneTransition.Instance.gameObject);
		Coroutine.DelayInvoke(OperationComplete);
	}
}

class SwitchSceneOpertation : Mikan.OperationBase
{
	public string SceneName;
	#region implemented abstract members of OperationBase
	
	public override void Execute ()
	{
		Message.AddListener(MessageID.NextScene, OnNextScene);
		Application.LoadLevel(SceneName);
	}
	
	#endregion
	
	private void OnNextScene()
	{
		Message.RemoveListener(MessageID.NextScene, OnNextScene);
		if(!Context.Instance.IsPreloading)
			UIManager.Instance.AddToRoot((GameObject)GameObject.Instantiate(Essentials.Instance.sceneTranslation));
		Coroutine.DelayInvoke(OperationComplete);
	}
	
}

class CreateStageOperation : Mikan.OperationBase
{
	public StageContext Current;
	public StageContext Stage;

	#region implemented abstract members of OperationBase

	public override void Execute ()
	{
		Context.Instance.StageID = Stage.ID;

		if(!string.IsNullOrEmpty(Stage.Prefrab))
			Coroutine.Start(CreateStageRoutine());
		else
			OperationComplete();
		

	}

	#endregion

	private IEnumerator CreateStageRoutine()
	{
		var asset = ResourceManager.Instance.LoadPrefab(Stage.Prefrab);
		while(!asset.IsComplete) yield return new WaitForEndOfFrame();

		var stageObj = UIManager.Instance.AddToRoot<StageObject>(asset.Content.layer);
		stageObj.Context = Stage;
		stageObj.name += " - " + asset.Content.name;

		while(!stageObj.IsStarted) yield return new WaitForEndOfFrame();

		stageObj.AddStage(asset.Content);
		asset.DecreaseReferenceCount();

		while(!stageObj.IsInitialized) yield return new WaitForEndOfFrame();

		Current.Objs.Push(stageObj);

		OperationComplete();
	}
}

class ShowStageOperation : Mikan.OperationBase
{
	public StageContext Stage;

	public bool Smooth;
	
	#region implemented abstract members of OperationBase
	
	public override void Execute ()
	{
		if(Stage.Current == null)
		{
			OperationComplete();
			return;
		}

		if(Stage.Current.Context.IsAppend)
		{
			StageManager.Instance.HidePrev(Stage.Current.HideType);
			Message.Send(MessageID.AppendStage);
		}

		Stage.Current.Show(Smooth);

		Coroutine.Start(WaitFor());
	}
	
	#endregion

	private IEnumerator WaitFor()
	{
		while(!Stage.Current.IsComplete) yield return new WaitForEndOfFrame();
		OperationComplete();
	}
}

class HideStageOperation : Mikan.OperationBase
{
	public StageContext Stage;
	
	#region implemented abstract members of OperationBase
	
	public override void Execute ()
	{
		if(Stage.IsValid)
		{
			Headline.HideAppendix();
			
			var objs = Stage.Objs.ToArray();
			foreach(var obj in objs)
			{
				if(obj.IsDestroyed) continue;
				//foreach(var scrollbar in obj.GetComponentsInChildren<UIScrollBar>()) scrollbar.gameObject.SetActive(false);
				
				var widget = obj.GetComponent<UIWidget>();
				widget.updateAnchors = UIRect.AnchorUpdate.OnStart;
				TweenX.Begin(obj.gameObject, 0.1f, -(UIDefine.ScreenWidth + 70));
			}
			Coroutine.DelayInvoke(OperationComplete, 0.15f);
		}
		else
			OperationComplete();
	}
	
	#endregion
}

class DestroyStageOperation : Mikan.OperationBase
{
	public StageContext Stage;

	#region implemented abstract members of OperationBase
	
	public override void Execute ()
	{
		StageManager.Instance.DestroyStage(Stage);
		Coroutine.DelayInvoke(OperationComplete);
	}
	
	#endregion
	
	
}

class DestroyAppendStageOperation : Mikan.OperationBase
{
	public StageObject Stage;

	#region implemented abstract members of OperationBase
	
	public override void Execute ()
	{
		GameObject.Destroy(Stage.gameObject);
		Coroutine.DelayInvoke(OperationComplete);
	}
	
	#endregion
}

class DestroyAllAppendStageOperation : Mikan.OperationBase
{
	public StageContext Stage;
	
	#region implemented abstract members of OperationBase
	
	public override void Execute ()
	{
		var list = new List<StageObject>(Stage.Objs.ToArray());

		if(list.Count <= 1)
			OperationComplete();
		else
		{
			while(Stage.Objs.Count > 1)
			{
				var obj = Stage.Objs.Pop();
				
				if(obj.stage != null) obj.stage.IsManaged = true;
				GameObject.Destroy(obj.gameObject);
			}

			var current = Stage.Current;

			current.ForceActive();

			if(current.stage != null) 
			{
				current.stage.UpdateHeadline();
				current.stage.GetBack();
			}
			Coroutine.DelayInvoke(OperationComplete);
		}
	}

	#endregion
}
