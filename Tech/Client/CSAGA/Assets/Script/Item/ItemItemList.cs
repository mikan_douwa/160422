﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ItemItemList : ItemList {
	
	public Mikan.CSAGA.ItemType filter;

	private EquipmentItemDataProvider provider;

	private bool isFirstTime = true;

	protected override void OnStart ()
	{
		base.OnStart ();

		Rebuild();
	}

	public void Rebuild()
	{
		switch(filter)
		{
		case Mikan.CSAGA.ItemType.Equipment:
		{
			var list = new List<EquipmentData>();
			
			foreach(var item in GameSystem.Service.EquipmentInfo.List)
			{
				if(!item.CardSerial.IsEmpty) continue;
				var data = new EquipmentData{ Origin = item, ItemID = item.ID, IsLocked = item.IsLocked, Type = filter };
				data.Rare = ConstData.Tables.Item[item.ID].n_RARE;
				list.Add(data);
			}

			while(list.Count < GameSystem.Service.PlayerInfo.Data.n_EQUIP_SPACE) list.Add(new EquipmentData{ Type = filter });
			
			provider = new EquipmentItemDataProvider(list);

			provider.Sort(Context.Instance.SortEquipment.Value);

			Setup(provider);
			
			Message.AddListener(MessageID.Sort, OnSort);
			break;
		}
		case Mikan.CSAGA.ItemType.Consumable:
		{
			var list = new List<ItemData>();
			
			foreach(var item in ConstData.Tables.Item.Select(o=>{ return o.n_TYPE == Mikan.CSAGA.ItemType.Material || o.n_TYPE == Mikan.CSAGA.ItemType.Gift; }))
			{
				var data = new ItemData{ ItemID = item.n_ID, Type = item.n_TYPE, Usable = (item.n_EFFECT == Mikan.CSAGA.ItemEffect.Usable) };
				list.Add(data);
			}
			Setup(list);
			break;
		}
		default:
		{
			var list = new List<ItemData>();
			
			foreach(var item in ConstData.Tables.Item.Select(o=>{ return o.n_TYPE == filter; }))
			{
				var data = new ItemData{ ItemID = item.n_ID, Type = filter, Usable = (item.n_EFFECT == Mikan.CSAGA.ItemEffect.Usable) };
				list.Add(data);
			}
			Setup(list);
			break;
		}
		}
	}

	protected override void OnCommitChange ()
	{
		if(isFirstTime)
		{
			isFirstTime = false;
			base.OnCommitChange ();
		}
	}

	protected override void OnDestroy ()
	{
		Message.RemoveListener(MessageID.Sort, OnSort);
		base.OnDestroy ();
	}

	private void OnSort(IMessage msg)
	{
		if(provider == null || Context.Instance.SortTarget != SortTarget.Equipment) return;
		
		var method = (SortMethod)msg.Data;
		provider.Sort(method);
	}
}


public class ItemData
{
	public int ItemID;
	public Mikan.CSAGA.ItemType Type;
	public bool Usable = false;
}

public class EquipmentData : ItemData
{
	public Mikan.CSAGA.Data.Equipment Origin;
	public int Rare;
	public bool IsLocked;
}

public class EquipmentItemDataProvider : ListItemDataProvider<EquipmentData>
{
	private List<EquipmentData> origin;
	public EquipmentItemDataProvider(List<EquipmentData> list)
	{
		this.list = list;
	}
	
	public void Sort(SortMethod method)
	{
		list.Sort(EquipmentSortMethod.Get(method));
		TriggerOnChange();
	}

}