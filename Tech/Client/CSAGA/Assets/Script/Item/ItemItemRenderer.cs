﻿using UnityEngine;
using System.Collections;


public class ItemItemRenderer : ItemItemRenderer<ItemData> 
{
	

}

public class ItemItemRenderer<T> : ItemRenderer<T> 
	where T : ItemData
{
	public Item item;

	public bool enableLongPress = true;

	protected int itemId;
	
	protected override void OnStart ()
	{
		gameObject.AddMissingComponent<LongPressDetector>();

		if(item == null) ResourceManager.Instance.CreatePrefabInstance(gameObject, "Template/Item", OnCreateComplete);
	}
	
	private void OnCreateComplete(GameObject obj)
	{
		item = obj.GetComponent<Item>();
		ApplyChange();
	}

	#region implemented abstract members of ItemRenderer
	
	protected override void ApplyChange (T data)
	{
		itemId = data.ItemID;

		if(data.Type == Mikan.CSAGA.ItemType.Equipment)
		{
			if(item != null) item.Count = 0;
		}

		if(item != null) 
		{
			item.ItemID = data.ItemID;
			item.ApplyChange();
		}
	}

	#endregion
	
	virtual protected void OnClick()
	{
		Context.Instance.Item = data;
		Message.Send(MessageID.SelectItem, data);
		Message.Send(MessageID.PlaySound, SoundID.CLICK);

		if(Context.Instance.StageID == StageID.Item)
			OnLongPress();
	}

	void OnLongPress()
	{
		if(!enableLongPress || data.ItemID == 0) return;
		if(data.Type == Mikan.CSAGA.ItemType.Equipment)
		{
			var _data = data as EquipmentData;
			if(_data == null || _data.Origin == null || _data.Origin.CardSerial.IsEmpty || _data.Origin.CardSerial.IsExternal)
				Message.Send(MessageID.ShowMessageBox, MessageBoxContext.Create("Common/EquipmentInfo3", data));
			else
			{
				var msgBox = MessageBox.Show("Common/EquipmentInfo3", "", SystemTextID._261_BTN_REMOVE_EQUIP);
				msgBox.enableCloseByClick = true;
				msgBox.args = new object[]{ _data };
				msgBox.callback = (index)=>
				{
					MessageBox.ShowConfirmWindow("GameScene/Card/EquipmentRemove", "", null).args = new object[]{ _data };
				};
			}
		}
		else
		{
			ItemInfo.Show(data.ItemID, data.Usable);
		}

	}
	
}

