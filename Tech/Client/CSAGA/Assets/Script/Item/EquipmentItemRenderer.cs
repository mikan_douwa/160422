﻿using UnityEngine;
using System.Collections;

public class EquipmentItemRenderer : ItemItemRenderer<EquipmentData>
{
	public UILabel info;

	public GameObject rareObj;

	public GameObject infoObj;

	public UIWidget lckObj;

	public SimpleToggle toggle;

	public bool isStatic = false;

	public bool useDefaultFormat = false;

	private SortMethod method;

	private SelectObject selectObj;

	protected override void OnStart ()
	{
		base.OnStart ();

		if(!isStatic && Context.Instance.StageID == StageID.Equipment) selectObj = gameObject.AddComponent<SelectObject>();

		GameSystem.Service.OnEquipmentChange += OnEquipmentChange;
	}

	void OnDestroy()
	{
		if(GameSystem.IsAvailable) GameSystem.Service.OnEquipmentChange -= OnEquipmentChange;
	}

	void OnEquipmentChange (long serial)
	{
		if(data == null || data.Origin == null || data.Origin.Serial != serial) return;
		data.IsLocked = data.Origin.IsLocked;
		if(data.IsLocked)
		{
			if(selectObj != null && selectObj.Selected) selectObj.Select(10, false);
		}
		ApplyChange();
	}

	protected override bool OnIndexChange (int newIndex)
	{
		var isChanged = base.OnIndexChange (newIndex);
		
		if(!isChanged)
		{
			if(data != null) isChanged = (method != Context.Instance.SortEquipment.Value);
		}
		
		return isChanged;
	}

	protected override void ApplyChange (EquipmentData data)
	{
		base.ApplyChange (data);

		if(selectObj != null) 
		{
			selectObj.data = data;
			selectObj.UpdateInitSelected();
		}

		if(rareObj != null) rareObj.SetActive(false);

		if(data == null) return;
		
		method = Context.Instance.SortEquipment.Value;

		if(data.Origin != null)
		{
			infoObj.SetActive(true);

			if(lckObj != null) lckObj.alpha = data.IsLocked ? 1 : 0;

			if(useDefaultFormat)
			{
				info.text = data.Rare.ToString("   0");
				if(rareObj != null) rareObj.SetActive(true);
			}
			else
			{

				switch(method)
				{
				case SortMethod.Serial:	
				case SortMethod.Rare:	
					info.text = data.Rare.ToString("   0");
					if(rareObj != null) rareObj.SetActive(true);
					break;
				case SortMethod.HP:	
					info.text = data.Origin.Hp.ToString();
					break;
				case SortMethod.Atk:	
					info.text = data.Origin.Atk.ToString();
					break;
				case SortMethod.Rev:	
					info.text = data.Origin.Rev.ToString();
					break;
				case SortMethod.Chg:	
					info.text = data.Origin.Chg.ToString() + "%";
					break;
				case SortMethod.Counter01:
				case SortMethod.Counter02:
				case SortMethod.Counter04:
				case SortMethod.Counter08:
				case SortMethod.Counter16:
				{
					var counterType = (int)Mathf.Pow(2, (int)(method - SortMethod.Counter01));
					var counterVal = data.Origin.CounterType == counterType ? data.Origin.Counter : 0;
					info.text = counterVal.ToString() + "%";
					break;
				}
				default:
					break;
				}

			}
			
		}
		else
		{
			infoObj.SetActive(false);
			if(lckObj != null) lckObj.alpha = 0;
		}

		if(toggle != null)
		{
			toggle.value = ((Context.Instance.Item as EquipmentData) == data);
		}
	}

	protected override void OnClick ()
	{
		if(isStatic || data.Origin == null || data.IsLocked) return;

		if(selectObj != null) selectObj.Select(10, false);

		if(toggle != null) toggle.value = true;

		base.OnClick ();
	}
}
