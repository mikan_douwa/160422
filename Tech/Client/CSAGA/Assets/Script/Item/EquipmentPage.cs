﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EquipmentPage : ItemPage {

	public UILabel selected;
	
	public UILabel crystal;

	protected override void Initialize ()
	{
		base.Initialize ();

		GameSystem.Service.OnEquipmentSell += OnEquipmentSell;

		Message.AddListener(MessageID.Confirm, OnConfirm);
		Message.AddListener(MessageID.Cancel, OnCancel);
		Message.AddListener(MessageID.SelectItem, OnSelectItem);

		Context.Instance.Selections.Clear();

		RefreshLabels();
	}

	void OnEquipmentSell ()
	{
		Context.Instance.Selections.Clear();
		
		RefreshLabels();

		var itemList = GetComponentInChildren<ItemItemList>();
		if(itemList != null) itemList.Rebuild();

		if(spaceInfo != null) spaceInfo.ApplyChange();
	}

	protected override void OnDestroy ()
	{
		if(GameSystem.IsAvailable) GameSystem.Service.OnEquipmentSell -= OnEquipmentSell;

		Message.RemoveListener(MessageID.Confirm, OnConfirm);
		Message.RemoveListener(MessageID.Cancel, OnCancel);
		Message.RemoveListener(MessageID.SelectItem, OnSelectItem);
		base.OnDestroy ();
	}

	private void OnConfirm()
	{
		if(Context.Instance.Selections.Count == 0) return;

		MessageBox.ShowConfirmWindow("GameScene/Setup/EquipSellMessageBox", "", null);
	}

	private void OnCancel()
	{
		Context.Instance.Selections.Clear();
		
		RefreshLabels();

		if(itemList != null)
		{
			for(int i = 0; i < itemList.RendererCount; ++i) itemList[i].ApplyChange();
		}

	}

	private void OnSelectItem(IMessage msg)
	{
		RefreshLabels();
	}
	
	private void RefreshLabels()
	{
		selected.text = ConstData.GetSystemText(SystemTextID.SELECTED_V1_TOTAL, Context.Instance.Selections.Count);
		
		var crystal = 0;
		
		foreach(var obj in Context.Instance.Selections)
		{
			var item = GameSystem.Service.EquipmentInfo[((EquipmentData)obj).Origin.Serial];
			var data = ConstData.Tables.Item[item.ID];

			crystal += data.n_EXP_GAIN;
		}
		
		this.crystal.text = ConstData.GetSystemText(SystemTextID.CRYSTAL_V1, crystal);
	}
}
