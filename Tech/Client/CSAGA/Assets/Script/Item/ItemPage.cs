﻿using UnityEngine;
using System.Collections;

public class ItemPage : Stage {

	public int viewCount;

	public Mikan.CSAGA.ItemType filter;

	public GameObject prefab;

	public GameObject listPrefab;

	public ItemItemList itemList;

	public SpaceInfo spaceInfo;

	protected override void Initialize ()
	{
		titleText = Context.Instance.SetupData.Text;

		if(listPrefab == null)
			ResourceManager.Instance.CreatePrefabInstance(gameObject, "Template/ItemList", OnCreateComplete);
		else
			OnCreateComplete(UIManager.Instance.AddChild(gameObject, listPrefab));

		if(filter == Mikan.CSAGA.ItemType.Equipment)
		{
			Context.Instance.SortTarget = SortTarget.Equipment;
			ResourceManager.Instance.CreatePrefabInstance(gameObject, "Common/SpaceInfo", o=>{ 
				spaceInfo = o.GetComponent<SpaceInfo>(); 
				spaceInfo.type = SpaceInfo.TYPE.EQUIPMENT;
				spaceInfo.ApplyChange(); 
			});
		}
		else
			GameSystem.Service.OnItemUse += OnItemUse;
	}

	void OnItemUse (int dropId)
	{
		if(dropId > 0) QuestDrop.Show(dropId);

		for(int i = 0; i < itemList.RendererCount; ++i) itemList[i].ApplyChange();
	}

	protected override void OnNext ()
	{
		if(filter == Mikan.CSAGA.ItemType.Equipment)
		{
			Context.Instance.SortTarget = SortTarget.Equipment;
			Message.Send(MessageID.SwitchStage, StageID.SortBoard);
		}
	}

	protected override void OnDestroy ()
	{
		if(GameSystem.IsAvailable) GameSystem.Service.OnItemUse -= OnItemUse;
		base.OnDestroy ();
	}

	private void OnCreateComplete(GameObject obj)
	{
		itemList = obj.GetComponent<ItemItemList>();
		itemList.viewCount = viewCount;
		itemList.filter = filter;
		if(prefab != null)
			itemList.prefab = prefab;

		Coroutine.Start(DelayTriggerInitializeComplete());
	}

	private IEnumerator DelayTriggerInitializeComplete()
	{
		while(true)
		{
			yield return new WaitForEndOfFrame();
			if(itemList.IsStarted) break;
		}

		yield return new WaitForSeconds(0.1f);
		InitializeComplete();
	}
}
