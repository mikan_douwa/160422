﻿using UnityEngine;
using System.Collections;

public class ItemInfo : SubmitWindow {

	public Item item;

	public UILabel itemName;

	public UILabel description;

	public UILabel count;

	private ItemData data;

	public static void Show(int itemId, bool usable)
	{
		var data = new ItemData{ ItemID = itemId, Usable = usable };

		if(usable)
		{
			var msgBox = MessageBox.Show("Common/ItemInfo","", SystemTextID._174_BTN_USE, SystemTextID.BTN_DISABLE);
			msgBox.args = new object[]{ data };
			msgBox.colors = new ButtonColor[]{ ButtonColor.Green, ButtonColor.None };
		}
		else
		{
			var msgBox = MessageBox.Show("Common/ItemInfo","", SystemTextID.BTN_DISABLE);
			msgBox.args = new object[]{ data };
		}
	}

	public static void Show(int itemId)
	{
		Show (itemId, false);
	}

	protected override void OnReady ()
	{
		data = context.args[0] as ItemData;

		item.ItemID = data.ItemID;
		item.Count = 0;
		item.ApplyChange();

		itemName.text = ConstData.GetItemName(data.ItemID);

		var tip = ConstData.GetItemTip(data.ItemID);

		var itemCount = GameSystem.Service.ItemInfo.Count(data.ItemID);

		/*
		if(item.ItemID == ConstData.Tables.RescueRewardItemID)
		{
			var rescue = GameSystem.Service.QuestInfo.Rescue;
			if(rescue != null)
			{
				var endTime = rescue.EndTime.AddDays(7);
				description.text = Mikan.CSAGA.ConstData.Tables.Format(tip, endTime.ToString("yyyy/MM/dd HH:mm:ss"));
				if(endTime < GameSystem.Service.CurrentTime) itemCount = 0;
			}
			else
			{
				description.text = Mikan.CSAGA.ConstData.Tables.Format(tip, "--");
				itemCount = 0;
			}
		}
		else*/
			description.text = tip;


		count.text = ConstData.GetSystemText(SystemTextID.CURRENT_COUNT_V1, itemCount);

		if(data.Usable)
		{
			if(itemCount < ConstData.Tables.Item[data.ItemID].n_EFFECT_Y) SetButtonEnabled(0, false);
		}
	}

	protected override bool OnSubmit ()
	{
		if(data == null || !data.Usable) return true;

		if(!ShopManager.Instance.CheckCardSpace(1)) return true;
		if(!QuestManager.Instance.CheckEquipmentSpace(1)) return true;

		GameSystem.Service.ItemUse(data.ItemID);

		return true;
	}
}
