﻿using UnityEngine;
using System.Collections;

public class Singleton<T> : MonoBehaviour
	where T : Component
{
	private static T instance;
	public static T Instance
	{
		get
		{
			if(instance != null) return instance;

			Create();

			return instance;
		}
	}

	public static void Create()
	{
		if(instance != null) return;

		var gameSys = FindObjectOfType<GameSystem>();
		
		if(gameSys != null) instance = gameSys.gameObject.AddMissingComponent<T>();
		
		if(instance == null)
		{
			instance = FindObjectOfType<T>();
			if(instance) GameSystem.LogWarning("singleton already exists, will not be managed, type = {0}", typeof(T));
		}
	}
}
