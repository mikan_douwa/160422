﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Mikan.CSAGA.ConstData;

public class RankingRewardView : View {
	
	public UITable table;
	public GameObject itemPrefab;
	public UIScrollView scrollView;
	
	protected override void Initialize ()
	{
		int id = Context.Instance.EventScheduleID;
		EventSchedule schedule = ConstData.Tables.EventSchedule[id];
		if(schedule != null)
		{
			Dictionary<int, RankingGroupData> dict = new Dictionary<int, RankingGroupData>();
			foreach(Trophy trophy in ConstData.Tables.Trophy.Select(o=>{ return (int)o.n_IDSET == schedule.n_RANKING; }))
			{
				int lvBottom = trophy.n_EFFECT[0];
				if(dict.ContainsKey(lvBottom))
				{
					RankingGroupData data = dict[lvBottom];
					data.bonusList.Add(trophy.n_BONUS_LINK);
				}
				else
				{
					RankingGroupData data = new RankingGroupData(lvBottom, trophy.n_EFFECT[1], new List<int>(){ trophy.n_BONUS_LINK });
					dict.Add(lvBottom, data);
				}
			}
			
			foreach(RankingGroupData data in dict.Values)
			{
				RankingGroupRenderer rank = NGUITools.AddChild(table.gameObject, itemPrefab).GetComponent<RankingGroupRenderer>();
				rank.Init(data);
				rank.GetComponent<UIDragScrollView>().scrollView = scrollView;
			}
			
			if(schedule.n_POINT > 0) appendixTextID = SystemTextID._207_POINT_REWARD;
		}
		
		//table.Reposition();
		table.repositionNow = true;

		SafeStartCoroutine(WaitFor());
		//InitializeComplete();
	}

	private IEnumerator WaitFor()
	{
		while(table.enabled) yield return new WaitForEndOfFrame();
		yield return new WaitForFixedUpdate();
		InitializeComplete();
		//while(table.repositionNow
	}
}
