﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Mikan.CSAGA.ConstData;

public class ScoreRewardView : RewardListView {

	protected override void Setup ()
	{
		int id = Context.Instance.EventScheduleID;
		EventSchedule schedule = ConstData.Tables.EventSchedule[id];
		if(schedule != null)
		{
			var isPVP = (schedule.n_TYPE == Mikan.CSAGA.EventScheduleType.PVP);

			List<RewardData> lst = new List<RewardData>();
			foreach(Trophy trophy in ConstData.Tables.Trophy.Select(o=>{ return (int)o.n_IDSET == schedule.n_POINT; }))
			{
				lst.Add(new RewardData(){ TrophyConstObj = trophy, IsPVPReward = isPVP });
			}
			
			lst.Sort(Compare);
			Setup(lst);
			
			if(schedule.n_RANKING > 0 && schedule.n_RANKING != 990001) appendixTextID = SystemTextID._206_RANKING_REWARD;
		}
	}
	
	private static int Compare(RewardData lhs, RewardData rhs)
	{
		return lhs.TrophyConstObj.n_EFFECT[0].CompareTo(rhs.TrophyConstObj.n_EFFECT[0]);
	}
}
