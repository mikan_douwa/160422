﻿using UnityEngine;
using System.Collections;

public class RankingInfoHost : MonoBehaviour {
	
	private bool isEnabled = false;
	// Use this for initialization
	void Start () 
	{
		isEnabled = true;
		RankingInfo.Show();
	}

	void OnDestroy()
	{
		if(isEnabled) RankingInfo.Hide();
	}

}
