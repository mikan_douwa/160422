﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RankingListView : View {

	public enum TAG
	{
		Top10,
		MyRanking,
	}

	public ItemList itemList;
	
	private Mikan.CSAGA.Data.Ranking data;
	
	private List<RankingItemData> list;

	protected override void Initialize ()
	{
		data = GameSystem.Service.RankingInfo[Context.Instance.EventScheduleID];
		
		if(data == null || data.PlayerCount < ConstData.Tables.RankingPlayerLimit)
		{
			InitializeComplete();
			MessageBox.Show(SystemTextID._211_EVENT_COMMITING).callback = (index)=>{ Message.Send(MessageID.Back); };
		}
		else
		{
			var from = 0;
			
			if(Context.Instance.RankingTag == TAG.MyRanking)
			{
				appendixTextID = SystemTextID._208_TOP;

				if(data.Point > 0)
					from = -1;
				else
				{
					InitializeComplete();
					return;
				}
			}
			else
			{
				if(data.Point == 0) 
					appendixTextID = SystemTextID.NONE;
				else
					appendixTextID = SystemTextID._209_MY_RANKING;
			}
			
			
			GameSystem.Service.OnRankingQuery += OnRankingQuery;
			GameSystem.Service.RankingQuery(Context.Instance.EventScheduleID, from);
		}
	}

	protected override void OnDestroy ()
	{
		if(GameSystem.IsAvailable) 
		{
			GameSystem.Service.OnRankingQuery -= OnRankingQuery;
			GameSystem.Service.OnRankingPlayer -= OnRankingPlayer;
		}

		base.OnDestroy ();
	}

	private void OnRankingQuery(int eventId, List<Mikan.CSAGA.Data.RankingPlayer> players)
	{
		GameSystem.Service.OnRankingQuery -= OnRankingQuery;
		
		list = new List<RankingItemData>();
		
		var playerIds = new List<string>();
		
		foreach(var item in players)
		{
			var obj = new RankingItemData{ Origin = item, EventData = data };
			
			if(item.PublicID == GameSystem.Service.Profile.PublicID)
			{
				obj.PlayerName = "[ffff00]" + GameSystem.Service.PlayerInfo.Name;
			}
			else
			{
				var playerData = GameSystem.Service.SocialInfo[item.PublicID];
				if(playerData == null)
					playerIds.Add(item.PublicID);
				else
					obj.PlayerName = "[000000]" + playerData.Name;
			}
			list.Add(obj);
		}
		
		if(playerIds.Count == 0)
		{
			itemList.Setup(list);
			InitializeComplete();
		}
		else
		{
			GameSystem.Service.OnRankingPlayer += OnRankingPlayer;
			GameSystem.Service.SocialQueryRanking(playerIds);
		}
	}
	
	private void OnRankingPlayer()
	{
		GameSystem.Service.OnRankingPlayer -= OnRankingPlayer;
		
		foreach(var item in list)
		{
			if(!string.IsNullOrEmpty(item.PlayerName)) continue;
			
			var playerData = GameSystem.Service.RankingInfo.GetPlayer(item.Origin.PublicID);
			
			if(playerData != null)
				item.PlayerName = "[000000]" + playerData.Name;
			else
				item.PlayerName = "[000000]???";
		}
		
		itemList.Setup(list);
		
		InitializeComplete();
	}
}
