﻿using UnityEngine;
using System.Collections;

public class RankingListItemRenderer : ItemRenderer<RankingItemData> {

	public UnitLoader unit;
	public UILabel playerName;
	public UILabel ranking;
	public UILabel point;

	public GameObject rankingObj;

	#region implemented abstract members of ItemRenderer

	protected override void ApplyChange (RankingItemData data)
	{
		unit.CardID = data.Origin.CardID;
		unit.ApplyChange();

		playerName.text = data.PlayerName;

		if(data.Origin.PublicID == GameSystem.Service.Profile.PublicID)
			playerName.effectStyle = UILabel.Effect.Outline;
		else
			playerName.effectStyle = UILabel.Effect.None;

		point.text = ConstData.GetSystemText(SystemTextID._203_EVENT_POINT) + " " + data.Origin.Point.ToString();

		if(Context.Instance.StageID == StageID.RankingSelf)
			rankingObj.SetActive(false);
		else
			ranking.text = string.Format("{0}\n{1}", ConstData.GetSystemText(SystemTextID._208_TOP), data.Origin.Ranking + 1);
	}

	#endregion



}

public class RankingItemData
{
	public Mikan.CSAGA.Data.Ranking EventData;
	public Mikan.CSAGA.Data.RankingPlayer Origin;
	public string PlayerName;
}