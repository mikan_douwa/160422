﻿using UnityEngine;
using System.Collections;
using Mikan.CSAGA.ConstData;

public class RankingDetailRenderer : UIBase {

	public int BonusLink;
	public Icon icon;

	protected override void OnChange ()
	{
		Drop data = ConstData.Tables.Drop[BonusLink];
		if(data != null)
		{
			icon.IconType = data.n_DROP_TYPE;
			icon.IconID = data.n_DROP_ID;
			icon.Count = data.n_COUNT;
			icon.Rare = data.n_RARE;
			icon.Luck = data.n_LUCK + 1;
			icon.ApplyChange();
		}
	}

	void OnLongPress()
	{
		icon.ShowInfo();
	}
}
