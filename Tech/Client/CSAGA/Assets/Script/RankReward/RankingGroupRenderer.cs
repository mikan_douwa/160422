﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class RankingGroupData
{
	public int rankFrom;
	public int rankTo;
	public List<int> bonusList;

	public RankingGroupData(int from, int to, List<int> lst)
	{
		rankFrom = from;
		rankTo = to;
		bonusList = lst;
	}
}


public class RankingGroupRenderer : MonoBehaviour {

	public RankingGroupData rData;

	public UILabel rankRange;
	public UIGrid grid;
	public GameObject detailPrefab;

	private UIWidget widget;
	public UIWidget Widget{
		get{
			if(widget == null)
				widget = GetComponent<UIWidget>();
			return widget;
		}
	}

	public void Init(RankingGroupData data)
	{
		rData = data;
		rankRange.text = string.Format("{0}% ~ {1}%", rData.rankFrom.ToString(), rData.rankTo.ToString());

		int detailHeight = 0;
		for(int i = 0; i < rData.bonusList.Count; i++)
		{
			int bonusId = rData.bonusList[i];
			RankingDetailRenderer detail = NGUITools.AddChild(grid.gameObject, detailPrefab).GetComponent<RankingDetailRenderer>();
			detail.BonusLink = bonusId;
			detail.ApplyChange();

			if(detailHeight == 0)
				detailHeight = detail.GetComponent<UIWidget>().height;
		}

		Widget.height = -(int)grid.transform.localPosition.y + detailHeight * rData.bonusList.Count + 20;
		grid.Reposition();
	}
}
