﻿using UnityEngine;
using System.Collections;

public class Report : MonoBehaviour {

	private string errMsg;

	// Use this for initialization
	void Start () 
	{
		Application.RegisterLogCallback(LogCallback);
		GameSystem.Service.OnReport += OnReport;
	}

	void OnReport (Mikan.CSAGA.ReportType type)
	{
		if(type == Mikan.CSAGA.ReportType.Error)
			MessageBox.Show("Common/ReportMessageBox", ConstData.GetSystemText(SystemTextID._237_TIP_INTERNAL_ERROR_REPORTED), SystemTextID._238_BTN_CONTINTUE_GAME, SystemTextID._241_BTN_RETURN_TITLE).callback = OnMessageBoxClose;
	}

	void OnDestroy() 
	{
		Application.RegisterLogCallback(null);
	}
	
	private void LogCallback (string condition, string stackTrace, LogType type)
	{
		if(type == LogType.Error || type == LogType.Exception)
		{
			errMsg = string.Format("{0}\n[{1} {2}]\n{3}\n{4}", type, Application.platform, UIDefine.AppVersion,  condition, stackTrace);

			MessageBox.Show("Common/ReportMessageBox", ConstData.GetSystemText(SystemTextID._236_TIP_INTERNAL_ERROR), SystemTextID._238_BTN_CONTINTUE_GAME, SystemTextID._241_BTN_RETURN_TITLE, SystemTextID._239_BTN_REPORT_ERROR).callback = OnMessageBoxClose;
		}
	}

	private void OnMessageBoxClose(int index)
	{
		switch(index)
		{
		case 0:
			break;
		case 1:
			Application.LoadLevel(0);
			break;
		case 2:
			if(GameSystem.IsAvailable) GameSystem.Service.Report(Mikan.CSAGA.ReportType.Error, errMsg);
			break;
		}
	}

}
