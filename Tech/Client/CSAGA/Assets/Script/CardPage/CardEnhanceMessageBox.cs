﻿using UnityEngine;
using System.Collections;
using Mikan.CSAGA;

public class CardEnhanceMessageBox : MessageBox {

	public FightResLoader fx;

	public TweenAlpha lvUpFx;

	public CardProfile currentProfile;

	public CardProfile expectProfile;

	public UISlider slider;

	public UILabel remains;

	public UILabel expense;

	private CardData card;

	private Mikan.CSAGA.ConstData.CardData data;
	private Mikan.CSAGA.ConstData.CardBalance balance;

	private int expMax;

	private int exp;

	private int originLV;

	private bool isRecursive = false;

	private float timestamp = 0;

	private float scale = 1;

	public void OnSliderValueChange()
	{
		if(card == null || isRecursive) return;

		exp = (int)(expMax * UISlider.current.value);

		UpdateExp();
	}
	
	private void OnIncrease(IMessage msg)
	{
		isRecursive = true;

		var current = Time.time;

		if(current - timestamp < 0.1f) 
		{
			scale *= 1.05f;

			if(scale > 5) scale = 5;
		}
		else
		{
			scale = 1f;
		}

		timestamp = current;

		var lv = CardUtils.CalcLV(data, balance, card.Origin.Exp + exp);

		var newLV = lv + (int)((int)msg.Data * scale);

		newLV = Mathf.Max(1, Mathf.Min(newLV, balance.n_LVMAX));

		var newExp = CardUtils.CalcExp(data, newLV);

		var newValue = newExp - card.Origin.Exp;

		newValue = Mathf.Max(0, Mathf.Min(newValue, expMax));

		if(newValue != exp) 
		{
			exp = newValue;

			slider.value = (float)exp / (float)expMax;

			UpdateExp();
		}

		isRecursive = false;
	}

	private void UpdateExp()
	{
		expectProfile.UpdateProperty(exp);
		
		expense.text = "[000000]" + ConstData.GetSystemText(SystemTextID.EXPENSE_V1, FormatExpExpense(exp));
	}

	protected override void OnReady ()
	{
		fx.Preload("StrengthenFX");

		card = global::Context.Instance.Card;

		Reload();

		card.Origin.OnChange += OnCardChange;

		Message.AddListener(MessageID.Increase, OnIncrease);

	}

	private void Reload()
	{
		currentProfile.UpdateProperty(0);

		data = ConstData.Tables.Card[card.CardID];
		balance = ConstData.Tables.CardBalance[card.Origin.Rare];
		expMax = CardUtils.CalcExp(card.CardID, balance.n_LVMAX) - card.Origin.Exp;
		originLV = CardUtils.CalcLV(data, balance, card.Origin.Exp);

		remains.text = "[000000]" + ConstData.GetSystemText(SystemTextID.CRYSTAL_V1, GameSystem.Service.PlayerInfo[DropType.Crystal]);
		expense.text = "[000000]" + ConstData.GetSystemText(SystemTextID.EXPENSE_V1, FormatExpExpense(0));

		slider.value = 0;
	}

	void OnCardChange (Mikan.CSAGA.Data.Card obj)
	{
		var originLV = this.originLV;

		Reload();
		
		fx.PlayFX("StrengthenFX", Vector3.zero, fx.gameObject);
		Message.Send(MessageID.PlaySound, SoundID.POWERUP);

		if(originLV != this.originLV)
		{
			lvUpFx.ResetToBeginning();
			lvUpFx.PlayForward();
		}
	}

	protected override void OnDestroy ()
	{
		base.OnDestroy ();
		if(card != null) card.Origin.OnChange -= OnCardChange;

		Message.RemoveListener(MessageID.Increase, OnIncrease);
	}

	protected override void OnClickButton (int index)
	{
		if(index == 0)
		{
			if(exp > GameSystem.Service.PlayerInfo[DropType.Crystal]) 
			{
				MessageBox.Show(SystemTextID._260_TIP_CRYSTAL_NOT_ENOUGH);
				return;
			}

			if(exp > 0)	GameSystem.Service.CardEnhance(card.Origin.Serial, exp);
		}
		else
			base.OnClickButton (index);
	}

	private string FormatExpExpense(int exp)
	{
		var remains = GameSystem.Service.PlayerInfo[DropType.Crystal];
		if(exp > remains) return "[ff0000]" + exp.ToString();
		return exp.ToString();
	}
}
