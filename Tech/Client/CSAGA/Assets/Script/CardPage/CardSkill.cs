﻿using UnityEngine;
using System.Collections;

public class CardSkill : UIBase {

	public SystemTextID prefix = SystemTextID.NONE;

	public int SkillID;

	public UILabel skillName;
	public ScrollableLabel skillTip;
	public UILabel skillTip2;
	public UILabel skillCD;

	protected override void OnChange ()
	{
		if(skillCD != null)
		{
			var data  = ConstData.Tables.Skill[SkillID];
			
			if(data != null && data.n_TYPE == 2 && data.n_CD > 0)
				skillCD.text = ConstData.GetSystemText(SystemTextID._287_SKILL_CD_V1, data.n_CD);
			else
				skillCD.text = "";
		}

		if(SkillID == 0)
		{
			skillName.text = "(none)";
			SetTip("(none)");
		}
		else
		{
			var text = ConstData.Tables.SkillText[SkillID];
			if(text != null)
			{
				skillName.text = Format(text.s_NAME);
				SetTip(text.s_TIP);
			}
			else
			{
				skillName.text = Format(string.Format("SKILL({0})", SkillID));
				SetTip(string.Format("SKILL({0})", SkillID));
			}
		}
	}

	private string Format(string name)
	{
		if(prefix == SystemTextID.NONE) return name;
		return ConstData.GetSystemText(prefix, name);
	}

	private void SetTip(string text)
	{
		if(skillTip != null)
		{
			skillTip.Text = text;
			skillTip.ApplyChange();
		}

		if(skillTip2 != null) skillTip2.text = text;
	}

}
