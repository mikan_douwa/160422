﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class VirtualCardTeamView : MonoBehaviour {

	public PresetNavigator navigator;

	public int defaultView = 0;
	public int selectOne = 1;
	public int selectMany = 2;
	public int selectSupport = -1;
	public int randomSupport = -1;

	public int Current { get { return navigator.current; } }

	private int prev = -1;

	private bool isInQuery = false;

	public bool Back()
	{
		if(Current == selectOne || Current == selectMany)
			Next (defaultView);
		else if(Current == selectSupport || Current == randomSupport)
			Next (prev);
		else
			return false;

		return true;
	}
	
	public bool Next()
	{
		if(Current == defaultView)
		{
			Context.Instance.Cards = new List<Mikan.Serial>(GameSystem.Service.TeamInfo[Context.Instance.TeamIndex].Members);
			Next (selectMany);
		}
		else if(Current == selectSupport)
		{
			Context.Instance.SocialAction = SocialMenuData.SOCIAL_ACTION.RANDOM;
			Next(randomSupport);
		}
		else if(Current == randomSupport)
		{
			Context.Instance.SocialAction = SocialMenuData.SOCIAL_ACTION.CHOOSE_COMRADE;
			Next(selectSupport);
		}
		else
			return false;

		return true;
	}
	
	public void Next(int index)
	{
		navigator.Next(index);
	}

	void Start ()
	{
		Message.AddListener(MessageID.SwitchTeamMember, OnSwitchTeamMember);
		Message.AddListener(MessageID.SelectTeamMember, OnSelectTeamMember);
		Message.AddListener(MessageID.EditTeamConfirm, OnEditTeamConfirm);
		Message.AddListener(MessageID.SelectSupport, OnSelectSupport);
		Message.AddListener(MessageID.ConfirmSupport, OnConfirmSupport);
		GameSystem.Service.OnSocialInfo += OnSocialInfo;
	}



	void OnDestroy ()
	{
		if(GameSystem.IsAvailable) GameSystem.Service.OnSocialInfo -= OnSocialInfo;

		Message.RemoveListener(MessageID.SwitchTeamMember, OnSwitchTeamMember);
		Message.RemoveListener(MessageID.SelectTeamMember, OnSelectTeamMember);
		Message.RemoveListener(MessageID.EditTeamConfirm, OnEditTeamConfirm);
		Message.RemoveListener(MessageID.SelectSupport, OnSelectSupport);
		Message.RemoveListener(MessageID.ConfirmSupport, OnConfirmSupport);
	}

	void OnSocialInfo ()
	{
		if(!isInQuery) return;

		isInQuery = false;

		foreach(var item in GameSystem.Service.SocialInfo.List)
		{
			if(item.Relation == Mikan.CSAGA.Relation.Follow || item.Relation == Mikan.CSAGA.Relation.CrossFollow)
			{
				Context.Instance.SocialAction = SocialMenuData.SOCIAL_ACTION.CHOOSE_COMRADE;
				Next(selectSupport);
				return;
			}
		}

		Context.Instance.SocialAction = SocialMenuData.SOCIAL_ACTION.RANDOM;
		Next(randomSupport);
	}

	private void OnSwitchTeamMember(IMessage msg)
	{
		Context.Instance.TeamMemberIndex = (int)msg.Data;
		
		Next (selectOne);
	}
	
	private void OnSelectTeamMember()
	{
		var data = Context.Instance.Card;
		
		GameSystem.Service.TeamUpdate(Context.Instance.TeamIndex, Context.Instance.TeamMemberIndex, data.Origin != null ? data.Origin.Serial : Mikan.Serial.Empty);
		
		Next(defaultView);
	}
	
	private void OnEditTeamConfirm()
	{
		if(Current != selectMany) return;
		
		var list = new List<Mikan.Serial>(Context.Instance.Cards);
		while(list.Count < 5) list.Add(Mikan.Serial.Empty);
		GameSystem.Service.TeamUpdate(Context.Instance.TeamIndex, list);
		Next(defaultView);
	}

	private void OnSelectSupport(IMessage msg)
	{
		prev = Current;

		Context.Instance.TeamMemberIndex = (int)msg.Data;
			
		isInQuery = true;
		GameSystem.Service.GetSocialInfo();
	}

	private void OnConfirmSupport()
	{
		GameSystem.Service.TeamCache(Context.Instance.CachedFellow.CachedTeamIndex, Context.Instance.CachedFellow.CachedTeamMemberIndex, Context.Instance.CachedFellow.Origin);
		Next(defaultView);
	}

}
