﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class CardTeamSubView : View {
	
	public bool subTotal = false;
	public UILabel hp;
	public UILabel atk;
	public UILabel rev;
	public UILabel dropRate;
	
	public CardSkill leaderSkill;
	public CardSkill subSkill;
	
	public Navigator navigator;

	public UILabel switchText;
	
	public SlipEffect[] objs;
	public ChallengeInfo challenge;

	private int current = 0;
	
	private IItemDataProvider<CardTeamData> provider;

	private QuestItemData cachedQuest;
	
	protected override void Initialize ()
	{
		GameSystem.Service.TeamReset();
		
		if(Context.Instance.CachedFellow != null)
		{
			if(Context.Instance.CachedFellow.Origin != null)
				GameSystem.Service.TeamCache(Context.Instance.CachedFellow.CachedTeamIndex, Context.Instance.CachedFellow.CachedTeamMemberIndex, Context.Instance.CachedFellow.Origin);
			Context.Instance.CachedFellow = null;
		}
		
		var count = GameSystem.Service.TeamInfo.Count;
		
		var list = new List<CardTeamData>(count);
		for(int i = 0; i < count; ++i) list.Add(new CardTeamData{ Index = i });
		provider = ListItemDataProvider<CardTeamData>.Create(list);
		
		navigator.OnCurrentChange += OnNavigatorCurrentChange;
		navigator.current = GameSystem.Service.TeamInfo.Selection;
		navigator.Setup(provider);
		
		GameSystem.Service.TeamInfo.OnChange += OnTeamInfoChange;
		GameSystem.Service.OnEquipmentReplace += OnEquipmentReplace;

		/*
		if(challenge != null)
		{
			switchText.text = ConstData.GetSystemText(SystemTextID._480_QUEST_MISSION);

			SwitchInfo(0);
		}
		*/

		Message.AddListener(MessageID.SwicthChallengeInfo, OnSwitchChallengeInfo);

		SwitchInfo(Context.Instance.ChallengeInfoButtonState, false);
		
		InitializeComplete();
	}

	public override void OnGetBack ()
	{
		if(cachedQuest != null && cachedQuest != Context.Instance.Quest)
			GameSystem.Service.TeamReset();

		cachedQuest = Context.Instance.Quest;

		OnTeamInfoChange();

		SwitchInfo(Context.Instance.ChallengeInfoButtonState, false);
	}

	public void OnSwitch()
	{
		//SwitchInfo((current + 1) % objs.Length);
	}

	private void OnSwitchChallengeInfo()
	{
		SwitchInfo((current + 1) % objs.Length, true);
	}

	private void SwitchInfo(int next, bool playAnimation)
	{
		if(objs == null || objs.Length == 0) return;

		Context.Instance.ChallengeInfoButtonState = next;
		Message.Send(MessageID.SwicthChallengeInfoEnd);

		/*
		if(Context.Instance.Quest.ChallengeList.Count == 0)
		{
			switchObj.SetActive(false);

			current = 0;
			objs[0].transform.localPosition = Vector3.zero;
			objs[1].transform.localPosition = UIDefine.OutScreen;
			return;
		}
		*/

		if(Context.Instance.Quest.ChallengeList.Count == 0)
		{
			objs[0].transform.localPosition = Vector3.zero;
			objs[1].transform.localPosition = UIDefine.OutScreen;
			return;
		}

		challenge.ApplyChange();

		//switchObj.SetActive(true);

		if(playAnimation)
		{
			if(next > current)
			{
				objs[next].from = new Vector3(640, 0);
				objs[next].to = Vector3.zero;
				objs[next].Play();

				objs[current].from = Vector3.zero;
				objs[current].to = new Vector3(-640, 0);
				objs[current].Play();
			}
			else
			{
				objs[next].from = new Vector3(-640, 0);
				objs[next].to = Vector3.zero;
				objs[next].Play();
				
				objs[current].from = Vector3.zero;
				objs[current].to = new Vector3(640, 0);
				objs[current].Play();
			}

			SlipEndProcess(objs[current], objs[next]);
		}
		else if(current == next)
		{
			if(current == 0)
			{
				objs[0].transform.localPosition = Vector3.zero;
				objs[1].transform.localPosition = UIDefine.OutScreen;
			}
			else
			{
				objs[1].transform.localPosition = Vector3.zero;
				objs[0].transform.localPosition = UIDefine.OutScreen;
			}
		}
		else
		{
			objs[next].transform.localPosition = Vector3.zero;
			objs[current].transform.localPosition = UIDefine.OutScreen;
		}

		current = next;
	}

	private void SlipEndProcess(SlipEffect current, SlipEffect next)
	{
		current.onEnd = ()=> current.transform.localPosition = UIDefine.OutScreen;
		next.onEnd = null;
	}

	void OnEquipmentReplace ()
	{
		UpdateProperty();
	}

	protected override void OnDestroy ()
	{
		Message.RemoveListener(MessageID.SwicthChallengeInfo, OnSwitchChallengeInfo);

		if(GameSystem.IsAvailable) 
		{
			GameSystem.Service.OnEquipmentReplace -= OnEquipmentReplace;
			GameSystem.Service.TeamInfo.OnChange -= OnTeamInfoChange;
			GameSystem.Service.TeamInfo.ApplyChange();
		}

		base.OnDestroy ();
	}
	
	void OnTeamInfoChange ()
	{
		var renderer = navigator.GetRelativeItemRenderer(navigator.Current);
		if(renderer != null) renderer.ApplyChange();
		
		UpdateProperty();
	}
	
	void OnNavigatorCurrentChange (Navigator navigator)
	{
		var current = provider[navigator.Current];
		Context.Instance.TeamIndex = current.Index;
		
		UpdateProperty();
		
		GameSystem.Service.TeamSelect(current.Index);
		
		Message.Send(MessageID.SwitchTeam);
	}
	
	private void UpdateProperty()
	{
		var team = GameSystem.Service.TeamInfo[Context.Instance.TeamIndex];
		
		if(subTotal)
		{
			var hp = 0;
			var atk = 0;
			var rev = 0;
			
			foreach(var serial in team.Members)
			{
				var card = GameSystem.Service.CardInfo[serial];
				if(card == null) continue;
				
				var prop = card.Calc();
				prop.Append(card.SelectEquipment());
				hp += prop.HP;
				atk += prop.Atk;
				rev += prop.Rev;
			}
			
			this.hp.text = hp.ToString();
			this.atk.text = atk.ToString();
			this.rev.text = rev.ToString();
		}
		
		if(dropRate != null)
		{
			var drop = 0;
			for(int i = 0; i < 2; ++i)
			{
				var leader = GameSystem.Service.CardInfo[team.Members[i]];
				if(leader == null || leader.Serial.IsExternal) continue;
				var data = ConstData.Tables.Player[leader.Luck];
				if(data  == null) continue;
				drop += data.n_LUCK_DROP;
			}

			var add = 0;
			foreach(var item in GameSystem.Service.TacticsInfo.List)
			{
				var tactics = ConstData.Tables.Tactics[item];
				if(tactics.n_EFFECT == Mikan.CSAGA.TacticsEffect.Drop)
				{
					if(add < tactics.n_EFFECT_Z) add = tactics.n_EFFECT_Z;
				}
			}
			
			dropRate.text = string.Format("{0}+{1}%", ConstData.GetSystemText(SystemTextID._200_DROP_RATE), drop + add);
		}
		
		UpdateSkill(leaderSkill, team.Members[0]);
		UpdateSkill(subSkill, team.Members[1]);
	}
	
	private void UpdateSkill(CardSkill skill, Mikan.Serial serial)
	{
		var skillId = 0;
		
		var card = GameSystem.Service.CardInfo[serial];
		if(card != null) skillId = card.LeaderSkill;
		
		skill.SkillID = skillId;
		skill.ApplyChange();
	}
}
