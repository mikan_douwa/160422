﻿using UnityEngine;
using System.Collections;

public class CardTeamPage : Stage {

	public const int DEFAULT = 0;
	public const int SELECT_ONE = 1;
	public const int SELECT_MANY = 2;

	public PresetNavigator navigator;

	public VirtualCardTeamView view;

	protected override void Initialize ()
	{
		SafeStartCoroutine(WaitForInitialized());
	}

	protected override void OnBack ()
	{
		if(!view.Back()) base.OnBack();
	}

	protected override void OnNext ()
	{
		view.Next();
	}

	private IEnumerator WaitForInitialized()
	{
		navigator.Next(DEFAULT);
		while(navigator.currentObj == null) yield return new WaitForEndOfFrame();

		view = GetComponent<VirtualCardTeamView>();

		InitializeComplete();
	}



}
