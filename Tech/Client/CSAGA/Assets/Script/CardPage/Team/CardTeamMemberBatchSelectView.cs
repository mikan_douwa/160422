﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class CardTeamMemberBatchSelectView : CardListView {
	
	public UIButton confirmButton;
	
	public CardList preview;
	
	protected override void Initialize ()
	{
		Message.AddListener(MessageID.SelectedChange, OnSelectItem);

		base.Initialize ();
	}
	
	protected override void OnDestroy ()
	{
		Message.RemoveListener(MessageID.SelectedChange, OnSelectItem);
		
		base.OnDestroy ();
	}

	public override void OnGetBack ()
	{
		base.OnGetBack();
		preview.Rebuild();
	}

	protected override void OnSelectItem (IMessage msg)
	{
		preview.Refresh();

		var list = Context.Instance.Cards;
		confirmButton.isEnabled = list.Count > 0 && !list[0].IsEmpty;
	}
}
