﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CardTeamMemberListItemRenderer : ItemRenderer<CardTeamData> {

	public CardTeamMemberList itemList;
	
	#region implemented abstract members of ItemRenderer

	protected override void ApplyChange (CardTeamData data)
	{
		var team = GameSystem.Service.TeamInfo[data.Index];
		var list = new List<CardData>(team.Members.Count);
		foreach(var serial in team.Members)
		{
			var obj = new CardData();
			if(!serial.IsEmpty)
			{
				obj.Origin = GameSystem.Service.CardInfo[serial];
				obj.CardID = obj.Origin.ID;
				obj.IsLocked = obj.Origin.IsLocked;
			}
			list.Add(obj);
		}
		itemList.Setup(list);
	}

	#endregion



}
