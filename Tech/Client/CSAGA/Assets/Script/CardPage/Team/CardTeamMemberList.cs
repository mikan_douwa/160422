﻿using UnityEngine;
using System.Collections;

public class CardTeamMemberList : ItemList {

	protected override ItemRenderer GetItemRenderer (GameObject item)
	{
		return item.AddComponent<CardTeamMemberItemRenderer>();
	}
}

public class CardTeamData
{
	public int Index;
}