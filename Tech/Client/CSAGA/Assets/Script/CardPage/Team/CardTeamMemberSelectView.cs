﻿using UnityEngine;
using System.Collections;

public class CardTeamMemberSelectView : CardListView {

	private int cachedTeamMemberIndex = -1;

	protected override bool NeedRebuild {
		get {
			if(cachedTeamMemberIndex == Context.Instance.TeamMemberIndex) return false;
			var origin = cachedTeamMemberIndex;
			UpdateCache();
			if(origin > 0 && cachedTeamMemberIndex > 0) return false;
			return true;
		}
	}

	protected override void OnInitializeComplete ()
	{
		UpdateCache();
	}

	private void UpdateCache()
	{
		cachedTeamMemberIndex = Context.Instance.TeamMemberIndex;
	}
}
