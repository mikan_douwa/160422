﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class CardTeamView : View {

	public const int DEFAULT = 0;
	public const int SELECT_ONE = 1;
	public const int SELECT_MANY = 2;
	public const int SELECT_FELLOW = 3;

	public PresetNavigator navigator;

	public int Current { get { return navigator.current; } }

	public void Back()
	{
		switch(Current)
		{
		case SELECT_ONE:
		case SELECT_MANY:
			Next(DEFAULT);
			break;
		}
	}

	public void Next()
	{
		switch(Current)
		{
		case DEFAULT:
			Context.Instance.Cards = new List<Mikan.Serial>(GameSystem.Service.TeamInfo[Context.Instance.TeamIndex].Members);
			Next (SELECT_MANY);
			break;
		case SELECT_ONE:
		case SELECT_MANY:
			break;
		}
	}

	public void Next(int index)
	{
		navigator.Next(index);
	}

	protected override void OnDestroy ()
	{
		Message.RemoveListener(MessageID.SwitchTeamMember, OnSwitchTeamMember);
		Message.RemoveListener(MessageID.SelectTeamMember, OnSelectTeamMember);
		Message.RemoveListener(MessageID.EditTeamConfirm, OnEditTeamConfirm);

		navigator.OnCurrentChange -= OnCurrentChange;
		base.OnDestroy ();
	}

	protected override void Initialize ()
	{
		Message.AddListener(MessageID.SwitchTeamMember, OnSwitchTeamMember);
		Message.AddListener(MessageID.SelectTeamMember, OnSelectTeamMember);
		Message.AddListener(MessageID.EditTeamConfirm, OnEditTeamConfirm);

		navigator.OnCurrentChange += OnCurrentChange;
		navigator.Preload(DEFAULT);

		navigator.Next();
		
		InitializeComplete();
	}

	private void OnSwitchTeamMember(IMessage msg)
	{
		Context.Instance.TeamMemberIndex = (int)msg.Data;
		
		Next (SELECT_ONE);
	}
	
	private void OnSelectTeamMember()
	{
		var data = Context.Instance.Card;
		
		GameSystem.Service.TeamUpdate(Context.Instance.TeamIndex, Context.Instance.TeamMemberIndex, data.Origin != null ? data.Origin.Serial : Mikan.Serial.Empty);

		Next (DEFAULT);
	}

	private void OnEditTeamConfirm()
	{
		if(Current != SELECT_MANY) return;

		var list = new List<Mikan.Serial>(Context.Instance.Cards);
		while(list.Count < 5) list.Add(Mikan.Serial.Empty);
		GameSystem.Service.TeamUpdate(Context.Instance.TeamIndex, list);
		Next(DEFAULT);
	}

	void OnCurrentChange (PresetNavigator obj)
	{
		switch(Current)
		{
		case DEFAULT:
			UpdateAppendix(null, ConstData.GetSystemText(SystemTextID.BATCH_SELECT));
			break;
		case SELECT_ONE:
		case SELECT_MANY:
			UpdateAppendix(Essentials.Instance.sortButton, null);
			break;
		}
	}

	/*


	void OnEquipmentReplace ()
	{
		UpdateProperty();
	}

	protected override void OnNext ()
	{
		Context.Instance.Cards = new List<Mikan.Serial>(GameSystem.Service.TeamInfo[Context.Instance.TeamIndex].Members);
		
		Message.Send(MessageID.SwitchStage, StageID.CardTeamMemberBatchSelect);
	}
	

	protected override void OnDestroy ()
	{
		Message.RemoveListener(MessageID.SwitchTeamMember, OnSwitchTeamMember);
		Message.RemoveListener(MessageID.SelectTeamMember, OnSelectTeamMember);
		
		if(GameSystem.IsAvailable) 
		{
			GameSystem.Service.OnEquipmentReplace -= OnEquipmentReplace;
			GameSystem.Service.TeamInfo.OnChange -= OnTeamInfoChange;
			GameSystem.Service.TeamInfo.ApplyChange();
		}
		
		base.OnDestroy ();
	}
	
	void OnTeamInfoChange ()
	{
		var renderer = navigator.GetRelativeItemRenderer(navigator.Current);
		if(renderer != null) renderer.ApplyChange();
		
		UpdateProperty();
	}
	
	private void OnSwitchTeamMember(IMessage msg)
	{
		Context.Instance.TeamMemberIndex = (int)msg.Data;
		
		Message.Send(MessageID.SwitchStage, StageID.CardTeamMemberSelect);
	}
	
	private void OnSelectTeamMember()
	{
		var data = Context.Instance.Card;
		
		GameSystem.Service.TeamUpdate(Context.Instance.TeamIndex, Context.Instance.TeamMemberIndex, data.Origin != null ? data.Origin.Serial : Mikan.Serial.Empty);
	}
	
	void OnNavigatorCurrentChange (Navigator navigator)
	{
		var current = provider[navigator.Current];
		Context.Instance.TeamIndex = current.Index;
		
		UpdateProperty();
		
		GameSystem.Service.TeamSelect(current.Index);
		
		Message.Send(MessageID.SwitchTeam);
	}
	
	private void UpdateProperty()
	{
		var team = GameSystem.Service.TeamInfo[Context.Instance.TeamIndex];
		
		if(subTotal)
		{
			var hp = 0;
			var atk = 0;
			var rev = 0;
			
			foreach(var serial in team.Members)
			{
				var card = GameSystem.Service.CardInfo[serial];
				if(card == null) continue;
				
				var prop = card.Calc();
				prop.Append(card.SelectEquipment());
				hp += prop.HP;
				atk += prop.Atk;
				rev += prop.Rev;
			}
			
			this.hp.text = hp.ToString();
			this.atk.text = atk.ToString();
			this.rev.text = rev.ToString();
		}
		
		if(dropRate != null)
		{
			var drop = 0;
			for(int i = 0; i < 2; ++i)
			{
				var leader = GameSystem.Service.CardInfo[team.Members[i]];
				if(leader == null || leader.Serial.IsExternal) continue;
				var data = ConstData.Tables.Player[leader.Luck];
				if(data  == null) continue;
				drop += data.n_LUCK_DROP;
			}
			
			dropRate.text = string.Format("{0}+{1}%", ConstData.GetSystemText(SystemTextID._200_DROP_RATE), drop);
		}
		
		UpdateSkill(leaderSkill, team.Members[0]);
		UpdateSkill(subSkill, team.Members[1]);
	}
	
	private void UpdateSkill(CardSkill skill, Mikan.Serial serial)
	{
		var skillId = 0;
		
		var card = GameSystem.Service.CardInfo[serial];
		if(card != null) skillId = card.LeaderSkill;
		
		skill.SkillID = skillId;
		skill.ApplyChange();
	}
	*/
}
