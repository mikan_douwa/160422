﻿using UnityEngine;
using System.Collections;

public class CardTeamIndicator : ItemRenderer<CardTeamData> {
	
	public UIToggle toggle;

	protected override void OnStart ()
	{
		base.OnStart ();
		Message.AddListener(MessageID.SwitchTeam, OnSwitchTeam);
	}

	void OnDestroy()
	{
		Message.RemoveListener(MessageID.SwitchTeam, OnSwitchTeam);
	}

	private void OnSwitchTeam()
	{
		toggle.value = Context.Instance.TeamIndex == data.Index;
	}

	#region implemented abstract members of ItemRenderer
	protected override void ApplyChange (CardTeamData data)
	{
		OnSwitchTeam();
	}
	#endregion



}
