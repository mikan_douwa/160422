﻿using UnityEngine;
using System.Collections;
using System;

public class CardTeamMemberItemRenderer : CardItemRenderer
{
	protected override bool UseDefaultFormat { get { return true; } }
	protected override void TriggerEvent ()
	{
		Message.Send(this, MessageID.SwitchTeamMember, Index);
	}
}

