﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RequireCardList : MonoBehaviour {

	public ItemList itemList;

	// Use this for initialization
	void Start () {
		itemList.GetItemRendererFunc = GetItemRenderer;

		var data = ConstData.Tables.Interactive[Context.Instance.InteractiveID];
		
		var list = new List<CardData>();

		for(int i = 1; i < data.n_CARDID.Length; ++i)
		{
			var item = data.n_CARDID[i];
			if(item != 0) list.Add(new CardData{ CardID = item });
		}
		
		itemList.Setup(list);
	}

	private ItemRenderer GetItemRenderer(GameObject obj)
	{
		var renderer = obj.AddMissingComponent<CardItemRenderer>();
		renderer.enableLongPress = false;
		return renderer;
	}
}
