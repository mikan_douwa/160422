﻿using UnityEngine;
using System.Collections;

public class EventRequireItemRenderer : ItemRenderer<RequireData>
{
	public Icon icon;
	#region implemented abstract members of ItemRenderer
	
	protected override void ApplyChange (RequireData data)
	{
		if(icon == null) icon = GetComponent<Icon>();
		
		icon.IconType = data.IconType;
		icon.IconID = data.IconID;
		icon.manualIconSprite = data.ManualIconSprite;
		icon.ApplyChange();
	}
	
	#endregion
	
	
}

public class RequireData
{
	public Mikan.CSAGA.DropType IconType = Mikan.CSAGA.DropType.None;
	public int IconID;
	public string ManualIconSprite;
}