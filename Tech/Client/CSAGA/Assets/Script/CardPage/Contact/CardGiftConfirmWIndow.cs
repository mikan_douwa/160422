﻿using UnityEngine;
using System.Collections;

public class CardGiftConfirmWIndow : SubmitWindow {

	public Item item;
	public UISlider slider;

	private bool isRecursive;

	private int origin;

	private int current = 1;

	private int max = 0;

	private bool isKizuna;

	private CardData cardData;

	private ItemData itemData;

	private Mikan.CSAGA.ConstData.Item rawdata;

	protected override void OnReady ()
	{
		Message.AddListener(MessageID.Increase, OnIncrease);

		cardData = Context.Instance.Card;
		itemData = Context.Instance.Item;

		rawdata = ConstData.Tables.Item[itemData.ItemID];



		if(rawdata.n_HP_MAX > 0)
		{
			SetupPlus(0);
		}
		else if(rawdata.n_ATK_MAX > 0)
		{
			SetupPlus(1);
		}
		else if(rawdata.n_REV_MAX > 0)
		{
			SetupPlus(2);
		}
		else
		{
			max = GameSystem.Service.ItemInfo.Count(itemData.ItemID);

			isKizuna = true;
		}



		item.ItemID = itemData.ItemID;

		UpdateItemCount();
	}

	protected override void OnDestroy ()
	{
		base.OnDestroy ();
		Message.RemoveListener(MessageID.Increase, OnIncrease);
	}

	#region implemented abstract members of SubmitWindow
	protected override bool OnSubmit ()
	{
		GameSystem.Service.CardGift(cardData.Origin.Serial, itemData.ItemID, current);
		return true;
	}
	#endregion

	public void OnSliderValueChange()
	{
		if(isRecursive) return;

		var total = max - origin - 1;

		if(total == 0) return;

		current = 1 + (int)(total * slider.value);

		UpdateItemCount();
	}
	
	private void OnIncrease(IMessage msg)
	{
		var diff = (int)msg.Data;

		current = Mathf.Min(Mathf.Max(current + diff, 1), max - origin);

		isRecursive = true;

		var total = max - origin - 1;

		if(total > 0) slider.value = (float)(current - 1) / (float)total;

		isRecursive = false;

		UpdateItemCount();
	}

	private void SetupPlus(int index)
	{
		origin = cardData.Origin.GetPlus(index);
		max = ConstData.Tables.CardBalance[cardData.Origin.Rare].n_PLUS;
		if(origin > max) origin = max;

		contentLabel.alignment = NGUIText.Alignment.Center;
		contentLabel.text = ConstData.GetSystemText(SystemTextID._196_USED_V1_V2_V3, ConstData.GetItemName(itemData.ItemID), origin, max);
	}

	private void UpdateItemCount()
	{
		item.Count = current;
		item.ApplyChange();

		if(isKizuna)
		{
			var add = rawdata.n_FAVOR * current;
			if(cardData.Origin.Data.n_GIFT == rawdata.n_EFFECT_Y) add = (int)(add * ConstData.Tables.GiftBonus);

			if(cardData.Origin.Kizuna + add > ConstData.Tables.KizunaLimit)
				contentLabel.text = ConstData.GetSystemText(SystemTextID._197_TIP_KIZUNA_OVERFLOW);
			else
				contentLabel.text = "";
		}
	}
}
