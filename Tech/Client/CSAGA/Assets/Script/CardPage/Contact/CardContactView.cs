﻿using UnityEngine;
using System.Collections;
using Mikan.CSAGA;
using System.Collections.Generic;

public class CardContactView : View {
	
	public InteractiveChara chara;
	
	public GameObject dialog;
	
	public UILabel kizuna;
	
	public UIProgressBar kizunaProgress;
	
	public UILabel total;
	
	public GameObject buttons;
	
	public GameObject functionArea;

	public PresetNavigator function;
	
	public FightResLoader fx;
	
	private CardData card;
	
	private UITweener tweener;
	
	private int originKizuna = 0;
	
	private int[] originPlus = new int[]{ 0, 0, 0 };
	
	enum STATE
	{
		Default,
		ChoiceList,
		GiftList,
		EventList,
		EndTalk,
	}
	
	private STATE state = STATE.Default;
	
	private STATE State 
	{
		get { return state; }
		set
		{
			if(state == value) return;
			
			if(value == STATE.Default)
			{
				buttons.SetActive(true);
				dialog.SetActive(true);
				functionArea.transform.DestroyChildren();
				
				if(state == STATE.ChoiceList || state == STATE.GiftList)
				{
					UpdateChara(0, 0);
				}
			}
			else
			{
				buttons.SetActive(false);
				
				switch(value)
				{
				case STATE.ChoiceList:
					ResourceManager.Instance.CreatePrefabInstance(functionArea, "GameScene/Card/ContactChoiceList");
					break;
				case STATE.GiftList:
					dialog.SetActive(false);
					ResourceManager.Instance.CreatePrefabInstance(functionArea, "GameScene/Card/GiftList");
					break;
				case STATE.EventList:
					dialog.SetActive(false);
					ResourceManager.Instance.CreatePrefabInstance(functionArea, "GameScene/Card/ContactEventList");
					break;
				}
			}
			
			state = value;
			
		}
	}
	
	
	protected override void Initialize ()
	{
		fx.Preload("LoveFX");
		fx.Preload("SoulFX");
		
		tweener = chara.GetComponent<UITweener>();
		
		card = Context.Instance.Card;
		originKizuna = card.Origin.Kizuna;
		
		for(int i = 0; i < 3; ++i) originPlus[i] = card.Origin.GetPlus(i);
		
		total.text = string.Format("/{0}", ConstData.Tables.GetVariable(VariableID.COMMU_LIMIT));
		
		chara.ApplyChange();
		
		Message.AddListener(MessageID.CharaTalk, OnCharaTalk);
		Message.AddListener(MessageID.CharaGift, OnCharaGift);
		Message.AddListener(MessageID.CharaEvent, OnCharaEvent);
		Message.AddListener(MessageID.SelectItem, OnSelectItem);
		
		GameSystem.Service.OnCardTalkBegin += OnCardTalkBegin;
		GameSystem.Service.OnCardTalkEnd += OnCardTalkEnd;
		GameSystem.Service.OnCardGift += OnCardGift;
		GameSystem.Service.OnCardEvent += OnCardEvent;
		
		RefreshKizuna();
		if(card.Origin.ReservedInteractiveID > 0)
		{
			OnCardTalkBegin(card.Origin.Serial, card.Origin.ReservedInteractiveID, 0);
			Coroutine.DelayInvoke(InitializeComplete, 0.5f);
		}
		else
			InitializeComplete();
		/*
		if(card.Origin.ReservedInteractiveID > 0)
			OnCardTalkBegin(card.Origin.Serial, card.Origin.ReservedInteractiveID, 0);
		else if(Context.Instance.RestoreQuestEntrance) 
		{
			Context.Instance.RestoreQuestEntrance = false;
			OnCharaEvent();
		}
		*/
	}
	
	void OnCardEvent (Mikan.Serial serial, int interactiveId, int dropId)
	{
		RefreshKizuna();
		
		ShowDrop(dropId);
	}
	
	void OnCardGift ()
	{
		RefreshKizuna();
		
		var hasPlus = false;
		for(int i = 0; i < 3; ++i)
		{
			var plus = card.Origin.GetPlus(i);
			if(originPlus[i] == plus) continue;
			originPlus[i] = plus;
			hasPlus = true;
		}
		
		if(hasPlus)
		{
			fx.PlayFX("SoulFX", Vector3.zero, fx.gameObject);
			Message.Send(MessageID.PlaySound, SoundID.SOUL);
		}
		
		State = STATE.Default;
	}
	
	void OnCardTalkEnd (Mikan.Serial serial, int eventId, int dropId)
	{
		tweener.enabled = true;
		tweener.ResetToBeginning();
		tweener.PlayForward();
		
		RefreshKizuna();
		UpdateChara(0, eventId);
		State = STATE.Default;
		
		ShowDrop(dropId);
	}
	
	void OnCardTalkBegin (Mikan.Serial serial, int interactiveId, int ignore)
	{
		tweener.enabled = true;
		tweener.ResetToBeginning();
		tweener.PlayForward();
		
		RefreshKizuna();
		
		Context.Instance.InteractiveID = interactiveId;
		
		UpdateChara(interactiveId, 0);
		
		if(interactiveId == 0) return;
		
		var ev = EventUtils.CallEvent(interactiveId);
		if(ev.n_CHOICE[0] != 0)
			State = STATE.ChoiceList;
	}
	
	private void ShowDrop(int dropId)
	{
		if(dropId > 0) QuestDrop.Show(dropId);
	}
	
	protected override void OnDestroy ()
	{
		if(GameSystem.IsAvailable) 
		{
			GameSystem.Service.OnCardTalkBegin -= OnCardTalkBegin;
			GameSystem.Service.OnCardTalkEnd -= OnCardTalkEnd;
			GameSystem.Service.OnCardGift -= OnCardGift;
			GameSystem.Service.OnCardEvent -= OnCardEvent;
		}
		
		Message.RemoveListener(MessageID.CharaTalk, OnCharaTalk);
		Message.RemoveListener(MessageID.CharaGift, OnCharaGift);
		Message.RemoveListener(MessageID.CharaEvent, OnCharaEvent);
		Message.RemoveListener(MessageID.SelectItem, OnSelectItem);
		Message.RemoveListener(MessageID.EndEvent, OnEndEvent);
		base.OnDestroy ();
	}
	
	public bool OnBack ()
	{
		switch(State)
		{
		case STATE.Default:
			return false;
		case STATE.ChoiceList:
			return true;
		default:
			State = STATE.Default;
			return true;
		}
	}
	
	private void OnCharaTalk()
	{
		if(GameSystem.Service.PlayerInfo.TalkPoint > 0)	
			GameSystem.Service.CardBeginTalk(card.Origin.Serial);
		else
			Message.Send(MessageID.ChargeTalkPoint);
	}
	
	private void OnCharaGift()
	{
		State = STATE.GiftList;
	}
	
	private void OnCharaEvent()
	{
		State = STATE.EventList;
	}
	
	private void OnEndEvent(IMessage msg)
	{		
		Message.RemoveListener(MessageID.EndEvent, OnEndEvent);
		
		var interactiveId = (int)msg.Data;
		
		var index = Mikan.CSAGA.Calc.CardEvent.GetIndex(card.CardID, interactiveId);
		
		if(index > -1 && !card.Origin.IsEventFinished(index))
		{
			GameSystem.Service.CardEvent(card.Origin.Serial, interactiveId);
		}
	}
	
	private void OnSelectItem(IMessage msg)
	{
		switch(State)
		{
		case STATE.ChoiceList:
		{
			var choice = (int)msg.Data;
			GameSystem.Service.CardEndTalk(card.Origin.Serial, choice);
			State = STATE.EndTalk;
			break;
		}
		case STATE.EventList:
		{
			var interactiveId = (int)msg.Data;
			
			if(CheckEvent(interactiveId))
			{
				var data = Mikan.CSAGA.EventUtils.CallEvent(interactiveId);
				if(data.n_RESULT == EventResult.Quest)
				{
					QuestManager.Instance.PrepareQuest(card.Origin.Serial, interactiveId);
					Message.Send(MessageID.ShowQuestView);
				}
				else if(CheckEvent(interactiveId))
				{
					Message.AddListener(MessageID.EndEvent, OnEndEvent);
					Message.Send(MessageID.PlayEvent, interactiveId);
				}
			}
			break;
		}
		case STATE.GiftList:
		{
			var item = msg.Data as ItemData;
			if(GameSystem.Service.ItemInfo.Count(item.ItemID) > 0)
			{
				var itemData = ConstData.Tables.Item[item.ItemID];
				
				var plusIndex = -1;
				if(itemData.n_HP_MAX > 0)
					plusIndex  = 0;
				else if(itemData.n_ATK_MAX > 0)
					plusIndex = 1;
				else if(itemData.n_REV_MAX > 0)
					plusIndex = 2;
				
				if(plusIndex >= 0)
				{
					var plus = card.Origin.GetPlus(plusIndex);
					if(plus >= ConstData.Tables.CardBalance[card.Origin.Rare].n_PLUS)
					{
						MessageBox.Show(ConstData.GetSystemText(SystemTextID._198_TIP_PLUS_OVERFLOW_V1, ConstData.GetItemName(item.ItemID)));
						break;
					}
				}
				
				MessageBox.ShowConfirmWindow("GameScene/Card/CardGiftConfirmWindow", "", null).title = ConstData.GetSystemText(SystemTextID._195_DEAL_GIFT_COUNT);
			}
			
			break;
		}
		}
		
	}
	
	private bool CheckEvent(int interactiveId)
	{
		Context.Instance.InteractiveID = interactiveId;
		
		var data = ConstData.Tables.Interactive[interactiveId];
		
		var errCode = Mikan.CSAGA.Calc.CardEvent.Check<Mikan.CSAGA.Data.QuestState>(data, card.Origin.Kizuna, GameSystem.Service.CardInfo.IDs, GameSystem.Service.QuestInfo.Get);

		switch(errCode)
		{
		case ErrorCode.KizunaNotEnough:
			MessageBox.Show(SystemTextID.TIP_EVENT_KIZUNA_NOT_ENOUGH);
			return false;
		case ErrorCode.CardNotExists:
			MessageBox.Show("GameScene/Card/CardNotExistsMessageBox", "");
			return false;
		case ErrorCode.QuestIllegal:
			MessageBox.Show(ConstData.GetSystemText(SystemTextID.TIP_EVENT_REQUIRE_QUEST, ConstData.Tables.QuestName[data.n_PRE_QUEST].s_SUB_NAME));
			return false;
		}
		return true;
	}
	
	private void UpdateChara(int interactiveId, int eventId)
	{
		chara.InteractiveID = interactiveId;
		chara.EventID = eventId;
		chara.ApplyChange();
	}
	
	private void RefreshKizuna()
	{
		kizuna.text = ConstData.GetSystemText(SystemTextID._173_KLV_V1, card.Origin.Kizuna / 100);
		kizunaProgress.value = card.Origin.Kizuna%100 / 100f;
		
		if(originKizuna < card.Origin.Kizuna) 
		{
			fx.PlayFX("LoveFX", Vector3.zero, fx.gameObject);
			Message.Send(MessageID.PlaySound, SoundID.LOVE);
		}
		
		originKizuna = card.Origin.Kizuna;
	}
}
