﻿using UnityEngine;
using System.Collections;

public class CardContactChoiceItemRenderer : ItemRenderer<ChoiceData>
{
	public UILabel title;
	public UILabel description;

	protected override void ApplyChange (ChoiceData data)
	{
		title.text = ConstData.Tables.GetSystemText((int)SystemTextID.CHOICE_1 + Index);
		var ev = Mikan.CSAGA.EventUtils.CallEvent(Context.Instance.InteractiveID);
		description.text = ConstData.GetEventChoicText(ev.n_ID, data.Choice);
	}

	void OnClick()
	{
		Message.Send(MessageID.SelectItem, data.Choice);
	}
}

