﻿using UnityEngine;
using System.Collections;

public class CardContactPage : Stage {

	public const int DEFAULT = 0;
	public const int CONTACT_VIEW = 1;
	public const int QUEST_PREPARE = 2;
	public const int SELECT_ONE = 3;
	public const int SELECT_SUPPORT = 4;
	public const int RANDOM_SUPPORT = 5;

	public PresetNavigator navigator;
	public VirtualCardTeamView teamView;

	protected override void Initialize ()
	{
		Message.AddListener(MessageID.SelectItem, OnSelectItem);
		Message.AddListener(MessageID.ShowQuestView, OnShowQuestView);
		base.Initialize ();
	}

	protected override void OnDestroy ()
	{
		Message.RemoveListener(MessageID.SelectItem, OnSelectItem);
		Message.RemoveListener(MessageID.ShowQuestView, OnShowQuestView);
		base.OnDestroy ();
	}

	protected override void OnBack ()
	{
		if(IsDestroyed) return;

		if(teamView.Back()) return;


		if(navigator.currentObj != null)
		{
		switch(navigator.current)
		{
			case CONTACT_VIEW:
			{
				var view = navigator.currentObj.GetComponent<CardContactView>();
				if(view != null)
				{
					if(!view.OnBack()) navigator.Next(DEFAULT);
					return;
				}
				break;
			}
			default:

				break;
			}
		}
		base.OnBack();
	}
	protected override void OnNext ()
	{
		if(teamView.Next()) return;

	}

	protected override void OnInitializeComplete ()
	{
		if(Context.Instance.RestoreQuestEntrance) 
			navigator.Next(CONTACT_VIEW);
		else
			navigator.Next(DEFAULT);
	}

	private void OnSelectItem(IMessage msg)
	{
		if(msg.Data is CardData) 
		{
			if(navigator.current == DEFAULT)
				navigator.Next(CONTACT_VIEW);
		}
	}

	private void OnShowQuestView()
	{
		navigator.Next(QUEST_PREPARE);
	}
}
