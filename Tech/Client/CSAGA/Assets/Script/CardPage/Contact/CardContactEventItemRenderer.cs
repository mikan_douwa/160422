﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CardContactEventItemRenderer : ItemRenderer<CardEventData> {

	public UILabel title;
	public UILabel kizuna;
	public GameObject clear;
	public ItemList require;
	private int interactiveId;
	
	protected override void ApplyChange (CardEventData data)
	{
		interactiveId = data.InteractiveID;

		var interactive = ConstData.Tables.Interactive[interactiveId];

		title.text = data.IsAvailable ? ConstData.GetEventName(interactive.n_CALL_EVENT) : "? ? ?";

		kizuna.text = ConstData.GetSystemText(SystemTextID._173_KLV_V1, interactive.n_FAVOR_MIN / 100);

		RefreshState();

		var list = new List<RequireData>();

		var ev = Mikan.CSAGA.EventUtils.CallEvent(interactive);

		if(ev.n_RESULT == Mikan.CSAGA.EventResult.Quest)
			list.Add(new RequireData{ ManualIconSprite = "symbol_battle" });

		if(interactive.n_PRE_QUEST > 0)
			list.Add(new RequireData{ ManualIconSprite = "symbol_quest" });

		for(int i = 1; i < interactive.n_CARDID.Length; ++i)
		{
			var item = interactive.n_CARDID[i];
			if(item > 0) list.Add(new RequireData{ IconType = Mikan.CSAGA.DropType.Card, IconID = item });
		}

		require.Setup(list);
	}

	public void RefreshState()
	{
		if(data != null)
			clear.SetActive(Context.Instance.Card.Origin.IsEventFinished(data.Index));
	}

	void OnClick()
	{
		if(data.IsAvailable)
			Message.Send(MessageID.SelectItem, interactiveId);
		else
			Message.Send(MessageID.ShowMessageBox, ConstData.GetSystemText(SystemTextID.TIP_EVENT_KIZUNA_NOT_ENOUGH));
	}
}

public class CardEventData
{
	public int InteractiveID;
	public int Index;
	public bool IsAvailable;
}



