﻿using UnityEngine;
using System.Collections;

public class TalkPointTip : MonoBehaviour {

	public UILabel remain;
	public UILabel limit;
	public UILabel countdown;

	private int _remain = -1;

	// Use this for initialization
	void Start () {
		limit.text = string.Format("/{0}", ConstData.Tables.GetVariable(Mikan.CSAGA.VariableID.COMMU_LIMIT));
	}

	void OnEnable()
	{
		StartCoroutine(RefreshTalkPoint());
	}

	IEnumerator RefreshTalkPoint()
	{
		while(true)
		{
			if(countdown != null)
			{
				var cd = GameSystem.Service.PlayerInfo.TalkPointCountdown;
				
				if(cd.Hours == 0)
					countdown.text = string.Format("{0:00}:{1:00}", (int)cd.Minutes, (int)cd.Seconds);
				else
					countdown.text = string.Format("{0:00}:{1:00}:{2:00}", (int)cd.Hours, (int)cd.Minutes, (int)cd.Seconds);
			}

			var pt = GameSystem.Service.PlayerInfo.TalkPoint;
			if(pt != _remain)
			{
				_remain = pt;
				remain.text = _remain.ToString();
			}
			
			yield return new WaitForSeconds(0.5f);
		}
	}
}
