﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CardContactChoiceList : UIBase {

	public ItemList itemList;

	protected override void OnStart ()
	{
		Message.AddListener(MessageID.SelectItem, OnSelectItem);

		var seed = new List<int>(3);
		for(int i = 0; i < 3; ++i) seed.Add(i);
		Mikan.MathUtils.Shuffle(seed);
		var list = new List<ChoiceData>(3);

		foreach(var item in seed) list.Add(new ChoiceData{ Choice = item });
	
		itemList.Setup(list);
	}

	protected override void OnDestroy ()
	{
		Message.RemoveListener(MessageID.SelectItem, OnSelectItem);
		base.OnDestroy ();
	}

	private void OnSelectItem()
	{
		Destroy(gameObject);
	}
}

public class ChoiceData
{
	public int Choice;
}