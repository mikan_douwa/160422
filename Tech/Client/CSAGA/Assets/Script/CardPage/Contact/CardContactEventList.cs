﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CardContactEventList : UIBase {

	public ItemList itemList;

	protected override void OnStart ()
	{
		GameSystem.Service.OnCardEvent += OnCardEvent;

		var list = new List<CardEventData>();

		var kizuna = Context.Instance.Card.Origin.Kizuna;

		foreach(var item in Mikan.CSAGA.Calc.CardEvent.Select(Context.Instance.Card.CardID))
		{
			var data = new CardEventData{ InteractiveID = item.n_ID, Index = list.Count };
			data.IsAvailable = kizuna >= item.n_FAVOR_MIN;

			list.Add(data);
		}

		itemList.Setup(list);
	}

	protected override void OnDestroy ()
	{
		if(GameSystem.IsAvailable) GameSystem.Service.OnCardEvent -= OnCardEvent;
		base.OnDestroy ();
	}

	void OnCardEvent (Mikan.Serial serial, int interactiveId, int dropId)
	{
		for(int i = 0; i < itemList.RendererCount; ++i)
		{
			var renderer = itemList[i] as CardContactEventItemRenderer;
			renderer.RefreshState();
		}
	}
}
