﻿using UnityEngine;
using System.Collections;

public class CardAwakenItemRenderer : ItemRenderer<CardAwakenData> {

	public TextureLoader icon;

	public UILabel lv;

	#region implemented abstract members of ItemRenderer

	protected override void ApplyChange (CardAwakenData data)
	{
		icon.Load("ICON/" + data.Origin.s_ICON);
		lv.text = ConstData.GetSystemText(SystemTextID.LV_V1, data.LV);

		if(data.LV > 0) 
			icon.texture.color = Color.white;
		else
			icon.texture.color = Color.gray;
	}

	#endregion

	void OnClick()
	{
		if(data == null) return;
		Context.Instance.CardAwaken = data;
		MessageBox.Show("Common/AwakenInfo", "");
	}

	void OnLongPress()
	{
		OnClick();
	}

}

public class CardAwakenData
{
	public Mikan.CSAGA.ConstData.Awaken Origin;
	public int LV;
}
