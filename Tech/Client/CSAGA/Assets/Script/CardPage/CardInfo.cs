﻿using UnityEngine;
using System.Collections;

public class CardInfo : Stage {

	public CardSkill skill;

	public GameObject awaken;

	private CardData card;

	private Mikan.CSAGA.ConstData.CardData data;

	public static void Show(CardData card)
	{
		Context.Instance.Card = card;

		Message.Send(MessageID.SwitchStage, StageID.CardInfo);
	}

	protected override void Initialize ()
	{
		Menu.Hide();
		RankingInfo.Hide();

		Message.AddListener(MessageID.SelectItem, OnSelectItem);

		card = Context.Instance.Card;

		data = ConstData.Tables.Card[card.CardID];

		ShowLeaderSkill();

		base.Initialize ();
	}

	protected override void OnDestroy ()
	{
		base.OnDestroy ();

		Message.RemoveListener(MessageID.SelectItem, OnSelectItem);

		Menu.Show();
		RankingInfo.Show();
	}

	protected override void OnBack ()
	{
		if(Context.Instance.IsTutorialComplete)
		{
			if(GachaDrawShowCard.Current != null && GachaDrawShowCard.Current.IsSingle)
				StageManager.Instance.RemoveAllAppend();
			else
				StageManager.Instance.RemoveAppend();
		}
		else
		{
			Context.Instance.InteractiveID = ConstData.Tables.TutorialEventID2;
			StageManager.Instance.ReplaceAppend(StageID.EventPlayer);
		}
	}

	public void ShowLeaderSkill()
	{
		ShowSkill(card.Origin != null ? card.Origin.LeaderSkill : data.n_LEADER_SKILL);
	}

	public void ShowSkill1()
	{
		ShowSkill(card.Origin != null ? card.Origin.Skill1 : data.n_SKILL[0]);
	}

	public void ShowSkill2()
	{
		ShowSkill(card.Origin != null ? card.Origin.Skill2 : data.n_SKILL[1]);
	}

	public void ShowAwaken()
	{
		awaken.SetActive(true);
		skill.gameObject.SetActive(false);
	}
		
	public void ShowLuck()
	{
		if(card.Luck == 0)
			MessageBox.Show("GameScene/Card/LuckInfo", "").title = ConstData.GetSystemText(SystemTextID._199_LUCKY_EFFECT);
	}

	private void ShowSkill(int id)
	{
		awaken.SetActive(false);
		skill.gameObject.SetActive(true);

		skill.SkillID = id;
		skill.ApplyChange(true);
	}

	private void OnSelectItem(IMessage msg)
	{
		if(card.Origin != null && !card.Origin.Serial.IsExternal && FindObjectOfType<CardEquipment>() == null)
			Message.Send(MessageID.SwitchStage, StageID.CardEquipment);
	}

	void OnClick()
	{
		Message.Send(MessageID.PlaySound, SoundID.CANCEL);
		OnBack();
	}
}
