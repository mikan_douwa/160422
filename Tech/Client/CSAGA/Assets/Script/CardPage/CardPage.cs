﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CardPage : Stage {

	public TextureLoader pic;
	public UILabel cardName;
	public Stars stars;
	public ItemList itemList;
	public GameObject tacticsObj;
	protected override void Initialize ()
	{
		Menu.Show();
#if !DEBUG
		if(ConstData.Tables.GetVariable(Mikan.CSAGA.VariableID.TACTICS_ENABLE) != 0)
#endif
		{
			tacticsObj.SetActive(true);
		}

		var teamLeader = GameSystem.Service.TeamInfo[Context.Instance.TeamIndex].Members[0];

		var card = GameSystem.Service.CardInfo[teamLeader];

		stars.viewCount = card.Rare;
		stars.ApplyChange();
		cardName.text = ConstData.GetCardName(card.ID);
		pic.Load(ConstData.GetCardPicPath(card.ID));

		var list = new List<CardPageData>();
		list.Add(new CardPageData{ StageID = StageID.CardTeam, TextID = SystemTextID.BTN_CARD_TEAM });
		list.Add(new CardPageData{ StageID = StageID.CardContact, TextID = SystemTextID.BTN_CARD_CONTACT });
		list.Add(new CardPageData{ StageID = StageID.CardEnhance, TextID = SystemTextID.BTN_CARD_ENHANCE });
		list.Add(new CardPageData{ StageID = StageID.CardAwaken, TextID = SystemTextID.BTN_CARD_AWAKEN });
		list.Add(new CardPageData{ StageID = StageID.CardSell, TextID = SystemTextID.BTN_CARD_SELL });
		list.Add(new CardPageData{ StageID = StageID.CardList, TextID = SystemTextID.BTN_CARD_LIST });

		itemList.Setup(ListItemDataProvider<CardPageData>.Create(list));

		InitializeComplete();

		if(Context.Instance.RestoreQuestEntrance) Message.Send(MessageID.SwitchStage, StageID.CardContact);
	}

	public void OpenTactic()
	{
		Message.Send(MessageID.SwitchStage, StageID.Tactics);
	}
}
