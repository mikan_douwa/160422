﻿using UnityEngine;
using System.Collections;

public class CardAwakenInfo : MessageBox {

	public TextureLoader icon;

	protected override void OnReady ()
	{
		var data = Context.Instance.CardAwaken;
		var text = ConstData.Tables.AwakenText[data.Origin.n_ID];

		if(data.LV == 0)
			titleLabel.text = text.s_NAME + ConstData.GetSystemText(SystemTextID.LV_V1, data.Origin.n_LV) + ConstData.GetSystemText(SystemTextID._242_BTN_AWAKEN_NOT_AVAILABLE);
		else
			titleLabel.text = text.s_NAME + ConstData.GetSystemText(SystemTextID.LV_V1, data.LV);

		contentLabel.text = text.s_TEXT;
		icon.Load("ICON/" + data.Origin.s_ICON);
	}
}
