﻿using UnityEngine;
using System.Collections;

public class CardPageItemRenderer : ItemRenderer<CardPageData> {

	public SystemTextLabel label;
	public SwitchStage switchStage;

	#region implemented abstract members of ItemRenderer

	protected override void ApplyChange (CardPageData data)
	{
		label.textId = data.TextID;
		label.ApplyChange();
		switchStage.stageId = data.StageID;
	}

	#endregion

}

public class CardPageData
{
	public SystemTextID TextID;
	public StageID StageID;
}
