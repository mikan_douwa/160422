﻿using UnityEngine;
using System;
using System.Collections;
using Mikan.CSAGA.Data;

public class ActiveTargetItem : MonoBehaviour {

	public UITexture icon;
	public UILabel count;
	public UILabel name;
	public UILabel point;
	public GameObject check;

	Action initCallback = null;

	public void Init(Mikan.CSAGA.ConstData.Trophy data, Trophy trophy, Action callback)
	{
		initCallback = callback;
		//icon
		ResourceManager.Instance.LoadTexture("UI/ICON_" + data.s_CONDITION_TYPE, InitHandler);
		//progress
		int progress = trophy.Progress;
		if(trophy.UpdateTime.Date != GameSystem.Service.CurrentTime.Date)
			progress = 0;
		count.text = progress.ToString() + "/" + data.n_EFFECT[0].ToString();
		NGUITools.SetActive(check, (progress == data.n_EFFECT[0]));
		//name
		Mikan.CSAGA.ConstData.TrophyText text = ConstData.Tables.TrophyText[data.n_ID];
		if(text != null)
			name.text = text.s_NAME;
		point.text = "(+" + data.n_BONUS_LINK.ToString() + ")";
	}
		
	public void InitHandler(IAsset<Texture2D> asset)
	{
		icon.mainTexture = asset.Content;
		if(initCallback != null)
			initCallback();
	}
}
