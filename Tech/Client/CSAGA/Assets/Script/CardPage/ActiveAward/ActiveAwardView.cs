﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class ActiveAwardView : Stage {

	public const int CLIP_COUNT = 16;

	public GameObject clipPrefab;
	public UIGrid grid;
	public UILabel point;

	bool inited = false;
	List<BrainClip> clipList = new List<BrainClip>();
	int openId = -1;
	int getItemId = 0;
	bool blockTouch = false;

	protected override void Initialize ()
	{
		GameSystem.Service.OnDailyInfo += HandleOnDailyInfo;

		nextButtonText = ConstData.Tables.GetSystemText(147);

		GameSystem.Service.TrophyList();
		GameSystem.Service.GetDailyInfo();

	}

	protected override void OnNext ()
	{
		Message.Send(MessageID.SwitchStage, StageID.ActiveTarget);
	}

	protected override void OnDestroy ()
	{
		if(GameSystem.IsAvailable)
			GameSystem.Service.OnDailyInfo -= HandleOnDailyInfo;

		base.OnDestroy();
	}

	void HandleOnDailyInfo (int getId)
	{
		getItemId = getId;
		//Refresh point
		point.text = GameSystem.Service.DailyInfo.Remains.ToString() + "/" + (ConstData.Tables.DailyActiveCount + GameSystem.Service.DailyInfo.Add).ToString();

		List<int> lst = GameSystem.Service.DailyInfo.List;

		if(!inited)
		{
			//Create.
			inited = true;

			for(int i = 0; i < lst.Count; i++)
			{
				BrainClip clip = NGUITools.AddChild(grid.gameObject, clipPrefab).GetComponent<BrainClip>();
				clip.Init(i, lst[i], GameSystem.Service.DailyInfo.GetSlotState(i));
				clipList.Add(clip);

				UIEventListener.Get(clip.gameObject).onClick = OnOpenClip;
			}
			grid.Reposition();

			base.Initialize ();
		}
		else
		{
			//open
			if(openId >= 0)
			{
				BrainClip clip = clipList[openId];
				int dropId = lst[openId];
				blockTouch = true;
				if(dropId > 0)
				{
					//Play Open
					clip.Open(dropId, OpenFinish);
				}
				else
				{
					RefreshAllClips();
				}
				openId = -1;
			}
		}

	}

	void OnOpenClip(GameObject btn)
	{
		if(blockTouch) return;

#if !DEBUG
		if(GameSystem.Service.DailyInfo.Remains <= 0 && GameSystem.Service.DailyInfo.Cached < 0)
			MessageBox.Show(ConstData.Tables.GetSystemText(163));
		else
#endif
		{

			BrainClip clip = btn.GetComponent<BrainClip>();
			if(clip != null && !clip.IsOpen)
			{
				openId = clip.Index;
				GameSystem.Service.DailyActivePick(clip.Index);
			}
		}
	}


	public void OpenFinish()
	{
		float waiting = 0.1f;
		//Get item
		if(getItemId > 0)
		{

			QuestDrop.Show(getItemId);

			getItemId = 0;
			//Check clips.
			for(int i = 0; i < clipList.Count; i++)
			{
				BrainClip clip = clipList[i];
				if(!clip.Checked && GameSystem.Service.DailyInfo.GetSlotState(i))
				{
					clip.Check();
				}
			}
			//Sound
			Message.Send(MessageID.PlaySound, SoundID.CARD_RIGHT);
		}
		else
		{
			bool isClose = RefreshAllClips();
			if(isClose)
			{
				//Sound
				Message.Send(MessageID.PlaySound, SoundID.CARD_WRONG);
				waiting = 1f;
			}
		}

		StartCoroutine(EnableTouchAfterSec(waiting));
	}

	bool RefreshAllClips()
	{
		//close back
		bool isClose = false;
		for(int i = 0; i < clipList.Count; i++)
		{
			BrainClip clip = clipList[i];
			if(clip.IsOpen && !GameSystem.Service.DailyInfo.GetSlotState(i))
			{
				clip.Close();
				isClose = true;
			}
		}

		return isClose;
	}

	IEnumerator EnableTouchAfterSec(float sec)
	{
		yield return new WaitForSeconds(sec);

		blockTouch = false;
	}
}
