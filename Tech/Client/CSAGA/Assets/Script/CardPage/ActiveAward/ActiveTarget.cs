﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using Mikan.CSAGA.Data;

public class ActiveTarget : Stage {

	public GameObject itemPrefab;
	public UIGrid grid;
	public GameObject closeButton;

	int totalCount;
	int finishCount;

	protected override void Initialize ()
	{
		List<Trophy> lst = GameSystem.Service.TrophyInfo.Daily;
		totalCount = lst.Count;
		finishCount = 0;

		for(int i = 0; i < lst.Count; i++)
		{
			Trophy t = lst[i];
			Mikan.CSAGA.ConstData.Trophy data = ConstData.Tables.Trophy[t.Group];
			ActiveTargetItem item = NGUITools.AddChild(grid.gameObject, itemPrefab).GetComponent<ActiveTargetItem>();
			if(data != null)
				item.Init(data, t, ItemInitHandler);
		}
		grid.Reposition();

		UIEventListener.Get(closeButton).onClick = Close;
	}

	public void ItemInitHandler()
	{
		finishCount++;
		if(finishCount == totalCount)
		{
			base.Initialize();
		}
	}

	public void Close(GameObject btn)
	{
		Destroy(this);
	}
}
