﻿using UnityEngine;
using System.Collections;
using Mikan.CSAGA.ConstData;

public class BrainClip : MonoBehaviour {
	
	public UISprite bg;
	public Icon icon;
	public UILabel amount;
	public GameObject star;
	public GameObject check;
	public GameObject contentObj;
	public AnimationCurve checkCurve;

	public int Index;
	public bool IsOpen;
	public bool Checked;
	int DropID;
	bool Inited = false;
	EventDelegate.Callback AnimationFinished = null;

	public void Init(int index, int dropId, bool isOpen)
	{
		Index = index;
		IsOpen = isOpen;
		DropID = dropId;

		SetIcon(dropId);
		NGUITools.SetActive(contentObj, (isOpen && dropId > 0));

		Checked = (isOpen && GameSystem.Service.DailyInfo.Cached != Index);
		NGUITools.SetActive(check, Checked);

		//bg
		if(isOpen)
		{
			bg.spriteName = "dialog_event01";
			bg.type = UIBasicSprite.Type.Sliced;
		}
		else
		{
			bg.spriteName = "active_card";
			bg.type = UIBasicSprite.Type.Simple;
		}

	}

	void SetIcon(int dropId)
	{
		if(dropId > 0)
		{
			Inited = true;
			//icon
			Drop data = ConstData.Tables.Drop[dropId];
			if(data != null)
			{
				icon.IconType = data.n_DROP_TYPE;
				icon.IconID = data.n_DROP_ID;
				icon.Count = data.n_COUNT;
				icon.ApplyChange();
				//Label
				switch(data.n_DROP_TYPE)
				{
					case Mikan.CSAGA.DropType.Card:
						Mikan.CSAGA.ConstData.CardData card = ConstData.Tables.Card[data.n_DROP_ID];
						if(card != null)
						{
							amount.text = "   " + card.n_RARE.ToString();
						}
						NGUITools.SetActive(star, true);
						break;

					case Mikan.CSAGA.DropType.Coin:
					case Mikan.CSAGA.DropType.Crystal:
					case Mikan.CSAGA.DropType.Gem:
						amount.text = data.n_COUNT.ToString();
						NGUITools.SetActive(star, false);
						break;

					case Mikan.CSAGA.DropType.Item:
						Mikan.CSAGA.ConstData.Item itemData = ConstData.Tables.Item[data.n_DROP_ID];
						if(itemData != null && itemData.n_TYPE == Mikan.CSAGA.ItemType.Equipment)
						{
							//Equipment
							amount.text = "   " + itemData.n_RARE.ToString();
							NGUITools.SetActive(star, true);
						}
						else
						{
							amount.text = "x" + data.n_COUNT.ToString();
							NGUITools.SetActive(star, false);
						}
						break;

					default:
						amount.text = "";
						NGUITools.SetActive(star, false);
						break;
				}
			}
		}

	}

	public void Open(int dropId, EventDelegate.Callback callback)
	{
		IsOpen = true;
		DropID = dropId;
		AnimationFinished = callback;

		//Play
		TweenClockY tc = TweenClockY.Begin(gameObject, 0.15f, 90);
		tc.ignoreTimeScale = false;
		tc.delay = 0f;
		EventDelegate.Add(tc.onFinished, OpenAnimation, true);
		//Sound
		Message.Send(MessageID.PlaySound, SoundID.CARD_OPEN);
	}

	void OpenAnimation()
	{
		if(!Inited)
		{
			Inited = true;
			
			SetIcon(DropID);
		}

		bg.spriteName = "dialog_event01";
		bg.type = UIBasicSprite.Type.Sliced;
		NGUITools.SetActive(contentObj, true);

		transform.localRotation = Quaternion.Euler(0, 90, 0);
		TweenClockY tc = TweenClockY.Begin(gameObject, 0.15f, 0);
		tc.ignoreTimeScale = false;
		tc.delay = 0f;
		if(AnimationFinished != null)
			EventDelegate.Add(tc.onFinished, AnimationFinished, true);
	}

	public void Close()
	{
		IsOpen = false;
		//Play
		TweenClockY tc = TweenClockY.Begin(gameObject, 0.15f, 90);
		tc.ignoreTimeScale = false;
		tc.delay = 1f;
		EventDelegate.Add(tc.onFinished, CloseAnimation, true);
	}

	void CloseAnimation()
	{
		bg.spriteName = "active_card";
		bg.type = UIBasicSprite.Type.Simple;
		NGUITools.SetActive(contentObj, false);

		transform.localRotation = Quaternion.Euler(0, 90, 0);
		TweenClockY tc = TweenClockY.Begin(gameObject, 0.15f, 0);
		tc.ignoreTimeScale = false;
		tc.delay = 0f;
	}

	public void Check()
	{
		Checked = true;

		NGUITools.SetActive(check, true);
		check.transform.localScale = Vector3.zero;

		TweenScale ts = TweenScale.Begin(check, 0.5f, Vector3.one);
		ts.ignoreTimeScale = false;
		ts.animationCurve = checkCurve;
	}
}
