﻿using UnityEngine;
using System.Collections;

public class LuckInfo : MonoBehaviour {

	public UILabel luck;

	public UILabel hp;
	public UILabel atk;
	public UILabel rev;
	public UILabel chg;
	public UILabel drop;

	// Use this for initialization
	void Start () {
		var card = Context.Instance.Card;

		var _luck = card.Luck;

		if(_luck == 0)
		{
			if(card.Origin != null)
				_luck = card.Origin.Luck;
			else
				_luck = GameSystem.Service.CardInfo.GetLuck(card.CardID);
		}

		luck.text = string.Format("{0}/{1}", _luck, card.Origin.Data.n_LUCK_MAX);

		var data = ConstData.Tables.Player[_luck];

		var property = string.Format("+{0}%", data.n_LUCK_ABILITY);
		hp.text = atk.text = rev.text = chg.text = property;

		drop.text = string.Format("+{0}%", data.n_LUCK_DROP);
	}

}
