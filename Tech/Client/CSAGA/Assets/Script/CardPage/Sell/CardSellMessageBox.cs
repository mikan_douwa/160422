﻿using UnityEngine;
using System.Collections;
using Mikan.CSAGA;
using System.Collections.Generic;

public class CardSellMessageBox : SubmitWindow {

	public ItemList itemList;

	public UILabel crystal;

	public UILabel coin;

	protected override void OnReady ()
	{
		var _crystal = 0;
		var _coin = 0;
		
		foreach(var serial in Context.Instance.Cards)
		{
			var card = GameSystem.Service.CardInfo[serial];
			var data = ConstData.Tables.Card[card.ID];
			
			_coin += data.n_TRADE_COIN;
			_crystal += CardUtils.CalcCrystal(card.Exp);
		}
		
		crystal.text = string.Format("{0}+{1}", ConstData.GetSystemText(SystemTextID.CRYSTAL), _crystal);
		coin.text = string.Format("{0}+{1}", ConstData.GetSystemText(SystemTextID.COIN), _coin);

		itemList.GetItemRendererFunc = GetItemRenderer;

		var list = new List<CardData>();
		foreach(var item in Context.Instance.Cards)
		{
			var data = new CardData{ ForceSortID = list.Count };
			data.Origin = GameSystem.Service.CardInfo[item];
			data.CardID = data.Origin.ID;
			data.Property = data.Origin.Calc();
			list.Add(data);
		}

		if(list.Count <= 5)
			itemList.transform.localPosition = new Vector3(itemList.transform.localPosition.x, itemList.transform.localPosition.y - 65);

		itemList.Setup(list);
	}

	private ItemRenderer GetItemRenderer(GameObject item) 
	{
		return item.AddComponent<CardSellConfirmItemRenderer>();
	}

	#region implemented abstract members of SubmitWindow

	protected override bool OnSubmit ()
	{
		GameSystem.Service.CardSell(Context.Instance.Cards);
		return true;
	}

	#endregion



}


public class CardSellConfirmItemRenderer : CardItemRenderer
{
	public override bool EnableLongPress { get { return false; } }

	protected override bool ShowMask { get { return false; } }

	protected override bool UseDefaultFormat { get { return true; } }

	protected override void ApplyChange (CardData data)
	{
		base.ApplyChange (data);
		SetActive(false);
	}
	
}