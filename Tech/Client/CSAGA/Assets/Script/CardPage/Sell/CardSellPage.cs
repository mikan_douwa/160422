﻿using UnityEngine;
using System.Collections;
using Mikan.CSAGA;

public class CardSellPage : CardListPage {

	public UILabel selected;

	public UILabel crystal;

	public UILabel coin;


	protected override void OnInitializeComplete ()
	{
		Message.AddListener(MessageID.Confirm, OnConfirm);
		GameSystem.Service.OnCardOperate += OnCardOperate;
		Context.Instance.Cards.Clear();
		RefreshLabels();
	}

	void OnCardOperate (CardOpertaionType type, SyncCard cardDiff, PropertyDiff playerDiff)
	{
		if(type == CardOpertaionType.Sell) 
		{
			Context.Instance.Cards.Clear();
			RefreshLabels();
		}
	}

	protected override void OnDestroy ()
	{
		Message.RemoveListener(MessageID.Confirm, OnConfirm);
		if(GameSystem.IsAvailable) GameSystem.Service.OnCardOperate -= OnCardOperate;
		base.OnDestroy ();
	}

	protected override void OnSelectItem (IMessage msg)
	{
		RefreshLabels();
	}

	private void OnConfirm()
	{
		if(Context.Instance.Cards.Count == 0) return;

		MessageBox.ShowConfirmWindow("GameScene/Card/CardSellMessageBox", "", null);
	}

	private void RefreshLabels()
	{
		selected.text = ConstData.GetSystemText(SystemTextID.SELECTED_V1_TOTAL, Context.Instance.Cards.Count);

		var crystal = 0;
		var coin = 0;

		foreach(var serial in Context.Instance.Cards)
		{
			var card = GameSystem.Service.CardInfo[serial];
			var data = ConstData.Tables.Card[card.ID];

			coin += data.n_TRADE_COIN;
			crystal += CardUtils.CalcCrystal(card.Exp);
		}

		this.crystal.text = ConstData.GetSystemText(SystemTextID.CRYSTAL_V1, crystal);
		this.coin.text = ConstData.GetSystemText(SystemTextID.COIN_V1, coin);
	}
}
