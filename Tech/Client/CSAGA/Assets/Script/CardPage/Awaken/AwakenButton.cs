﻿using UnityEngine;
using System.Collections;

public class AwakenButton : MonoBehaviour {

	public int Group;
	public int Lv;
	public int MaxLv;
	public UILabel label;
	public UITexture texture;
	public GameObject FxPanel;

	Mikan.CSAGA.ConstData.Awaken awakenData;

	public void Init(int group)
	{
		Group = group;

	}

	public void Refresh(int lv, int maxLv, Mikan.CSAGA.ConstData.Awaken data)
	{
		awakenData = data;
		//icon
		ResourceManager.Instance.LoadTexture("ICON/" + awakenData.s_ICON, texture);
		//lv
		Lv = lv;
		MaxLv = maxLv;
		if(lv == 0)
			texture.color = Color.gray;
		else
			texture.color = Color.white;
		
		string lvStr = "Lv" + lv;
		if(lv == maxLv)
			lvStr = "Lv[FF00FF]MAX[-]";
		label.text = lvStr;

	}

	public void OnLongPress()
	{
		if(Lv > 0)
		{
			Context.Instance.CardAwaken = new CardAwakenData(){ Origin = awakenData, LV = Lv };
			MessageBox.Show("Common/AwakenInfo", "");
		}
	}
}
