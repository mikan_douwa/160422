﻿using UnityEngine;
using System;
using System.Collections;
using Mikan.CSAGA.Data;

public class CardAwakenConfirm : Stage {

	public UILabel title;
	public GameObject awaken0Root;
	public GameObject awakenOtherRoot;
	public CardProfile awakenCurrent;
	public CardProfile awakenTarget;
	public UILabel awakenName;
	public UILabel awakenDetail;
	public UILabel reletionship;
	public UILabel itemTitle;
	public Item[] itemArray;
	public GameObject confirmButton;
	public GameObject cancelButton;
	public Stars currentStar;
	public Stars expectStar;
	public GameObject LuckObj;
	public UILabel currentLuck;
	public UILabel expectLuck;

	private CardData card;
	private CardProperty prop;
	private Mikan.CSAGA.ConstData.CardData data;
	private int group;

	private bool isCloseAll = false;
	private bool kizunaEnough = true;
	private bool materialEnough = true;

	protected override void Initialize ()
	{
		card = Context.Instance.Card;
		prop = card.Origin.Calc();
		data = ConstData.Tables.Card[card.CardID];

		itemTitle.text = ConstData.Tables.GetSystemText(76);

		UIEventListener.Get(confirmButton).onClick = OnConfirmClick;
		UIEventListener.Get(cancelButton).onClick = OnCancelClick;

		GameSystem.Service.OnCardOperate += OnOperateComplete;

		Refresh();
		base.Initialize ();
	}

	public void Refresh()
	{
		group = Context.Instance.AwakenGroup;

		NGUITools.SetActive(awaken0Root, (group == 0 || group == -1));
		NGUITools.SetActive(awakenOtherRoot, (group != 0 && group != -1));
		NGUITools.SetActive(LuckObj, (group == -1));

		Mikan.CSAGA.ConstData.Awaken aData = null;
		switch(group)
		{
		case 0:
			awakenCurrent.stars = currentStar;
			awakenCurrent.ApplyChange(card);
			awakenTarget.stars = expectStar;

			title.text = ConstData.Tables.GetSystemText(75);
			aData = ConstData.Tables.Awaken.SelectFirst(
				o=>{ return o.n_CARDID == card.CardID && o.n_GROUP == 0 && o.n_RARE == card.Origin.Rare; });
			if(aData != null)
			{
				awakenTarget.UpdateProperty(-card.Origin.Exp, card.Origin.SelectEquipment(), 1);
			}
			break;

		case -1:
			awakenCurrent.stars = null;
			awakenCurrent.ApplyChange(card);
			awakenTarget.stars = null;

			title.text = ConstData.Tables.GetSystemText(460);
			CardData cardAfter = card.Clone();
			cardAfter.Luck = cardAfter.Origin.Luck + 1;
			awakenTarget.ApplyChange(cardAfter);

			currentLuck.text = card.Origin.Luck.ToString();
			expectLuck.text = cardAfter.Luck.ToString();
			break;

		default:
			title.text = ConstData.Tables.GetSystemText(125);
			int lv = card.Origin.GetAwakenLV(group) + 1;
			aData = ConstData.Tables.Awaken.SelectFirst(
				o=>{ return o.n_CARDID == card.CardID && o.n_GROUP == group && o.n_LV == lv; });
			
			string name = "";
			string detail = "";
			int kizuna = 0;
			if(aData != null)
			{
				Mikan.CSAGA.ConstData.AwakenText text = ConstData.Tables.AwakenText[aData.n_ID];
				if(text != null)
				{
					name = text.s_NAME + "Lv." + lv.ToString();
					detail = text.s_TEXT;
				}
				kizuna = aData.n_KIZUNA;
				
				kizunaEnough = (kizuna <= card.Origin.Kizuna);
			}
			awakenName.text = name;
			awakenDetail.text = detail;
			reletionship.text = "KLv." + ((int)((float)kizuna / 100f)).ToString();
			break;
		}

		//Required materials.
		materialEnough = true;
		if(group == -1)
		{
			for(int i = 0; i < 5; i++)
			{
				Item item = itemArray[i];
				if(i == 0)
				{
					item.ItemID = ConstData.Tables.LuckItemID;
					item.Count = -1;
					int count = (int)((float)data.n_LUCK_GIFT / 100f);
					if(item.ItemID > 0)
						item.CountFormat = count.ToString() + "({0})";
					else
						item.CountFormat = "";
					item.ApplyChange();
					//Check
					if(count > GameSystem.Service.ItemInfo.Count(item.ItemID))
						materialEnough = false;
				}
				else
				{
					item.ItemID = 0;
					item.Count = -1;
					item.CountFormat = "";
					item.ApplyChange();
				}
			}
		}
		else if(aData != null)
		{
			for(int i = 0; i < aData.n_MATERIAL.Length; i++)
			{
				Item item = itemArray[i];
				item.ItemID = aData.n_MATERIAL[i];
				item.Count = -1;
				int count = aData.n_COUNT[i];
				if(item.ItemID > 0)
					item.CountFormat = count.ToString() + "({0})";
				else
					item.CountFormat = "";
				item.ApplyChange();
				//Check
				if(materialEnough && count > GameSystem.Service.ItemInfo.Count(item.ItemID))
					materialEnough = false;
			}
		}
	}

	void OnConfirmClick(GameObject btn)
	{
		int errorMsg = 0;
		//Check Condition.
		switch(group)
		{
		case 0:
			if(prop.LV < prop.LVMax) 
				errorMsg = 128;
			break;

		default:
			if(!kizunaEnough)
				errorMsg = 126;
			break;
		}

		if(errorMsg == 0 && !materialEnough)
			errorMsg = 127;

		if(errorMsg > 0)
		{
			MessageBox.Show(ConstData.Tables.GetSystemText(errorMsg));
		}
		else
		{
			isCloseAll = true;
			if(group == -1)
				GameSystem.Service.CardLuck(card.Origin.Serial);
			else
				GameSystem.Service.CardAwaken(card.Origin.Serial, group);
			Destroy(gameObject);
		}
	}

	void OnCancelClick(GameObject btn)
	{
		isCloseAll = false;
		Destroy(gameObject);
	}

#region On Material Item Long Press
	public void OnLongPress1()
	{
		Item item = itemArray[0];
		if(item.ItemID > 0)
		{
			ItemInfo.Show(item.ItemID);
		}
	}
	public void OnLongPress2()
	{
		Item item = itemArray[1];
		if(item.ItemID > 0)
		{
			ItemInfo.Show(item.ItemID);
		}
	}
	public void OnLongPress3()
	{
		Item item = itemArray[2];
		if(item.ItemID > 0)
		{
			ItemInfo.Show(item.ItemID);
		}
	}
	public void OnLongPress4()
	{
		Item item = itemArray[3];
		if(item.ItemID > 0)
		{
			ItemInfo.Show(item.ItemID);
		}
	}
	public void OnLongPress5()
	{
		Item item = itemArray[4];
		if(item.ItemID > 0)
		{
			ItemInfo.Show(item.ItemID);
		}
	}
#endregion

	protected override void OnDestroy ()
	{
		if(GameSystem.IsAvailable)
			GameSystem.Service.OnCardOperate -= OnOperateComplete;
		base.OnDestroy ();
	}

#region Callbacks
	void OnOperateComplete(Mikan.CSAGA.CardOpertaionType type, Mikan.CSAGA.SyncCard cardDiff, Mikan.CSAGA.PropertyDiff playerDiff)
	{
		/*	//Destroyed when button clicked.
		if(type == Mikan.CSAGA.CardOpertaionType.Awaken)
		{
			Destroy(gameObject);
		}
		*/
	}

#endregion
}
