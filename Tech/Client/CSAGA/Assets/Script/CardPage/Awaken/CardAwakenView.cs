﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using Mikan.CSAGA.Data;

public class CardAwakenView : Stage {

	public UILabel titleLabel;
	public GameObject awakenBtn0;
	public UITexture awakenBg0;
	public UILabel awakenLabel0;
	public GameObject awakenBtn1;
	public UITexture awakenBg1;
	public UILabel awakenLabel1;
	public UILabel LuckyLabel;
	public GameObject cancelBtn;
	public Stars stars;
	public CardProfile current;
	public UIGrid grid;
	public GameObject buttonPrefab;
	//FX
	public FightResLoader fxLoader;
	public GameObject cardFxPanel;

	private CardData card;
	private Mikan.CSAGA.ConstData.CardData data;
	private bool Inited = false;
	private Dictionary<int, AwakenButton> buttonDict = new Dictionary<int, AwakenButton>();

	protected override void Initialize ()
	{
		card = Context.Instance.Card;
		data = ConstData.Tables.Card[card.CardID];

		//preload fx
		fxLoader.Preload("StrengthenFX");

		//Title
		titleLabel.text = ConstData.Tables.GetSystemText(124);
		//Buttons
		awakenLabel0.text = ConstData.Tables.GetSystemText(75);
		awakenLabel1.text = ConstData.Tables.GetSystemText(460);
		//Cancel
		UIEventListener.Get(cancelBtn).onClick = Close;

		GameSystem.Service.OnCardOperate += OnOperateComplete;

		Refresh();
		base.Initialize ();
	}

	public void Refresh()
	{
		CardProperty cp = card.Origin.Calc();
		current.UpdateProperty(0);

		//Awaken 0
		Mikan.CSAGA.ConstData.Awaken awakenData = ConstData.Tables.Awaken.SelectFirst(o=>{ return o.n_CARDID == data.n_ID && o.n_GROUP == 0 && o.n_RARE == card.Origin.Rare; });
		if(awakenData != null)
		{
			NGUITools.SetActive(awakenBtn0, true);
			ResourceManager.Instance.LoadTexture("ICON/" + awakenData.s_ICON, awakenBg0);
			stars.viewCount = card.Origin.Rare + 1;
			stars.ApplyChange();

			UIEventListener.Get(awakenBtn0).onClick = OnAwaken0Click;
		}
		else
			NGUITools.SetActive(awakenBtn0, false);

		//Awaken Luck
		int luck = card.Origin.Luck;
		if(luck < data.n_LUCK_MAX && data.n_LUCK_GIFT > 0)
		{
			NGUITools.SetActive(awakenBtn1, true);
			luck++;
			LuckyLabel.text = luck.ToString();
			ResourceManager.Instance.LoadTexture("ICON/ICON_AWAKEN_012", awakenBg1);

			UIEventListener.Get(awakenBtn1).onClick = OnAwaken1Click;
		}
		else
		{
			//Luck max
			NGUITools.SetActive(awakenBtn1, false);
		}


		//other awakens.
		for(int i = 1; i <= 5; i++)
		{
			bool hasAwaken = false;
			int awakenLv = card.Origin.GetAwakenLV(i);
			Mikan.CSAGA.ConstData.Awaken nowLvData = null;
			int maxLv = 0;
			foreach(Mikan.CSAGA.ConstData.Awaken aData in ConstData.Tables.Awaken.Select(o=>{ return o.n_CARDID == data.n_ID && o.n_GROUP == i; }))
			{
				hasAwaken = true;
				if(aData.n_LV == awakenLv || (awakenLv == 0 && aData.n_LV == 1))
					nowLvData = aData;
				if(aData.n_LV > maxLv)
					maxLv = aData.n_LV;
			}

			if(hasAwaken)
			{
				AwakenButton aBtn = null;
				if(!Inited)
				{
					aBtn = NGUITools.AddChild(grid.gameObject, buttonPrefab).GetComponent<AwakenButton>();
					aBtn.Init(i);
					UIEventListener.Get(aBtn.gameObject).onClick = OnOtherAwakenClick;

					buttonDict.Add(i, aBtn);
				}
				else
				{
					if(buttonDict.ContainsKey(i))
						aBtn = buttonDict[i];
				}

				aBtn.Refresh(awakenLv, maxLv, nowLvData);
			}
		}
		Inited = true;

		grid.Reposition();
	}

	void OnAwaken0Click(GameObject btn)
	{
		//GameSystem.Service.OnCardOperate += (Mikan.CSAGA.CardOpertaionType type, Mikan.CSAGA.SyncCard cardDiff, Mikan.CSAGA.PropertyDiff playerDiff) => {};
		//GameSystem.Service.CardAwaken(card.Origin.Serial, 0);
		Context.Instance.AwakenGroup = 0;
		Context.Instance.CloseWithStage = gameObject;
		Message.Send(MessageID.SwitchStage, StageID.CardAwakenConfirm);
	}

	void OnOtherAwakenClick(GameObject btn)
	{
		AwakenButton aBtn = btn.GetComponent<AwakenButton>();
		if(aBtn != null)
		{
			int lvNow = card.Origin.GetAwakenLV(aBtn.Group);
			if(lvNow < aBtn.MaxLv)
			{
				Context.Instance.AwakenGroup = aBtn.Group;
				Context.Instance.CloseWithStage = gameObject;
				Message.Send(MessageID.SwitchStage, StageID.CardAwakenConfirm);
			}
		}
	}

	void OnAwaken1Click(GameObject btn)
	{
		Context.Instance.AwakenGroup = -1;
		Context.Instance.CloseWithStage = gameObject;
		Message.Send(MessageID.SwitchStage, StageID.CardAwakenConfirm);
	}

	void Close(GameObject btn)
	{
		Destroy(gameObject);
	}

	protected override void OnDestroy ()
	{
		if(GameSystem.IsAvailable)
			GameSystem.Service.OnCardOperate -= OnOperateComplete;
		base.OnDestroy ();
	}


#region Callbacks
	void OnOperateComplete(Mikan.CSAGA.CardOpertaionType type, Mikan.CSAGA.SyncCard cardDiff, Mikan.CSAGA.PropertyDiff playerDiff)
	{
		if(type == Mikan.CSAGA.CardOpertaionType.Awaken)
		{
			Refresh();
			//Show FX
			GameObject fxRoot = null;
			int group = Context.Instance.AwakenGroup;
			if(group == 0)
				fxRoot = cardFxPanel;
			else
			{
				if(buttonDict.ContainsKey(group))
					fxRoot = buttonDict[group].FxPanel;
			}
			if(fxRoot != null)
			{
				fxLoader.PlayFX("StrengthenFX", Vector3.zero, fxRoot);
			}
			//Sound
			Message.Send(MessageID.PlaySound, SoundID.POWERUP);
		}
		else if(type == Mikan.CSAGA.CardOpertaionType.Luck)
		{
			Refresh();
			//Show FX
			fxLoader.PlayFX("StrengthenFX", Vector3.zero, cardFxPanel);		
			//Sound
			Message.Send(MessageID.PlaySound, SoundID.POWERUP);
		}
	}
	
#endregion

}
