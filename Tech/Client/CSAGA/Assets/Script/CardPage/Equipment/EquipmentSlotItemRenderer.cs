﻿using UnityEngine;
using System.Collections;
using System;

public class EquipmentSlotItemRenderer : ItemItemRenderer<EquipmentData> {

	protected override void ApplyChange (EquipmentData data)
	{
		base.ApplyChange (data);
		var toggle = GetComponent<UIToggle>();
		
		if(toggle != null && Context.Instance.SlotIndex == Index) toggle.value = true;
	}

	protected override void OnClick ()
	{
		Context.Instance.SlotIndex = Index;
		Message.Send(MessageID.SelectItem, Index);
	}

}