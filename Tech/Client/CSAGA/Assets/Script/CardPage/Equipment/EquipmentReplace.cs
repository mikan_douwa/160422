﻿using UnityEngine;
using System.Collections;

public class EquipmentReplace : MonoBehaviour {

	public Item originObj;
	public Item newObj;

	// Use this for initialization
	void Start () {
		originObj.ItemID = Context.Instance.Card.Origin.GetEquipment(Context.Instance.SlotIndex).ID;
		originObj.ApplyChange();

		newObj.ItemID = Context.Instance.Item.ItemID;
		newObj.ApplyChange();
	}
	

}
