﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CardEquipment : Stage {

	public CardProfile expect;

	private CardData card;

	protected override void Initialize ()
	{
		card = Context.Instance.Card;

		GameSystem.Service.OnEquipmentReplace += OnEquipmentReplace;
		Message.AddListener(MessageID.SelectItem, OnSelectItem);
		Message.AddListener(MessageID.EquipConfirm, OnEquipConfirm);

		base.Initialize ();
	}

	void OnEquipmentReplace ()
	{
		Destroy(gameObject);
	}

	protected override void OnDestroy ()
	{
		if(GameSystem.IsAvailable) 
		{
			GameSystem.Service.OnEquipmentReplace -= OnEquipmentReplace;
			Message.RemoveListener(MessageID.SelectItem, OnSelectItem);
			Message.RemoveListener(MessageID.EquipConfirm, OnEquipConfirm);

			Context.Instance.Item = null;
		}

		base.OnDestroy ();
	}

	private void OnSelectItem(IMessage msg)
	{
		if(!(msg.Data is EquipmentData)) return;

		var appends = new List<Mikan.CSAGA.Data.Equipment>();

		for(int i = 0; i < card.Origin.SlotCount; ++i)
		{
			if(i == Context.Instance.SlotIndex)
			{
				var equipment = Context.Instance.Item as EquipmentData;
				if(equipment != null) appends.Add(equipment.Origin);
			}
			else
			{	
				var equipment = card.Origin.GetEquipment(i);
				
				if(equipment != null) appends.Add(equipment);
			}

		}

		expect.UpdateProperty(0, appends);

	}

	private void OnEquipConfirm()
	{
		var equipment = Context.Instance.Item as EquipmentData;
		if(equipment == null) 
		{
			MessageBox.Show(SystemTextID.TIP_SELECT_EQUIPMENT);
			return;
		}

		var origin = GameSystem.Service.EquipmentInfo.List.Find(o=>{ return o.CardSerial == card.Origin.Serial && o[Mikan.CSAGA.PropertyID.Slot] == Context.Instance.SlotIndex; });

		if(origin != null)
		{
			MessageBox.ShowConfirmWindow("GameScene/Card/EquipmentReplaceMessageBox", ConstData.GetSystemText(SystemTextID.TIP_REPLACE_EQUIPMENT),
                 index =>
                 {
					if(index == 0) GameSystem.Service.EquipmentReplace(card.Origin.Serial, Context.Instance.SlotIndex, equipment.Origin.Serial);	
				 }
			);
			                                                
		}
		else
			GameSystem.Service.EquipmentReplace(card.Origin.Serial, Context.Instance.SlotIndex, equipment.Origin.Serial);
	}
}
