﻿using UnityEngine;
using System.Collections;

public class EquipmentInfo : MonoBehaviour {
	
	public Item item;
	public Stars stars;

	public UILabel itemName;

	public UILabel hp;
	public UILabel atk;
	public UILabel rev;
	public UILabel chg;

	public UILabel tip;

	public UIWidget container;
	public UITable table;

	public GameObject lckObj;

	public GameObject counterObj;
	public UISprite counterType;
	public UILabel counter;

	public GameObject bindObj;
	public Unit bindCard;
	public UILabel bindTip;
	
	public int bindHeight = 0;

	private EquipmentData data;

	private bool originIsLocked;

	// Use this for initialization
	void Start () {

		var msgBox = GetComponent<MessageBox>();

		if(msgBox.HasButton) container.height += 50;

		var args = msgBox.context.args;

		data = args[0] as EquipmentData;

		originIsLocked = data.IsLocked;

		item.ItemID = data.ItemID;
		item.ApplyChange();

		itemName.text = ConstData.GetItemName(data.ItemID);
	
		var rawdata = ConstData.Tables.Item[data.ItemID];

		if(data.Origin != null)
		{
			hp.text = Format(data.Origin.Hp);
			atk.text = Format(data.Origin.Atk);
			rev.text = Format(data.Origin.Rev);
			chg.text = Format(data.Origin.Chg, "%");

			if(lckObj != null && !data.Origin.CardSerial.IsExternal)
			{
				lckObj.SetActive(true);

				if(!originIsLocked)
				{
					foreach(var sprite in lckObj.GetComponentsInChildren<UISprite>()) sprite.color = Color.gray;
				}
			}

			if(counterObj != null && data.Origin.Counter > 0)
			{
				counterObj.SetActive(true);
				counter.text = Format(data.Origin.Counter, "%");
				counterType.spriteName = string.Format("attribute_{0:000}", data.Origin.CounterType);
			}
		}
		else
		{
			hp.text = FormatUnknown(rawdata.n_HP_MIN, rawdata.n_HP_MAX);
			atk.text = FormatUnknown(rawdata.n_ATK_MIN, rawdata.n_ATK_MAX);
			rev.text = FormatUnknown(rawdata.n_REV_MIN, rawdata.n_REV_MAX);

			if(rawdata.n_CHG_RATE == 100 && rawdata.n_CHG_MIN == rawdata.n_CHG_MAX)
				chg.text = Format(rawdata.n_CHG_MIN, "%");
			else
				chg.text = "? ? ?";

			if(counterObj != null && rawdata.n_COUNTER_RATE > 0)
			{
				counterObj.SetActive(true);
				counterType.spriteName = "attribute_q";
				for(int i = 0; i < 5; ++i)
				{
					if((int)Mathf.Pow(2, i) != rawdata.n_COUNTER_TYPE) continue;
					counterType.spriteName = string.Format("attribute_{0:000}", rawdata.n_COUNTER_TYPE);
					break;
				}

				if(rawdata.n_COUNTER_MIN == rawdata.n_COUNTER_MAX)
					counter.text = Format(rawdata.n_COUNTER_MIN, "%");
				else
					counter.text = "? ? ?";
			}
		}

		stars.viewCount = rawdata.n_RARE;
		stars.ApplyChange();

		var tipText = ConstData.GetItemTip(data.ItemID);

		if(!string.IsNullOrEmpty(tipText)) 
		{
			tip.gameObject.SetActive(true);
			tip.text = tipText;

			container.height += tip.height;
		}

		if(bindObj != null && rawdata.n_CARD > 0)
		{
			bindObj.SetActive(true);
			bindCard.CardID = rawdata.n_CARD;
			bindCard.ApplyChange();
			bindTip.text = ConstData.GetItemTip2(data.ItemID);

			container.height += bindHeight;
		}

		if(table != null) table.repositionNow = true;
	}

	void OnDestroy()
	{
		if(data.IsLocked != originIsLocked)
		{
			GameSystem.Service.EquipmentLock(data.Origin.Serial, data.IsLocked);
		}
	}

	public void SetLock()
	{
		data.IsLocked = !data.IsLocked;

		if(data.IsLocked)
		{
			Message.Send(MessageID.PlaySound, SoundID.CLICK);
			foreach(var item in lckObj.GetComponentsInChildren<UISprite>()) item.color = Color.white;
		}
		else
		{
			Message.Send(MessageID.PlaySound, SoundID.CANCEL);
			foreach(var item in lckObj.GetComponentsInChildren<UISprite>()) item.color = Color.gray;
		}
		/*
		card.IsLocked = !card.IsLocked;
		
		if(card.IsLocked)
		{
			Message.Send(MessageID.PlaySound, SoundID.CLICK);
			foreach(var item in lckObj.GetComponentsInChildren<UISprite>()) item.color = Color.white;
		}
		else
		{
			Message.Send(MessageID.PlaySound, SoundID.CANCEL);
			foreach(var item in lckObj.GetComponentsInChildren<UISprite>()) item.color = Color.gray;
		}
		*/
	}

	private string Format(int val, string postfix = "")
	{
		if(val == 0) return "--";
		return string.Format ("{0}{1}", val, postfix);
	}

	private string FormatUnknown(int min, int max)
	{
		if(min == 0 && max == 0) return "0";
		if(min == max) return min.ToString();
		return "? ? ?";
	}

}
