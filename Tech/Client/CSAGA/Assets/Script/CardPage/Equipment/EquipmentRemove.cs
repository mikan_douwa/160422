﻿using UnityEngine;
using System.Collections;

public class EquipmentRemove : SubmitWindow {

	public UILabel equipName;

	public Icon priceIcon;
	
	public UILabel pricePrefix;
	
	private EquipmentData data;

	private Mikan.CSAGA.ConstData.Item rawdata;
	
	protected override void OnReady ()
	{
		base.OnReady ();
		
		data = context.args[0] as EquipmentData;

		rawdata = ConstData.Tables.Item[data.ItemID];

		equipName.text = ConstData.GetSystemText(SystemTextID._262_TIP_REMOVE_EQUIP_CONFIRM_V1, ConstData.GetItemName(data.ItemID));
		
		pricePrefix.text = ConstData.GetSystemText(SystemTextID.EXPENSE_V1, "");
		
		priceIcon.IconType = rawdata.n_TAKE_COINID;
		priceIcon.Count = rawdata.n_TAKE_COIN;
		priceIcon.ApplyChange();
	}
	
	#region implemented abstract members of SubmitWindow
	
	protected override bool OnSubmit ()
	{
		if(!ShopManager.Instance.CheckCurrency(rawdata.n_TAKE_COINID, rawdata.n_TAKE_COIN)) return true;

		if(!QuestManager.Instance.CheckEquipmentSpace(1)) return true;
		
		GameSystem.Service.EquipmentRemove(data.Origin.Serial);

		return true;
	}
	
	#endregion
	
	
	
}
