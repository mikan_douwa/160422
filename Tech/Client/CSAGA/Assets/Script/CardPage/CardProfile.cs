﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CardProfile : MonoBehaviour {

	public TextureLoader pic;
	
	public UnitLoader icon;
	
	public UILabel type;
	public UISprite attribute;
	
	public UILabel lv;
	public UILabel expLeft;
	public UIProgressBar expProgress;
	public UILabel hp;
	public UILabel atk;
	public UILabel rev;
	public UILabel cng;
	public UILabel kizuna;
	public UIProgressBar kizunaProgress;
	public GameObject kizunaObj;

	public UILabel luck;

	public Stars stars;

	public UILabel hpPlus;
	public UILabel atkPlus;
	public UILabel revPlus;

	public ItemList equipments;

	public ItemList awakens;

	public CardProfile reference;
	
	private CardData card;
	
	private Mikan.CSAGA.ConstData.CardData data;

	public Mikan.CSAGA.Data.CardProperty Property;

	private Mikan.LiveOperationQueue opQueue = new Mikan.LiveOperationQueue();

	public void ApplyChange(CardData card)
	{
		Initialize(card);
		UpdateProperty(0);
	}

	public void UpdateProperty(int exp, IEnumerable<Mikan.CSAGA.Data.Equipment> appends, int awakenLv = 0)
	{
		if(card == null) Initialize();

		int rare = card.Origin.Rare + awakenLv;
		var prop = card.Origin.Calc(card.Origin.Exp + exp, rare, card.Luck);

		if(appends != null) prop.Append(appends);
		
		if(lv != null) lv.text = string.Format("{0}/{1}", prop.LV, prop.LVMax);
		
		if(icon != null)
		{
			icon.LV = prop.LV;
			icon.ApplyChange();
		}

		if(attribute != null) attribute.spriteName = string.Format("attribute_{0:000}", data.n_TYPE);

		if(reference != null && reference.Property != null)
		{
			if(hp != null) hp.text = Format(reference.Property.HP, prop.HP);
			if(atk != null) atk.text = Format(reference.Property.Atk, prop.Atk);
			if(rev != null) rev.text = Format(reference.Property.Rev, prop.Rev);
		}
		else
		{
			if(hp != null) hp.text = Format(prop.HP - prop.HPDiff, prop.HP);
			if(atk != null) atk.text = Format(prop.Atk - prop.AtkDiff, prop.Atk);
			if(rev != null) rev.text = Format(prop.Rev - prop.RevDiff, prop.Rev);

		}

		if(hpPlus != null) hpPlus.text = "+" + prop.HPPlus.ToString();
		if(atkPlus != null) atkPlus.text = "+" + prop.AtkPlus.ToString();
		if(revPlus != null) revPlus.text = "+" + prop.RevPlus.ToString();

		if(kizunaObj != null)
		{
			if(card.Origin.Serial.IsEmpty)
				kizunaObj.SetActive(false);
			else
			{
				kizunaObj.SetActive(true);

				if(kizuna != null) kizuna.text = ConstData.GetSystemText(SystemTextID._173_KLV_V1, card.Origin.Kizuna / 100);
				
				if(kizunaProgress != null) kizunaProgress.value = (float)(card.Origin.Kizuna % 100) / 100f;
			}
		}

		if(luck != null)
		{
			if(card.Luck > 0)
				luck.text = card.Luck.ToString();
			else
				luck.text = card.Origin.Luck.ToString();
		}

		if(cng != null) 
		{
			if(reference != null && reference.Property != null)
				cng.text = Format(reference.Property.Charge, prop.Charge);
			else
				cng.text = Format(prop.ChargeDiff != null && prop.ChargeDiff.Count > 0 ? 100f : prop.Charge, prop.Charge);
		}
		
		if(expLeft != null)
		{
			if(prop.LV < prop.LVMax)
				expLeft.text = (prop.Next - card.Origin.Exp).ToString();
			else
				expLeft.text = "--";
			
			expProgress.value = prop.ExpProgress;
		}
		
		if(equipments != null)
		{
			var list = new List<EquipmentData>();

			if(card.Origin.IsFormal)
			{
				for(int i = 0; i < card.Origin.SlotCount; ++i)
				{
					var item = new EquipmentData();
					item.Type = Mikan.CSAGA.ItemType.Equipment;
					item.Origin = card.Origin.GetEquipment(i);
					if(item.Origin != null) item.ItemID = item.Origin.ID;
					list.Add(item);
				}
			}
			
			equipments.Setup(list);
		}

		if(stars != null)
		{
			stars.viewCount = rare;
			stars.ApplyChange();
		}

		if(awakens != null)
		{
			var awakenList = new List<CardAwakenData>();

			for(int i = 1; i <= ConstData.Tables.AwakenCount; ++i)
			{
				var obj = new CardAwakenData();

				obj.LV = card.Origin.GetAwakenLV(i);

				var compareLv = obj.LV > 0 ? obj.LV : 1;

				obj.Origin = ConstData.Tables.Awaken.SelectFirst(o=>{ return o.n_CARDID == card.CardID && o.n_GROUP == i && o.n_LV == compareLv; });

				if(obj.Origin == null) continue;

				awakenList.Add(obj);
			}

			awakens.Setup(awakenList);
		}

		Property = prop;

		TweenProperty();
	}
	private string Format(float originVal, float newVal)
	{
		if(originVal > newVal) return "[00ffff]" + newVal.ToString(UIDefine.FORMAT_FLOAT);
		if(originVal < newVal) return "[ff0000]" + newVal.ToString(UIDefine.FORMAT_FLOAT);
		return newVal.ToString(UIDefine.FORMAT_FLOAT);
	}

	private string Format(int originVal, int newVal)
	{
		if(originVal > newVal) return "[ff0000]" + newVal.ToString();
		if(originVal < newVal) return "[00ffff]" + newVal.ToString();
		return newVal.ToString();
	}

	public void UpdateProperty(int exp)
	{
		if(card == null) Initialize();
		UpdateProperty(exp, card.Origin.SelectEquipment());
	}
	
	void Start()
	{
		GameSystem.Service.OnEquipmentReplace += OnEquipmentReplace;

		if(card == null) UpdateProperty(0);
		
		if(pic != null) 
			pic.Load("CARD/CARD_" + data.s_FILENAME);

		if(icon != null)
		{
			icon.CardID = card.CardID;
			icon.ApplyChange();
		}
		
		if(type != null) 
			type.text = ConstData.GetGroupText(data.n_GROUP);
	}

	void OnDestroy()
	{
		opQueue.Clear();

		if(GameSystem.IsAvailable) GameSystem.Service.OnEquipmentReplace -= OnEquipmentReplace;
	}

	void OnEquipmentReplace ()
	{
		UpdateProperty(0);
	}

	private void Initialize()
	{
		Initialize(Context.Instance.Card);
	}

	private void Initialize(CardData card)
	{
		this.card = card;
		data = ConstData.Tables.Card[card.CardID];
	}

	private void TweenProperty()
	{
		const float ALPHA_DURATION = 1f;

		if(!opQueue.IsEmpty)
		{
			opQueue.Clear();
			hp.alpha = atk.alpha = rev.alpha = 1;
			hpPlus.alpha = atkPlus.alpha = revPlus.alpha = 0;
		}


		var opList = new List<Mikan.IOperation>();

		if(hpPlus != null && Property.HPPlus > 0)
		{
			opList.Add(new TweenAlphaOperation(hp.gameObject, ALPHA_DURATION, 1, 0));
			opList.Add(new TweenAlphaOperation(hpPlus.gameObject, ALPHA_DURATION, 0, 1));
		}

		if(atkPlus != null && Property.AtkPlus > 0)
		{
			opList.Add(new TweenAlphaOperation(atk.gameObject, ALPHA_DURATION, 1, 0));
			opList.Add(new TweenAlphaOperation(atkPlus.gameObject, ALPHA_DURATION, 0, 1));
		}

		if(revPlus != null && Property.RevPlus > 0)
		{
			opList.Add(new TweenAlphaOperation(rev.gameObject, ALPHA_DURATION, 1, 0));
			opList.Add(new TweenAlphaOperation(revPlus.gameObject, ALPHA_DURATION, 0, 1));
		}

		if(opList.Count == 0) return;

		var para1 = new Mikan.ParallelOperationChain(opList);

		opList = new List<Mikan.IOperation>();

		if(hpPlus != null && Property.HPPlus > 0)
		{
			opList.Add(new TweenAlphaOperation(hp.gameObject, ALPHA_DURATION, 0, 1));
			opList.Add(new TweenAlphaOperation(hpPlus.gameObject, ALPHA_DURATION, 1, 0));
		}
		
		if(atkPlus != null && Property.AtkPlus > 0)
		{
			opList.Add(new TweenAlphaOperation(atk.gameObject, ALPHA_DURATION, 0, 1));
			opList.Add(new TweenAlphaOperation(atkPlus.gameObject, ALPHA_DURATION, 1, 0));
		}
		
		if(revPlus != null && Property.RevPlus > 0)
		{
			opList.Add(new TweenAlphaOperation(rev.gameObject, ALPHA_DURATION, 0, 1));
			opList.Add(new TweenAlphaOperation(revPlus.gameObject, ALPHA_DURATION, 1, 0));
		}

		var para2 = new Mikan.ParallelOperationChain(opList);

		opList = new List<Mikan.IOperation>();
		opList.Add(new DelayOperation(2f));
		opList.Add(para1);
		opList.Add(new DelayOperation(2f));
		opList.Add(para2);

		opQueue.Enqueue(new Mikan.LoopOperation(new Mikan.SequentialOperationChain(opList)));
	}

}
