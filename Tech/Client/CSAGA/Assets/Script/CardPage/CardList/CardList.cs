﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CardList : ItemList {

	public enum BEHAVIOR
	{
		Auto = 0,
		TeamMemberSelect,
		TeamEdit,
		TeamPreview,
		PVPEdit,
	}
	
	public BEHAVIOR behavior = BEHAVIOR.Auto;
	public bool isStatic = false;

	private CardItemDataProvider provider;
	private StageID cachedStageID;

	private bool isFirstTime = true;

	public bool IsReady { get; private set; }
	protected override void OnStart ()
	{
		base.OnStart ();

		cachedStageID = Context.Instance.StageID;

		Rebuild();

		//if(Context.Instance.StageID == StageID.CardContact && Context.Instance.RestoreQuestEntrance) Message.Send(MessageID.SwitchStage, StageID.CardContactView);
	}

	public void Rebuild()
	{
		IsReady = false;

		var list = GetList();
		
		provider = new CardItemDataProvider(list, isStatic);
		
		if(!isStatic) 
		{
			Message.AddListener(MessageID.Sort, OnSort);
			provider.Sort(Context.Instance.SortCard.Value);
		}
		
		Setup(provider);

		StartCoroutine(WaitForReady());
	}

	private IEnumerator WaitForReady()
	{
		while(UIBase.IsChanging) yield return new WaitForEndOfFrame();
		IsReady = true;
	}

	protected override void OnCommitChange ()
	{
		if(isFirstTime)
		{
			isFirstTime = false;
			base.OnCommitChange ();
		}
	}

	private List<CardData> GetList()
	{
		switch(behavior)
		{
		case BEHAVIOR.TeamMemberSelect:
		{
			var list = new List<CardData>();

			if(Context.Instance.TeamMemberIndex > 0)
				list.Add(new CardData{ CardID = UIDefine.RemoveCardID });
			
			if(cachedStageID == StageID.Quest)
				list.Add(new CardData{ CardID = UIDefine.FellowCardID });

			AddCards(list);

			return list;
		}
		case BEHAVIOR.TeamEdit:
		{
			var list = new List<CardData>();
			AddCards(list);
			return list;
		}
		case BEHAVIOR.TeamPreview:
		{
			var list = new List<CardData>(5);
			foreach(var item in Context.Instance.Cards)
			{
				var data = new CardData{ Origin = GameSystem.Service.CardInfo[item] };
				if(data.Origin != null) data.CardID = data.Origin.ID;
				list.Add(data);
			}
			return list;
		}
		case BEHAVIOR.Auto:
		default:
			return GetListViaStageID(cachedStageID);

		}
	}

	private List<CardData> GetListViaStageID(StageID stageId)
	{
		var list = new List<CardData>();
			
		if(stageId == StageID.Gallery)
		{
			//container.cellHeight = 110;
			//container.startPos = new Vector2(-5, -55);
			foreach(var item in ConstData.Tables.Card.Select())
			{
				if(item.n_GALLERY == 0) continue;
				var data = new CardData{ CardID = item.n_ID, ForceSortID = item.n_GALLERY };
				data.Origin = Mikan.CSAGA.CardUtils.GenerateDummy(item.n_ID);
				list.Add(data);
			}
		}
		else if(stageId == StageID.GachaDrawShowResult)
		{
			container.cellHeight = 165;
			
			foreach(var item in Context.Instance.Cards)
			{
				var data = new CardData{ ForceSortID = list.Count };
				data.Origin = GameSystem.Service.CardInfo[item];
				data.CardID = data.Origin.ID;
				data.Property = data.Origin.Calc();
				list.Add(data);
			}
		}
		else
		{
			if(stageId == StageID.CardSell) Context.Instance.Cards = new List<Mikan.Serial>();
			AddCards(list);
		}
		
		return list;
	}

	private void AddCards(List<CardData> list)
	{
		foreach(var item in GameSystem.Service.CardInfo.List)
		{
			var data = new CardData{ Origin = item, CardID = item.ID, IsLocked = item.IsLocked };
			data.Property = data.Origin.Calc();
			data.Property.Append(data.Origin.SelectEquipment());
			list.Add(data);
		}
	}

	protected override void OnDestroy ()
	{
		
		//foreach(var item in GetComponentsInChildren<Unit>()) item.transform.parent = Essentials.Instance.transform;

		Message.RemoveListener(MessageID.Sort, OnSort);

		if(GameSystem.IsAvailable && cachedStageID == StageID.CardList)
		{
			var locked = new List<Mikan.Serial>();
			var unlocked = new List<Mikan.Serial>();

			for(int i = 0; i < provider.Count; ++i)
			{
				var item = provider[i];
				if(item.IsLocked != item.Origin.IsLocked)
				{
					if(item.IsLocked) 
						locked.Add(item.Origin.Serial);
					else
						unlocked.Add(item.Origin.Serial);
				}
			}

			if(locked.Count > 0 || unlocked.Count > 0)
				GameSystem.Service.CardLock(locked, unlocked);
		}

		base.OnDestroy ();
	}

	protected override ItemRenderer GetItemRenderer (GameObject item)
	{
		var renderer = default(ItemRenderer);

		switch(behavior)
		{
		case BEHAVIOR.TeamMemberSelect:
			renderer = item.AddComponent<CardTeamMemberSelectItemRenderer>();
			break;
		case BEHAVIOR.TeamEdit:
			renderer = item.AddComponent<CardTeamMemberBatchSelectItemRenderer>();
			break;
		case BEHAVIOR.TeamPreview:
			renderer = item.AddComponent<CardBatchSelectPreviewItemRenderer>();
			break;
		case BEHAVIOR.PVPEdit:
			renderer = item.AddComponent<PVPSelectCardItemRenderer>();
			break;
		case BEHAVIOR.Auto:
		default:
		{
			switch(cachedStageID)
			{
			case StageID.CardList:
				renderer = item.AddComponent<CardListItemRenderer>();
				break;
			case StageID.CardEnhance:
				renderer = item.AddComponent<CardEnhanceItemRenderer>();
				break;
			case StageID.CardSell:
				renderer = item.AddComponent<CardSellItemRenderer>();
				break;
			case StageID.CardContact:
				renderer = item.AddComponent<CardContactSelectItemRenderer>();
				break;
			case StageID.CardAwaken:
				renderer = item.AddComponent<CardAwakenSelectItemRenderer>();
				break;
			case StageID.Gallery:
				renderer = item.AddComponent<CardGalleryItemRenderer>();
				break;
			case StageID.GachaDrawShowResult:
				renderer = item.AddComponent<CardGachaResultItemRenderer>();
				break;
			default:
				renderer = item.AddComponent<CardItemRenderer>();
				break;
			}
			break;
		}
		}

		return renderer;
	}

	private void OnSort(IMessage msg)
	{
		if(Context.Instance.SortTarget != SortTarget.Card) return;

		var method = (SortMethod)msg.Data;
		provider.Sort(method);
	}

	public void Refresh()
	{
		provider.Refresh(GetList(), Context.Instance.SortCard.Value);
	}

	public void Remove(List<long> deleteList)
	{
		provider.Remove(deleteList);
	}
}

public class CardData
{
	public int CardID;
	public int Luck;
	public Mikan.CSAGA.Data.Card Origin;
	public Mikan.CSAGA.Data.CardProperty Property;
	public int ForceSortID;
	public bool IsLocked;

	public CardData Clone()
	{
		var copy = new CardData();
		copy.CardID = CardID;
		copy.Luck = Luck;
		copy.Origin = Origin;
		copy.Property = Property;
		copy.ForceSortID = ForceSortID;
		copy.IsLocked = IsLocked;
		return copy;
	}
}

class CardItemDataProvider : ListItemDataProvider<CardData>
{
	private bool isStatic;
	public CardItemDataProvider(List<CardData> list, bool isStatic)
	{
		this.list = list;
		this.isStatic = isStatic;
	}

	public void Sort(SortMethod method)
	{
		if(isStatic) return;
		list.Sort(CardSortMethod.Get(method));
		TriggerOnChange();
	}

	public void Refresh(List<CardData> list, SortMethod method)
	{
		this.list  = list;
		if(isStatic)
			TriggerOnChange();
		else
			Sort (method);
	}

	public void Remove(List<long> cards)
	{
		var deleteList = new List<long>();

		foreach(var item in list)
		{
			if(cards.Contains(item.Origin.Serial.Value)) deleteList.Add(item.Origin.Serial.Value);
		}

		if(deleteList.Count == 0) return;

		list.RemoveAll((o)=>deleteList.Contains(o.Origin.Serial.Value));

		TriggerOnChange();
	}
}

