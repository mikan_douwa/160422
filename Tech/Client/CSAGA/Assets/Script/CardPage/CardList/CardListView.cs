﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CardListView : View {

	public CardList.BEHAVIOR behavior = CardList.BEHAVIOR.Auto;

	public GameObject container;
	public GameObject prefab;
	public GameObject listPrefab;
	public CardList list;
	public SpaceInfo spaceInfo;
	
	virtual protected bool ShowSpaceInfo { get { return true; } }

	public override bool IsReady {
		get {
			return IsInitialized && list != null && list.IsReady;
		}
	}

	protected override void OnStart ()
	{
		base.OnStart ();
		if(container == null) container = gameObject;
	}

	protected override void Initialize ()
	{
		GameSystem.Service.OnCardOperate += OnCardOperate;
		Message.AddListener(MessageID.SelectItem, OnSelectItem);

		if(ShowSpaceInfo)
		{
			ResourceManager.Instance.CreatePrefabInstance(container, "Common/SpaceInfo", o=>{ 
				spaceInfo = o.GetComponent<SpaceInfo>(); 
				spaceInfo.type = SpaceInfo.TYPE.CARD;
				spaceInfo.ApplyChange(); 
			});
		}

		if(list != null)
			StartCoroutine(WaitForReady());
		else if(listPrefab == null)
			ResourceManager.Instance.CreatePrefabInstance(container, "Template/CardList", OnCreateComplete);
		else
			OnCreateComplete(UIManager.Instance.AddChild(container, listPrefab));
	}

	public override void OnGetBack ()
	{
		if(list != null) 
		{
			if(NeedRebuild)
				list.Rebuild();
			else
				list.ForceRefresh();
		}
	}

	virtual protected bool NeedRebuild { get { return false; } }

	virtual protected void OnSelectItem(IMessage msg){}
	
	protected override void OnDestroy ()
	{
		if(GameSystem.IsAvailable) GameSystem.Service.OnCardOperate -= OnCardOperate;
		Message.RemoveListener(MessageID.SelectItem, OnSelectItem);
		base.OnDestroy ();
	}
	
	private void OnCardOperate (Mikan.CSAGA.CardOpertaionType type, Mikan.CSAGA.SyncCard cardDiff, Mikan.CSAGA.PropertyDiff playerDiff)
	{
		if(type == Mikan.CSAGA.CardOpertaionType.Sell) 
		{
			if(list != null) list.Remove(cardDiff.DeleteList);
		}

		if(cardDiff != null && (cardDiff.AddList.Count > 0 || cardDiff.DeleteList.Count > 0)) spaceInfo.ApplyChange();
	}

	private void OnCreateComplete(GameObject obj)
	{
		list = obj.GetComponent<CardList>();
		list.behavior = behavior;
		if(prefab != null) list.prefab = prefab;

		StartCoroutine(WaitForReady());
	}

	private IEnumerator WaitForReady()
	{
		while(UIBase.IsChanging || !list.IsReady) yield return new WaitForFixedUpdate();

		InitializeComplete();
	}

}