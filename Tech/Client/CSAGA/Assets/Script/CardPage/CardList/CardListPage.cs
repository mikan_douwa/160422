﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CardListPage : Stage {

	public GameObject prefab;
	public GameObject listPrefab;
	public CardList list;
	public SpaceInfo spaceInfo;

	virtual protected bool ShowSpaceInfo { get { return true; } }
	protected override void Initialize ()
	{
		GameSystem.Service.OnCardOperate += OnCardOperate;
		Message.AddListener(MessageID.SelectItem, OnSelectItem);

		Rebuild();

		if(ShowSpaceInfo)
		{
			ResourceManager.Instance.CreatePrefabInstance(gameObject, "Common/SpaceInfo", o=>{ 
				spaceInfo = o.GetComponent<SpaceInfo>(); 
				spaceInfo.type = SpaceInfo.TYPE.CARD;
				spaceInfo.ApplyChange(); 
			});
		}
	}

	protected override void OnNext ()
	{
		Context.Instance.SortTarget = SortTarget.Card;
		
		Message.Send(MessageID.SwitchStage, StageID.SortBoard);
	}
	virtual protected void OnSelectItem(IMessage msg){}

	protected override void OnDestroy ()
	{
		if(GameSystem.IsAvailable) GameSystem.Service.OnCardOperate -= OnCardOperate;
		Message.RemoveListener(MessageID.SelectItem, OnSelectItem);
		base.OnDestroy ();
	}

	private void OnCardOperate (Mikan.CSAGA.CardOpertaionType type, Mikan.CSAGA.SyncCard cardDiff, Mikan.CSAGA.PropertyDiff playerDiff)
	{
		if(type == Mikan.CSAGA.CardOpertaionType.Sell) 
		{

			if(list != null) list.Remove(cardDiff.DeleteList);
		}

		if(cardDiff != null && (cardDiff.AddList.Count > 0 || cardDiff.DeleteList.Count > 0)) spaceInfo.ApplyChange();

	}

	private void OnCreateComplete(GameObject obj)
	{
		var old = list;
		list = obj.GetComponent<CardList>();
		if(prefab != null) list.prefab = prefab;

		if(old != null)
			Destroy(old.gameObject);
		else
		{
			StartCoroutine(WaitForInitialized());
		}
	}

	private void Rebuild()
	{
		if(listPrefab == null)
			ResourceManager.Instance.CreatePrefabInstance(gameObject, "Template/CardList", OnCreateComplete);
		else
			OnCreateComplete(UIManager.Instance.AddChild(gameObject, listPrefab));
	}

	private IEnumerator WaitForInitialized()
	{
		while(!list.IsReady) yield return new WaitForEndOfFrame();

		InitializeComplete();
	}
}