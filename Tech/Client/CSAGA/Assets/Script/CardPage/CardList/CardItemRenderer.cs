﻿using UnityEngine;
using System.Collections;

public class CardItemRenderer : ItemRenderer<CardData>
{
	protected Unit unit;
	
	private Mask mask;
	
	private CardChosenTip tip;
	
	private bool isActive = true;
	
	public bool enableLongPress = true;
	
	private SortMethod method;
	
	virtual public bool EnableLongPress { get { return enableLongPress; } }
	
	virtual protected bool UseDefaultFormat { get { return false; } }
	
	virtual protected bool ShowMask { get { return true; } }

	virtual protected GameObject prefab { get { return Essentials.Instance.unit; } }
	
	protected override bool OnIndexChange (int newIndex)
	{
		var isChanged = base.OnIndexChange (newIndex);
		
		if(!isChanged)
		{
			if(data != null) isChanged = (method != Context.Instance.SortCard.Value);
		}
		
		return isChanged;
	}
	
	protected bool Selected { get { return Context.Instance.Cards.Contains(data.Origin.Serial); } }
	
	#region implemented abstract members of ItemRenderer
	protected override void ApplyChange (CardData data)
	{
		Cache(data);
		
		if(data == null) return;
		
		method = Context.Instance.SortCard.Value;
		
		if(mask != null)
		{
			Destroy(mask.gameObject);
			mask = null;
		}
		
		if(tip != null)
		{
			Destroy(tip.gameObject);
			tip = null;
		}
		
		unit.CardID = data.CardID;
		unit.IsLocked = data.IsLocked;
		unit.Rare = 0;
		
		unit.IsExternal = data.Origin != null ? data.Origin.Serial.IsExternal : false;
		
		if(data.Origin != null)
		{
			unit.Rare = data.Origin.Rare;
			
			if(UseDefaultFormat)
			{
				unit.LVFormat = SystemTextID.LV_V1;
				unit.LV = Mikan.CSAGA.CardUtils.CalcLV(data.CardID, data.Origin.Rare, data.Origin.Exp);
			}
			else if(Context.Instance.StageID == StageID.Gallery)
			{
				unit.LVFormat = SystemTextID.NO_V1;
				unit.TextFormat = "000";
				unit.LV = ConstData.Tables.Card[unit.CardID].n_GALLERY;
			}
			else
			{
				unit.LVFormat = SystemTextID.NONE;
				unit.TextFormat = null;
				unit.ShowPrefixIcon = false;
				
				switch(method)
				{
				case SortMethod.Serial:	
				case SortMethod.LV:	
				case SortMethod.Type:
				case SortMethod.Rare:
				case SortMethod.ID:
					unit.LVFormat = SystemTextID.LV_V1;
					unit.LV = Mikan.CSAGA.CardUtils.CalcLV(data.CardID, data.Origin.Rare, data.Origin.Exp);
					break;
				case SortMethod.Kizuna:	
					unit.LV = Mathf.FloorToInt(data.Origin.Kizuna/100);
					break;
				case SortMethod.Group:	
					unit.LVFormat = ConstData.GetGroupTextID(ConstData.Tables.Card[data.CardID].n_GROUP);
					break;
				case SortMethod.HP:	
					unit.LV = data.Property.HP;
					break;
				case SortMethod.Atk:	
					unit.LV = data.Property.Atk;
					break;
				case SortMethod.Rev:	
					unit.LV = data.Property.Rev;
					break;
				case SortMethod.Chg:	
					unit.TextFormat = UIDefine.FORMAT_FLOAT;
					unit.LV = data.Property.Charge;
					break;
				case SortMethod.Luck:
					unit.TextFormat = "   0";
					unit.PrefixIconSpriteName = "image_clover";
					unit.LV = data.Origin.Luck;
					unit.ShowPrefixIcon = true;
					break;
				default:
					break;
				}
			}
			
		}
		else
		{
			var cardData = ConstData.Tables.Card[data.CardID];
			if(cardData != null) unit.Rare = cardData.n_RARE;
		}
		
		unit.ApplyChange();
	}
	#endregion
	
	protected override void OnStart ()
	{
		if(unit == null) 
			unit = GetComponent<Unit>();
		if(unit == null) 
			unit = UIManager.Instance.AddChild(gameObject, prefab).GetComponent<Unit>();
		
		if(EnableLongPress) gameObject.AddMissingComponent<LongPressDetector>();
	}
	
	private CardData cache;
	
	private void Cache(CardData data)
	{
		if(cache != data)
		{
			if(cache != null && cache.Origin != null) cache.Origin.OnChange -= OnCardChange;
			cache = data;
			if(cache != null && cache.Origin != null) cache.Origin.OnChange += OnCardChange;
		}
	}
	
	void OnCardChange (Mikan.CSAGA.Data.Card obj)
	{
		cache.Property = cache.Origin.Calc();
		cache.Property.Append(cache.Origin.SelectEquipment());
		
		SortUtils.ReSort(SortTarget.Card);
		
		ApplyChange(cache);
	}
	
	virtual protected void OnDestroy()
	{
		if(cache != null && cache.Origin != null) cache.Origin.OnChange -= OnCardChange;
	}
	
	private void OnClick()
	{	
		if(!isActive) return;
		
		Context.Instance.Card = data;
		
		Message.Send(MessageID.PlaySound, SoundID.CLICK);
		
		TriggerEvent();
	}
	
	virtual protected void TriggerEvent() {}
	
	protected void SetActive(bool active)
	{
		isActive = active;
		
		if(!ShowMask) return;
		
		if(active)
		{
			if(mask != null) Destroy(mask.gameObject);
			mask = null;
		}
		else if(mask == null)
		{
			mask = UIManager.Instance.AddChild(gameObject, Essentials.Instance.mask).GetComponent<Mask>();
			if(unit.lvContainer != null) mask.depthLimit = unit.lvContainer.GetComponent<UIWidget>().depth;
		}
	}
	
	protected void Select(int maxCount, bool replaceZero)
	{
		var selected = Selected;
		
		if(selected)
		{
			if(replaceZero)
			{
				var pos = Context.Instance.Cards.IndexOf(data.Origin.Serial);
				if(pos > -1) Context.Instance.Cards[pos] = Mikan.Serial.Empty;
			}
			else
				Context.Instance.Cards.Remove(data.Origin.Serial);
			
			if(tip != null) Destroy(tip.gameObject);
			tip = null;
			return;
		}
		
		if(replaceZero)
		{
			var replaced = false;
			for(int i = 0; i < Context.Instance.Cards.Count; ++i)
			{
				if(!Context.Instance.Cards[i].IsEmpty) continue;
				Context.Instance.Cards[i] = data.Origin.Serial;
				replaced = true;
				break;
			}
			
			if(!replaced) 
			{
				if(Context.Instance.Cards.Count >= maxCount) return;
				Context.Instance.Cards.Add(data.Origin.Serial);
			}
		}
		else
		{
			if(Context.Instance.Cards.Count >= maxCount) return;
			Context.Instance.Cards.Add(data.Origin.Serial);
		}
		
		SetSelected();
		
	}
	
	protected void UpdateInitSelected()
	{
		if(Selected) 
			SetSelected();
		else if(tip != null) 
		{
			Destroy(tip.gameObject);
			tip = null;
		}
		
	}
	
	private void SetSelected()
	{
		if(tip != null) return;
		
		ResourceManager.Instance.CreatePrefabInstance(gameObject, "GameScene/Card/ChosenTip", (obj)=>{
			
			
			if(!Context.Instance.Cards.Contains(data.Origin.Serial))
			{
				Destroy(obj);
				if(tip != null) Destroy(tip.gameObject);
				tip = null;
			}
			else
			{
				tip = obj.GetComponent<CardChosenTip>();
				tip.Serial = data.Origin.Serial;
				tip.transform.localPosition = new Vector3(34f,34f);
			}
		});	
	}
	
	void OnLongPress()
	{
		if(!EnableLongPress) return;
		if(data == null || data.CardID <= 0) return;

		Message.Send(MessageID.PlaySound, SoundID.CLICK);
		CardInfo.Show(data);
	}
	
	void Awake()
	{
		var widget = GetComponent<UIWidget>();
		
		var parent = gameObject.GetComponentInParent<UIWidget>();
		
		if(parent != null) widget.depth = parent.width;
		
		var collider = GetComponent<BoxCollider>();
		
		if(collider == null)
		{
			collider = gameObject.AddComponent<BoxCollider>();
			
			collider.size = new Vector3(widget.width, widget.height);
		}
	}
}

public class CardListItemRenderer : CardItemRenderer
{
	protected override void TriggerEvent ()
	{
		Message.Send(MessageID.SwitchStage, StageID.CardInfo);
		//data.IsLocked = !data.IsLocked;
		//unit.IsLocked = data.IsLocked;
		//unit.ApplyChange();
	}
}

public class CardEnhanceItemRenderer : CardItemRenderer
{
	protected override void TriggerEvent ()
	{
		MessageBox.ShowConfirmWindow("GameScene/Card/CardEnhance", "", null);
	}
}

public class CardTeamMemberSelectItemRenderer : CardItemRenderer
{
	protected override void ApplyChange (CardData data)
	{
		base.ApplyChange (data);
		
		if(data.Origin == null)
			SetActive(true);
		else
		{
			var team = GameSystem.Service.TeamInfo[Context.Instance.TeamIndex];
			
			SetActive(!team.Contains(data.Origin));
		}
	}
	
	protected override void TriggerEvent ()
	{
		if(data.Origin == null && data.CardID == UIDefine.FellowCardID)
			Message.Send(MessageID.SelectSupport, Context.Instance.TeamMemberIndex);
		else
			Message.Send(MessageID.SelectTeamMember);
	}
	
}

public class CardTeamMemberBatchSelectItemRenderer : CardItemRenderer
{
	protected override void ApplyChange (CardData data)
	{
		base.ApplyChange (data);
		
		UpdateInitSelected();
	}
	
	protected override void TriggerEvent ()
	{
		Select(5, true);
		Message.Send(MessageID.SelectItem);
	}
	
	protected override void OnStart ()
	{
		base.OnStart ();
		Message.AddListener(MessageID.SelectedChange, OnSelectedChange);
	}
	
	protected override void OnDestroy ()
	{
		base.OnDestroy ();
		Message.RemoveListener(MessageID.SelectedChange, OnSelectedChange);
	}
	
	private void OnSelectedChange()
	{
		UpdateInitSelected();
	}
}

public class CardSellItemRenderer : CardItemRenderer
{
	protected override void ApplyChange (CardData data)
	{
		base.ApplyChange (data);
		
		var locked = data.IsLocked || GameSystem.Service.CardInfo.IsProtected(data.Origin.Serial) || Context.Instance.FavoriteCard.Value == data.Origin.Serial.Value;
		
		SetActive(!locked);
		
		UpdateInitSelected();
	}
	
	protected override void TriggerEvent ()
	{
		Select(10, false);
		Message.Send(MessageID.SelectItem);
	}
	
	protected override void OnStart ()
	{
		base.OnStart ();
		Message.AddListener(MessageID.CancelSelect, OnCancelSelect);
	}
	
	protected override void OnDestroy ()
	{
		base.OnDestroy ();
		Message.RemoveListener(MessageID.CancelSelect, OnCancelSelect);
	}
	
	private void OnCancelSelect()
	{
		if(Selected) TriggerEvent();
	}
}

public class CardContactSelectItemRenderer : CardItemRenderer
{	
	protected override void ApplyChange (CardData data)
	{
		base.ApplyChange (data);
		
		SetActive(ConstData.Tables.Card[data.CardID].n_GIFT != 0);
	}
	
	protected override void TriggerEvent ()
	{
		Message.Send(MessageID.SelectItem, data);
	}
	
}

public class CardAwakenSelectItemRenderer : CardItemRenderer
{	
	protected override void TriggerEvent ()
	{
		Message.Send(MessageID.SwitchStage, StageID.CardAwakenView);
	}
}

public class CardGalleryItemRenderer : CardItemRenderer
{	
	public override bool EnableLongPress { get { return false; } }
	
	protected override bool ShowMask { get { return false; } }
	
	protected override void ApplyChange (CardData data)
	{
		base.ApplyChange (data);
		
		if(GameSystem.Service.GalleryInfo.List.Contains(data.CardID))
			SetActive(true);
		else
		{
			SetActive(false);
			unit.CardID = UIDefine.EmptyCardID;
			unit.Rare = 0;
			unit.ApplyChange();
		}
	}
	protected override void TriggerEvent ()
	{
		Message.Send(MessageID.SwitchStage, StageID.CardInfo);
	}
}

public class CardGachaResultItemRenderer : CardItemRenderer
{
	
	protected override bool UseDefaultFormat { get { return true; } }
	protected override void TriggerEvent ()
	{
		Message.Send(MessageID.SwitchStage, StageID.CardInfo);
	}
}


public class CardBatchSelectPreviewItemRenderer : ItemRenderer<CardData>
{
	private Unit unit;
	
	private Mikan.Serial cache = Mikan.Serial.Empty;
	
	#region implemented abstract members of ItemRenderer
	
	protected override void ApplyChange (CardData data)
	{
		unit.CardID = data.CardID;
		unit.ApplyChange();
		
		if(data.Origin != null) cache = Mikan.Serial.Empty;
	}
	
	#endregion
	
	protected override void OnStart ()
	{
		unit = GetComponentInChildren<Unit>();
		var cllider = gameObject.AddMissingComponent<BoxCollider>();
		var widget = GetComponent<UIWidget>();
		cllider.size = new Vector3(widget.width, widget.height);
		Message.AddListener(MessageID.Cancel, OnCancelSelect);
	}
	
	private void OnCancelSelect()
	{
		if(data.Origin == null) return;
		
		cache = data.Origin.Serial;
		
		Context.Instance.Cards[Index] = Mikan.Serial.Empty;
		
		Message.Send(MessageID.SelectedChange);
	}
	
	void OnDestroy()
	{
		Message.RemoveListener(MessageID.Cancel, OnCancelSelect);
	}
	
	void OnClick()
	{
		if(cache.IsEmpty && data.Origin == null) return;
		
		Context.Instance.Cards[Index] = cache;
		
		if(data.Origin != null) 
			cache = data.Origin.Serial;
		else
			cache = Mikan.Serial.Empty;
		
		Message.Send(MessageID.SelectedChange);
	}
}

public class PVPSelectCardItemRenderer : CardItemRenderer
{
	protected override void ApplyChange (CardData data)
	{
		base.ApplyChange (data);

		var contains = false;
		foreach(var item in Context.Instance.PVPWaves.List)
		{
			if(data.Origin != item.Card) continue;
			contains = true;
			break;

		}

		SetActive(!contains);

	}
	
	protected override void TriggerEvent ()
	{
		Context.Instance.PVPWave.Card = data.Origin;
		Context.Instance.PVPWaves.IsChanged = true;
		Message.Send(MessageID.Back);
	}
	
}