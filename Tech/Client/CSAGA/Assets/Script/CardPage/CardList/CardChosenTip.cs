﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CardChosenTip : UIBase {

	private static List<CardChosenTip> instances = new List<CardChosenTip>();

	public UILabel label;

	public Mikan.Serial Serial = Mikan.Serial.Empty;

	public object data;

	protected override void OnStart ()
	{
		base.OnStart ();
		instances.Add(this);
		ApplyChange();
	}
	protected override void OnDestroy ()
	{
		instances.Remove(this);

		foreach(var item in instances) item.ApplyChange();

		base.OnDestroy ();
	}
	protected override void OnChange ()
	{
		var no = 0;

		if(data != null)
			no = Context.Instance.Selections.IndexOf(data) + 1;
		else
			no = Context.Instance.Cards.IndexOf(Serial) + 1;

		if(label != null) label.text = no.ToString();
	}
}
