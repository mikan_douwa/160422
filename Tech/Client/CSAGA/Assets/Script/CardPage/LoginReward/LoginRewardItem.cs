﻿using UnityEngine;
using System;
using System.Collections;
using Mikan.CSAGA.Data;

public class LoginRewardItem : MonoBehaviour {

	public Icon icon;
	public GameObject arrow;
	public GameObject check;
	public UILabel dayText;

	public void Init(Mikan.CSAGA.ConstData.Trophy tData, bool showArrow, bool isGet)
	{
		if(tData != null)
		{
			if(tData.n_BONUS_LINK > 0)
			{
				//icon
				Mikan.CSAGA.ConstData.Drop data = ConstData.Tables.Drop[tData.n_BONUS_LINK];
				if(data != null)
				{
					icon.IconType = data.n_DROP_TYPE;
					icon.IconID = data.n_DROP_ID;
					icon.Count = data.n_COUNT;
					icon.ApplyChange();
				}
			}
			NGUITools.SetActive(arrow, showArrow);
			NGUITools.SetActive(check, isGet);
			dayText.text = ConstData.Tables.GetSystemText(169, tData.n_EFFECT[0]);
		}
	}
}
