﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using Mikan.CSAGA.Data;

public class LoginRewardMessageBox : MessageBox {

	public GameObject itemPrefab;
	public UIGrid grid;
	public UILabel title;
	public UILabel tip;

	protected override void OnReady ()
	{
		title.text = ConstData.GetSystemText(SystemTextID._167_LOGIN_BONUS);
		tip.text = ConstData.GetSystemText(SystemTextID._168_TIP_LOGIN_BONUS);

		Trophy trophy = GameSystem.Service.TrophyInfo.DailyCounter;
		
		foreach(Mikan.CSAGA.ConstData.Trophy data in ConstData.Tables.Trophy.Select(o=>{ return (int)o.n_IDSET == trophy.Group; }))
		{
			int day = data.n_EFFECT[0];
			
			GameObject go = NGUITools.AddChild(grid.gameObject, itemPrefab);
			go.name = day.ToString();
			LoginRewardItem item = go.GetComponent<LoginRewardItem>();
			item.Init(data, ((day % grid.maxPerLine) != 1), (trophy.Progress >= day));
		}
		grid.Reposition();
		
		base.OnReady ();
	}
	
}
