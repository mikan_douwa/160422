﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CardTitle : MonoBehaviour {

	public UILabel label;
	public Stars stars;
	public GameObject statusObj;

	public GameObject favoriteObj;

	public GameObject lckObj;

	public bool showNo = true;

	private CardData card;

	private int cardId;

	private bool originIsLocked;
	// Use this for initialization
	void Start () {

		ApplyChange();
	}

	void OnDestroy()
	{
		if(card.IsLocked != originIsLocked)
		{
			var locked = new List<Mikan.Serial>();
			var unlocked = new List<Mikan.Serial>();
			if(card.IsLocked) 
				locked.Add(card.Origin.Serial);
			else
				unlocked.Add(card.Origin.Serial);

			GameSystem.Service.CardLock(locked, unlocked);
		}
	}

	public void ApplyChange()
	{
		card = Context.Instance.Card;
		
		cardId = card.CardID;
		
		originIsLocked = card.IsLocked;
		
		var data = ConstData.Tables.Card[cardId];
		var text = ConstData.Tables.CardText[cardId];
		
		if(showNo)
			label.text = ConstData.GetSystemText(SystemTextID.NO_V1, data.n_GALLERY.ToString("D3")) + " " + text.s_NAME;
		else
			label.text = text.s_NAME;
		
		if(card.Origin != null)
			stars.viewCount = card.Origin.Rare;
		else
			stars.viewCount = data.n_RARE;
		
		stars.ApplyChange();
		
		if(card.Origin != null && !card.Origin.Serial.IsEmpty && !card.Origin.Serial.IsExternal)
		{
			statusObj.SetActive(true);
			
			if(!card.IsLocked)
			{
				foreach(var item in lckObj.GetComponentsInChildren<UISprite>()) item.color = Color.gray;
			}
			
			if(data.n_GIFT == 0)
				favoriteObj.SetActive(false);
			else
			{
				if(Context.Instance.FavoriteCard.Value != card.Origin.Serial.Value)
				{
					foreach(var item in favoriteObj.GetComponentsInChildren<UISprite>()) item.color = Color.gray;
				}
			}
		}
	}

	public void SetLock()
	{
		card.IsLocked = !card.IsLocked;

		if(card.IsLocked)
		{
			Message.Send(MessageID.PlaySound, SoundID.CLICK);
			foreach(var item in lckObj.GetComponentsInChildren<UISprite>()) item.color = Color.white;
		}
		else
		{
			Message.Send(MessageID.PlaySound, SoundID.CANCEL);
			foreach(var item in lckObj.GetComponentsInChildren<UISprite>()) item.color = Color.gray;
		}
	}

	public void SetFavorite()
	{
		MessageBox.Show(ConstData.GetSystemText(SystemTextID.TIP_FAVORITE_OFFER_V1, ConstData.Tables.DailyFavoriteKizuna/100)).title = ConstData.GetSystemText(SystemTextID.FAVORITE_SUCC);
		
		if(Context.Instance.FavoriteCard.Value == card.Origin.Serial.Value) return;
		
		Context.Instance.FavoriteCard.Value = card.Origin.Serial.Value;
		GameSystem.Service.SetFavorite(card.Origin.Serial);
		
		foreach(var item in favoriteObj.GetComponentsInChildren<UISprite>()) item.color = Color.white;
	}

}
