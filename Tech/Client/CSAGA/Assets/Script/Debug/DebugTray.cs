﻿using UnityEngine;
using System.Collections;

public class DebugTray : MonoBehaviour {

	public UIInput input;
	public UILabel tip;
	// Use this for initialization
	void Start () {
		tip.text = Resources.Load<TextAsset>("debug_code").text;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void OnSubmit()
	{
		var opCode = input.value;

		input.value = "";

		if(OfflineDebug(opCode)) return;

		GameSystem.Service.Debug(opCode);

	}

	public void OpenDebugUI()
	{
		var active = !input.gameObject.activeSelf;
		input.gameObject.SetActive(active);
		if(active) 
			input.isSelected = true;
		else
			input.value = "";
	}

	private bool OfflineDebug(string opCode)
	{
		if(opCode.StartsWith("play_event"))
		{
			Message.Send(MessageID.PlayEvent, int.Parse(opCode.Split(' ')[1]));
			return true;
		}
		if(opCode.StartsWith("pvp_skill"))
		{
			GameSystem.Service.PVPRerollSkill(0);
			return true;
		}
		if(opCode == "pvp")
		{
			Message.Send(MessageID.SwitchStage, StageID.PVP);
			return true;
		}
		return false;
	}
}
