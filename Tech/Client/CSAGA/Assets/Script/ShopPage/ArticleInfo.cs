﻿using UnityEngine;
using System.Collections;

public class ArticleInfo : SubmitWindow {

	public Icon icon;
	public Icon priceIcon;

	public UILabel pricePrefix;

	private RandomShopItemData data;

	protected override void OnReady ()
	{
		base.OnReady ();

		data = context.args[0] as RandomShopItemData;

		pricePrefix.text = ConstData.GetSystemText(SystemTextID.EXPENSE_V1, "");

		icon.IconType = data.Drop.n_DROP_TYPE;
		icon.IconID = data.Drop.n_DROP_ID;
		icon.Count = data.Drop.n_COUNT;
		icon.ApplyChange();
		
		priceIcon.IconType = data.Origin.n_COINTYPE;
		priceIcon.Count = data.Origin.n_COIN;
		priceIcon.IconID = data.Origin.n_COINID;
		if(data.Origin.n_TAG == Mikan.CSAGA.GachaListTag.Static)
		{
			if(data.Drop.n_DROP_TYPE == Mikan.CSAGA.DropType.Card && GameSystem.Service.CardInfo.GetLuck(data.Drop.n_DROP_ID) > 0)
				priceIcon.Count = data.Origin.n_COIN2;
		}
		priceIcon.ApplyChange();
	}

	#region implemented abstract members of SubmitWindow

	protected override bool OnSubmit ()
	{
		switch(data.Drop.n_DROP_TYPE)
		{
		case Mikan.CSAGA.DropType.Card:
			if(!ShopManager.Instance.CheckCardSpace(1)) return true;
			break;
		case Mikan.CSAGA.DropType.Item:
		{
			var itemData = ConstData.Tables.Item[data.Drop.n_DROP_ID];
			if(itemData.n_TYPE == Mikan.CSAGA.ItemType.Equipment && !QuestManager.Instance.CheckEquipmentSpace(1)) return true;
			break;
		}
		}

		ShopManager.Instance.ShopPurchase(data.Index, data.ID);

		return true;
	}

	#endregion



}
