﻿using UnityEngine;
using System.Collections;

public class IAPItemRenderer : ItemRenderer<IAPItemData> {
	
	public UILabel NameLabel;
	public UILabel BonusLabel;
	public UILabel PriceLabel;
	public UISprite bannerSprite;
	public SystemTextLabel bannerLabel;
	
	IAPShopPage shopPage;

	#region implemented abstract members of ItemRenderer
	
	protected override void ApplyChange (IAPItemData data)
	{
		NameLabel.text = data.Name;
		PriceLabel.text = data.Price;

		NGUITools.SetActive(bannerSprite.gameObject, (data.Bonus > 0));
		if(data.Bonus > 0)
		{
			BonusLabel.text = ConstData.GetSystemText(SystemTextID._226_IAP_BONUS_GEM) + data.Bonus.ToString();
#if MYCARD
			if(data.Index == 0)
			{
				bannerLabel.bbcode = "[00ffff]";
				bannerLabel.textId = SystemTextID._429_DAILY_IAP_BONUS;
			}
			else
			{
				bannerLabel.bbcode = "[ffff00]";
				bannerLabel.textId = SystemTextID._225_MONTHLY_IAP_BONUS;
			}
			bannerLabel.ApplyChange();
#endif

		}
		else
			BonusLabel.text = "";

		shopPage = data.Page;
	}
	
	#endregion
	
	void OnClick()
	{

		shopPage.PurchaseItem(data.ProductId);
	}
	
}
