﻿using UnityEngine;
using System.Collections;

public class RandomShopItemRenderer : ItemRenderer<RandomShopItemData> {

	public RewardIcon icon;
	public UILabel remainTime;
	public Icon priceIcon;
	public GameObject emptyObj;
	public UISprite background;

	public GameObject remainTimeObj;

	public GameObject donateObj;
	public UILabel donateRequire;

	private int price;

	#region implemented abstract members of ItemRenderer

	protected override void ApplyChange (RandomShopItemData data)
	{
		StopAllCoroutines();

		remainTimeObj.SetActive(false);
		remainTime.text = "";

		background.spriteName = data.IsRegular ? "button_01a" : "button_01";

		if(data.IsEmpty)
		{
			emptyObj.SetActive(true);
			donateObj.SetActive(false);

			icon.Hide();

			priceIcon.IconType = Mikan.CSAGA.DropType.None;
			priceIcon.manualIconName = "";
			priceIcon.ApplyChange();

			remainTime.text = "";

			price = 0;
		}
		else
		{
			emptyObj.SetActive(false);

			icon.ApplyChange(data.Drop);

			price = data.Origin.n_COIN;

			if(data.Origin.n_TAG == Mikan.CSAGA.GachaListTag.SocialShop)
			{
				remainTimeObj.SetActive(false);

				if(data.Origin.n_DEVOTE == 0)
					donateObj.SetActive(false);
				else
				{
					donateObj.SetActive(true);
					donateRequire.text = data.Origin.n_DEVOTE.ToString();
				}
			}
			else
			{
				if(data.Origin.n_TAG == Mikan.CSAGA.GachaListTag.Static)
				{
					donateObj.SetActive(false);

					if(data.Drop.n_DROP_TYPE == Mikan.CSAGA.DropType.Card && GameSystem.Service.CardInfo.GetLuck(data.Drop.n_DROP_ID) > 0)
						price = data.Origin.n_COIN2;
				}

				StartCoroutine(UpdateRemainTimeRoutine());
			}

			priceIcon.IconType = data.Origin.n_COINTYPE;
			priceIcon.IconID = data.Origin.n_COINID;
			priceIcon.Count = price;
			priceIcon.ApplyChange();
		}
	}

	#endregion

	void OnClick()
	{
		if(data.IsEmpty) return;

		if(ShopManager.Instance.CheckCurrency(data.Origin.n_COINTYPE, price, data.Origin.n_COINID))
		{
			if(data.Origin.n_TAG == Mikan.CSAGA.GachaListTag.SocialShop)
			{
				if(data.Origin.n_DEVOTE > GameSystem.Service.CooperateMissionInfo.TotalPoint)
				{
					MessageBox.Show(SystemTextID._420_TIP_DONATE_NOT_ENOUGH_V1, data.Origin.n_DEVOTE);
					return;
				}
			}

			var msgBox = MessageBox.ShowConfirmWindow("GameScene/Shop/ArticleInfo", "", null);
			msgBox.title = ConstData.GetSystemText(SystemTextID._180_TIP_RANDOM_SHOP_PURCHASE_CONFIRM);
			msgBox.args = new object[]{ data };
		}
	}

	IEnumerator UpdateRemainTimeRoutine()
	{
		remainTimeObj.SetActive(data != null && data.EndTime < System.DateTime.MaxValue);

		while(data != null && data.EndTime < System.DateTime.MaxValue)
		{
			remainTime.text = UIUtils.FormatRemainTime(data.EndTime, false);
			
			yield return new WaitForSeconds(1f);
		}

		remainTimeObj.SetActive(false);
		remainTime.text = "";
	}

}
