﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using Mikan.CSAGA.Data;
using System.Text;

public class IAPShopPage : Stage {
	
	public ItemList itemList;
	public GameObject blocker;

	List<string> unavailableList = new List<string>();
	Queue<string> consumeQueue = new Queue<string>();
	bool inConsume = false;
	bool inPurchase = false;
	//iOS Variables.
	string productIdentiers;
	bool iOSInited = false;
	GameObject blockWindow = null;
	//mycard variables.
	string MyCardProductID;

	protected override void Initialize ()
	{
		GameSystem.Service.OnIAPInfo += OnIAPInfo;
		GameSystem.Service.OnIAPClaim += PurchaseServerCallback;
#if MYCARD
		GameSystem.Service.OnMyCardBeginTransaction += MyCardGetAuthCodeHandler;
		MyCardAdapter.Instance.OnPurchase += PurchasePluginCallback;
		MyCardAdapter.Instance.OnCancel += MyCardPayCancel;

		GameSystem.Service.GetIAPInfo();

#else

		PluginAdapter.Instance.OnPurchase += PurchasePluginCallback;
		PluginAdapter.Instance.OnQuery += QueryCallback;
		PluginAdapter.Instance.OnConsume += ConsumeCallback;
		

		//Init flags.
		unavailableList.Clear();
		consumeQueue.Clear();
		inConsume = false;

#if UNITY_EDITOR
		GameSystem.Service.GetIAPInfo();
#elif UNITY_ANDROID
		if(PluginAdapter.Inited)
		{
			PluginAdapter.Instance.QueryPurchase("");
		}
		else
			base.Initialize();
#else
		//iOS
		if(PluginAdapter.Inited)
		{
			GameSystem.Service.GetIAPInfo();
		}
		else
			base.Initialize();
#endif
#endif
	}

	void OnIAPInfo ()
	{
#if UNITY_IOS && !UNITY_EDITOR
		if(iOSInited)
			RefreshList();
		else
			RetrieveProductID();
#else
		RefreshList();
#endif
	}
	
	protected override void OnDestroy ()
	{
		if(GameSystem.IsAvailable) 
		{
			GameSystem.Service.OnIAPInfo -= OnIAPInfo;
			GameSystem.Service.OnIAPClaim -= PurchaseServerCallback;
#if MYCARD
			GameSystem.Service.OnMyCardBeginTransaction -= MyCardGetAuthCodeHandler;
#endif
		}

#if MYCARD
		MyCardAdapter.Instance.OnPurchase -= PurchasePluginCallback;
		MyCardAdapter.Instance.OnCancel -= MyCardPayCancel;
#else
		PluginAdapter.Instance.OnPurchase -= PurchasePluginCallback;
		PluginAdapter.Instance.OnQuery -= QueryCallback;
		PluginAdapter.Instance.OnConsume -= ConsumeCallback;
#endif

		base.OnDestroy ();
	}
	
	private void RefreshList()
	{
#if MYCARD
		//Resend unfinished receipt.
		RecheckMyCardReceipt();
#endif

		var list = new List<IAPItemData>();
		List<IAPProduct> lstIAP = GameSystem.Service.IAPInfo.List;
		
		for(int i = 0; i < lstIAP.Count; i++)
		{
			IAPProduct product = lstIAP[i];
			var obj = new IAPItemData{ Index = i, ProductId = product.ProductID, Name = product.ProductName, Price = product.Price, Bonus = product.Bonus, Page = this};
			list.Add(obj);
		}
		itemList.Setup(list);

		if(!IsInitialized) base.Initialize ();
	}

	private void RecheckMyCardReceipt()
	{
		if(PlayerPrefs.GetString("MyCardProductID", "") != "")
		{
			GameSystem.Service.IAPClaim(PlayerPrefs.GetString("MyCardProductID", ""), "MYCARD", PlayerPrefs.GetString("MyCardToken", ""));
		}
	}

	private void RetrieveProductID()
	{
		productIdentiers = "";
		List<IAPProduct> lstIAP = GameSystem.Service.IAPInfo.List;

		for(int i = 0; i < lstIAP.Count; i++)
		{
			IAPProduct product = lstIAP[i];
			if(productIdentiers.Length > 0)
				productIdentiers += ";";
			productIdentiers += product.ProductID;
		}

		PluginAdapter.Instance.QueryPurchase(productIdentiers);
	}

	void ConsumeUnfinished()
	{
		//Queue to consume product.
		if(consumeQueue.Count > 0 && !inConsume)
		{
			inConsume = true;
			string sku = consumeQueue.Dequeue();
			PluginAdapter.Instance.ConsumePurchase(sku);
		}

	}

	public void PurchaseItem(string productId)
	{
		if(!inPurchase)
		{
#if MYCARD
			if(PlayerPrefs.GetString("MyCardProductID", "") != "")
				RecheckMyCardReceipt();
			else
				GameSystem.Service.MyCardBeginTransaction(productId);
#else
			if(unavailableList.Contains(productId))
			{
				string orderId = PlayerPrefs.GetString(productId);
				if(orderId != "")
				{
					GameSystem.Service.IAPClaim(productId, orderId, PlayerPrefs.GetString(productId + "_token"));//Convert.ToBase64String(Encoding.UTF8.GetBytes(PlayerPrefs.GetString(productId + "_token"))));					
				}
			}
			else
			{
				inPurchase = true;
				ResourceManager.Instance.CreatePrefabInstance("Common/Connecting", BlockWindowCreated);
				PluginAdapter.Instance.PurchaseItem(productId);
			}
#endif
		}
	}

	void MyCardGetAuthCodeHandler(string authCode, string productId, bool isTest)
	{
#if MYCARD
		inPurchase = true;
		ResourceManager.Instance.CreatePrefabInstance("Common/Connecting", BlockWindowCreated);

		MyCardProductID = productId;

		MyCardAdapter.Instance.StartPay(isTest, authCode);
#endif
	}

	void MyCardPayCancel()
	{
		inPurchase = false;
		if(blockWindow != null)
			Destroy(blockWindow);

		MyCardProductID = "";
	}


	public void PurchasePluginCallback(IAPReceipt receipt)
	{
		//Purchase completed, tell server.
		inPurchase = false;
		if(blockWindow != null)
			Destroy(blockWindow);

		if(receipt.ResultCode == "0")
		{
			if(!unavailableList.Contains(receipt.SKU))
				unavailableList.Add(receipt.SKU);
#if MYCARD
			//Save Receipt
			PlayerPrefs.SetString("MyCardProductID", MyCardProductID);
			PlayerPrefs.SetString("MyCardToken", receipt.token);

			GameSystem.Service.IAPClaim(MyCardProductID, receipt.OrderId, receipt.token);
#else
			GameSystem.Service.IAPClaim(receipt.SKU, receipt.OrderId, receipt.token);//Convert.ToBase64String(Encoding.UTF8.GetBytes(receipt.token)));
#endif
		}
	}

	public void QueryCallback(List<IAPQueryResult> lst)
	{
		for(int i = 0; i < lst.Count; i++)
		{
			IAPQueryResult result = lst[i];
#if UNITY_ANDROID
			if(result.HasPurchased)
			{
				if(!unavailableList.Contains(result.SKU))
					unavailableList.Add(result.SKU);

				string orderId = PlayerPrefs.GetString(result.SKU);
				if(orderId != "")
				{
					GameSystem.Service.IAPClaim(result.SKU, orderId, PlayerPrefs.GetString(result.SKU + "_token"));
				}
				else
				{
					consumeQueue.Enqueue(result.SKU);
				}
			}
#else
			//iOS
			string orderId = PlayerPrefs.GetString(result.SKU);
			if(orderId != "")
			{
				unavailableList.Add(result.SKU);
				GameSystem.Service.IAPClaim(result.SKU, orderId, PlayerPrefs.GetString(result.SKU + "_token"));//Convert.ToBase64String(Encoding.UTF8.GetBytes(PlayerPrefs.GetString(result.SKU + "_token"))));
			}

#endif
		}

#if UNITY_EDITOR
		GameSystem.Service.GetIAPInfo();
#elif UNITY_ANDROID
		ConsumeUnfinished();
		GameSystem.Service.GetIAPInfo();
#elif UNITY_IOS
		iOSInited = true;
		RefreshList();
#else
		GameSystem.Service.GetIAPInfo();
#endif
	}

	public void ConsumeCallback(IAPConsumeResult consume)
	{
		if(unavailableList.Contains(consume.SKU))
			unavailableList.Remove(consume.SKU);

		inConsume = false;
		ConsumeUnfinished();
	}

	void PurchaseServerCallback (string orderId, string productId, int gem)
	{
		if(gem > 0)
			QuestDrop.Show(new Mikan.CSAGA.Reward{ Type = Mikan.CSAGA.DropType.Gem, Value1 = gem });

		//Clear local saved receipt.
#if MYCARD
		PlayerPrefs.SetString("MyCardProductID", "");
		PlayerPrefs.SetString("MyCardToken", "");
#else
		PlayerPrefs.SetString(productId, "");
		PlayerPrefs.SetString(productId + "_token", "");
#endif

		//Add Gem done, requery and consume product.
#if MYCARD
		GameSystem.Service.GetIAPInfo();
#elif UNITY_IOS
		if(unavailableList.Contains(productId))
			unavailableList.Remove(productId);

		GameSystem.Service.GetIAPInfo();
#else
		//Google Play.
		PluginAdapter.Instance.QueryPurchase("");	
#endif
	}

	void BlockWindowCreated(GameObject go)
	{
		blockWindow = go;
	}

}

public class IAPItemData
{
	public int Index;
	public string ProductId;
	public string Name;
	public string Price;
	public int Bonus;
	public IAPShopPage Page;
}