﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ArticleList : ItemList 
{
	public Mikan.CSAGA.GachaListTag tag;

	private ListItemDataProvider<RandomShopItemData> provider;

	public void Rebuild()
	{
		var list = new List<RandomShopItemData>();

		switch(tag)
		{
		case Mikan.CSAGA.GachaListTag.Random:
		{
			foreach(var item in ConstData.Tables.GachaList.Select(o=>{ return o.n_TAG == Mikan.CSAGA.GachaListTag.Static; }))
			{
				var obj = new RandomShopItemData{ Index = -1, ID = item.n_ID, IsRegular = true };
				if(item.n_TIME > 0)
				{
					var year = 2000 + item.n_TIME / 10000;
					var month = (item.n_TIME % 10000) / 100; 
					var day = item.n_TIME % 100;
					
					obj.EndTime = new System.DateTime(year, month, day).AddDays(1);
					
					if(GameSystem.Service.CurrentTime >= obj.EndTime) continue;
				}
				
				obj.Origin = item;
				obj.Drop = ConstData.Tables.Drop[item.n_GOODS];
				list.Add(obj);
			}
			
			var count = ConstData.Tables.RandomShopSlotCalc.Count(GameSystem.Service.PurchaseInfo.Count(Mikan.CSAGA.ArticleID.RandomShopSlot));
			
			for(int i = 0; i < count; ++i)
			{
				var id = GameSystem.Service.RandomShopInfo.List[i];
				var data = ConstData.Tables.GachaList[id];
				var drop = ConstData.Tables.Drop[data.n_GOODS];
				var obj = new RandomShopItemData{ Index = i, ID = id, Origin = data, Drop = drop, IsEmpty = GameSystem.Service.RandomShopInfo.State[i] };
				list.Add(obj);
			}

			break;
		}
		case Mikan.CSAGA.GachaListTag.SocialShop:
		case Mikan.CSAGA.GachaListTag.CommonShop:
		{
			foreach(var item in ConstData.Tables.GachaList.Select(o=>{ return o.n_TAG == tag; }))
			{
				var obj = new RandomShopItemData{ ID = item.n_ID };
				if(item.n_TIME > 0)
				{
					var year = 2000 + item.n_TIME / 10000;
					var month = (item.n_TIME % 10000) / 100; 
					var day = item.n_TIME % 100;
					
					obj.EndTime = new System.DateTime(year, month, day).AddDays(1);
					
					if(GameSystem.Service.CurrentTime >= obj.EndTime) continue;
				}
				
				obj.Origin = item;
				obj.Drop = ConstData.Tables.Drop[item.n_GOODS];

				if(tag == Mikan.CSAGA.GachaListTag.SocialShop)
				{
					if(Context.Instance.SocialPurchaseList.Contains(item.n_ID))
						obj.IsEmpty = true;
				}

				list.Add(obj);
			}

			break;
		}
		default:
			return;
		}

		if(provider == null)
		{
			provider = ListItemDataProvider<RandomShopItemData>.Create(list);
			Setup (provider);
		}
		else
		{
			if(provider.Count != list.Count)
				provider.Setup(list, true);
			else
			{
				provider.Setup(list, false);
				Refresh();
			}
		}
	}

}
