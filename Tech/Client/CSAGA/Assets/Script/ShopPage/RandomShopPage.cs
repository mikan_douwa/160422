﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RandomShopPage : Stage {

	public UILabel updateTime;

	public Item item;

	public ArticleList itemList;

	public UIWidget expandButton;

	private bool isDisabled;

	protected override void Initialize ()
	{
		GameSystem.Service.OnRandomShopInfo += OnRandomShopInfo;
		GameSystem.Service.OnPurchase += OnPurchase;
		GameSystem.Service.OnRandomShopPurchase += OnRandomShopPurchase;

		GameSystem.Service.GetRandomShopInfo();
	}

	protected override void OnGetBack ()
	{
		StopAllCoroutines();
		StartCoroutine(RefreshUpdateTime());
	}

	void OnRandomShopPurchase (int dropId)
	{
		QuestDrop.Show(dropId);

		RefreshList();
	}

	void OnPurchase (Mikan.CSAGA.ArticleID id)
	{
		if(id == Mikan.CSAGA.ArticleID.RandomShopSlot) RefreshList();
	}

	void OnRandomShopInfo ()
	{

		RefreshList();

		StopAllCoroutines();
		StartCoroutine(RefreshUpdateTime());

		if(!IsInitialized) base.Initialize ();
	}
	
	protected override void OnDestroy ()
	{
		if(GameSystem.IsAvailable) 
		{
			GameSystem.Service.OnRandomShopInfo -= OnRandomShopInfo;
			GameSystem.Service.OnPurchase -= OnPurchase;
			GameSystem.Service.OnRandomShopPurchase -= OnRandomShopPurchase;
		}
		base.OnDestroy ();
	}

	private void RefreshList()
	{
		itemList.Rebuild();

		if(ConstData.Tables.RandomShopSlotCalc.Count(GameSystem.Service.PurchaseInfo.Count(Mikan.CSAGA.ArticleID.RandomShopSlot)) < ConstData.Tables.RandomShopSlotCalc.Limit)
			expandButton.color = Color.white;
		else
			expandButton.color = Color.gray;

		this.item.ItemID = ConstData.Tables.RandomShopRefreshItemID;
		this.item.ApplyChange();
	}
	
	IEnumerator RefreshUpdateTime()
	{
		var endTime = GameSystem.Service.RandomShopInfo.UpdateTime.AddMinutes(ConstData.Tables.RandomShopRefreshTime);

		while(GameSystem.Service.CurrentTime < endTime)
		{
			var cd = endTime.Subtract(GameSystem.Service.CurrentTime);
			
			if(cd.Hours == 0)
				updateTime.text = string.Format("{0:00}:{1:00}", (int)cd.Minutes, (int)cd.Seconds);
			else
				updateTime.text = string.Format("{0:00}:{1:00}:{2:00}", (int)cd.Hours, (int)cd.Minutes, (int)cd.Seconds);
			
			yield return new WaitForSeconds(0.5f);
		}

		GameSystem.Service.GetRandomShopInfo();
	}
}

public class RandomShopItemData
{
	public int Index;
	public int ID;
	public bool IsEmpty;
	public bool IsRegular;

	public Mikan.CSAGA.ConstData.GachaList Origin;
	public Mikan.CSAGA.ConstData.Drop Drop;

	public System.DateTime EndTime = System.DateTime.MaxValue;
}