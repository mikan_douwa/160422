﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CommonShop : Stage {

	public ArticleList itemList;

	protected override void Initialize ()
	{
		GameSystem.Service.OnRandomShopPurchase += OnRandomShopPurchase;
		
		itemList.Rebuild();

		InitializeComplete();
	}
	
	protected override void OnDestroy ()
	{
		if(GameSystem.IsAvailable) 
		{
			GameSystem.Service.OnRandomShopPurchase -= OnRandomShopPurchase;
		}
		base.OnDestroy ();
	}
	
	void OnRandomShopPurchase (int dropId)
	{
		QuestDrop.Show(dropId);
	}
}
