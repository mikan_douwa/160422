﻿using UnityEngine;
using System.Collections;
using Mikan;
using System.Collections.Generic;

public class SetupPage : Stage {
	
	public PresetNavigator navigator;
	
	private SetupData root;
	
	protected override void Initialize ()
	{
		GenerateData();
		
		Context.Instance.SetupData = root;

		navigator.OnCurrentChange += OnCurrentChange;
		navigator.Next();
		
		base.Initialize ();
	}

	protected override void OnDestroy ()
	{
		navigator.OnCurrentChange -= OnCurrentChange;
		base.OnDestroy ();
	}

	void OnCurrentChange (PresetNavigator obj)
	{
		if(Context.Instance.SetupData == root)
			titleFormat = TitleFormat.Static;
		else
		{
			titleFormat = TitleFormat.Dynamic;
			titleText = Context.Instance.SetupData.Text.Replace("\n", " ");
		}
		UpdateHeadline();
	}

	protected override void OnNext ()
	{
		var data = Context.Instance.SetupData;
		
		switch(data.Action)
		{
		case SetupData.ACTION.MENU:
		{
			if(data.Function == Mikan.CSAGA.SetupFunction.Memory && data.List.Count == 0)
				LoadMemoryList(Context.Instance.SetupData);
			
			navigator.Next();
			return;
		}
		case SetupData.ACTION.MESSAGE:
		{
			Context.Instance.TutorialID = data.Origin.n_ID;
			MessageBox.Show("GameScene/Setup/TutorialMessageBox", "");
			break;
		}
		case SetupData.ACTION.FUNCTION:
		{
			switch(data.Function)
			{
			case Mikan.CSAGA.SetupFunction.HomePage:
				Application.LoadLevel(0);
				break;
			case Mikan.CSAGA.SetupFunction.Gallery:
				Message.Send(MessageID.SwitchStage, StageID.Gallery);
				return;
			case Mikan.CSAGA.SetupFunction.Item:
				Message.Send(MessageID.SwitchStage, StageID.Item);
				return;
			case Mikan.CSAGA.SetupFunction.Equipment:
				Message.Send(MessageID.SwitchStage, StageID.Equipment);
				return;
			case Mikan.CSAGA.SetupFunction.System:
			{
				var msgBox = MessageBox.Show("Setting/SystemSetting", "");
				msgBox.enableCloseByClick = true;
				break;
			}
			case Mikan.CSAGA.SetupFunction.Memory:
				Message.Send(MessageID.PlayEvent, data.Interactive.n_ID);
				break;
			case Mikan.CSAGA.SetupFunction.Name:
			{
				var msgBox = MessageBox.ShowConfirmWindow("GameScene/Setup/EditNameMessageBox", GameSystem.Service.PlayerInfo.Name, null);
				msgBox.enableCloseByClick = true;
				msgBox.title = data.Text;
				break;
			}
			case Mikan.CSAGA.SetupFunction.Link:
				Application.OpenURL(data.Origin.s_STRING);
				break;
				#if UNITY_EDITOR || UNITY_STANDALONE
			case Mikan.CSAGA.SetupFunction.Reset:
				PlayerPrefs.DeleteAll();
				Application.LoadLevel(0);
				break;
				#endif
			case Mikan.CSAGA.SetupFunction.Repair:
				MessageBox.ShowConfirmWindow(ConstData.GetSystemText(SystemTextID._240_TIP_REPAIR_DATA), (index)=>
				                             {
					if(index == 0)
					{
						PatchManager.Instance.DeleteAllFiles();
						Application.LoadLevel(0);
					}
				});
				break;
			case Mikan.CSAGA.SetupFunction.Publish:
			{
				var msgBox = MessageBox.Show("GameScene/Setup/PublishConfirmMessageBox", "", SystemTextID._256_BTN_ACCOUNT_PUBLISH, SystemTextID._257_TRANSFER_HINT, SystemTextID.BTN_CANCEL);
				msgBox.title = data.Text;
				msgBox.colors = new ButtonColor[]{ ButtonColor.Green, ButtonColor.None, ButtonColor.Red };
				break;
			}
			default:
				MessageBox.Show(SystemTextID.TIP_NOT_AVAILABLE);
				break;
			}
			break;
		}
			
		}

		// restore
		Context.Instance.SetupData = data.Owner;
	}
	
	protected override void OnBack ()
	{
		var data = Context.Instance.SetupData;
		
		if(data == root)
			base.OnBack ();
		else
		{
			Context.Instance.SetupData = data.Owner;
			navigator.Prev ();
		}
	}
	
	private void GenerateData()
	{
		root = new SetupData();
		root.Layer = 0;
		root.Action = SetupData.ACTION.MENU;
		
		foreach(var item in ConstData.Tables.Setup.Select())
		{
			if(item.n_FUNCTION == Mikan.CSAGA.SetupFunction.Banner) continue;
			Add(root, item);
		}
	}

	private void LoadMemoryList(SetupData parent)
	{
		foreach(var chapterList in QuestManager.Instance.ChaperList)
		{
			foreach(var chapter in chapterList.Value)
			{
				var data = default(SetupData);
				
				foreach(var item in chapter.MemoryList)
				{
					if(!GameSystem.Service.QuestInfo[item.n_QUEST].IsCleared) continue;
					if(data == null) 
					{
						data = AddMemory(parent, item);
						data.Action = SetupData.ACTION.MENU;
					}
					AddMemory(data, item);
				}
			}
		}
		
		foreach(var list in QuestManager.Instance.MemoryList)
		{
			var data = default(SetupData);
			
			foreach(var item in list.List)
			{
				if(!GameSystem.Service.QuestInfo[item.n_QUEST].IsCleared) continue;
				if(data == null) 
				{
					data = AddMemory(parent, item);
					data.Action = SetupData.ACTION.MENU;
				}
				AddMemory(data, item);
			}
		}

		Sort(parent);
	}
	
	private void Add(SetupData parent, Mikan.CSAGA.ConstData.Setup origin)
	{
		if(parent.List == null) parent.List = new List<SetupData>();
		
		var layer = parent.Layer + 1;
		
		var group = 0;
		
		switch(layer)
		{
		case 1: group = origin.n_MAIN; 	break;
		case 2: group = origin.n_SUB; 	break;		
		case 3: group = origin.n_EVENT;	break;
		}
		
		if(group != 0) 
		{
			var isExists = false;
			
			foreach(var item in parent.List)
			{
				if(item.Group != group) continue; 
				
				isExists = true;
				
				Add (item, origin);
				
				break;
			}
			
			if(!isExists)
			{
				var data = new SetupData();
				data.Origin = origin;
				data.Owner = parent;
				data.Layer = layer;
				data.Group = group;
				data.Action = SetupData.ACTION.MENU;
				
				parent.List.Add(data);
				Add (parent, origin);
			}
			
		}
		else
		{
			switch(parent.Function)
			{
			case Mikan.CSAGA.SetupFunction.Tutorial:
				parent.Action = SetupData.ACTION.MESSAGE;
				break;
			case Mikan.CSAGA.SetupFunction.Memory:
				parent.Action = SetupData.ACTION.MENU;
				//LoadMemoryList(parent);
				break;
			default:
				parent.Action = SetupData.ACTION.FUNCTION;
				break;
			}
		}
	}
	
	private SetupData AddMemory(SetupData parent, Mikan.CSAGA.ConstData.Interactive interactive)
	{
		var data = new SetupData();
		data.Owner = parent;
		data.Layer = parent.Layer + 1;
		data.Action = SetupData.ACTION.FUNCTION;
		data.Interactive = interactive;
		if(parent.List == null) parent.List = new List<SetupData>();
		parent.List.Add(data);
		return data;
	}
	
	private void Sort(SetupData data)
	{
		if(data.List == null) return;
		
		data.List.Sort((lhs,rhs)=>lhs.Interactive.n_ID.CompareTo(rhs.Interactive.n_ID));
		
		foreach(var item in data.List) Sort (item);
	}
}

public class SetupData
{
	public enum ACTION
	{
		MENU,
		FUNCTION,
		MESSAGE,
		SPACE,
	}
	
	public Mikan.CSAGA.ConstData.Setup Origin;
	
	public Mikan.CSAGA.ConstData.Interactive Interactive;
	
	public Mikan.CSAGA.SetupFunction Function 
	{ 
		get 
		{ 
			if(Origin != null) return Origin.n_FUNCTION; 
			if(Interactive != null) return Mikan.CSAGA.SetupFunction.Memory;
			return Mikan.CSAGA.SetupFunction.None;
		} 
	}
	
	public string Text
	{
		get
		{
			if(Origin != null)
			{
				var text = ConstData.Tables.SetupText[Origin.n_ID];
				
				switch(Layer)
				{
				case 1: return text.s_MAIN_TEXT;
				case 2: return text.s_SUB_TEXT;
				case 3: return text.s_EVENT_TEXT;
				case 4: return text.s_FUNCTION_TEXT;
				}
			}
			else if(Interactive != null)
			{
				switch(Layer)
				{
				case 2: return ConstData.GetChaperName(Interactive.n_RECALL, ConstData.Tables.QuestDesign[Interactive.n_QUEST].n_QUEST_TYPE);
				case 3: return ConstData.GetEventName(Interactive.n_CALL_EVENT);
				}
			}
			
			return "#illegal";
		}
	}
	
	public int Layer;
	
	public int Group;
	
	public ACTION Action;
	
	public SetupData Owner;
	
	public List<SetupData> List;
}
