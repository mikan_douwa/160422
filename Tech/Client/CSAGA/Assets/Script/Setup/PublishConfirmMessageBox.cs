﻿using UnityEngine;
using System.Collections;

public class PublishConfirmMessageBox : SubmitWindow {

	private bool isSucc = false;

	protected override void OnReady ()
	{
		GameSystem.Service.OnAccountPublish += OnAccountPublish;
	}

	void OnAccountPublish (string token)
	{
		isSucc = true;

		var msgBox = MessageBox.Show(ConstData.GetSystemText(SystemTextID._245_TIP_PUBLISH_SUCC_V1_V2_V3, GameSystem.Service.Profile.PublicID.Insert(6, ",").Insert(3, ","), token));
		msgBox.pivot = UIWidget.Pivot.Left;
		msgBox.height = 500;
		msgBox.width = 600;


		msgBox.callback = (index)=>{ Message.Send(MessageID.Back); };
	}
	
	protected override bool IsSucc { get{ return isSucc; } }

	protected override void OnClickButton (int index)
	{
		if(index == 0)
			base.OnClickButton(index);
		else if(index == 1)
			Application.OpenURL(Context.Instance.SetupData.Origin.s_STRING);
		else
		{
			isSucc = true;
			base.OnClickButton (index);
		}

	}

	protected override void OnDestroy ()
	{
		if(GameSystem.IsAvailable) GameSystem.Service.OnAccountPublish -= OnAccountPublish;
		base.OnDestroy ();
	}


	#region implemented abstract members of SubmitWindow

	protected override bool OnSubmit ()
	{
		GameSystem.Service.AccountPublish();

		return true;
	}

	#endregion



}
