﻿using UnityEngine;
using System.Collections;

public class SetupItemRenderer : ItemRenderer<SetupData> {
	
	public UILabel label;
	
	#region implemented abstract members of ItemRenderer
	
	protected override void ApplyChange (SetupData data)
	{
		if(data.Action == SetupData.ACTION.SPACE)
			gameObject.SetActive(false);
		else
		{
			gameObject.SetActive(true);
			label.text = data.Text;
		}
	}
	
	#endregion
	
	
	void OnClick()
	{
		Context.Instance.SetupData = data;
		Message.Send(MessageID.Next);
	}
	
}