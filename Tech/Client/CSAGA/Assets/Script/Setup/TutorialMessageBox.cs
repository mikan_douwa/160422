﻿using UnityEngine;
using System.Collections;

public class TutorialMessageBox : MessageBox {

	public UILabel title;

	public UILabel content;

	protected override void OnReady ()
	{
		var data = ConstData.Tables.Setup[Context.Instance.TutorialID];
		
		var textData = ConstData.Tables.SetupText[Context.Instance.TutorialID];
		
		if(data.n_EVENT != 0)
			title.text = textData.s_EVENT_TEXT;
		else if(data.n_SUB != 0)
			title.text = textData.s_SUB_TEXT;
		else if(data.n_MAIN != 0)
			title.text = textData.s_MAIN_TEXT;
		else 
			title.text = "";
		
		content.text = textData.s_FUNCTION_TEXT;
	}
}
