﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SetupList : MonoBehaviour {

	public ItemList itemList;

	// Use this for initialization
	void Start () {

		var list = new List<SetupData>(Context.Instance.SetupData.List);

		//for(int i = 0; i < 3; ++i) list.Add(new SetupData{ Action = SetupData.ACTION.SPACE });

		itemList.Setup(list);
	}
}
