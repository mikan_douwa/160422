﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EquipSellMessageBox : SubmitWindow {

	public ItemList itemList;
	
	public UILabel crystal;
	
	protected override void OnReady ()
	{
		var _crystal = 0;

		var list = new List<EquipmentData>();

		foreach(var _obj in Context.Instance.Selections)
		{
			var obj = (EquipmentData)_obj;

			var item = GameSystem.Service.EquipmentInfo[obj.Origin.Serial];
			var data = ConstData.Tables.Item[item.ID];
			
			_crystal += data.n_EXP_GAIN;

			list.Add(obj);
		}

		crystal.text = string.Format("{0}+{1}", ConstData.GetSystemText(SystemTextID.CRYSTAL), _crystal);

		if(list.Count <= 5)
			itemList.transform.localPosition = new Vector3(itemList.transform.localPosition.x, itemList.transform.localPosition.y - 65);

		itemList.GetItemRendererFunc = GetItemRenderer;
		itemList.Setup(list);
	}

	private ItemRenderer GetItemRenderer(GameObject item) 
	{
		var renderer = item.GetComponent<EquipmentItemRenderer>();
		renderer.isStatic = true;
		renderer.enableLongPress = false;
		renderer.useDefaultFormat = true;
		return renderer;
	}

	#region implemented abstract members of SubmitWindow
	
	protected override bool OnSubmit ()
	{
		GameSystem.Service.EquipmentSell(new List<long>(Context.Instance.SelectEquipment()));
		return true;
	}
	
	#endregion

}
