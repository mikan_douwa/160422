﻿using UnityEngine;
using System.Collections;

public class EditNameMessageBox : SubmitWindow {
	
	public UIInput editName;

	protected override void OnReady ()
	{
		editName.isSelected = true;
	}

	public void OnInputChange()
	{
		editName.value = UIUtils.RestrictPlayerName(editName.value);
	}

	protected override void OnCloseByClick ()
	{
		Message.Send(MessageID.Back);
	}

	#region implemented abstract members of SubmitWindow
	protected override bool OnSubmit ()
	{
		if(string.IsNullOrEmpty(editName.value)) return false;

		if(editName.value != GameSystem.Service.PlayerInfo.Name)
		{
			GameSystem.Service.PlayerNameEdit(editName.value);
		}
		
		return true;
	}
	#endregion
}
