﻿using UnityEngine;
using System;
using System.Collections;
using Mikan;
using Mikan.Net;

public class HttpServiceProvider : Singleton<HttpServiceProvider>, IHttpServiceProvider
{
	private UnityCompatibleQueue<HttpRequest> queue = new UnityCompatibleQueue<HttpRequest>();

	#region IHttpServiceProvider implementation
	public event Action<HttpResponse> OnResponse;

	private bool isPaused = false;

	private bool isCancelled = false;
	
	public void SendRequest (HttpRequest req)
	{
		queue.Enqueue(req);
	}

	public void Rewind()
	{
		isPaused = false;
	}

	public void Cancel()
	{
		if(isPaused)
		{
			isPaused = false;
			isCancelled = true;
		}
	}
	
	#endregion

	void Start()
	{
		StartCoroutine(SendRequestRoutine());
	}

	IEnumerator SendRequestRoutine()
	{
		while(true)
		{
			if(queue.IsEmpty) 
			{
				yield return new WaitForSeconds(0.1f);
				continue;
			}
			
			var req = queue.Dequeue();

			var retryCount = 0;

			while(true)
			{
				var www = new WWW(PatchManager.Instance.ServerURL + req.URI, req.Data);

				var beginTime = Time.time;

				yield return new WaitForEndOfFrame();

				while(true)
				{
					if(www.isDone) break;

					if(Time.time - beginTime > 30) break;

					yield return new WaitForSeconds(0.1f);
				}

				
				var res = new HttpResponse{ Request = req };

				if(!www.isDone)
				{
					res.IsSucc = false;
					res.Error = "timeout";
					Debug.LogWarning(string.Format("[HTTP_FAILED] Error = {0}", res.Error)); 
				}
				else if(string.IsNullOrEmpty(www.error))
				{
					res.IsSucc = true;
					res.Data = new System.IO.MemoryStream(www.bytes);
				}
				else
				{
					res.IsSucc = false;
					res.Error = www.error;
					Debug.LogWarning(string.Format("[HTTP_FAILED] Error = {0}", res.Error));
				}

				www.Dispose();

				if(!res.IsSucc)
				{
					if(++retryCount < 3) 
						continue;
					else
					{
						isPaused = true;
						isCancelled = false;
						
						Message.Send(MessageID.NetFail);
						
						while(isPaused) yield return new WaitForSeconds(1f);
						
						if(!isCancelled)
						{
							--retryCount;
							continue;
						}
					}
				}

				if(OnResponse != null) OnResponse(res);	

				break;
			}
		}
	}
}
