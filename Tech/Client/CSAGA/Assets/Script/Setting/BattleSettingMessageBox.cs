﻿using UnityEngine;
using System.Collections;

public class BattleSettingMessageBox : MessageBox {

	public UIToggle enableSkillCheck;
	public UIToggle disableSkillCheck;
	public UIToggle enableAuto;
	public UIToggle disableAuto;

	protected override void OnReady ()
	{
		Message.Send(MessageID.BattleState, (object)0);

		Message.AddListener(MessageID.BattleSkillCheck, OnBattleSkillCheck);
		Message.AddListener(MessageID.BattleFX, OnBattleFX);
		Message.AddListener(MessageID.BattleSpeed, OnBattleSpeed);
		Message.AddListener(MessageID.BattleCancel, OnBattleCancel);

		enableSkillCheck.value = Context.Instance.BattleSetting.SkillCheck;
		disableSkillCheck.value = !enableSkillCheck.value;

		enableAuto.value = Context.Instance.BattleSetting.FXEnabled;
		disableAuto.value = !enableAuto.value;
	}

	protected override void OnDestroy ()
	{
		base.OnDestroy ();

		Message.RemoveListener(MessageID.BattleSkillCheck, OnBattleSkillCheck);
		Message.RemoveListener(MessageID.BattleFX, OnBattleFX);
		Message.RemoveListener(MessageID.BattleSpeed, OnBattleSpeed);
		Message.RemoveListener(MessageID.BattleCancel, OnBattleCancel);

		if(GameSystem.IsAvailable)
		{
			Context.Instance.BattleSetting.Save();

			Message.Send(MessageID.BattleState, (object)1);
		}
	}

	private void OnBattleCancel(IMessage msg)
	{
		MessageBox.ShowConfirmWindow(ConstData.GetSystemText(SystemTextID.TIP_CANCEL_QUEST_CONFIRM), OnBattleCancelConfirm);
	}

	private void OnBattleSkillCheck(IMessage msg)
	{
		Context.Instance.BattleSetting.SkillCheck = (int)msg.Data != 0;
	}

	private bool isInFunction = false;
	private void OnBattleFX(IMessage msg)
	{
		if(isInFunction) return;

		isInFunction = true;
		var enabled = (int)msg.Data != 0;
		Context.Instance.BattleSetting.FXEnabled = enabled;
		enableAuto.value = enabled;
		disableAuto.value = !enabled;

		isInFunction = false;
	}

	private void OnBattleSpeed(IMessage msg)
	{
		var newVal = Context.Instance.BattleSetting.Speed + 0.5f;
		if(newVal > 1.5f) newVal = 0.5f;

	}

	private void OnBattleCancelConfirm(int index)
	{
		if(index == 0) QuestManager.Instance.Controller.Cancel();
	}
}
