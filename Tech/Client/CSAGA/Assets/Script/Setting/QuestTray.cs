﻿using UnityEngine;
using System.Collections;

public class QuestTray : MonoBehaviour {
	
	public UISprite speedIcon;
	
	public UISprite stateIcon;

	public UISprite autoIcon;

	private int pauseCount = 0;

	private bool auto;
	
	void Start()
	{
		auto = Context.Instance.BattleSetting.Auto;

		Message.AddListener(MessageID.BattleState, OnBattleState);
		Message.AddListener(MessageID.BattleSpeed, OnBattleSpeed);
		Message.AddListener(MessageID.BattleAuto, OnBattleAuto);

		UpdateIcon();
		UpdateSpeedIcon();
		UpdateAutoIcon();
	}

	void OnDestroy()
	{
		Message.RemoveListener(MessageID.BattleState, OnBattleState);
		Message.RemoveListener(MessageID.BattleSpeed, OnBattleSpeed);
		Message.RemoveListener(MessageID.BattleAuto, OnBattleAuto);

		if(GameSystem.IsAvailable)
		{
			if(Context.Instance.BattleSetting.Auto != auto)
			{
				if(Context.Instance.BattleSetting.Auto) 
				{
					if(!Context.Instance.AutoCache.Contains(Context.Instance.QuestID))
						Context.Instance.AutoCache.Add(Context.Instance.QuestID);
				}
				else
					Context.Instance.AutoCache.Remove(Context.Instance.QuestID);
			}
		}
	}

	public void OnStateIconClick()
	{
		SetIsIsPaused(!QuestManager.Instance.Controller.IsPaused);
	}
	
	private void OnBattleSpeed(IMessage msg)
	{
		SetIsIsPaused(true);
		var newVal = Context.Instance.BattleSetting.Speed + 0.5f;
		if(newVal > 1.5f) newVal = 0.5f;
		Context.Instance.BattleSetting.Speed = newVal;
		Context.Instance.BattleSetting.Save();
		UpdateSpeedIcon();

		SetIsIsPaused(false);
	}

	private void OnBattleState(IMessage msg)
	{
		SetIsIsPaused((int)msg.Data == 0);
	}

	private void OnBattleAuto()
	{
		if(!GameSystem.Service.QuestInfo[QuestManager.Instance.Current.QuestID].IsCleared)
		{
			MessageBox.Show(SystemTextID.TIP_BATTLE_AUTO_NOT_ALLOW);
			return;
		}

		SetIsIsPaused(true);
		Context.Instance.BattleSetting.Auto = !Context.Instance.BattleSetting.Auto;
		if(Context.Instance.BattleSetting.Auto)
			GameSystem.Service.OnAutoBattleEnabled();
		UpdateAutoIcon();
		SetIsIsPaused(false);
	}

	private void SetIsIsPaused(bool isPaused)
	{
		if(isPaused)
		{
			if(++pauseCount == 1) QuestManager.Instance.Controller.Pause();
		}
		else
		{
			if(--pauseCount == 0) QuestManager.Instance.Controller.Resume();
		}

		UpdateIcon();
	}

	private void UpdateIcon()
	{
		stateIcon.spriteName = QuestManager.Instance.Controller.IsPaused ? "button_04b" : "button_04c";
	}

	private void UpdateSpeedIcon()
	{
		if(Context.Instance.BattleSetting.Speed < 1)
			speedIcon.spriteName = "button_04d";
		else if(Context.Instance.BattleSetting.Speed > 1)
			speedIcon.spriteName = "button_04f";
		else
			speedIcon.spriteName = "button_04e";
	}

	private void UpdateAutoIcon()
	{
		autoIcon.color = Context.Instance.BattleSetting.Auto ? Color.gray : Color.white;
	}
}