﻿using UnityEngine;
using System.Collections;

public class SystemSettingMessageBox : MessageBox {

	public UISlider sound;
	public UISlider music;

	private bool isReady = false;
	protected override void OnReady ()
	{
		sound.value = Context.Instance.SystemSetting.SoundVolume;
		music.value = Context.Instance.SystemSetting.MusicVolume;
		isReady = true;
	}
	
	protected override void OnDestroy ()
	{
		if(GameSystem.IsAvailable) Context.Instance.SystemSetting.Save();
		base.OnDestroy ();
	}

	protected override void OnCloseByClick ()
	{
		Message.Send(MessageID.Back);
	}

	public void OnSoundValueChange()
	{
		if(!isReady) return;
		Context.Instance.SystemSetting.SoundVolume = sound.value;
		Message.Send(MessageID.SoundVolume, sound.value);
	}

	public void OnMusicValueChange()
	{
		if(!isReady) return;
		Context.Instance.SystemSetting.MusicVolume = music.value;
		Message.Send(MessageID.MusicVolume, music.value);
	}
}

