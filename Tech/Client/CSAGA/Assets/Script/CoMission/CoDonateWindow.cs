﻿using UnityEngine;
using System.Collections;

public class CoDonateWindow : SubmitWindow {

	public Item item;

	public UILabel count;
	
	public UISlider slider;
	
	private CoSubjectItemData data;
	
	private bool isRecursive = false;
	
	private int current;
	
	private int originCount;
	
	private int max;

	protected override void OnReady ()
	{
		Message.AddListener(MessageID.Increase, OnIncrease);

		data = Context.Instance.CoSubject;

		item.ItemID = data.Rawdata.n_ID_[0];
		item.ApplyChange();
		
		originCount = GameSystem.Service.ItemInfo.Count(data.Rawdata.n_ID_[0]);

		
		if(data.Rawdata.n_MAX > 0)
			max = data.Rawdata.n_MAX - data.Origin.Progress;
		else
			max = originCount;
		
		if(max > 0) current = 1;
		
		UpdateProgress();
	}

	protected override void OnDestroy ()
	{
		Message.RemoveListener(MessageID.Increase, OnIncrease);
		base.OnDestroy ();
	}
	
	public void OnSliderValueChange()
	{
		if(isRecursive) return;
		
		current =  max > 0 ? 1 + (int)((max - 1) * UISlider.current.value) : 0;
		
		UpdateProgress();
	}
	
	private void OnIncrease(IMessage msg)
	{
		isRecursive = true;
		
		var newValue = Mathf.Max(Mathf.Min(current + (int)msg.Data, max), Mathf.Min(1, max));
		
		if(newValue != current) 
		{
			current = newValue;
			
			slider.value = (float)current / (float)max;
			
			UpdateProgress();
		}
		
		isRecursive = false;
	}
	
	private void UpdateProgress()
	{
		count.text = current.ToString();
	}

	#region implemented abstract members of SubmitWindow

	protected override bool OnSubmit ()
	{
		if(data.Mission.BeginTime.Date == GameSystem.Service.CurrentTime.Date)
		{
			if(current == 0) return false;
			GameSystem.Service.MissionDonate(data.Mission.Serial, data.ID, current);
			return true;
		}
		else
			return true;
	}

	#endregion



}
