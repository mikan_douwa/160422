﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CoMissionInfo : Stage {
	
	public CoMissionProfile profile;

	public ItemList subjectItemList;

	public UIButton claimBtn;

	private CoMissionItemData data;

	private ListItemDataProvider<CoSubjectItemData> provider;

	protected override void Initialize ()
	{
		data = Context.Instance.CoMission;

		profile.Data = data;
		profile.ApplyChange();

		var list = new List<CoSubjectItemData>(data.Origin.Subjects.Count);
		
		foreach(var item in data.Origin.Subjects)
		{
			var obj = new CoSubjectItemData{ Origin = item, ID = item.ID };
			obj.Rawdata = ConstData.Tables.CoMission[item.ID];
			obj.Mission = data.Origin;
			list.Add(obj);
		}

		provider = ListItemDataProvider<CoSubjectItemData>.Create(list);

		subjectItemList.Setup(provider);

		if(data.Origin.State.IsClaimed) claimBtn.isEnabled = false;

		GameSystem.Service.OnCooperateMissionDonate += OnCooperateMissionDonate;
		GameSystem.Service.OnCooperateMissionClaim += OnCooperateMissionClaim;

		base.Initialize ();
	}

	void OnCooperateMissionDonate ()
	{
		profile.ApplyChange();
		provider.TriggerOnChange();
	}

	void OnCooperateMissionClaim (List<Mikan.CSAGA.Reward> list)
	{
		QuestDrop.Show(list);

		claimBtn.isEnabled = false;
	}

	protected override void OnDestroy ()
	{
		if(GameSystem.IsAvailable) 
		{
			GameSystem.Service.OnCooperateMissionDonate -= OnCooperateMissionDonate;
			GameSystem.Service.OnCooperateMissionClaim -= OnCooperateMissionClaim;
		}

		base.OnDestroy ();
	}

	public void OnClaim()
	{
		if(data.Origin.Progress < 100)
		{
			MessageBox.Show(SystemTextID._274_TIP_CO_MISSION_NOT_FINISH_YET);
			return;
		}

		var progress = 0;

		foreach(var item in data.Origin.Subjects) progress += item.Progress;

		if(progress < ConstData.Tables.CoMissionThreshold)
			MessageBox.Show(SystemTextID._275_TIP_CO_MISSION_REQUIRE_V1, ConstData.Tables.CoMissionThreshold - progress);
		else
			GameSystem.Service.MissionClaim(data.Origin.Serial);
	}
	
	public void OnRanking()
	{
		if(!StageManager.Instance.IsAvailable(StageID.CoMissionRanking))
			MessageBox.Show(SystemTextID.TIP_NOT_AVAILABLE);
		//GameSystem.Service.GetMissionRanking(data.Origin.Serial);
	}
}
