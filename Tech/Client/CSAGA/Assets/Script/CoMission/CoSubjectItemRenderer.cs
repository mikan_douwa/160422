﻿using UnityEngine;
using System.Collections;

public class CoSubjectItemRenderer : ItemRenderer<CoSubjectItemData> {

	public Icon icon;

	protected override void OnStart ()
	{
		GameSystem.Service.OnCooperateMissionDonate += OnCooperateMissionDonate;
	}
	void OnDestroy()
	{
		if(GameSystem.IsAvailable) GameSystem.Service.OnCooperateMissionDonate -= OnCooperateMissionDonate;
	}

	void OnCooperateMissionDonate ()
	{
		if(data != null && data.Rawdata.n_TYPE == Mikan.CSAGA.MissionType.Item)
		{
			if(data.Rawdata.n_MAX > 0) 
				icon.manualIconName = string.Format("{0}/{1}", data.Origin.Progress, data.Rawdata.n_MAX);
			else
				icon.manualIconName = string.Format("{0}/∞", data.Origin.Progress);

			icon.RefreshIconName();
		}
	}

	#region implemented abstract members of ItemRenderer

	protected override void ApplyChange (CoSubjectItemData data)
	{
		switch(data.Rawdata.n_TYPE)
		{
		case Mikan.CSAGA.MissionType.Item:
			icon.IconType = Mikan.CSAGA.DropType.Item;
			icon.IconID = data.Rawdata.n_ID_[0];
			//icon.Count = -1;

			//icon.manualIconSprite = ConstData.Tables.Item[data.Rawdata.n_ID_[0]].s_ICON;
			//icon.manualIconSize = new Vector2(72, 58);
			break;
		case Mikan.CSAGA.MissionType.Quest1:
		case Mikan.CSAGA.MissionType.Quest2:
			icon.manualIconSprite = "button_menu02";
			icon.manualIconSize = new Vector2(70, 70);
			break;
		case Mikan.CSAGA.MissionType.Talk:
			icon.manualIconSprite = "image_talk";
			icon.manualIconSize = new Vector2(75, 75);
			break;
		case Mikan.CSAGA.MissionType.Gift:
			icon.manualIconSprite = "image_gift";
			icon.manualIconSize = new Vector2(75, 75);
			break;
		}

		if(data.Rawdata.n_MAX > 0) 
			icon.manualIconName = string.Format("{0}/{1}", data.Origin.Progress, data.Rawdata.n_MAX);
		else
			icon.manualIconName = string.Format("{0}/∞", data.Origin.Progress);
		icon.ApplyChange();
	}

	#endregion

	void OnClick()
	{
		Context.Instance.CoSubject = data;

		if(data.Mission.BeginTime.Date == GameSystem.Service.CurrentTime.Date)
		{
			if(data.Rawdata.n_TYPE == Mikan.CSAGA.MissionType.Item)
				MessageBox.ShowConfirmWindow("GameScene/CoMission/DonateWindow", "", null);
			else
				MessageBox.Show("GameScene/CoMission/CoSubjectInfo", "");
		}
	}

	void OnLongPress()
	{
		if(data.Rawdata.n_TYPE == Mikan.CSAGA.MissionType.Item)
			ItemInfo.Show(data.Rawdata.n_ID_[0]);

	}

}
