﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CoMissionPage : Stage {

	public ItemList itemList;

	protected override void Initialize ()
	{
		GameSystem.Service.OnCooperateMissionInfo += OnCooperateMissionInfo;
		GameSystem.Service.GetCooperateMissionInfo();
	}

	protected override void OnDestroy ()
	{
		if(GameSystem.IsAvailable) GameSystem.Service.OnCooperateMissionInfo -= OnCooperateMissionInfo;
		base.OnDestroy ();
	}

	void OnCooperateMissionInfo ()
	{
		var list = new List<CoMissionItemData>();

		var currentTime = GameSystem.Service.CurrentTime;

		foreach(var item in GameSystem.Service.CooperateMissionInfo.List)
		{
			if(item.BeginTime > currentTime) continue;

			var rawdata = ConstData.Tables.CoMission[item.Subjects[0].ID];

			if(item.BeginTime.Date < currentTime.Date && (item.Progress < 100 || item.State.IsClaimed || item.Point == 0)) continue;

			var data = new CoMissionItemData{ Origin = item };
			data.ID = rawdata.n_ID;
			data.Rawdata = rawdata;
			list.Add(data);
		}

		list.Sort((lhs, rhs)=>{ return rhs.Origin.Serial.CompareTo(lhs.Origin.Serial); });

		itemList.Setup(list);

		InitializeComplete();
	}
}


public class CoMissionItemData
{
	public Mikan.CSAGA.Data.CooperateMission Origin;

	public Mikan.CSAGA.ConstData.CoMission Rawdata;

	public int ID;
}

public class CoSubjectItemData
{
	public Mikan.CSAGA.Data.CooperateMission Mission;

	public Mikan.CSAGA.Data.MissionSubejct Origin;

	public Mikan.CSAGA.ConstData.CoMission Rawdata;

	public int ID;

	public bool IsMissionClaimed;
}