﻿using UnityEngine;
using System.Collections;

public class CoMissionProfile : UIBase {

	public CoMissionItemData Data;

	public TextureLoader icon;

	public UILabel title;

	public UILabel talk;
	
	public UILabel point;
	
	public UILabel progress;
	
	public UILabel remainTime;

	public UIProgressBar progressBar;

	protected override void OnChange ()
	{
		var text = ConstData.Tables.MissionText[Data.ID];
		
		if(title != null) title.text = text.s_NAME;

		if(talk != null) talk.text = text.s_TALK;
		
		point.text = ConstData.GetSystemText(SystemTextID._265_CO_MISSION_POINT_V1, Data.Origin.Point);

#if DEBUG
		progress.text = ConstData.GetSystemText(SystemTextID._266_CO_MISSION_PERCENT_V1, Data.Origin.Progress);
#else
		var _progress = Data.Origin.Progress > 100 ? "100+" : Data.Origin.Progress.ToString();
		
		progress.text = ConstData.GetSystemText(SystemTextID._266_CO_MISSION_PERCENT_V1, _progress);
#endif

		if(progressBar != null) progressBar.value = Data.Origin.Progress / 100f;
		
		icon.Load(ConstData.GetCardPicPath(Data.Rawdata.n_CARDID));
		
		remainTime.text = UIUtils.FormatRemainTime(Data.Origin.BeginTime.AddDays(1), false);
	}
}
