﻿using UnityEngine;
using System.Collections;

public class CoMissionItemRenderer : ItemRenderer<CoMissionItemData> {

	public CoMissionProfile profile;

	#region implemented abstract members of ItemRenderer

	protected override void ApplyChange (CoMissionItemData data)
	{
		profile.Data = data;
		profile.ApplyChange();
	}

	#endregion


	void OnClick()
	{
		Context.Instance.CoMission = data;
		Message.Send(MessageID.SwitchStage, StageID.CoMissionInfo);
	}
}
