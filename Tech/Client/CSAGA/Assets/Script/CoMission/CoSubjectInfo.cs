﻿using UnityEngine;
using System.Collections;

public class CoSubjectInfo : MonoBehaviour {

	public UILabel description;

	public UILabel progress;

	void Start ()
	{
		var data = Context.Instance.CoSubject;
		
		if(description != null) description.text = ConstData.GetSubjectTip(data.Rawdata);
		
		progress.text = ConstData.GetSystemText(SystemTextID._273_CO_MISSION_PROGRESS_V1_V2, data.Origin.Progress, data.Rawdata.n_MAX > 0 ? data.Rawdata.n_MAX.ToString() : "∞");

	}
}
