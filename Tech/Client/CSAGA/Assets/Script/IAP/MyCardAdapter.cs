﻿#if MYCARD
using UnityEngine;
using System.Collections; 
//using Newtonsoft.Json;

// Data Structure
public class GetAuthCodeResult {
	public string ReturnCode { get; set; } 
	public string ReturnMsg { get; set; } 
	public string AuthCode { get; set; } 
	public string TradeSeq { get; set; }
}
public class SDKResult {
	public string ReturnCode { get; set; } 
	public string ReturnMsg { get; set; } 
	public string PayResult { get; set; }
	public string FacTradeSeq { get; set; } 
	public string PaymentType { get; set; } 
	public string Amount { get; set; }
	public string Currency { get; set; }
	public string MyCardTradeNo { get; set; } 
	public string MyCardType { get; set; } 
	public string PromoCode { get; set; } 
	public string SerialId { get; set; }
}

public class MyCardAdapter : Singleton<MyCardAdapter> {

	public delegate void PurchaseHandler(IAPReceipt receipt);
	public delegate void CancelHandler();

	public event PurchaseHandler OnPurchase;
	public event CancelHandler OnCancel;

	string isTest = "true";

	private AndroidJavaClass MyCardSDK;
	string payresult;
	string paymentConfirmResult;
	GetAuthCodeResult authcodeOBJ;
	SDKResult sdkOBJ;

	public void InitMyCard()
	{
#if !UNITY_EDITOR
		MyCardSDK = new AndroidJavaClass ("tw.com.mycard.paymentsdk.unity.MyCardSDK");
#endif
	}

	// Call SDK Method
	public void StartPay (bool isTest, string authCode)
	{
		string sandBox = "false";
		if(isTest)
			sandBox = "true";
		string code = authCode;
		if(code == null)
			code = "";

		// Set whether to test environment
		MyCardSDK.CallStatic ("SetIsTestServer", new object[] {sandBox});
		// Execution SDK for payment
		MyCardSDK.CallStatic ("StartPayment", new object[] {
			gameObject.name,
			code
		});
	}

	
	// Return SDK paid results
	void PayCompleted (string result)
	{
		IAPReceipt receipt = new IAPReceipt(){ ResultCode = "0", OrderId = "MYCARD", token = result };
		if(OnPurchase != null)
			OnPurchase(receipt);
		/*
		payresult = result;
		sdkOBJ = JsonConvert.DeserializeObject<SDKResult> (payresult);
		if (sdkOBJ.ReturnCode.Equals ("1") && sdkOBJ.PayResult.Equals ("3")) {
			//StartCoroutine(TradeQuery());
		}
		*/
	}
	
	// Return SDK cancel payments
	void PayCancel (string result) {
		if(OnCancel != null)
			OnCancel();
	}


}
#endif