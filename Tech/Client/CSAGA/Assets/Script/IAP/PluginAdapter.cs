﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;

public class PluginAdapter : Singleton<PluginAdapter> {

	public static bool Inited = false;
	public static bool Cancelled = false;
	int TotalRetry = 3;
	int InitRetry = 0;

	public delegate void PurchaseHandler(IAPReceipt receipt);
	public delegate void ConsumeHandler(IAPConsumeResult consume);
	public delegate void QueryHandler(List<IAPQueryResult> query);

	public event PurchaseHandler OnPurchase;
	public event ConsumeHandler OnConsume;
	public event QueryHandler OnQuery;

#if UNITY_EDITOR
	public void InitIAB()
	{
		UIDefine.IAPName = "Editor";
		Inited = true;
	}
	
	public void PurchaseItem(string sku)
	{}
	
	public void QueryPurchase(string identifiers)
	{}
	
	public void ConsumePurchase(string sku)
	{}
#elif MYCARD
	public void InitIAB()
	{
		if(Inited) return;
		using(AndroidJavaClass jc = new AndroidJavaClass ("com.unity3d.player.UnityPlayer")){
			using(AndroidJavaObject jo = jc.GetStatic<AndroidJavaObject> ("currentActivity")){
				using(AndroidJavaClass ajc = new AndroidJavaClass ("com.mikan.csagaplugin.MyCardPlugin")){
					ajc.CallStatic("InitMyCard", jo, gameObject.name);
				}
			}
		}
	}
	
	public void PurchaseItem(string sku)
	{}
	
	public void QueryPurchase(string identifiers)
	{}
	
	public void ConsumePurchase(string sku)
	{}
#elif UNITY_ANDROID
	public void InitIAB(){
		if(Inited) return;
		using(AndroidJavaClass jc = new AndroidJavaClass ("com.unity3d.player.UnityPlayer")){
			using(AndroidJavaObject jo = jc.GetStatic<AndroidJavaObject> ("currentActivity")){
				jo.Call("InitIAB", gameObject.name);
			}
		}
	}

	public void PurchaseItem(string sku){
		//Debug.Log("Purchase " + sku);

		if(Inited)
		{
			using(AndroidJavaClass jc = new AndroidJavaClass ("com.unity3d.player.UnityPlayer")){
				using(AndroidJavaObject jo = jc.GetStatic<AndroidJavaObject> ("currentActivity")){
					jo.Call("PurchaseItem", sku, UIDefine.IAPName);
				}
			}
		}
	}

	public void QueryPurchase(string identifiers)
	{
		if(Inited)
		{
			using(AndroidJavaClass jc = new AndroidJavaClass ("com.unity3d.player.UnityPlayer")){
				using(AndroidJavaObject jo = jc.GetStatic<AndroidJavaObject> ("currentActivity")){
					jo.Call("QueryPurchase");
				}
			}
		}
	}

	public void ConsumePurchase(string sku)
	{
		if(Inited)
		{
			using(AndroidJavaClass jc = new AndroidJavaClass ("com.unity3d.player.UnityPlayer")){
				using(AndroidJavaObject jo = jc.GetStatic<AndroidJavaObject> ("currentActivity")){
					jo.Call("ConsumePurchase", sku);
				}
			}
		}
	}

#elif UNITY_IOS
	[DllImport ("__Internal")]
	private static extern void _InitIAP (string gameObjectName);
	
	[DllImport ("__Internal")]
	private static extern bool _canMakePayment ();
	
	[DllImport ("__Internal")]
	private static extern void _requestProducts (string identifiers);
	
	[DllImport ("__Internal")]
	private static extern void _requestPayment (string identify);

	public void InitIAB()
	{
		_InitIAP (gameObject.name);
	}
	
	public void PurchaseItem(string sku)
	{
		_requestPayment (sku);
	}
	
	public void QueryPurchase(string identifiers)
	{
		_requestProducts(identifiers);	
	}
	
	public void ConsumePurchase(string sku)
	{}

	#else
	public void InitIAB()
	{
		Inited = true;
	}
	
	public void PurchaseItem(string sku)
	{}
	
	public void QueryPurchase(string identifiers)
	{}
	
	public void ConsumePurchase(string sku)
	{}
	

#endif

	public void InitResult(string resultCode)
	{
#if UNITY_ANDROID
		string[] arr = resultCode.Split(';');
		if(arr.Length >= 2)
			UIDefine.IAPName = arr[1];

		if(arr.Length > 0 && arr[0] == "0")
			Inited = true;
		else if(InitRetry < TotalRetry)
		{
			InitRetry++;
			InitIAB();
		}
		else
		{
			UIDefine.IAPName = "NotAvailable";
			Cancelled = true;
		}
#else

		if(resultCode == "0")
		{
			UIDefine.IAPName = "AppStore";
			Inited = true;
		}
		else if(InitRetry < TotalRetry)
		{
			InitRetry++;
			InitIAB();
		}
		else
		{
			UIDefine.IAPName = "NotAvailable";
			Cancelled = true;
		}
#endif
	}

	public void PurchaseResult(string result)
	{
		string[] str = result.Split(',');
		string sku = str[0];
		string resultCode = str[1];

		//Debug.Log("PurchaseResult");// sku:" + sku + ", result:" + resultCode + ", orderId:" + PlayerPrefs.GetString(sku) + ", token:" + PlayerPrefs.GetString(sku + "_token"));
		//if(resultCode != "0") return;

		IAPReceipt receipt = new IAPReceipt(){ SKU = sku, ResultCode = resultCode, OrderId = PlayerPrefs.GetString(sku), token = PlayerPrefs.GetString(sku + "_token") };
		if(OnPurchase != null)
			OnPurchase(receipt);
	}

	public void QueryResult(string result)
	{
		//Debug.Log("QueryResult:" + result);

		List<IAPQueryResult> lst = new List<IAPQueryResult>();
		if(result.Length > 0)
		{
			string[] str = result.Split(';');
			for(int i = 0; i < str.Length; i++)
			{
				string[] qStr = str[i].Split(',');
				if(qStr.Length < 2) continue;
				string sku = qStr[0];
				string resultCode = qStr[1];
				lst.Add(new IAPQueryResult(){ SKU = sku, HasPurchased = (resultCode == "1") });
			}
		}
		if(OnQuery != null)
			OnQuery(lst);
	}

	public void ConsumeResult(string result)
	{
		string[] str = result.Split(',');
		string sku = str[0];
		string resultCode = str[1];
		//Debug.Log("Consume Result");// sku:" + sku + ", result:" + resultCode);

		IAPConsumeResult consume = new IAPConsumeResult(){ SKU = sku, ResultCode = resultCode };
		if(OnConsume != null)
			OnConsume(consume);
	}
}

public class IAPReceipt{
	public string SKU;
	public string ResultCode;
	public string OrderId;
	public string token;

	public IAPReceipt(){}
}

public class IAPConsumeResult{
	public string SKU;
	public string ResultCode;
}

public class IAPQueryResult{
	public string SKU;
	public bool HasPurchased;
}
