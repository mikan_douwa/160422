﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using Mikan.CSAGA.Data;

public class IAPRechecker : Singleton<IAPRechecker> {

	public bool inited = false;

	public void Init()
	{
		if(!inited)
		{
			GameSystem.Service.OnIAPClaim += PurchaseServerCallback;
			inited = true;
		}
	}

	public void CheckIAP()
	{
		GameSystem.Service.OnIAPInfo += OnIAPInfo;
		GameSystem.Service.GetIAPInfo();
	}

	public void OnIAPInfo()
	{
		GameSystem.Service.OnIAPInfo -= OnIAPInfo;
		List<IAPProduct> lstIAP = GameSystem.Service.IAPInfo.List;
		
		for(int i = 0; i < lstIAP.Count; i++)
		{
			IAPProduct product = lstIAP[i];
			string orderId = PlayerPrefs.GetString(product.ProductID);
			if(orderId != "")
			{
				GameSystem.Service.IAPClaim(product.ProductID, orderId, PlayerPrefs.GetString(product.ProductID + "_token"));
			}

		}

	}

	public void PurchaseServerCallback (string orderId, string productId, int gem)
	{
		PlayerPrefs.SetString(productId, "");
		PlayerPrefs.SetString(productId + "_token", "");
	}

}
