﻿using UnityEngine;
using System.Collections;

public class GachaShowCardItemRenderer : ItemRenderer<GachaShowCardData> {

	public TextureLoader pic;
	
	public Stars stars;
	
	public UILabel cardName;

	#region implemented abstract members of ItemRenderer

	protected override void ApplyChange (GachaShowCardData data)
	{
		Context.Instance.Card = new CardData{ Origin = data.Origin, CardID = data.Origin.ID };
		
		pic.Load(ConstData.GetCardPicPath(data.Origin.ID));
		
		cardName.text = ConstData.GetCardName(data.Origin.ID);
		
		stars.viewCount = data.Origin.Rare;
		stars.ApplyChange();
	}

	#endregion

}

public class GachaShowCardData
{
	public Mikan.CSAGA.Data.Card Origin;
}