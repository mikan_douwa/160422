﻿using UnityEngine;
using System.Collections;

public class GachaDrawShowResult : Stage {

	protected override void Initialize ()
	{
		nextButtonText = "[ffff00]" + ConstData.GetSystemText(SystemTextID.BTN_IAP);

		base.Initialize ();
	}

	protected override void OnNext ()
	{
		Message.Send(MessageID.IAP);
	}

	void OnClick()
	{
		OnBack();
	}
}
