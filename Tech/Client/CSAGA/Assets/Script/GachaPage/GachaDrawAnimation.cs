﻿using UnityEngine;
using System.Collections;

public class GachaDrawAnimation : Stage {

	public UISprite cover;
	public UISprite gem;
	public FXController light;
	public AnimationCurve dropCurve;
	public AnimationCurve riseCurve;

	bool isOnce = true;

	protected override void Initialize ()
	{
		isOnce = (Context.Instance.Cards.Count == 1);

		Menu.Hide ();

		ResourceManager.Instance.CreatePrefabInstance(gameObject, "GameScene/Gacha/BottomMask");
		
		cover.color = Color.black;
		cover.alpha = 1f;
		gem.alpha = 0f;
		gem.transform.localPosition = new Vector3(0f, 800f, 0f);
		gem.transform.localRotation = Quaternion.Euler(Vector3.zero);
		NGUITools.SetActive(light.gameObject, false);

		TweenAlpha ta = TweenAlpha.Begin(cover.gameObject, 0.5f, 0f);
		ta.ignoreTimeScale = false;
		EventDelegate.Add(ta.onFinished, DropStart, true);

		base.Initialize ();
	}

	protected override void OnDestroy ()
	{
		base.OnDestroy ();
		Menu.Show ();
	}

	protected override void OnGetBack ()
	{
		OnBack();
	}

	void DropStart()
	{
		float finalAngle = -359f;
		float rotateTime = 1.8f;
		if(isOnce)
		{
			gem.transform.localScale = Vector3.one;
		}
		else
		{
			gem.transform.localScale = new Vector3(5, 5, 5);
			finalAngle = 40f;
			rotateTime = 1.8f;
		}

		//Animation.
		TweenY ty = TweenY.Begin(gem.gameObject, 0.8f, 150f);
		ty.ignoreTimeScale = false;
		EventDelegate.Add(ty.onFinished, Drop2, true);
		//Rotate
		TweenClock tc = TweenClock.Begin(gem.gameObject, rotateTime, finalAngle);
		tc.ignoreTimeScale = false;
		tc.method = UITweener.Method.EaseIn;
		//Alpha
		TweenAlpha ta = TweenAlpha.Begin(gem.gameObject, 0.3f, 1f);
		ta.ignoreTimeScale = false;
	}

	void Drop2()
	{
		//Sound
		if(isOnce)
			Message.Send(MessageID.PlaySound, SoundID.GACHA_DROP);
		else
			Message.Send(MessageID.PlaySound, SoundID.GACHA_DROP2);

		if(isOnce)
		{
			TweenY ty = TweenY.Begin(gem.gameObject, 0.7f, 450f);
			ty.ignoreTimeScale = false;
			ty.animationCurve = dropCurve;
			EventDelegate.Add(ty.onFinished, Drop3, true);
		}
		else
		{
			Camera camera = CameraList.Instance.Get((int)UILayer.Stage);
			if(camera != null)
			{
				TweenShake ts = TweenShake.Begin(camera.gameObject, 0.5f, 0.1f, 3);
				ts.ignoreTimeScale = false;
				EventDelegate.Add(ts.onFinished, DropEnd, true);

				TweenY ty = TweenY.Begin(gem.gameObject, 1f, 250f);
				ty.ignoreTimeScale = false;
				ty.method = UITweener.Method.EaseOut;
			}
			else
				DropEnd();
		}
	}

	void Drop3()
	{
		//Sound
		Message.Send(MessageID.PlaySound, SoundID.GACHA_DROP);

		TweenY ty = TweenY.Begin(gem.gameObject, 0.3f, 230f);
		ty.ignoreTimeScale = false;
		ty.animationCurve = dropCurve;
		EventDelegate.Add(ty.onFinished, DropEnd, true);
	}

	void DropEnd()
	{
		//Sound
		Message.Send(MessageID.PlaySound, SoundID.GACHA_LIGHT);
		//Recover Camera
		Camera camera = CameraList.Instance.Get((int)UILayer.Stage);
		if(camera != null)
		{
			camera.transform.localPosition = Vector3.zero;
		}

		NGUITools.SetActive(light.gameObject, true);
		light.Play(null);
		//Rise
		if(isOnce)
		{
			TweenY ty = TweenY.Begin(gem.gameObject, 1f, 480f);
			ty.ignoreTimeScale = false;
			ty.animationCurve = riseCurve;
		}
		//Shine
		cover.color = Color.white;
		cover.alpha = 0f;
		TweenAlpha ta = TweenAlpha.Begin(cover.gameObject, 0.5f, 1f);
		ta.ignoreTimeScale = false;
		EventDelegate.Add(ta.onFinished, RiseEnd, true);

	}

	void RiseEnd()
	{
		NGUITools.SetActive(gem.gameObject, false);
		//Shine fade out
		TweenAlpha ta = TweenAlpha.Begin(cover.gameObject, 0.5f, 0f);
		ta.ignoreTimeScale = false;

		Message.Send(MessageID.SwitchStage, StageID.GachaDrawShowCard);
	}
}
