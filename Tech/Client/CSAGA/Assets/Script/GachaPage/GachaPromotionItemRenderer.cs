﻿using UnityEngine;
using System.Collections;

public class GachaPromotionItemRenderer : ItemRenderer<GachaPromotionData> {

	public TextureLoader pic;

	#region implemented abstract members of ItemRenderer

	protected override void ApplyChange (GachaPromotionData data)
	{
		pic.Load(ConstData.GetCardPicPath(data.Origin.n_CARDID));
	}

	#endregion

}

public class GachaPromotionData
{
	public Mikan.CSAGA.ConstData.Gacha Origin;
}