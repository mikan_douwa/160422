﻿using UnityEngine;
using System.Collections;

public class GachaPromotionNavigator : Navigator {


	protected override ItemRenderer GetItemRenderer (GameObject item)
	{
		var renderer = base.GetItemRenderer (item) as GachaPromotionItemRenderer;
		renderer.Owner = this;
		return renderer;

	}
	protected override void OnForwardEnd (ItemRenderer renderer)
	{

		base.OnForwardEnd(renderer);
	}
}
