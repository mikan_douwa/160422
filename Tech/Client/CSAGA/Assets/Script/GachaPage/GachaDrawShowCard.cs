﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GachaDrawShowCard : Stage {

	public static GachaDrawShowCard Current { get; private set; }
	public bool IsSingle { get { return isSingle; } }
	public Navigator navigator;

	private bool isShowCardInfo = false;

	private bool isSingle = false;

	protected override void OnStart ()
	{
		Current = this;

		base.OnStart ();
	}

	protected override void Initialize ()
	{
		var list = new List<GachaShowCardData>(Context.Instance.Cards.Count);
		foreach(var item in Context.Instance.Cards)
		{
			list.Add(new GachaShowCardData{ Origin = GameSystem.Service.CardInfo[item] });
		}

		isSingle = list.Count == 1;

		navigator.Setup(list);
		
		base.Initialize ();
	}

	protected override void OnDestroy ()
	{
		if(Current == this) Current = null;
		base.OnDestroy ();
	}

	void OnClick()
	{
		if(isSingle)
			Message.Send(MessageID.SwitchStage, StageID.CardInfo);
		else if(navigator.Current < navigator.DataProvider.Count - 1)
			navigator.Next();
		else
			Message.Send(MessageID.SwitchStage, StageID.GachaDrawShowResult);

	}
}
