﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GachaPage : Stage {

	const int DAILY = 0;
	const int SINGLE = 1;
	const int MULTI = 2;

	public CardProfile cardProfile;
	public CardTitle cardTitle;

	public UIToggle regularTab;
	public UIToggle activityTab;

	public UILabel iapGem;
	public UILabel freeGem;

	public GameObject dailyDrawObj;
	public GameObject singleDrawObj;
	public GameObject multiDrawObj;

	public UILabel dailyDraw;
	public UILabel singleDraw;
	public UILabel multiDraw;

	public UIWidget dailyDisableMask;

	public GameObject dailyTip;

	public Navigator promotionList;

	public UILabel remainTime;

	public UISlider boostBar;

	private List<int> gachaList;

	private Mikan.CSAGA.GachaListTag gachaTag;

	private Mikan.CSAGA.Calc.GachaSchedule schedule;

	private bool dailyEnabled;

	private System.DateTime activityEndTime;

	protected override void Initialize ()
	{
		ResourceManager.Instance.CreatePrefabInstance(gameObject, "GameScene/StageNavigatorGacha");

		Message.AddListener(MessageID.DrawGacha, OnDrawGacha);

		GameSystem.Service.PlayerInfo.OnPropertyChange += OnPropertyChange;
		GameSystem.Service.OnGachaDraw += OnGachaDraw;

		schedule = Mikan.CSAGA.Calc.Gacha.GetGachaSchdule();

		activityEndTime = schedule.ActivityEndTime;
		
		var activityGachaID = 0;
		
		foreach(var gachaListID in ConstData.Tables.ActivityGachaIDs)
		{
			activityGachaID = schedule.GetGachaID(gachaListID, GameSystem.Service.CurrentTime);
			if(activityGachaID != 0) break;
		}
		
		if(activityGachaID == 0) activityTab.gameObject.SetActive(false);

		//ShowRegularGacha();
		ShowGacha(Mikan.CSAGA.GachaListTag.RegularGacha, ConstData.Tables.RegularGachaIDs, true);

		RefreshGem();
		RefreshBoost();

		nextButtonText = "[ffff00]" + ConstData.GetSystemText(SystemTextID.BTN_IAP);

		base.Initialize ();
	}

	protected override void OnInitializeComplete ()
	{
		promotionList.gameObject.SetActive(true);
	}

	protected override void OnGetBack ()
	{
		RefreshBoost();
	}

	void OnEnable()
	{
		switch(gachaTag)
		{
		case Mikan.CSAGA.GachaListTag.RegularGacha:
			regularTab.value = true;
			break;
		case Mikan.CSAGA.GachaListTag.ActivityGacha:
			activityTab.value = true;
			break;
		}
	}

	void OnPropertyChange (Mikan.CSAGA.Data.PlayerInfo sender, Mikan.CSAGA.PropertyID id, int oldValue, int newValue)
	{
		if(id == Mikan.CSAGA.PropertyID.Gem || id == Mikan.CSAGA.PropertyID.IAPGem) RefreshGem();
	}

	protected override void OnDestroy ()
	{
		if(GameSystem.IsAvailable) 
		{
			GameSystem.Service.OnGachaDraw -= OnGachaDraw;
			GameSystem.Service.PlayerInfo.OnPropertyChange -= OnPropertyChange;
		}

		Message.RemoveListener(MessageID.DrawGacha, OnDrawGacha);

		promotionList.OnCurrentChange -= OnNavigatorCurrentChange;

		base.OnDestroy ();
	}

	protected override void OnNext ()
	{
		Message.Send(MessageID.SwitchStage, StageID.IAP);
	}

	private void OnDrawGacha(IMessage msg)
	{
		var param = (int)msg.Data;
		var tip = default(SystemTextID);
		var title = default(SystemTextID);
		switch(param)
		{
		case DAILY:
			tip = SystemTextID._191_TIP_DAILY_DRAW_CONFIRM_V1;
			title = SystemTextID._184_DAILY_DRAW;
			break;
		case SINGLE:
			tip = SystemTextID._192_TIP_SIGNLE_DRAW_CONFIRM_V1;
			title = SystemTextID._185_SINGLE_DRAW;
			break;
		case MULTI:
			tip = SystemTextID._193_TIP_MULTI_DRAW_CONFIRM_V1;
			title = SystemTextID._186_MULTI_DRAW;
			break;
		}

		var gachaListID = gachaList[param];
		var price = ConstData.Tables.GachaList[gachaListID].n_COIN;

		var text = ConstData.GetSystemText(tip, price);

		if(param == DAILY)
		{
			MessageBox.ShowConfirmWindow("GameScene/Gacha/DailyGachaConfirmWindow", "[000000]" + text, (index)=>
			                             {
				if(index == 0) ShopManager.Instance.DrawGacha(gachaListID);
			}).title = ConstData.GetSystemText(title);
		}
		else
		{
			MessageBox.ShowConfirmWindow(text, (index)=>
			{
				if(index == 0) ShopManager.Instance.DrawGacha(gachaListID);
			}).title = ConstData.GetSystemText(title);
		}

	}
	
	private void OnGachaDraw (Mikan.CSAGA.SyncCard syncCard)
	{
		RefreshGem();
		RefreshDailyButton();
	}

	public void ShowRegularGacha()
	{
		ShowGacha(Mikan.CSAGA.GachaListTag.RegularGacha, ConstData.Tables.RegularGachaIDs, false);
	}

	public void ShowActivityGacha()
	{
		ShowGacha(Mikan.CSAGA.GachaListTag.ActivityGacha, ConstData.Tables.ActivityGachaIDs, false);
	}

	private void ShowGacha(Mikan.CSAGA.GachaListTag tag, List<int> list, bool force)
	{
		if(!force && gachaTag == tag) return;

		gachaTag = tag;
		gachaList = list;
		UpdateRemainTime();
		RefreshPromotionList();
	}
	
	private void RefreshGem()
	{
		iapGem.text = GameSystem.Service.PurchaseInfo.IAPGem.ToString();
		freeGem.text = (GameSystem.Service.PurchaseInfo.Gem - GameSystem.Service.PurchaseInfo.IAPGem).ToString();
	}

	private void RefreshDailyButton()
	{
		if(dailyEnabled)
		{
			var articleId = Mikan.CSAGA.ArticleID.Invalid;
			
			switch(gachaTag)
			{
			case Mikan.CSAGA.GachaListTag.RegularGacha:
				articleId = Mikan.CSAGA.ArticleID.RegularGachaDaily;
				break;
			case Mikan.CSAGA.GachaListTag.ActivityGacha:
				articleId = Mikan.CSAGA.ArticleID.ActivityGachaDaily;
				break;
			}
			
			if(articleId != Mikan.CSAGA.ArticleID.Invalid)
			{
				var article = GameSystem.Service.PurchaseInfo[articleId];
				dailyEnabled = (article == null || article.LastPurchase.Date != GameSystem.Service.CurrentTime.Date);
			}
			else
				dailyEnabled = false;
		}
		
		dailyTip.SetActive(dailyEnabled);
		
		dailyDisableMask.alpha = dailyEnabled ? 0 : 0.3f;
		
		dailyDrawObj.collider.enabled = dailyEnabled;
	}

	private void RefreshPromotionList()
	{
		dailyEnabled = SetGachaPrice(dailyDrawObj, dailyDraw, gachaList[DAILY]);
		SetGachaPrice(singleDrawObj, singleDraw, gachaList[SINGLE]);
		SetGachaPrice(multiDrawObj, multiDraw, gachaList[MULTI]);
		dailyDraw.text = string.Format("       x{0}", ConstData.Tables.GachaList[gachaList[DAILY]].n_COIN);
		singleDraw.text = string.Format("       x{0}", ConstData.Tables.GachaList[gachaList[SINGLE]].n_COIN);
		multiDraw.text = string.Format("       x{0}", ConstData.Tables.GachaList[gachaList[MULTI]].n_COIN);

		RefreshDailyButton();

		var list = new List<GachaPromotionData>();

		var gachaId = schedule.GetGachaID(gachaList[DAILY], GameSystem.Service.CurrentTime);

		if(gachaId == 0)
		{
			if(gachaTag == Mikan.CSAGA.GachaListTag.ActivityGacha)
			{
				activityTab.gameObject.SetActive(false);
				ShowRegularGacha();
			}

			return;
		}

		foreach(var item in GameSystem.Service.GachaInfo.GetPromotionList(gachaId))
		{
			var data = new GachaPromotionData{ Origin = item };
			list.Add(data);
		}

		promotionList.current = 0;

		promotionList.Setup(list);
		
		promotionList.OnCurrentChange += OnNavigatorCurrentChange;

		OnNavigatorCurrentChange(promotionList);
	}

	private void RefreshBoost()
	{
		float boost = ConstData.Tables.GetVariablef(Mikan.CSAGA.VariableID.GACHA_BOOST);
		if(boost > 0)
		{
			boostBar.value = (float)GameSystem.Service.PlayerInfo.GachaBoost / boost;
		}
	}

	public void BoostClick()
	{
		MessageBoxContext msg = MessageBox.Show(ConstData.GetSystemText(SystemTextID._456_TIP_LUCKY_DRAW), SystemTextID._457_RATE_TABLE, SystemTextID.BTN_DISABLE);
		msg.title = ConstData.GetSystemText(SystemTextID._455_LUCKY_DRAW);
		msg.callback = OnBoostMsgCallback;
	}

	public void GachaRateMsg()
	{
		switch(gachaTag)
		{
		case Mikan.CSAGA.GachaListTag.RegularGacha:
		{
			MessageBoxContext msg = MessageBox.Show(SystemTextID._458_RATE_TABLE_1);
			msg.title = ConstData.GetSystemText(SystemTextID._457_RATE_TABLE);
			break;
		}
		case Mikan.CSAGA.GachaListTag.ActivityGacha:
		{
			MessageBoxContext msg = MessageBox.Show(SystemTextID._459_RATE_TABLE_2);
			msg.title = ConstData.GetSystemText(SystemTextID._457_RATE_TABLE);
			break;
		}
		}
	}

	public void OnBoostMsgCallback(int index)
	{
		if(index == 0)
			GachaRateMsg();
	}

	private bool SetGachaPrice(GameObject container, UILabel label, int gachaListId)
	{
		var data = ConstData.Tables.GachaList[gachaListId];

		if(data == null)
			container.SetActive(false);
		else
		{
			container.SetActive(true);
			label.text = string.Format("       x{0}", data.n_COIN);
			return true;
		}

		return false;

	}

	void OnNavigatorCurrentChange (Navigator navigator)
	{
		var data = (navigator.DataProvider as IItemDataProvider<GachaPromotionData>)[navigator.Current];

		ChangeCard(data.Origin);
	}

	public void Prev()
	{
		promotionList.Prev();
	}

	public void Next()
	{
		promotionList.Next();
	}

	private void ChangeCard(Mikan.CSAGA.ConstData.Gacha data)
	{
		var card = new CardData();
		card.Origin = GameSystem.Service.GachaInfo.GenerateDummy(data.n_ID);

		card.CardID = card.Origin.ID;

		card.Luck = 1;
		
		Context.Instance.Card = card;

		cardTitle.ApplyChange();

		cardProfile.ApplyChange(card);
	}

	private void UpdateRemainTime()
	{
		remainTime.text = "";
		remainTime.StopAllCoroutines();
		if(gachaTag == Mikan.CSAGA.GachaListTag.ActivityGacha)
			remainTime.StartCoroutine(UpdateRemainTimeRoutine());

	}
	IEnumerator UpdateRemainTimeRoutine()
	{
		while(true)
		{
			remainTime.text = UIUtils.FormatRemainTime(activityEndTime);
			
			yield return new WaitForSeconds(1f);
		}
	}
}
