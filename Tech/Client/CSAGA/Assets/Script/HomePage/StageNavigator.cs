﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class StageNavigator : MonoBehaviour {

	public Navigator navigator;

	// Use this for initialization
	void Start () {
		var list = new List<StageIconData>();

		foreach(var item in ConstData.Tables.Setup.Select(o=>{ return o.n_FUNCTION == Mikan.CSAGA.SetupFunction.Banner; }))
		{
			if(Context.Instance.StageID == StageID.Home || item.n_GACHA == 1)
				list.Add(new StageIconData{ Origin = item });
		}

		navigator.Setup(list);
	}
}
