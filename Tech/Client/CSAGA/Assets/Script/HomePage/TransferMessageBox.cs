﻿using UnityEngine;
using System.Collections;

public class TransferMessageBox : SubmitWindow {
	
	public UIInput publicId;
	public UIInput token;
	#region implemented abstract members of SubmitWindow
	protected override bool OnSubmit ()
	{
		if(string.IsNullOrEmpty(publicId.value) || string.IsNullOrEmpty(token.value)) return false;

		if(!Context.Instance.PublicID.IsEmpty)
		{
			MessageBox.ShowConfirmWindow(ConstData.GetSystemText(SystemTextID._286_TRANSFER_CONFIRM), (index)=>{
				if(index == 0)
				{
					PlayerPrefs.DeleteAll();
					Application.LoadLevel(0);
				}
			});
		}
		else
			GameSystem.Service.AccountTransferBegin(publicId.value, token.value);

		return true;
	}
	#endregion


}
