﻿using UnityEngine;
using System.Collections;

public class Title : MonoBehaviour {
	
	public GameObject loadingIcon;
	public GameObject startIcon;
	public GameObject texture;

	public UILabel publicId;

	public UILabel version;

	public GameObject transferButton;
	public UILabel transferText;

	private UIRoot root;
	private bool isClickEnabled = false;

	void Start()
	{
		root = GetComponentInParent<UIRoot>();
		DontDestroyOnLoad(root.gameObject);
		CameraList.Instance.Add((int)UILayer.Title);

		StartCoroutine(WaitForGameSystemReady());
	}

	void OnDestroy()
	{
		if(GameSystem.IsAvailable && GameSystem.Service != null) 
		{
			GameSystem.Service.OnAccountTransferBegin -= OnAccountTransferBegin;
			GameSystem.Service.OnAccountTransferEnd -= OnAccountTransferEnd;
		}
	}

	public void Show()
	{
		TweenAlpha.Begin(texture, 1, 1);
	}

	public void Hide()
	{
		TweenAlpha.Begin(startIcon, 0.5f, 0).style = UITweener.Style.Once;
	}

	public void OnGameSystemReady()
	{
		isClickEnabled = true;
		loadingIcon.SetActive(false);
		startIcon.SetActive(true);
		transferButton.SetActive(true);

		GameSystem.Service.OnAccountTransferBegin += OnAccountTransferBegin;
		GameSystem.Service.OnAccountTransferEnd += OnAccountTransferEnd;
	}

	public void OnTransfer()
	{
		MessageBox.ShowConfirmWindow("PreloadScene/TransferMessageBox", "", null);
	}

	void OnAccountTransferBegin (string publicId, string name, int rank, int gem)
	{
		var text = string.Format("{0}: {1}\n{2}: {3}\n{4}: {5}\n{6}: {7}",
		                         ConstData.GetSystemText(SystemTextID._250_TRANSFER_ID), FormatID(publicId),
		                         ConstData.GetSystemText(SystemTextID._251_TRANSFER_NAME), name,
		                         ConstData.GetSystemText(SystemTextID._252_TRANSFER_RANK), rank,
		                         ConstData.GetSystemText(SystemTextID._253_TRANSFER_GEM), gem
		);

		var msgBox = MessageBox.ShowConfirmWindow(text, (index)=>{ GameSystem.Service.AccountTransferEnd(index != 0); });
		msgBox.title = ConstData.GetSystemText(SystemTextID._246_BTN_ACCOUNT_TRANFER);
		msgBox.pivot = UIWidget.Pivot.Left;
	}

	void OnAccountTransferEnd ()
	{
		Context.Instance.PublicID.Value = GameSystem.Service.Profile.PublicID;
		publicId.text = string.Format("ID: {0}", FormatID(Context.Instance.PublicID.Value));

		MessageBox.Show(SystemTextID._254_TIP_TRANSFER_SUCC).callback = (index)=>{ OnClick(); };
	}

	void OnClick()
	{
		if(!isClickEnabled) return;

		TweenAlpha.Begin(transferButton, 0.5f, 0).style = UITweener.Style.Once;

		TweenAlpha.Begin(startIcon, 0.5f, 0).style = UITweener.Style.Once;
		Message.Send(MessageID.PlaySound, SoundID.CLICK2);

		isClickEnabled = false;

		GameSystem.Service.Login(Application.platform.ToString(), UIDefine.AppVersion, PatchManager.Instance.Signature, Mikan.Serial.Create(Context.Instance.FavoriteCard.Value));
	}

	void OnLevelWasLoaded(int level)
	{
		switch(Application.loadedLevelName)
		{
		case SceneName.APPSTARTUP:
			Destroy(root.gameObject);
			break;
		case SceneName.GAME:
			DelayDestroy();
			break;
		}
	}

	public void DelayDestroy()
	{
		StartCoroutine(DestroyRoutine());
	}

	IEnumerator DestroyRoutine()
	{
		while(StageManager.Instance.IsSwitching) yield return new WaitForEndOfFrame();

		TweenAlpha.Begin(gameObject, 0.5f, 0);
		yield return new WaitForSeconds(1f);

		Context.Instance.IsPreloading = false;
		UIManager.Instance.DestroyRoot(root.gameObject.layer);
	}

	IEnumerator WaitForGameSystemReady() 
	{
		publicId.text = "";
		version.text = "";


		while(!GameSystem.IsAvailable || !ConstData.IsReady) 
		{
			yield return new WaitForSeconds(0.1f);
		}

		if(!string.IsNullOrEmpty(Context.Instance.PublicID.Value))
			publicId.text = string.Format("ID: {0}", FormatID(Context.Instance.PublicID.Value));
		
		//if(!string.IsNullOrEmpty(PatchManager.Instance.Version))
			//version.text = ConstData.GetSystemText(SystemTextID._227_VERSION_V1, PatchManager.Instance.PatchVersion);
		//else
			version.text = ConstData.GetSystemText(SystemTextID._227_VERSION_V1, UIDefine.AppVersion);

		transferText.text = ConstData.GetSystemText(SystemTextID._246_BTN_ACCOUNT_TRANFER);

#if DEBUG
		if(string.IsNullOrEmpty(Context.Instance.PublicID.Value))
		{
			while(!isClickEnabled || GameSystem.Service.Profile.IsEmpty) yield return new WaitForSeconds(0.1f);

			publicId.text = string.Format("ID: {0}", FormatID(GameSystem.Service.Profile.PublicID));
		}
#endif
	}

	private string FormatID(string id)
	{
		return id.Insert(3, ",").Insert(7, ",");
	}
}
