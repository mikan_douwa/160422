﻿using UnityEngine;
using System.Collections;

public class ExpFloatTip : MonoBehaviour {

	public UILabel exp;
	public UILabel id;
	public UILabel fp;

	// Use this for initialization
	void Start () {
		var next = ConstData.Tables.Player[GameSystem.Service.PlayerInfo.LV + 1];

		var nextExp = next != null ? next.n_TOTALEXP.ToString() : "--";

		exp.text = ConstData.GetSystemText(SystemTextID.RANKEXP_V1_V2, GameSystem.Service.PlayerInfo[Mikan.CSAGA.PropertyID.Exp], nextExp);

		fp.text = ConstData.GetSystemText(SystemTextID._416_FP_VI_V2, GameSystem.Service.PlayerInfo.FP, ConstData.Tables.FPMax);

		id.text = "ID: " + GameSystem.Service.Profile.PublicID.Insert(6, ",").Insert(3, ",");

	}
	

}
