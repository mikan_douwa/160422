﻿using UnityEngine;
using System.Collections;

public class FirstGachaStage : Stage {

	protected override void Initialize ()
	{
		Message.Send(MessageID.PlayEvent, ConstData.Tables.TutorialEventID1);

		base.Initialize ();
	}
}
