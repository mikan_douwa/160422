﻿using UnityEngine;
using System.Collections;

public class UserTreaty : SubmitWindow {

	public UIToggle toggle;

	public void OnToggleChange()
	{
		SetButtonEnabled(0, toggle.value);
	}

	protected override void OnReady ()
	{
		SetButtonEnabled(0, false);
	}

	protected override bool OnSubmit ()
	{
		if(!toggle.value) return false;

		return true;
	}
}
