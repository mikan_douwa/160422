﻿using UnityEngine;
using System.Collections;

public class Menu : MonoBehaviour {

	private static Menu instance;

	private static bool isVisible = false;

	private static bool isActive = true;

	private static bool isShowing = false;

	public GameObject mask;

	void Awake()
	{
		instance = this;
		isShowing = false;
	}

	void Start()
	{
		if(!isVisible) Hide ();
		SetActive(isActive);
	}

	void OnDestroy()
	{
		if(instance == this) 
		{
			instance = null;
			isActive = true;
			isVisible = false;
			isShowing = false;
		}
	}

	public static void Show()
	{
		if(!GameSystem.IsAvailable) return;

		isVisible = true;

		if(instance == null)
		{
			if(!isShowing)
			{
				isShowing = true;
				ResourceManager.Instance.CreatePrefabInstance("GameScene/Menu");
			}
		}
		else 
			instance.gameObject.transform.localPosition = Vector3.zero;
	}



	public static void Hide()
	{
		isVisible = false;

		if(instance != null) 
			instance.gameObject.transform.localPosition = new Vector3(0, -100);

	}

	public static void SetActive(bool active)
	{
		isActive = active;
		if(instance != null) instance.mask.SetActive(!active);
	}
}
