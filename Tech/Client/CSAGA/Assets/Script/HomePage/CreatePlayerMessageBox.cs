﻿using UnityEngine;
using System.Collections;

public class CreatePlayerMessageBox : SubmitWindow {

	public UIInput playerName;
	public UIInput code;

	private bool isSucc;

	protected override void OnReady ()
	{
		GameSystem.Service.OnPlayerCreate += OnPlayerCreate;

#if DEBUG
		code.value = "k5eZmZeX";
#endif
	}

	void OnPlayerCreate ()
	{
		isSucc = true;
	}

	protected override void OnDestroy ()
	{
		base.OnDestroy ();

		if(GameSystem.IsAvailable) 
		{
			GameSystem.Service.OnPlayerCreate -= OnPlayerCreate;
			Application.LoadLevel(0);
		}
	}

	public void OnInputChange()
	{
		playerName.value = UIUtils.RestrictPlayerName(playerName.value);
	}

	protected override bool OnSubmit ()
	{
		if(string.IsNullOrEmpty(playerName.value)) return false;
		GameSystem.Service.PlayerCreate(playerName.value, 0, code.value);
		return true;
	}

	protected override bool IsSucc { get { return isSucc; } }
}
