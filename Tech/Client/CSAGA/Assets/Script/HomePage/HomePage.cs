﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Mikan.CSAGA.Calc;

public class HomePage : Stage {

	public InteractiveChara chara;

	public GameObject giftObj;

	public GameObject giftTipObj;

	public UILabel giftTip;

	public GameObject dailyTipObj;
	
	public UILabel dailyTip;

	public GameObject missionTipObj;

	public GameObject bahamutObj;

	protected override void Initialize ()
	{
		if(!StageManager.Instance.IsAvailable(StageID.CoMission))
			missionTipObj.SetActive(false);

		if(StageManager.Instance.IsAvailable(StageID.Bahamut))
			bahamutObj.SetActive(true);

		Menu.Show();

		ResourceManager.Instance.CreatePrefabInstance(gameObject, "GameScene/StageNavigator");

		var origin = GameSystem.Service.CardInfo[Mikan.Serial.Create(Context.Instance.FavoriteCard.Value)];

		if(origin == null) origin = GameSystem.Service.CardInfo.List.Find(o=>{ return o.Data.n_GIFT != 0; });

		if(origin == null) origin = GameSystem.Service.CardInfo.List[0];

		Context.Instance.Card = new CardData{ Origin = origin, CardID = origin.ID };

		chara.ApplyChange();

		GameSystem.Service.OnGiftList += OnGiftList;
		GameSystem.Service.TrophyList();
		GameSystem.Service.GetDailyInfo();
		GameSystem.Service.GiftList();

		base.Initialize ();
	}

	protected override void OnDestroy ()
	{
		if(GameSystem.IsAvailable) GameSystem.Service.OnGiftList -= OnGiftList;
		base.OnDestroy ();
	}

	void OnGiftList ()
	{

		UpdateCount(giftTipObj, giftTip, GameSystem.Service.GiftInfo.List.Count);

		UpdateCount(dailyTipObj, dailyTip, GameSystem.Service.DailyInfo.Remains);

		if(GameSystem.Service.TrophyInfo.DailyCounter.Progress != Context.Instance.DailyCounter.Value)
		{
			if(GameSystem.Service.TrophyInfo.DailyCounter.Progress > 0)
				MessageBox.Show("GameScene/LoginReward/LoginReward", "").callback = OnLoginRewardClose;
			Context.Instance.DailyCounter.Value = GameSystem.Service.TrophyInfo.DailyCounter.Progress;
		}
		else
		{
			OnLoginRewardClose(0);
		}
	}

	private void UpdateCount(GameObject obj, UILabel label, int count)
	{
		obj.SetActive(count > 0);
		
		if(count > 50)
			label.text = "50+";
		else if(count > 0)
			label.text = count.ToString();
	}

	private void OnLoginRewardClose(int res)
	{
		var dailyFP = GameSystem.Service.CommitDailyFP();

		if(dailyFP > 0)
		{
			var text  = ConstData.Tables.TrophyText[ConstData.Tables.DailyFansTrophyID];
			if(text != null)
			{
				var msgBox = MessageBox.Show(ConstData.Format(text.s_NAME, dailyFP, GameSystem.Service.PlayerInfo.FP, ConstData.Tables.FPMax));
				msgBox.pivot = UIWidget.Pivot.Left;
			}
		}
	}
	
}
