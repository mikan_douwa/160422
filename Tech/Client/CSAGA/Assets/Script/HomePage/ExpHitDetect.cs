﻿using UnityEngine;
using System.Collections;

public class ExpHitDetect : MonoBehaviour {

	public GameObject owner;
	private GameObject tip;

	private bool isPress;
	void OnPress(bool isDown)
	{
		this.isPress = isDown;
		ShowTip();
	}

	private void ShowTip()
	{
		if(isPress)
		{
			if(tip == null)
				ResourceManager.Instance.CreatePrefabInstance(owner, "GameScene/ExpFloatTip", OnCreateComplete);
			else
				UpdatePosition();
		}
		else
		{
			DestroyTip();
		}
	}
	void OnDrag (Vector2 delta)
	{
		if(UIUtils.HitTest(gameObject, Input.mousePosition))
			UpdatePosition();
		else
			DestroyTip();
	}
	void Update()
	{
		UpdatePosition();
	}
	private void OnCreateComplete(GameObject obj)
	{
		tip = obj;
		tip.transform.localScale = new Vector3(0.1f, 0.1f);
		TweenScale.Begin(tip, 0.1f, Vector3.one);
		ShowTip();
	}

	private void UpdatePosition()
	{
		if(tip == null) return;

		var mouse = Input.mousePosition;
		mouse.z = 10f;
		mouse = UIManager.Instance.GetCamera(UILayer.Stage).ScreenToWorldPoint(mouse);

		tip.transform.localPosition = new Vector3(mouse.x > 0 ? -140f : 140f, 0f);
	}

	private void DestroyTip()
	{
		if(tip == null) return;

		var current = tip;

		tip = null;

		var tweener = TweenScale.Begin(current, 0.1f, Vector3.zero);
		var callback = new EventDelegate(()=>{ Destroy(current); });
		callback.oneShot  =true;

		tweener.AddOnFinished(callback);
	}

}
