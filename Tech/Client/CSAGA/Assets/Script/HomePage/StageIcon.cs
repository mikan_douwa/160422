﻿using UnityEngine;
using System.Collections;

public class StageIcon : ItemRenderer<StageIconData> {
	
	public TextureLoader texture;

	public int currentIndex;

	private StageID stageId;

	#region implemented abstract members of ItemRenderer
	protected override void ApplyChange (StageIconData data)
	{
		stageId = (StageID)data.Origin.n_EVENT;

		if(texture.path != data.Origin.s_STRING)
			texture.Unload();
		texture.Load(data.Origin.s_STRING);

		currentIndex = Index;
	}

	#endregion

	void OnClick()
	{
		if(stageId != StageID.None)
		{
			Context.Instance.SwitchByNavigator = true;
			Message.Send(MessageID.SwitchStage, stageId);
		}
		else if(!string.IsNullOrEmpty(data.Origin.s_STRING2) && data.Origin.s_STRING2.StartsWith("http"))
		{
			Application.OpenURL(UIUtils.FormatURL(data.Origin.s_STRING2));
		}

	}

}

public class StageIconData
{
	public Mikan.CSAGA.ConstData.Setup Origin;
}
