﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Headline : UIBase {

	public class Behavior
	{
		public string Title;
		public GameObject TitlePrefab;
		public bool DisableBackButton = false;

		public bool DisableNextButton = true;
		public int NextButtonWidth;
		public string NextButtonText;
		public GameObject NextButtonPrefab;

		public bool IsTemporary = false;

		public bool IsAppendixHidden = false;
	}
	class Context
	{
		public Headline Instance;
		public Behavior Behavior;
		public Behavior Temporary;
	}

	private static Context context = new Context();

	public UILabel title;

	public GameObject container;

	private GameObject content;

	public GameObject backButton;
	public GameObject nextButton;
	public GameObject nextButtonContainer;

	public UILabel nextLabel;

	public GameObject appendixContainer;

	private GameObject nextButtonContent;

	protected override void OnStart ()
	{
		context.Instance = this;

		title.text = "";

		if(context.Behavior == null) 
			Hide ();
		else
			ApplyChange();
	}

	protected override void OnDestroy ()
	{
		base.OnDestroy ();
		if(context.Instance == this) context = new Context();
	}

	protected override void OnChange ()
	{
		var isAppendixHidden = false;

		if(context.Temporary == null)
		{
			if(content != null) 
			{
				Destroy(content);
				content = null;
			}

			if(nextButtonContent != null)
			{
				Destroy(nextButtonContent);
				nextButtonContent = null;
			}

			if(context.Behavior == null)
			{
				title.text = "";
				backButton.SetActive(true);
			}
			else
			{
				isAppendixHidden = context.Behavior.IsAppendixHidden;

				if(string.IsNullOrEmpty(context.Behavior.Title))
					title.text = "";
				else
					title.text = context.Behavior.Title;
				
				if(context.Behavior.TitlePrefab != null)
				{
					content = UIManager.Instance.AddChild(container, context.Behavior.TitlePrefab);
				}

				backButton.SetActive(!context.Behavior.DisableBackButton);

				//nextButton.SetActive(!context.Behavior.DisableNextButton);

				if(!context.Behavior.DisableNextButton)
					//nextButton.SetActive(false);
				//else
				{
					//nextButton.SetActive(true);
					/*

					nextButtonWidget.width = context.Behavior.NextButtonWidth;
					nextButtonWidget.ResetAndUpdateAnchors();

					if(string.IsNullOrEmpty(context.Behavior.NextButtonText))
						nextLabel.text = "";
					else
						nextLabel.text = context.Behavior.NextButtonText;
					  */
					if(context.Behavior.NextButtonPrefab != null)
					{
						nextButtonContent = UIManager.Instance.AddChild(nextButtonContainer, context.Behavior.NextButtonPrefab);
					}
					else
					{
						nextButtonContent = UIManager.Instance.AddChild(nextButtonContainer, Essentials.Instance.nextButtonPrefab);
						if(!string.IsNullOrEmpty(context.Behavior.NextButtonText))
						{
							var label = nextButtonContent.GetComponentInChildren<UILabel>();
							if(label != null) label.text = context.Behavior.NextButtonText;
						}
					}
				}
			}
		}
		else
		{
			isAppendixHidden = context.Temporary.IsAppendixHidden;

			if(string.IsNullOrEmpty(context.Temporary.Title))
				title.text = "";
			else
				title.text = context.Temporary.Title;

			context.Temporary = null;
		}

		if(isAppendixHidden)
			nextButtonContainer.transform.localPosition = new Vector3(0, 200);
		else
			nextButtonContainer.transform.localPosition = Vector3.zero;
	}

	public static void Show(Behavior behavior)
	{
		if(context.Behavior == null || behavior == null || !behavior.IsTemporary)
			context.Behavior = behavior;
		else
			context.Temporary = behavior;

		Show ();
	}

	private static void Show()
	{
		if(context.Instance != null) 
		{
			context.Instance.SetActive(true);
			context.Instance.ApplyChange();
		}
	}

	public static void Hide()
	{
		if(context.Instance != null)
			context.Instance.SetActive(false);
	}

	public static void HideAppendix()
	{
		if(context.Instance != null) context.Instance.nextButtonContainer.transform.localPosition = new Vector3(0, 200);
	}

	public static GameObject Append(GameObject prefab)
	{
		return Append(prefab, null);
	}

	public static GameObject Append(GameObject prefab, string text)
	{
		if(context.Instance == null) return null;

		if(prefab == null) prefab = Essentials.Instance.nextButtonPrefab;
		var go = UIManager.Instance.AddChild(context.Instance.appendixContainer, prefab);
		if(!string.IsNullOrEmpty(text))
		{
			var label = go.GetComponentInChildren<UILabel>();
			if(label != null) label.text = text;
		}
		return go;
	}

	public static GameObject Append(string text)
	{
		return Append(Essentials.Instance.nextButtonPrefab, text);
	}

}
