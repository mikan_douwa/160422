﻿using UnityEngine;
using System.Collections;
using Mikan.CSAGA;

public class Banner : MonoBehaviour {

	public UILabel rank;
	public UIProgressBar expProgress;
	public UILabel crystal;
	public UILabel coin;
	public UILabel gem;
	public UILabel playerName;
	// Use this for initialization
	void Start () 
	{
		crystal.text = GameSystem.Service.PlayerInfo[PropertyID.Crystal].ToString();
		coin.text = GameSystem.Service.PlayerInfo[PropertyID.Coin].ToString();
		gem.text = GameSystem.Service.PlayerInfo[PropertyID.Gem].ToString();
		playerName.text = "[ffca80]" + GameSystem.Service.PlayerInfo.Name;

		UpdateRank();

		GameSystem.Service.PlayerInfo.OnPropertyChange += OnPropertyChange;
		GameSystem.Service.PlayerInfo.OnNameChange += OnNameChange;
	}




	
	void OnDestroy()
	{
		if(GameSystem.IsAvailable)
		{
			GameSystem.Service.PlayerInfo.OnPropertyChange -= OnPropertyChange;
			GameSystem.Service.PlayerInfo.OnNameChange -= OnNameChange;
		}
	}

	void OnPropertyChange (Mikan.CSAGA.Data.PlayerInfo sender, PropertyID id, int oldValue, int newValue)
	{
		switch(id)
		{
		case PropertyID.Crystal:
			crystal.text = newValue.ToString();
			break;
		case PropertyID.Coin:
			coin.text = newValue.ToString();
			break;
		case PropertyID.Gem:
			gem.text = newValue.ToString();
			break;
		case PropertyID.Exp:
			UpdateRank();
			break;
		}
	}

	void OnNameChange (Mikan.CSAGA.Data.PlayerInfo sender, string oldValue, string newValue)
	{
		playerName.text = newValue;
	}

	private void UpdateRank()
	{
		var exp = GameSystem.Service.PlayerInfo[PropertyID.Exp];

		var lv = PlayerUtils.CalcLV(exp);

		rank.text = lv.ToString();

		expProgress.value = PlayerUIUtils.GetRankExpProgress(exp);
	}
}

public static class PlayerUIUtils
{
	public static float GetRankExpProgress(int exp)
	{
		var lv = PlayerUtils.CalcLV(exp);

		var nexLV = ConstData.Tables.Player[lv+1];
		
		if(nexLV != null)
		{
			var total = nexLV.n_EXP;
			var zero = nexLV.n_TOTALEXP - total;
			return (float)(exp - zero) / (float)total;
		}

		return 1;
	}
}
