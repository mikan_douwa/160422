﻿using UnityEngine;
using System.Collections;

public class TweenProgress : UITweener {

	private float from;
	private float to;

	private UIProgressBar progressBar;

	#region implemented abstract members of UITweener

	protected override void OnUpdate (float factor, bool isFinished)
	{
		progressBar.value = Mathf.Lerp(from, to, factor);
	}

	#endregion

	static public TweenProgress Begin (UIProgressBar progressBar, float duration, float to)
	{
		TweenProgress comp = UITweener.Begin<TweenProgress>(progressBar.gameObject, duration);
		comp.progressBar = progressBar;
		comp.from = progressBar.value;
		comp.to = to;
		
		if (duration <= 0f)
		{
			comp.Sample(1f, true);
			comp.enabled = false;
		}

		return comp;
	}

}
