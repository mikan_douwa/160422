﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public enum SortMethod
{
	Serial,
	Rare,
	Kizuna,
	Group,
	Type,
	LV,
	HP,
	Atk,
	Rev,
	Chg,
	Luck,
	ID,
	Counter01,
	Counter02,
	Counter04,
	Counter08,
	Counter16,
}
public enum SortTarget
{
	Card,
	Equipment,
}

public static class SortUtils
{
	public static void ReSort(SortTarget target)
	{
		Context.Instance.SortTarget = target;
		switch(target)
		{
		case SortTarget.Card:
			Message.Send(MessageID.Sort, Context.Instance.SortCard.Value);
			break;
		case SortTarget.Equipment:
			Message.Send(MessageID.Sort, Context.Instance.SortEquipment.Value);
			break;
		}
	}
}

public static class CardSortMethod
{
	class Comparer : IComparer<CardData>
	{
		private Comparison<CardData> comparision;

		public static IComparer<CardData> Create(Comparison<CardData> comparision)
		{
			var inst = new Comparer();
			inst.comparision = comparision;
			return inst;
		}
		#region IComparer implementation

		public int Compare (CardData x, CardData y)
		{
			if(x.ForceSortID != 0 || y.ForceSortID != 0) 
				return x.ForceSortID.CompareTo(y.ForceSortID);

			if(x.Origin == null) return y.Origin == null ? x.CardID.CompareTo(y.CardID) : -1;

			if(y.Origin == null) return 1;

			var res = comparision(x, y);

			if(res == 0)
				res = CompareSerial(x, y);

			return res;
		}

		#endregion


	}
	public static IComparer<CardData> Get(SortMethod method)
	{
		switch(method)
		{
		case SortMethod.Rare:	return Comparer.Create(CompareRare);
		case SortMethod.Kizuna:	return Comparer.Create(CompareKizuna);
		case SortMethod.Type:	return Comparer.Create(CompareType);
		case SortMethod.Group:	return Comparer.Create(CompareGroup);
		case SortMethod.LV:		return Comparer.Create(CompareLV);
		case SortMethod.HP:		return Comparer.Create(CompareHP);
		case SortMethod.Atk:	return Comparer.Create(CompareAtk);
		case SortMethod.Rev:	return Comparer.Create(CompareRev);
		case SortMethod.Chg:	return Comparer.Create(CompareChg);
		case SortMethod.Luck:	return Comparer.Create(CompareLuck);
		case SortMethod.ID:		return Comparer.Create(CompareID);
		case SortMethod.Serial:	
		default:
			return Comparer.Create(CompareSerial);
		}
	}

	private static int CompareSerial(CardData lhs, CardData rhs)
	{
		return lhs.Origin.Serial.CompareTo(rhs.Origin.Serial);
	}

	private static int CompareRare(CardData lhs, CardData rhs)
	{
		return rhs.Origin.Rare.CompareTo(lhs.Origin.Rare);
	}
	private static int CompareKizuna(CardData lhs, CardData rhs)
	{
		return rhs.Origin.Kizuna.CompareTo(lhs.Origin.Kizuna);
	}
	private static int CompareType(CardData lhs, CardData rhs)
	{
		var _lhs = ConstData.Tables.Card[lhs.CardID].n_TYPE;
		var _rhs = ConstData.Tables.Card[rhs.CardID].n_TYPE;
		return _lhs.CompareTo(_rhs);
	}
	private static int CompareGroup(CardData lhs, CardData rhs)
	{
		var _lhs = ConstData.Tables.Card[lhs.CardID].n_GROUP;
		var _rhs = ConstData.Tables.Card[rhs.CardID].n_GROUP;
		return _lhs.CompareTo(_rhs);
	}
	private static int CompareLV(CardData lhs, CardData rhs)
	{
		return rhs.Property.LV.CompareTo(lhs.Property.LV);
	}
	private static int CompareHP(CardData lhs, CardData rhs)
	{
		return rhs.Property.HP.CompareTo(lhs.Property.HP);
	}

	private static int CompareAtk(CardData lhs, CardData rhs)
	{
		return rhs.Property.Atk.CompareTo(lhs.Property.Atk);
	}

	private static int CompareRev(CardData lhs, CardData rhs)
	{
		return rhs.Property.Rev.CompareTo(lhs.Property.Rev);
	}

	private static int CompareChg(CardData lhs, CardData rhs)
	{
		return lhs.Property.Charge.CompareTo(rhs.Property.Charge);
	}

	private static int CompareLuck(CardData lhs, CardData rhs)
	{
		return rhs.Origin.Luck.CompareTo(lhs.Origin.Luck);
	}
	private static int CompareID(CardData lhs, CardData rhs)
	{
		var res = CompareType(lhs, rhs);

		if(res == 0) 
		{
			res = rhs.Origin.ID.CompareTo(lhs.Origin.ID);
			if(res == 0) res = CompareRare(lhs, rhs);
		}

		return res;
	}
}

public static class EquipmentSortMethod
{
	class Comparer : IComparer<EquipmentData>
	{
		private Comparison<EquipmentData> comparision;
		
		public static IComparer<EquipmentData> Create(Comparison<EquipmentData> comparision)
		{
			var inst = new Comparer();
			inst.comparision = comparision;
			return inst;
		}
		#region IComparer implementation
		
		public int Compare (EquipmentData x, EquipmentData y)
		{
			if(x.Origin == null) return y.Origin == null ? 0 : 1;
			
			if(y.Origin == null) return -1;
			
			var res = comparision(x, y);
			
			if(res == 0)
				res = CompareSerial(x, y);
			
			return res;
		}
		
		#endregion
		
		
	}
	public static IComparer<EquipmentData> Get(SortMethod method)
	{

		switch(method)
		{
		case SortMethod.Rare:	return Comparer.Create(CompareRare);
		case SortMethod.HP:		return Comparer.Create(CompareHP);
		case SortMethod.Atk:	return Comparer.Create(CompareAtk);
		case SortMethod.Rev:	return Comparer.Create(CompareRev);
		case SortMethod.Chg:	return Comparer.Create(CompareChg);
		case SortMethod.Counter01:
		case SortMethod.Counter02:
		case SortMethod.Counter04:
		case SortMethod.Counter08:
		case SortMethod.Counter16:
			return Comparer.Create((lhs, rhs)=>{
				var counter = (int)Mathf.Pow(2, (int)(method - SortMethod.Counter01));

				var _lhs = lhs.Origin.CounterType == counter ? lhs.Origin.Counter : 0;
				var _rhs = rhs.Origin.CounterType == counter ? rhs.Origin.Counter : 0;

				var res = _rhs.CompareTo(_lhs);

				if(res != 0) return res;
				
				return CompareSerial(lhs, rhs);
			});
		case SortMethod.Serial:	
		default:
			return Comparer.Create(CompareSerial);
		}

	}
	private static int CompareRare(EquipmentData lhs, EquipmentData rhs)
	{
		return rhs.Rare.CompareTo(lhs.Rare);
	}
	private static int CompareSerial(EquipmentData lhs, EquipmentData rhs)
	{
		return lhs.Origin.Serial.CompareTo(rhs.Origin.Serial);
	}
	private static int CompareHP(EquipmentData lhs, EquipmentData rhs)
	{
		return rhs.Origin.Hp.CompareTo(lhs.Origin.Hp);
	}
	
	private static int CompareAtk(EquipmentData lhs, EquipmentData rhs)
	{
		return rhs.Origin.Atk.CompareTo(lhs.Origin.Atk);
	}
	
	private static int CompareRev(EquipmentData lhs, EquipmentData rhs)
	{
		return rhs.Origin.Rev.CompareTo(lhs.Origin.Rev);
	}
	
	private static int CompareChg(EquipmentData lhs, EquipmentData rhs)
	{
		return rhs.Origin.Chg.CompareTo(lhs.Origin.Chg);
	}
}
