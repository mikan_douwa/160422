﻿using UnityEngine;
using System.Collections;

public class Quest : MonoBehaviour {

	// Use this for initialization
	void Start () {
		Message.AddListener(MessageID.DownloadEnd, OnDownloadEnd);
		PatchManager.Instance.DownloadQuestPatch(QuestManager.Instance.Current);

	}

	private void OnDownloadEnd()
	{
		Message.RemoveListener(MessageID.DownloadEnd, OnDownloadEnd);

		QuestManager.Instance.PlayQuest();
	}

}
