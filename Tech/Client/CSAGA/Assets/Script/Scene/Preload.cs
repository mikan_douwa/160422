﻿using UnityEngine;
using System.Collections;

public class Preload : MonoBehaviour {

	private static bool isCreatingPlayer = false;

	public Title title;

	// Use this for initialization
	void Start () {

		AsyncWork.Reset();

		if(!isCreatingPlayer) title.Show();

		StartCoroutine(Restart());
	}

	void OnDestroy()
	{
		if(GameSystem.Service != null) 
		{
			GameSystem.Service.OnLogin -= OnLogin;
		}
	}

	private IEnumerator Restart()
	{
		yield return StartCoroutine(ConstData.Reload(this));

		yield return StartCoroutine(GameSystem.Restart());

		if(!isCreatingPlayer) Message.Send(MessageID.PlayMusic, "bgm_normal");

		yield return StartCoroutine(ConstData.Reload(this));

		yield return new WaitForSeconds(0.7f);

		GameSystem.EnableService();

		GameSystem.Service.OnLogin += OnLogin;

		if(isCreatingPlayer)
		{
			title.Hide();
			GameSystem.Service.Login(Application.platform.ToString(), UIDefine.AppVersion, PatchManager.Instance.Signature, Mikan.Serial.Create(Context.Instance.FavoriteCard.Value));
		}
		else
			title.OnGameSystemReady();

		isCreatingPlayer = false;
	}

	void OnLogin (double timeDiff)
	{
		if(GameSystem.Service.PlayerInfo.IsAvailable)
		{
			if(Context.Instance.FavoriteCard.Value == 0) Context.Instance.FavoriteCard.Value = GameSystem.Service.CardInfo.List[0].Serial.Value;
			Message.AddListener(MessageID.DownloadEnd, OnDownloadEnd);

			PatchManager.Instance.DownloadUserPatch();
		}
		else
		{
			isCreatingPlayer = true;
			MessageBox.ShowConfirmWindow("PreloadScene/UserTreaty", "", OnUserTreatyClose);
			//MessageBox.Show("PreloadScene/CreatePlayerMessageBox", "").title = ConstData.GetSystemText(SystemTextID.CREATE_PLAYER);
		}
	}

	private void OnUserTreatyClose(int index)
	{
		if(index != 0) 
			MessageBox.ShowConfirmWindow("PreloadScene/UserTreaty", "", OnUserTreatyClose);
		else
			MessageBox.Show("PreloadScene/CreatePlayerMessageBox", "").title = ConstData.GetSystemText(SystemTextID.CREATE_PLAYER);
	}

	private void OnDownloadEnd()
	{
		Message.RemoveListener(MessageID.DownloadEnd, OnDownloadEnd);

		Context.Instance.IsTutorialComplete = GameSystem.Service.QuestInfo[ConstData.Tables.TutorialQuestID].IsCleared;

		if(Context.Instance.PublicID.Value != GameSystem.Service.Profile.PublicID)
			Context.Instance.PublicID.Value = GameSystem.Service.Profile.PublicID;

		if(Context.Instance.IsTutorialComplete)
		{
			if(GameSystem.Service.PlayerInfo.IsFirstGachaDone)
				Message.Send (MessageID.SwitchStage, StageID.Home);
			else
			{
				Context.Instance.IsTutorialComplete = false;
				Message.Send(MessageID.SwitchStage, StageID.FirstGacha);
			}
		}
		else
		{
			var title = FindObjectOfType<Title>();
			if(title != null) title.DelayDestroy();
			QuestManager.Instance.BeginTutorialQuest();
		}
	}

}
