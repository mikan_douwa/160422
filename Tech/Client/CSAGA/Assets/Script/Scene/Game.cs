﻿using UnityEngine;
using System.Collections;

public class Game : MonoBehaviour {

	// Use this for initialization
	void Start () {
		Message.Send(MessageID.PlayMusic, "bgm_normal");
		//Recheck IAP here.
		//IAPRechecker.Instance.Init();
		//IAPRechecker.Instance.CheckIAP();

		//Clear Fight manager AGAIN.
		FightManager.Instance.ClearFight();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
