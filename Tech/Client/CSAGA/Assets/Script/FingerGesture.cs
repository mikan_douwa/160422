using UnityEngine;
using System.Collections.Generic;
[RequireComponent(typeof(Collider))]
public class FingerGesture : MonoBehaviour
{
	public static FingerGesture current;
	
	public FingerGestureData Data  { get; private set; }
	
	public List<EventDelegate> onTap = new List<EventDelegate>();
	public List<EventDelegate> onSwip = new List<EventDelegate>();
	public List<EventDelegate> onLongPress = new List<EventDelegate>();
	public List<EventDelegate> onDragBegin = new List<EventDelegate>();
	public List<EventDelegate> onDragMoved = new List<EventDelegate>();
	public List<EventDelegate> onDragEnd = new List<EventDelegate>();

	public int priority = 0;

	public int minSwipDistance = 0;
	
	void Start()
	{
		Message.AddListener(MessageID.GestureTap, OnFingerGesture);
		Message.AddListener(MessageID.GestureDragBegin, OnFingerGesture);
		Message.AddListener(MessageID.GestureDragMoved, OnFingerGesture);
		Message.AddListener(MessageID.GestureDragEnd, OnFingerGesture);
		Message.AddListener(MessageID.GestureLongPress, OnFingerGesture);
		Message.AddListener(MessageID.GestureSwip, OnFingerGesture);
	}

	void OnDestroy()
	{
		Message.RemoveListener(MessageID.GestureTap, OnFingerGesture);
		Message.RemoveListener(MessageID.GestureDragBegin, OnFingerGesture);
		Message.RemoveListener(MessageID.GestureDragMoved, OnFingerGesture);
		Message.RemoveListener(MessageID.GestureDragEnd, OnFingerGesture);
		Message.RemoveListener(MessageID.GestureLongPress, OnFingerGesture);
		Message.RemoveListener(MessageID.GestureSwip, OnFingerGesture);
	}

	private void OnFingerGesture(IMessage msg)
	{

		if(FingerGestureManager.Instance.TopLayer > gameObject.layer) return;
	
		var data = msg.Data as FingerGestureData;

		if(!UIUtils.HitTest(collider, data.Finger0.StartPosition)) return;

		Data = data;

		var origin = current;

		current = this;
		switch(msg.MessageID)
		{
		case MessageID.GestureTap:			EventDelegate.Execute(onTap);	break;
		case MessageID.GestureDragBegin:	EventDelegate.Execute(onDragBegin);	break;
		case MessageID.GestureDragMoved:	EventDelegate.Execute(onDragMoved);	break;
		case MessageID.GestureDragEnd:		EventDelegate.Execute(onDragEnd);	break;
		case MessageID.GestureLongPress:	EventDelegate.Execute(onLongPress);	break;
		case MessageID.GestureSwip:			
		{
			if(onSwip.Count > 0)
			{
				if(Mathf.Abs(data.SwipDistance.x) > minSwipDistance || Mathf.Abs(data.SwipDistance.y) > minSwipDistance)
					EventDelegate.Execute(onSwip);	
			}

			break;
		}
		default:
			break;
		}

		current = origin;
	}
}

