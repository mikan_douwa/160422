using System;
using UnityEngine;

public class TweenNumberLabel : UITweener
{
	public UILabel CurrentLable;
	public float from = 0f;
	public float to = 1f;
	public string parseText;
	private int mNumber;
	public int number
	{
		get{
			return mNumber;
		}
		set{
			mNumber = value;
			
			if(string.IsNullOrEmpty(parseText))
				CurrentLable.text = value.ToString();
			else
				CurrentLable.text = string.Format(parseText, mNumber);
		}
	}

	#region implemented abstract members of UITweener
	protected override void OnUpdate (float factor, bool isFinished) 
	{
		number = (int)Mathf.Lerp(from, to, factor);
	}
	#endregion
	
	public void SetTextColor(Color textColor)
	{
		CurrentLable.color = textColor;
	}
	
	public void AddToNumberAndReset(int addNumber)
	{
		to += addNumber;
		from = number;
		//Reset();
		Play(true);
	}
	
	static public TweenNumberLabel Begin (GameObject go, float duration, float newNumber)
	{
		TweenNumberLabel comp = UITweener.Begin<TweenNumberLabel>(go, duration);
		
		UILabel label = go.GetComponent<UILabel>();
		
		int fromNumber;

		if(!int.TryParse(label.text, out fromNumber)) fromNumber = 0;
		
		comp.CurrentLable = label;
		
		comp.number = fromNumber;
		comp.from = fromNumber;
		comp.to = newNumber;
		
		if (duration <= 0f)
		{
			comp.Sample(1f, true);
			comp.enabled = false;
		}
		return comp;
	}
}