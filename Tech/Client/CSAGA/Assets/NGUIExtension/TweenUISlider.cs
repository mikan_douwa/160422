using UnityEngine;
public class TweenUISlider : UITweener
{
	public UISlider CurrentUISlider;
	public float from = 0f;
	public float to = 1f;

	public float number
	{
		get{
			return CurrentUISlider.sliderValue;
		}
		set{
			CurrentUISlider.sliderValue = value;
		}
	}

	
	void Awake ()
	{

	}

	#region implemented abstract members of UITweener
	protected override void OnUpdate (float factor, bool isFinished) 
	{
		number = Mathf.Lerp(from, to, factor);
	}
	#endregion
	
	
	
	static public TweenUISlider Begin (GameObject go, float duration, float newNumber)
	{
		TweenUISlider comp = UITweener.Begin<TweenUISlider>(go, duration);
		
		UISlider slider = go.GetComponent<UISlider>();
		comp.CurrentUISlider = slider;

		comp.from = slider.sliderValue;
		comp.to = newNumber;
		
		if (duration <= 0f)
		{
			comp.Sample(1f, true);
			comp.enabled = false;
		}
		return comp;
	}
}