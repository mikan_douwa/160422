using UnityEngine;
using System.Collections;

/// <summary>
/// This is a script used to set UIRoot's manualHeight according to mobile device's width:height ratio
/// Only works if UIRoot's scaling mode is FixedSizeOnMobiles
/// </summary>

[ExecuteInEditMode]
public class UIRootMobile : MonoBehaviour {
	
	public float width = 640;
	public float height = 1136;

	private UIRoot uiRoot;

	public float ActualWidth { get { return ActualHeight * (width/height); } }

	public float ActualHeight {	get { return uiRoot.manualHeight; }	}


	
	void Awake () {

		const float _16_9 = 16f/9f;
		const float _16_10 = 16f/10f;
		const float _3_2 = 3f/2f;
		var target = 0f;
		var current = (float)Screen.height/(float)Screen.width;

		var diff_16_9 = Mathf.Abs(_16_9 - current);
		var diff_16_10 = Mathf.Abs(_16_10 - current);
		var diff_3_2 = Mathf.Abs(_3_2 - current);

		var overSize = false;

		if(current >= _16_9)
		{
			target = _16_9;
			height = 1136;
			overSize = current > target;
		}
		else if(current >= _16_10)
		{
			target = _16_10;
			height = 1024;
		}
		else
		{
			target = _3_2;
			height = 960;
			overSize = target > current;
		}

		/*
		if(diff_16_9 <= diff_16_10 && diff_16_9 <= diff_3_2) 
		{
			target = _16_9;
			height = 1136;
			overSize = current > target;
		}
		else if(diff_16_10 <= diff_16_9 && diff_16_10 <= diff_3_2)
		{
			target = _16_10;
			height = 1024;
		}
		else if(diff_3_2 <= diff_16_9 && diff_3_2 <= diff_16_10)
		{
			target = _3_2;
			height = 960;
			overSize = current < target;
		}
		*/

		//Debug.LogWarning(string.Format("target:{0}|{1}/{2}, current={3}|{4}/{5}, 16:9={6}, 16:10={7}, 3:2={8}", target, height, width, current, Screen.height, Screen.width, _16_9, _16_10, _3_2));

		uiRoot = GetComponent<UIRoot>();

		//NGUIDebug.Log(" h =" + Screen.height + " w =" + Screen.width   );
		
		if (uiRoot.scalingStyle == UIRoot.Scaling.ConstrainedOnMobiles) {
			uiRoot.manualWidth = (int)width;
			uiRoot.manualHeight = (int)height ; 

			if(overSize)
			{
				if (current < target) {
					uiRoot.fitHeight = true;
					uiRoot.fitWidth = false;
				} else if (target < current) {
					uiRoot.fitHeight = false;
					uiRoot.fitWidth = true;
				}
			}
			else
			{
				//uiRoot.fitHeight = true;
				//uiRoot.fitWidth = true;


				if (current < target) {
					//uiRoot.manualHeight = (int)height ; 
					//uiRoot.manualWidth = (int)(height / current);
					uiRoot.fitHeight = true;
					uiRoot.fitWidth = false;
					//NGUIDebug.Log("1.deficeR="+deviceRatio+" h =" + Screen.height + " w =" + Screen.width  + " MH="+uiRoot.manualHeight   );
				} else if (target < current) {
					uiRoot.manualHeight =(int)(height * (current / target));
					uiRoot.fitHeight = false;
					uiRoot.fitWidth = true;
					//NGUIDebug.Log("3.deficeR="+deviceRatio+" h =" + Screen.height + " w =" + Screen.width + " MH="+uiRoot.manualHeight    );
					
				}

			}
			/*
			float targetRatio = width/height;
			float deviceRatio = (float)Screen.width/(float)Screen.height;
			if (Mathf.Abs(targetRatio - deviceRatio) < 0.01f || deviceRatio > targetRatio) {
				//uiRoot.manualHeight = (int)height ; 
				//uiRoot.manualWidth = (int)(height * deviceRatio);
				uiRoot.fitHeight = true;
				uiRoot.fitWidth = false;
				//NGUIDebug.Log("1.deficeR="+deviceRatio+" h =" + Screen.height + " w =" + Screen.width  + " MH="+uiRoot.manualHeight   );
			} else if (deviceRatio < targetRatio) {
				//uiRoot.manualHeight =(int)(height / deviceRatio * targetRatio);
				uiRoot.fitHeight = false;
				uiRoot.fitWidth = true;
				//NGUIDebug.Log("3.deficeR="+deviceRatio+" h =" + Screen.height + " w =" + Screen.width + " MH="+uiRoot.manualHeight    );

			}
			*/
		}

		var panel = GetComponent<UIPanel>();
		panel.baseClipRegion = new Vector4(0, 0, uiRoot.manualWidth, uiRoot.manualHeight);
	}
}
