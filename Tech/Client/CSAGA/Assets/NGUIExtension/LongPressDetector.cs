using UnityEngine;
using System.Collections;

public class LongPressDetector : MonoBehaviour {
	
	public static bool GlobalEnabled = true;
	
	public const float LONG_PRESS_TIME = 0.3f;
	public const float CANCEL_PRESS_DISTANCE = 5;
	
	public GameObject eventReceiver;
	public string longPressFunctionName = "OnLongPress";
	public string longReleaseFunctionName = "";
	public string clickFunctionName = "";
	
	private Vector2 mTouchPosition;
	private Vector2 mReleasePosition;

	private bool triggered = false;
	private bool isLongRelease = false;
	private bool isDrag = false;
	
	void Start ()
	{
		if (eventReceiver == null) {
			eventReceiver = gameObject;
		}
	}
	
	void OnPress (bool isDown)
	{
		if (!GlobalEnabled || !enabled) return;
		
		if (isDown) 
		{
			DoPress();
		}	
		
		else
		{
			DoRelease();				
		}
	}
	
	void OnClick()
	{
		// Note: on click should not affect about long press member.
		if(string.IsNullOrEmpty(clickFunctionName)) return;
			
		eventReceiver.SendMessage(clickFunctionName);
	}
	
	public void SetClickFuncName(string name)
	{
		clickFunctionName = name;
	}
	
	void DoPress()
	{
		mTouchPosition = UICamera.lastTouchPosition;
		mReleasePosition = mTouchPosition;
		
		Invoke("LongPressMethod", LONG_PRESS_TIME);
		
		UICamera.currentTouch.clickNotification = UICamera.ClickNotification.None;
		isDrag = false;
		isLongRelease = false;
		triggered = false;
	}
	
	void DoRelease()
	{
		if(triggered)// we think now is long press
		{
			//Debug.Log("enter LongReleaseMethod");
			isLongRelease = true;
			
			if(string.IsNullOrEmpty(longReleaseFunctionName)) return;
			
			eventReceiver.SendMessage(longReleaseFunctionName);
		}
		else
		{
			//Debug.Log("common release");
			CancelInvoke("LongPressMethod");
			UICamera.currentTouch.clickNotification = UICamera.ClickNotification.BasedOnDelta;	
		}
	}
	
	//If move too far when Pressing, cancel it
	void OnDrag (Vector2 delta)
	{
		mReleasePosition = UICamera.lastTouchPosition;
		float deltaX = mReleasePosition.x - mTouchPosition.x;
		float deltaY = mReleasePosition.y - mTouchPosition.y;
		if(Mathf.Abs(deltaX) + Mathf.Abs(deltaY) > CANCEL_PRESS_DISTANCE)
		{
			//Debug.Log("enter Drag");
			isDrag = true;
		}
	}
	
	void LongPressMethod()
	{
		if(isDrag)
			return;
		
		//Debug.Log("enter LongPressMethod");
		
		if(string.IsNullOrEmpty(longPressFunctionName)) return;
		
		//Play Sound
		//AudioEvent evt = new AudioEvent(){ Type = AudioEventType.Play, Channel = AudioChannel.click };
		//GameEventManager.DispatchEvent(evt);
		
		//Send Message
		eventReceiver.SendMessage(longPressFunctionName, SendMessageOptions.DontRequireReceiver);
		triggered = true;
	}
}
