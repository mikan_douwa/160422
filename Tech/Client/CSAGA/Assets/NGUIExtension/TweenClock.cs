﻿using UnityEngine;


[AddComponentMenu("NGUI/Tween/Tween Clock")]
public class TweenClock : UITweener
{
	public float from;
	public float to;
	
	Transform mTrans;
	
	public Transform cachedTransform { get { if (mTrans == null) mTrans = transform; return mTrans; } }
	public Quaternion rotation { get { return cachedTransform.localRotation; } set { cachedTransform.localRotation = value; } }
	
	protected override void OnUpdate (float factor, bool isFinished)
	{
		rotation = Quaternion.Euler(rotation.eulerAngles.x, rotation.eulerAngles.y, from * (1f - factor) - to * factor);

	}
	
	/// <summary>
	/// Start the tweening operation.
	/// </summary>
	
	static public TweenClock Begin (GameObject go, float duration, float angle)
	{
		TweenClock comp = UITweener.Begin<TweenClock>(go, duration);
		comp.from = comp.rotation.eulerAngles.z;
		comp.to = angle;
		
		if (duration <= 0f)
		{
			comp.Sample(1f, true);
			comp.enabled = false;
		}
		return comp;
	}
}
