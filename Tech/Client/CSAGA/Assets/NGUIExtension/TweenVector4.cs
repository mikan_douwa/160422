//----------------------------------------------
//            NGUI: Next-Gen UI kit
// Copyright © 2011-2012 Tasharen Entertainment
//----------------------------------------------

using UnityEngine;
using System;

/// <summary>
/// Tween a value, and can registry a callback function when update
/// </summary>

[AddComponentMenu("NGUI/Tween/TweenVector4")]
public class TweenVector4 : UITweener
{
	public Vector4 from;
	public Vector4 to;
	
	public Action<Vector4> OnValueChanged;
	public string OnValueChangedFunName;
	
	private Vector4 mCurrentValue;
	public Vector4 CurrentValue 
	{
		protected set
		{
			mCurrentValue = value;
			
			if(OnValueChanged != null)
				OnValueChanged(CurrentValue);
			
			if (eventReceiver != null && !string.IsNullOrEmpty(OnValueChangedFunName))
				eventReceiver.SendMessage(
					OnValueChangedFunName, this, SendMessageOptions.DontRequireReceiver);
		}
		get
		{
			return mCurrentValue;
		}
	}
	
	override protected void OnUpdate (float factor, bool isFinished) 
	{
		CurrentValue = from * (1f - factor) + to * factor;
	}

	/// <summary>
	/// Start the tweening operation.
	/// </summary>
	static public TweenVector4 Begin (GameObject go, float duration, Vector4 newValue)
	{
		TweenVector4 comp = UITweener.Begin<TweenVector4>(go, duration);

		comp.from = comp.CurrentValue;
		comp.to = newValue;

		if (duration <= 0f)
		{
			comp.Sample(1f, true);
			comp.enabled = false;
		}
		return comp;
	}
}