//----------------------------------------------
//            NGUI: Next-Gen UI kit
// Copyright © 2011-2012 Tasharen Entertainment
//----------------------------------------------

using UnityEngine;

/// <summary>
/// Tween the object's position.
/// </summary>

[AddComponentMenu("NGUI/Tween/TweenV")]
public class TweenV : UITweener
{
	public float from;
	public float to;
	public bool isRandom = false;
	bool mHasChanged = false;

	UITexture mUITexture;
	
	public UITexture CachedUITexture { get { if (mUITexture == null) mUITexture = GetComponent<UITexture>(); return mUITexture; } }
	public Rect UVRect { get { return CachedUITexture.uvRect; } set { CachedUITexture.uvRect = value; } }

	override protected void OnUpdate (float factor, bool isFinished) {
		if (isRandom && !mHasChanged) {
			to = UnityEngine.Random.Range(-to, to);
			mHasChanged = true;
		}
		
		float newPoint = from * (1f - factor) + to * factor;
		
		UVRect = new Rect(UVRect.x, newPoint, UVRect.width, UVRect.height);
		
		if (isFinished) mHasChanged = false;
	}

	/// <summary>
	/// Start the tweening operation.
	/// </summary>

	static public TweenV Begin (GameObject go, float duration, float newPoint)
	{
		TweenV comp = UITweener.Begin<TweenV>(go, duration);

		comp.from = comp.UVRect.y;
		comp.to = newPoint;

		if (duration <= 0f)
		{
			comp.Sample(1f, true);
			comp.enabled = false;
		}
		return comp;
	}
}