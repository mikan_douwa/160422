﻿using UnityEngine;


[AddComponentMenu("NGUI/Tween/Tween Clock Y")]
public class TweenClockY : UITweener
{
	public float from;
	public float to;
	
	Transform mTrans;
	
	public Transform cachedTransform { get { if (mTrans == null) mTrans = transform; return mTrans; } }
	public Quaternion rotation { get { return cachedTransform.localRotation; } set { cachedTransform.localRotation = value; } }
	
	protected override void OnUpdate (float factor, bool isFinished)
	{
		rotation = Quaternion.Euler(rotation.eulerAngles.x, from * (1f - factor) - to * factor, rotation.eulerAngles.z);

	}
	
	/// <summary>
	/// Start the tweening operation.
	/// </summary>
	
	static public TweenClockY Begin (GameObject go, float duration, float angle)
	{
		TweenClockY comp = UITweener.Begin<TweenClockY>(go, duration);
		comp.from = comp.rotation.eulerAngles.y;
		comp.to = angle;
		
		if (duration <= 0f)
		{
			comp.Sample(1f, true);
			comp.enabled = false;
		}
		return comp;
	}
}
