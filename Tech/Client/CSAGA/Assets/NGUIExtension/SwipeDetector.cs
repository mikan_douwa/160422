using UnityEngine;
using System.Collections;

public class SwipeDetector : MonoBehaviour {
	
	public GameObject eventReceiver;
	public string slideLeftFunctionName = "OnSlideLeft";
	public string slideRightFunctionName = "OnSlideRight";
	public float minSwipeDist = 50;
	
	private Vector2 mLastTouchPosition;
	private bool mHasMoved = false;
	
	void Start ()
	{
		if (eventReceiver == null) {
			eventReceiver = gameObject;
		}
	}
	
	void OnPress (bool isDown)
	{
		if (!enabled) return;
		
		if (isDown) {
			mHasMoved = false;
			mLastTouchPosition = UICamera.lastTouchPosition;
		} else {
			if (mHasMoved) {
				float swipeDist = UICamera.lastTouchPosition.x - mLastTouchPosition.x;
				
				if (Mathf.Abs(swipeDist) >= minSwipeDist) {
					if (swipeDist < 0) {
						eventReceiver.SendMessage(slideLeftFunctionName);
					} else {
						eventReceiver.SendMessage(slideRightFunctionName);
					}
				}
				
				mHasMoved = false;
			}
		}
	}
	
	void OnDrag (Vector2 delta)
	{
		if (!enabled) return;
		
		mHasMoved = true;
	}
}
