//----------------------------------------------
//            NGUI: Next-Gen UI kit
// Copyright © 2011-2012 Tasharen Entertainment
//----------------------------------------------

using UnityEngine;
using System;

/// <summary>
/// Tween a value, and can registry a callback function when update
/// </summary>

[AddComponentMenu("NGUI/Tween/TweenColorValue")]
public class TweenColorValue : UITweener
{
	public Color from;
	public Color to;
	
	public Action<Color> OnValueChanged;
	public string OnValueChangedFunName;
	
	private Color mCurrentValue;
	public Color CurrentValue 
	{
		protected set
		{
			mCurrentValue = value;
			
			if(OnValueChanged != null)
				OnValueChanged(CurrentValue);
			
			if (eventReceiver != null && !string.IsNullOrEmpty(OnValueChangedFunName))
				eventReceiver.SendMessage(
					OnValueChangedFunName, this, SendMessageOptions.DontRequireReceiver);
		}
		get
		{
			return mCurrentValue;
		}
	}
	
	override protected void OnUpdate (float factor, bool isFinished) 
	{
		CurrentValue = from * (1f - factor) + to * factor;
	}

	/// <summary>
	/// Start the tweening operation.
	/// </summary>
	static public TweenColorValue Begin (GameObject go, float duration, Color newValue)
	{
		TweenColorValue comp = UITweener.Begin<TweenColorValue>(go, duration);

		comp.from = comp.CurrentValue;
		comp.to = newValue;

		if (duration <= 0f)
		{
			comp.Sample(1f, true);
			comp.enabled = false;
		}
		return comp;
	}
}
