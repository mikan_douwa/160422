using UnityEngine;
public class TweenShowLabel : UITweener
{
	
	public UILabel CurrentLable;
	public string CurrentText = "";
	public int from = 0;
	public int to =  0;
	
	private int mCurrentIndex;
	public int CurrentIndex
	{
		get{
			return mCurrentIndex;
		}
		set{
			mCurrentIndex = value;
			string str = CurrentText.Substring(0, CurrentIndex);
			CurrentLable.text = str;
		}
	}
 
	protected override void OnUpdate (float factor, bool isFinished)
	{
		CurrentIndex = (int)Mathf.Lerp(from, to, factor);
	}
	
	static public TweenShowLabel Begin (GameObject go, float duration, string targetText)
	{
		TweenShowLabel comp = UITweener.Begin<TweenShowLabel>(go, duration);
		
		UILabel label = go.GetComponent<UILabel>();

		
		int fromNumber = 0;
		int toNumber = targetText.Length;
		
		comp.CurrentLable = label;
		comp.CurrentText = targetText;
		
		comp.CurrentIndex = fromNumber;
		comp.from = fromNumber;
		comp.to = targetText.Length;
		
		if (duration <= 0f)
		{
			comp.Sample(1f, true);
			comp.enabled = false;
		}
		return comp;
	}
}
