//----------------------------------------------
//            NGUI: Next-Gen UI kit
// Copyright © 2011-2012 Tasharen Entertainment
//----------------------------------------------

using UnityEngine;

/// <summary>
/// Tween nothing, usually for animation prefab to destroy self.
/// </summary>

[AddComponentMenu("NGUI/Tween/TweenNull")]
public class TweenNull : UITweener
{

	override protected void OnUpdate (float factor, bool isFinished) {
		// do nothing
	}

	/// <summary>
	/// Start the tweening operation.
	/// </summary>
	static public TweenNull Begin (GameObject go, float duration)
	{
		TweenNull comp = UITweener.Begin<TweenNull>(go, duration);
		
		return comp;
	}
}