//----------------------------------------------
//            NGUI: Next-Gen UI kit
// Copyright © 2011-2012 Tasharen Entertainment
//----------------------------------------------

using UnityEngine;
using System;

/// <summary>
/// Tween a value, and can registry a callback function when update
/// </summary>

[AddComponentMenu("NGUI/Tween/TweenFloatValue")]
public class TweenFloatValue : UITweener
{
	public float from;
	public float to;
	
	public Action<float> OnValueChanged;
	public string OnValueChangedFunName;
	
	private float mCurrentValue = 0;
	public float CurrentValue 
	{
		protected set
		{
			mCurrentValue = value;
			
			if(OnValueChanged != null)
				OnValueChanged(CurrentValue);
			
			if (eventReceiver != null && !string.IsNullOrEmpty(OnValueChangedFunName))
				eventReceiver.SendMessage(
					OnValueChangedFunName, this, SendMessageOptions.DontRequireReceiver);
		}
		get
		{
			return mCurrentValue;
		}
	}
	
	override protected void OnUpdate (float factor, bool isFinished) 
	{
		CurrentValue = from * (1f - factor) + to * factor;
	}

	/// <summary>
	/// Start the tweening operation.
	/// </summary>
	static public TweenFloatValue Begin (GameObject go, float duration, float newValue)
	{
		TweenFloatValue comp = UITweener.Begin<TweenFloatValue>(go, duration);

		comp.from = comp.CurrentValue;
		comp.to = newValue;

		if (duration <= 0f)
		{
			comp.Sample(1f, true);
			comp.enabled = false;
		}
		return comp;
	}

	static public TweenFloatValue Begin (GameObject go, float duration, float beginValue, float newValue)
	{
		TweenFloatValue comp = UITweener.Begin<TweenFloatValue>(go, duration);
		
		comp.from = beginValue;
		comp.to = newValue;
		
		if (duration <= 0f)
		{
			comp.Sample(1f, true);
			comp.enabled = false;
		}
		return comp;
	}
}