
using UnityEngine;

/// <summary>
/// Tween the camera's size.
/// </summary>

[AddComponentMenu("NGUI/Tween/TweenCameraSize")]
public class TweenCameraSize : UITweener
{
	public float from;
	public float to;

	Camera mCam;
	public Camera camera{
		get{
			if(mCam == null)
				mCam = GetComponent<Camera>();
			return mCam;
		}
		set{
			mCam = value;
		}
	}
	
	override protected void OnUpdate (float factor, bool isFinished) {

		float newSize = from * (1f - factor) + to * factor;
		
		camera.orthographicSize = newSize;
		
	}

	/// <summary>
	/// Start the tweening operation.
	/// </summary>

	static public TweenCameraSize Begin (GameObject go, float duration, float newSize)
	{
		TweenCameraSize comp = UITweener.Begin<TweenCameraSize>(go, duration);

		comp.from = comp.camera.orthographicSize;
		comp.to = newSize;

		if (duration <= 0f)
		{
			comp.Sample(1f, true);
			comp.enabled = false;
		}
		return comp;
	}
}