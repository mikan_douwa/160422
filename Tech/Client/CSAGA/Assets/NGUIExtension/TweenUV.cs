//----------------------------------------------
//            NGUI: Next-Gen UI kit
// Copyright © 2011-2012 Tasharen Entertainment
//----------------------------------------------

using UnityEngine;

/// <summary>
/// Tween the object's position.
/// </summary>

[AddComponentMenu("NGUI/Tween/TweenUV")]
public class TweenUV : UITweener
{
	public Vector2 from;
	public Vector2 to;

	UITexture mUITexture;
	
	public UITexture CachedUITexture { get { if (mUITexture == null) mUITexture = GetComponent<UITexture>(); return mUITexture; } }
	public Rect UVRect { get { return CachedUITexture.uvRect; } set { CachedUITexture.uvRect = value; } }

	override protected void OnUpdate (float factor, bool isFinished) {		
		Vector2 newPoint = from * (1f - factor) + to * factor;
		
		UVRect = new Rect(newPoint.x, newPoint.y, UVRect.width, UVRect.height);
	}

	/// <summary>
	/// Start the tweening operation.
	/// </summary>

	static public TweenUV Begin (GameObject go, float duration, Vector2 newPoint)
	{
		TweenUV comp = UITweener.Begin<TweenUV>(go, duration);

		comp.from = new Vector2(comp.UVRect.x, comp.UVRect.y);
		comp.to = newPoint;

		if (duration <= 0f)
		{
			comp.Sample(1f, true);
			comp.enabled = false;
		}
		return comp;
	}
}