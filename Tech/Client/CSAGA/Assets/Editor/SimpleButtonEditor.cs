﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(SimpleButton))]
public class SimpleButtonEditor : Editor
{
	public override void OnInspectorGUI ()
	{
		serializedObject.Update();
		
		NGUIEditorTools.DrawProperty("Priority", serializedObject, "priority");
		
		serializedObject.ApplyModifiedProperties();

		NGUIEditorTools.SetLabelWidth(80f);
		var obj = target as SimpleButton;
		
		NGUIEditorTools.DrawEvents("On Click", obj, obj.onClick);

	}
}