﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using System.IO;
using Mikan;

public class AssetBundleTool : MonoBehaviour {

	private const string PATCH_ROOT = "Assets/__patch";

	private static string[] IGNORE_LIST = new string[]
	{

	};

	private static string[] FIRST_PACK_PREFIX = new string[]
	{
		"metadata",
		"CSAGA_",
		"Texture/BG/bg_main",
		"Texture/BG/bg_map",
		"Texture/BG/bg_summon",
		"Sound/se_",
		"Sound/BGM/bgm_normal",
		"Sound/BGM/bgm_battle",
		"Sound/BGM/bgm_battle2"

	};

	class FileInfo
	{
		public string Path;
		public uint CRC;
		public int Flag;
	}

	class PathInfo
	{
		public string FileName;
		public string Directory;
		public string FullName;
		public string Path;
	}

	private static List<FileInfo> files;
	
	private static string OutputDir
	{
#if UNITY_IPHONE
		get { return Directory.GetCurrentDirectory() + "/patch/ios"; }
#elif UNITY_ANDROID
		get { return Directory.GetCurrentDirectory() + "/patch/android"; }
#else
		get { return Directory.GetCurrentDirectory() + "/patch"; }
#endif
	}

	private static BuildTarget BuildTarget
	{
#if UNITY_IPHONE
		get { return BuildTarget.iPhone; }
#elif UNITY_ANDROID
		get { return BuildTarget.Android; }
#else
		get { return BuildTarget.StandaloneWindows; }
#endif
	}

	private static PathInfo ParsePath(string file)
	{
		return ParsePath(PATCH_ROOT, file);
	}

	private static PathInfo ParsePath(string root, string file)
	{
		foreach(var item in IGNORE_LIST)
		{
			if(file.Contains(item)) return null;
		}
		file = file.Replace("\\", "/");
		var path = file.Substring(0, file.LastIndexOf("/")).Replace(root, "");
		
		var filename = file.Substring(file.LastIndexOf("/") + 1);
		
		if(filename.StartsWith(".")) return null;
		
		filename = filename.Substring(0, filename.LastIndexOf("."));
		
		var directory = OutputDir + path;
		
		if(!Directory.Exists(directory)) Directory.CreateDirectory(directory);
		
		var filePath = path + "/" + filename + ".unity";
		
		var fullname = directory + "/" + filename + ".unity";
		
		return new PathInfo{ FileName = filename, Directory = directory, FullName = fullname, Path = filePath };
	}

	private static void BuildAssetBundle(string file)
	{
		BuildAssetBundle(file, AssetDatabase.LoadMainAssetAtPath(file), null);
	}

	private static void BuildAssetBundle(string file, UnityEngine.Object[] assets, string[] names)
	{
		try{
			var info = ParsePath(file);// file.Substring(0, file.LastIndexOf("/")).Replace(PATCH_ROOT, "");

			if(info == null) return;
			
			BuildAssetBundleOptions option = BuildAssetBundleOptions.CompleteAssets | BuildAssetBundleOptions.CollectDependencies;

			var fileInfo = new FileInfo();
			fileInfo.Path = info.Path;
			
			if (!BuildPipeline.BuildAssetBundleExplicitAssetNames(
			                                    assets,
												names,
												info.FullName,
			                                    out fileInfo.CRC,
			                                    option,
			                                    BuildTarget)) 
				Debug.LogWarning(string.Format("build asset bundle failed, file = {0}", file));
			
			files.Add(fileInfo);
		}
		catch
		{
			Debug.LogError(string.Format("build asset bundle failed, file = {0}", file));
			throw;
		}
	}

	private static void BuildAssetBundle(string file, UnityEngine.Object mainAsset, UnityEngine.Object[] assets)
	{
		BuildAssetBundle(PATCH_ROOT, file, mainAsset, assets);
	}

	private static void BuildAssetBundle(string root, string file, UnityEngine.Object mainAsset, UnityEngine.Object[] assets)
	{
		try{
			var info = ParsePath(root, file);
			
			if(info == null) return;
			
			BuildAssetBundleOptions option = BuildAssetBundleOptions.CompleteAssets | BuildAssetBundleOptions.CollectDependencies;

			var fileInfo = new FileInfo();
			fileInfo.Path = info.Path;

			if (!BuildPipeline.BuildAssetBundle(mainAsset,
			                                    assets,
			                                    info.FullName,
			                                    out fileInfo.CRC,
			                                    option,
			                                    BuildTarget)) 
				Debug.LogWarning(string.Format("build asset bundle failed, file = {0}", file));

			files.Add(fileInfo);
		}
		catch
		{
			Debug.LogError(string.Format("build asset bundle failed, file = {0}, mainAsset = {1}, assets = {2}", file, mainAsset, assets));
			throw;
		}
	}

	private static void CopyStreamingAssets()
	{
		var root = Application.streamingAssetsPath;

		foreach(var file in SelectAllAsset(root))
		{
			var filename = file.Substring(file.LastIndexOf('/') + 1);

			var tmp = PATCH_ROOT + "/_tmp_file.bytes";

			if(File.Exists(tmp)) File.Delete(tmp);

			File.Copy(file, tmp);

			AssetDatabase.ImportAsset(tmp, ImportAssetOptions.ForceUpdate);

			var mainAsset = AssetDatabase.LoadAssetAtPath(tmp, typeof(TextAsset)) as TextAsset;

			BuildAssetBundle(root, file, mainAsset, null);

			AssetDatabase.DeleteAsset(tmp);

		}
	}

	private static IEnumerable<string> SelectAllAsset(string directory)
	{

		return ResourceManager.SelectAllFile(directory, o=>{

			if(o.EndsWith(".meta")) return false;
			foreach(var ignore in IGNORE_LIST)
			{
				if(o.Contains(ignore)) return false;
			}
			return true;

		});
	}

	private static IEnumerable<UnityEngine.Object> LoadAllAsset(string directory)
	{
		foreach(var item in SelectAllAsset(directory)) 
		{
			foreach(var obj in AssetDatabase.LoadAllAssetsAtPath(item)) yield return obj;
		}
	}

	private static void PackageAllAsset(string directory, string mainAssetTrait, bool explicitName)
	{
		string mainAssetPath = null;
		UnityEngine.Object mainAsset = null;
		List<UnityEngine.Object> assets = new List<Object>();

		if(explicitName)
		{
			List<string> names = new List<string>();
			
			foreach(var item in SelectAllAsset(directory))
			{
				if(item.Contains(mainAssetTrait))
					mainAssetPath = item;
				
				var asset = AssetDatabase.LoadMainAssetAtPath(item);
				assets.Add(asset);

				var name = item.Replace(directory + "/", "");
				if(name.EndsWith(".bytes")) name = name.Substring(0, name.LastIndexOf(".bytes"));

				names.Add(name);
				
			}

			if(mainAssetPath == null)
			{
				Debug.LogWarning(string.Format("load main asset failed, directory = {0}, mainAssetTrait = {1}", directory, mainAssetTrait));
				return;
			}
			
			BuildAssetBundle(mainAssetPath, assets.ToArray(), names.ToArray());
		}
		else
		{
			foreach(var item in SelectAllAsset(directory))
			{
				if(item.Contains(mainAssetTrait))
				{
					mainAssetPath = item;
					mainAsset = AssetDatabase.LoadMainAssetAtPath(mainAssetPath);
				}
				else
				{
					foreach(var asset in AssetDatabase.LoadAllAssetsAtPath(item)) assets.Add(asset);
				}
			}
			
			if(mainAsset == null)
			{
				Debug.LogWarning(string.Format("load main asset failed, directory = {0}, mainAssetTrait = {1}", directory, mainAssetTrait));
				return;
			}
			
			BuildAssetBundle(mainAssetPath, mainAsset, assets.ToArray());
		}
	}

	private static void PackageAssetByDirectory(string directory, string mainAssetTrait, bool explicitName)
	{
		foreach(var item in Directory.GetDirectories(directory)) PackageAllAsset(item, mainAssetTrait, explicitName);
	}

	[MenuItem("AssetBundle/ExportMetadata")]
	public static void ExportMetadata()
	{
		using(var output = File.Create(PATCH_ROOT + "/metadata.txt"))
		{
			using(var writer = new StreamWriter(output))
			{
				var texture = new Texture2D(2,2);
				foreach(var item in Directory.GetDirectories(PATCH_ROOT))
				{
					foreach(var file in SelectAllAsset(item)) 
					{
						if(!file.EndsWith(".png")) continue;
						texture.LoadImage(File.ReadAllBytes(file));
						var _file = file.Replace("\\", "/");
						var path = _file.Replace(PATCH_ROOT + "/Texture/", "").Replace(".png", "");

						writer.Write("{0}|{1}|{2}|{3}\n", 1, path, texture.width, texture.height);
					}
				}
			}
		}

		Debug.Log("Done!");
	}

	[MenuItem("AssetBundle/Export")]
	public static void Export()
	{
		files = new List<FileInfo>();
		
		CopyStreamingAssets();

		foreach(var file in SelectAllAsset(PATCH_ROOT)) BuildAssetBundle(file);

		/*
		foreach(var item in Directory.GetDirectories(PATCH_ROOT))
		{
			foreach(var file in SelectAllAsset(item)) BuildAssetBundle(file);
		}
		*/

		ExportFileList(files);
	}

	
	[MenuItem("AssetBundle/Export ConstData")]
	public static void ExportConstData()
	{
		files = new List<FileInfo>();
		
		CopyStreamingAssets();

		foreach(var item in files)
		{
			if(item.Path.StartsWith("/")) item.Path = item.Path.Substring(1);
		}
			
		if(File.Exists(OutputDir + "/file_list.txt"))
		{
			var oldList = File.ReadAllLines(OutputDir + "/file_list.txt");

			foreach(var item in oldList)
			{
				
				var split = item.Split('|');
				
				var file = new FileInfo{ Path = split[0], CRC = uint.Parse(split[1]), Flag = int.Parse(split[2]) };

				bool isExists = false;

				foreach(var current in files)
				{
					if(file.Path != current.Path) continue;
					isExists = true;
					break;
				}

				if(!isExists) files.Add(file);
			}
		}

		ExportFileList(files);
	}

	private static void ExportFileList(List<FileInfo> files)
	{
		WriteFlag(files);
		
		using(var file = File.Create(OutputDir + "/file_list.txt"))
		{
			using(var writer = new StreamWriter(file))
			{
				foreach(var item in files) 
				{
					writer.Write("{0}|{1}|{2}\n", item.Path, item.CRC, item.Flag);
				}
			}
		}

		using(var file = File.Create(OutputDir + "/signature.txt"))
		{
			using(var writer = new StreamWriter(file))
			{
				writer.Write("{0}", System.DateTime.Now.Ticks);
			}
		}
		
		Debug.Log("Done!");
	}

	private static void WriteFlag(List<FileInfo> files)
	{
		foreach(var item in files)
		{
			if(item.Path.StartsWith("/")) item.Path = item.Path.Substring(1);
			
			item.Flag = 0;

			foreach(var prefix in FIRST_PACK_PREFIX)
			{
				if(item.Path.StartsWith(prefix))
				{
					item.Flag = 1;
					break;
				}
			}
		}

		/*
		using(var stream = new MemoryStream(LoadBytes("Assets/StreamingAssets/CSAGA_CONSTDATA.pak")))
		{
			var reader = new ConstDataReader();
			reader.Load(stream);

			var provider = reader.Combine();

			var table = provider.GetTable("CARD_DATA").Convert<Mikan.CSAGA.ConstData.CardData>();

			var ignoreList = new List<string>();

			foreach(var item in table.Select()) 
			{
				foreach(var file in ConstData.SelectRelated(item)) 
				{
					if(!ignoreList.Contains(file)) ignoreList.Add(file);
				}
			}

			var itemTable = provider.GetTable("ITEM").Convert<Mikan.CSAGA.ConstData.Item>();

			foreach(var item in itemTable.Select()) 
			{
				foreach(var file in ConstData.SelectRelated(item)) 
				{
					if(!ignoreList.Contains(file)) ignoreList.Add(file);
				}
			}

			foreach(var item in files)
			{
				if(item.Path.StartsWith("/")) item.Path = item.Path.Substring(1);

				item.Flag = 1;

				if(!item.Path.StartsWith("Skeleton/item_") && !item.Path.StartsWith("Texture/ICON/IB_") && !item.Path.StartsWith("Texture/ICON/IBT_") && !item.Path.StartsWith("Texture/CARD/CARD_BORDER_") && 
				   (item.Path.StartsWith("Skeleton/") || 
				 	item.Path.StartsWith("STAND/") || 
				 	item.Path.StartsWith("Texture/CARD/") ||
				 	item.Path.StartsWith("Texture/ICON/") ||
				 	item.Path.StartsWith("Texture/BG/") || 
				 	ignoreList.Contains(item.Path)))
					item.Flag = 0;

			}

			reader.Dispose();
		}
		*/

	}

	private static byte[] LoadBytes(string file)
	{
		var tmp = PATCH_ROOT + "/_tmp_file.bytes";
		File.Copy(file, tmp);
		AssetDatabase.ImportAsset(tmp, ImportAssetOptions.ForceUpdate);

		var asset = AssetDatabase.LoadAssetAtPath(tmp, typeof(TextAsset)) as TextAsset;

		var bytes = new byte[asset.bytes.Length];
		asset.bytes.CopyTo(bytes, 0);

		AssetDatabase.DeleteAsset(tmp);

		return bytes;
	}
}
