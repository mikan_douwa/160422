﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(FingerGesture))]
public class FingerGestureEditor : Editor
{
	public override void OnInspectorGUI ()
	{
		serializedObject.Update();

		NGUIEditorTools.DrawProperty("Priority", serializedObject, "priority");
		NGUIEditorTools.DrawProperty("MinSwipDistance", serializedObject, "minSwipDistance");

		serializedObject.ApplyModifiedProperties();

		NGUIEditorTools.SetLabelWidth(80f);
		var obj = target as FingerGesture;

		NGUIEditorTools.DrawEvents("On Tap", obj, obj.onTap);
		NGUIEditorTools.DrawEvents("On Drag Begin", obj, obj.onDragBegin);
		NGUIEditorTools.DrawEvents("On Drag Moved", obj, obj.onDragMoved);
		NGUIEditorTools.DrawEvents("On Drag End", obj, obj.onDragEnd);
		NGUIEditorTools.DrawEvents("On Long Press", obj, obj.onLongPress);
		NGUIEditorTools.DrawEvents("On Swip", obj, obj.onSwip);
	}
}