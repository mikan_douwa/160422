﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.IO;

public class DebugMenu {

	[MenuItem("Debug/ClearLocalData")]
	public static void Clear()
	{
		PlayerPrefs.DeleteAll();
	}

	[MenuItem("Debug/ClearPatchData")]
	public static void ClearPatch()
	{
		foreach(var item in Directory.GetFiles(Application.persistentDataPath))
		{
			File.Delete(item);
		}
		
		foreach(var item in Directory.GetDirectories(Application.persistentDataPath))
		{
			Directory.Delete(item, true);
		}
	}

}
