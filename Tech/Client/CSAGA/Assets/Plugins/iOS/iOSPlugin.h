#import <Foundation/Foundation.h>
#import <StoreKit/StoreKit.h>


@interface iOSPlugin : NSObject<SKProductsRequestDelegate, SKPaymentTransactionObserver>
{
    NSString *gameObjectName;
    
    SKProductsRequest *request;
    NSArray<SKProduct*> *products;
    
}

@property(nonatomic, retain) NSString *gameObjectName;
@property(nonatomic, retain) SKProductsRequest *request;
@property(nonatomic, retain) NSArray<SKProduct*> *products;

- (BOOL)canMakePayment;

- (void)validateProductIdentifiers:(NSArray *)productIdentifiers;
- (void)productsRequest:(SKProductsRequest *)request
     didReceiveResponse:(SKProductsResponse *)response;
- (void)paymentRequest:(NSString *)productIdentify;

@end

