#import "iOSPlugin.h"

@implementation iOSPlugin

@synthesize gameObjectName;
@synthesize request;
@synthesize products;

- (id)init
{
    self = [super init];
    return self;
}



- (void)dealloc
{
    [gameObjectName release];
    [products release];
    [request release];
    [super dealloc];
}

- (BOOL)canMakePayment
{
    return [SKPaymentQueue canMakePayments];

}

//Query IAP Products
- (void)validateProductIdentifiers:(NSArray *)productIdentifiers
{
    SKProductsRequest *productsRequest = [[SKProductsRequest alloc]
                                          initWithProductIdentifiers:[NSSet setWithArray:productIdentifiers]];
    
    // Keep a strong reference to the request.
    self.request = productsRequest;
    productsRequest.delegate = self;
    [productsRequest start];
}

// SKProductsRequestDelegate protocol method
- (void)productsRequest:(SKProductsRequest *)request
     didReceiveResponse:(SKProductsResponse *)response
{
    //NSLog(@"request callback");
    self.products = response.products;
    
    for (NSString *invalidIdentifier in response.invalidProductIdentifiers) {
        // Handle any invalid product identifiers.
    }
    
    NSMutableString * allProducts = [NSMutableString stringWithString:@""];
    int i = 0;
    for (SKProduct *p in products)
    {
        if(i > 0)
            [allProducts appendString:@";"];
            
        [allProducts appendString:p.productIdentifier];
        [allProducts appendString:@",0"];
        i++;
    }
    
    //NSLog(@"%@",gameObjectName);
    //NSLog(@"%@",allProducts);
    UnitySendMessage([self.gameObjectName UTF8String], "QueryResult", [allProducts UTF8String]);
}

//Request Payment
- (void)paymentRequest:(NSString *)productIdentify
{
    //NSLog(@"paymentRequest:%@", productIdentify);
    for(SKProduct *p in self.products)
    {
        if ([productIdentify isEqualToString:p.productIdentifier]) {
            //NSLog(@"request %@", p.productIdentifier);
            
            SKMutablePayment *payment = [SKMutablePayment paymentWithProduct:p];
            payment.quantity = 1;
            [[SKPaymentQueue defaultQueue] addPayment:payment];
            break;
       }
    }
}

//Payment Callback
- (void)paymentQueue:(SKPaymentQueue *)queue
 updatedTransactions:(NSArray *)transactions
{
    //NSLog(@"payment callback");
    
    for (SKPaymentTransaction *transaction in transactions) {
        switch (transaction.transactionState) {
                // Call the appropriate custom method for the transaction state.
            case SKPaymentTransactionStatePurchasing:
            {
                //NSLog(@"SKPaymentTransactionStatePurchasing");
                break;
            }
            case SKPaymentTransactionStateDeferred:
                //NSLog(@"SKPaymentTransactionStateDeferred");
                break;
            case SKPaymentTransactionStateFailed:
            {
                NSMutableString * result = [NSMutableString stringWithString:transaction.payment.productIdentifier];
                [result appendString:@", "];
                [result appendString:transaction.error.domain];
                UnitySendMessage([self.gameObjectName UTF8String], "PurchaseResult", [result UTF8String]);
                
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                break;
            }
            case SKPaymentTransactionStatePurchased:
            {
                //save to user default
                //order id
                NSUserDefaults *storage = [NSUserDefaults standardUserDefaults];
                [storage setObject:transaction.transactionIdentifier forKey:transaction.payment.productIdentifier];
                //receipt
                NSMutableString * key = [NSMutableString stringWithString:transaction.payment.productIdentifier];
                [key appendString:@"_token"];
                NSString *receipt = [[NSString alloc] initWithData:transaction.transactionReceipt encoding:NSUTF8StringEncoding];
                [storage setObject:receipt forKey:key];
                
                //NSLog(@"orderId:%@, token:%@", transaction.transactionIdentifier, receipt);
                
                //result string.
                NSMutableString * result = [NSMutableString stringWithString:transaction.payment.productIdentifier];
                [result appendString:@",0"];
                UnitySendMessage([self.gameObjectName UTF8String], "PurchaseResult", [result UTF8String]);
                
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                break;
            }
            case SKPaymentTransactionStateRestored:
                break;
            default:
                // For debugging
                //NSLog(@"Unexpected transaction state %@", @(transaction.transactionState));
                break;
        }
    }
    
}


@end

static iOSPlugin* delegateObject = nil;

// Converts C style string to NSString
NSString* CreateNSString (const char* string)
{
    if (string)
        return [NSString stringWithUTF8String: string];
    else
        return [NSString stringWithUTF8String: ""];
}

// Helper method to create C string copy
char* MakeStringCopy (const char* string)
{
    if (string == NULL)
        return NULL;
    
    char* res = (char*)malloc(strlen(string) + 1);
    strcpy(res, string);
    return res;
}

// When native code plugin is implemented in .mm / .cpp file, then functions
// should be surrounded with extern "C" block to conform C function naming rules
extern "C" {
    
    void _InitIAP (const char* gameObject)
    {
        //NSLog(@"Plugin Called");
        if (delegateObject == nil)
            delegateObject = [[iOSPlugin alloc] init];
        
        delegateObject.gameObjectName = CreateNSString(gameObject);
        [[SKPaymentQueue defaultQueue] addTransactionObserver:delegateObject];

        UnitySendMessage([delegateObject.gameObjectName UTF8String], "InitResult", "0");
    }
    
    BOOL _canMakePayment()
    {
        return [delegateObject canMakePayment];
    }
    
    void _requestProducts(const char* identifiers)
    {
        //NSLog(@"_requestProducts");
        NSString *nsIdentifier = CreateNSString(identifiers);
        NSArray *arrIdentifier = [nsIdentifier componentsSeparatedByString:@";"];
        [delegateObject validateProductIdentifiers:arrIdentifier];
    }
    
    void _requestPayment(const char* identify)
    {
        NSString *nsIdentify = CreateNSString(identify);
        [delegateObject paymentRequest:nsIdentify];
    }
}

