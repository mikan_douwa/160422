﻿using System;
using System.Collections.Generic;
using System.Text;
using Mikan;
using Mikan.CSAGA.ConstData;

internal static class Global
{
    public static Tables Tables { get { return Mikan.CSAGA.ConstData.Tables.Instance; } }

    public static Mikan.CSAGA.Service Service { get { return Mikan.CSAGA.Service.Instance; } }
}