﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;

namespace Mikan.CSAGA
{
    interface ISerializableAdaptor
    {
        ISerializable DeSerialize(IInput input);
    }

    class SerializableAdaptor<T> : ISerializableAdaptor
        where T : ISerializable, new()
    {
        public ISerializable DeSerialize(IInput input)
        {
            var obj = new T();
            obj.DeSerialize(input);
            return obj;
        }
    }

    abstract class FieldAdaptor
    {
        public FieldID ID;
        public FieldInfo Field;

        public void GetValueFrom(CSAGACommand target)
        {
            Field.SetValue(target, GetValue(target));
        }

        public void SetValueTo(CSAGACommand target)
        {
            var val = Field.GetValue(target);

            SetValue(target, val);
        }

        abstract protected object GetValue(CSAGACommand target);

        abstract protected void SetValue(CSAGACommand target, object val);
    }

    class Int32FieldAdaptor : FieldAdaptor
    {
        protected override object GetValue(CSAGACommand target)
        {
            return target.Get<int>(ID);
        }

        protected override void SetValue(CSAGACommand target, object val)
        {
            target.Add(ID, (int)val);
        }
    }

    class Int64FieldAdaptor : FieldAdaptor
    {
        protected override object GetValue(CSAGACommand target)
        {
            return target.Get<Int64>(ID);
        }

        protected override void SetValue(CSAGACommand target, object val)
        {
            target.Add(ID, (Int64)val);
        }
    }

    class BooleanFieldAdaptor : FieldAdaptor
    {
        protected override object GetValue(CSAGACommand target)
        {
            return target.Get<bool>(ID);
        }

        protected override void SetValue(CSAGACommand target, object val)
        {
            target.Add(ID, (bool)val);
        }
    }

    class StringFieldAdaptor : FieldAdaptor
    {
        protected override object GetValue(CSAGACommand target)
        {
            return target.GetString(ID);
        }

        protected override void SetValue(CSAGACommand target, object val)
        {
            target.Add(ID, (string)val);
        }
    }

    class DateTimeFieldAdaptor : FieldAdaptor
    {
        protected override object GetValue(CSAGACommand target)
        {
            return target.GetDateTime(ID);
        }

        protected override void SetValue(CSAGACommand target, object val)
        {
            target.Add(ID, (DateTime)val);
        }
    }
}
