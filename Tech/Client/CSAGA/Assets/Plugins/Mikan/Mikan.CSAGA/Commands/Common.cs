﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mikan.CSAGA.Commands
{

    #region Request

    public class CSAGARequest : CSAGACommand
    {
        [CommandField(ID= FieldID.TransferID)]
        public int transferId;

        virtual public bool RequireToken { get { return true; } }
        virtual public bool IsReadOnly { get { return false; } }

        /// <inheritdoc />
        sealed protected override void CustomSerialize(IOutput output)
        {
            base.CustomSerialize(output);
            OnCustomSerialize(output);
        }

        /// <inheritdoc />
        sealed protected override void CustomDeSerialize(IInput input)
        {
            base.CustomDeSerialize(input);
            OnCustomDeSerialize(input);
        }

        virtual protected void OnCustomSerialize(IOutput output) { }

        virtual protected void OnCustomDeSerialize(IInput input) { }

        public override void InjectTransferID(int id)
        {
            transferId = id;
        }

        public override int TransferID { get { return transferId; } }
    }

    abstract public class NoTokenRequestBase : CSAGARequest
    {
        public override bool RequireToken { get { return false; } }
    }


    abstract public class ReadOnlyRequestBase : CSAGARequest
    {
        public override bool IsReadOnly { get { return true; } }
        public override bool IsRegular { get { return true; } }
    }

    abstract public class RegularResponseBase : CSAGAResponse
    {
        public override bool IsRegular { get { return true; } }
    }

    #endregion

    #region Response

    public class CSAGAResponse : CSAGACommand
    {
        public override bool IsSucc { get { return Status == ErrorCode.Success; } }
        public override bool AutoRetry { get { return Status == ErrorCode.Initializing; } }
        public override string ErrorMessage { get { return Status.ToString(); } }

        virtual public bool IsTrophySensitive { get { return false; } }

        [CommandField(ID = FieldID.Status)]
        public ErrorCode Status;

        /// <inheritdoc />
        sealed protected override void CustomSerialize(IOutput output)
        {
            base.CustomSerialize(output);
            if (IsSucc) OnCustomSerialize(output);
        }

        /// <inheritdoc />
        sealed protected override void CustomDeSerialize(IInput input)
        {
            base.CustomDeSerialize(input);
            if (IsSucc) OnCustomDeSerialize(input);
        }

        virtual protected void OnCustomSerialize(IOutput output) { }

        virtual protected void OnCustomDeSerialize(IInput input) { }
    }

    /// <summary>
    /// 無效的操作
    /// </summary>
    public class InvalidOperation : CSAGAResponse
    {
        /// <summary>
        /// 建構子
        /// </summary>
        public InvalidOperation()
        {
            Status = ErrorCode.InvalidOperation;
        }
    }

    /// <summary>
    /// 一般錯誤
    /// </summary>
    public class RegularError : CSAGAResponse
    {
        public RegularError()
        {
            Status = ErrorCode.Error;
        }
        public RegularError(ErrorCode error)
        {
            Status = error;
        }
    }

    public class MaintenanceMessage : CSAGAResponse
    {
        public static string DefaultMessage;

        public override string ErrorMessage { get { return Message; } }

        [CommandField(ID = FieldID.Value)]
        public string Message;
        /// <summary>
        /// 建構子
        /// </summary>
        public MaintenanceMessage()
        {
            Status = ErrorCode.Maintaining;
            Message = DefaultMessage;
        }
    }
    #endregion
}
