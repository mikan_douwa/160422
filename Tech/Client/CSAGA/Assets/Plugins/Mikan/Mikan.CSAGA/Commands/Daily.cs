﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mikan.CSAGA.Commands.Daily
{
    /// <summary>
    /// C->S 要求每日活躍資料
    /// </summary>
    public class InfoRequest : ReadOnlyRequestBase { }

    /// <summary>
    /// S->C 回傳每日活躍資料
    /// </summary>
    public class InfoResponse : RegularResponseBase
    {
        /// <summary>
        /// 每日活躍資料
        /// </summary>
        [CommandField(ID = FieldID.DailyInfo)]
        public string DailyInfo;
    }

    /// <summary>
    /// C->S 要求每日活躍資料
    /// </summary>
    public class PickRequest : CSAGARequest
    {
        [CommandField(ID = FieldID.Index)]
        public int Index;
    }

    /// <summary>
    /// S->C 回傳每日活躍資料
    /// </summary>
    public class PickResponse : InfoResponse
    {
        [CommandField(ID = FieldID.ID)]
        public int DropID;
    }
}
