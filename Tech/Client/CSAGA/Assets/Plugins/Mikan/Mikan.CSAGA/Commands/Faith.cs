﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
#if UNITY_ANDROID
using Ionic.Zlib;
#else
using System.IO.Compression;
#endif

namespace Mikan.CSAGA.Commands.Faith
{
    public class InfoRequest : ReadOnlyRequestBase
    {
    }

    public class InfoResponse : RegularResponseBase
    {
        [CommandField(ID = FieldID.FaithInfo)]
        public string FaithInfo;
    }

    public class ClaimRequest : CSAGARequest
    {
        
    }

    public class ClaimResponse : CSAGAResponse
    {
        [CommandField(ID = FieldID.Timestamp)]
        public DateTime ClaimTime;

        public Dictionary<int, ValuePair> Diff;

        protected override void OnCustomDeSerialize(IInput input)
        {
            base.OnCustomDeSerialize(input);

            Diff = new Dictionary<int, ValuePair>();

            foreach(var item in SyncList.FindAll(o => o is SyncItem))
            {
                var syncData = item as SyncItem;
                Diff[syncData.Diff.First.Key] = syncData.Diff.First.Value;
            }
             
        }
    }

    public class BeginProduceRequest : CSAGARequest
    {
        [CommandField(ID = FieldID.Index)]
        public int Index;

        public List<int> List;

        /// <inheritdoc />
        protected override void OnCustomSerialize(IOutput output)
        {
            output.Write(List);
        }

        /// <inheritdoc />
        protected override void OnCustomDeSerialize(IInput input)
        {
            List = input.ReadInt32List();
        }

    }

    public class BeginProduceResponse : CSAGAResponse
    {

    }

    public class EndProduceRequest : CSAGARequest
    {
        [CommandField(ID = FieldID.Serial)]
        public Int64 Serial;
    }

    public class EndProduceResponse : CSAGAResponse
    {
        [CommandField(ID = FieldID.ID)]
        public int DropID;
    }
}
