﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mikan.CSAGA.Commands.Purchase
{
    /// <summary>
    /// C->S 要求消費資料
    /// </summary>
    public class InfoRequest : ReadOnlyRequestBase { }

    /// <summary>
    /// S->C 回傳消費資料
    /// </summary>
    public class InfoResponse : RegularResponseBase
    {
        /// <summary>
        /// 消費資料
        /// </summary>
        [CommandField(ID = FieldID.PurchaseInfo)]
        public string PurchaseInfo;
    }

    public class ArticleRequest : CSAGARequest
    {
        [CommandField(ID = FieldID.ID)]
        public ArticleID ArticleID;
        [CommandField(ID = FieldID.Value)]
        public int Param;
    }

    public class ArticleResponse : CSAGAResponse
    {
        [CommandField(ID = FieldID.ID)]
        public ArticleID ArticleID;

        [CommandField(ID = FieldID.Value)]
        public int Count;
    }

}
