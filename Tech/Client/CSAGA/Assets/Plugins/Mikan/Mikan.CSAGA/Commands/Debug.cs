﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mikan.CSAGA.Commands
{
    public class DebugRequest : CSAGARequest
    {
    }

    /// <summary>
    /// S->C 回傳測試命令執行結果
    /// </summary>
    public class DebugResponse : CSAGAResponse
    {

    }

    public class ReportRequest : CSAGARequest
    {
        [CommandField(ID = FieldID.ID)]
        public ReportType Type;
        [CommandField(ID = FieldID.Value)]
        public string Message;
    }

    public class ReportResponse : CSAGAResponse
    {
        [CommandField(ID = FieldID.ID)]
        public ReportType Type;
    }

    public class RestoreRequest : CSAGARequest
    {

    }

    public class RestoreResponse : CSAGAResponse
    {
        public List<Backup> List;

        protected override void OnCustomDeSerialize(IInput input)
        {
            List = input.ReadList<Backup>();
        }

        protected override void OnCustomSerialize(IOutput output)
        {
            output.Write(List);
        }
    }
}
