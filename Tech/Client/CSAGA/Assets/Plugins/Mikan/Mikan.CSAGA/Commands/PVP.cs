﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mikan.CSAGA.Commands.PVP
{
    /// <summary>
    /// C->S 要求PVP資料
    /// </summary>
    public class InfoRequest : ReadOnlyRequestBase
    {
        [CommandField(ID = FieldID.Flag)]
        public bool Initial = false;
    }

    /// <summary>
    /// S->C 回傳PVP資料
    /// </summary>
    public class InfoResponse : RegularResponseBase
    {
        /// <summary>
        /// PVP資料
        /// </summary>
        [CommandField(ID = FieldID.PVPInfo)]
        public string PVPInfo;
    }

    /// <summary>
    /// C->S 要求更新防禦隊伍
    /// </summary>
    public class UpdateRequest : CSAGARequest
    {
        public List<Internal.PVPWaveDiff> Diffs;

        protected override void OnCustomSerialize(IOutput output)
        {
            output.Write(Diffs);
        }

        protected override void OnCustomDeSerialize(IInput input)
        {
            Diffs = input.ReadList<Internal.PVPWaveDiff>();
        }
    }

    public class UpdateResponse : CSAGAResponse
    {
        public List<Internal.PVPWaveDiff> Diffs;

        protected override void OnCustomSerialize(IOutput output)
        {
            output.Write(Diffs);
        }

        protected override void OnCustomDeSerialize(IInput input)
        {
            Diffs = input.ReadList<Internal.PVPWaveDiff>();
        }
    }

    public class RerollSkillRequest : CSAGARequest
    {
        [CommandField(ID = FieldID.Index)]
        public int Index;
    }

    public class RerollSkillResponse : CSAGAResponse
    {
        [CommandField(ID = FieldID.Index)]
        public int Index;

        public List<int> Skills;

        protected override void OnCustomSerialize(IOutput output)
        {
            output.Write(Skills);
        }

        protected override void OnCustomDeSerialize(IInput input)
        {
            Skills = input.ReadInt32List();
        }
    }

    public class QueryRequest : CSAGARequest
    {
    }

    public class QueryResponse : CSAGAResponse
    {
        [CommandField(ID = FieldID.CardInfo)]
        public string ListObj;
    }
    /*
    public class BeginRequest : CSAGARequest
    {
        public string PublicID;
        public string Key;
    }

    public class BeginResponse : CSAGAResponse
    {
    }

    public class EndRequest : CSAGARequest
    {
    }

    public class EndResponse : CSAGAResponse
    {
    }
    */
}
