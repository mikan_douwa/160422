﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
#if UNITY_ANDROID
using Ionic.Zlib;
#else
using System.IO.Compression;
#endif

namespace Mikan.CSAGA.Commands.IAP
{
    public class InitRequest : CSAGARequest
    {
        [CommandField(ID = FieldID.Name)]
        public string IAPName;
    }

    public class InitResponse : CSAGAResponse
    {
        
    }

    public class InfoRequest : ReadOnlyRequestBase
    {
    }

    public class InfoResponse : RegularResponseBase
    {
        [CommandField(ID = FieldID.IAPInfo)]
        public string IAPInfo;
    }

    public class ClaimRequest : CSAGARequest
    {
        [CommandField(ID = FieldID.ID)]
        public string OrderID;

        [CommandField(ID = FieldID.Category)]
        public string ProductID;
        
        //[CommandField(ID = FieldID.Token)]
        public string Token;

        protected override void OnCustomSerialize(IOutput output)
        {
            output.Write(Compress(Token));
        }
        protected override void OnCustomDeSerialize(IInput input)
        {
            Token = Decompress(input.ReadByteArray());
        }

        private byte[] Compress(string text)
        {
            var rawdata = Encoding.UTF8.GetBytes(Token);

            using (MemoryStream ms = new MemoryStream())
            {
                using (GZipStream compressedzipStream = new GZipStream(ms, CompressionMode.Compress, true))
                {
                    compressedzipStream.Write(rawdata, 0, rawdata.Length);
                    compressedzipStream.Close();
                    return ms.ToArray();
                }
            }
        }

        private string Decompress(byte[] zippedData)
        {
            using (MemoryStream ms = new MemoryStream(zippedData))
            {
                using (GZipStream compressedzipStream = new GZipStream(ms, CompressionMode.Decompress))
                {
                    using (MemoryStream outBuffer = new MemoryStream())
                    {
                        byte[] block = new byte[1024];
                        while (true)
                        {
                            int bytesRead = compressedzipStream.Read(block, 0, block.Length);
                            if (bytesRead <= 0)
                                break;
                            else
                                outBuffer.Write(block, 0, bytesRead);
                        }
                        compressedzipStream.Close();

                        return Encoding.UTF8.GetString(outBuffer.ToArray());
                    }
                }
            }
        }
    }

    public class ClaimResponse : CSAGAResponse
    {
        public override bool IsTrophySensitive { get { return true; } }
        [CommandField(ID = FieldID.ID)]
        public string OrderID;
        [CommandField(ID = FieldID.Category)]
        public string ProductID;

        [CommandField(ID = FieldID.Value)]
        public int Gem;
    }
}
#if MYCARD
namespace Mikan.CSAGA.Commands.MyCard
{
    public class BeginTransactionRequest : CSAGARequest
    {
        [CommandField(ID = FieldID.Category)]
        public string ProductID;
    }

    public class BeginTransactionResponse : CSAGAResponse
    {
        [CommandField(ID = FieldID.Serial)]
        public string AuthCode;

        [CommandField(ID = FieldID.Category)]
        public string ProductID;

        [CommandField(ID = FieldID.Flag)]
        public bool Sandbox;
    }
}
#endif