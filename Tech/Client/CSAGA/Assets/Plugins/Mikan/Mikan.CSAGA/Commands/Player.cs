﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mikan.CSAGA.Commands.Player
{
    /// <summary>
    /// C->S 要求創建玩家資料
    /// </summary>
    public class CreateRequest : CSAGARequest
    {
        /// <summary>
        /// 玩家名稱
        /// </summary>
        [CommandField(ID = FieldID.Name)]
        public string Name;

        /// <summary>
        /// 玩家名稱
        /// </summary>
        [CommandField(ID = FieldID.Category)]
        public int Category;

        [CommandField(ID = FieldID.Value)]
        public string Code;
    }

    /// <summary>
    /// S->C 回傳角色創建結果
    /// </summary>
    public class CreateResponse : CSAGAResponse { }


    /// <summary>
    /// C->S 要求初始化玩家
    /// </summary>
    public class InitRequest : CSAGARequest
    {
        /// <summary>
        /// 玩家基本資料
        /// </summary>
        [CommandField(ID = FieldID.Serial)]
        public Int64 Header;
    }


    /// <summary>
    /// S->C 回傳玩家基本資料
    /// </summary>
    public class InitResponse : CSAGAResponse
    {
        /// <summary>
        /// 玩家基本資料
        /// </summary>
        [CommandField(ID = FieldID.PlayerInfo)]
        public string PlayerInfo;
    }

    /// <summary>
    /// C->S 要求變更玩家名稱
    /// </summary>
    public class NameRequest : CSAGARequest
    {
        /// <summary>
        /// 玩家名稱
        /// </summary>
        [CommandField(ID = FieldID.Name)]
        public string Name;
    }

    /// <summary>
    /// S->C 回傳玩家名稱變更結果
    /// </summary>
    public class NameResponse : CSAGAResponse
    {
        /// <summary>
        /// 玩家名稱
        /// </summary>
        [CommandField(ID = FieldID.Name)]
        public string Name;
    }
}
