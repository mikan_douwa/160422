﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mikan.CSAGA.Commands.Gacha
{
    /// <summary>
    /// C->S 要求抽轉蛋
    /// </summary>
    public class DrawRequest : CSAGARequest
    {
        [CommandField(ID = FieldID.ID)]
        public int GachaID;
    }

    /// <summary>
    /// S->C 回傳抽轉蛋結果
    /// </summary>
    public class DrawResponse : CSAGAResponse
    {
        public override bool IsTrophySensitive { get { return true; } }

        [CommandField(ID = FieldID.ID)]
        public int GachaID;
        [CommandField(ID = FieldID.Value)]
        public int Count;

        public SyncCard SyncCard;

        public SyncPlayer SyncPlayer;

        protected override void OnCustomDeSerialize(IInput input)
        {
            SyncCard = SyncList.Find(o => { return o is SyncCard; }) as SyncCard;
            SyncPlayer = SyncList.Find(o => { return o is SyncPlayer; }) as SyncPlayer;
        }
    }
}
