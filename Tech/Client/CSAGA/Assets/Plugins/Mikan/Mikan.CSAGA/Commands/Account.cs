﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mikan.CSAGA.Commands.Account
{
    /// <summary>
    /// C->S 要求創建新帳號
    /// </summary>
    public class CreateRequest : NoTokenRequestBase
    {
        /// <summary>
        /// 帳號識別碼
        /// </summary>
        [CommandField(ID = FieldID.UUID)]
        public string UUID;

        /// <summary>
        /// 裝置ID
        /// </summary>
        [CommandField(ID = FieldID.DeviceID)]
        public string DeviceID;
    }

    /// <summary>
    /// S->C 回傳創建帳號結果
    /// </summary>
    public class CreateResponse : CSAGAResponse
    {
        /// <summary>
        /// 帳號資訊(編碼過)
        /// </summary>
        [CommandField(ID = FieldID.Profile)]
        public string Profile;
    }

    /// <summary>
    /// C->S 要求登入
    /// </summary>
    public class LoginRequest : NoTokenRequestBase
    {
        /// <summary>
        /// 帳號識別碼
        /// </summary>
        [CommandField(ID = FieldID.UUID)]
        public string UUID;

        [CommandField(ID = FieldID.Platform)]
        public string Platform;

        [CommandField(ID = FieldID.Flag)]
        public bool UseSuperPrivilege = false;
    }

    /// <summary>
    /// S->C 回傳登入結果
    /// </summary>
    public class LoginResponse : CSAGAResponse
    {
        /// <summary>
        /// 傳輸用公開密鑰
        /// </summary>
        [CommandField(ID = FieldID.Token)]
        public string Token;

        /// <summary>
        /// 伺服器時間
        /// </summary>
        [CommandField(ID = FieldID.ServerTime)]
        public DateTime ServerTime;

        [CommandField(ID = FieldID.Value)]
        public string Signature;

        [CommandField(ID = FieldID.ID)]
        public string AppVersion;

        [CommandField(ID = FieldID.Flag)]
        public bool IsMaintaining;
    }

    /// <summary>
    /// C->S 初始化帳號, 登入後一定要先呼叫才能繼續做其他事情
    /// </summary>
    public class InitRequest : CSAGARequest { }

    /// <summary>
    /// S->C 初始化帳號完成
    /// </summary>
    public class InitResponse : CSAGAResponse { }

    public class PublishRequest : CSAGARequest { }

    public class PublishResponse : CSAGAResponse
    {
        [CommandField(ID = FieldID.Token)]
        public string Token;
    }

    public class TransferBeginRequest : NoTokenRequestBase
    {
        [CommandField(ID = FieldID.ID)]
        public string ID;
        [CommandField(ID = FieldID.Token)]
        public string Token;
    }

    public class TransferBeginResponse : CreateResponse
    {
        [CommandField(ID = FieldID.Key)]
        public int Key;

        [CommandField(ID = FieldID.Name)]
        public string Name;
        [CommandField(ID = FieldID.Rank)]
        public int Rank;
        [CommandField(ID = FieldID.Value)]
        public int Gem;
    }

    public class TransferEndRequest : NoTokenRequestBase
    {
        [CommandField(ID = FieldID.ID)]
        public string ID;
        [CommandField(ID = FieldID.Token)]
        public string Token;
        [CommandField(ID = FieldID.Key)]
        public int Key;
    }

    public class TransferEndResponse : CSAGAResponse { }
}
