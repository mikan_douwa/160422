﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mikan.CSAGA.Commands.CooperateMission
{
    /// <summary>
    /// C->S 要求協力任務資料
    /// </summary>
    public class InfoRequest : CSAGARequest { }

    /// <summary>
    /// S->C 回傳協力任務資料
    /// </summary>
    public class InfoResponse : CSAGAResponse
    {
        /// <summary>
        /// 協力任務資料
        /// </summary>
        [CommandField(ID = FieldID.CooperateMissionInfo)]
        public string CooperateMissionInfo;
    }

    public class DonateRequest : CSAGARequest
    {
        public SyncMission Mission;

        protected override void OnCustomSerialize(IOutput output)
        {
            output.Write(Mission);
        }

        protected override void OnCustomDeSerialize(IInput input)
        {
            Mission = input.Read<SyncMission>();
        }
        
    }

    public class DonateResponse : CSAGAResponse
    {
    }

    public class ClaimRequest : CSAGARequest
    {
        [CommandField(ID = FieldID.Serial)]
        public Int64 Serial;
    }

    public class ClaimResponse : CSAGAResponse
    {
        [CommandField(ID = FieldID.Serial)]
        public Int64 Serial;

        public List<Reward> Rewards;

        protected override void OnCustomSerialize(IOutput output)
        {
            output.Write(Rewards);
        }

        protected override void OnCustomDeSerialize(IInput input)
        {
            Rewards = input.ReadList<Reward>();
        }
    }

    public class RankingRequest : CSAGARequest
    {
        [CommandField(ID = FieldID.Serial)]
        public Int64 Serial;
    }

    public class RankingResponse : CSAGAResponse
    {

    }
}
