﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mikan.CSAGA.Commands.Gift
{
    /// <summary>
    /// C->S 要求贈獎資料
    /// </summary>
    public class InfoRequest : ReadOnlyRequestBase { }

    /// <summary>
    /// S->C 回傳贈獎資料
    /// </summary>
    public class InfoResponse : RegularResponseBase
    {
        /// <summary>
        /// 贈獎資料
        /// </summary>
        [CommandField(ID = FieldID.GiftInfo)]
        public string GiftInfo;
    }

    public class ClaimRequest : CSAGARequest
    {
        [CommandField(ID = FieldID.Serial)]
        public Int64 Serial;

        [CommandField(ID = FieldID.Value)]
        public int Count;
    }

    public class ClaimResponse : CSAGAResponse
    {
        public override bool IsTrophySensitive { get { return true; } }

        /// <summary>
        /// 贈獎資料
        /// </summary>
        [CommandField(ID = FieldID.GiftInfo)]
        public string GiftInfo;

        [CommandField(ID = FieldID.Serial)]
        public Int64 Serial;

        public List<Reward> List;

        /// <inheritdoc />
        protected override void OnCustomSerialize(IOutput output)
        {
            output.Write(List);
        }

        /// <inheritdoc />
        protected override void OnCustomDeSerialize(IInput input)
        {
            List = input.ReadList<Reward>();
        }
    }
}
