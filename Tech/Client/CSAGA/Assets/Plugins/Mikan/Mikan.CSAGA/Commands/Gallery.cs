﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mikan.CSAGA.Commands.Gallery
{
    /// <summary>
    /// C->S 同步已開啟圖鑑數量
    /// </summary>
    public class SyncRequest : CSAGARequest
    {
        [CommandField(ID = FieldID.Value)]
        public int Count;
    }

    /// <summary>
    /// S->C 同步結果
    /// </summary>
    public class SyncResponse : CSAGAResponse
    {
        public override bool IsTrophySensitive { get { return true; } }
    }
}
