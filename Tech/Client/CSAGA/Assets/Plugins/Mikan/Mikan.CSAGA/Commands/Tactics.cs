﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mikan.CSAGA.Commands.Tactics
{
    /// <summary>
    /// C->S 要求戰術資訊
    /// </summary>
    public class InfoRequest : ReadOnlyRequestBase { }

    /// <summary>
    /// S->C 回傳戰術資訊
    /// </summary>
    public class InfoResponse : RegularResponseBase
    {
        /// <summary>
        /// 戰術資訊
        /// </summary>
        [CommandField(ID = FieldID.TacticsInfo)]
        public string TacticsInfo;
    }

    public class EnableRequest : CSAGARequest
    {
        [CommandField(ID = FieldID.Value)]
        public int TacticsID;
    }

    public class EnableResponse : CSAGAResponse
    {
        [CommandField(ID = FieldID.Value)]
        public int TacticsID;
    }
}
