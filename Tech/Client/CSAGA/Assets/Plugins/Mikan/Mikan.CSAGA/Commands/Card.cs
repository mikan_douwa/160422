﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mikan.CSAGA.Commands.Card
{
    /// <summary>
    /// C->S 要求卡片資料
    /// </summary>
    public class InfoRequest : ReadOnlyRequestBase { }

    /// <summary>
    /// S->C 回傳卡片資料
    /// </summary>
    public class InfoResponse : RegularResponseBase
    {
        /// <summary>
        /// 卡片資料
        /// </summary>
        [CommandField(ID = FieldID.CardInfo)]
        public string CardInfo;
    }

    public class ResponseBase : CSAGAResponse
    {
        public override bool IsTrophySensitive { get { return true; } }

        public SyncCard SyncCard;

        public SyncPlayer SyncPlayer;

        protected override void OnCustomDeSerialize(IInput input)
        {
            foreach (var item in SyncList)
            {
                if (SyncCard == null) SyncCard = item as SyncCard;
                if (SyncPlayer == null) SyncPlayer = item as SyncPlayer;
            }
        }
    }

    public class EnhanceRequest : CSAGARequest
    {
        [CommandField(ID = FieldID.Serial)]
        public Int64 Serial;
        [CommandField(ID = FieldID.Value)]
        public int Exp;
    }

    public class EnhanceResponse : ResponseBase
    {
        
    }

    public class SellRequest : CSAGARequest
    {
        public List<Int64> List;

        protected override void OnCustomSerialize(IOutput output)
        {
            output.Write(List);
        }
        protected override void OnCustomDeSerialize(IInput input)
        {
            List = input.ReadInt64List();
        }
    }

    public class SellResponse : ResponseBase
    {
    }

    public class GiftRequest : CSAGARequest
    {
        [CommandField(ID = FieldID.Serial)]
        public Int64 Serial;
        [CommandField(ID = FieldID.ID)]
        public int ItemID;
        [CommandField(ID = FieldID.Value)]
        public int Count;
    }

    public class GiftResponse : ResponseBase
    {
        [CommandField(ID = FieldID.Serial)]
        public Int64 Serial;
    }

    public class BeginTalkRequest : CSAGARequest
    {
        [CommandField(ID = FieldID.Serial)]
        public Int64 Serial;
    }

    public class BeginTalkResponse : ResponseBase
    {
        [CommandField(ID = FieldID.Serial)]
        public Int64 Serial;
        [CommandField(ID = FieldID.ID)]
        public int InteractiveID;
    }

    public class EndTalkRequest : CSAGARequest
    {
        [CommandField(ID = FieldID.Serial)]
        public Int64 Serial;
        [CommandField(ID = FieldID.Value)]
        public int Choice;
    }

    public class EndTalkResponse : ResponseBase
    {
        [CommandField(ID = FieldID.Serial)]
        public Int64 Serial;

        [CommandField(ID = FieldID.Value)]
        public int EventID;

        [CommandField(ID = FieldID.ID)]
        public int DropID;
    }

    public class EventRequest : CSAGARequest
    {
        [CommandField(ID = FieldID.Serial)]
        public Int64 Serial;
        [CommandField(ID = FieldID.ID)]
        public int InteractiveID;
    }

    public class EventResponse : ResponseBase
    {
        [CommandField(ID = FieldID.Serial)]
        public Int64 Serial;

        [CommandField(ID = FieldID.ID)]
        public int InteractiveID;

        [CommandField(ID = FieldID.Value)]
        public int DropID;
    }

    public class AwakenRequest : CSAGARequest
    {
        [CommandField(ID = FieldID.Serial)]
        public Int64 Serial;

        [CommandField(ID = FieldID.ID)]
        public int Group;
    }

    public class AwakenResponse : ResponseBase
    {
        [CommandField(ID = FieldID.Serial)]
        public Int64 Serial;
    }

    public class LockRequest : CSAGARequest
    {
        public List<Int64> Locked;
        public List<Int64> Unlocked;

        protected override void OnCustomSerialize(IOutput output)
        {
            output.Write(Locked);
            output.Write(Unlocked);
        }
        protected override void OnCustomDeSerialize(IInput input)
        {
            Locked = input.ReadInt64List();
            Unlocked = input.ReadInt64List();
        }
    }

    public class LockResponse : ResponseBase
    {
    }

    public class HeaderRequest : CSAGARequest
    {
        [CommandField(ID = FieldID.Serial)]
        public Int64 Serial;
    }

    public class HeaderResponse : ResponseBase
    {
        [CommandField(ID = FieldID.Serial)]
        public Int64 Serial;
    }

    public class LuckRequest : CSAGARequest
    {
        [CommandField(ID = FieldID.Serial)]
        public Int64 Serial;
    }

    public class LuckResponse : ResponseBase
    {
        [CommandField(ID = FieldID.Serial)]
        public Int64 Serial;
    }
}
