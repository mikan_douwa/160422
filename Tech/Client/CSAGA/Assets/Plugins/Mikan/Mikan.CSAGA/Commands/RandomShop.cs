﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mikan.CSAGA.Commands.RandomShop
{
    /// <summary>
    /// C->S 要求取得隨機商店資訊
    /// </summary>
    public class InfoRequest : CSAGARequest
    {
        [CommandField(ID = FieldID.Flag)]
        public bool ForceRefresh = false;
    }

    /// <summary>
    /// S->C 回傳隨機商店資訊
    /// </summary>
    public class InfoResponse : CSAGAResponse
    {
        /// <summary>
        /// 隨機商店資訊
        /// </summary>
        [CommandField(ID = FieldID.RandomShopInfo)]
        public string RandomShopInfo;
    }

    public class PurchaseRequest : CSAGARequest
    {
        [CommandField(ID = FieldID.Index)]
        public int Index;
        /// <summary>
        /// 商品ID
        /// </summary>
        [CommandField(ID = FieldID.ID)]
        public int ArticleID;
    }

    public class PurchaseResponse : CSAGAResponse
    {
        /// <summary>
        /// 商品列表狀態更新
        /// </summary>
        [CommandField(ID = FieldID.Value)]
        public int State;

        [CommandField(ID = FieldID.ID)]
        public int DropID;
        /// <summary>
        /// 商品ID
        /// </summary>
        [CommandField(ID = FieldID.OriginValue)]
        public int ArticleID;
    }
}
