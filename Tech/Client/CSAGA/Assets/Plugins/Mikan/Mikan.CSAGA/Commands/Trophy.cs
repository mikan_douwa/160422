﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mikan.CSAGA.Commands.Trophy
{
    /// <summary>
    /// C->S 要求成就資料
    /// </summary>
    public class InfoRequest : ReadOnlyRequestBase { }

    /// <summary>
    /// S->C 回傳成就資料
    /// </summary>
    public class InfoResponse : RegularResponseBase
    {
        /// <summary>
        /// 成就資料
        /// </summary>
        [CommandField(ID = FieldID.TrophyInfo)]
        public string TrophyInfo;
    }
}
