﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mikan.CSAGA.Commands.Item
{
    /// <summary>
    /// C->S 要求道具資料
    /// </summary>
    public class InfoRequest : ReadOnlyRequestBase { }

    /// <summary>
    /// S->C 回傳道具資料
    /// </summary>
    public class InfoResponse : RegularResponseBase
    {
        /// <summary>
        /// 道具資料
        /// </summary>
        [CommandField(ID = FieldID.ItemInfo)]
        public string ItemInfo;
    }

    public class UseRequest : CSAGARequest
    {
        [CommandField(ID = FieldID.ID)]
        public int ItemID;
    }

    public class UseResponse : CSAGAResponse
    {
        [CommandField(ID = FieldID.ID)]
        public int DropID;
    }
}
