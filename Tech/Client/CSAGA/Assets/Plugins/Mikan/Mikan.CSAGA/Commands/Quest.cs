﻿using System;
using System.Collections.Generic;
using System.Text;


namespace Mikan.CSAGA.Commands.Quest
{
    /// <summary>
    /// C->S 要求任務資料
    /// </summary>
    public class InfoRequest : ReadOnlyRequestBase { }

    /// <summary>
    /// S->C 回傳任務資料
    /// </summary>
    public class InfoResponse : RegularResponseBase
    {
        /// <summary>
        /// 任務資料
        /// </summary>
        [CommandField(ID = FieldID.QuestInfo)]
        public string QuestInfo;

        public List<Internal.LimitedQuestObj> Limited;

        protected override void OnCustomSerialize(IOutput output)
        {
            output.Write(Limited);
        }

        protected override void OnCustomDeSerialize(IInput input)
        {
            Limited = input.ReadList<Internal.LimitedQuestObj>();
        }
    }

    public class BeginRequest : CSAGARequest
    {
        [CommandField(ID = FieldID.ID)]
        public int QuestID;

        [CommandField(ID = FieldID.Value)]
        public Int64 Leader;

        [CommandField(ID = FieldID.NewValue)]
        public Int64 SubLeader;

        [CommandField(ID = FieldID.PublicID)]
        public string Support;

        [CommandField(ID = FieldID.Flag)]
        public bool DisableRescue = false;

        [CommandField(ID = FieldID.Serial)]
        public Int64 Crusade;

        [CommandField(ID = FieldID.Profile)]
        public string MembersString;

        public List<Int64> Members;

        [CommandField(ID = FieldID.Ticket)]
        public string Ticket;

        protected override void OnSerialize()
        {
            MembersString = StringUtils.ToString(Members);
            base.OnSerialize();
        }
        protected override void OnDeSerialize()
        {
            base.OnDeSerialize();
            Members = StringUtils.ToInt64List(MembersString);

        }

    }

    public class BeginEventRequest : CSAGARequest
    {

        [CommandField(ID = FieldID.Serial)]
        public Int64 Serial;

        [CommandField(ID = FieldID.Value)]
        public int InteractiveID;

    }

    public class BeginResponse : CSAGAResponse
    {
        public QuestInstance Quest;

        protected override void OnCustomSerialize(IOutput output)
        {
            output.Write(Quest);
        }
        protected override void OnCustomDeSerialize(IInput input)
        {
            Quest = input.Read<QuestInstance>();
        }
    }

    public class EndRequest : CSAGARequest
    {
        [CommandField(ID = FieldID.Serial)]
        public Int64 Serial;

        [CommandField(ID = FieldID.IsSucc)]
        public bool IsQuestSucc;

        [CommandField(ID = FieldID.IsWin)]
        public bool IsPVPWin;

        [CommandField(ID = FieldID.Value)]
        public bool IsRescueSucc;

        [CommandField(ID = FieldID.Flag)]
        public bool IsAutoBattleEnabled;

        [CommandField(ID = FieldID.NewValue)]
        public int SupportCard;

        public List<Int64> List;

        public List<int> History;

        protected override void OnCustomSerialize(IOutput output)
        {
            output.Write(List);
        }

        protected override void OnCustomDeSerialize(IInput input)
        {
            List = input.ReadInt64List();

            var syncHistory = SyncList.Find(o => o is SyncQuestHistory) as SyncQuestHistory;
            if (syncHistory != null) History = syncHistory.List;
                
        }
    }

    public class EndResponse : CSAGAResponse
    {
        public override bool IsTrophySensitive { get { return IsQuestSucc; } }

        [CommandField(ID = FieldID.IsSucc)]
        public bool IsQuestSucc;

        [CommandField(ID = FieldID.Flag)]
        public bool IsAutoBattleEnabled;

        [CommandField(ID = FieldID.Value)]
        public RescueState RescueState;

        public List<int> Drops = new List<int>();
       
        public QuestSettlement Settlement;

        protected override void OnCustomSerialize(IOutput output)
        {
            output.Write(Drops);
        }

        protected override void OnCustomDeSerialize(IInput input)
        {
            Drops = input.ReadInt32List();

            Settlement = new QuestSettlement();
            Settlement.IsSucc = IsQuestSucc;
            Settlement.IsAutoBattleEnabled = IsAutoBattleEnabled;
            Settlement.Drops = Drops;
            Settlement.RescueState = RescueState;
            Settlement.SyncPlayer = SyncList.Find(o => { return o is SyncPlayer; }) as SyncPlayer;
            Settlement.SyncCard = SyncList.Find(o => { return o is SyncCard; }) as SyncCard;
            Settlement.SyncItem = SyncList.Find(o => { return o is SyncItem; }) as SyncItem;
            Settlement.SyncLimitedQuest = SyncList.Find(o => { return o is SyncLimitedQuest; }) as SyncLimitedQuest;

            var eventPoint = SyncList.Find(o => { return o is SyncEventPoint; }) as SyncEventPoint;
            if (eventPoint != null) Settlement.Point = eventPoint.Add;

            var infinity = SyncList.Find(o => { return o is SyncInfinityPoint; }) as SyncInfinityPoint;
            if (infinity != null) Settlement.Point = infinity.Point;

            if (Settlement.SyncPlayer != null)
            {
                var discover = Settlement.SyncPlayer.Diff[PropertyID.Discover];
                if (discover != null) Settlement.Discover = discover.Diff;
            }
        }
    }


    public class OpenRequest : CSAGARequest
    {
        [CommandField(ID = FieldID.ID)]
        public int QuestID;

    }

    public class OpenResponse : CSAGAResponse
    {
        [CommandField(ID = FieldID.ID)]
        public int QuestID;
    }

    public class QueryCrusadeRequest : CSAGARequest
    {
        
    }

    public class QueryCrusadeResponse : CSAGAResponse
    {
        [CommandField(ID = FieldID.Crusade)]
        public string Crusade;
    }
}
