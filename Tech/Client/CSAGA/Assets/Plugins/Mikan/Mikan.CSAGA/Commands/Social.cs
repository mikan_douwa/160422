﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mikan.CSAGA.Commands.Social
{

    public class QueryRankRequest : ReadOnlyRequestBase
    {
        public List<string> List;

        protected override void OnCustomSerialize(IOutput output)
        {
            output.Write(List);
        }

        protected override void OnCustomDeSerialize(IInput input)
        {
            List = input.ReadStringList();
        }
    }


    public class QueryRankResponse : RegularResponseBase
    {
        public List<Internal.SocialPlayerObj> List;

        protected override void OnCustomSerialize(IOutput output)
        {
            output.Write(List);
        }

        protected override void OnCustomDeSerialize(IInput input)
        {
            List = input.ReadList<Internal.SocialPlayerObj>();
        }
    }

    public class InfoRequest : ReadOnlyRequestBase
    {
        
    }

    public class InfoResponse : RegularResponseBase
    {
        [CommandField(ID = FieldID.SocialInfo)]
        public string SocialInfo;


    }

    public class QueryRequest : ReadOnlyRequestBase
    {
        public List<string> List;

        protected override void OnCustomSerialize(IOutput output)
        {
            output.Write(List);
        }

        protected override void OnCustomDeSerialize(IInput input)
        {
            List = input.ReadStringList();
        }
    }


    public class QueryResponse : RegularResponseBase
    {
        public List<Internal.Snapshot> Snapshots;

        protected override void OnCustomSerialize(IOutput output)
        {
            output.Write(Snapshots);
        }

        protected override void OnCustomDeSerialize(IInput input)
        {
            Snapshots = input.ReadList<Internal.Snapshot>();
        }
    }

    public class AddRequest : CSAGARequest
    {
        [CommandField(ID = FieldID.ID)]
        public string ID;
    }

    public class AddResponse : CSAGAResponse
    {
        [CommandField(ID = FieldID.ID)]
        public string ID;
    }

    public class RemoveRequest : CSAGARequest
    {
        [CommandField(ID = FieldID.ID)]
        public string ID;
    }

    public class RemoveResponse : CSAGAResponse
    {
        [CommandField(ID = FieldID.ID)]
        public string ID;
    }

    public class RandomRequest : ReadOnlyRequestBase
    {
    }

    public class RandomResponse : RegularResponseBase
    {
        public List<string> List;

        protected override void OnCustomSerialize(IOutput output)
        {
            output.Write(List);
        }

        protected override void OnCustomDeSerialize(IInput input)
        {
            List = input.ReadStringList();
        }
    }

    public class PurchaseHistoryRequest : ReadOnlyRequestBase
    {
    }

    public class PurchaseHistoryResponse : RegularResponseBase
    {
        public List<Data.PurchaseHistory> List;

        protected override void OnCustomSerialize(IOutput output)
        {
            output.Write(List);
        }

        protected override void OnCustomDeSerialize(IInput input)
        {
            List = input.ReadList<Data.PurchaseHistory>();
        }
    }
}
