﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mikan.CSAGA.Commands.Equipment
{
    /// <summary>
    /// C->S 要求裝備資料
    /// </summary>
    public class InfoRequest : ReadOnlyRequestBase { }

    /// <summary>
    /// S->C 回傳裝備資料
    /// </summary>
    public class InfoResponse : RegularResponseBase
    {
        /// <summary>
        /// 裝備資料
        /// </summary>
        [CommandField(ID = FieldID.EquipmentInfo)]
        public string EquipmentInfo;
    }

    public class ReplaceRequest : CSAGARequest
    {
        [CommandField(ID = FieldID.Value)]
        public Int64 Serial;
        [CommandField(ID = FieldID.Serial)]
        public Int64 CardSerial;
        [CommandField(ID = FieldID.Index)]
        public int Slot;
    }

    public class ReplaceResponse : CSAGAResponse
    {
        public override bool IsTrophySensitive { get { return true; } }

        public Int64 CardSerial;

        protected override void OnCustomDeSerialize(IInput input)
        {
            var syncData = SyncList.Find(o => { return o is SyncEquipment; }) as SyncEquipment;
            if (syncData != null && syncData.UpdateList.Count > 0)
            {
                var serialDiff = syncData.UpdateList[0].CardSerial;
                CardSerial = serialDiff.OriginValue != 0 ? serialDiff.OriginValue : serialDiff.NewValue;
            }

        }
    }

    public class SellRequest : CSAGARequest
    {
        public List<Int64> List;

        protected override void OnCustomSerialize(IOutput output)
        {
            output.Write(List);
        }
        protected override void OnCustomDeSerialize(IInput input)
        {
            List = input.ReadInt64List();
        }
    }

    public class SellResponse : CSAGAResponse
    {
    }

    public class RemoveRequest : CSAGARequest
    {
        [CommandField(ID = FieldID.Serial)]
        public Int64 Serial;
    }

    public class RemoveResponse : CSAGAResponse
    {
        [CommandField(ID = FieldID.Serial)]
        public Int64 Serial;
        [CommandField(ID = FieldID.Value)]
        public Int64 CardSerial;
    }

    public class LockRequest : CSAGARequest
    {
        [CommandField(ID = FieldID.Serial)]
        public Int64 Serial;

        [CommandField(ID = FieldID.Flag)]
        public bool IsLocked;
    }

    public class LockResponse : CSAGAResponse
    {
        [CommandField(ID = FieldID.Serial)]
        public Int64 Serial;
    }
}
