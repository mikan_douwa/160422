﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mikan.CSAGA.Commands.Ranking
{
    /// <summary>
    /// C->S 要求排名資料
    /// </summary>
    public class InfoRequest : CSAGARequest { }

    /// <summary>
    /// S->C 回傳排名資料
    /// </summary>
    public class InfoResponse : CSAGAResponse
    {
        /// <summary>
        /// 排名資料
        /// </summary>
        [CommandField(ID = FieldID.RankingInfo)]
        public string RankingInfo;
    }

    public class QueryRequest : CSAGARequest
    {
        [CommandField(ID = FieldID.ID)]
        public int EventID;
        [CommandField(ID = FieldID.Value)]
        public int From;
    }

    public class QueryResponse : CSAGAResponse
    {
        [CommandField(ID = FieldID.ID)]
        public int EventID;
        [CommandField(ID = FieldID.Value)]
        public int From;

        public List<Internal.RankingPlayerObj> List;

        protected override void OnCustomSerialize(IOutput output)
        {
            output.Write(List);
        }
        protected override void OnCustomDeSerialize(IInput input)
        {
            List = input.ReadList<Internal.RankingPlayerObj>();
        }
    }
}
