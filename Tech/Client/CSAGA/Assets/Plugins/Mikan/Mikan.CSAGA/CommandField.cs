﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mikan.CSAGA
{
    /// <summary>
    /// 命令可序列化欄位特徵
    /// </summary>
    public class CommandFieldAttribute : Attribute
    {
        /// <summary>
        /// 欄位ID
        /// </summary>
        public FieldID ID;
    }

    /// <summary>
    /// 欄位ID
    /// </summary>
    public enum FieldID
    {
        #region 協定相關(0)
        /// <summary>
        /// 錯誤代碼
        /// </summary>
        Status = 0,

        /// <summary>
        /// 帳號狀態旗標
        /// </summary>
        Flag = 1,

        /// <summary>
        /// 測試代碼
        /// </summary>
        DebugCode = 2,

        /// <summary>
        /// 傳輸序號
        /// </summary>
        TransferID = 3,

        #endregion

        #region 帳號相關(100)

        /// <summary>
        /// 帳號識別碼
        /// </summary>
        UUID = 100,

        /// <summary>
        /// 裝置ID
        /// </summary>
        DeviceID,

        /// <summary>
        /// 帳號資訊
        /// </summary>
        Profile,

        /// <summary>
        /// 伺服器時間
        /// </summary>
        ServerTime,

        /// <summary>
        /// 傳輸用公開密鑰
        /// </summary>
        Token,

        /// <summary>
        /// 玩家公開ID
        /// </summary>
        PublicID,

        /// <summary>
        /// 識別碼
        /// </summary>
        Key,

        /// <summary>
        /// 平台
        /// </summary>
        Platform,
        #endregion

        #region 通用(200)
        /// <summary>
        /// 流水號
        /// </summary>
        Serial = 200,
        /// <summary>
        /// ID
        /// </summary>
        ID,
        /// <summary>
        /// 值
        /// </summary>
        Value,
        /// <summary>
        /// 名字
        /// </summary>
        Name,
        /// <summary>
        /// 群組
        /// </summary>
        Category,
        /// <summary>
        /// 是否成功
        /// </summary>
        IsSucc,
        /// <summary>
        /// 索引
        /// </summary>
        Index,
        /// <summary>
        /// 原值
        /// </summary>
        OriginValue,
        /// <summary>
        /// 新值
        /// </summary>
        NewValue,
        /// <summary>
        /// 時戳
        /// </summary>
        Timestamp,
        /// <summary>
        /// 等級
        /// </summary>
        Rank,
        /// <summary>
        /// 門票
        /// </summary>
        Ticket,
        /// <summary>
        /// 列表字串
        /// </summary>
        List,
        /// <summary>
        /// 是否勝利
        /// </summary>
        IsWin,
        #endregion

        #region 帳號資料(300)
        /// <summary>
        /// 玩家資訊
        /// </summary>
        PlayerInfo = 300,
        /// <summary>
        /// 卡片資訊
        /// </summary>
        CardInfo,
        /// <summary>
        /// 道具資訊
        /// </summary>
        ItemInfo,
        /// <summary>
        /// 任務資訊
        /// </summary>
        QuestInfo,
        /// <summary>
        /// 裝備資訊
        /// </summary>
        EquipmentInfo,
        /// <summary>
        /// 消費資訊
        /// </summary>
        PurchaseInfo,
        /// <summary>
        /// 贈獎資訊
        /// </summary>
        GiftInfo,
        /// <summary>
        /// 成就資訊
        /// </summary>
        TrophyInfo,
        /// <summary>
        /// 每日活躍資訊
        /// </summary>
        DailyInfo,
        /// <summary>
        /// 隨機商店資訊
        /// </summary>
        RandomShopInfo,
        /// <summary>
        /// 排名資訊
        /// </summary>
        RankingInfo,
        /// <summary>
        /// IAP資訊
        /// </summary>
        IAPInfo,
        /// <summary>
        /// 協力任務資訊
        /// </summary>
        CooperateMissionInfo,
        /// <summary>
        /// 社群資訊
        /// </summary>
        SocialInfo,
        /// <summary>
        /// 討伐任務資訊
        /// </summary>
        Crusade,
        /// <summary>
        /// 信仰資訊
        /// </summary>
        FaithInfo,
        /// <summary>
        /// 戰術資訊
        /// </summary>
        TacticsInfo,
        /// <summary>
        /// PVP資訊
        /// </summary>
        PVPInfo,
        #endregion
    }
}
