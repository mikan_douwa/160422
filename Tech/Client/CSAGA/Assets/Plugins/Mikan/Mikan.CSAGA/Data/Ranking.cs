﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mikan.CSAGA.Internal
{
    public class RankingInfoObj<T> : DictionaryObj<T>
        where T : RankingObj, new()
    {

        
    }

    public class RankingObj : ISerializable
    {
        public int EventID;

        public DateTime BeginTime;

        public int PlayerCount;

        public int Ranking;

        public int Point;

        public int Next;

        public void Serialize(IOutput output)
        {
            output.Write(EventID);
            output.Write(BeginTime);
            output.Write(PlayerCount);
            output.Write(Ranking);
            output.Write(Point);
            output.Write(Next);
        }

        public void DeSerialize(IInput input)
        {
            EventID = input.ReadInt32();
            BeginTime = input.ReadDateTime();
            PlayerCount = input.ReadInt32();
            Ranking = input.ReadInt32();
            Point = input.ReadInt32();
            Next = input.ReadInt32();
        }
    }

    public class RankingPlayerObj : ISerializable
    {
        public string PublicID;
        public int CardID;
        public int Ranking;
        public int Point;

        public void Serialize(IOutput output)
        {
            output.Write(PublicID);
            output.Write(CardID);
            output.Write(Ranking);
            output.Write(Point);
        }

        public void DeSerialize(IInput input)
        {
            PublicID = input.ReadString();
            CardID = input.ReadInt32();
            Ranking = input.ReadInt32();
            Point = input.ReadInt32();
        }
    }
}
namespace Mikan.CSAGA.Data
{
    public class RankingInfo : Internal.RankingInfoObj<Ranking>
    {
        public RankingInfo()
        {
            // 10分鐘刷新一次
            TimeLimit.Add(GetType(), 60 * 10);
        }
        internal bool IsOverdue
        {
            get { return TimeLimit.IsOverdue(GetType()); }
            set { TimeLimit.SetIsOverdue(GetType(), value); }
        }

        internal Dictionary<string, SocialPlayer> PlayerList;

        public SocialPlayer GetPlayer(string id)
        {
            if (PlayerList == null) return null;
            var obj = DictionaryUtils.SafeGetValueNullable(PlayerList, id);
            if (obj == null || obj.IsOverdue) return null;
            return obj;
        }

        internal void ApplyChange(SyncEventPoint syncData)
        {
            var ranking = this[syncData.EventID];

            if (ranking != null)
                ranking.Point += syncData.Add;
            else
                IsOverdue = true;
        }

        protected override void DeSerialize(IInput input)
        {
            base.DeSerialize(input);
            IsOverdue = false;
        }
    }

    public class Ranking : Internal.RankingObj
    {
        public Dictionary<int, List<RankingPlayer>> Players = new Dictionary<int, List<RankingPlayer>>();
    }

    public class RankingPlayer : Internal.RankingPlayerObj
    {

    }
    
    public class InfinityInfo : LocalObject<InfinityInfo>
    {
        private int eventId;
        private Int64 seed;
        private DateTime beginTime;
        private int point;
        private DateTime updateTime;

        private bool isOverdue = true;

        private List<ValuePair> bonus;

        class Record : ISerializable
        {
            public int Current;
            public DateTime UpdateTime = DateTime.MinValue;

            public void Serialize(IOutput output)
            {
                output.Write(Current);
                output.Write(UpdateTime);
            }

            public void DeSerialize(IInput input)
            {
                Current = input.ReadInt32();
                UpdateTime = input.ReadDateTime();
            }
        }

        private Dictionary<int, Record> list = new Dictionary<int, Record>();

        internal override LocalKey LocalKey { get { return CSAGA.LocalKey.InfinityInfo; } }

        protected override int ServerKey { get { return (int)LocalKey; } }

        protected override void Serialize(IOutput output)
        {
            output.Write(list);
        }

        protected override void DeSerialize(IInput input)
        {
            list = input.ReadInt32Dictionary<Record>();
        }

        public int Current(int mainQuestId, DateTime beginTime)
        {
            var origin = DictionaryUtils.SafeGetValueNullable(list, mainQuestId);

            if (origin == null || origin.UpdateTime < beginTime) return 0;
            return origin.Current;
        }

        public ValuePair Bonus(DateTime currentTime)
        {
			if(isOverdue) return null;
            return Calc.Quest.CalcInfinityBonus(bonus, beginTime, currentTime);
        }

        public int Point(DateTime currentTime)
        {
            if (currentTime.Date != updateTime.Date) return 0;
            return point;
        }

        public void Update(int mainQuestId, int current)
        {
            var origin = DictionaryUtils.SafeGetValue(list, mainQuestId);

            origin.Current = current;
            origin.UpdateTime = Global.Service.CurrentTime;
            Save();
            Sync();
        }

        internal void ApplyChange(SyncInfinity syncData)
        {
            eventId = syncData.EventID;
            seed = syncData.Seed;
            beginTime = syncData.BeginTime;
            point = syncData.Point;
            updateTime = syncData.UpdateTime;

            bonus = Calc.Quest.CalcInfinityBonus(seed);

			isOverdue = false;
        }

        internal void ApplyChange(SyncInfinityPoint syncData)
        {
            if(syncData.Point > point || syncData.UpdateTime.Date != updateTime.Date)
                point = syncData.Point;

            updateTime = syncData.UpdateTime;

        }
    }
}
