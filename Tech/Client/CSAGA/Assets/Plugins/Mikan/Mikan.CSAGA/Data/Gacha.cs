﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mikan.CSAGA.Data
{
    public class GachaInfo
    {
        public IEnumerable<ConstData.Gacha> GetPromotionList(int gachaId)
        {
            var items = new Dictionary<int, ConstData.Gacha>();

            foreach (var item in Global.Tables.Gacha.Select(o => { return o.n_GROUP == gachaId && o.n_SHOWCARD != 0; }))
            {
                var origin = DictionaryUtils.SafeGetValueNullable(items, item.n_CARDID);
                if (origin == null || origin.n_RARE < item.n_RARE) items[item.n_CARDID] = item;
            }

            foreach (var item in items) yield return item.Value;
        }

        public Card GenerateDummy(int gachaId)
        {
            var data = Global.Tables.Gacha[gachaId];
            var card = new Card { Complex = new Complex() };
            card.ID = data.n_CARDID;
            card.Rare = data.n_RARE;
            var balance = Global.Tables.CardBalance[card.Rare];
            card.Exp = CardUtils.CalcExp(card.ID, balance.n_LVMAX);
            return card;
        }
    }
}
