﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mikan.CSAGA.Internal
{
    public class PVPWaveDiff : ISerializable
    {
        public Serial Serial;
        /// <summary>
        /// 從server回傳時才會有內容
        /// </summary>
        public List<int> Skills;
        public List<int> Selections;
        public List<string> Prologs;

        public void Serialize(IOutput output)
        {
            output.Write(Serial);
            output.Write(Skills);
            output.Write(Selections);
            output.Write(Prologs);
        }

        public void DeSerialize(IInput input)
        {
            Serial = input.Read<Serial>();
            Skills = input.ReadInt32List();
            Selections = input.ReadInt32List();
            Prologs = input.ReadStringList();
        }
    }

    public class PVPWave<TCard, TEquipment> : ISerializable
        where TCard : CardObj, new()
        where TEquipment : EquipmentObj, new()
    {
        public TCard Card;
        public List<TEquipment> Equipments;
        public List<int> Skills;
        public List<int> Selections;
        public List<string> Prologs;

        public void Serialize(IOutput output)
        {
            output.Write(Card);
            output.Write(Equipments);
            output.Write(Skills);
            output.Write(Selections);
            output.Write(Prologs);
        }

        virtual public void DeSerialize(IInput input)
        {
            Card = input.Read<TCard>();
            Equipments = input.ReadList<TEquipment>();
            Skills = input.ReadInt32List();
            Selections = input.ReadInt32List();
            Prologs = input.ReadStringList();
        }
    }

    public class PVPPlayerObj<TWave> : ISerializable
        where TWave : ISerializable, new()
    {
        public string Name;
        public int Point;
        public List<TWave> Waves;
        public string Ticket;

        public void Serialize(IOutput output)
        {
            output.Write(Name);
            output.Write(Point);
            output.Write(Waves);
            output.Write(Ticket);
        }

        virtual public void DeSerialize(IInput input)
        {
            Name = input.ReadString();
            Point = input.ReadInt32();
            Waves = input.ReadList<TWave>();
            Ticket = input.ReadString();
        }
    }
}
namespace Mikan.CSAGA.Data
{
    public class PVPInfo : SerializableObject
    {
        public List<int> Skills;
        public int EventID;
        public int Point;
        public int BPConsumed;
        public DateTime BPUpdateTime;
        public List<PVPWave> Waves;
        public List<Internal.PVPWaveDiff> Origins;

        public Card Boss { get; private set; }

        public List<Serial> Cards { get; private set; }

        public List<PVPPlayer> Players { get; private set; }

        private DateTime queryTime = DateTime.MinValue;

        private DateTime updateTime;

        public DateTime QueryTime { get { return queryTime; } }

        public bool IsOverdue
        {
            get
            {
                var schedule = Global.Service.QuestInfo.PVP;
                var eventId = schedule != null ? schedule.EventID : 0;
                if (eventId != EventID) return true;

                return TimeLimit.IsOverdue(GetType());
            }
            set
            {
                TimeLimit.SetIsOverdue(GetType(), value);
            }
        }

        public bool IsQueryOverdue
        {
            get
            {
                return queryTime.AddMinutes(1) < Global.Service.CurrentTime;
            }
        }

        public int BP
        {
            get
            {
                return PVPUtils.CalcBP(BPConsumed, BPUpdateTime, Global.Service.CurrentTime);
            }
        }

        public int CalcBPConsumed(DateTime currentTime)
        {
            return PVPUtils.CalcBPConsumed(BPConsumed, BPUpdateTime, Global.Service.CurrentTime);
        }

        internal void Update(List<Internal.PVPWaveDiff> diffs)
        {
            Origins = diffs;

            if (Origins.Count > 0)
            {
                var waves = PVPUtils.ConvertToWaves(Origins);

                if (Waves != null && Waves.Count == waves.Count)
                {
                    for (int i = 0; i < waves.Count; ++i)
                    {
                        var wave = waves[i];
                        if (wave.Skills == null) wave.Skills = Waves[i].Skills;
                    }
                }

                Waves = waves;

                Cards = new List<Serial>(Waves.Count);
                foreach (var item in Waves) Cards.Add(item.Card.Serial);
                Boss = Waves[Waves.Count - 1].Card;
            }
            else
            {
                Waves = null;
                Cards = null;
                Boss = null;
            }

            TimeLimit.Add(GetType(), 30, false);

            Global.Service.CardInfo.IsProtectListOverdue = true;
        }

        internal void RefreahPlayer(string rawdata)
        {
            Players = ListObj.DeSerialize<PVPPlayer>(rawdata);
            queryTime = Global.Service.CurrentTime;
        }

        protected override void Serialize(IOutput output)
        {
            output.Write(Skills);
            output.Write(EventID);
            output.Write(Point);
            output.Write(BPConsumed);
            output.Write(BPUpdateTime);
            output.Write(Origins);
        }

        protected override void DeSerialize(IInput input)
        {
            Skills = input.ReadInt32List();
            EventID = input.ReadInt32();
            Point = input.ReadInt32();
            BPConsumed = input.ReadInt32();
            BPUpdateTime = input.ReadDateTime();

            var origins = input.ReadList<Internal.PVPWaveDiff>();

            Update(origins);

            Players = new List<PVPPlayer>();
            queryTime = DateTime.MinValue;
        }
    }

    public class PVPWave : Internal.PVPWave<Card, Equipment>
    {
        public override void DeSerialize(IInput input)
        {
            base.DeSerialize(input);
            Card.Serial.target = Card;
            Card.equipments = Equipments;
            Card.FixRare();
        }

    }

    public class PVPPlayer : Internal.PVPPlayerObj<PVPWave>
    {
        public Card Boss;

        public override void DeSerialize(IInput input)
        {
            base.DeSerialize(input);

            Boss = Waves[Waves.Count - 1].Card;
        }
    }
}
namespace Mikan.CSAGA
{
    public class PVPUtils
    {
        public static int CalcBP(int consumed, DateTime updateTime, DateTime currentTime)
        {
            return ConstData.Tables.Instance.BPLimit - CalcBPConsumed(consumed, updateTime, currentTime);
        }

        public static int CalcBPConsumed(int consumed, DateTime updateTime, DateTime currentTime)
        {
            var reserved = BPReserved(updateTime, currentTime);
            var diff = consumed - reserved;
            if (diff < 0) return 0;
            return diff;
        }

        public static int BPReserved(DateTime updateTime, DateTime currentTime)
        {
            return (int)Math.Floor(currentTime.Subtract(updateTime).TotalMinutes / ConstData.Tables.Instance.BPCD);
        }

        public static List<Internal.PVPWaveDiff> ConvertToWaveDiffs(Data.Team team)
        {
            return ConvertToWaveDiffs(team.Members);
        }

        public static List<Internal.PVPWaveDiff> ConvertToWaveDiffs(List<Serial> cards)
        {
            var list = new List<Internal.PVPWaveDiff>(5);
            foreach (var item in cards)
            {
                if (item.IsEmpty) return null;

                var wave = new Internal.PVPWaveDiff();
                wave.Serial = item;
                list.Add(wave);
            }

            list.Reverse();

            return list;
        }

        public static List<Data.PVPWave> ConvertToWaves(List<Internal.PVPWaveDiff> diffs)
        {
            var list = new List<Data.PVPWave>(diffs.Count);

            foreach (var diff in diffs)
            {
                var wave = new Data.PVPWave();
                wave.Card = Global.Service.CardInfo[diff.Serial];
                wave.Equipments = new List<Data.Equipment>(wave.Card.SelectEquipment());
                if (diff.Skills != null && diff.Skills.Count > 0)
                    wave.Skills = diff.Skills;
                wave.Selections = diff.Selections;
                wave.Prologs = diff.Prologs;

                list.Add(wave);
            }

            return list;
        }
    }
}