﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mikan.CSAGA.Internal
{
    public class CardEventCache : ISerializable
    {
        public Serial Serial = Serial.Empty;
        public int InteractiveID;

        public void Serialize(IOutput output)
        {
            output.Write(Serial.Value);
            output.Write(InteractiveID);
        }

        public void DeSerialize(IInput input)
        {
            Serial = Serial.Create(input.ReadInt64());
            InteractiveID = input.ReadInt32();
        }
    }

    public class CardInfoObj<T> : ListObj<T>
        where T : CardObj, new()
    {
        protected List<CardEventCache> cacheList;

        protected Dictionary<int, int> luckList;

        public int GetLuck(int cardId)
        {
            var luck = 0;
            if (luckList.TryGetValue(cardId, out luck)) return luck;
            return 0;
        }

        protected override void Serialize(IOutput output)
        {
            base.Serialize(output);
            output.Write(cacheList);
            output.Write(luckList);
        }

        protected override void DeSerialize(IInput input)
        {
            base.DeSerialize(input);
            cacheList = input.ReadList<CardEventCache>();
            luckList = input.ReadInt32Dictionary();
        }
    }

    public class CardObj : ISerializable
    {
        public Serial Serial = Serial.Empty;
        public int ID;
        public int Exp;
        public int Kizuna;
        public Complex Complex;

        public int Rare
        {
            get { return GetAwakenLV(0); }
            set { SetAwakenLV(0, value); }
        }

        public bool IsLocked
        {
            get { return Complex[ComplexIndex.CardIsLocked]; }
            set { Complex[ComplexIndex.CardIsLocked] = value; }
        }

        public int GetAwakenLV(int group) { return Complex.Get(ComplexIndex.CardAwaken + group * ComplexIndex.CardAwakenLen, ComplexIndex.CardAwakenLen); }

        internal void SetAwakenLV(int group, int lv) { Complex.Set(ComplexIndex.CardAwaken + group * ComplexIndex.CardAwakenLen, ComplexIndex.CardAwakenLen, lv); }

        public int GetPlus(int index) { return Complex.Get(ComplexIndex.CardPlus + index * ComplexIndex.CardPlusLen, ComplexIndex.CardPlusLen); }

        internal void SetPlus(int index, int value) { Complex.Set(ComplexIndex.CardPlus + index * ComplexIndex.CardPlusLen, ComplexIndex.CardPlusLen, value); }

        public bool IsEventFinished(int index) { return Complex[ComplexIndex.CardEvent + index]; }

        public void Serialize(IOutput output)
        {
            output.Write(Serial.Value);
            output.Write(ID);
            output.Write(Exp);
            output.Write(Kizuna);
            output.SafeWrite(Complex);
        }

        public void DeSerialize(IInput input)
        {
            Serial = Serial.Create(input.ReadInt64());
            ID = input.ReadInt32();
            Exp = input.ReadInt32();
            Kizuna = input.ReadInt32();
            Complex = input.SafeRead<Complex>();

            if (Complex == null) Complex = new Complex();
        }
    }
}

namespace Mikan.CSAGA.Data
{
    public class CardInfo : Internal.CardInfoObj<Card>
    {
        internal bool IsProtectListOverdue = true;

        private List<Serial> protectList;

        private bool isIdsOverdue = true;
        private List<int> ids;

        public List<int> IDs
        {
            get
            {
                if (isIdsOverdue)
                {
                    ids = CardUtils.GetIDs(List);
                    isIdsOverdue = false;
                }

                return ids;
            }
        }

        public List<int> GetIDs(IEnumerable<Serial> serials)
        {
            var list = new List<int>();
            foreach (var serial in serials)
            {
                var card = this[serial];
                if (card != null)
                    list.Add(card.ID);
                else
                    list.Add(0);
            }

            return list;
        }

        public Card this[Serial serial]
        {
            get
            {
                if (serial.target != null) return serial.target;

                return List.Find((o) => { return o.Serial == serial; });
            }
        }

        internal Card this[Int64 serial]
        {
            get
            {
                return List.Find((o) => { return o.Serial.Value == serial; });
            }
        }

        public bool IsProtected(Serial serial)
        {
            if (IsProtectListOverdue) RebuildProtectList();
            return protectList.Contains(serial);
        }

        private void RebuildProtectList()
        {
            protectList = new List<Serial>();

            for (int i = 0; i < Global.Service.TeamInfo.Count; ++i)
            {
                var team = Global.Service.TeamInfo[i];

                foreach (var serial in team.Members)
                {
                    if (!protectList.Contains(serial)) protectList.Add(serial);
                }
            }

            if (Global.Service.PVPInfo != null && Global.Service.PVPInfo.Boss != null)
            {
                foreach (var serial in Global.Service.PVPInfo.Cards)
                {
                    if (!protectList.Contains(serial)) protectList.Add(serial);
                }
            }

            IsProtectListOverdue = false;
        }

        internal void ApplyChange(SyncCard syncData)
        {

            if (syncData.AddList != null)
            {
                foreach (var item in syncData.AddList)
                {
                    var card = new Card
                    {
                        Serial = item.Serial,
                        ID = item.ID,
                        Complex = item.Complex,
                        Exp = item.Exp,
                        Kizuna = item.Kizuna
                    };
                    card.FixRare();

                    List.Add(card);

                    GalleryInfo.Instance.Add(item.ID);
                }
            }

            if (syncData.DeleteList != null)
            {
                foreach (var serial in syncData.DeleteList)
                {
                    var card = this[serial];
                    if (card != null) List.Remove(card);
                }
            }

            if (syncData.UpdateList != null)
            {
                foreach (var diff in syncData.UpdateList)
                {
                    if (diff.Contains(PropertyID.Luck))
                    {
                        var cardId = (int)diff.Serial.Value;
                        foreach (var item in diff.Select())
                        {
                            switch (item.Key)
                            {
                                case PropertyID.Luck:
                                    luckList[cardId] = item.Value.NewValue;
                                    foreach (var card in List.FindAll(o => o.ID == cardId)) card.TriggerOnChange();
                                    break;
                            }
                        }
                    }
                    else
                    {
                        var card = this[diff.Serial];
                        if (card != null)
                        {
                            card.ApplyChange(diff);
                            card.FixRare();
                        }
                    }
                }
            }

            isIdsOverdue = true;
        }
        protected override void DeSerialize(IInput input)
        {
            base.DeSerialize(input);

            foreach (var item in cacheList)
            {
                var card = this[item.Serial];
                if (card != null) card.ReservedInteractiveID = item.InteractiveID;
            }

            foreach (var item in List) item.FixRare();
        }

    }

    public class Card : Internal.CardObj
    {
        internal Fellow owner;

        internal List<Equipment> equipments;

        public Fellow Owner { get { return owner; } }

        public bool IsStatic { get { return owner != null; } }

        public bool IsFormal { get { return !Serial.IsEmpty; } }

        public event Action<Card> OnChange;
        public event PropertyChangeDelegate<Card, int> OnPropertyChange;

        private Append awaken;

        private Append plus;

        private ConstData.CardData data;

        internal void TriggerOnChange()
        {
            if (OnChange != null) OnChange(this);
        }

        public ConstData.CardData Data
        {
            get 
            {
                if (data == null) data = Global.Tables.Card[ID];
                return data;
            }
        }

        public int Luck 
        {
            get
            {
                if (owner != null) return owner.Snapshot.Favorite.Luck;
                return Global.Service.CardInfo.GetLuck(ID); 
            } 
        }

        public int ReservedInteractiveID { get; internal set; }

        public int SlotCount
        {
            get
            {
                if (Rare >= 5) return 3;
                if (Rare >= 4) return 2;
                return 1;
            }
        }

        public int Skill1 { get { return GetSkill(0); } }
        public int Skill2 { get { return GetSkill(1); } }

        public int LeaderSkill { get { return GetSkill(2); } }

        public Equipment GetEquipment(int slot)
        {
            if (!IsFormal) return null;

            foreach (var item in SelectEquipment())
            {
                if(item.Slot == slot) return item;
            }

            return null;
        }

        public IEnumerable<Equipment> SelectEquipment()
        {
            if (!IsFormal) return null;
            
            if (owner != null) return owner.Equipments;

            if (equipments != null) return equipments;

            return Global.Service.EquipmentInfo.List.FindAll(o => { return o.CardSerial == Serial; });
        }

        internal void ApplyChange(CardDiff diff)
        {
            foreach (var item in diff.Select())
            {
                switch (item.Key)
                {
                    case PropertyID.Exp:
                        Exp = item.Value.NewValue;
                        break;
                    case PropertyID.Rare:
                        Rare = item.Value.NewValue;
                        break;
                    case PropertyID.Kizuna:
                        Kizuna = item.Value.NewValue;
                        break;
                    // 複合值特別處理,索引-值
                    case PropertyID.Complex:
                        Complex[item.Value.OriginValue] = item.Value.NewValue != 0;
                        break;
                    // 覺醒特別處理,索引-值
                    case PropertyID.Awaken:
                        SetAwakenLV(item.Value.OriginValue, item.Value.NewValue);
                        UpdateAwaken();
                        break;
                    // 加蛋特別處理,索引-值
                    case PropertyID.Plus:
                        SetPlus(item.Value.OriginValue, item.Value.NewValue);
                        UpdatePlus();
                        break;
                }
                if (OnPropertyChange != null) OnPropertyChange(this, item.Key, item.Value.OriginValue, item.Value.NewValue);
            }

            TriggerOnChange();
        }

        internal void OnEquipmentReplace()
        {
            TriggerOnChange();
        }

        internal void FixRare()
        {
            if (Rare > 0) return;
            var data = ConstData.Tables.Instance.Card[ID];
            Rare = data.n_RARE;
        }

        private int GetSkill(int index)
        {
            if (awaken == null) UpdateAwaken();

            var id = awaken.Skill[index];
            if (id == 0)
            {
                if (index < 2) 
                    id = Data.n_SKILL[index];
                else
                    id = Data.n_LEADER_SKILL;
            }

            return id;
        }

        private void UpdateAwaken()
        {
            if (awaken == null)
                awaken = new Append { Charge = new List<float>() };
            else
            {
                awaken.HP = 0;
                awaken.Atk = 0;
                awaken.Rev = 0;
                awaken.Charge.Clear();
            }

            awaken.Skill = new int[] { 0, 0, 0 };

            for (int i = 1; i <= Global.Tables.AwakenCount; ++i)
            {
                var lv = GetAwakenLV(i);
                if (lv == 0) continue;
                var data = Global.Tables.Awaken.SelectFirst(o => { return o.n_CARDID == ID && o.n_GROUP == i && o.n_LV == lv; });
                awaken.HP += data.n_HP;
                awaken.Atk += data.n_ATK;
                awaken.Rev += data.n_REV;
                if (data.n_CHARGE != 0) awaken.Charge.Add(1 - data.n_CHARGE / 100f);
                
                if (data.n_SKILL[0] > 0) awaken.Skill[0] = data.n_SKILL[0];
                if (data.n_SKILL[1] > 0) awaken.Skill[1] = data.n_SKILL[1];
                if (data.n_LEADER_SKILL > 0) awaken.Skill[2] = data.n_LEADER_SKILL;
            }
        }

        private void UpdatePlus()
        {
            if (plus == null) plus = new Append();

            plus.HP = GetPlus(0) * Global.Tables.HPPlus;
            plus.Atk = GetPlus(1) * Global.Tables.AtkPlus;
            plus.Rev = GetPlus(2) * Global.Tables.RevPlus;
        }

        public CardProperty Calc()
        {
            return Calc(Exp);
        }

        public CardProperty Calc(int exp)
        {
            return Calc(exp, Rare, Luck);
        }

        public CardProperty Calc(int exp, int rare, int luck)
        {
            if (luck == 0 && Serial.IsExternal) luck = Luck;

            var data = ConstData.Tables.Instance.Card[ID];
            var balance = ConstData.Tables.Instance.CardBalance[rare];
			var prop = Calc(data, balance, exp, luck);

            if (awaken == null) UpdateAwaken();
            prop.awaken = awaken;

            if (plus == null) UpdatePlus();
            prop.plus = plus;

            return prop;
        }

        public CardProperty CalcByLV(int lv)
        {
            return Calc(CardUtils.CalcExp(ID, lv));
        }

        private static CardProperty Calc(ConstData.CardData data, ConstData.CardBalance balance, int exp, int luck)
        {
            var prop = new CardProperty();
            prop.LV = CardUtils.CalcLV(data, balance, exp);
            prop.LVMax = balance.n_LVMAX;

            if (prop.LV < prop.LVMax)
            {
                prop.Zero = CardUtils.CalcExp(data, prop.LV);
                prop.Next = CardUtils.CalcExp(data, prop.LV + 1);
                prop.ExpProgress = (float)(exp - prop.Zero) / (float)(prop.Next - prop.Zero);
            }
            else
            {
                prop.ExpProgress = 1;
            }

            var hp = CardUtils.CalcProperty(prop.LV, prop.LVMax, balance.n_HP_MIN, balance.n_HP_MAX, data.f_HP, data.f_HP_LVUP, ConstData.Tables.Instance.BasicAspd);
            var atk = CardUtils.CalcProperty(prop.LV, prop.LVMax, balance.n_ATK_MIN, balance.n_ATK_MAX, data.f_ATK, data.f_ATK_LVUP, data.f_CHARGE);
            var rev = CardUtils.CalcProperty(prop.LV, prop.LVMax, balance.n_REV_MIN, balance.n_REV_MAX, data.f_REV, data.f_REV_LVUP, data.f_CHARGE);
            var charge = data.f_CHARGE;
            
            #region 神運效果
            
			if(luck == 0) luck = Global.Service.CardInfo.GetLuck(data.n_ID);
            
            var factor = luck > 0 ? Global.Tables.Player[luck].n_LUCK_ABILITY * 0.01f : 0f;

            prop.HP = hp + (int)Math.Ceiling(hp * factor);
            prop.Atk = atk + (int)Math.Ceiling(atk * factor);
            prop.Rev = rev + (int)Math.Ceiling(rev * factor);
            prop.Charge = charge * (1f - factor);
            
            #endregion

            return prop;
        }

        internal class Append
        {
            public int HP;
            public int Atk;
            public int Rev;
            public List<float> Charge;
            public int[] Skill;
        }
    }

    public class CardProperty
    {
        public int LV;
        public int LVMax;
        public float ExpProgress;
        public int Zero;
        public int Next;

        public int HP
        {
            get { return Math.Max(1, hp + awaken.HP + plus.HP + HPDiff); }
            internal set { hp = value; }
        }
        public int Atk
        {
            get { return Math.Max(1, atk + awaken.Atk + plus.Atk + AtkDiff); }
            internal set { atk = value; }
        }
        public int Rev
        {
            get { return Math.Max(1, rev + awaken.Rev + plus.Rev + RevDiff); }
            internal set { rev = value; }
        }
        public float Charge
        {
            get
            {
                var tmp = charge;

                foreach (var item in awaken.Charge) tmp *= item;

                if (ChargeDiff == null || ChargeDiff.Count == 0) return tmp;
                
                foreach (var item in ChargeDiff) tmp *= item;
                return tmp;
            }
            internal set { charge = value; }
        }

        public int HPDiff { get; internal set; }
        public int AtkDiff { get; internal set; }
        public int RevDiff { get; internal set; }
        public int HPPlus { get { return plus.HP; } }
        public int AtkPlus { get { return plus.Atk; } }
        public int RevPlus { get { return plus.Rev; } }
        public List<float> ChargeDiff { get; internal set; }

        private int hp;
        private int atk;
        private int rev;
        private float charge;

        internal Card.Append awaken;
        internal Card.Append plus;

        public void Append(IEnumerable<Equipment> list)
        {
            HPDiff = 0;
            AtkDiff = 0;
            RevDiff = 0;

            ChargeDiff = new List<float>(1);

            foreach (var item in list)
            {
                HPDiff += item.Hp;
                AtkDiff += item.Atk;
                RevDiff += item.Rev;
                if (item.Chg != 0) ChargeDiff.Add(1 - item.Chg / 100f);
            }
        }
    }

}
