﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mikan.CSAGA.Data
{
    /// <summary>
    /// 玩家帳號資訊
    /// </summary>
    public class Profile : SerializableObject
    {
        /// <summary>
        /// 公開ID
        /// </summary>
        public string PublicID { get; private set; }

        /// <summary>
        /// 帳號識別碼
        /// </summary>
        public string UUID { get; private set; }

        /// <summary>
        /// 通訊用金鑰
        /// </summary>
        public int Key { get; private set; }

        /// <summary>
        /// 創建新的帳號資訊實體
        /// </summary>
        /// <param name="pid">公開ID</param>
        /// <param name="uuid">帳號識別碼</param>
        /// <param name="key">通訊用金鑰</param>
        /// <returns></returns>
        public static Profile Create(string pid, string uuid, int key)
        {
            var profile = new Profile();
            profile.PublicID = pid;
            profile.UUID = uuid;
            profile.Key = key;
            return profile;
        }

        /// <inheritdoc />
        protected override void Serialize(IOutput output)
        {
            output.Write(Key);
            output.Write(PublicID);
            output.Write(UUID);
        }

        /// <inheritdoc />
        protected override void DeSerialize(IInput input)
        {
            Key = input.ReadInt32();

            PublicID = input.ReadString();

            UUID = input.ReadString();
        }
    }

    public class TransferCache : SerializableObject
    {
        public string PublicID;
        public string Token;
        public string Profile;
        public int Key;

        protected override void Serialize(IOutput output)
        {
            output.Write(PublicID);
            output.Write(Token);
            output.Write(Profile);
            output.Write(Key);
        }

        protected override void DeSerialize(IInput input)
        {
            PublicID = input.ReadString();
            Token = input.ReadString();
            Profile = input.ReadString();
            Key = input.ReadInt32();
        }
    }

    internal class BackupInfo : LocalObject<BackupInfo>
    {
        private bool isSyncComplete;

        public void Sync(List<Backup> list)
        {
            foreach (var item in list)
            {
                switch ((LocalKey)item.Key)
                {
                    case CSAGA.LocalKey.InfinityInfo:
                        InfinityInfo.Instance.DeSerialize(item.Data);
                        break;
                }
            }

            isSyncComplete = true;
            Save();
        }

        internal override LocalKey LocalKey { get { return CSAGA.LocalKey.BackupInfo; } }

        protected override void Serialize(IOutput output)
        {
            output.Write(isSyncComplete);
        }

        protected override void DeSerialize(IInput input)
        {
            isSyncComplete = input.ReadBoolean();
        }
    }
}
