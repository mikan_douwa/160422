﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mikan.CSAGA.Internal
{
    public class FaithInfoObj<T> : ListObj<T>
        where T : FaithObj, new()
    {
        public DateTime ClaimTime;

        protected override void Serialize(IOutput output)
        {
            base.Serialize(output);
            output.Write(ClaimTime);
        }
        protected override void DeSerialize(IInput input)
        {
            base.DeSerialize(input);
            ClaimTime = input.ReadDateTime();
        }
    }

    public class FaithObj : ISerializable
    {
        /// <summary>
        /// DB流水號
        /// </summary>
        public Int64 Serial;

        /// <summary>
        /// 索引
        /// </summary>
        public int Index;

        /// <summary>
        /// 結果
        /// </summary>
        public int Result;

        /// <summary>
        /// 結束時間
        /// </summary>
        public DateTime EndTime;


        /// <inheritdoc />
        public void Serialize(IOutput output)
        {
            output.Write(Serial);
            output.Write(Index);
            output.Write(Result);
            output.Write(EndTime);
        }

        /// <inheritdoc />
        public void DeSerialize(IInput input)
        {
            Serial = input.ReadInt64();
            Index = input.ReadInt32();
            Result = input.ReadInt32();
            EndTime = input.ReadDateTime();
        }
    }
}

namespace Mikan.CSAGA.Data
{
    /// <summary>
    /// 系統獎勵資訊
    /// </summary>
    public class FaithInfo : Internal.FaithInfoObj<Faith>
    {
        internal bool IsOverdue
        {
            get { return TimeLimit.IsOverdue(GetType()); }
            set { TimeLimit.SetIsOverdue(GetType(), value); }
        }

        public FaithInfo()
        {
            TimeLimit.Add(GetType(), 60 * 60);
        }

        internal void ApplyChange(SyncFaith syncData)
        {
            foreach (var item in syncData.List)
            {
                var obj = default(Faith);
                foreach (var origin in List)
                {
                    if (origin.Index == item.Index)
                    {
                        obj = origin;
                        break;
                    }
                }

                if (obj == null)
                {
                    obj = new Faith();
                    obj.Index = item.Index;
                    List.Add(obj);
                }

                obj.Serial = item.Serial;
                obj.Result = item.Result;
                obj.EndTime = item.EndTime;
            }
        }

        protected override void DeSerialize(IInput input)
        {
            base.DeSerialize(input);

            IsOverdue = false;
        }

    }

    /// <summary>
    /// 信仰資料
    /// </summary>
    public class Faith : Internal.FaithObj
    {
    }
}