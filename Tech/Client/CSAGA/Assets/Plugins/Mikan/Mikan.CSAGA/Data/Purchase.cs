﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mikan.CSAGA.Internal
{
    public class PurchaseInfoObj<T> : SerializableObject
        where T : ArticleObj, new()
    {
        public int IAPGem;

        private int gem;

        /// <summary>
        /// 剩餘寶石
        /// </summary>
        public int Gem
        {
            get { return gem; }
            set
            {
                if (gem == value) return;
                var oldValue = gem;
                gem = value;
                onGemChange(oldValue, value);
            }
        }

        /// <summary>
        /// 消費列表
        /// </summary>
        public List<T> List;

        /// <inheritdoc />
        protected override void Serialize(IOutput output)
        {
            output.Write(Gem);
            output.Write(IAPGem);
            output.Write(List);
        }

        /// <inheritdoc />
        protected override void DeSerialize(IInput input)
        {
            Gem = input.ReadInt32();
            IAPGem = input.ReadInt32();
            List = input.ReadList<T>();
        }

        /// <inheritdoc />
        virtual protected void onGemChange(int oldValue, int newValue) { }
    }

    /// <summary>
    /// 商品資訊
    /// </summary>
    public class ArticleObj : ISerializable
    {
        /// <summary>
        /// 商品ID
        /// </summary>
        public ArticleID ID;

        /// <summary>
        /// 數量
        /// </summary>
        public int Count;

        public DateTime LastPurchase = DateTime.MinValue;

        /// <inheritdoc />
        public void Serialize(IOutput output)
        {
            output.Write((byte)ID);
            output.Write(Count);
            output.Write(LastPurchase);
        }

        /// <inheritdoc />
        public void DeSerialize(IInput input)
        {
            ID = (ArticleID)input.ReadByte();
            Count = input.ReadInt32();
            LastPurchase = input.ReadDateTime();
        }
    }
}

namespace Mikan.CSAGA.Data
{
    /// <summary>
    /// 消費資訊
    /// </summary>
    public class PurchaseInfo : Internal.PurchaseInfoObj<Article>
    {
        /// <summary>
        /// 持有寶石數量變更事件
        /// </summary>
        public event PropertyChangeDelegate<PurchaseInfo, int> OnGemChange;

        protected override void onGemChange(int oldValue, int newValue)
        {
            if (OnGemChange != null) OnGemChange(this, PropertyID.Gem, oldValue, newValue);
        }

        public Article this[ArticleID id]
        {
            get
            {
                foreach (var item in List)
                {
                    if (item.ID == id) return item;
                }
                return null;
            }
        }

        public int Count(ArticleID id)
        {
            var item = this[id];
            if (item == null) return 0;
            return item.Count;
        }

        internal void Set(ArticleID id, int count)
        {
            var article = this[id];
            if (article != null)
                article.Count = count;
            else
            {
                article = new Article();
                article.ID = id;
                article.Count = count;
                List.Add(article);
            }

            article.LastPurchase = Global.Service.CurrentTime;
        }
        internal void Add(ArticleID id)
        {
            var article = this[id];
            if (article != null)
                ++article.Count;
            else
            {
                article = new Article();
                article.ID = id;
                article.Count = 1;
                List.Add(article);
            }

            article.LastPurchase = Global.Service.CurrentTime;
        }
    }

    /// <summary>
    /// 商品資訊
    /// </summary>
    public class Article : Internal.ArticleObj
    {

    }
}