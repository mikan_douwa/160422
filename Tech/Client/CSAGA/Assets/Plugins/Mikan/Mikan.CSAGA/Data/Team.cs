﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mikan.CSAGA.Data
{
    public class TeamInfo : LocalObject<TeamInfo>
    {
        public event Callback OnChange;

        public List<Team> List;

        private int cachedIndex = -1;

        internal Serial Header = Serial.Empty;

        public Team Current { get { return this[Selection]; } }

        public int Count
        {
            get
            {
                return Global.Service.PlayerInfo.Data.n_TEAM;

            }
        }

        public int Selection { get; internal set; }

        public Team this[int index]
        {
            get
            {
                if (List != null)
                {
                    foreach (var item in List)
                    {
                        if (item.Index == index) return Fix(item);
                    }
                }

                var tmp = new Team { Index = index, IsEmpty = true };

                if (index == 0 && IsEmpty)
                {
                    for (int i = 0; i < 5; ++i)
                    {
                        if (i < Global.Service.CardInfo.List.Count)
                            tmp.Add(Global.Service.CardInfo.List[i].Serial);
                        else
                            tmp.Add(Serial.Empty);
                    }

                    Apply(tmp);
                }
                else
                {
                    tmp.Add(Global.Service.CardInfo.List[0].Serial);
                    for (int i = 1; i < 5; ++i) tmp.Add(Serial.Empty);
                }

                return Fix(tmp);
            }
        }

        internal override LocalKey LocalKey { get { return LocalKey.TeamInfo; } }

        protected override void Serialize(IOutput output)
        {
            output.Write(Selection);
            output.Write(List);
        }

        protected override void DeSerialize(IInput input)
        {
            Selection = input.ReadInt32();
            List = input.ReadList<Team>();
            if (List.Count > 0)
                Header = List[0].Members[0];
            else
                Header = Serial.Empty;
        }

        public void ApplyChange()
        {
            Save();

            if (List.Count > 0)
            {
                var header = List[0].Members[0];
                if (header != Header)
                {
                    Global.Service.SendCommand(new Commands.Card.HeaderRequest { Serial = header.Value });
                }
            }
        }

        internal void Update(int index, List<Serial> members)
        {
            var item = this[index];

            item.Replace(members);

            ApplyAndTrigger(item);
        }

        internal void Update(int index, int memberIndex, Serial serial)
        {
            var item = this[index];

            item.Replace(memberIndex, serial);

            ApplyAndTrigger(item);
        }

        internal void Cache(int index, int memberIndex, Fellow fellow)
        {
            var item = this[index];
            cachedIndex = index;
            item.Cache(memberIndex, fellow);
            Apply(item);
        }

        internal void Reset()
        {
            if (cachedIndex > -1)
            {
                this[cachedIndex].Reset();
                cachedIndex = -1;
            }
        }

        private void Apply(Team item)
        {
            if (item.IsEmpty)
            {
                item.IsEmpty = false;
                if (List == null) List = new List<Team>();
                List.Add(item);
            }

            Global.Service.CardInfo.IsProtectListOverdue = true;
        }

        private void ApplyAndTrigger(Team item)
        {
            Apply(item);

            if (OnChange != null) OnChange();
        }

        private Team Fix(Team origin)
        {
            origin.Fix();
            

            return origin;
        }
    }

    public class Team : ISerializable
    {
        public int Index;

        private int cachedIndex = -1;
        private Fellow cachedFellow;
        public List<Serial> Members
        {
            get
            {
                var list = new List<Serial>(members.Count);

                for (int i = 0; i < members.Count; ++i)
                {
                    if (i == cachedIndex)
                        list.Add(cachedFellow.Card.Serial);
                    else
                    {

                        var item = members[i];
                        list.Add(Serial.Create(item));
                    }

                }

                return list;
            }
        }

        internal List<Int64> Rawdata { get { return members; } }

        private List<Int64> members;
        internal bool IsEmpty = false;

        internal void Add(Serial serial)
        {
            if (members == null) members = new List<long>(5);

            members.Add(serial.Value);
        }

        internal void Replace(List<Serial> list)
        {
            for (int i = 0; i < 5; ++i) Replace(i, list[i]);
        }

        internal void Replace(int index, Serial serial)
        {
            members[index] = serial.Value;

            if (index == cachedIndex) Reset();
        }

        internal void Fix()
        {
            var exists = new List<Int64>(members.Count);

            for (int i = 0; i < members.Count; ++i)
            {
                var serial = members[i];

                if (serial == 0) continue;

                if (!exists.Contains(serial))
                {
                    var card = Global.Service.CardInfo[serial];
                    if (card != null)
                    {
                        exists.Add(serial);
                        continue;
                    }
                }

                members[i] = 0;
            }

            if (members[0] == 0)
            {
                for (int i = 0; i < members.Count; ++i) members[i] = 0;
                members[0] = Global.Service.CardInfo.List[0].Serial.Value;
            }
        }

        internal void Cache(int index, Fellow fellow)
        {
            cachedIndex = index;
            cachedFellow = fellow;
        }

        internal void Reset()
        {
            cachedIndex = -1;
            cachedFellow = null;
        }

        public bool Contains(Serial serial)
        {
            if (serial.IsEmpty) return false;
            return members.Contains(serial.Value);
        }

        public bool Contains(Card card)
        {
            if (card == null) return false;
            return Contains(card.Serial);
        }

        public void Serialize(IOutput output)
        {
            output.Write(Index);
            output.Write(members);
        }

        public void DeSerialize(IInput input)
        {
            Index = input.ReadInt32();
            members = input.ReadInt64List();
        }
    }
}
