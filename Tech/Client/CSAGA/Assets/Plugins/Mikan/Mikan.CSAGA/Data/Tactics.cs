﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mikan.CSAGA.Internal
{
    public class TacticsInfoObj : SerializableObject
    {
        public List<int> List;

        protected override void Serialize(IOutput output)
        {
            output.Write(List);
        }

        protected override void DeSerialize(IInput input)
        {
            List = input.ReadInt32List();
        }
    }
}

namespace Mikan.CSAGA.Data
{
    public class TacticsInfo : Internal.TacticsInfoObj
    {
        internal void ApplyChange(SyncTactics syncData)
        {
            List.Add(syncData.TacticsID);
        }
    }

}