﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mikan.CSAGA.Internal
{
    public class SocialPlayerObj : ISerializable
    {
        public string PublicID;
        public string Name;

        public void Serialize(IOutput output)
        {
            output.Write(PublicID);
            output.Write(Name);
        }

        public void DeSerialize(IInput input)
        {
            PublicID = input.ReadString();
            Name = input.ReadString();
        }
    }

    public class FellowObj : ISerializable
    {
        public string PublicID;
        public ComplexI32 Complex;

        public void Serialize(IOutput output)
        {
            output.Write(PublicID);
            output.Write(Complex);
        }

        public void DeSerialize(IInput input)
        {
            PublicID = input.ReadString();
            Complex = input.Read<ComplexI32>();
        }
    }

    public class SocialInfoObj<T> : ListObj<T>
        where T : FellowObj, new()
    {
        
    }

    public class Snapshot : ISerializable
    {
        public string ID;
        public DateTime UpdateTime;
        public string Data;

        public void Serialize(IOutput output)
        {
            output.Write(ID);
            output.Write(UpdateTime); 
            output.Write(Data);
        }

        public void DeSerialize(IInput input)
        {
            ID = input.ReadString();
            UpdateTime = input.ReadDateTime();
            Data = input.ReadString();
        }
    }

    public class SnapshotObj<TCard> : SerializableObject
        where TCard : CardObj, new()
    {
        public class Card : ISerializable
        {
            public TCard Obj;
            public int Luck;

            public void Serialize(IOutput output)
            {
                output.Write(Obj);
                output.Write(Luck);
            }

            public void DeSerialize(IInput input)
            {
                Obj = input.Read<TCard>();
                Luck = input.ReadInt32();
            }
        }

        public class Equipment : Internal.EquipmentBase, ISerializable
        {
            public void Serialize(IOutput output)
            {
                output.Write(list);
            }

            public void DeSerialize(IInput input)
            {
                list = input.Read<PropertyCollection>();
            }
        }

        public string Name;

        public int Exp;

        public Card Favorite;
        public List<Equipment> Equipments;

        protected override void Serialize(IOutput output)
        {
            output.Write(Name);
            output.Write(Exp);
            output.Write(Favorite);
            output.Write(Equipments);
        }

        protected override void DeSerialize(IInput input)
        {
            Name = input.ReadString();
            Exp = input.ReadInt32();
            Favorite = input.Read<Card>();
            Equipments = input.ReadList<Equipment>();
        }
    }

}

namespace Mikan.CSAGA.Data
{
    public class SocialInfo : Internal.SocialInfoObj<Fellow>
    {
        public SocialInfo()
        {
            // 5分鐘刷新一次
            TimeLimit.Add(GetType(), 60 * 5);
        }

        internal bool IsOverdue
        {
            get { return TimeLimit.IsOverdue(GetType()); }
            set { TimeLimit.SetIsOverdue(GetType(), value); }
        }

        public int FollowCount
        {
            get 
            {
                var count = 0;
                foreach (var item in List)
                {
                    if (item.Complex[0]) ++count;
                }

                return count;
            }

        }

        internal DateTime PurchaseHistoryUpdateTime = DateTime.MinValue;

        internal List<PurchaseHistory> PurchaseHistoryList;

        internal List<int> GetPurchaseHistory(DateTime currentTime)
        {
            var list = new List<int>();

            var today = currentTime.Date;

            foreach (var item in PurchaseHistoryList)
            {
                if (item.Timestamp >= today) list.Add(item.ID);
            }

            return list;
        }

        internal DateTime RandomListUpdateTime = DateTime.MinValue;

        internal List<string> RandomList;

        private Dictionary<string, Fellow> map = new Dictionary<string, Fellow>();

        public Fellow this[string id]
        {
            get
            {
                return DictionaryUtils.SafeGetValueNullable(map, id);
            }
        }

        internal void Update(Internal.Snapshot snapshot)
        {
            var fellow = this[snapshot.ID];

            if (fellow == null)
            {
                fellow = new Fellow { PublicID = snapshot.ID, Complex = new ComplexI32() };
                map[snapshot.ID] = fellow;
            }

            fellow.ApplyChange(snapshot.Data, snapshot.UpdateTime);
        }

        internal void ApplyChange(SyncSocialPurchase purchase)
        {
            if (PurchaseHistoryList == null) return;
            var item = PurchaseHistoryList.Find(o => o.ID == purchase.AricleID);
            if (item != null)
                item.Timestamp = purchase.Timestamp;
            else
                PurchaseHistoryList.Add(new PurchaseHistory { ID = purchase.AricleID, Timestamp = purchase.Timestamp });
        }

        protected override void DeSerialize(IInput input)
        {
            base.DeSerialize(input);

            foreach (var item in List) map[item.PublicID] = item;

            IsOverdue = false;
        }

        internal void Add(Fellow fellow)
        {
            var origin = List.Find(o => o.PublicID == fellow.PublicID);

            if (origin == null) List.Add(fellow);
        }
    }

    public class SocialPlayer : Internal.SocialPlayerObj
    {
        internal DateTime UpdateTime;

        public bool IsOverdue { get { return UpdateTime.AddMinutes(10) < Global.Service.CurrentTime; } }
    }

    
    public class Fellow : Internal.FellowObj
    {
        public string Name { get; private set; }
        public DateTime LastPlayTime { get; private set; }
        public Snapshot Snapshot;
        public int Point
        {
            get
            {
                if (FellowCache.Instance.Contains(PublicID, Global.Service.CurrentTime)) return 0;
                if (Relation == CSAGA.Relation.CrossFollow) return Global.Tables.GetVariable(VariableID.FP_FOLLOW);
                return Global.Tables.GetVariable(VariableID.FP_NORMAL);
            }
        }

        public Card Card;

        public List<Equipment> Equipments;

        public Relation Relation
        {
            get
            {
                if (Complex[0])
                {
                    if (Complex[16]) return CSAGA.Relation.CrossFollow;

                    return CSAGA.Relation.Follow;
                }
                else
                {
                    if (Complex[16]) return CSAGA.Relation.Fans;

                    return CSAGA.Relation.None;
                }
            }
        }

        internal DateTime UpdateTime = DateTime.MinValue;

        internal bool IsOverdue { get { return UpdateTime.AddMinutes(3) < Global.Service.CurrentTime; } }

        internal void ApplyChange(string text, DateTime lastPlayTime)
        {
           
            if (Snapshot == null) Snapshot = new Snapshot();
            Snapshot.DeSerialize(text);

            Name = Snapshot.Name;
            LastPlayTime = lastPlayTime;

            Card = Snapshot.Favorite.Obj;
            Card.owner = this;
            Card.Serial.target = Card;
            Card.FixRare();

            Equipments = new List<Equipment>(Snapshot.Equipments.Count);
            foreach (var item in Snapshot.Equipments)
            {
                var copy = new Equipment();
                copy.list = item.list;
                copy.Slot = item.Slot;
                copy.CardSerial = Card.Serial;
                Equipments.Add(copy);
            }

            UpdateTime = Global.Service.CurrentTime;
        }

        
    }

    public class Snapshot : Internal.SnapshotObj<Card>
    {

    }

    internal class FellowCache : LocalObject<FellowCache>
    {
        public DateTime Date = DateTime.MinValue;

        public List<string> IDs = new List<string>();

        public bool Add(string id, DateTime now)
        {
            if (Date != now.Date)
            {
                Date = now.Date;
                IDs = new List<string>();
            }

            if (IDs.Contains(id)) return false;

            IDs.Add(id);

            Save();

            return true;
        }

        public bool Contains(string id, DateTime now)
        {
            if (Date != now.Date) return false;
            return IDs.Contains(id);
        }

        internal override LocalKey LocalKey
        {
            get { return LocalKey.FellowCache; }
        }

        protected override void Serialize(IOutput output)
        {
            output.Write(Date);
            output.Write(IDs);
        }

        protected override void DeSerialize(IInput input)
        {
            Date = input.ReadDateTime();
            IDs = input.ReadStringList();
        }
    }


    public class PurchaseHistory : ISerializable
    {
        public int ID;
        public DateTime Timestamp;

        public void Serialize(IOutput output)
        {
            output.Write(ID);
            output.Write(Timestamp);
        }

        public void DeSerialize(IInput input)
        {
            ID = input.ReadInt32();
            Timestamp = input.ReadDateTime();
        }
    }
}