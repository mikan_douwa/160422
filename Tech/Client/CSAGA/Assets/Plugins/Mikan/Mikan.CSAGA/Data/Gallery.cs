﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mikan.CSAGA.Data
{
    public class GalleryInfo : LocalObject<GalleryInfo>, IEventTrigger
    {
        public List<int> List = new List<int>();

        public bool HasNewData { get; private set; }

        internal override LocalKey LocalKey { get { return LocalKey.GalleryInfo; } }

        protected override void Serialize(IOutput output)
        {
            output.Write(List);
        }

        protected override void DeSerialize(IInput input)
        {
            List = input.ReadInt32List();
        }

        internal void Add(int cardId)
        {
            if (List.Contains(cardId)) return;
            List.Add(cardId);
            HasNewData = true;
        }

        public void TriggerEvent()
        {
            if (!HasNewData) return;

            Global.Service.SendCommand(new Commands.Gallery.SyncRequest { Count = List.Count });
            HasNewData = false;
            Save();
        }
    }
}
