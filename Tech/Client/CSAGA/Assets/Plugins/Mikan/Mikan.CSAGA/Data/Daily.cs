﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mikan.CSAGA.Internal
{
    public class DailyInfoObj : SerializableObject
    {
        public DateTime UpdateTime;
        public int Consumed;
        public int Add;
        public int Cached;

        public List<int> List;
        public ComplexI32 State;

        protected override void Serialize(IOutput output)
        {
            output.Write(UpdateTime);
            output.Write(Consumed);
            output.Write(Add);
            output.Write(List);
            output.Write(State);
            output.Write(Cached);
        }

        protected override void DeSerialize(IInput input)
        {
            UpdateTime = input.ReadDateTime();
            Consumed = input.ReadInt32();
            Add = input.ReadInt32();
            List = input.ReadInt32List();
            State = input.Read<ComplexI32>();
            Cached = input.ReadInt32();
        }
    }
}

namespace Mikan.CSAGA.Data
{
    public class DailyInfo : Internal.DailyInfoObj
    {
        private bool isOverdue = true;
        internal bool IsOverdue
        {
            get
            {
                return isOverdue || (UpdateTime.Date != Global.Service.CurrentTime.Date);
            }

            set
            {
                isOverdue = value;
            }
        }

        public int Remains 
        { 
            get 
            {
                if (UpdateTime.Date != Global.Service.CurrentTime.Date) return Global.Tables.DailyActiveCount;
                return Global.Tables.DailyActiveCount - Consumed + Add; 
            } 
        }

        public bool GetSlotState(int index)
        {
            if (UpdateTime.Date != Global.Service.CurrentTime.Date) return false;
            return State[index];
        }


        protected override void DeSerialize(IInput input)
        {
            base.DeSerialize(input);
            isOverdue = false;
        }
    }
}
