﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mikan.CSAGA.Internal
{
    public class GiftInfoObj<T> : ListObj<T>
        where T : GiftObj, new() { }

    public class GiftObj : ISerializable
    {
        /// <summary>
        /// DB流水號
        /// </summary>
        public Int64 Serial;

        /// <summary>
        /// 過期時間
        /// </summary>
        public DateTime ExpireTime;

        /// <summary>
        /// 類型
        /// </summary>
        public DropType Type;

        /// <summary>
        /// 數值1
        /// </summary>
        public int Value1;

        /// <summary>
        /// 數值2
        /// </summary>
        public int Value2;

        /// <summary>
        /// 描述
        /// </summary>
        public string Description;

        /// <inheritdoc />
        public void Serialize(IOutput output)
        {
            output.Write(Serial);
            output.Write(ExpireTime);
            output.Write((int)Type);
            output.Write(Value1);
            output.Write(Value2);
            output.Write(Description);
        }

        /// <inheritdoc />
        public void DeSerialize(IInput input)
        {
            Serial = input.ReadInt64();
            ExpireTime = input.ReadDateTime();
            Type = (DropType)input.ReadInt32();
            Value1 = input.ReadInt32();
            Value2 = input.ReadInt32();
            Description = input.ReadString();
        }
    }
}

namespace Mikan.CSAGA.Data
{
    /// <summary>
    /// 系統獎勵資訊
    /// </summary>
    public class GiftInfo : Internal.GiftInfoObj<Gift>
    {
        internal bool IsOverdue
        {
            get { return TimeLimit.IsOverdue(GetType()); }
            set { TimeLimit.SetIsOverdue(GetType(), value); }
        }

        public GiftInfo()
        {
            TimeLimit.Add(GetType(), 60);
        }

        internal void Remove(Int64 serial)
        {
            var item = List.Find(o => { return o.Serial == serial; });
            if (item != null) List.Remove(item);
        }

        protected override void DeSerialize(IInput input)
        {
            base.DeSerialize(input);

            IsOverdue = false;
        }

    }

    /// <summary>
    /// 獎勵資料
    /// </summary>
    public class Gift : Internal.GiftObj
    {
    }
}