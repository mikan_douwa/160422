﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mikan.CSAGA.Internal
{

    public class EquipmentBase
    {
        public PropertyCollection list = new PropertyCollection();

        public int ID
        {
            get { return this[PropertyID.ID]; }
            set { this[PropertyID.ID] = value; }
        }

        public int Hp
        {
            get { return this[PropertyID.Hp]; }
            set { this[PropertyID.Hp] = value; }
        }

        public int Atk
        {
            get { return this[PropertyID.Atk]; }
            set { this[PropertyID.Atk] = value; }
        }

        public int Rev
        {
            get { return this[PropertyID.Rev]; }
            set { this[PropertyID.Rev] = value; }
        }

        public int Chg
        {
            get { return this[PropertyID.Chg]; }
            set { this[PropertyID.Chg] = value; }
        }

        public int Slot
        {
            get { return this[PropertyID.Slot]; }
            set { this[PropertyID.Slot] = value; }
        }

        public int EffectID
        {
            get { return this[PropertyID.EffectID]; }
            set { this[PropertyID.EffectID] = value; }
        }

        public int Counter
        {
            get { return this[PropertyID.Counter]; }
            set { this[PropertyID.Counter] = value; }
        }

        public int CounterType
        {
            get { return this[PropertyID.CounterType]; }
            set { this[PropertyID.CounterType] = value; }
        }

        public int this[PropertyID id]
        {
            get { return list[id]; }
            set { list[id] = value; }
        }
    }

    public class EquipmentObj : EquipmentBase, ISerializable
    {
        public Int64 Serial;
        public Mikan.Serial CardSerial = Mikan.Serial.Empty;

        public bool IsLocked
        {
            get { return CardSerial.IsEmpty && Slot > 0; }
        }

        public void Serialize(IOutput output)
        {
            output.Write(Serial);
            output.Write(CardSerial.Value);
            output.Write(list);
        }

        public void DeSerialize(IInput input)
        {
            Serial = input.ReadInt64();
            CardSerial = Mikan.Serial.Create(input.ReadInt64());
            list = input.Read<PropertyCollection>();
        }
    }

    public class EquipmentInfoObj<T> : ListObj<T>
        where T : EquipmentObj, new()
    {
        
    }
}

namespace Mikan.CSAGA.Data
{
    public class EquipmentInfo : Internal.EquipmentInfoObj<Equipment>
    {
        public Equipment this[Int64 serial]
        {
            get
            {
                return List.Find(o => { return o.Serial == serial; });
            }
        }

        internal void ApplyChange(SyncEquipment syncData)
        {
            if (syncData.AddList != null)
            {
                foreach (var item in syncData.AddList)
                {
                    var obj = new Equipment
                    {
                        Serial = item.Serial,
                        ID = item.ID,
                        CardSerial = item.CardSerial,
                        Hp = item.Hp,
                        Atk = item.Atk,
                        Rev = item.Rev,
                        Chg = item.Chg,
                        Slot = item.Slot,
                        EffectID = item.EffectID,
                        Counter = item.Counter,
                        CounterType = item.CounterType
                    };

                    Add(obj);
                }
            }

            if (syncData.DeleteList != null)
            {
                foreach (var serial in syncData.DeleteList)
                {
                    var obj = this[serial];
                    if (obj != null) Remove(obj);
                }
            }

            if (syncData.UpdateList != null)
            {
                foreach (var diff in syncData.UpdateList)
                {
                    var obj = this[diff.Serial];
                    if (obj != null) obj.ApplyChange(diff);
                }
            }
        }

        private void Add(Equipment obj)
        {
            List.Add(obj);
        }

        private void Remove(Equipment obj)
        {
            List.Remove(obj);
        }
    }

    public class Equipment : Internal.EquipmentObj
    {
        internal void ApplyChange(EquipmentDiff diff)
        {
            if (diff.CardSerial != null) CardSerial = Mikan.Serial.Create(diff.CardSerial.NewValue);
            foreach (var item in diff.Select()) this[item.Key] = item.Value.NewValue;
        }
    }
}

namespace Mikan.CSAGA
{
    public class EquipmentDiff : PropertyDiff
    {
        public Int64 Serial;

        public Int64ValuePair CardSerial;

        public override void Serialize(IOutput output)
        {
            base.Serialize(output);
            output.Write(Serial);
            output.SafeWrite(CardSerial);
        }

        public override void DeSerialize(IInput input)
        {
            base.DeSerialize(input);
            Serial = input.ReadInt64();
            CardSerial = input.SafeRead<Int64ValuePair>();
        }

        public static EquipmentDiff Create(Int64 serial, PropertyID id, int oldValue, int newValue)
        {
            var diff = new EquipmentDiff();
            diff.Serial = serial;
            diff.Add(id, oldValue, newValue);
            return diff;
        }
    }
}