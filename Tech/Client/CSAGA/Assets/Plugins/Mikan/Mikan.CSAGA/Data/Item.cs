﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mikan.CSAGA.Internal
{
    public class ItemInfoObj<T> : ListObj<T>
        where T : ItemObj, new ()
    {
        
    }

    public class ItemObj : ISerializable
    {
        public int ID;
        public int Count;

        public void Serialize(IOutput output)
        {
            output.Write(ID);
            output.Write(Count);
        }

        public void DeSerialize(IInput input)
        {
            ID = input.ReadInt32();
            Count = input.ReadInt32();
        }
    }
}
namespace Mikan.CSAGA.Data
{
    public class ItemInfo : Internal.ItemInfoObj<Item>
    {
        public int Count(int id)
        {
            var item = List.Find((o) => { return o.ID == id; });
            if(item != null) return item.Count;
            return 0;
        }
        public Item this[int id]
        {
            get
            {
                foreach (var item in List)
                {
                    if (item.ID == id) return item;
                }
                return null;
            }
        }

        internal void ApplyChange(SyncItem syncData)
        {
            foreach (var diff in syncData.Diff.Select())
            {
                var item = this[diff.Key];

                if (item != null) 
                    item.Count = diff.Value.NewValue;
                else
                    List.Add(new Item { ID = diff.Key, Count = diff.Value.NewValue });
            }
        }

        internal void DebugAdd(int id, int count)
        {
            var item = this[id];

            if (item != null)
                item.Count += count;
            else
                List.Add(new Item { ID = id, Count = count });
        }
    }

    public class Item : Internal.ItemObj
    {
    }
}
