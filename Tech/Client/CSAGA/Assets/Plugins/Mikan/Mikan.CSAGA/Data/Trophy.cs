﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mikan.CSAGA.Internal
{
    public class TrophyObj : ISerializable
    {
        public int Current;
        public int Progress;
        public DateTime UpdateTime;

        /// <inheritdoc />
        public void Serialize(IOutput output)
        {
            output.Write(Current);
            output.Write(Progress);
            output.Write(UpdateTime);
        }

        /// <inheritdoc />
        public void DeSerialize(IInput input)
        {
            Current = input.ReadInt32();
            Progress = input.ReadInt32();
            UpdateTime = input.ReadDateTime();
        }
    }
    public class TrophyInfoObj<T> : DictionaryObj<T>
        where T : TrophyObj, new()
    {
    }
}

namespace Mikan.CSAGA.Data
{
    public class TrophyInfo : Internal.TrophyInfoObj<Trophy>
    {
        public List<Trophy> Daily;

        public Trophy DailyCounter;

        public DateTime updateTime;

        internal bool IsOverdue
        {
            get 
            {
                if (updateTime.Date != Global.Service.CurrentTime.Date) return true;
                return TimeLimit.IsOverdue(GetType()); 
            }
            set { TimeLimit.SetIsOverdue(GetType(), value); }
        }

        public TrophyInfo()
        {
            TimeLimit.Add(GetType());
        }

        /// <inheritdoc />
        protected override void DeSerialize(IInput input)
        {
            base.DeSerialize(input);

            Daily = new List<Trophy>();

            var removeList = new List<int>();

            foreach (var item in Global.Tables.Trophy.Select())
            {
                var group = (int)item.n_IDSET;

                switch (item.n_LEGACY_TYPE)
                {
                    case TrophyType.Progressive:
                    {
                        if (List.ContainsKey(group)) continue;
                        var obj = new Trophy { Current = 0, Progress = 0, Group = group };
                        List.Add(group, obj);
                        break;
                    }
                    case TrophyType.Daily:
                    {
                        var obj = default(Trophy);
                        if (List.TryGetValue(group, out obj))
                        {
                            removeList.Add(group);
                            obj.Current = item.n_ID;
                            obj.Group = group;
                            if (obj.UpdateTime.Date != Global.Service.CurrentTime.Date) obj.Progress = 0;
                        }
                        else
                        {
                            obj = new Trophy { Current = item.n_ID, Progress = 0, Group = group };
                            obj.UpdateTime = DateTime.MinValue;
                        }

                        Daily.Add(obj);
                        break;
                    }
                    case TrophyType.DailyCounter:
                    {
                        if (List.TryGetValue(group, out DailyCounter))
                        {
                            removeList.Add(group);
                            DailyCounter.Group = group;
                        }
                        else
                            DailyCounter = new Trophy { Group = group };
                        break;
                    }
                    case TrophyType.Challenge:
                        break;
                    default:
                    {
                        var obj = default(Trophy);
                        if (List.TryGetValue(group, out obj)) removeList.Add(group);
                        break;
                    }
                }
            }

            foreach (var item in removeList) List.Remove(item);
            removeList.Clear();

            foreach (var item in List)
            {
                item.Value.Group = item.Key;

                if (item.Value.Current > 0)
                {
                    var data = Global.Tables.Trophy[item.Value.Current];
                    if (data.n_LEGACY_TYPE == TrophyType.Challenge) continue;
                }
                
                var next = Global.Tables.Trophy.SelectFirst(o => (int)o.n_IDSET == item.Key && o.n_ID > item.Value.Current);
                if (next == null)
                    removeList.Add(item.Key);
                else
                    item.Value.Current = next.n_ID;
            }

            foreach (var item in removeList) List.Remove(item);

            if (DailyCounter == null) DailyCounter = new Trophy{ Group = Global.Tables.DailyCounterTrophyID };

            updateTime = Global.Service.CurrentTime;

            IsOverdue = false;
        }
    }

    public class Trophy : Internal.TrophyObj
    {
        public int Group;
    }
}