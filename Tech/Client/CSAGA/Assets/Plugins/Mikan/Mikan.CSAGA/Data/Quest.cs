﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mikan.CSAGA.Internal
{
    public class QuestInfoObj<T> : ListObj<T>
        where T : QuestStateObj, new ()
    {
        public int RescueCount;
        public DateTime RescueUpdateTime;

        protected override void Serialize(IOutput output)
        {
            base.Serialize(output);
            output.Write(RescueCount);
            output.Write(RescueUpdateTime);
        }
        protected override void DeSerialize(IInput input)
        {
            base.DeSerialize(input);
            RescueCount = input.ReadInt32();
            RescueUpdateTime = input.ReadDateTime();
        }
    }

    public class QuestStateObj : ISerializable
    {
        public int QuestID;
        public ComplexI32 Complex;
        public DateTime LastPlayTime;

        public bool IsCleared 
        { 
            get { return Complex != null ? Complex[0] : false; }
            set
            {
                if (Complex == null) Complex = new ComplexI32();
                Complex[0] = value;
            }
        }

        public void Serialize(IOutput output)
        {
            output.Write(QuestID);
            output.Write(Complex);
            output.Write(LastPlayTime);
        }

        public void DeSerialize(IInput input)
        {
            QuestID = input.ReadInt32();
            Complex = input.Read<ComplexI32>();
            LastPlayTime = input.ReadDateTime();
        }
    }

    public class LimitedQuestObj : ISerializable
    {
        public int QuestID;
        public int PlayCount;
        public DateTime EndTime;

        public void Serialize(IOutput output)
        {
            output.Write(QuestID);
            output.Write(PlayCount);
            output.Write(EndTime);
        }

        public void DeSerialize(IInput input)
        {
            QuestID = input.ReadInt32();
            PlayCount = input.ReadInt32();
            EndTime = input.ReadDateTime();
        }
    }

    public class SharedQuestObj : ISerializable
    {
        public string PlayerID;
        public Int64 Serial;
        public int QuestID;
        public DateTime EndTime;
        public int PlayCount;

        public void Serialize(IOutput output)
        {
            output.Write(PlayerID);
            output.Write(Serial);
            output.Write(QuestID);
            output.Write(EndTime);
            output.Write(PlayCount);
        }

        public void DeSerialize(IInput input)
        {
            PlayerID = input.ReadString();
            Serial = input.ReadInt64();
            QuestID = input.ReadInt32();
            EndTime = input.ReadDateTime();
            PlayCount = input.ReadInt32();
        }
    }

    public class CrusadeObj<T> : SerializableObject
        where T : SharedQuestObj, new()
    {
        public int Discover;
        public int Point;
        public DateTime UpdateTime;
        public List<T> QuestList;
        public List<Int64> PlayList;

        protected override void Serialize(IOutput output)
        {
            output.Write(Discover);
            output.Write(Point);
            output.Write(UpdateTime);
            output.Write(PlayList);
            output.Write(QuestList);
        }

        protected override void DeSerialize(IInput input)
        {
            Discover = input.ReadInt32();
            Point = input.ReadInt32();
            UpdateTime = input.ReadDateTime();
            PlayList = input.ReadInt64List();
            QuestList = input.ReadList<T>();
        }
    }
}
namespace Mikan.CSAGA.Data
{
    public class QuestInfo : Internal.QuestInfoObj<QuestState>
    {
        public QuestInstance Current { get; internal set; }

        internal List<Serial> CachedQuestMemberList { get; set; }

        public Rescue Rescue
        {
            get
            {
                var rescue = default(Rescue);

                var currentTime = Global.Service.CurrentTime;
                var beginTime= default(DateTime);
                var endTime = default(DateTime);
                foreach (var item in rescueSchedules)
                {
                    if (item.n_START_DAY == 0) continue;

                    beginTime = Calc.Quest.ParseBeginTime(item.n_START_DAY, item.n_START_TIME);
                    endTime = beginTime.AddMinutes(item.n_DURATION);

                    if (currentTime < beginTime) continue;

                    if (rescue != null && rescue.EndTime > endTime) continue;
                    
                    rescue = new Rescue();
                    rescue.BeginTime = beginTime;
                    rescue.EndTime = endTime;
                    rescue.Max = Global.Tables.HelpMax;

                    if (RescueUpdateTime < beginTime)
                    {
                        RescueUpdateTime = beginTime;
                        RescueCount = Global.Tables.HelpInital * Global.Tables.HelpScale;
                    }

                    var add = (int)Math.Floor((currentTime.Subtract(RescueUpdateTime).TotalMinutes / Global.Tables.HelpAdd) * Global.Tables.HelpScale);

                    var originCount = Math.Min(RescueCount + add, rescue.Max * Global.Tables.HelpScale);
                    rescue.Count = originCount / Global.Tables.HelpScale;

                    if (rescue.Count < rescue.Max)
                    {
                        var remainMinutes = Global.Tables.HelpAdd * (1f - (float)(originCount % Global.Tables.HelpScale) / (float)Global.Tables.HelpScale);

                        rescue.NextUpdateTime = currentTime.AddMinutes(remainMinutes);
                    }

                }
                return rescue;
            }
        }

        private List<ConstData.EventSchedule> rescueSchedules;

        private Dictionary<int, LimitedQuest> limited = new Dictionary<int, LimitedQuest>();

        private Dictionary<int, LimitedQuest> schedule = new Dictionary<int, LimitedQuest>();

        private Dictionary<int, LimitedQuest> cachedSchedule = new Dictionary<int, LimitedQuest>();

        private DateTime timestamp;

        public QuestSettlement Settlement { get; private set; }

        public Crusade Crusade { get; internal set; }

        public LimitedQuest PVP
        {
            get
            {
                var list = ReCalcSchedule();

                var current = default(LimitedQuest);

                foreach (var item in list)
                {
                    if (item.Value.IsPVP)
                    {
                        if(current == null || (current.BeginTime < item.Value.BeginTime && item.Value.BeginTime < Global.Service.CurrentTime ))
                            current = item.Value;
                    }
                }

                return current;
            }
        }

        public QuestState this[int questId]
        {
            get { return Get(questId); }
        }

        public QuestState Get(int questId)
        {
            return Get(questId, false);
        }

        public LimitedQuest GetLimited(int questId)
        {
            var data = Global.Tables.QuestDesign[questId];

            var obj = default(LimitedQuest);

            switch (data.n_QUEST_TYPE)
            { 
                case QuestType.Limited:
                    if (!limited.TryGetValue(questId, out obj)) return null;
                    break;
                case QuestType.Schedule:
                {
                    var list = ReCalcSchedule();
                    if (!list.TryGetValue(questId, out obj)) return null;
                    break;
                }
                default:
                    return null;
            }

            return obj;
        }
        public IEnumerable<LimitedQuest> SelectLimited()
        {
            foreach (var item in limited)
            {
                if (item.Value.EndTime > Global.Service.CurrentTime) yield return item.Value;
            }

            var list = ReCalcSchedule();

            foreach (var item in list) yield return item.Value;
        }

        private Dictionary<int, LimitedQuest> ReCalcSchedule()
        {
            var currentTime = Global.Service.CurrentTime;

            if (timestamp.AddSeconds(30) > currentTime && timestamp.Date == currentTime.Date) return cachedSchedule;

            if (Crusade == null)
            {
                Crusade = new Crusade();
                Crusade.Candidates = new List<int>();
            }
            else
                Crusade.Candidates.Clear();

            timestamp = currentTime;

            var list = new Dictionary<int, LimitedQuest>();

            var beginTime = currentTime;
            var endTime = currentTime;

            foreach (var item in Global.Tables.EventSchedule.Select())
            {
                if (!Calc.Quest.ParseSchedulePeriod(item, currentTime, ref beginTime, ref endTime)) continue;

                if (currentTime < beginTime) continue;

                if (item.n_TYPE == EventScheduleType.Gacha) continue;

                if (item.n_TYPE != EventScheduleType.PVP)
                {
                    // 積分活動要保留3天,一般活動直接關掉
                    if (item.n_POINT > 0 || item.n_RANKING > 0)
                    {
                        if (currentTime > endTime.AddDays(3)) continue;
                    }
                    else if (currentTime > endTime)
                        continue;
                }

                foreach (var sub in Global.Tables.QuestDesign.Select(o => { return o.n_MAIN_QUEST == item.n_QUESTID; }))
                {
                    var obj = new LimitedQuest { MainQuestID = item.n_QUESTID, QuestID = sub.n_ID, BeginTime = beginTime, EndTime = endTime, EventID = item.n_ID };

                    if (item.n_TYPE != EventScheduleType.PVP && sub.n_COUNT == 0) obj.IsInfinity = true;

                    var record = default(LimitedQuest);

                    if (schedule.TryGetValue(sub.n_ID, out record))
                    {
                        if (!record.IsOverdue) obj.PlayCount = record.PlayCount;
                    }

                    if (item.n_TYPE == EventScheduleType.PVP)
                    {
                        var origin = DictionaryUtils.SafeGetValue(list, obj.QuestID);
                        if (origin != null)
                        {
                            if (currentTime > origin.EndTime || (currentTime > obj.BeginTime && obj.BeginTime > origin.BeginTime))
                                list[obj.QuestID] = obj;
                        }
                        else
                            list.Add(obj.QuestID, obj);
                    }
                    else
                        list.Add(obj.QuestID, obj);

                    if (item.n_TYPE == EventScheduleType.Crusade)
                    {
                        obj.IsCrusade = true;

                        Crusade.Candidates.Add(obj.QuestID);

                        if (Crusade.BeginTime < beginTime) Crusade.BeginTime = beginTime;
                    }

                    if (item.n_TYPE == EventScheduleType.PVP)
                        obj.IsPVP = true;
                }
                
            }

            cachedSchedule = list;

            return list;
        }

        internal void OnQuestEnd(QuestSettlement settlement)
        {
            Settlement = settlement;

            var state = Get(Current.QuestID, true);

            if (state == null)
            {
                state = new QuestState { QuestID = Current.QuestID };
                List.Add(state);
            }

            state.LastPlayTime = Global.Service.CurrentTime;

            if(settlement.IsSucc) state.IsCleared = true;

            if (settlement.SyncLimitedQuest != null)
            {
                foreach (var item in settlement.SyncLimitedQuest.List)
                {
                    if (item.QuestID == Current.QuestID) continue;
                    settlement.TriggerQuestID = item.QuestID;
                    break;
                }
            }
        }

        private QuestState Get(int questId, bool nullable)
        {
            var state = List.Find(o => { return o.QuestID == questId; });
            if (state == null && !nullable) return new QuestState { QuestID = questId };
            return state;
        }

        internal void ApplyChange(SyncLimitedQuest syncData)
        {
            ApplyChange(syncData.List);
        }

        internal void ApplyChange(SyncRescue syncData)
        {
            RescueCount = syncData.Count;
            RescueUpdateTime = syncData.UpdateTime;
        }

        internal void ApplyChange(List<Internal.LimitedQuestObj> syncList)
        {
            foreach (var item in syncList)
            {
                var data = Global.Tables.QuestDesign[item.QuestID];
				if(data == null) continue;

                var obj = new LimitedQuest { QuestID = item.QuestID, EndTime = item.EndTime, PlayCount = item.PlayCount };
                obj.MainQuestID = data.n_MAIN_QUEST;

                if (item.EndTime < Global.Service.CurrentTime) continue;

                if (data.n_QUEST_TYPE == QuestType.Schedule)
                {
                    schedule[item.QuestID] = obj;
                    timestamp = DateTime.MinValue;
                }
                else
                    limited[item.QuestID] = obj;
                
            }
        }

        internal void ApplyChange(SyncQuest syncData)
        {
            var state = Get(syncData.QuestID, true);
            if (state == null)
            {
                state = new QuestState { QuestID = syncData.QuestID };
                List.Add(state);
            }

            if (state.Complex == null) state.Complex = new ComplexI32();

            state.LastPlayTime = Global.Service.CurrentTime;
            state.Complex.Value |= syncData.Complex;
        }

        protected override void DeSerialize(IInput input)
        {
            base.DeSerialize(input);
            rescueSchedules = new List<ConstData.EventSchedule>(Global.Tables.EventSchedule.Select(o => { return o.n_TYPE == EventScheduleType.Rescue && o.n_START_DAY > 0; }));

            ReCalcSchedule();
        }
    }

    public class QuestState : Internal.QuestStateObj
    {
        public bool this[int index]
        {
            get
            {
                if (Complex == null) return false;
                return Complex[index];
            }
        }

        public bool IsChallegeFinish(int index)
        {
            return this[index + 1];
        }
    }

    public class LimitedQuest
    {
        public int MainQuestID;
        public int QuestID;
        public DateTime BeginTime;
        public DateTime EndTime;
        public int PlayCount;

        public int EventID;
        public bool IsInfinity;
        public bool IsCrusade;
        public bool IsPVP;

        public bool IsOverdue { get { return EndTime < Global.Service.CurrentTime; } }
    }

    public class Rescue
    {
        public int Count;
        public int Max;
        public DateTime NextUpdateTime;
        public DateTime BeginTime;
        public DateTime EndTime;
    }

    public class SharedQuest : Internal.SharedQuestObj
    {
        internal static SharedQuest Convert(Internal.SharedQuestObj origin)
        {
            var obj = new SharedQuest();
            obj.PlayerID = origin.PlayerID;
            obj.Serial = origin.Serial;
            obj.QuestID = origin.QuestID;
            obj.EndTime = origin.EndTime;
            obj.PlayCount = origin.PlayCount;

            obj.Timestamp = Global.Service.CurrentTime;
            return obj;
        }

        internal DateTime Timestamp;

        public bool IsOverdue
        {
            get
            {
                return Global.Service.CurrentTime > EndTime;
            }
        }

        public bool IsSelf
        {
            get
            {
                return PlayerID == Global.Service.Profile.PublicID;
            }
        }
    }

    internal class SharedQuestProxy
    {
        public SharedQuest Origin;
    }

    public class Crusade : Internal.CrusadeObj<SharedQuest>
    {
        private DateTime timestamp = DateTime.MinValue;

        internal bool IsOverdue { get { return timestamp.AddMinutes(3) < Global.Service.CurrentTime; } }
        internal DateTime BeginTime = DateTime.MinValue;
        internal List<SharedQuestProxy> MyList = new List<SharedQuestProxy>();
        internal List<SharedQuestProxy> RandomList = new List<SharedQuestProxy>();
        public List<int> Candidates { get; internal set; }

        public IEnumerable<SharedQuest> SelectQuest()
        {
            foreach (var item in MyList)
            {
                if (!item.Origin.IsOverdue) yield return item.Origin;
            }

            foreach (var item in RandomList)
            {
                if (!item.Origin.IsOverdue) yield return item.Origin;
            }
        }

        public int CalcPoint(DateTime currentTime)
        {
            var point = Calc.Quest.CalcCrusadeCount(Point, UpdateTime, BeginTime, currentTime);

            return point / Global.Tables.CrusadeScale;
        }

        public int PointRemainSeconds(DateTime currentTime)
        {
            var point = Calc.Quest.CalcCrusadeCount(Point, UpdateTime, BeginTime, currentTime);

            if (point >= (Global.Tables.CrusadeMax * Global.Tables.CrusadeScale)) return 0;
            var remain = 1 - (point % Global.Tables.CrusadeScale) / (float)Global.Tables.CrusadeScale;
            return (int)Math.Floor(Global.Tables.CrusadeAdd * 60 * remain); 
        }

        private Dictionary<Int64, SharedQuestProxy> map = new Dictionary<long, SharedQuestProxy>();

        internal void ApplyChange(SyncCrusade data)
        {
            Point = data.Point;
            UpdateTime = data.UpdateTime;
        }

        internal void ApplyChange(SyncSharedQuest data)
        {
            foreach (var item in data.List) Add(SharedQuest.Convert(item));

            var overdueList = new List<SharedQuestProxy>();

            foreach (var item in RandomList)
            {
                if (item.Origin.Timestamp.AddMinutes(10) < Global.Service.CurrentTime) overdueList.Add(item);
            }

            foreach (var item in overdueList)
            {
                if (RandomList.Count <= 10) break;
                RandomList.Remove(item);
            }

            Sort();
        }

        protected override void DeSerialize(IInput input)
        {
            base.DeSerialize(input);

            foreach (var item in QuestList) Add(item);

            Sort();

            timestamp = Global.Service.CurrentTime;
        }

        private void Add(SharedQuest obj)
        {
            var proxy = DictionaryUtils.SafeGetValueNullable(map, obj.Serial);

            if (proxy != null)
                proxy.Origin = obj;
            else
            {
                proxy = new SharedQuestProxy { Origin = obj };
                map.Add(obj.Serial, proxy);

                if (obj.PlayerID == Global.Service.Profile.PublicID)
                    MyList.Add(proxy);
                else
                    RandomList.Add(proxy);
            }
        }

        private void Sort()
        {
            MyList.Sort((lhs, rhs) => lhs.Origin.EndTime.CompareTo(rhs.Origin.EndTime));
            RandomList.Sort((lhs, rhs) => lhs.Origin.EndTime.CompareTo(rhs.Origin.EndTime));
        }
    }
}
