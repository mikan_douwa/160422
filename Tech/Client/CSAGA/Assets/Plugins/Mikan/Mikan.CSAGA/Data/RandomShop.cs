﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mikan.CSAGA.Internal
{
    /// <summary>
    /// 隨機商店資訊
    /// </summary>
    public class RandomShopInfoObj : SerializableObject
    {
        /// <summary>
        /// 商品列表
        /// </summary>
        public List<int> List;

        /// <summary>
        /// 商品購買狀態
        /// </summary>
        public ComplexI32 State;

        /// <summary>
        /// 列表更新時間
        /// </summary>
        public DateTime UpdateTime = DateTime.MinValue;

        /// <inheritdoc />
        protected override void Serialize(IOutput output)
        {
            output.Write(List);
            output.Write(State);
            output.Write(UpdateTime);
        }

        /// <inheritdoc />
        protected override void DeSerialize(IInput input)
        {
            List = input.ReadInt32List();
            State = input.Read<ComplexI32>();
            UpdateTime = input.ReadDateTime();
        }
    }
}

namespace Mikan.CSAGA.Data
{
    /// <summary>
    /// 隨機商店資訊
    /// </summary>
    public class RandomShopInfo : Internal.RandomShopInfoObj
    {
        public bool IsOverdue
        {
            get
            {
                return UpdateTime.AddMinutes(Global.Tables.RandomShopRefreshTime) < Global.Service.CurrentTime;
            }
        }
    }
}

