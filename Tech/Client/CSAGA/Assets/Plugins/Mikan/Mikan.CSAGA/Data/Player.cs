﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mikan.CSAGA.Internal
{
    public class PlayerInfoObj : SerializableObject
    {
        public bool IsAvailable { get { return complex[ComplexIndex.PlayerAvailable]; } }

        public bool IsFirstGachaDone { get { return complex[ComplexIndex.PlayerFirstGacha]; } }

        public int GachaBoost { get { return complex.Get(ComplexIndex.GachaBoost, ComplexIndex.GachaBoostLen); } }

        protected Complex complex;
        protected string name;
        protected int tpConsumed;
        protected DateTime tpChangeTime;
        protected PropertyCollection list = new PropertyCollection();

        protected override void Serialize(IOutput output)
        {
            output.Write(complex);
            output.Write(list);
            output.Write(name);
            output.Write(tpConsumed);
            output.Write(tpChangeTime);
        }

        protected override void DeSerialize(IInput input)
        {
            complex = input.Read<Complex>();
            list = input.Read<PropertyCollection>();
            name = input.ReadString();
            tpConsumed = input.ReadInt32();
            tpChangeTime = input.ReadDateTime();
        }

        virtual public int this[PropertyID id]
        {
            get
            {
                return list[id];
            }

            set
            {
                list[id] = value;
            }
        }

        public int this[DropType coinType]
        {
            get
            {
                switch (coinType)
                {
                    case DropType.Coin: return this[PropertyID.Coin];
                    case DropType.Crystal: return this[PropertyID.Crystal];
                    case DropType.Gem: return this[PropertyID.Gem];
                    case DropType.FP: return this[PropertyID.FP];
                }
                return 0;
            }
        }

        internal int TalkPointReserved(DateTime currentTime)
        {
            return (int)currentTime.Subtract(tpChangeTime).TotalSeconds;
        }

        public int TalkPointConsumed(DateTime currentTime)
        {
            var reserved = TalkPointReserved(currentTime);
            var diff = tpConsumed - reserved;
            if (diff < 0) return 0;
            return diff;
        }
    }
}
namespace Mikan.CSAGA.Data
{
    public class PlayerInfo : Internal.PlayerInfoObj
    {
        public event PropertyChangeDelegate<PlayerInfo, int> OnPropertyChange;

        public event ValueChangeDelegate<PlayerInfo, string> OnNameChange;

        public string Name
        {
            get { return name; }
            internal set
            {
                if (name == value) return;
                var origin = name;
                name = value;
                if(OnNameChange != null) OnNameChange(this, origin, name);
            }
        }

        public Complex Complex { get { return complex; } }

        public override int this[PropertyID id]
        {
            get { return base[id]; }

            set
            {
                var origin = this[id];
                list[id] = value;
                if (OnPropertyChange != null) OnPropertyChange(this, id, origin, value);
            }
        }

        public int LV
        {
            get
            {
                var exp = this[PropertyID.Exp];
                return PlayerUtils.CalcLV(exp);
            }
        }

        public int FP
        {
            get
            {
                return this[PropertyID.FP];
            }
        }


        public int TalkPoint
        {
            get
            {
                var origin = ConstData.Tables.Instance.TalkPointLimit - TalkPointConsumed(Global.Service.CurrentTime);
                return origin / ConstData.Tables.Instance.TalkPointScale;
            }
        }

        public TimeSpan TalkPointCountdown
        {
            get
            {
                var origin = ConstData.Tables.Instance.TalkPointLimit + TalkPointReserved(Global.Service.CurrentTime) - tpConsumed;
                if (origin >= ConstData.Tables.Instance.TalkPointLimit) return new TimeSpan(0);
                var _decimal = origin % ConstData.Tables.Instance.TalkPointScale;
                var remains = ConstData.Tables.Instance.TalkPointScale - _decimal;
                return new TimeSpan(remains / 3600, (remains / 60) % 60, remains % 60);
            }
        }

        public ConstData.PlayerData Data
        {
            get
            {
                return Global.Tables.Player[LV];
            }
        }

        internal void ApplyChange(PropertyDiff diff)
        {
            if (diff == null) return;
            foreach (var item in diff.Select())
            {
                switch (item.Key)
                {                    
                    case PropertyID.Complex:
                    {
                        var index = item.Value.Value1;
                        switch (index)
                        {
                            case ComplexIndex.GachaBoost:
                                KernelService.Logger.Debug("boost = {0}", item.Value.Value2);
                                complex.Set(ComplexIndex.GachaBoost, ComplexIndex.GachaBoostLen, item.Value.Value2);
                                break;
                            default:
                                complex[index] = item.Value.Value2 != 0;
                                break;
                        }
                        break;
                    }
                    case PropertyID.TPConsumed:
                    {
                        tpConsumed = item.Value.NewValue;
                        tpChangeTime = Global.Service.CurrentTime;
                        break;
                    }
                    case PropertyID.BPConsumed:
                    {
                        if (Global.Service.PVPInfo != null)
                        {
                            Global.Service.PVPInfo.BPConsumed = item.Value.NewValue;
                        }
                        break;
                    }
                    case PropertyID.BPUpdateTime:
                    {
                        if (Global.Service.PVPInfo != null)
                        {
                            Global.Service.PVPInfo.BPUpdateTime = DateTimeUtils.FromUnixTime(item.Value.NewValue);
                        }
                        break;
                    }
                    default:
                    {
                        this[item.Key] = item.Value.NewValue;

                        switch (item.Key)
                        {
                            case PropertyID.IAPGem:
                                Global.Service.PurchaseInfo.IAPGem = item.Value.NewValue;
                                break;
                            case PropertyID.Gem:
                                Global.Service.PurchaseInfo.Gem = item.Value.NewValue;
                                break;
                            case PropertyID.Discover:
                                if (Global.Service.QuestInfo != null && Global.Service.QuestInfo.Crusade != null)
                                    Global.Service.QuestInfo.Crusade.Discover = item.Value.NewValue;
                                break;

                        }
                        break;
                    }
                }

                if (OnPropertyChange != null) OnPropertyChange(this, item.Key, item.Value.OriginValue, item.Value.NewValue);
            }

        }

        protected override void DeSerialize(IInput input)
        {
            base.DeSerialize(input);


        }
    }
}

namespace Mikan.CSAGA
{
    public static class PlayerUtils
    {
        private static ConstData.Tables Tables { get { return ConstData.Tables.Instance; } }

        /// <summary>
        /// 使用binary search來找出玩家等級
        /// </summary>
        /// <param name="exp"></param>
        /// <returns></returns>
        public static int CalcLV(int exp)
        {
            return CalcLV(exp, 1, Tables.PlayerLVMax);
        }

        /// <summary>
        /// 使用binary search來找出玩家等級
        /// </summary>
        /// <param name="exp"></param>
        /// <param name="minLV"></param>
        /// <param name="maxLV"></param>
        /// <returns></returns>
        public static int CalcLV(int exp, int minLV, int maxLV)
        {
            if (exp == 0) return 1;

            var min = Tables.Player[minLV].n_TOTALEXP;

            var max = Tables.Player[maxLV].n_TOTALEXP;

            if (exp >= max) return maxLV;

            if (maxLV - minLV < 2) return minLV;

            var midLV = (int)((minLV + maxLV) / 2);

            var mid = Tables.Player[midLV].n_TOTALEXP;

            if (exp < mid) return CalcLV(exp, minLV, midLV - 1);

            return CalcLV(exp, midLV, maxLV - 1);
        }
    }
}
