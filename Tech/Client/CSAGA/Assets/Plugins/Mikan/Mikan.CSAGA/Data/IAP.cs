﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mikan.CSAGA.Internal
{
    public class IAPProductObj : ISerializable
    {
        public string ProductID;

        public string ProductName;

        public string Price;

        virtual public void Serialize(IOutput output)
        {
            output.Write(ProductID);
            output.Write(ProductName);
            output.Write(Price);
        }

        virtual public void DeSerialize(IInput input)
        {
            ProductID = input.ReadString();
            ProductName = input.ReadString();
            Price = input.ReadString();
        }
    }

    public class IAPInfoObj<T> : ListObj<T>
     where T : IAPProductObj, new()
    {
    }
}

namespace Mikan.CSAGA.Data
{
    public class IAPInfo : Internal.IAPInfoObj<IAPProduct>
    {
    }

    public class IAPProduct : Internal.IAPProductObj
    {
        public int Bonus;

        public override void Serialize(IOutput output)
        {
            base.Serialize(output);
            output.Write(Bonus);
        }

        public override void DeSerialize(IInput input)
        {
            base.DeSerialize(input);
            Bonus = input.ReadInt32();
        }
    }
}
