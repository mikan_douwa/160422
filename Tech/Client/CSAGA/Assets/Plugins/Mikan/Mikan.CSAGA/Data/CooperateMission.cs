using System;
using System.Collections.Generic;
using System.Text;

namespace Mikan.CSAGA.Internal
{

    public class CooperationMissionObj<T, U> : ISerializable
        where T : MissionSubjectObj, new()
        where U : MissionStateObj, new()
    {
        public Int64 Serial;
        public List<T> Subjects;
        public U State;
        public DateTime BeginTime;
        public int Progress;
        public int Point;

        public void Serialize(IOutput output)
        {
            output.Write(Serial);
            output.Write(Subjects);
            output.Write(State);
            output.Write(BeginTime);
            output.Write(Progress);
            output.Write(Point);
        }

        virtual public void DeSerialize(IInput input)
        {
            Serial = input.ReadInt64();
            Subjects = input.ReadList<T>();
            State = input.Read<U>();
            BeginTime = input.ReadDateTime();
            Progress = input.ReadInt32();
            Point = input.ReadInt32();
        }
    }

    public class MissionStateObj : ComplexI32
    {
        public bool IsClaimed 
        { 
            get { return this[0]; }
            set { this[0] = value; }
        }
    }

    public class MissionSubjectObj : ISerializable
    {
        public int ID;
        public int Progress;

        public void Serialize(IOutput output)
        {
            output.Write(ID);
            output.Write(Progress);
        }

        public void DeSerialize(IInput input)
        {
            ID = input.ReadInt32();
            Progress = input.ReadInt32();
        }
    }

    public class CooperationMissionInfoObj<T, U, V> : ListObj<T>
		where T : CooperationMissionObj<U,V>, new() 
		where U : MissionSubjectObj, new()
        where V : MissionStateObj, new()
    {
    }
}

namespace Mikan.CSAGA.Data
{
    public class CooperateMissionInfo : Internal.CooperationMissionInfoObj<CooperateMission, MissionSubejct, MissionState>
    {
        internal bool IsOverdue
        {
            get { return TimeLimit.IsOverdue(GetType()); }
            set { TimeLimit.SetIsOverdue(GetType(), value); }
        }

        public int TotalPoint
        {
            get
            {
                var point = 0;

                foreach (var item in List)
                {
                    if (item.BeginTime < Global.Service.CurrentTime.Date) continue;
                    point += item.Point;
                }

                return point;
            }
        }

        public CooperateMissionInfo()
        {
            TimeLimit.Add(GetType(), 60);
        }

        public CooperateMission this[Int64 serial]
        {
            get
            {
                return List.Find(o=>{ return o.Serial == serial; });
            }
        }

        protected override void DeSerialize(IInput input)
        {
            base.DeSerialize(input);

            foreach (var item in List)
            {
                item.Point = 0;

                for (int i = 0; i < item.Subjects.Count; ++i)
                {
                    var subject = item.Subjects[i];
                    subject.MissionID = item.Serial;
                    subject.Index = i;

                    item.Point += (Global.Tables.CoMission[subject.ID].n_EFF * subject.Progress);
                }
            }

            IsOverdue = false;
        }

        internal IEnumerable<MissionSubejct> SelectSubject(DateTime currentTime)
        {
            foreach (var item in List)
            {
                if (item.BeginTime.Date != currentTime.Date) continue;

                foreach (var subject in item.Subjects) yield return subject;
            }
        }

        internal void ApplyChange(SyncMission syncData)
        {
            var mission = this[syncData.Serial];
            if (mission == null) return;
            var subject = mission.Subjects[syncData.Index];

            if (subject.ID != syncData.SubjectID)
            {
                var isExists = false;
                foreach (var item in mission.Subjects)
                {
                    if (item.ID == syncData.SubjectID)
                    {
                        subject = item;
                        break;
                    }
                }

                if (!isExists) return;
            }

            subject.Progress += syncData.Count;
            var data = Global.Tables.CoMission[subject.ID];
            if (data.n_MAX > 0 && subject.Progress > data.n_MAX)
                subject.Progress = data.n_MAX;

            mission.Point = 0;
            foreach (var item in mission.Subjects)
            {
                mission.Point += (Global.Tables.CoMission[item.ID].n_EFF * item.Progress);
            }
        }
    }

    public class CooperateMission : Internal.CooperationMissionObj<MissionSubejct, MissionState>
    {

        public override void DeSerialize(IInput input)
        {
            base.DeSerialize(input);
            if (State == null) State = new MissionState();
        }
    }

    public class MissionSubejct : Internal.MissionSubjectObj
    {
        public Int64 MissionID;
        public int Index;
    }

    public class MissionState : Internal.MissionStateObj
    {

    }
}
