﻿using System;
using System.Collections.Generic;
using System.Text;
using Mikan.CSAGA.Data;

namespace Mikan.CSAGA
{
    public partial class Service : GameService
    {
        internal static Service Instance { get; private set; }

        public Profile Profile { get; private set; }

        public PlayerInfo PlayerInfo { get; private set; }

        public CardInfo CardInfo { get; private set; }

        public ItemInfo ItemInfo { get; private set; }

        public QuestInfo QuestInfo { get; private set; }

        public TeamInfo TeamInfo { get { return TeamInfo.Instance; } }

        public GalleryInfo GalleryInfo { get { return GalleryInfo.Instance; } }

        public EquipmentInfo EquipmentInfo { get; private set; }

        public PurchaseInfo PurchaseInfo { get; private set; }

        public GachaInfo GachaInfo { get; private set; }

        public GiftInfo GiftInfo { get; private set; }

        public TrophyInfo TrophyInfo { get; private set; }

        public DailyInfo DailyInfo { get; private set; }

        public RandomShopInfo RandomShopInfo { get; private set; }

        public RankingInfo RankingInfo { get; private set; }

        public SocialInfo SocialInfo { get; private set; }

        public IAPInfo IAPInfo { get; private set; }

        public CooperateMissionInfo CooperateMissionInfo { get; private set; }

        public InfinityInfo InfinityInfo { get { return InfinityInfo.Instance; } }

        public FaithInfo FaithInfo { get; private set; }

        public TacticsInfo TacticsInfo { get; private set; }

        public PVPInfo PVPInfo { get; private set; }

        public int DailyEventPoint { get { return dailyEP != null && dailyEP.UpdateTime.Date == CurrentTime.Date ? dailyEP.Current : 0; } }

        internal BackupInfo BackupInfo { get { return BackupInfo.Instance; } }

        internal FellowCache FellowCache { get { return FellowCache.Instance; } }

        internal CommandResolver Resolver { get; private set; }

        #region Events

        public event ErrorDelegate OnError;

        public event RewardDelegate OnReward;

        public event LoginDelegate OnLogin;

        public event Callback OnPlayerCreate;

        public event CardOperateDelegate OnCardOperate;

        public event CardEventDelegate OnCardTalkBegin;
        public event CardEventDelegate OnCardTalkEnd;
        public event CardEventDelegate OnCardEvent;
        public event Callback OnCardGift;

        public event QuestBeginDelegate OnQuestBegin;
        public event QuestEndDelegate OnQuestEnd;
        public event QuestOpenDelegate OnQuestOpen;
        public event Callback OnCrusade;

        public event Callback OnEquipmentReplace;
        public event Callback OnEquipmentSell;
        public event EquipmentChangeDelegate OnEquipmentChange;

        public event GachaDrawDelegate OnGachaDraw;

        public event PurchaseDelegate OnPurchase;

        public event Callback OnGiftList;

        public event Callback OnTrophyList;

        public event GiftClaimDelegate OnGiftClaim;

        public event DailyInfoDelegate OnDailyInfo;

        public event Callback OnRandomShopInfo;

        public event RandomShopPurchaseDelegate OnRandomShopPurchase;

        public event ItemUseDelegate OnItemUse;

        public event Callback OnRankingInfo;

        public event RankingQueryDelegate OnRankingQuery;

        public event Callback OnRankingPlayer;

        public event Callback OnSocialInfo;
        public event Callback OnSocialQuery;
        public event SocialUpdateDelegate OnSocialUpdate;
        public event SocialRandomQueryDelegate OnSocialRandomQuery;
        public event SocialPurchaseHistoryDelegate OnSocialPurchaseHistory;

        public event ReportDelegate OnReport;

        public event Callback OnIAPInfo;

        public event IAPClaimDelegate OnIAPClaim;

        public event AccountPublishDelegate OnAccountPublish;

        public event AccountTransferBeginDelegate OnAccountTransferBegin;
        public event Callback OnAccountTransferEnd;

        public event Callback OnCooperateMissionInfo;

        public event Callback OnCooperateMissionDonate;

        public event CooperateMissionClaimDelegate OnCooperateMissionClaim;

        public event Callback OnFaithInfo;
        public event FaithClaimDelegate OnFaithClaim;
        public event Callback OnFaithUpdate;
        public event FaithEndProduceDelegate OnFaithEndProduce;

        public event TacticsEnableDelegate OnTacticsEnable;

        public event Callback OnPVPInfo;
        public event Callback OnPVPUpdate;
        public event Callback OnPVPRerollSkill;
        public event Callback OnPVPQuery;

#if MYCARD
        public event MyCardBeginTransactionDelegate OnMyCardBeginTransaction;
#endif

        private void RemoveAllEventListener()
        {
            OnError = null;
            OnReward = null;
            OnLogin = null;
            OnPlayerCreate = null;

            OnCardOperate = null;
            OnCardTalkBegin = null;
            OnCardTalkEnd = null;
            OnCardGift = null;

            OnQuestBegin = null;
            OnQuestEnd = null;
            OnQuestOpen = null;
            OnCrusade = null;

            OnEquipmentReplace = null;
            OnEquipmentSell = null;
            OnEquipmentChange = null;

            OnGachaDraw = null;
            OnPurchase = null;
            OnGiftList = null;
            OnGiftClaim = null;
            OnTrophyList = null;
            OnDailyInfo = null;
            OnRandomShopInfo = null;
            OnRandomShopPurchase = null;
            OnItemUse = null;

            OnRankingInfo = null;
            OnRankingQuery = null;
            OnRankingPlayer = null;

            OnSocialInfo = null;
            OnSocialQuery = null;
            OnSocialUpdate = null;
            OnSocialRandomQuery = null;
            OnSocialPurchaseHistory = null;

            OnReport = null;

            OnIAPInfo = null;
            OnIAPClaim = null;

            OnAccountPublish = null;
            OnAccountTransferBegin = null;
            OnAccountTransferEnd = null;

            OnCooperateMissionInfo = null;
            OnCooperateMissionDonate = null;
            OnCooperateMissionClaim = null;

            OnFaithInfo = null;
            OnFaithClaim = null;
            OnFaithUpdate = null;
            OnFaithEndProduce = null;

            OnTacticsEnable = null;

            OnPVPInfo = null;
            OnPVPUpdate = null;
            OnPVPRerollSkill = null;
            OnPVPQuery = null;

#if MYCARD
            OnMyCardBeginTransaction = null;
#endif
        }
        
        #endregion

        #region Variable

        public TimeSpan TimeDiff { get; private set; }

        public DateTime CurrentTime { get { return DateTime.Now.Subtract(TimeDiff); } }

        public bool IsLogin { get; private set; }

        private DateTime loginTime;

        private bool isReLogin = false;

        private Queue<ICommand> cachedList = new Queue<ICommand>();

        private string platform;
        private string signature;
        private string appVersion;

        private Serial favorite = Serial.Empty;

        private bool isAutoBattleEnabled = false;

        private bool isStayAtCurrentLevel = false;

        private bool isInQuest = false;

        private bool isPatchNotMatch = false;

        private bool isMaintaining = false;

        private TransferCache transferCache;

        private bool transferRestoring = false;

        private int lastTransferId = 0;

        private string cachedSupport;

        private int supportCard;

        private Int64 cachedCrusade;

        private SyncDailyFP dailyFP;

        private SyncDailyEventPoint dailyEP;

        private SyncPlayer syncPlayer;

        #endregion

    }
}
