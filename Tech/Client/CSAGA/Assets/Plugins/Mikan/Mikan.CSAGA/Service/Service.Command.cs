﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Mikan.CSAGA
{
    public partial class Service : GameService
    {
        #region Debug
        public void Debug(string opCode)
        {
            
            var req = new Commands.DebugRequest();
            req.Add(FieldID.DebugCode, opCode);
            SendCommand(req);
        }

        public void Report(ReportType type, string message)
        {
            SendCommand(new Commands.ReportRequest { Type = type, Message = message });
        }



        #endregion

        #region Login
        public static void CopyStream(Stream input, Stream output)
        {
            byte[] buffer = new byte[16 * 1024];
            int read;
            while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
            {
                output.Write(buffer, 0, read);
            }
        }
        public void Login(string platform, string appVersion, string signature, Serial favorite)
        {
#if MYCARD
            if (!platform.EndsWith(".MyCard")) platform += ".MyCard";
#endif
            this.platform = platform;
            this.appVersion = appVersion;
            this.signature = signature;
            this.favorite = favorite;

            transferCache = Load<Data.TransferCache>(LocalKey.TransferCache);

            if (transferCache != null && !transferCache.IsEmpty)
            {
                transferRestoring = true;
                SendCommand(new Commands.Account.TransferEndRequest { ID = transferCache.PublicID, Token = transferCache.Token, Key = transferCache.Key });
                return;
            }
            else
                transferCache = null;

            if (Profile.IsEmpty)
                SendCommand(new Commands.Account.CreateRequest { UUID = ServiceProvider.UUID });
            else
#if DEBUG
                SendCommand(new Commands.Account.LoginRequest { UUID = Profile.UUID, Platform = platform, UseSuperPrivilege = true });
#else
                SendCommand(new Commands.Account.LoginRequest { UUID = Profile.UUID, Platform = platform });
#endif
        }
        #endregion

        #region Transfer

        public void AccountPublish()
        {
            SendCommand(new Commands.Account.PublishRequest());
        }

        public void AccountTransferBegin(string id, string token)
        {
            transferCache = new Data.TransferCache { PublicID = id, Token = token };

            SendCommand(new Commands.Account.TransferBeginRequest { ID = id, Token = token });
        }

        public void AccountTransferEnd(bool cancelled)
        {
            if (cancelled)
            {
                transferCache = null;
                ServiceProvider.ClearLocalData(Format(LocalKey.TransferCache));
            }
            else
            {
                Save(LocalKey.TransferCache, transferCache);
                SendCommand(new Commands.Account.TransferEndRequest { ID = transferCache.PublicID, Token = transferCache.Token, Key = transferCache.Key });
            }
        }

        #endregion

        #region Player

        public void PlayerCreate(string name, int category, string code)
        {
            SendCommand(new Commands.Player.CreateRequest { Name = name, Category = category, Code = code });
        }

        public void PlayerNameEdit(string name)
        {
            SendCommand(new Commands.Player.NameRequest { Name = name });
        }

        private void QueryEssential()
        {
            if (BackupInfo.IsEmpty) SendCommand(new Commands.RestoreRequest());

            SendCommand(new Commands.Card.InfoRequest());
            SendCommand(new Commands.Item.InfoRequest());
            SendCommand(new Commands.Equipment.InfoRequest());
            SendCommand(new Commands.Quest.InfoRequest());
            SendCommand(new Commands.CooperateMission.InfoRequest());
            SendCommand(new Commands.Purchase.InfoRequest());
            SendCommand(new Commands.Tactics.InfoRequest());
            SendCommand(new Commands.PVP.InfoRequest { Initial = true });

            SendCommand(new Commands.IAP.InitRequest { IAPName = ServiceProvider.IAPName });
        }

        #endregion

        #region Card

        public void CardEnhance(Serial serial, int exp)
        {
            SendCommand(new Commands.Card.EnhanceRequest { Serial = serial.Value, Exp = exp });
        }

        public void CardSell(List<Serial> list)
        {
            SendCommand(new Commands.Card.SellRequest { List = Serial.Convert(list) });
        }

        public void CardGift(Serial serial, int itemId, int count)
        {
            EnqueueMissionTrigger(Calc.CoMission.TriggerGift(CooperateMissionInfo.SelectSubject(CurrentTime), CardInfo[serial].ID, itemId, count));

            SendCommand(new Commands.Card.GiftRequest { Serial = serial.Value, ItemID = itemId, Count = count });
        }

        public void CardEvent(Serial serial, int interactiveId)
        {
            var ev = EventUtils.CallEvent(interactiveId);
            if(ev.n_RESULT == EventResult.Quest)
                SendCommand(new Commands.Quest.BeginEventRequest { Serial = serial.Value, InteractiveID = interactiveId });
            else
                SendCommand(new Commands.Card.EventRequest { Serial = serial.Value, InteractiveID = interactiveId });
        }

        public void CardBeginTalk(Serial serial)
        {
            EnqueueMissionTrigger(Calc.CoMission.TriggerTalk(CooperateMissionInfo.SelectSubject(CurrentTime), CardInfo[serial].ID));

            SendCommand(new Commands.Card.BeginTalkRequest { Serial = serial.Value });
        }
        public void CardEndTalk(Serial serial, int choice)
        {
            SendCommand(new Commands.Card.EndTalkRequest { Serial = serial.Value, Choice = choice });
        }

        public void CardAwaken(Serial serial, int group)
        {
            SendCommand(new Commands.Card.AwakenRequest { Serial = serial.Value, Group = group });
        }

        public void CardLuck(Serial serial)
        {
            SendCommand(new Commands.Card.LuckRequest { Serial = serial.Value });
        }

        public void CardLock(List<Serial> locked, List<Serial> unlocked)
        {
            SendCommand(new Commands.Card.LockRequest { Locked = Serial.Convert(locked), Unlocked = Serial.Convert(unlocked) });
        }

        public void SetFavorite(Serial serial)
        {
            this.favorite = serial;

            EnqueueSyncData(typeof(SyncFavorite), new SyncFavorite { Serial = serial.Value });
        }

        #endregion

        #region Equipment

        public void EquipmentReplace(Serial cardSerial, int slot, Int64 equipSerial)
        {
            SendCommand(new Commands.Equipment.ReplaceRequest { CardSerial = cardSerial.Value, Slot = slot, Serial = equipSerial });
        }

        public void EquipmentSell(List<Int64> list)
        {
            SendCommand(new Commands.Equipment.SellRequest { List = list });
        }

        public void EquipmentRemove(Int64 serial)
        {
            SendCommand(new Commands.Equipment.RemoveRequest { Serial = serial });
        }
        public void EquipmentLock(Int64 serial, bool isLocked)
        {
            SendCommand(new Commands.Equipment.LockRequest { Serial = serial, IsLocked = isLocked });
        }
        #endregion

        #region Team
        public void TeamUpdate(int index, List<Serial> members)
        {
            TeamInfo.Update(index, members);
        }
        public void TeamUpdate(int index, int memberIndex, Serial serial)
        {
            TeamInfo.Update(index, memberIndex, serial);
        }
        public void TeamSelect(int index)
        {
            if (TeamInfo.Selection == index) return;
            TeamInfo.Selection = index;
        }

        public void TeamCache(int index, int memberIndex, Data.Fellow fellow)
        {
            TeamInfo.Cache(index, memberIndex, fellow);
        }

        public void TeamReset()
        {
            TeamInfo.Reset();
        }
        #endregion

        #region Quest
        public void BeginQuest(int questId, List<Serial> members, bool disableRescue = false, Int64 crusade = 0, string ticket = null)
        {
            isAutoBattleEnabled = false;
            isStayAtCurrentLevel = false;
            cachedSupport = null;
            supportCard = 0;
            cachedCrusade = crusade;
            challenge = null;

#if OFFLINE
            var quest = Calc.Quest.Generate(questId);
            QuestInfo.Current = quest;
            if (OnQuestBegin != null) OnQuestBegin(quest);
            return;
#endif
            QuestInfo.CachedQuestMemberList = new List<Serial>(members);

            foreach (var item in members)
            {
                if (item.target != null)
                {
                    if (!FellowCache.Contains(item.target.owner.PublicID, CurrentTime))
                        cachedSupport = item.target.owner.PublicID;

                    supportCard = item.target.ID;
                    break;
                }
            }

            SendCommand(new Commands.Quest.BeginRequest { QuestID = questId, Leader = members[0].Value, SubLeader = members[1].Value, Members = Serial.Convert(QuestInfo.CachedQuestMemberList), Support = cachedSupport, Crusade = crusade, DisableRescue = disableRescue, Ticket = ticket });
        }
        public void EndQuest(IQuestResult result, List<int> history)
        {
#if OFFLINE
            var settlement = new QuestSettlement { IsSucc = isSucc };
            settlement.Drops = new List<int>();
            QuestInfo.OnQuestEnd(settlement);

            if (OnQuestEnd != null) OnQuestEnd(settlement);
            return;
#endif
            bool isRescueSucc = true;
            foreach (var item in result.Results)
            {
                if (item.IsSucc) continue;
                isRescueSucc = false;
                break;
            }

            if (result.IsFinish)
            {
                EnqueueMissionTrigger(Calc.CoMission.TriggerQuest(CooperateMissionInfo.SelectSubject(CurrentTime), QuestInfo.Current.QuestID, CardInfo.GetIDs(QuestInfo.CachedQuestMemberList)));
                EnqueueSyncData(typeof(SyncQuestHistory), new SyncQuestHistory { List = history });
                if(challenge != null) EnqueueSyncData(typeof(SyncChallenge), challenge);
            }

            var cmd = new Commands.Quest.EndRequest { Serial = QuestInfo.Current.Serial, IsQuestSucc = result.IsFinish, IsAutoBattleEnabled = isAutoBattleEnabled, List = Serial.Convert(QuestInfo.CachedQuestMemberList), IsRescueSucc = isRescueSucc, SupportCard = supportCard };

            if (QuestInfo.Current.QuestID == Tables.PVPQuestID)
            {
                cmd.IsPVPWin = result.IsFinish;
                cmd.IsQuestSucc = true;
            }

            SendCommand(cmd);
        }
        public void EndQuest()
        {
            EnqueueMissionTrigger(Calc.CoMission.TriggerQuest(CooperateMissionInfo.SelectSubject(CurrentTime), QuestInfo.Current.QuestID, CardInfo.GetIDs(QuestInfo.CachedQuestMemberList)));

            SendCommand(new Commands.Quest.EndRequest { Serial = QuestInfo.Current.Serial, IsQuestSucc = true, IsAutoBattleEnabled = true, List = Serial.Convert(QuestInfo.CachedQuestMemberList) });
        }
        public void OpenQuest(int questId)
        {
            SendCommand(new Commands.Quest.OpenRequest { QuestID = questId });
        }

        public void QueryCrusade()
        {
            if (QuestInfo.Crusade == null || QuestInfo.Crusade.IsOverdue)
                SendCommand(new Commands.Quest.QueryCrusadeRequest());
            else
                DelayInvoke(TriggerOnCrusade);
        }

        public void OnAutoBattleEnabled()
        {
            isAutoBattleEnabled = true;
        }

        public void StayAtCurrentLevel()
        {
            isStayAtCurrentLevel = true;
        }

        public void EnqueueMissionTrigger(IEnumerable<SyncMission> list)
        {
#if !DEBUG
            if (Global.Tables.GetVariable(VariableID.COMISSION_ENABLE) == 0) return;      
#endif
            var isEmpty = true;
            foreach (var item in list)
            {
                EnqueueSyncData(item, item);
                isEmpty = false;
            }

            if (!isEmpty) CooperateMissionInfo.IsOverdue = true;
        }

        private SyncChallenge challenge;

        public void SetChallenge(params int[] values)
        {
            if (challenge == null) challenge = new SyncChallenge();
            challenge.List.Add(new Challenge { Values = new List<int>(values) });
        }
        #endregion
        #region Gacha
        public void GachaDraw(int id)
        {
            SendCommand(new Commands.Gacha.DrawRequest { GachaID = id });
        }
        #endregion
        #region Purchase
        public void Purchase(ArticleID id, int param = 0)
        {
            SendCommand(new Commands.Purchase.ArticleRequest { ArticleID = id, Param = param });
        }
        #endregion

        #region Gift

        public void GiftList()
        {
#if OFFLINE
            return;
#endif
            if (GiftInfo == null || GiftInfo.IsOverdue)
                SendCommand(new Commands.Gift.InfoRequest());
            else
                DelayInvoke(TriggerOnGiftList);   
        }
        public void GiftClaim(Int64 serial)
        {
            SendCommand(new Commands.Gift.ClaimRequest { Serial = serial });
        }

        public void GiftBatchClaim(int count)
        {
            SendCommand(new Commands.Gift.ClaimRequest { Count = count });
        }

        private void TriggerOnGiftList()
        {
            if (OnGiftList != null) OnGiftList();
        }
        #endregion

        #region Trophy
        public void TrophyList()
        {
#if OFFLINE
            return;
#endif
            if (TrophyInfo == null || TrophyInfo.IsOverdue)
                SendCommand(new Commands.Trophy.InfoRequest());
            else
                DelayInvoke(TriggerOnTrophyList);
        }
        private void TriggerOnTrophyList()
        {
            if (OnTrophyList != null) OnTrophyList();
        }
        #endregion

        #region Daily

        public void GetDailyInfo()
        {
            if (DailyInfo == null || DailyInfo.IsOverdue)
                SendCommand(new Commands.Daily.InfoRequest());
            else
                TriggerOnDailyInfo(0);
        }
        private void TriggerOnDailyInfo(int dropId)
        {
            if (OnDailyInfo != null) OnDailyInfo(dropId);
        }
        public void DailyActivePick(int index)
        {
            SendCommand(new Commands.Daily.PickRequest { Index = index });
        }
        #endregion

        #region RandomShop

        public void GetRandomShopInfo()
        {
            if (RandomShopInfo == null || RandomShopInfo.IsOverdue)
                SendCommand(new Commands.RandomShop.InfoRequest());
            else
                TriggerOnRandomShopInfo();
        }
        private void TriggerOnRandomShopInfo()
        {
            if (OnRandomShopInfo != null) OnRandomShopInfo();
        }

        public void RandomShopPurchase(int index, int articleId)
        {
            SendCommand(new Commands.RandomShop.PurchaseRequest { Index = index, ArticleID = articleId });
        }

        public void RandomShopRefresh()
        {
            SendCommand(new Commands.RandomShop.InfoRequest { ForceRefresh = true });
        }
        #endregion

        #region Item

        public void ItemUse(int itemId)
        {
            SendCommand(new Commands.Item.UseRequest { ItemID = itemId });
        }
        #endregion

        #region Ranking

        public void GetRankingInfo()
        {
            if (RankingInfo == null || RankingInfo.IsOverdue)
            {
                if (QuestInfo.Crusade != null)
                    SendCommand(new Commands.Quest.QueryCrusadeRequest());
                SendCommand(new Commands.Ranking.InfoRequest());
            }
            else
                DelayInvoke(TriggerOnRankingInfo);
        }

        public void RankingQuery(int eventId, int from)
        {
            var ranking = RankingInfo[eventId];

            if (ranking != null && ranking.Players.ContainsKey(from))
                TriggerOnRankingQuery(eventId, ranking.Players[from]);
            else
                SendCommand(new Commands.Ranking.QueryRequest { EventID = eventId, From = from });
        }

        private void TriggerOnRankingQuery(int eventId, List<Data.RankingPlayer> players)
        {
            if (OnRankingQuery != null) OnRankingQuery(eventId, players);
        }
        #endregion

        #region Social

        public int CommitDailyFP()
        {
            if (dailyFP != null)
            {
                var value = dailyFP.Add;
                dailyFP = null;
                return value;
            }

            return 0;
        }


        public void GetSocialInfo()
        {
            if (SocialInfo == null || SocialInfo.IsOverdue)
                SendCommand(new Commands.Social.InfoRequest());
            else
                DelayInvoke(TriggerOnSocialInfo);
        }

        public void SocialAdd(string id)
        {
            if(SocialInfo.FollowCount >= PlayerInfo.Data.n_FOLLOW)
                TriggerOnError(ErrorCode.SocialFollowOverflow, ErrorLevel.Notify);
            else
                SendCommand(new Commands.Social.AddRequest { ID = id });
        }

        public void SocialRemove(string id)
        {
            SendCommand(new Commands.Social.RemoveRequest { ID = id });
        }

        public void SocialQuery(string id)
        {
            var ids = new List<string>(1);
            ids.Add(id);
            SocialQuery(ids); 
        }

        public void SocialQuery(List<string> players)
        {
            var queryList = new List<string>();
            foreach (var item in players)
            {
                var fellow = SocialInfo[item];
                if (fellow == null || fellow.IsOverdue) queryList.Add(item);
            }

            if(queryList.Count > 0)
                SendCommand(new Commands.Social.QueryRequest { List = queryList });
            else
                DelayInvoke(TriggerOnSocialQuery);
        }

        public void SocialQueryRanking(List<string> players)
        {
            var filter = new List<string>();

            foreach (var item in players)
            {
                var player = RankingInfo.GetPlayer(item);

                if (player == null) filter.Add(item);
            }

            if (filter.Count > 0)
                SendCommand(new Commands.Social.QueryRankRequest { List = filter });
            else
                DelayInvoke(TriggerOnRankingPlayer);
        }

        private void TriggerOnRankingPlayer()
        {
            if (OnRankingPlayer != null) OnRankingPlayer();
        }

        public void SocialRandomQuery()
        {
            if (SocialInfo.RandomListUpdateTime.AddMinutes(3) < CurrentTime)
                SendCommand(new Commands.Social.RandomRequest());
            else
                DelayInvoke(TriggerOnSocialRandomQuery);
        }


        public void GetSocialPurchaseHistory()
        {
            if (SocialInfo.PurchaseHistoryUpdateTime.Date < CurrentTime.Date)
                SendCommand(new Commands.Social.PurchaseHistoryRequest());
            else
                DelayInvoke(TriggerOnSocialPurchaseHistory);
        }

        private void TriggerOnSocialRandomQuery()
        {
            if (OnSocialRandomQuery != null) OnSocialRandomQuery(SocialInfo.RandomList);
        }

        private void TriggerOnSocialPurchaseHistory()
        {
            if (OnSocialPurchaseHistory != null) OnSocialPurchaseHistory(SocialInfo.GetPurchaseHistory(CurrentTime));
        }
        #endregion

        #region IAP

        public void GetIAPInfo()
        {
            SendCommand(new Commands.IAP.InfoRequest());
        }

        public void IAPClaim(string productId, string orderId, string token)
        {
            SendCommand(new Commands.IAP.ClaimRequest { ProductID = productId, OrderID = orderId, Token = token });
        }
#if MYCARD

        public void MyCardBeginTransaction(string productId)
        {
            SendCommand(new Commands.MyCard.BeginTransactionRequest { ProductID = productId });
        }

#endif
        #endregion

        #region CooperateMission

        public void GetCooperateMissionInfo()
        {
            if (CooperateMissionInfo.IsOverdue)
                SendCommand(new Commands.CooperateMission.InfoRequest());
            else
                TriggerOnCooperateMissionInfo();
        }

        public void GetMissionRanking(Int64 serial)
        {

        }

        public void MissionClaim(Int64 serial)
        {
            SendCommand(new Commands.CooperateMission.ClaimRequest { Serial = serial });
        }

        public void MissionDonate(Int64 serial, int subjectId, int count)
        {
            foreach (var item in CooperateMissionInfo.List)
            {
                if (item.Serial != serial) continue;

                for (int i = 0; i < item.Subjects.Count; ++i)
                {
                    var subject = item.Subjects[i];
                    if (subject.ID != subjectId) continue;
                    SendCommand(new Commands.CooperateMission.DonateRequest { Mission = new SyncMission { Serial = serial, Index = i, Count = count } });
                    return;
                }
            }
        }

        #endregion

        #region Faith

        public void GetFaithInfo()
        {
            if (FaithInfo == null || FaithInfo.IsOverdue)
                SendCommand(new Commands.Faith.InfoRequest());
            else
                DelayInvoke(TriggerOnFaithInfo);
        }

        private void TriggerOnFaithInfo()
        {
            if (OnFaithInfo != null) OnFaithInfo();
        }

        public void FaithClaim()
        {
            SendCommand(new Commands.Faith.ClaimRequest());
        }

        public void FaithBeginProduce(int index, List<int> recipe)
        {
            SendCommand(new Commands.Faith.BeginProduceRequest { Index = index, List = recipe });
        }

        public void FaithEndProduce(Int64 serial)
        {
            SendCommand(new Commands.Faith.EndProduceRequest { Serial = serial });
        }

        #endregion

        #region Tactics

        public void TacticsEnable(int tacticsId)
        {
            SendCommand(new Commands.Tactics.EnableRequest { TacticsID = tacticsId });
        }

        #endregion

        #region PVP

        public void GetPVPInfo()
        {
            if (PVPInfo.IsOverdue)
                SendCommand(new Commands.PVP.InfoRequest());
            else
                DelayInvoke(TriggerOnPVPInfo);
        }

        private void TriggerOnPVPInfo()
        {
            if (OnPVPInfo != null) OnPVPInfo();
        }

        public void PVPUpdate(List<Data.PVPWave> waves)
        {
            var diffs = new List<Internal.PVPWaveDiff>(5);
            foreach (var wave in waves)
            {
                var diff = new Internal.PVPWaveDiff();
                diff.Serial = wave.Card.Serial;
                diff.Selections = wave.Selections;
                diff.Prologs = wave.Prologs;
                diffs.Add(diff);
            }

            SendCommand(new Commands.PVP.UpdateRequest { Diffs = diffs });
        }

        public void PVPRerollSkill(int index)
        {
            SendCommand(new Commands.PVP.RerollSkillRequest { Index = index });
        }

        public void PVPQuery(bool force = false)
        {
            if (force || PVPInfo.IsQueryOverdue)
                SendCommand(new Commands.PVP.QueryRequest());
            else
                DelayInvoke(TriggerOnPVPQuery);
        }

        public void PVPBegin(List<Serial> members, string ticket)
        {
            BeginQuest(0, members, true, 0, ticket);
        }

        public bool PVPInit()
        {
            if (PVPInfo.Boss == null)
            {
                var diffs = default(List<Internal.PVPWaveDiff>);

                if (!TeamInfo.IsEmpty)
                {
                    var current = TeamInfo[TeamInfo.Selection];

                    diffs = PVPUtils.ConvertToWaveDiffs(current);

                    if (diffs == null)
                    {
                        foreach (var item in TeamInfo.List)
                        {
                            diffs = PVPUtils.ConvertToWaveDiffs(current);
                            if (diffs != null) break;
                        }
                    }
                }

                if (diffs == null)
                {
                    var list = new List<Serial>(5);
                    for (int i = 0; i < 5; ++i) list.Add(CardInfo.List[i].Serial);
                    diffs = PVPUtils.ConvertToWaveDiffs(list);
                }


                if (diffs != null)
                {
                    SendCommand(new Commands.PVP.UpdateRequest { Diffs = diffs });
                    return true;
                }
            }

            return false;
        }
        #endregion

    }
}
