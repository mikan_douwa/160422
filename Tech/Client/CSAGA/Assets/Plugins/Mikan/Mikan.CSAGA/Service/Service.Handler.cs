﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mikan.CSAGA
{
    public partial class Service : GameService
    {
        private Dictionary<Type, Action<ICommand>> handlers = new Dictionary<Type, Action<ICommand>>();

        private void RegisterCommandHandlers()
        {

            RegisterCommandHandler<Commands.Account.CreateResponse>(onAccountCreate);
            RegisterCommandHandler<Commands.Account.LoginResponse>(onAccountLogin);
            RegisterCommandHandler<Commands.Account.InitResponse>(onAccountInit);
            RegisterCommandHandler<Commands.Account.PublishResponse>(onAccountPublish);
            RegisterCommandHandler<Commands.Account.TransferBeginResponse>(onAccountTransferBegin);
            RegisterCommandHandler<Commands.Account.TransferEndResponse>(onAccountTransferEnd);

            RegisterCommandHandler<Commands.Player.CreateResponse>(onPlayerCreate);
            RegisterCommandHandler<Commands.Player.InitResponse>(onPlayerInit);
            RegisterCommandHandler<Commands.Player.NameResponse>(onPlayerName);

            RegisterCommandHandler<Commands.Card.InfoResponse>(onCardInfo);
            RegisterCommandHandler<Commands.Card.EnhanceResponse>(onCardEnhance);
            RegisterCommandHandler<Commands.Card.SellResponse>(onCardSell);
            RegisterCommandHandler<Commands.Card.BeginTalkResponse>(onCardBeginTalk);
            RegisterCommandHandler<Commands.Card.EndTalkResponse>(onCardEndTalk);
            RegisterCommandHandler<Commands.Card.GiftResponse>(onCardGift);
            RegisterCommandHandler<Commands.Card.EventResponse>(onCardEvent);
            RegisterCommandHandler<Commands.Card.AwakenResponse>(onCardAwaken);
            RegisterCommandHandler<Commands.Card.LockResponse>(onCardLock);
            RegisterCommandHandler<Commands.Card.HeaderResponse>(onCardHeader);
            RegisterCommandHandler<Commands.Card.LuckResponse>(onCardLuck);

            RegisterCommandHandler<Commands.Item.InfoResponse>(onItemInfo);
            RegisterCommandHandler<Commands.Item.UseResponse>(onItemUse);

            RegisterCommandHandler<Commands.Equipment.InfoResponse>(onEquipmentInfo);
            RegisterCommandHandler<Commands.Equipment.ReplaceResponse>(onEquipmentReplace);
            RegisterCommandHandler<Commands.Equipment.SellResponse>(onEquipmentSell);
            RegisterCommandHandler<Commands.Equipment.RemoveResponse>(onEquipmentRemove);
            RegisterCommandHandler<Commands.Equipment.LockResponse>(onEquipmentLock);

            RegisterCommandHandler<Commands.Quest.InfoResponse>(onQuestInfo);
            RegisterCommandHandler<Commands.Quest.BeginResponse>(onQuestBegin);
            RegisterCommandHandler<Commands.Quest.EndResponse>(onQuestEnd);
            RegisterCommandHandler<Commands.Quest.OpenResponse>(onQuestOpen);
            RegisterCommandHandler<Commands.Quest.QueryCrusadeResponse>(onQuestQueryCrusade);

            RegisterCommandHandler<Commands.Gift.InfoResponse>(onGiftInfo);
            RegisterCommandHandler<Commands.Gift.ClaimResponse>(onGiftClaim);

            RegisterCommandHandler<Commands.Purchase.InfoResponse>(onPurchaseInfo);
            RegisterCommandHandler<Commands.Purchase.ArticleResponse>(onPurchaseArticle);

            RegisterCommandHandler<Commands.Gacha.DrawResponse>(onGachaDraw);

            RegisterCommandHandler<Commands.Trophy.InfoResponse>(onTrophyInfo);

            RegisterCommandHandler<Commands.Gallery.SyncResponse>(onGallerySync);

            RegisterCommandHandler<Commands.Daily.InfoResponse>(onDailyInfo);
            RegisterCommandHandler<Commands.Daily.PickResponse>(onDailyPick);

            RegisterCommandHandler<Commands.RandomShop.InfoResponse>(onRandomShopInfo);
            RegisterCommandHandler<Commands.RandomShop.PurchaseResponse>(onRandomShopPurchase);

            RegisterCommandHandler<Commands.Ranking.InfoResponse>(onRankingInfo);
            RegisterCommandHandler<Commands.Ranking.QueryResponse>(onRankingQuery);

            RegisterCommandHandler<Commands.Social.InfoResponse>(onSocialInfo);
            RegisterCommandHandler<Commands.Social.QueryResponse>(onSocialQuery);
            RegisterCommandHandler<Commands.Social.AddResponse>(onSocialAdd);
            RegisterCommandHandler<Commands.Social.RemoveResponse>(onSocialRemove);
            RegisterCommandHandler<Commands.Social.RandomResponse>(onSocialRandom);
            RegisterCommandHandler<Commands.Social.PurchaseHistoryResponse>(onSocialPurchaseHistory);
            RegisterCommandHandler<Commands.Social.QueryRankResponse>(onSocialQueryRank);

            RegisterCommandHandler<Commands.IAP.InfoResponse>(onIAPInfo);
            RegisterCommandHandler<Commands.IAP.ClaimResponse>(onIAPClaim);
            RegisterCommandHandler<Commands.IAP.InitResponse>(onIAPInit);

            RegisterCommandHandler<Commands.CooperateMission.InfoResponse>(onCooperateMissionInfo);
            RegisterCommandHandler<Commands.CooperateMission.ClaimResponse>(onCooperateMissionClaim);
            RegisterCommandHandler<Commands.CooperateMission.DonateResponse>(onCooperateMissionDonate);

            RegisterCommandHandler<Commands.Faith.InfoResponse>(onFaithInfo);
            RegisterCommandHandler<Commands.Faith.ClaimResponse>(onFaithClaim);
            RegisterCommandHandler<Commands.Faith.BeginProduceResponse>(onFaithBeginProduce);
            RegisterCommandHandler<Commands.Faith.EndProduceResponse>(onFaithEndProduce);

            RegisterCommandHandler<Commands.Tactics.InfoResponse>(onTacticsInfo);
            RegisterCommandHandler<Commands.Tactics.EnableResponse>(onTacticsEnable);

            RegisterCommandHandler<Commands.PVP.InfoResponse>(onPVPInfo);
            RegisterCommandHandler<Commands.PVP.QueryResponse>(onPVPQuery);
            RegisterCommandHandler<Commands.PVP.UpdateResponse>(onPVPUpdate);
            RegisterCommandHandler<Commands.PVP.RerollSkillResponse>(onPVPRerollSkill);

#if MYCARD
            RegisterCommandHandler<Commands.MyCard.BeginTransactionResponse>(onMyCardBeginTransaction);
#endif

            RegisterCommandHandler<Commands.ReportResponse>(onReport);

            RegisterCommandHandler<Commands.RestoreResponse>(onRestore);
        }

        private void RegisterCommandHandler<T>(Action<ICommand> handler)
        {
            handlers.Add(typeof(T), handler);
        }

        protected override bool ProcessCommand(ICommand cmd)
        {
            var handler = default(Action<ICommand>);

            if (!handlers.TryGetValue(cmd.GetType(), out handler)) return false;

            var res = cmd as Commands.CSAGAResponse;
            if (res != null && res.IsTrophySensitive)
            {
                if (GiftInfo != null) GiftInfo.IsOverdue = true;
                if (TrophyInfo != null) TrophyInfo.IsOverdue = true;
                if (DailyInfo != null) DailyInfo.IsOverdue = true;
            }

            handler(cmd);

            return true;
        }

        #region Sys

        private void TriggerOnError(ErrorCode errCode, ErrorLevel level)
        {
            TriggerOnError(errCode, level, "");
        }

        private void TriggerOnError(ErrorCode errCode, ErrorLevel level, string errMsg)
        {
            var errText = "";

            switch (errCode)
            {
                case ErrorCode.ItemNotEnough:
                    errText = Global.Tables.GetSystemText(SystemTextID.TIP_ITEM_NOT_ENOUGH);
                    break;
                case ErrorCode.GemNotEnough:
                    errText = Global.Tables.GetSystemText(SystemTextID.TIP_GEM_NOT_ENOUGH);
                    break;
                case ErrorCode.Maintaining:
                    errText = errMsg;
                    level = ErrorLevel.Fatal;
                    break;
                default:
                    errText = Global.Tables.GetErrorText(errCode);
                    break;
            }

            if (OnError != null) OnError(errCode, errText, level);

            if (level == ErrorLevel.Fatal) CancelQueuedCommands();
        }

        protected override void OnFail(ICommand req, ICommand res)
        {
            var _res = res as Commands.CSAGAResponse;

            if (!IsLogin)
            {
                if (_res != null && _res.Status == ErrorCode.AccountNotExists)
                {
                    SendCommand(new Commands.Account.CreateRequest { UUID = ServiceProvider.UUID });
                    return;
                }
            }
            else if(_res != null)
            {
                switch (_res.Status)
                {
                    case ErrorCode.NotLoginYet:
                    case ErrorCode.InvalidToken:
                    case ErrorCode.TokenExpired:
                        IsLogin = false;
                        isReLogin = true;
                        Login(platform, appVersion, signature, favorite);
                        return;
                }
            }

            var errCode = _res != null ? _res.Status : ErrorCode.NetFail;

            if (_res != null)
            {
                KernelService.Logger.Debug("operation failed, req = {0}, res = {1}, err = {2}", req, res, res.ErrorMessage);
                TriggerOnError(_res.Status, ErrorLevel.Error, res.ErrorMessage);
            }
            else
            {
                KernelService.Logger.Debug("net failed, req = {0}", req);
                TriggerOnError(ErrorCode.NetFail, ErrorLevel.Fatal, null);

            }
        }

        protected override void OnCancel(ICommand req)
        {
            if(isReLogin) cachedList.Enqueue(req);
        }
        #endregion

        #region Login

        private void onAccountCreate(ICommand cmd)
        {
            var uuid = !Profile.IsEmpty ? Profile.UUID : null;

            var res = cmd as Commands.Account.CreateResponse;

            Profile.DeSerialize(res.Profile);
            if (!Profile.IsEmpty)
            {
                if (Profile.UUID != uuid) ServiceProvider.ClearLocalData();

                Save(LocalKey.Profile, Profile);
                Login(platform, appVersion, signature, favorite);
            }
        }
        private void onAccountLogin(ICommand cmd)
        {
            var res = cmd as Commands.Account.LoginResponse;

            Resolver.ChangeToken(res.Token);

            isMaintaining = res.IsMaintaining;

            TimeDiff = DateTime.Now.Subtract(res.ServerTime);

            if ((!string.IsNullOrEmpty(signature) && signature != res.Signature) || (!string.IsNullOrEmpty(appVersion) && appVersion.CompareTo(res.AppVersion) < 0))
            {
                KernelService.Logger.Debug("version not match!!, old - app_ver = {0} | signature = {1}, new - app_ver = {2} | signature = {3}", appVersion, signature, res.AppVersion, res.Signature);
                isPatchNotMatch = true;
            }

            EnqueueSyncData(typeof(SyncPlatform), new SyncPlatform { Platform = this.platform, AppVer = this.appVersion, Transaction = Convert.ToBase64String(Transaction.ToArray()) });

            SendCommand(new Commands.Account.InitRequest());
        }

        private void onAccountInit(ICommand cmd)
        {
            SetFavorite(favorite);
            SendCommand(new Commands.Player.InitRequest { Header = TeamInfo.Header.Value });
        }

        private void onRestore(ICommand cmd)
        {
            var res = cmd as Commands.RestoreResponse;
            BackupInfo.Sync(res.List);
        }
        #endregion

        #region Transfer

        private void onAccountPublish(ICommand cmd)
        { 
            var res = cmd as Commands.Account.PublishResponse;

            if (OnAccountPublish != null) OnAccountPublish(res.Token);
        }

        private void onAccountTransferBegin(ICommand cmd)
        {
            var res = cmd as Commands.Account.TransferBeginResponse;

            transferCache.Key = res.Key;
            transferCache.Profile = res.Profile;

            if (OnAccountTransferBegin != null) OnAccountTransferBegin(transferCache.PublicID, res.Name, res.Rank, res.Gem);
        }

        private void onAccountTransferEnd(ICommand cmd)
        {
            ServiceProvider.ClearLocalData();
            Profile.DeSerialize(transferCache.Profile);
            Save(LocalKey.Profile, Profile);
            transferCache = null;

            if (transferRestoring)
                Login(platform, appVersion, signature, favorite);
            else if (OnAccountTransferEnd != null)
                OnAccountTransferEnd();
        }

        #endregion

        #region Player
        private void onPlayerCreate(ICommand cmd)
        {
            if (OnPlayerCreate != null) OnPlayerCreate();
        }
        
        private void onPlayerInit(ICommand cmd)
        {
            var res = cmd as Commands.Player.InitResponse;

            IsLogin = true;

            loginTime = CurrentTime;

            PlayerInfo = DeSerialize(PlayerInfo, res.PlayerInfo);

            if (isReLogin)
            {
                isReLogin = false;

                if (PurchaseInfo != null) PlayerInfo[PropertyID.Gem] = PurchaseInfo.Gem;

                if (DailyInfo != null) DailyInfo.IsOverdue = true;

                if (TrophyInfo != null) TrophyInfo.IsOverdue = true;

                if (RankingInfo != null) RankingInfo.IsOverdue = true;

                if (FaithInfo != null) FaithInfo.IsOverdue = true;

                if (PVPInfo != null) PVPInfo.IsOverdue = true;

                while (cachedList.Count > 0) SendCommand(cachedList.Dequeue());
                return;
            }

            if (PlayerInfo.IsAvailable)
            {
                if (syncPlayer != null)
                {
                    PlayerInfo.ApplyChange(syncPlayer.Diff);
                    syncPlayer = null;
                }
                QueryEssential();
            }
            else
            {
                if (OnLogin != null) OnLogin(TimeDiff.TotalSeconds);
            }
        }

        private void onPlayerName(ICommand cmd)
        {
            var res = cmd as Commands.Player.NameResponse;
            PlayerInfo.Name = res.Name;
        }
        #endregion

        #region Card
        private void onCardInfo(ICommand cmd)
        {
            var res = cmd as Commands.Card.InfoResponse;
            CardInfo = DeSerialize(CardInfo, res.CardInfo);

            foreach (var item in Tables.Card.Select())
            {
                if(CardInfo.GetLuck(item.n_ID) > 0) GalleryInfo.Add(item.n_ID);
            }
        }

        private void onCardEnhance(ICommand cmd)
        {
            var res = cmd as Commands.Card.EnhanceResponse;

            onCardOperate(CardOpertaionType.Enhance, res);
        }
        private void onCardSell(ICommand cmd)
        {
            var res = cmd as Commands.Card.SellResponse;

            onCardOperate(CardOpertaionType.Sell, res);
        }
        private void onCardBeginTalk(ICommand cmd)
        {
            var res = cmd as Commands.Card.BeginTalkResponse;

            onCardOperate(CardOpertaionType.BeginTalk, res);

            if (OnCardTalkBegin != null) OnCardTalkBegin(Serial.Create(res.Serial), res.InteractiveID, 0);
        }
        private void onCardEndTalk(ICommand cmd)
        {
            var res = cmd as Commands.Card.EndTalkResponse;

            var card = CardInfo[res.Serial];

            card.ReservedInteractiveID = 0;

            onCardOperate(CardOpertaionType.EndTalk, res);

            if (OnCardTalkEnd != null) OnCardTalkEnd(Serial.Create(res.Serial), res.EventID, res.DropID);
        }
        private void onCardGift(ICommand cmd)
        {
            var res = cmd as Commands.Card.GiftResponse;

            onCardOperate(CardOpertaionType.Gift, res);

            if (OnCardGift != null) OnCardGift();
        }

        private void onCardEvent(ICommand cmd)
        {
            var res = cmd as Commands.Card.EventResponse;

            onCardOperate(CardOpertaionType.Event, res);

            if (OnCardEvent != null) OnCardEvent(Serial.Create(res.Serial), res.InteractiveID, res.DropID);
        }

        private void onCardAwaken(ICommand cmd)
        {
            var res = cmd as Commands.Card.AwakenResponse;

            onCardOperate(CardOpertaionType.Awaken, res);
        }

        private void onCardLock(ICommand cmd)
        {
            var res = cmd as Commands.Card.LockResponse;

            onCardOperate(CardOpertaionType.Lock, res);
        }

        private void onCardHeader(ICommand cmd)
        {
            var res = cmd as Commands.Card.HeaderResponse;

            TeamInfo.Header = Serial.Create(res.Serial);
        }

        private void onCardLuck(ICommand cmd)
        {
            var res = cmd as Commands.Card.LuckResponse;

            onCardOperate(CardOpertaionType.Luck, res);
        }

        private void onCardOperate(CardOpertaionType type, Commands.Card.ResponseBase res)
        {
            if (OnCardOperate != null) OnCardOperate(type, res.SyncCard, res.SyncPlayer != null ? res.SyncPlayer.Diff : null);
        }
        #endregion

        #region 道具
        private void onItemInfo(ICommand cmd)
        {
            var res = cmd as Commands.Item.InfoResponse;
            ItemInfo = DeSerialize(ItemInfo, res.ItemInfo);
        }
        private void onItemUse(ICommand cmd)
        {
            var res = cmd as Commands.Item.UseResponse;

            if (OnItemUse != null) OnItemUse(res.DropID);
            
        }
        #endregion

        #region 裝備
        private void onEquipmentInfo(ICommand cmd)
        {
            var res = cmd as Commands.Equipment.InfoResponse;
            EquipmentInfo = DeSerialize(EquipmentInfo, res.EquipmentInfo);
        }
        private void onEquipmentReplace(ICommand cmd)
        {
            var res = cmd as Commands.Equipment.ReplaceResponse;
            if (res.CardSerial > 0) CardInfo[res.CardSerial].OnEquipmentReplace();

            if (OnEquipmentReplace != null) OnEquipmentReplace();
        }
        private void onEquipmentSell(ICommand cmd)
        {
            if (OnEquipmentSell != null) OnEquipmentSell();
        }
        private void onEquipmentRemove(ICommand cmd)
        {
            var res = cmd as Commands.Equipment.RemoveResponse;
            if (res.CardSerial > 0) CardInfo[res.CardSerial].OnEquipmentReplace();

            if (OnEquipmentReplace != null) OnEquipmentReplace();
        }
        private void onEquipmentLock(ICommand cmd)
        {
            var res = cmd as Commands.Equipment.LockResponse;

            if (OnEquipmentChange != null) OnEquipmentChange(res.Serial);
            
        }
        #endregion

        #region 任務
        private void onQuestInfo(ICommand cmd)
        {
            var res = cmd as Commands.Quest.InfoResponse;
            QuestInfo = DeSerialize(QuestInfo, res.QuestInfo);
            QuestInfo.ApplyChange(res.Limited);
        }

        private void onQuestBegin(ICommand cmd)
        {
            isInQuest = true;

            var res = cmd as Commands.Quest.BeginResponse;
            QuestInfo.Current = res.Quest;
            if (OnQuestBegin != null) OnQuestBegin(res.Quest);
        }

        private void onQuestEnd(ICommand cmd)
        {
            isInQuest = false;

            var res = cmd as Commands.Quest.EndResponse;

            QuestInfo.OnQuestEnd(res.Settlement);

            var limited = QuestInfo.GetLimited(QuestInfo.Current.QuestID);

            if (limited != null && limited.IsInfinity)
            {
                var list = new List<int>();
                foreach(var item in Tables.QuestDesign.Select(o=>{ return o.n_MAIN_QUEST == limited.MainQuestID; })) list.Add(item.n_ID);
                list.Sort();
                var index = list.IndexOf(QuestInfo.Current.QuestID);

                if (!isStayAtCurrentLevel)
                {
                    if (res.IsQuestSucc)
                    {
                        if (index < list.Count - 1)
                            InfinityInfo.Update(limited.MainQuestID, list[index + 1]);
                    }
                    else
                    {
                        if (index > 0)
                            InfinityInfo.Update(limited.MainQuestID, list[index - 1]);
                    }
                }
            }

            if (res.IsQuestSucc)
            {
                if (!string.IsNullOrEmpty(cachedSupport))
                {
                    FellowCache.Add(cachedSupport, CurrentTime);
                    cachedSupport = null;
                }

                if (cachedCrusade > 0)
                {
                    if (!QuestInfo.Crusade.PlayList.Contains(cachedCrusade))
                        QuestInfo.Crusade.PlayList.Add(cachedCrusade);
                }
            }

            cachedCrusade = 0;
            
            if (OnQuestEnd != null) OnQuestEnd(res.Settlement);
        }

        private void onQuestOpen(ICommand cmd)
        {
            var res = cmd as Commands.Quest.OpenResponse;

            if (OnQuestOpen != null) OnQuestOpen(res.QuestID);
        }

        private void onQuestQueryCrusade(ICommand cmd)
        {
            var res = cmd as Commands.Quest.QueryCrusadeResponse;

            QuestInfo.Crusade = DeSerialize(QuestInfo.Crusade, res.Crusade);

            TriggerOnCrusade();
        }

        private void TriggerOnCrusade()
        {
            if (OnCrusade != null) OnCrusade();
        }

        #endregion
        #region 贈獎
        private void onGiftInfo(ICommand cmd)
        {
            var res = cmd as Commands.Gift.InfoResponse;

            GiftInfo = DeSerialize(GiftInfo, res.GiftInfo);

            TriggerOnGiftList();
        }

        private void onGiftClaim(ICommand cmd)
        {
            var res = cmd as Commands.Gift.ClaimResponse;

            GiftInfo = DeSerialize(GiftInfo, res.GiftInfo);

            GiftInfo.Remove(res.Serial);

            if (OnGiftClaim != null) OnGiftClaim(res.List);
        }

        #endregion
        #region 消費
        private void onPurchaseInfo(ICommand cmd)
        {
            var res = cmd as Commands.Purchase.InfoResponse;
            PurchaseInfo = DeSerialize(PurchaseInfo, res.PurchaseInfo);
            PlayerInfo[PropertyID.Gem] = PurchaseInfo.Gem;
        }

        private void onPurchaseArticle(ICommand cmd)
        {
            var res = cmd as Commands.Purchase.ArticleResponse;

            PurchaseInfo.Set(res.ArticleID, res.Count);

            if (OnPurchase != null) OnPurchase(res.ArticleID);
        }
        #endregion

        #region 轉蛋
        private void onGachaDraw(ICommand cmd)
        {
            var res = cmd as Commands.Gacha.DrawResponse;

            if (res.GachaID == Global.Tables.RegularGachaIDs[0])
                PurchaseInfo.Add(ArticleID.RegularGachaDaily);
            else if(res.GachaID == Global.Tables.ActivityGachaIDs[0])
                PurchaseInfo.Add(ArticleID.ActivityGachaDaily);
            else if(Global.Tables.ActivityGachaIDs.Contains(res.GachaID))
                PurchaseInfo.Add(ArticleID.ActivityGacha);
            else
                PurchaseInfo.Add(ArticleID.RegularGacha);

            if (OnGachaDraw != null) OnGachaDraw(res.SyncCard);
        }
        #endregion
        #region 圖鑑
        private void onGallerySync(ICommand cmd)
        {
            // do nothing
        }
        #endregion
        #region 成就
        private void onTrophyInfo(ICommand cmd)
        {
            var res = cmd as Commands.Trophy.InfoResponse;
            TrophyInfo = DeSerialize(TrophyInfo, res.TrophyInfo);

            TriggerOnTrophyList();
        }
        #endregion

        #region 每日
        private void onDailyInfo(ICommand cmd)
        {
            var res = cmd as Commands.Daily.InfoResponse;
            DailyInfo = DeSerialize(DailyInfo, res.DailyInfo);

            TriggerOnDailyInfo(0);
        }

        private void onDailyPick(ICommand cmd)
        {
            var res = cmd as Commands.Daily.PickResponse;
            DailyInfo = DeSerialize(DailyInfo, res.DailyInfo);

            TriggerOnDailyInfo(res.DropID);
        }
        #endregion

        #region 隨機商店

        private void onRandomShopInfo(ICommand cmd)
        {
            var res = cmd as Commands.RandomShop.InfoResponse;
            RandomShopInfo = DeSerialize(RandomShopInfo, res.RandomShopInfo);

            TriggerOnRandomShopInfo();
        }

        private void onRandomShopPurchase(ICommand cmd)
        {
            var res = cmd as Commands.RandomShop.PurchaseResponse;

            var data = Tables.GachaList[res.ArticleID];

            if (data != null && (data.n_TAG == GachaListTag.Random || data.n_TAG == GachaListTag.Static))
            {
                RandomShopInfo.State.Value = res.State;
            }

            if (OnRandomShopPurchase != null) OnRandomShopPurchase(res.DropID);
            
        }

        #endregion

        #region 排名

        private void onRankingInfo(ICommand cmd)
        {
            var res = cmd as Commands.Ranking.InfoResponse;

            RankingInfo = DeSerialize(RankingInfo, res.RankingInfo);

            TriggerOnRankingInfo();
        }

        private void onRankingQuery(ICommand cmd)
        {
            var res = cmd as Commands.Ranking.QueryResponse;

            var list = new List<Data.RankingPlayer>(res.List.Count);
            foreach (var item in res.List)
                list.Add(new Data.RankingPlayer { PublicID = item.PublicID, CardID = item.CardID, Point = item.Point, Ranking = item.Ranking });

            var ranking = RankingInfo[res.EventID];

            if(ranking == null) 
            {
                ranking = new Data.Ranking{ EventID = res.EventID };
                RankingInfo.List.Add(res.EventID, ranking);
            }

            ranking.Players[res.From] = list;

            TriggerOnRankingQuery(res.EventID, list);
        }

        private void TriggerOnRankingInfo()
        {
            if (OnRankingInfo != null) OnRankingInfo();
        }

        #endregion

        #region 社群

        private void onSocialInfo(ICommand cmd)
        {
            var res = cmd as Commands.Social.InfoResponse;

            SocialInfo = DeSerialize(SocialInfo, res.SocialInfo);

            TriggerOnSocialInfo();
        }
        private void TriggerOnSocialInfo()
        {
            if (OnSocialInfo != null) OnSocialInfo();
        }

        private void onSocialQuery(ICommand cmd)
        {
            var res = cmd as Commands.Social.QueryResponse;

            if (SocialInfo == null) SocialInfo = new Data.SocialInfo();

            foreach (var item in res.Snapshots) SocialInfo.Update(item);

            TriggerOnSocialQuery();
        }
        private void TriggerOnSocialQuery()
        {
            if (OnSocialQuery != null) OnSocialQuery();
        }
        private void onSocialAdd(ICommand cmd)
        {
            var res = cmd as Commands.Social.AddResponse;

            var fellow = SocialInfo[res.ID];

            if (fellow != null) fellow.Complex[0] = true;

            SocialInfo.Add(fellow);

            if (OnSocialUpdate != null) OnSocialUpdate(res.ID);
        }
        private void onSocialRemove(ICommand cmd)
        {
            var res = cmd as Commands.Social.RemoveResponse;

            var fellow = SocialInfo[res.ID];

            if (fellow != null) fellow.Complex[0] = false;

            if (OnSocialUpdate != null) OnSocialUpdate(res.ID);
        }

        private void onSocialRandom(ICommand cmd)
        {
            var res = cmd as Commands.Social.RandomResponse;

            SocialInfo.RandomList = res.List;
            SocialInfo.RandomListUpdateTime = CurrentTime;

            TriggerOnSocialRandomQuery();
        }

        private void onSocialPurchaseHistory(ICommand cmd)
        {
            var res = cmd as Commands.Social.PurchaseHistoryResponse;

            SocialInfo.PurchaseHistoryList = res.List;
            SocialInfo.PurchaseHistoryUpdateTime = CurrentTime;

            TriggerOnSocialPurchaseHistory();
        }

        private void onSocialQueryRank(ICommand cmd)
        {
            var res = cmd as Commands.Social.QueryRankResponse;

            if (RankingInfo.PlayerList == null) RankingInfo.PlayerList = new Dictionary<string, Data.SocialPlayer>();
            
            foreach (var item in res.List)
            {
                RankingInfo.PlayerList[item.PublicID] = new Data.SocialPlayer { PublicID = item.PublicID, Name = item.Name, UpdateTime = CurrentTime };
            }


            TriggerOnRankingPlayer();
        }
        #endregion

        #region IAP

        private void onIAPInfo(ICommand cmd)
        {
            var res = cmd as Commands.IAP.InfoResponse;
            IAPInfo = DeSerialize(IAPInfo, res.IAPInfo);

            if (OnIAPInfo != null) OnIAPInfo();
        }

        private void onIAPClaim(ICommand cmd)
        {
            var res = cmd as Commands.IAP.ClaimResponse;

            if (OnIAPClaim != null) OnIAPClaim(res.OrderID, res.ProductID, res.Gem);
        }

        private void onIAPInit(ICommand cmd)
        {
            if (OnLogin != null) OnLogin(TimeDiff.TotalSeconds);
        }

#if MYCARD
        private void onMyCardBeginTransaction(ICommand cmd)
        {
            var res = cmd as Commands.MyCard.BeginTransactionResponse;
            if (OnMyCardBeginTransaction != null) OnMyCardBeginTransaction(res.AuthCode, res.ProductID, res.Sandbox);
        }
#endif
        #endregion

        #region CooperateMission

        private void onCooperateMissionInfo(ICommand cmd)
        {
            var res = cmd as Commands.CooperateMission.InfoResponse;

            CooperateMissionInfo = DeSerialize(CooperateMissionInfo, res.CooperateMissionInfo);

            TriggerOnCooperateMissionInfo();
        }

        private void onCooperateMissionClaim(ICommand cmd)
        {
            var res = cmd as Commands.CooperateMission.ClaimResponse;

            var mission = CooperateMissionInfo[res.Serial];
            mission.State.IsClaimed = true;
            CooperateMissionInfo.IsOverdue = true;

            if (OnCooperateMissionClaim != null) OnCooperateMissionClaim(res.Rewards);
        }

        private void onCooperateMissionDonate(ICommand cmd)
        {
            CooperateMissionInfo.IsOverdue = true;

            if (OnCooperateMissionDonate != null) OnCooperateMissionDonate();
        }

        private void TriggerOnCooperateMissionInfo()
        {
            if (OnCooperateMissionInfo != null) OnCooperateMissionInfo();
        }

        #endregion

        #region Faith

        private void onFaithInfo(ICommand cmd)
        {
            var res = cmd as Commands.Faith.InfoResponse;

            FaithInfo = DeSerialize(FaithInfo, res.FaithInfo);

            TriggerOnFaithInfo();
        }

        private void onFaithClaim(ICommand cmd)
        {
            var res = cmd as Commands.Faith.ClaimResponse;

            FaithInfo.ClaimTime = res.ClaimTime;

            if(res.Diff != null) 
            {
                if (OnFaithClaim != null) OnFaithClaim(res.Diff);
            }
        }

        private void onFaithBeginProduce(ICommand cmd)
        {
            var res = cmd as Commands.Faith.BeginProduceResponse;

            if (OnFaithUpdate != null) OnFaithUpdate();
        }

        private void onFaithEndProduce(ICommand cmd)
        {
            var res = cmd as Commands.Faith.EndProduceResponse;

            if (OnFaithUpdate != null) OnFaithUpdate();

            if (OnFaithEndProduce != null) OnFaithEndProduce(res.DropID);
        }

        #endregion

        #region Tactics

        private void onTacticsInfo(ICommand cmd)
        {
            var res = cmd as Commands.Tactics.InfoResponse;

            TacticsInfo = DeSerialize(TacticsInfo, res.TacticsInfo);
        }

        private void onTacticsEnable(ICommand cmd)
        {
            var res = cmd as Commands.Tactics.EnableResponse;

            if (OnTacticsEnable != null) OnTacticsEnable(res.TacticsID);
        }

        #endregion

        #region PVP

        private void onPVPInfo(ICommand cmd)
        {
            var res = cmd as Commands.PVP.InfoResponse;

            PVPInfo = DeSerialize(PVPInfo, res.PVPInfo);

            if (PVPInfo.Waves == null || PVPInfo.Waves.Count == 0)
            {
                if (!TeamInfo.IsEmpty)
                {
                    var current = TeamInfo[TeamInfo.Selection];

                    var diffs = PVPUtils.ConvertToWaveDiffs(current);

                    if (diffs == null)
                    {
                        foreach (var item in TeamInfo.List)
                        {
                            diffs = PVPUtils.ConvertToWaveDiffs(current);
                            if (diffs != null) break;
                        }
                    }

                    if(diffs != null) SendCommand(new Commands.PVP.UpdateRequest { Diffs = diffs });
                }
            }

            TriggerOnPVPInfo();
        }

        private void onPVPUpdate(ICommand cmd)
        {
            var res = cmd as Commands.PVP.UpdateResponse;

            PVPInfo.Update(res.Diffs);
            
            if (OnPVPUpdate != null) OnPVPUpdate();
        }

        private void onPVPRerollSkill(ICommand cmd)
        {
            var res = cmd as Commands.PVP.RerollSkillResponse;

            PVPInfo.Waves[res.Index].Skills = res.Skills;

            if (OnPVPRerollSkill != null) OnPVPRerollSkill();
        }

        private void onPVPQuery(ICommand cmd)
        {
            var res = cmd as Commands.PVP.QueryResponse;

            PVPInfo.RefreahPlayer(res.ListObj);

            TriggerOnPVPQuery();
        }

        private void TriggerOnPVPQuery()
        {
            if (OnPVPQuery != null) OnPVPQuery();
        }

        

        #endregion

        #region Debug
        private void onReport(ICommand cmd)
        {
            var res = cmd as Commands.ReportResponse;
            if (OnReport != null) OnReport(res.Type);
        }
        #endregion
    }
}
