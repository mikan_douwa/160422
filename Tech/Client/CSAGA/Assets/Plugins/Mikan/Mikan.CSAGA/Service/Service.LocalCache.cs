﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mikan.CSAGA
{
    public partial class Service : GameService
    {
        internal void Backup(object key, Backup backup)
        {
            EnqueueSyncData(key, backup);
        }

        internal void Save(LocalKey key, SerializableObject obj)
        {
            ServiceProvider.SaveLocalData(Format(key), obj.Serialize());
        }

        internal void Load(LocalKey key, SerializableObject obj)
        {
            var base64 = ServiceProvider.LoadString(Format(key));
            if (!string.IsNullOrEmpty(base64)) obj.DeSerialize(base64);
        }

        internal T Load<T>(LocalKey key)
        where T : SerializableObject, new()
        {
            var obj = new T();
            Load(key, obj);
            return obj;
        }

        private string Format(LocalKey key)
        {
            return string.Format("sys.{0}", key);
        }
    }

    internal enum LocalKey
    {
        Profile,
        TeamInfo,
        LocalCache,
        GalleryInfo,
        TransferCache,
        InfinityInfo,
        BackupInfo,
        FellowCache,
    }
}
