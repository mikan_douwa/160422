﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mikan.CSAGA
{
    public partial class Service : GameService
    {
        
        public Service()
        {
            Instance = this;
            Resolver = new CommandResolver(this);
        }
        public ConstData.Tables Tables { get { return ConstData.Tables.Instance; } }
        public string Token { get { return Resolver.Token; } }
        protected override ISerializer<ICommand> Serializer { get { return Resolver.Serializer; } }

        public override void Setup(ISystemServiceProvider serviceProvider)
        {
            base.Setup(serviceProvider);

            // 在此呼叫是為了能讓client也能自行計算
            Calc.Calculator.Initialize();

            RegisterCommandHandlers();
            SetupSyncManger();

            Profile = Load<Data.Profile>(LocalKey.Profile);

            GachaInfo = new Data.GachaInfo();
            SocialInfo = new Data.SocialInfo();
        }

        public override void TriggerEvent()
        {
            base.TriggerEvent();
#if OFFLINE
            return;
#endif
            GalleryInfo.TriggerEvent();
        }

        public override void SendCommand(ICommand cmd)
        {
            if (isPatchNotMatch && !isInQuest)
            {
                TriggerOnError(ErrorCode.PatchNotMatch, ErrorLevel.Fatal, null);
                return;
            }

            var transferId = MathUtils.Random.Next();
            while (transferId == lastTransferId) transferId = MathUtils.Random.Next(1, int.MaxValue);
            cmd.InjectTransferID(transferId);
            lastTransferId = transferId;

            //KernelService.Logger.Trace("send cmd, {0}", cmd);
            base.SendCommand(cmd);
        }

        protected override byte[] Serialize(ICommand cmd)
        {
            return Resolver.Serialize(cmd);
        }

        protected override ICommand DeSerialize(System.IO.Stream rawdata)
        {
            return Resolver.DeSerialize(rawdata);
        }

        private T DeSerialize<T>(T obj, string base64)
            where T : SerializableObject, new()
        {
            if (obj == null) obj = new T();
            obj.DeSerialize(base64);
            return obj;
        }
    }
}
