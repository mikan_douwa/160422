﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mikan.CSAGA
{
    public partial class Service : GameService
    {
        private SyncManager<Service> SyncManager;

        private void SetupSyncManger()
        {
            SyncManager = new SyncManager<Service>();
            SyncManager.RegisterHandler<SyncPlayer>(OnSyncPlayer);
            SyncManager.RegisterHandler<SyncCard>(OnSyncCard);
            SyncManager.RegisterHandler<SyncItem>(OnSyncItem);
            SyncManager.RegisterHandler<SyncEquipment>(OnSyncEquipment);
            SyncManager.RegisterHandler<SyncLimitedQuest>(OnSyncLimitedQuest);
            SyncManager.RegisterHandler<SyncEventPoint>(OnSyncEventPoint);
            SyncManager.RegisterHandler<SyncRescue>(OnSyncRescue);
            SyncManager.RegisterHandler<SyncMission>(OnSyncMission);
            SyncManager.RegisterHandler<SyncInfinity>(OnSyncInfinity);
            SyncManager.RegisterHandler<SyncInfinityPoint>(OnSyncInfinityPoint);
            SyncManager.RegisterHandler<SyncSocialPurchase>(OnSyncSocialPurchase);
            SyncManager.RegisterHandler<SyncDailyFP>(OnSyncDailyFP);
            SyncManager.RegisterHandler<SyncDailyEventPoint>(OnSyncDailyEventPoint);
            SyncManager.RegisterHandler<SyncCrusade>(OnSyncCrusade);
            SyncManager.RegisterHandler<SyncSharedQuest>(OnSyncSharedQuest);
            SyncManager.RegisterHandler<SyncFaith>(OnSyncFaith);
            SyncManager.RegisterHandler<SyncTactics>(OnSyncTactics);
            SyncManager.RegisterHandler<SyncQuest>(OnSyncQuest);
        }

        protected override void DoSync(ICommand cmd)
        {
            foreach (var item in cmd.SelectSyncData()) SyncManager.OnSyncData(this, item);
        }

        private void OnSyncPlayer(Service owner, ISyncData syncData)
        {
            var data = syncData as SyncPlayer;
            if (PlayerInfo != null)
                PlayerInfo.ApplyChange(data.Diff);
            else
                syncPlayer = data;
        }

        private void OnSyncCard(Service owner, ISyncData syncData)
        {
            var data = syncData as SyncCard;
            if(CardInfo != null) CardInfo.ApplyChange(data);
        }

        private void OnSyncItem(Service owner, ISyncData syncData)
        {
            var data = syncData as SyncItem;
            ItemInfo.ApplyChange(data);
        }

        private void OnSyncEquipment(Service owner, ISyncData syncData)
        {
            var data = syncData as SyncEquipment;
            EquipmentInfo.ApplyChange(data);
        }

        private void OnSyncLimitedQuest(Service owner, ISyncData syncData)
        {
            var data = syncData as SyncLimitedQuest;
            QuestInfo.ApplyChange(data);
        }

        private void OnSyncEventPoint(Service owner, ISyncData syncData)
        {
            var data = syncData as SyncEventPoint;

            if(RankingInfo != null) RankingInfo.ApplyChange(data);
        }

        private void OnSyncRescue(Service owner, ISyncData syncData)
        {
            var data = syncData as SyncRescue;

            QuestInfo.ApplyChange(data);
        }

        private void OnSyncMission(Service owner, ISyncData syncData)
        {
            var data = syncData as SyncMission;

            CooperateMissionInfo.ApplyChange(data);
        }

        private void OnSyncInfinity(Service owner, ISyncData syncData)
        {
            var data = syncData as SyncInfinity;

            InfinityInfo.ApplyChange(data);
        }

        private void OnSyncInfinityPoint(Service owner, ISyncData syncData)
        {
            var data = syncData as SyncInfinityPoint;

            InfinityInfo.ApplyChange(data);
        }

        private void OnSyncSocialPurchase(Service owner, ISyncData syncData)
        {
            var data = syncData as SyncSocialPurchase;

            SocialInfo.ApplyChange(data);
        }

        private void OnSyncDailyFP(Service owner, ISyncData syncData)
        {
            var data = syncData as SyncDailyFP;

            dailyFP = data;
        }

        private void OnSyncDailyEventPoint(Service owner, ISyncData syncData)
        {
            var data = syncData as SyncDailyEventPoint;

            dailyEP = data;
        }

        private void OnSyncCrusade(Service owner, ISyncData syncData)
        {
            var data = syncData as SyncCrusade;

            if (QuestInfo != null) QuestInfo.Crusade.ApplyChange(data);
        }

        private void OnSyncSharedQuest(Service owner, ISyncData syncData)
        {
            var data = syncData as SyncSharedQuest;

            if (QuestInfo != null) QuestInfo.Crusade.ApplyChange(data);

            
        }

        private void OnSyncFaith(Service owner, ISyncData syncData)
        {
            var data = syncData as SyncFaith;

            if (FaithInfo != null) FaithInfo.ApplyChange(data);
        }

        private void OnSyncTactics(Service owner, ISyncData syncData)
        {
            var data = syncData as SyncTactics;

            if (TacticsInfo != null) TacticsInfo.ApplyChange(data);
        }

        private void OnSyncQuest(Service owner, ISyncData syncData)
        {
            var data = syncData as SyncQuest;

            if (QuestInfo != null) QuestInfo.ApplyChange(data);

        }
    }
}
