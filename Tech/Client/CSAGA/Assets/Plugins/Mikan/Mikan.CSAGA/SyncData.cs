﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mikan.CSAGA
{
    abstract public class SyncDataBase
    {
        virtual public SyncType Type { get{ return SyncType.Runtime; } }
    }

    #region C->S

    public class Backup : SyncDataBase, ISyncData
    {
        public int Key;
        public string Data;
        public void Serialize(IOutput output)
        {
            output.Write(Key);
            output.Write(Data);
        }

        public void DeSerialize(IInput input)
        {
            Key = input.ReadInt32();
            Data = input.ReadString();
        }
    }

    public class SyncMission : SyncDataBase, ISyncData
    {
        public Int64 Serial;
        public int SubjectID;
        public int Index;
        public int Count;

        public void Serialize(IOutput output)
        {
            output.Write(Serial);
            output.Write(SubjectID);
            output.Write(Index);
            output.Write(Count);
        }

        public void DeSerialize(IInput input)
        {
            Serial = input.ReadInt64();
            SubjectID = input.ReadInt32();
            Index = input.ReadInt32();
            Count = input.ReadInt32();
        }
    }

    public class SyncFavorite : SyncDataBase, ISyncData
    {
        public override SyncType Type { get { return SyncType.Advance; } }

        public Int64 Serial;
        public void Serialize(IOutput output)
        {
            output.Write(Serial);
        }

        public void DeSerialize(IInput input)
        {
            Serial = input.ReadInt64();
        }
    }

    public class SyncPlatform : SyncDataBase, ISyncData
    {
        public string Platform;
        public string AppVer;
        public string Transaction;

        public void Serialize(IOutput output)
        {
            output.Write(Platform);
            output.Write(AppVer);
            output.Write(Transaction);
        }

        public void DeSerialize(IInput input)
        {
            Platform = input.ReadString();
            AppVer = input.ReadString();
            Transaction = input.ReadString();
        }
    }

    public class SyncQuestHistory : SyncDataBase, ISyncData
    {
        public List<int> List;

        public void Serialize(IOutput output)
        {
            output.Write(List);
        }

        public void DeSerialize(IInput input)
        {
            List = input.ReadInt32List();
        }
    }

    public class Challenge : ISerializable
    {
        public List<int> Values;

        public int ID { get { return Values[0]; } }

        public int Value { get { return Values[1]; } }

        public void DeSerialize(IInput input)
        {
            Values = input.ReadInt32List();
        }

        public void Serialize(IOutput output)
        {
            output.Write(Values);
        }
    }
    public class SyncChallenge : SyncDataBase, ISyncData
    {
        public List<Challenge> List = new List<Challenge>();

        public void DeSerialize(IInput input)
        {
            List = input.ReadList<Challenge>();
        }

        public void Serialize(IOutput output)
        {
            output.Write(List);
        }
    }
    #endregion

    #region S->C

    public class SyncPlayer : SyncDataBase, ISyncData
    {
        public PropertyDiff Diff;
        public void Serialize(IOutput output)
        {
            output.Write(Diff);
        }

        public void DeSerialize(IInput input)
        {
            Diff = input.Read<PropertyDiff>();
        }
    }

    public class SyncCard : SyncDataBase, ISyncData
    {
        public List<CardDiff> UpdateList;
        public List<Internal.CardObj> AddList;
        public List<Int64> DeleteList;

        public void Serialize(IOutput output)
        {
            output.Write(UpdateList);
            output.Write(AddList);
            output.Write(DeleteList);
        }

        public void DeSerialize(IInput input)
        {
            UpdateList = input.ReadList<CardDiff>();
            AddList = input.ReadList<Internal.CardObj>();
            DeleteList = input.ReadInt64List();
        }
    }

    public class SyncItem : SyncDataBase, ISyncData
    {
        public Diff Diff;
        public void Serialize(IOutput output)
        {
            output.Write(Diff);
        }

        public void DeSerialize(IInput input)
        {
            Diff = input.Read<Diff>();
        }
    }

    public class SyncEquipment : SyncDataBase, ISyncData
    {
        public List<EquipmentDiff> UpdateList;
        public List<Internal.EquipmentObj> AddList;
        public List<Int64> DeleteList;

        public void Serialize(IOutput output)
        {
            output.Write(UpdateList);
            output.Write(AddList);
            output.Write(DeleteList);
        }

        public void DeSerialize(IInput input)
        {
            UpdateList = input.ReadList<EquipmentDiff>();
            AddList = input.ReadList<Internal.EquipmentObj>();
            DeleteList = input.ReadInt64List();
        }
    }

    public class SyncLimitedQuest : SyncDataBase, ISyncData
    {
        public List<Internal.LimitedQuestObj> List;

        public void Serialize(IOutput output)
        {
            output.Write(List);
        }

        public void DeSerialize(IInput input)
        {
            List = input.ReadList<Internal.LimitedQuestObj>();
        }
    }

    public class SyncEventPoint : SyncDataBase, ISyncData
    {
        public int EventID;
        public int Add;
        public void Serialize(IOutput output)
        {
            output.Write(EventID);
            output.Write(Add);
        }

        public void DeSerialize(IInput input)
        {
            EventID = input.ReadInt32();
            Add = input.ReadInt32();
        }
    }

    public class SyncRescue : SyncDataBase, ISyncData
    {
        public int Count;
        public DateTime UpdateTime;
        public void Serialize(IOutput output)
        {
            output.Write(Count);
            output.Write(UpdateTime);
        }

        public void DeSerialize(IInput input)
        {
            Count = input.ReadInt32();
            UpdateTime = input.ReadDateTime();
        }
    }

    public class SyncCrusade : SyncDataBase, ISyncData
    {
        public int Point;
        public DateTime UpdateTime;
        public void Serialize(IOutput output)
        {
            output.Write(Point);
            output.Write(UpdateTime);
        }

        public void DeSerialize(IInput input)
        {
            Point = input.ReadInt32();
            UpdateTime = input.ReadDateTime();
        }
    }

    public class SyncSharedQuest : SyncDataBase, ISyncData
    {
        public List<Internal.SharedQuestObj> List;

        public void Add(Internal.SharedQuestObj obj)
        {
            if (List == null) List = new List<Internal.SharedQuestObj>();
            List.Add(obj);
        }

        public void Serialize(IOutput output)
        {
            output.Write(List);
        }

        public void DeSerialize(IInput input)
        {
            List = input.ReadList<Internal.SharedQuestObj>();
        }
    }

    public class SyncInfinity : SyncDataBase, ISyncData
    {
        public int EventID;
        public Int64 Seed;
        public DateTime BeginTime;

        public int Point;
        public DateTime UpdateTime;
        public void Serialize(IOutput output)
        {
            output.Write(EventID);
            output.Write(Seed);
            output.Write(BeginTime);
            output.Write(Point);
            output.Write(UpdateTime);
        }

        public void DeSerialize(IInput input)
        {
            EventID = input.ReadInt32();
            Seed = input.ReadInt64();
            BeginTime = input.ReadDateTime();
            Point = input.ReadInt32();
            UpdateTime = input.ReadDateTime();
        }
    }

    public class SyncInfinityPoint : SyncDataBase, ISyncData
    {
        public int Point;
        public DateTime UpdateTime;

        public void Serialize(IOutput output)
        {
            output.Write(Point);
            output.Write(UpdateTime);
        }

        public void DeSerialize(IInput input)
        {
            Point = input.ReadInt32();
            UpdateTime = input.ReadDateTime();
        }
    }

    public class SyncSocialPurchase : SyncDataBase, ISyncData
    {
        public int AricleID;
        public DateTime Timestamp;
        public void Serialize(IOutput output)
        {
            output.Write(AricleID);
            output.Write(Timestamp);
        }

        public void DeSerialize(IInput input)
        {
            AricleID = input.ReadInt32();
            Timestamp = input.ReadDateTime();
        }
    }

    public class SyncDailyFP : SyncDataBase, ISyncData
    {
        public int Add;
        public void Serialize(IOutput output)
        {
            output.Write(Add);
        }

        public void DeSerialize(IInput input)
        {
            Add = input.ReadInt32();
        }
    }

    public class SyncDailyEventPoint : SyncDataBase, ISyncData
    {
        public int Current;
        public DateTime UpdateTime;
        public void Serialize(IOutput output)
        {
            output.Write(Current);
            output.Write(UpdateTime);
        }

        public void DeSerialize(IInput input)
        {
            Current = input.ReadInt32();
            UpdateTime = input.ReadDateTime();
        }
    }

    public class SyncFaith : SyncDataBase, ISyncData
    {
        public List<Internal.FaithObj> List;
        public void Serialize(IOutput output)
        {
            output.Write(List);
        }

        public void DeSerialize(IInput input)
        {
            List = input.ReadList<Internal.FaithObj>();
        }
    }

    public class SyncTactics : SyncDataBase, ISyncData
    {
        public int TacticsID;
        public void Serialize(IOutput output)
        {
            output.Write(TacticsID);
        }

        public void DeSerialize(IInput input)
        {
            TacticsID = input.ReadInt32();
        }
    }

    public class SyncQuest : SyncDataBase, ISyncData
    {
        public int QuestID;
        public int Complex;
        public void Serialize(IOutput output)
        {
            output.Write(QuestID);
            output.Write(Complex);
        }

        public void DeSerialize(IInput input)
        {
            QuestID = input.ReadInt32();
            Complex = input.ReadInt32();
        }
    }

    #endregion
}
