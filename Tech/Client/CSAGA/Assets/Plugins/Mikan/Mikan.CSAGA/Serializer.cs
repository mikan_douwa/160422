﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mikan.CSAGA
{
    /// <summary>
    /// 序列化工具類別
    /// </summary>
    public class Serializer : DefaultSerializer<ICommand>
    {
        /// <inheritdoc />
        protected override void RegisterType()
        {
            CSAGACommand.UnRegisterAll();

            // 如有相容舊版本的需求, 新增的類別只能放在EndRegion()前面, 否則會造成資料錯亂

            DoRegisterType<Commands.InvalidOperation>();
			DoRegisterType<Commands.RegularError>();
			DoRegisterType<Commands.DebugRequest>();
			DoRegisterType<Commands.DebugResponse>();
			DoRegisterType<Commands.ReportRequest>();
			DoRegisterType<Commands.ReportResponse>();
            DoRegisterType<Commands.MaintenanceMessage>();
            DoRegisterType<Commands.RestoreRequest>();
            DoRegisterType<Commands.RestoreResponse>();
            EndRegion();

            // 100
			DoRegisterType<Commands.Account.CreateRequest>();
			DoRegisterType<Commands.Account.CreateResponse>();
			DoRegisterType<Commands.Account.LoginRequest>();
			DoRegisterType<Commands.Account.LoginResponse>();
			DoRegisterType<Commands.Account.InitRequest>();
			DoRegisterType<Commands.Account.InitResponse>();
			DoRegisterType<Commands.Account.PublishRequest>();
			DoRegisterType<Commands.Account.PublishResponse>();
			DoRegisterType<Commands.Account.TransferBeginRequest>();
			DoRegisterType<Commands.Account.TransferBeginResponse>();
			DoRegisterType<Commands.Account.TransferEndRequest>();
			DoRegisterType<Commands.Account.TransferEndResponse>();
            EndRegion();

            // 200
			DoRegisterType<Commands.Player.CreateRequest>();
			DoRegisterType<Commands.Player.CreateResponse>();
			DoRegisterType<Commands.Player.InitRequest>();
			DoRegisterType<Commands.Player.InitResponse>();
			DoRegisterType<Commands.Player.NameRequest>();
			DoRegisterType<Commands.Player.NameResponse>();
            EndRegion();

            // 300
			DoRegisterType<Commands.Card.InfoRequest>();
			DoRegisterType<Commands.Card.InfoResponse>();
			DoRegisterType<Commands.Card.EnhanceRequest>();
			DoRegisterType<Commands.Card.EnhanceResponse>();
			DoRegisterType<Commands.Card.SellRequest>();
			DoRegisterType<Commands.Card.SellResponse>();
			DoRegisterType<Commands.Card.BeginTalkRequest>();
			DoRegisterType<Commands.Card.BeginTalkResponse>();
			DoRegisterType<Commands.Card.EndTalkRequest>();
			DoRegisterType<Commands.Card.EndTalkResponse>();
			DoRegisterType<Commands.Card.GiftRequest>();
			DoRegisterType<Commands.Card.GiftResponse>();
			DoRegisterType<Commands.Card.EventRequest>();
			DoRegisterType<Commands.Card.EventResponse>();
			DoRegisterType<Commands.Card.AwakenRequest>();
			DoRegisterType<Commands.Card.AwakenResponse>();
			DoRegisterType<Commands.Card.LockRequest>();
            DoRegisterType<Commands.Card.LockResponse>();
            DoRegisterType<Commands.Card.HeaderRequest>();
            DoRegisterType<Commands.Card.HeaderResponse>();
            DoRegisterType<Commands.Card.LuckRequest>();
            DoRegisterType<Commands.Card.LuckResponse>();
            EndRegion();

            // 400
			DoRegisterType<Commands.Item.InfoRequest>();
			DoRegisterType<Commands.Item.InfoResponse>();
			DoRegisterType<Commands.Item.UseRequest>();
			DoRegisterType<Commands.Item.UseResponse>();
            EndRegion();

            // 500
			DoRegisterType<Commands.Quest.InfoRequest>();
			DoRegisterType<Commands.Quest.InfoResponse>();
			DoRegisterType<Commands.Quest.BeginRequest>();
			DoRegisterType<Commands.Quest.BeginResponse>();
			DoRegisterType<Commands.Quest.EndRequest>();
			DoRegisterType<Commands.Quest.EndResponse>();
            DoRegisterType<Commands.Quest.BeginEventRequest>();
            DoRegisterType<Commands.Quest.OpenRequest>();
            DoRegisterType<Commands.Quest.OpenResponse>();
            DoRegisterType<Commands.Quest.QueryCrusadeRequest>();
            DoRegisterType<Commands.Quest.QueryCrusadeResponse>();
            EndRegion();

            // 600
			DoRegisterType<Commands.Equipment.InfoRequest>();
			DoRegisterType<Commands.Equipment.InfoResponse>();
			DoRegisterType<Commands.Equipment.ReplaceRequest>();
			DoRegisterType<Commands.Equipment.ReplaceResponse>();
			DoRegisterType<Commands.Equipment.SellRequest>();
			DoRegisterType<Commands.Equipment.SellResponse>();
			DoRegisterType<Commands.Equipment.RemoveRequest>();
            DoRegisterType<Commands.Equipment.RemoveResponse>();
            DoRegisterType<Commands.Equipment.LockRequest>();
            DoRegisterType<Commands.Equipment.LockResponse>();
            EndRegion();

            // 700
			DoRegisterType<Commands.Gacha.DrawRequest>();
			DoRegisterType<Commands.Gacha.DrawResponse>();
            EndRegion();

            // 800
			DoRegisterType<Commands.Purchase.InfoRequest>();
			DoRegisterType<Commands.Purchase.InfoResponse>();
			DoRegisterType<Commands.Purchase.ArticleRequest>();
			DoRegisterType<Commands.Purchase.ArticleResponse>();
            EndRegion();

            // 900
			DoRegisterType<Commands.Gift.InfoRequest>();
			DoRegisterType<Commands.Gift.InfoResponse>();
			DoRegisterType<Commands.Gift.ClaimRequest>();
			DoRegisterType<Commands.Gift.ClaimResponse>();
            EndRegion();

            // 1000
			DoRegisterType<Commands.Gallery.SyncRequest>();
			DoRegisterType<Commands.Gallery.SyncResponse>();
            EndRegion();

            // 1100
			DoRegisterType<Commands.Trophy.InfoRequest>();
			DoRegisterType<Commands.Trophy.InfoResponse>();
            EndRegion();

            // 1200
			DoRegisterType<Commands.Daily.InfoRequest>();
			DoRegisterType<Commands.Daily.InfoResponse>();
			DoRegisterType<Commands.Daily.PickRequest>();
			DoRegisterType<Commands.Daily.PickResponse>();
            EndRegion();

            // 1300
			DoRegisterType<Commands.RandomShop.InfoRequest>();
			DoRegisterType<Commands.RandomShop.InfoResponse>();
			DoRegisterType<Commands.RandomShop.PurchaseRequest>();
			DoRegisterType<Commands.RandomShop.PurchaseResponse>();
            EndRegion();

            // 1400
			DoRegisterType<Commands.Ranking.InfoRequest>();
			DoRegisterType<Commands.Ranking.InfoResponse>();
			DoRegisterType<Commands.Ranking.QueryRequest>();
			DoRegisterType<Commands.Ranking.QueryResponse>();
            EndRegion();

            // 1500
			DoRegisterType<Commands.Social.QueryRankRequest>();
            DoRegisterType<Commands.Social.QueryRankResponse>();

            DoRegisterType<Commands.Social.InfoRequest>();
            DoRegisterType<Commands.Social.InfoResponse>();
            DoRegisterType<Commands.Social.QueryRequest>();
            DoRegisterType<Commands.Social.QueryResponse>();
            DoRegisterType<Commands.Social.RandomRequest>();
            DoRegisterType<Commands.Social.RandomResponse>();
            DoRegisterType<Commands.Social.AddRequest>();
            DoRegisterType<Commands.Social.AddResponse>();
            DoRegisterType<Commands.Social.RemoveRequest>();
            DoRegisterType<Commands.Social.RemoveResponse>();
            DoRegisterType<Commands.Social.PurchaseHistoryRequest>();
            DoRegisterType<Commands.Social.PurchaseHistoryResponse>();
            EndRegion();

            // 1600
			DoRegisterType<Commands.IAP.InfoRequest>();
			DoRegisterType<Commands.IAP.InfoResponse>();
			DoRegisterType<Commands.IAP.ClaimRequest>();
            DoRegisterType<Commands.IAP.ClaimResponse>();
            DoRegisterType<Commands.IAP.InitRequest>();
            DoRegisterType<Commands.IAP.InitResponse>();
            EndRegion();

            // 1700
            DoRegisterType<Commands.CooperateMission.InfoRequest>();
            DoRegisterType<Commands.CooperateMission.InfoResponse>();
            DoRegisterType<Commands.CooperateMission.DonateRequest>();
            DoRegisterType<Commands.CooperateMission.DonateResponse>();
            DoRegisterType<Commands.CooperateMission.ClaimRequest>();
            DoRegisterType<Commands.CooperateMission.ClaimResponse>();
            EndRegion();

#if MYCARD  // 1800
            DoRegisterType<Commands.MyCard.BeginTransactionRequest>();
            DoRegisterType<Commands.MyCard.BeginTransactionResponse>();
#endif
            EndRegion();

            // 1900
            DoRegisterType<Commands.Faith.InfoRequest>();
            DoRegisterType<Commands.Faith.InfoResponse>();
            DoRegisterType<Commands.Faith.ClaimRequest>();
            DoRegisterType<Commands.Faith.ClaimResponse>();
            DoRegisterType<Commands.Faith.BeginProduceRequest>();
            DoRegisterType<Commands.Faith.BeginProduceResponse>();
            DoRegisterType<Commands.Faith.EndProduceRequest>();
            DoRegisterType<Commands.Faith.EndProduceResponse>();
            EndRegion();

            // 2000
            DoRegisterType<Commands.Tactics.InfoRequest>();
            DoRegisterType<Commands.Tactics.InfoResponse>();
            DoRegisterType<Commands.Tactics.EnableRequest>();
            DoRegisterType<Commands.Tactics.EnableResponse>();
            EndRegion();

            // 2100
            DoRegisterType<Commands.PVP.InfoRequest>();
            DoRegisterType<Commands.PVP.InfoResponse>();
            DoRegisterType<Commands.PVP.UpdateRequest>();
            DoRegisterType<Commands.PVP.UpdateResponse>();
            DoRegisterType<Commands.PVP.QueryRequest>();
            DoRegisterType<Commands.PVP.QueryResponse>();
            DoRegisterType<Commands.PVP.RerollSkillRequest>();
            DoRegisterType<Commands.PVP.RerollSkillResponse>();
            EndRegion();

            ListSerializeUtil<ISyncData>.Initialize();

            RegisterDynamicType<ISyncData, Backup>();
            RegisterDynamicType<ISyncData, SyncPlayer>();
            RegisterDynamicType<ISyncData, SyncCard>();
            RegisterDynamicType<ISyncData, SyncItem>();
            RegisterDynamicType<ISyncData, SyncEquipment>();
            RegisterDynamicType<ISyncData, SyncLimitedQuest>();
            RegisterDynamicType<ISyncData, SyncEventPoint>();
            RegisterDynamicType<ISyncData, SyncRescue>();
            RegisterDynamicType<ISyncData, SyncMission>();
            RegisterDynamicType<ISyncData, SyncFavorite>();
            RegisterDynamicType<ISyncData, SyncPlatform>();
            RegisterDynamicType<ISyncData, SyncInfinity>();
            RegisterDynamicType<ISyncData, SyncInfinityPoint>();
            RegisterDynamicType<ISyncData, SyncSocialPurchase>();
            RegisterDynamicType<ISyncData, SyncDailyFP>();
            RegisterDynamicType<ISyncData, SyncDailyEventPoint>();
            RegisterDynamicType<ISyncData, SyncQuestHistory>();
            RegisterDynamicType<ISyncData, SyncCrusade>();
            RegisterDynamicType<ISyncData, SyncSharedQuest>();
            RegisterDynamicType<ISyncData, SyncFaith>();
            RegisterDynamicType<ISyncData, SyncTactics>();
            RegisterDynamicType<ISyncData, SyncChallenge>();
            RegisterDynamicType<ISyncData, SyncQuest>();
        }

		private void DoRegisterType<U>()
			where U : ISerializable, ICommand, new()
		{
			RegisterType<U>();
			CSAGACommand.RegisterType<U>();
		}
		/*
        /// <inheritdoc />
        protected override void RegisterType<U>()
        {
            base.RegisterType<U>();

            CSAGACommand.RegisterType<U>();
        }
		*/
    }

    /// <summary>
    /// 本地資料物件
    /// </summary>
    abstract public class LocalObject : SerializableObject
    {
        
        /// <summary>
        /// 儲存用鍵值
        /// </summary>
        abstract internal LocalKey LocalKey { get; }

        /// <summary>
        /// Server端備份用鍵值
        /// </summary>
        virtual protected int ServerKey { get { throw new NotImplementedException(); } }

        /// <summary>
        /// 建構子
        /// </summary>
        public LocalObject()
        {
            Global.Service.Load(LocalKey, this);
        }

        /// <summary>
        /// 將目前資料回寫本地
        /// </summary>
        internal void Save()
        {
            Global.Service.Save(LocalKey, this);
        }

        /// <summary>
        /// 將目前資料備份到Server端
        /// </summary>
        internal void Sync()
        {
            if (IsEmpty) return;

            var data = new Backup { Key = ServerKey, Data = Serialize() };

            Global.Service.Backup(this, data);
        }
    }

    abstract public class LocalObject<T> : LocalObject
        where T : LocalObject, new()
    {

        private static T instance;
        public static T Instance
        {
            get
            {
                if (instance == null) instance = new T();
                return instance;
            }
        }
    }

}
