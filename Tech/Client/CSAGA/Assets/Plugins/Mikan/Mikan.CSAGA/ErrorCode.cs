﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mikan.CSAGA
{
    public enum ErrorCode
    {
        Success = 0,

        /// <summary>
        /// 保留
        /// </summary>
        _Error = 1,

        /// <summary>
        /// 系統尚未初始化完成
        /// </summary>
        Initializing = 2,
        /// <summary>
        /// Token已過期
        /// </summary>
        TokenExpired,
        /// <summary>
        /// 無效的Token
        /// </summary>
        InvalidToken,
        /// <summary>
        /// 重複的操作
        /// </summary>
        RaceCondition,
        /// <summary>
        /// 尚未登入
        /// </summary>
        NotLoginYet,
        /// <summary>
        /// 無效的流水號
        /// </summary>
        InvalidSerial,

        /// <summary>
        /// 資料版號不符
        /// </summary>
        PatchNotMatch,

        /// <summary>
        /// App版號不符
        /// </summary>
        AppNotMatch,

        /// <summary>
        /// 維修中
        /// </summary>
        Maintaining,

        #region Common
        CrystalNotEnough = 100,
        GemNotEnough,
        ItemNotEnough,
        CoinNotEnough,
        FPNotEnough,
        #endregion

        #region Account
        AccountExists = 200,
        AccountNotExists,
        AccountInvalid,
        AccountNotAllow,
        AcccountPublishCD,
        AccountTransferFail,
        AccountTransferIllegal,
        #endregion

        #region Player
        PlayerIllegal = 300,
        #endregion

        #region Card
        CardIllegal = 400,
        CardNotExists,
        KizunaNotEnough,
        #endregion

        #region Quest
        QuestIllegal = 500,
        QuestNotExists,
        QuestOverdue,
        QuestExhausted,
        QuestDiscoverNotEnough,
        QuestPointNotEnough,
        QuestNotMatch,
        QuestExists,
        #endregion

        #region Equipment
        EquipmentIllegal = 600,
        EquipmentNotExists,
        EquipmentDuplicate,
        #endregion

        #region Shop
        ArticleNotAvailable = 700,
        ArticleOverflow,
        #endregion

        #region Gacha
        GachaIllegal = 800,
        #endregion

        #region Item
        ItemIllegal = 900,
        #endregion

        #region Gift
        GiftUnknown = 1000,
        GiftOverflow,
        #endregion

        #region IAP
        IAPIllegal = 1100,
        IAPTimeout,
        IAPCancelled,
        IAPNotAvailable,
        #endregion

        #region CoMission
        CoMissionIllegal = 1200,
        CoMissionNotReach,
        CoMissionOverdue,
        #endregion

        #region Socail
        SocialAlreadyFollow = 1300,
        SocialFollowOverflow,
        SocialFansOverflow,
        #endregion

        #region Faith
        FaithEmpty = 1400,
        #endregion

        #region Tactics
        TacticsIllegal = 1500,
        #endregion

        #region PVP
        PVPQueryCD = 1600,
        PVPQuestInvalid,
        BPNotEnough,
        #endregion

        Error = 100001,
        InvalidOperation = 100002,
        NotImplemented = 100003,
        NetFail = 100004,
        IllegalOpertaion = 100005,
        Forbidden = 100006,
    }
}
