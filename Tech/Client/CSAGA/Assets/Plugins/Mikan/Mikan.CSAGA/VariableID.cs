﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mikan.CSAGA
{
    public enum VariableID
    {
        /// <summary>
        /// 基礎攻擊速度
        /// </summary>
        BASIC_ASPD = 1,
        /// <summary>
        /// 計算升級的需求經驗值用
        /// </summary>
        EXP_LVUP_STYLE = 2,
        /// <summary>
        /// 賣掉卡片可獲得的經驗水晶
        /// </summary>
        SELL_EXP_GAIN = 3,
        /// <summary>
        /// 同樣禮物傾向的加成
        /// </summary>
        GIFT_BONUS = 4,
        /// <summary>
        /// 交流次數上限
        /// </summary>
        COMMU_LIMIT = 5,
        /// <summary>
        /// 每N分鐘累積1點交流次數
        /// </summary>
        COMMU_CD = 6,
        /// <summary>
        /// 每一次交流增加羈絆值
        /// </summary>
        COMMU_KIZUNA = 7,
        /// <summary>
        /// 羈絆值上限
        /// </summary>
        KIZUNA_LIMIT = 8,

        /// <summary>
        /// 攻擊版每個LV的提升值
        /// </summary>
        ATK_BOARD_POWER = 9,
        /// <summary>
        /// 回復版每個LV的提升值
        /// </summary>
        REV_BOARD_POWER = 10,
        /// <summary>
        /// 特技版每個LV的提升值
        /// </summary>
        TEC_BOARD_POWER = 11,
        /// <summary>
        /// 戰鬥中角色可強化的等級上限
        /// </summary>
        STR_BOARD_LIMIT = 12,
        /// <summary>
        /// 每一級強化可以增加的HP、攻擊、回復%數
        /// </summary>
        STR_BOARD_EFFECT = 13,
        /// <summary>
        /// 時間版每個LV的提升值
        /// </summary>
        TIME_BOARD_POWER = 14,

        /// <summary>
        /// 玩家初始卡片01
        /// </summary>
        INITIAL_CARD_01 = 15,
        /// <summary>
        /// 玩家初始卡片02
        /// </summary>
        INITIAL_CARD_02 = 16,
        /// <summary>
        /// 玩家初始卡片03
        /// </summary>
        INITIAL_CARD_03 = 17,
        /// <summary>
        /// 玩家初始卡片04
        /// </summary>
        INITIAL_CARD_04 = 18,
        /// <summary>
        /// 玩家初始卡片05
        /// </summary>
        INITIAL_CARD_05 = 19,
        /// <summary>
        /// 剋制屬性傷害倍率
        /// </summary>
        ATK_POWERUP = 20,
        /// <summary>
        /// 被剋制屬性傷害倍率
        /// </summary>
        ATK_POWERDOWN = 21,
        /// <summary>
        /// 第二行動條倍率
        /// </summary>
        SECOND_ACT_TIME = 22,
        /// <summary>
        /// 復活耗費寶石
        /// </summary>
        REVIVE_DIAMOND = 23,
        /// <summary>
        /// 補充挑戰次數耗費寶石
        /// </summary>
        EVENT_COUNT_DIAMOND = 24,
        /// <summary>
        /// 補充交流次數耗費寶石
        /// </summary>
        COMMU_COUNT_DIAMOND = 25,
        /// <summary>
        /// 怪物HP條長度(1隻)
        /// </summary>
        MOBHP_LENGTH_1 = 26,
        /// <summary>
        /// 怪物HP條長度(2隻)
        /// </summary>
        MOBHP_LENGTH_2 = 27,
        /// <summary>
        /// 怪物HP條長度(3隻)
        /// </summary>
        MOBHP_LENGTH_3 = 28,
        /// <summary>
        /// 怪物HP條長度(4隻)
        /// </summary>
        MOBHP_LENGTH_4 = 29,
        /// <summary>
        /// 毒版的基礎值
        /// </summary>
        POISON_BOARD_BASE = 30,
        /// <summary>
        /// 毒版每個LV的提升值
        /// </summary>
        POISON_BOARD_POWER = 31,

        /// <summary>
        /// 角色空間上限
        /// </summary>
        CARD_SPACE_LIMIT = 32,
        /// <summary>
        /// 擴充可增加角色空間N格
        /// </summary>
        CARD_SPACE_ADD = 33,
        /// <summary>
        /// 擴充耗費寶石
        /// </summary>
        CARD_SPACE_COST = 34,
        /// <summary>
        /// 初始角色空間
        /// </summary>
        CARD_SPACE_BASE = 35,

        /// <summary>
        /// 初始商品格
        /// </summary>
        SHOP_SLOT_BASIC = 36,

        /// <summary>
        /// 商品格上限
        /// </summary>
        SHOP_SLOT_MAX = 37,

        /// <summary>
        /// 擴充商品格耗費寶石
        /// </summary>
        SHOP_SLOT_DIAMOND = 38,

        /// <summary>
        /// 商品更新時間(分)
        /// </summary>
        SHOP_UPDATE_TIME = 39,

        /// <summary>
        /// 初始活躍點數
        /// </summary>
        ACTIVE_BASIC_SCORE = 40,
        
        /// <summary>
        /// 強化版每個LV的提升值(行動條%數)
        /// </summary>
        STR_BOARD_POWER = 41,

        /// <summary>
        /// 救援活動的起始數量
        /// </summary>
        HELP_INITIAL = 42,

        /// <summary>
        /// 救援活動的上限數量
        /// </summary>
        HELP_MAX = 43,

        /// <summary>
        /// 救援活動的補充時間(分)
        /// </summary>
        HELP_ADD = 44,

        /// <summary>
        /// 經驗水晶上限
        /// </summary>
        EXP_MAX = 45,

        /// <summary>
        /// 代幣上限
        /// </summary>
        TOKEN_MAX = 46,

        /// <summary>
        /// 寶石上限
        /// </summary>
        DIAMOND_MAX = 47,

        /// <summary>
        /// 補充救援次數耗費寶石
        /// </summary>
        HELP_COUNT_DIAMOND = 48,

        /// <summary>
        /// 顯示排行榜上的人數下限
        /// </summary>
        SHOW_RANKING = 49,
        
        /// <summary>
        /// 協力任務首次可得道具數量
        /// </summary>
        CO_MISSION_FIRST = 50,

        /// <summary>
        /// 協力任務重複可得道具數量
        /// </summary>
        CO_MISSION_REPEAT = 51,

        /// <summary>
        /// 協力任務獎勵領取條件
        /// </summary>
        CO_MISSION_ITEM = 52,

        /// <summary>
        /// 爬塔關卡：屬性/類型額外加分
        /// </summary>
        TOWER_BONUS = 53,

        /// <summary>
        /// 爬塔關卡：倒數時間
        /// </summary>
        TOWER_COUNTDOWN = 54,

        /// <summary>
        /// 爬塔關卡：每秒減分%數
        /// </summary>
        TOWER_MINUS = 55,

        /// <summary>
        /// 雙方關注可以獲得的友情點
        /// </summary>
        FP_FOLLOW = 56,

        /// <summary>
        /// 其他戰友可以獲得的友情點
        /// </summary>
        FP_NORMAL = 57,

        /// <summary>
        /// 友情點上限
        /// </summary>
        FP_MAX = 58,

        /// <summary>
        /// 討伐活動的起始數量
        /// </summary>
        CRUSADE_INITIAL = 59,

        /// <summary>
        /// 討伐活動的上限數量
        /// </summary>
        CRUSADE_MAX = 60,

        /// <summary>
        /// 討伐活動的補充時間(分)
        /// </summary>
        CRUSADE_ADD = 61,

        /// <summary>
        /// 討伐關卡維持時間
        /// </summary>
        CRUSADE_TIME = 62,

        /// <summary>
        /// 關卡開啟者可獲得的討伐積分%數
        /// </summary>
        CRUSADE_OWNER = 63,

        /// <summary>
        /// 關卡開啟者可獲得的討伐積分%數上限
        /// </summary>
        CRUSADE_OWNER_LIMIT = 64,

        /// <summary>
        /// 補充討伐次數耗費寶石
        /// </summary>
        CRUSADE_COUNT_DIAMOND = 65,

        /// <summary>
        /// 極運必中上限
        /// </summary>
        GACHA_BOOST = 66,

        /// <summary>
        /// CHAIN數>多少開始有效果&減少時間
        /// </summary>
        CHAIN_ACTIVE = 67,

        /// <summary>
        /// CHAIN的基本維持時間
        /// </summary>
        CHAIN_DURATION = 68,

        /// <summary>
        /// 每+1CHAIN減少的時間
        /// </summary>
        CHAIN_DURATION_LOST = 69,
        
        /// <summary>
        /// CHAIN的最短維持時間
        /// </summary>
        CHAIN_DURATION_MIN = 70,

        /// <summary>
        /// 每+1CHAIN提升的傷害回復提升效果
        /// </summary>
        CHAIN_POWERUP = 71,

        /// <summary>
        /// 信仰時間上限(分)
        /// </summary>
        FAITH_MAX = 76,

        /// <summary>
        /// 多少時間生成一個信仰道具(分)
        /// </summary>
        FAITH_PERIOD = 77,

        /// <summary>
        /// 信仰道具數量浮動值上限(%)
        /// </summary>
        FAITH_BONUS_MAX = 78,

        /// <summary>
        /// 信仰融合第1格開放RANK
        /// </summary>
        FAITH_SLOT_1 = 79,

        /// <summary>
        /// 信仰融合第2格開放RANK
        /// </summary>
        FAITH_SLOT_2 = 80,

        /// <summary>
        /// 信仰融合第3格開放RANK
        /// </summary>
        FAITH_SLOT_3 = 81,

        /// <summary>
        /// 信仰融合第4格開放RANK
        /// </summary>
        FAITH_SLOT_4 = 82,

        /// <summary>
        /// 信仰融合可選擇單項的上限
        /// </summary>
        FAITH_SELECT_MAX = 83,

        /// <summary>
        /// 競技點數上限
        /// </summary>
        PVP_COUNT_LIMIT = 84,

        /// <summary>
        /// 每N分鐘累積1點競技點數
        /// </summary>
        PVP_COUNT_CD = 85,

        /// <summary>
        /// PVP基本積分
        /// </summary>
        PVP_BASE_POINT = 86,

        /// <summary>
        /// PVP加權積分
        /// </summary>
        PVP_EXTRA_POINT = 87,

        /// <summary>
        /// PVP預設技能1
        /// </summary>
        PVP_SKILL_1 = 88,

        /// <summary>
        /// PVP預設技能2
        /// </summary>
        PVP_SKILL_2 = 89,

        /// <summary>
        /// PVP預設技能3
        /// </summary>
        PVP_SKILL_3 = 90,

        /// <summary>
        /// PVP預設技能4
        /// </summary>
        PVP_SKILL_4 = 91,

        /// <summary>
        /// PVP預設技能5
        /// </summary>
        PVP_SKILL_5 = 92,

        /// <summary>
        /// PVP預設技能6
        /// </summary>
        PVP_SKILL_6 = 93,

        /// <summary>
        /// PVP洗技能道具
        /// </summary>
        PVP_SKILL_ITEM = 94,

        /// <summary>
        /// 功能開關：購買寶石
        /// </summary>
        IAP_ENABLE = 9001,

        /// <summary>
        /// 功能開關：活躍
        /// </summary>
        ACTIVE_ENABLE = 9002,

        /// <summary>
        /// 功能開關：交流
        /// </summary>
        COMMU_ENABLE = 9003,

        /// <summary>
        /// 功能開關：覺醒
        /// </summary>
        AWAKEN_ENABLE = 9004,

        /// <summary>
        /// 功能開關：隨機商店
        /// </summary>
        SHOP_ENABLE = 9005,

        /// <summary>
        /// 功能開關：協力任務
        /// </summary>
        COMISSION_RANKING_ENABLE = 9006,

        /// <summary>
        /// 功能開關：協力任務
        /// </summary>
        COMISSION_ENABLE = 9007,

        /// <summary>
        /// 功能開關：錯誤回報
        /// </summary>
        BUG_REPORT_ENABLE = 9008,

        /// <summary>
        /// 功能開關：巴哈姆特
        /// </summary>
        BAHAMUT_ENABLE = 9009,


        /// <summary>
        /// 功能開關：積分說明按鈕
        /// </summary>
        POINT_BUTTON_ENABLE = 9010,

        /// <summary>
        /// 功能開關：外傳
        /// </summary>
        EXSTAGE_ENABLE = 9011,

        /// <summary>
        /// 功能開關：CHAIN
        /// </summary>
        CHAIN_ENABLE = 9012,

        /// <summary>
        /// 功能開關：信仰之泉
        /// </summary>
        FAITH_ENABLE = 9013,

        /// <summary>
        /// 功能開關：戰術
        /// </summary>
        TACTICS_ENABLE = 9014,

    }
}
