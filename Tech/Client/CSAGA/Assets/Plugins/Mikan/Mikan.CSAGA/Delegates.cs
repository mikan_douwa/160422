﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mikan.CSAGA
{
    /// <summary>
    /// 錯誤訊息等級
    /// </summary>
    public enum ErrorLevel
    {
        /// <summary>
        /// 提示訊息
        /// </summary>
        Notify,
        /// <summary>
        /// 警告
        /// </summary>
        Warning,
        /// <summary>
        /// 錯誤
        /// </summary>
        Error,
        /// <summary>
        /// 重大錯誤
        /// </summary>
        Fatal,
    }

    public enum CardOpertaionType
    {
        Enhance,
        Sell,
        BeginTalk,
        EndTalk,
        Gift,
        Event,
        Awaken,
        Lock,
        Luck,
    }

    /// <summary>
    /// 屬性值變更時要呼叫的方法
    /// </summary>
    /// <typeparam name="TSender">事件發送者型別</typeparam>
    /// <typeparam name="TValue">值型別</typeparam>
    /// <param name="sender">事件發送者</param>
    /// <param name="id">屬性ID</param>
    /// <param name="oldValue">原值</param>
    /// <param name="newValue">新值</param>
    public delegate void PropertyChangeDelegate<TSender, TValue>(TSender sender, PropertyID id, TValue oldValue, TValue newValue);

    /// <summary>
    /// 值變更時要呼叫的方法
    /// </summary>
    /// <typeparam name="TSender">事件發送者型別</typeparam>
    /// <typeparam name="TValue">值型別</typeparam>
    /// <param name="sender">事件發送者</param>
    /// <param name="oldValue">原值</param>
    /// <param name="newValue">新值</param>
    public delegate void ValueChangeDelegate<TSender, TValue>(TSender sender, TValue oldValue, TValue newValue);

    /// <summary>
    /// 發生錯誤時要呼叫的方法
    /// </summary>
    /// <param name="errMsg">錯誤代碼</param>
    /// <param name="errMsg">錯誤訊息</param>
    /// <param name="level">錯誤等級</param>
    public delegate void ErrorDelegate(ErrorCode errCode, string errMsg, ErrorLevel level);

    /// <summary>
    /// 獲得獎勵時要呼叫的方法
    /// </summary>
    /// <param name="list">獲得的獎勵列表</param>
    public delegate void RewardDelegate(List<Reward> list);

    /// <summary>
    /// 登入成功時要呼叫的方法
    /// </summary>
    /// <param name="timeDiff">本地時間與伺服器時間相差秒數</param>
    public delegate void LoginDelegate(double timeDiff);

    /// <summary>
    /// 玩家帳號創建完成時要呼叫的方法
    /// </summary>
    public delegate void PlayerCreateDelegate();

    /// <summary>
    /// 收到卡片操作結果時要呼叫的方法
    /// </summary>
    /// <param name="type">操作類型</param>
    /// <param name="cardDiff">卡片變動</param>
    /// <param name="playerDiff">玩家屬性變化</param>
    public delegate void CardOperateDelegate(CardOpertaionType type, SyncCard cardDiff, PropertyDiff playerDiff);

    /// <summary>
    /// 觸發卡片事件時要呼叫的方法
    /// </summary>
    /// <param name="serial"></param>
    /// <param name="interactiveId"></param>
    /// <param name="dropId"></param>
    public delegate void CardEventDelegate(Serial serial, int interactiveId, int dropId);

    /// <summary>
    /// 任務開始時要呼叫的方法
    /// </summary>
    /// <param name="quest"></param>
    public delegate void QuestBeginDelegate(QuestInstance quest);

    /// <summary>
    /// 任務結束時要呼叫的方法
    /// </summary>
    /// <param name="quest"></param>
    public delegate void QuestEndDelegate(QuestSettlement settlement);

    /// <summary>
    /// 討伐任務開啟時要呼叫的方法
    /// </summary>
    /// <param name="questId"></param>
    public delegate void QuestOpenDelegate(int questId);

    /// <summary>
    /// 抽轉蛋時要呼叫的方法
    /// </summary>
    /// <param name="syncCard">卡片同步</param>
    public delegate void GachaDrawDelegate(SyncCard syncCard);

    /// <summary>
    /// 消費完成要呼叫的方法
    /// </summary>
    /// <param name="id"></param>
    public delegate void PurchaseDelegate(ArticleID id);

    /// <summary>
    /// 獎勵領取完成要呼叫的方法
    /// </summary>
    /// <param name="list">獲得的獎勵列表</param>
    public delegate void GiftClaimDelegate(List<Reward> list);

    /// <summary>
    /// 每日活躍資訊更新時要呼叫的方法
    /// </summary>
    /// <param name="dropId">獲得奬勵ID</param>
    public delegate void DailyInfoDelegate(int dropId);


    /// <summary>
    /// 購買隨機商店商品時要呼叫的方法
    /// </summary>
    /// <param name="dropId">獲得奬勵ID</param>
    public delegate void RandomShopPurchaseDelegate(int dropId);

    /// <summary>
    /// 使用禮包道具後要呼叫的方法
    /// </summary>
    /// <param name="dropId">獲得奬勵ID</param>
    public delegate void ItemUseDelegate(int dropId);

    /// <summary>
    /// 查詢排行榜時要呼叫的方法
    /// </summary>
    /// <param name="eventId">事件ID</param>
    /// <param name="players">玩家列表</param>
    public delegate void RankingQueryDelegate(int eventId, List<Data.RankingPlayer> players);

    /// <summary>
    /// 回報問題完成後要呼叫的方法
    /// </summary>
    /// <param name="type">回報類型</param>
    public delegate void ReportDelegate(ReportType type);

    /// <summary>
    /// IAP領取寶石完成時要呼叫的方法
    /// </summary>
    /// <param name="orderId">訂單ID</param>
    /// <param name="productId">產品ID</param>
    /// <param name="gem">增加寶石數量</param>
    public delegate void IAPClaimDelegate(string orderId, string productId, int gem);

    /// <summary>
    /// 引繼碼發行成功時要呼叫的方法
    /// </summary>
    /// <param name="orderId">引繼碼</param>
    public delegate void AccountPublishDelegate(string token);

    /// <summary>
    /// 引繼開始時要呼叫的方法
    /// </summary>
    /// <param name="publicId">好友碼</param>
    /// <param name="name">名稱</param>
    /// <param name="rank">等級</param>
    /// <param name="gem">寶石數</param>
    public delegate void AccountTransferBeginDelegate(string publicId, string name, int rank, int gem);

    /// <summary>
    /// 協力獎勵領取完成要呼叫的方法
    /// </summary>
    /// <param name="list">獲得的獎勵列表</param>
    public delegate void CooperateMissionClaimDelegate(List<Reward> list);


    /// <summary>
    /// 好友資料更新時要呼叫的方法
    /// </summary>
    /// <param name="id">玩家ID</param>
    public delegate void SocialUpdateDelegate(string id);


    /// <summary>
    /// 亂數取得玩家列表時要呼叫的方法
    /// </summary>
    /// <param name="ids">玩家列表</param>
    public delegate void SocialRandomQueryDelegate(List<string> ids);

    /// <summary>
    /// 取得友情商店購買紀錄時要呼叫的方法
    /// </summary>
    /// <param name="ids">本日已購買ID列表</param>
    public delegate void SocialPurchaseHistoryDelegate(List<int> list);

    /// <summary>
    /// 裝備資料更新時要呼叫的方法
    /// </summary>
    /// <param name="serial">裝備流水號</param>
    public delegate void EquipmentChangeDelegate(Int64 serial);

    /// <summary>
    /// 信仰領取完成要呼叫的方法
    /// </summary>
    /// <param name="list">獲得的信仰道具</param>
    public delegate void FaithClaimDelegate(Dictionary<int, ValuePair> diff);

    /// <summary>
    /// 信仰融合完成要呼叫的方法
    /// </summary>
    /// <param name="dropId">獲得的獎勵ID</param>
    public delegate void FaithEndProduceDelegate(int dropId);


    /// <summary>
    /// 啟用戰術完成要呼叫的方法
    /// </summary>
    /// <param name="tacticsId">戰術ID</param>
    public delegate void TacticsEnableDelegate(int tacticsId);

#if MYCARD
    /// <summary>
    /// MyCard開始交易時要呼叫的方法
    /// </summary>
    /// <param name="authCode">交易授權碼</param>
    /// <param name="productId">產品ID</param>
    /// <param name="sandbox">是否為測試環境</param>
    public delegate void MyCardBeginTransactionDelegate(string authCode, string productId, bool sandbox);
#endif
}
