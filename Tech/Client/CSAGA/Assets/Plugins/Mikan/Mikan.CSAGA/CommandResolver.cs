﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Mikan.CSAGA
{
    public class CommandResolver
    {
        public Serializer Serializer { get { return serializer; } }

        private Serializer serializer = new Serializer();

        private LCGCryptor cryptor = new LCGCryptor(157, 77873, 110592);

        private Service owner;

        public string Token { get; private set; }

        public CommandResolver(Service owner)
        {
            this.owner = owner;
        }

        public void ChangeToken(string token)
        {
            Token = token;
        }

        public byte[] Serialize(ICommand cmd)
        {
            var _cmd = cmd as Commands.CSAGARequest;

            var stream = new MemoryStream();

            using (var output = new BinaryWriter(stream))
            {
                var bytes = Serializer.Serialize(cmd);

                if (_cmd.RequireToken)
                {
                    output.Write((byte)0x18);
                    output.Write(Token);
                    cryptor.Encode(bytes, owner.Profile.Key);
                }
                else
                {
                    output.Write((byte)0x9C);
                    cryptor.Encode(bytes, LCGCryptor.DEFAULT_KEY);
                }

                output.Write(bytes.Length);
                output.Write(bytes);
            }
            return stream.ToArray();
        }

        public ICommand DeSerialize(Stream rawdata)
        {
            using (var reader = new BinaryReader(rawdata))
            {
                var symbol = reader.ReadByte();

                var copy = reader.ReadBytes(reader.ReadInt32());

                if (symbol == 0x18)
                    cryptor.Encode(copy, owner.Profile.Key);
                else if (symbol == 0x9C)
                    cryptor.Encode(copy, LCGCryptor.DEFAULT_KEY);

                return Serializer.DeSerialize(copy);
            }
        }
    }
}
