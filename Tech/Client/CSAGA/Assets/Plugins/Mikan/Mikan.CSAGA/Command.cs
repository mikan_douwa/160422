﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mikan.CSAGA
{
    public class CSAGACommand : Package<FieldID>, ICommand
    {
        virtual public bool IsSucc { get { return true; } }
        virtual public bool IsBackground { get { return false; } }
        virtual public bool IsActivePush { get { return false; } }
        virtual public bool AutoRetry { get { return true; } }
        virtual public bool IsRegular { get { return false; } }
        virtual public string URI { get { return Config.URI; } }
        virtual public string ErrorMessage
        {
            get { throw new NotImplementedException(); }
        }

        #region Serialize

        private bool isReady;

        /// <inheritdoc />
        protected override void OnSerialize()
        {
            if (isReady) return;
            Serialize(this);
            isReady = true;
        }

        /// <inheritdoc />
        protected override void OnDeSerialize()
        {
            DeSerialize(this);
        }

        /// <inheritdoc />
        protected override void CustomSerialize(IOutput output)
        {
            output.DynamicWrite(SyncList);
        }

        /// <inheritdoc />
        protected override void CustomDeSerialize(IInput input)
        {
            SyncList = input.ReadDynamicList<ISyncData>();
        }


        /// <inheritdoc />
        protected override int ToInt(FieldID key) { return (int)key; }

        /// <inheritdoc />
        protected override FieldID ConvertKey(int rawdata) { return (FieldID)rawdata; }

        virtual public int TransferID { get { return 0; } }

        virtual public void InjectTransferID(int id) { }

        #endregion

        #region Sync

        public List<ISyncData> SyncList;

        public void AddSyncData(ISyncData data)
        {
            if (SyncList == null) SyncList = new List<ISyncData>();
            SyncList.Add(data);
        }

        public IEnumerable<ISyncData> SelectSyncData()
        {
            return SyncList;
        }

        private SyncEquipment syncEquipment;
        public SyncEquipment EquipmentSync
        {
            get
            {
                if (syncEquipment == null)
                {
                    syncEquipment = new SyncEquipment();
                    AddSyncData(syncEquipment);
                }
                return syncEquipment;
            }
        }

        private SyncCard syncCard;
        public SyncCard CardSync
        {
            get
            {
                if (syncCard == null)
                {
                    syncCard = new SyncCard();
                    AddSyncData(syncCard);
                }
                return syncCard;
            }
        }

        private SyncPlayer syncPlayer;
        public void PlayerSync(PropertyID id, int oldValue, int newValue)
        {
            if (syncPlayer == null)
            {
                syncPlayer = new SyncPlayer { Diff = PropertyDiff.Create(id, oldValue, newValue) };
                AddSyncData(syncPlayer);
            }
            else
                syncPlayer.Diff.Add(id, oldValue, newValue);
        }

        private SyncLimitedQuest syncLimitedQuest;
        public SyncLimitedQuest LimitedQuestSync
        {
            get
            {
                if (syncLimitedQuest == null)
                {
                    syncLimitedQuest = new SyncLimitedQuest { List = new List<Internal.LimitedQuestObj>() };
                    AddSyncData(syncLimitedQuest);
                }
                return syncLimitedQuest;
            }
        }

        #endregion

        #region 序列化工具函式

        private static Dictionary<Type, List<FieldAdaptor>> adaptors;

        public static void UnRegisterAll()
        {
            adaptors = null;
        }

        /// <summary>
        /// 註冊有用到的命令用以序列化
        /// </summary>
        /// <typeparam name="U"></typeparam>
        public static void RegisterType<U>()
        {
            if (adaptors == null) adaptors = new Dictionary<Type, List<FieldAdaptor>>();

            var type = typeof(U);

            List<FieldAdaptor> list = null;

            if (!adaptors.TryGetValue(type, out list))
            {
                list = new List<FieldAdaptor>();
                adaptors.Add(type, list);
            }

            var fields = type.GetFields();

            foreach (var field in fields)
            {
                var attr = TypeUtils.GetAttribute<CommandFieldAttribute>(field);

                if (attr == null) continue;

                if (field.FieldType.Equals(typeof(int)) || field.FieldType.IsEnum)
                {
                    list.Add(new Int32FieldAdaptor { Field = field, ID = attr.ID });
                }
                else if (field.FieldType.Equals(typeof(Int64)))
                {
                    list.Add(new Int64FieldAdaptor { Field = field, ID = attr.ID });
                }
                else if (field.FieldType.Equals(typeof(string)))
                {
                    list.Add(new StringFieldAdaptor { Field = field, ID = attr.ID });
                }
                else if (field.FieldType.Equals(typeof(Boolean)))
                {
                    list.Add(new BooleanFieldAdaptor { Field = field, ID = attr.ID });
                }
                else if (field.FieldType.Equals(typeof(DateTime)))
                {
                    list.Add(new DateTimeFieldAdaptor { Field = field, ID = attr.ID });
                }
                else
                    throw new NotSupportedException(string.Format("field type not support, type={0}", field.FieldType));
            }
        }

        private static void DeSerialize(CSAGACommand cmd)
        {
            var list = adaptors[cmd.GetType()];

            foreach (var item in list) item.GetValueFrom(cmd);
        }

        private static void Serialize(CSAGACommand cmd)
        {
            var list = adaptors[cmd.GetType()];

            foreach (var item in list) item.SetValueTo(cmd);
        }

        #endregion



        
    }
}
