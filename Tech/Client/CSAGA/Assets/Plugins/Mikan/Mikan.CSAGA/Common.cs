﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mikan
{

    public class Serial : ISerializable
    {
        public static Serial Empty = new Serial();

        internal Mikan.CSAGA.Data.Card target;

        public Int64 Value;

        public bool IsEmpty { get { return Value == 0; } }

        public bool IsExternal { get { return target != null; } }

        public void Serialize(IOutput output)
        {
            output.Write(Value);
        }

        public void DeSerialize(IInput input)
        {
            Value = input.ReadInt64();
        }
        public static bool operator ==(Serial lhs, Serial rhs)
        {
            object _lhs = lhs;
            object _rhs = rhs;
            if (_lhs == null) return _rhs == null;
            if (_rhs == null) return false;
            return lhs.Value == rhs.Value && lhs.target == rhs.target;
        }

        public static bool operator !=(Serial lhs, Serial rhs)
        {
            return !(lhs == rhs);
        }

        public static Serial Create(Int64 val)
        {
            if (val == 0) return Empty;
            return new Serial { Value = val };
        }

        public override int GetHashCode()
        {
            return IsEmpty ? 0 : Value.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as Serial);
        }

        public int CompareTo(Serial obj)
        {
            var value = obj != null ? obj.Value : 0;
            return Value.CompareTo(value);
        }

        public bool Equals(Serial obj)
        {
            var value = obj != null ? obj.Value : 0;
            var _target = obj != null ? obj.target : null;
            return Value == value && target == _target;
        }

        public static List<Int64> Convert(List<Serial> src)
        {
            var list = new List<Int64>(src.Count);
            foreach (var item in src)
            {
                if (item.target != null)
                    list.Add(0);
                else
                    list.Add(item.Value);
            }
            return list;
        }

        public static Dictionary<Serial, T> CreateDictinary<T>()
        {
            return new Dictionary<Serial, T>(comparer);
        }


        private class Comparer : IEqualityComparer<Serial>
        {
            public bool Equals(Serial x, Serial y)
            {
                return x.Value == y.Value && x.target == y.target;
            }

            public int GetHashCode(Serial obj)
            {
                return obj.GetHashCode();
            }
        }

        private static Comparer comparer = new Comparer();
    }
}
namespace Mikan.CSAGA
{

    /// <summary>
    /// 商品ID
    /// </summary>
    public enum ArticleID
    {
        /// <summary>
        /// 無效ID
        /// </summary>
        Invalid = 0,

        RegularGacha = 1,

        CardSpace = 2,

        TalkPoint = 3,

        Continue = 4,

        QuestCount = 5,

        RandomShopSlot = 6,

        RandomShop = 7,

        RegularGachaDaily = 8,

        ActivityGacha = 9,

        ActivityGachaDaily = 10,

        RescuePoint = 11,

        RemoveEquip = 12,

        SocialShop = 13,

        CrusadePoint = 14,

        AddLuck = 15,

        Faith = 16,

        CommonShop = 17,

        RerollSkill = 18,

        /// <summary>
        /// 測試用
        /// </summary>
        Debug = 99,
    }

    /// <summary>
    /// 任務類型
    /// </summary>
    public enum QuestType
    {
        /// <summary>
        /// 常駐關卡
        /// </summary>
        Regular = 1,
        /// <summary>
        /// 限時觸發
        /// </summary>
        Limited = 2,
        /// <summary>
        /// 排程活動
        /// </summary>
        Schedule = 3,
        /// <summary>
        /// 外傳
        /// </summary>
        Extra = 4,
        /// <summary>
        /// 討伐關卡
        /// </summary>
        Crusade = 5,
    }

    /// <summary>
    /// 掉落物類型
    /// </summary>
    public enum DropType
    {
        /// <summary>
        /// 無
        /// </summary>
        None = 0,
        /// <summary>
        /// 水晶
        /// </summary>
        Crystal = 1,

        /// <summary>
        /// 友情點
        /// </summary>
        FP = 2,
        
        /// <summary>
        /// 卡片
        /// </summary>
        Card = 3,
        /// <summary>
        /// 道具
        /// </summary>
        Item = 4,
        /// <summary>
        /// 代幣
        /// </summary>
        Coin = 5,
        /// <summary>
        /// 寶石
        /// </summary>
        Gem = 6,

        /// <summary>
        /// 復原購買的寶石
        /// </summary>
        RestoreIAPGem = 7,

        /// <summary>
        /// 戰術
        /// </summary>
        Tactics = 8,

        Quest = 11,

        /// <summary>
        /// 購買的寶石
        /// </summary>
        IAPGem = 101,
    }
    public enum TacticsEffect
    {
        /// <summary>
        /// HP
        /// </summary>
        Hp = 1,
        /// <summary>
        /// 攻擊力
        /// </summary>
        Atk = 2,
        /// <summary>
        /// 回復力
        /// </summary>
        Rev = 3,
        /// <summary>
        /// 弱點
        /// </summary>
        Weak = 4,
        /// <summary>
        /// 反制
        /// </summary>
        Anti = 5,
        /// <summary>
        /// Chain
        /// </summary>
        Chain = 6,
        /// <summary>
        /// 經驗水晶
        /// </summary>
        Crystal = 7,
        /// <summary>
        /// Exp
        /// </summary>
        RankExp = 8,
        /// <summary>
        /// 掉寶率
        /// </summary>
        Drop = 9,

    }
    /// <summary>
    /// 互動觸發條件
    /// </summary>
    public enum InteractiveTrigger
    {
        None = 0,
        /// <summary>
        /// 一般對話
        /// </summary>
        Common = 1,
        /// <summary>
        /// 選項
        /// </summary>
        Choice = 2,
        /// <summary>
        /// 任務結算
        /// </summary>
        Settlement = 3,
        /// <summary>
        /// 任務
        /// </summary>
        Quest = 5,
    }

    public enum EventScheduleType
    {
        None = 0,
        Quest = 1,
        Gacha = 2,
        Rescue = 3,

        Crusade = 5,

        PVP = 6,
    }

    public enum RescueState
    {
        None,
        Succ,
        Fail,
    }

    public enum EventResult
    {
        None = 0,
        Kizuna = 1,
        Gift = 2,
        Quest = 3,
    }

    /// <summary>
    /// 道具類型
    /// </summary>
    public enum ItemType
    {
        /// <summary>
        /// 素材
        /// </summary>
        Material = 1,
        /// <summary>
        /// 禮物
        /// </summary>
        Gift = 2,
        /// <summary>
        /// 裝備
        /// </summary>
        Equipment = 3,

        Consumable = 100,
    }

    /// <summary>
    /// 道具特性
    /// </summary>
    public enum ItemEffect
    {
        /// <summary>
        /// 無特性
        /// </summary>
        None = 0,
        /// <summary>
        /// 禮物效果加成
        /// </summary>
        Gift = 1,
        /// <summary>
        /// 加速
        /// </summary>
        Boost = 2,
        /// <summary>
        /// 獲得被動技能
        /// </summary>
        Skill = 3,

        /// <summary>
        /// 加速2
        /// </summary>
        Boost2 = 4,

		Resist = 5,

		CounterEnhance = 6,

		BuffExtend = 7,

        /// <summary>
        /// 可使用道具
        /// </summary>
        Usable = 101,
    }


    public enum TrophyType
    {
        Progressive = 1,
        Immediately = 2,
        Daily = 3,
        DailyTrigger = 4,
        DailyCounter = 5,
        EventPoint = 6,
        EventRanking = 7,
        SerialNumber = 8,
        DailyLogin = 9,
        CreatePlayer = 10,
        Challenge = 11,
    }

    public enum GachaListTag
    {
        /// <summary>
        /// 轉蛋
        /// </summary>
        RegularGacha = 1,
        /// <summary>
        /// 隨機商店
        /// </summary>
        Random = 2,
        /// <summary>
        /// 固定商品
        /// </summary>
        Static = 3,
        /// <summary>
        /// 活動轉蛋
        /// </summary>
        ActivityGacha = 4,
        /// <summary>
        /// 友情商店
        /// </summary>
        SocialShop = 5,
        /// <summary>
        /// 一般商店
        /// </summary>
        CommonShop = 6,
    }

    public enum SetupFunction
    {
        None = 0,
        System = 1,
        Gallery = 2,
        Item = 3,
        Equipment = 4,
        Name = 5,
        Tutorial = 6,
        Memory = 7,
        HomePage = 8,
        Link = 9,
        Reset = 10,
        Repair = 11,
        Publish = 12,
        Banner = 1001,
    }

    /// <summary>
    /// 協力任務課題類型
    /// </summary>
    public enum MissionType
    {
        None = 0,
        Quest1 = 1,
        Quest2 = 2,
        Item = 3,
        Gift = 4,
        Talk = 5,
    }

    public class MissionRanking
    {
        public string Name;
        public int CardID;
        public int Point;
    }

    public enum ReportType
    {
        Error = 1,
        Debug = 2,
    }

    public enum Relation
    {
        None = 0,
        Follow,
        Fans,
        CrossFollow,
    }

    public enum PropertyID : byte
    {
        Invalid,
        Crystal = 1,
        Coin,
        Gem,
        Exp,
        Rare,
        Kizuna,
        TPConsumed,
        Complex,
        ID,
        Slot,
        Hp,
        Atk,
        Rev,
        Chg,
        EffectID,
        Awaken,
        Plus,
        IAPGem,
        Luck,
        FP,
        Discover,
        Counter,
        CounterType,
        BPConsumed,
        BPUpdateTime,
    }

    public class PropertyCollection : ISerializable
    {
        private Dictionary<PropertyID, int> list = new Dictionary<PropertyID, int>();

        public int this[PropertyID id]
        {
            get
            {
                var val = 0;
                if (list.TryGetValue(id, out val)) return val;
                return 0;
            }

            set
            {
                list[id] = value;
            }
        }

        public void Serialize(IOutput output)
        {
            output.Write(list.Count);
            foreach (var item in list)
            {
                output.Write((byte)item.Key);
                output.Write(item.Value);
            }
        }

        public void DeSerialize(IInput input)
        {
            var count = input.ReadInt32();
            for (int i = 0; i < count; ++i)
            {
                var type = (PropertyID)input.ReadByte();
                var value = input.ReadInt32();
                list.Add(type, value);
            }
        }
    }

    public class DiffBase : ISerializable
    {
        protected Dictionary<int, ValuePair> list = new Dictionary<int, ValuePair>();

        public bool IsEmpty { get { return list.Count == 0; } }

        public KeyValuePair<int, ValuePair> First
        {
            get
            {
                foreach (var item in list) return item;
                return new KeyValuePair<int, ValuePair>();
            }
        }

        public ValuePair this[int id]
        {
            get
            {
                return DictionaryUtils.SafeGetValueNullable(list, id);
            }
        }

        public bool Contains(int id)
        {
            return list.ContainsKey((int)id);
        }

        public void Add(int id, int oldValue, int newValue)
        {
            list.Add(id, ValuePair.Create(oldValue, newValue));
        }

       
        virtual public void Serialize(IOutput output)
        {
            output.Write(list);
        }

        virtual public void DeSerialize(IInput input)
        {
            list = input.ReadInt32Dictionary<ValuePair>();
        }

        public static Diff Create(int id, int oldValue, int newValue)
        {
            var diff = new Diff();
            diff.Add(id, oldValue, newValue);
            return diff;
        }

        public static T Create<T>(int id, int oldValue, int newValue)
            where T : DiffBase, new()
        {
            var diff = new T();
            diff.Add(id, oldValue, newValue);
            return diff;
        }
    }

    public class Diff : DiffBase
    {
        public IEnumerable<KeyValuePair<int, ValuePair>> Select()
        {
            foreach (var item in list) yield return new KeyValuePair<int, ValuePair>(item.Key, item.Value);
        }

    }

    public class PropertyDiff : DiffBase
    {
        public bool Contains(PropertyID id)
        {
            return Contains((int)id);
        }

        public ValuePair this[PropertyID id]
        {
            get
            {
                return this[(int)id];
            }
        }

        public void Add(PropertyID id, int oldValue, int newValue)
        {
            Add((int)id, oldValue, newValue);
        }

        public IEnumerable<KeyValuePair<PropertyID, ValuePair>> Select()
        {
            foreach (var item in list) yield return new KeyValuePair<PropertyID, ValuePair>((PropertyID)item.Key, item.Value);
        }

        public static PropertyDiff Create(PropertyID id, int oldValue, int newValue)
        {
            return Create<PropertyDiff>((int)id, oldValue, newValue);
        }
    }

    #region Reward
    /// <summary>
    /// 獎勵來源
    /// </summary>
    public enum RewardSource : byte
    {
        /// <summary>
        /// 無效值
        /// </summary>
        Invalid = 0,
        /// <summary>
        /// 任務戰利品
        /// </summary>
        Quest,
        /// <summary>
        /// 卡片事件
        /// </summary>
        CardEvent,
        /// <summary>
        /// 贈禮
        /// </summary>
        Gift,
        /// <summary>
        /// 成就
        /// </summary>
        Trophy,
        /// <summary>
        /// 每日活躍
        /// </summary>
        DailyActive,
        /// <summary>
        /// 隨機商店
        /// </summary>
        RandomShop,
        /// <summary>
        /// IAP(寶石)
        /// </summary>
        IAP,
        /// <summary>
        /// 道具禮包
        /// </summary>
        Item,
        /// <summary>
        /// 排名獎勵
        /// </summary>
        EventRanking,
        /// <summary>
        /// 序號
        /// </summary>
        SerialNumber,
        /// <summary>
        /// 每月首儲回饋
        /// </summary>
        IAPBonus,
        /// <summary>
        /// 協力任務
        /// </summary>
        CoMission,
        /// <summary>
        /// MyCard儲值優惠
        /// </summary>
        MyCardBonus,
        /// <summary>
        /// 友情商店
        /// </summary>
        SocialShop,
        /// <summary>
        /// 轉蛋回饋
        /// </summary>
        GachaFeedback,
        /// <summary>
        /// 信仰
        /// </summary>
        Faith,
        /// <summary>
        /// 一般商店
        /// </summary>
        CommonShop,
        /// <summary>
        /// 測試用
        /// </summary>
        Debug = 99,
    }

    public class Reward : ISerializable
    {
        public DropType Type;
        public RewardSource Source;
        public int Value { get { return Value1; } }
        public int Value1;
        public int Value2;

        public void Serialize(IOutput output)
        {
            output.Write((int)Type);
            output.Write((byte)Source);
            output.Write(Value1);
            output.Write(Value2);
        }

        public void DeSerialize(IInput input)
        {
            Type = (DropType)input.ReadInt32();
            Source = (RewardSource)input.ReadByte();
            Value1 = input.ReadInt32();
            Value2 = input.ReadInt32();
        }
    }

    #endregion

    #region Trophy
    public enum TrophyIDSet
    {
        PLAYER_LV = 1,
        GALLERY_NUM = 2,
        KIZUNA_LV1 = 3,
        KIZUNA_LV2 = 4,
        AWAKEN_RARITY = 5,
        AWAKEN_ABILITY = 6,

        KIZUNA_LV3 = 11,
        KIZUNA_LV4 = 12,
        KIZUNA_LV5 = 13,

        QUEST_CLEAR = 10001,

        LOGIN_COUNTER = 30001,
        EVENT_POINT = 40001,
        EVENT_RANKING = 40101,

        CREATE_PLAYER = 800001,
    }

    public static class TrophyConditionType
    {
        public const string PLAYER_LV = "PLAYER_LV";
        public const string GALLERY_NUM = "GALLERY_NUM";
        public const string KIZUNA_LV1 = "KIZUNA_LV1";
        public const string KIZUNA_LV2 = "KIZUNA_LV2";
        public const string KIZUNA_LV3 = "KIZUNA_LV3";
        public const string KIZUNA_LV4 = "KIZUNA_LV4";
        public const string KIZUNA_LV5 = "KIZUNA_LV5";
        public const string AWAKEN_RARITY = "AWAKEN_RARITY";
        public const string AWAKEN_ABILITY = "AWAKEN_ABILITY";
        public const string QUEST_CLEAR = "QUEST_CLEAR";

        public const string DAILY_DISCOVER = "DAILY_DISCOVER";
        public const string DAILY_EVENT = "DAILY_EVENT";
        public const string DAILY_COMMU = "DAILY_COMMU";
        public const string DAILY_EQUIP = "DAILY_EQUIP";
        public const string DAILY_GIFT = "DAILY_GIFT";
        public const string DAILY_SUMMON = "DAILY_SUMMON";

        public const string DAILY_FAVORITE = "DAILY_FAVORITE";

        public const string LOGIN_COUNTER = "LOGIN_COUNTER";

        public const string DAILY_LOGIN = "DAILY_LOGIN";

        public const string DAILY_FP_FANS = "DAILY_FP_FANS";

        public const string EVENT_POINT = "EVENT_POINT";
        public const string EVENT_RANKING = "EVENT_RANKING";
        public const string CREATE_PLAYER = "CREATE_PLAYER";
        public const string CHALLENGE = "CHALLENGE";

    }
    #endregion

    public static class ComplexIndex
    {
        public const int PlayerAvailable = 0;
        public const int PlayerFirstGacha = 1;
        //public const int DUMMY = 2; // 保留
        public const int PlayerGachaRefund = 3;

        public const int GachaBoost = 8;
        public const int GachaBoostLen = 8;

        public const int CardIsLocked = 0;
        
        /// <summary>
        /// 卡片事件(4~19) - 最多就16個事件,再多就要額外處理了
        /// </summary>
        public const int CardEvent = 4;

        /// <summary>
        /// Rare同時也是第1個覺醒,故與覺醒重疊
        /// </summary>
        public const int CardRare = 20;
        public const int CardRareLen = 4;
        
        /// <summary>
        /// 共4個覺醒20~35
        /// </summary>
        public const int CardAwaken = 20;
        public const int CardAwakenLen = 4;

        /// <summary>
        /// 共3種加蛋36~56
        /// </summary>
        public const int CardPlus = 36;
        public const int CardPlusLen = 7;
    }
}
