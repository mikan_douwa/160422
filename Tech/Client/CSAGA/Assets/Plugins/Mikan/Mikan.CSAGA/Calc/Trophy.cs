﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mikan.CSAGA.Calc
{
    public interface ITrophySourceProvider
    {
        int PlayerExp { get; }
        DateTime AccountCreateTime { get; }
        int GalleryCount { get; }
        int AwakenRarityCount { get; }
        int AwakenAbilityCount { get; }

        DateTime CurrentTime { get; }

        IEnumerable<Internal.CardObj> SelectCard();

        IEnumerable<int> SelectCardKizuna();

        Internal.TrophyObj GetTrophy(TrophyIDSet idset);
        bool IsTrophyFinished(int id);
        Internal.QuestStateObj GetQuest(int id);
    }

    public class TrophyCache
    {
        public TrophyIDSet IDSet;
        public int ID;
        public int Progress;
        public TrophyType Type = TrophyType.Progressive;
        public bool IsFinish;
    }

    public class Trophy : Calculator
    {
        private static ConditionChecker PlayerLV;
        private static ConditionChecker GalleryNum;
        private static ConditionChecker Kizuna;
        private static ConditionChecker AwakenRarity;
        private static ConditionChecker AwakenAbility;

        private static ConditionChecker QuestClear;
        private static ConditionChecker QuestChallenge;

        private static ConditionChecker DailyDiscorver;
        private static ConditionChecker DailyEvent;
        private static ConditionChecker DailyCommu;
        private static ConditionChecker DailyEquip;
        private static ConditionChecker DailyGift;
        private static ConditionChecker DailySummon;

        private static ConditionChecker DailyFavorite;

        private static ConditionChecker DailyCounter;

        private static ConditionChecker DailyLogin;

        private static ConditionChecker DailyFP;

        private static ConditionChecker EventPoint;
        private static ConditionChecker EventRanking;

        private static ConditionChecker CreatePlayer;

        private static List<ConditionChecker> tmpList;
        
        internal static void Setup()
        {
            tmpList = new List<ConditionChecker>();

            PlayerLV = null;
            GalleryNum = null;
            Kizuna = null;
            AwakenRarity = null;
            AwakenAbility = null;
            QuestClear = null;

            DailyDiscorver = null;
            DailyEvent = null;
            DailyCommu = null;
            DailyEquip = null;
            DailyGift = null;
            DailySummon = null;

            DailyFavorite = null;

            DailyCounter = null;

            DailyLogin = null;

            DailyFP = null;

            EventPoint = null;
            EventRanking = null;

            CreatePlayer = null;

            QuestChallenge = null;

            foreach (var item in Tables.Trophy.Select())
            {
                switch (item.s_CONDITION_TYPE)
                {
                    #region 挑戰
                    case TrophyConditionType.PLAYER_LV:
                        PlayerLV = AddCondition<SimpleTrophy>(PlayerLV, item);
                        break;
                    case TrophyConditionType.GALLERY_NUM:
                        GalleryNum = AddCondition<SimpleTrophy>(GalleryNum, item);
                        break;
                    case TrophyConditionType.KIZUNA_LV1:
                    case TrophyConditionType.KIZUNA_LV2:
                    case TrophyConditionType.KIZUNA_LV3:
                    case TrophyConditionType.KIZUNA_LV4:
                    case TrophyConditionType.KIZUNA_LV5:
                        Kizuna = AddCondition<KizunaTrophy>(Kizuna, item);
                        break;
                    case TrophyConditionType.AWAKEN_RARITY:
                        AwakenRarity = AddCondition<SimpleTrophy>(AwakenRarity, item);
                        break;
                    case TrophyConditionType.AWAKEN_ABILITY:
                        AwakenAbility = AddCondition<SimpleTrophy>(AwakenAbility, item);
                        break;
                    case TrophyConditionType.EVENT_POINT:
                        EventPoint = AddCondition<SimpleTrophy>(EventPoint, item);
                        break;
                    case TrophyConditionType.EVENT_RANKING:
                        //EventRanking = AddCondition<SimpleTrophy>(EventPoint, item);
                        break;
                    #endregion

                    #region 即時獎勵
                    case TrophyConditionType.QUEST_CLEAR:
                        QuestClear = AddCondition<QuestClearTrophy>(QuestClear, item);
                        break;
                    case TrophyConditionType.CREATE_PLAYER:
                        CreatePlayer = AddCondition<CreatePlayerTrophy>(CreatePlayer, item);
                        break;
                    case TrophyConditionType.CHALLENGE:
                        QuestChallenge = AddCondition<QuestChallengeTrophy>(QuestChallenge, item);
                        break;
                    #endregion
                    #region 每日重置
                    case TrophyConditionType.DAILY_DISCOVER:
                        DailyDiscorver = AddCondition<SimpleTrophy>(DailyDiscorver, item);
                        break;
                    case TrophyConditionType.DAILY_EVENT:
                        DailyEvent = AddCondition<SimpleTrophy>(DailyEvent, item);
                        break;
                    case TrophyConditionType.DAILY_COMMU:
                        DailyCommu = AddCondition<SimpleTrophy>(DailyCommu, item);
                        break;
                    case TrophyConditionType.DAILY_EQUIP:
                        DailyEquip = AddCondition<SimpleTrophy>(DailyEquip, item);
                        break;
                    case TrophyConditionType.DAILY_GIFT:
                        DailyGift = AddCondition<SimpleTrophy>(DailyGift, item);
                        break;
                    case TrophyConditionType.DAILY_SUMMON:
                        DailySummon = AddCondition<SimpleTrophy>(DailySummon, item);
                        break;

                    case TrophyConditionType.DAILY_FAVORITE:
                        DailyFavorite = AddCondition<SimpleTrophy>(DailyFavorite, item);
                        break;
                    case TrophyConditionType.LOGIN_COUNTER:
                        DailyCounter = AddCondition<SimpleTrophy>(DailyCounter, item);
                        break;
                    case TrophyConditionType.DAILY_LOGIN:
                        DailyLogin = AddCondition<DateTrophy>(DailyLogin, item);
                        break;
                    case TrophyConditionType.DAILY_FP_FANS:
                        DailyFP = AddCondition<SimpleTrophy>(DailyFP, item);
                        break;
                    #endregion
                    default:
                        break;
                }
            }

            foreach (var item in tmpList)
            {
                foreach (var group in item.Groups) group.Value.Merge();
            }

            tmpList = null;
        }
        private static ConditionChecker AddCondition<T>(ConditionChecker origin, ConstData.Trophy item)
            where T : ConditionChecker, new()
        {
            var inst = origin;
            if (inst == null)
            {
                inst = new T();
                // 連續的成就:一般(1),每日,活動積分
                if(item.n_LEGACY_TYPE == TrophyType.Progressive || item.n_LEGACY_TYPE == TrophyType.DailyCounter || item.n_LEGACY_TYPE == TrophyType.EventPoint || item.n_LEGACY_TYPE == TrophyType.Challenge) tmpList.Add(inst);
                inst.IsDaily = (item.n_LEGACY_TYPE == TrophyType.Daily || item.n_LEGACY_TYPE == TrophyType.DailyTrigger || item.n_LEGACY_TYPE == TrophyType.DailyCounter);
            }

            var group = DictionaryUtils.SafeGetValue(inst.Groups, item.n_IDSET);

            group.Add(item);

            return inst;
        }

        public static Trophy Generate(ITrophySourceProvider provider)
        {
            var inst = new Trophy();
            inst.provider = provider;
            return inst;
        }
        public static Data.Gift ToGift(int trophyId, RewardSource source, params object[] args)
        {
            var data = Tables.Trophy[trophyId];

            if (data == null) KernelService.Logger.Fatal("trophy not exists, id = {0}", trophyId);

            return ToGift(data, source, args);
        }
        public static Data.Gift ToGift(ConstData.Trophy data, RewardSource source, params object[] args)
        {
            var drop = Drop.Get(data.n_BONUS_LINK).First;

            var gift = Drop.ToGift(drop);

            // 寶石就將成就ID記進Value2, 以便日後追蹤
            if(gift.Type == DropType.Gem)
                gift.Value2 = data.n_ID;

            var text = Tables.TrophyText[data.n_ID];

            if (text != null)
                gift.Description = ConstData.Tables.Format(text.s_NAME, args);
            else
            {
                gift.Description = string.Format("TROPHY({0})", data.n_ID);
                KernelService.Logger.Fatal("trophy_text not exists, id = {0}", data.n_ID);
            }

            gift.ExpireTime = DateTime.MaxValue;

            return gift;
        }

        #region instance

        public event Action<TrophyCache> OnTrophyUpdate;

        private ITrophySourceProvider provider;

        private ICompareList kizunaComparision;
        private ICompareList questComparision;

        private int nextPlayerExp;

        private int originGalleryCount;
        private int originAwakenRarityCount;
        private int originAwakenAbilityCount;

        public void Init()
        {
            kizunaComparision = new KizunaList { Provider = provider };
            questComparision = new QuestList { Provider = provider };

            originAwakenRarityCount = provider.AwakenRarityCount;
            originAwakenAbilityCount = provider.AwakenAbilityCount;

            OnPlayerExpChange(provider.PlayerExp);
            OnGalleryCountChange(provider.GalleryCount);
            OnKizunaChange();

            Check(AwakenRarity, originAwakenRarityCount);
            Check(AwakenAbility, originAwakenAbilityCount);

            OnQuestClear();

            Check(DailyFavorite, 1);

            Check(DailyFP, 1);

            Check(DailyLogin, 1);

            var dailyCounter = provider.GetTrophy(TrophyIDSet.LOGIN_COUNTER);

            if (dailyCounter == null || dailyCounter.UpdateTime.Date != provider.CurrentTime.Date)
                Check(DailyCounter, 1);
        }
        
        public void OnPlayerExpChange(int newValue)
        {
            if (newValue < nextPlayerExp) return;

            var newPlayerLV = PlayerUtils.CalcLV(newValue);

            var next = Tables.Player[newPlayerLV + 1];

            if (next != null) nextPlayerExp = next.n_TOTALEXP;

            Check(PlayerLV, newPlayerLV);
            Check(CreatePlayer, newPlayerLV);
        }

        public void OnGalleryCountChange(int count)
        {
            if (count <= originGalleryCount) return;

            originGalleryCount = count;

            Check(GalleryNum, originGalleryCount);
        }

        public void OnKizunaChange()
        {
            Check(Kizuna, kizunaComparision);
        }

        public void OnAwakenRarity()
        {
            Check(AwakenRarity, ++originAwakenRarityCount);
        }

        public void OnAwakenAbility()
        {
            Check(AwakenAbility, ++originAwakenAbilityCount);
        }

        public void OnQuestClear()
        {
            Check(QuestClear, questComparision);
        }

        public void OnQuestClear(int questId, int challengeCount)
        {
            OnQuestClear();

            var data = Tables.QuestDesign[questId];
            if (data.n_QUEST_TYPE == QuestType.Regular)
                Check(DailyDiscorver, 1);
            else
                Check(DailyEvent, 1);

            if (challengeCount > 0)
                Check(QuestChallenge, data.n_MAIN_QUEST, challengeCount);
        }

        public void OnTalk()
        {
            Check(DailyCommu, 1);
        }

        public void OnEquip()
        {
            Check(DailyEquip, 1);
        }

        public void OnGift()
        {
            Check(DailyGift, 1);
        }

        public void OnSummon()
        {
            Check(DailySummon, 1);
        }

        public void OnEventPoint(int eventId, int point)
        {
            var schedule = Tables.EventSchedule[eventId];
            if (schedule == null) return;

            var value = new CompareSingle(point);
            value.IDSet = schedule.n_POINT;
            Check(EventPoint, value);
        }

        private void Check(ConditionChecker checker, int value)
        {
            if (checker != null) Trigger(checker.Check(provider, value));
        }

        private void Check(ConditionChecker checker, params int[] values)
        {
            Check(checker, new List<int>(values));
        }

        private void Check(ConditionChecker checker, IList<int> list)
        {
            if (checker != null) Trigger(checker.Check(provider, list));
        }

        private void Check(ConditionChecker checker, ICompareList value)
        {
            if (checker != null) Trigger(checker.Check(provider, value));
        }

        private void Trigger(IEnumerable<TrophyCache> caches)
        {
            foreach (var item in caches)
            {
                if (OnTrophyUpdate != null) OnTrophyUpdate(item);
            }
        }

        #endregion

        #region data classes

        class TrophyData
        {
            public TrophyData Next;
            public ConstData.Trophy Rawdata;
            public TrophyIDSet IDSet { get { return Rawdata.n_IDSET; } }
            public int ID { get { return Rawdata.n_ID; } }
            public bool IsFirst { get { return Rawdata.n_PREPOSITION == 0; } }
        }

        class TrophyGroup
        {
            public TrophyData First;
            public Dictionary<int, TrophyData> List = new Dictionary<int, TrophyData>();

            public TrophyData Get(int current)
            {
                if (current == 0) return null;
                return List[current];
            }

            public void Add(ConstData.Trophy data)
            {
                var obj = new TrophyData { Rawdata = data };
                List.Add(data.n_ID, obj);
                if (First == null) First = obj;
            }

            public void Merge()
            {
                foreach (var item in List)
                {
                    if (item.Value.Rawdata.n_PREPOSITION == 0) First = item.Value;

                    foreach (var next in List)
                    {
                        if (next.Value.Rawdata.n_PREPOSITION != item.Key) continue;
                        item.Value.Next = next.Value;
                        break;
                    }
                }
            }
        }

        #endregion
        #region compare classes
        interface ICompareList
        {
            IEnumerable<int> Select();
            int this[int index] { get; }
            int First { get; }
            bool Contains(int val);
            int IDSet { get; }
        }

        class CompareSingle : ICompareList
        {
            private int val;
            public CompareSingle(int val)
            {
                this.val = val;
            }

            public int First { get { return val; } }

            public IEnumerable<int> Select() { yield return val; }

            public bool Contains(int val) { return this.val == val; }

            public int IDSet { get; set; }


            public int this[int index]
            {
                get { throw new NotImplementedException(); }
            }
        }

        class CompareList : ICompareList
        {
            private IList<int> list;
            public CompareList(IList<int> list)
            {
                this.list = list;
            }

            public int First { get { return list[0]; } }

            public IEnumerable<int> Select()
            {
                foreach (var item in list) yield return item;
            }

            public bool Contains(int val) { return list.Contains(val); }

            public int IDSet { get; set; }


            public int this[int index]
            {
                get { return list[index]; }
            }
        }

        class KizunaList : ICompareList
        {
            public ITrophySourceProvider Provider;

            public IEnumerable<int> Select()
            {
                foreach (var item in Provider.SelectCardKizuna()) yield return item;
            }

            #region reserved

            public bool Contains(int val)
            {
                throw new NotImplementedException();
            }

            public int First
            {
                get { throw new NotImplementedException(); }
            }

            #endregion

            public int IDSet { get; set; }


            public int this[int index]
            {
                get { throw new NotImplementedException(); }
            }
        }

        class QuestList : ICompareList
        {
            public ITrophySourceProvider Provider;

            public bool Contains(int val)
            {
                var quest = Provider.GetQuest(val);
                return (quest != null && quest.IsCleared);
            }

            #region reserved
            public IEnumerable<int> Select()
            {
                throw new NotImplementedException();
            }
            public int First
            {
                get { throw new NotImplementedException(); }
            }

            #endregion

            public int IDSet { get; set; }


            public int this[int index]
            {
                get { throw new NotImplementedException(); }
            }
        }

        #endregion

        #region contidion
        abstract class ConditionChecker
        {
            protected static ConstData.Tables Tables { get { return ConstData.Tables.Instance; } }

            virtual public bool IsSequential { get { return true; } }

            virtual public bool IgnoreZero { get { return false; } }

            public bool IsDaily = false;

            public Dictionary<TrophyIDSet, TrophyGroup> Groups = new Dictionary<TrophyIDSet, TrophyGroup>();

            private TrophyCache CreateState(TrophyIDSet id, TrophyData current, int progress, bool isFinish)
            {
                var state = new TrophyCache();
                state.IDSet = id;
                state.ID = current != null ? current.ID : 0;
                state.Progress = progress;
                state.IsFinish = isFinish;
                state.Type = Tables.Trophy.SelectFirst(o => { return o.n_IDSET == id; }).n_LEGACY_TYPE;
                return state;
            }

            public IEnumerable<TrophyCache> Check(ITrophySourceProvider provider, int val)
            {
                return Check(provider, new CompareSingle(val));
            }

            public IEnumerable<TrophyCache> Check(ITrophySourceProvider provider, IList<int> list)
            {
                return Check(provider, new CompareList(list));
            }

            public IEnumerable<TrophyCache> Check(ITrophySourceProvider provider, ICompareList values)
            {
                foreach (var item in Groups)
                {
                    if (values.IDSet > 0 && values.IDSet != (int)item.Key) continue;

                    if (IsSequential)
                    {
                        var record = provider.GetTrophy(item.Key);

                        var current = record == null ? null : item.Value.Get(record.Current);

                        var next = current != null ? current.Next : item.Value.First;

                        int originProgress = record != null ? record.Progress : 0;

                        var _values = IsDaily ? new CompareSingle(originProgress + values.First) : values;

                        while (next != null)
                        {
                            int newProgress = 0;

                            if (Check(provider, next,_values, out newProgress))
                            {
                                yield return CreateState(item.Key, next, newProgress, true);

                                current = next;

                                next = current.Next;

                                originProgress = newProgress;
                            }
                            else
                            {
                                if (newProgress != originProgress && (newProgress != 0 || !IgnoreZero))
                                    yield return CreateState(item.Key, current, newProgress, false);

                                break;
                            }
                        }
                    }
                    else
                    {

                        var tmp = 0;

                        foreach (var trophy in item.Value.List)
                        {
                            if (provider.IsTrophyFinished(trophy.Value.ID)) continue;

                            if (Check(provider, trophy.Value, values, out tmp))
                            {
                                yield return CreateState(trophy.Value.IDSet, trophy.Value, 0, true);
                            }
                        }
                    }
                }
            }
            abstract protected bool Check(ITrophySourceProvider provider, TrophyData current, ICompareList values, out int progress);
        }

        class SimpleTrophy : ConditionChecker
        {
            protected override bool Check(ITrophySourceProvider provider, TrophyData current, ICompareList values, out int progress)
            {
                progress = values.First;

                return progress >= current.Rawdata.n_EFFECT[0];
            }
        }

        class KizunaTrophy : ConditionChecker
        {
            protected override bool Check(ITrophySourceProvider provider, TrophyData current, ICompareList values, out int progress)
            {
                progress = 0;
                var count = current.Rawdata.n_EFFECT[0];
                var val = current.Rawdata.n_EFFECT[1];

                foreach (var item in values.Select())
                {
                    if (item >= val) ++progress;
                }

                return progress >= count;
            }
        }

        class QuestClearTrophy : ConditionChecker
        {
            public override bool IsSequential { get { return false; } }
            protected override bool Check(ITrophySourceProvider provider, TrophyData current, ICompareList values, out int progress)
            {
                progress = 0;

                foreach (var item in current.Rawdata.n_EFFECT)
                {
                    if (item != 0 && !values.Contains(item)) return false;
                }

                progress = 1;

                return true;
            }
        }

        class DateTrophy : ConditionChecker
        {
            protected override bool Check(ITrophySourceProvider provider, TrophyData current, ICompareList values, out int progress)
            {
                progress = values.First;

                var beginTime = Calc.Quest.ParseBeginTime(current.Rawdata.n_EFFECT[0], 0);
                var endTime = Calc.Quest.ParseBeginTime(current.Rawdata.n_EFFECT[1], 0);

                var currentTime = provider.CurrentTime;

                return currentTime >= beginTime && currentTime < endTime.AddDays(1);
            }

        }

        class CreatePlayerTrophy : ConditionChecker
        {
            public override bool IsSequential { get { return false; } }
            protected override bool Check(ITrophySourceProvider provider, TrophyData current, ICompareList values, out int progress)
            {
                progress = 0;

                var overdue = Calc.Quest.ParseBeginTime(current.Rawdata.n_EFFECT[2], 0);

                if (provider.CurrentTime >= overdue.AddDays(1)) return false;

                var createTime = provider.AccountCreateTime;

                var beginTime = Calc.Quest.ParseBeginTime(current.Rawdata.n_EFFECT[0], 0);
                var endTime = Calc.Quest.ParseBeginTime(current.Rawdata.n_EFFECT[1], 0);

                if (createTime < beginTime || createTime >= endTime.AddDays(1)) return false;

                if (values.First < current.Rawdata.n_EFFECT[3]) return false;
   
                progress = 1;

                return true;
            }
        }

        class QuestChallengeTrophy : ConditionChecker
        {
            public override bool IgnoreZero { get { return true; } }
            protected override bool Check(ITrophySourceProvider provider, TrophyData current, ICompareList values, out int progress)
            {
                progress = 0;

                var id = current.Rawdata.n_EFFECT[0];

                if (values[0] != id) return false;

                progress = values[1];

                var count = current.Rawdata.n_EFFECT[1];
                    
                return values[1] >= count;
            }
        }
        #endregion
    }

    
}
