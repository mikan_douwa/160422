﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mikan.CSAGA.Calc
{
    /// <summary>
    /// 任務產生器
    /// </summary>
    public class Quest : Calculator
    {
        private static Dictionary<int, QuestDesign> quests;

        private static Dictionary<int, Dictionary<int, ConstData.Interactive>> events;

        private static List<ValuePair> infinityBonusProvider;

        /// <summary>
        /// 產生任務資料
        /// </summary>
        /// <param name="questId">任務ID</param>
        /// <returns>回傳任務實體</returns>
        public static QuestInstance Generate(int questId, int luck)
        {
            QuestDesign quest = null;

            if (!quests.TryGetValue(questId, out quest)) return null;

            var inst = new QuestInstance();

            inst.QuestID = questId;

            inst.Point = quest.Rawdata.n_POINT;

            if(quest.Rawdata.n_QUEST_TYPE != QuestType.Crusade)
                inst.Discover = quest.Rawdata.n_DISCOVER;

            inst.Events = new List<int>(quest.Rounds);

            for (int i = 0; i < quest.Rounds; ++i)
            {
                var data = quest.SelectEvent(i);
                inst.Events.Add(data.n_ID);
            }

            inst.Drop = quest.CalcDrop();

            if (luck > 0 && MathUtils.Random.Next(100) <= luck) inst.LuckDrop = quest.CalcDrop();

            return inst;
        }

        public static int TriggerLimited(int questId)
        {
            QuestDesign quest = null;

            if (!quests.TryGetValue(questId, out quest)) return 0;

            return quest.TriggerLimited();
        }

        public static int GetEvent(int questId, int round)
        {
            var list = DictionaryUtils.SafeGetValueNullable(events, questId);
            if (list == null) return 0;

            var ev = DictionaryUtils.SafeGetValueNullable(list, round);
            return ev != null ? ev.n_ID : 0;
        }

        public static IEnumerable<ConstData.Interactive> SelectEvents(int questId)
        {
            var list = DictionaryUtils.SafeGetValueNullable(events, questId);
            if (list == null) yield break;
            foreach (var item in list) yield return item.Value;
        }

        public static int CalcCrusadeCount(int count, DateTime updateTime, DateTime beginTime, DateTime currentTime)
        {
            var origin = count;

            if (updateTime < beginTime)
            {
                origin = Tables.CrusadeInital * Tables.CrusadeScale;
                updateTime = beginTime;
            }

            var minutes = currentTime.Subtract(updateTime).TotalMinutes;

            var add = (int)Math.Floor((minutes / (float)Tables.CrusadeAdd) * Tables.CrusadeScale);

            return Math.Min(origin + add, Tables.CrusadeMax * Tables.CrusadeScale);
        }

        internal static void Setup()
        {
            quests = new Dictionary<int, QuestDesign>(Tables.QuestDesign.Count);

            foreach (var item in Tables.QuestDesign.Select())
            {
                var quest = new QuestDesign { Rawdata = item };

                quest.Groups = new Dictionary<int, QuestEventGroup>();
                quest.DropGroup = Drop.Get(item.n_DROP);

                for (int i = 0; i < quest.Rounds; ++i)
                {
                    var group = new QuestEventGroup();

                    var flag = (int)Math.Pow(2, i);

                    foreach (var ev in Tables.QuestEvent.Select())
                    {
                        if (ev.n_GROUP != item.n_EVENT || ((ev.n_EVENT_STEP & flag) == 0)) continue;
                        group.Add(ev);
                    }

                    quest.Groups.Add(i, group);
                }

                quests.Add(quest.ID, quest);
            }

            events = new Dictionary<int, Dictionary<int, ConstData.Interactive>>();
            foreach (var item in Tables.Interactive.Select(o => { return o.n_TRIGGER == InteractiveTrigger.Quest; }))
            {
                var list = DictionaryUtils.SafeGetValue(events, item.n_QUEST);
                list[item.n_TURN] = item;
            }

            infinityBonusProvider = new List<ValuePair>(20);

            for (int i = 0; i < 5; ++i)
            {
                for (int j = 0; j < 4; ++j)
                {
                    infinityBonusProvider.Add(ValuePair.Create((int)Math.Pow(2, i), (int)Math.Pow(2, j)));
                }
            }
        }

        #region Schedule

        public static bool ParseSchedulePeriod(ConstData.EventSchedule schedule, DateTime currentTime, ref DateTime beginTime, ref DateTime endTime)
        {
            if (schedule.n_TYPE != EventScheduleType.Quest && schedule.n_TYPE != EventScheduleType.Crusade && schedule.n_TYPE != EventScheduleType.PVP) return false;

            if (schedule.n_WEEKLY > 0)
            {
                var complex = new ComplexI32 { Value = schedule.n_WEEKLY };

                var day = currentTime.DayOfWeek;

                if (day == DayOfWeek.Sunday)
                {
                    if (!complex[6]) return false;
                }
                else
                {
                    if (!complex[(int)day - 1]) return false;
                }

                beginTime = ParseBeginTime(currentTime.Year, currentTime.Month, currentTime.Day, schedule.n_START_TIME);
            }
            else if (schedule.n_START_DAY > 0)
            {
                beginTime = ParseBeginTime(schedule.n_START_DAY, schedule.n_START_TIME);
            }
            else
                return false;

            endTime = beginTime.AddMinutes(schedule.n_DURATION);

            return true;
        }

        public static DateTime ParseBeginTime(int date, int time)
        {
            if (date == 0) return DateTime.MinValue;

            var year = 2000 + date / 10000;
            var month = (date % 10000) / 100;
            var day = date % 100;

            return ParseBeginTime(year, month, day, time);
        }

        private static DateTime ParseBeginTime(int year, int month, int day, int time)
        {
            var hour = time / 100;
            var min = time % 100;

            return new DateTime(year, month, day, hour, min, 0);
        }

        #endregion

        #region Infinity

        public static List<ValuePair> CalcInfinityBonus(Int64 seed)
        {
            var list = new List<ValuePair>(infinityBonusProvider);

            var _seed = MathUtils.Seed.Next((int)seed);

            for (int i = 0; i < list.Count; ++i)
            {
                _seed = MathUtils.Seed.Next(_seed);
                var rand = _seed % list.Count;
                var tmp = list[i];
                list[i] = list[rand];
                list[rand] = tmp;
            }

            return list;
        }

        public static ValuePair CalcInfinityBonus(List<ValuePair> provider, DateTime beginTime, DateTime currentTime)
        {
            var day = (int)currentTime.Date.Subtract(beginTime.Date).TotalDays;
            return provider[day % provider.Count];
        }

        public static float CalcInfinityBonus(IEnumerable<int> cards, ValuePair bonus)
        {
            var bonus1 = 1f;
            var bonus2 = 1f;
            foreach (var item in cards)
            {
                var data = Tables.Card[item];
                if (data == null) continue;
                if (data.n_TYPE == bonus.Value1) bonus1 = 1f + Tables.TowerBonus * 0.01f;
                if (data.n_GROUP == bonus.Value2) bonus2 = 1f + Tables.TowerBonus * 0.01f;
            }



            return bonus1 * bonus2;
        }
        #endregion

        class QuestDesign
        {
            public Dictionary<int, QuestEventGroup> Groups;
            public Drop.DropGroup DropGroup;

            public ConstData.QuestDesign Rawdata;

            public int ID { get { return Rawdata.n_ID; } }

            public int Rounds { get { return Rawdata.n_STEP; } }

            public ConstData.QuestEvent SelectEvent(int round)
            {
                QuestEventGroup group = null;

                if (Groups.TryGetValue(round, out group)) return group.Select();

                return null;
            }

            public int CalcDrop()
            {
                if (DropGroup == null) return 0;

                var drop = DropGroup.Next();
                if (drop == null) return 0;
                return drop.n_ID;
            }

            public int TriggerLimited()
            {
                if (Rawdata.n_SPSTAGE_ID > 0 && MathUtils.Random.Next(100) < Rawdata.n_SPSTAGE_RATE) return Rawdata.n_SPSTAGE_ID;
                return 0;
            }
        }

        class QuestEventGroup
        {
            private int weights;

            private List<ConstData.QuestEvent> list = new List<ConstData.QuestEvent>();


            public void Add(ConstData.QuestEvent ev)
            {
                weights += ev.n_VALUE;
                list.Add(ev);
            }

            public ConstData.QuestEvent Select()
            {
                var rand = MathUtils.Random.Next(weights);

                var weight = 0;
                foreach (var ev in list)
                {
                    weight += ev.n_VALUE;

                    if (rand < weight) return ev;
                }

                if (list.Count == 0) return null;

                return list[list.Count - 1];
            }
        }
    }
}

namespace Mikan.CSAGA
{
    /// <summary>
    /// 任務副本
    /// </summary>
    public class QuestInstance : IQuest
    {

        /// <summary>
        /// DB流水號
        /// </summary>
        public Int64 Serial;

        /// <summary>
        /// 任務ID
        /// </summary>
        public int QuestID;

        /// <summary>
        /// 任務事件列表
        /// </summary>
        public List<int> Events;

        /// <summary>
        /// 掉落物ID
        /// </summary>
        public int Drop;

        /// <summary>
        /// 神運掉落物ID
        /// </summary>
        public int LuckDrop;

        /// <summary>
        /// 獲得點數
        /// </summary>
        public int Point;

        /// <summary>
        /// 獲得探索值
        /// </summary>
        public int Discover;

        /// <inheritdoc />
        public int Rounds
        {
            get { return Events.Count; }
        }

        /// <inheritdoc />
        public void Serialize(IOutput output)
        {
            output.Write(Serial);
            output.Write(QuestID);
            output.Write(Events);
            output.Write(Drop);
            output.Write(LuckDrop);
        }

        /// <inheritdoc />
        public void DeSerialize(IInput input)
        {
            Serial = input.ReadInt64();
            QuestID = input.ReadInt32();
            Events = input.ReadInt32List();
            Drop = input.ReadInt32();
            LuckDrop = input.ReadInt32();
        }
    }

    public class QuestSettlement
    {
        public bool IsSucc;

        public bool IsAutoBattleEnabled;

        public List<int> Drops;

        public SyncPlayer SyncPlayer;

        public SyncCard SyncCard;

        public SyncItem SyncItem;

        public int TriggerQuestID = 0;

        public RescueState RescueState;

        internal SyncLimitedQuest SyncLimitedQuest;

        public int Point;

        public int Discover;
    }

    abstract public class QuestEventFactoryBase : IQuestEventFactory
    {
        public event Action<IQuestEventFactory> OnReady;

        virtual public void Prepare(IQuest quest) { if (OnReady != null) OnReady(this); }

        public IEnumerable<IQuestEvent> Create(IQuest quest, int round)
        {
            var inst = quest as QuestInstance;

            var interactive = Calc.Quest.GetEvent(inst.QuestID, round + 1);

            if (interactive != 0) yield return CreateInteractiveEvent(inst, round, interactive);

            var ev = inst.Events[round];

            yield return CreateRoundBeginEvent(inst, round);
            yield return CreateFightEvent(inst, round, ev);
            yield return CreateRoundEndEvent(inst, round);

            if (round + 1 == quest.Rounds)
            {
                interactive = Calc.Quest.GetEvent(inst.QuestID, 99);
                if (interactive != 0) yield return CreateInteractiveEvent(inst, round, interactive);
            }
        }

        abstract protected IQuestEvent CreateInteractiveEvent(QuestInstance quest, int round, int interactiveId);
        abstract protected IQuestEvent CreateFightEvent(QuestInstance quest, int round, int eventId);
        virtual protected IQuestEvent CreateRoundBeginEvent(QuestInstance quest, int round) { return new DummyEvent(); }
        virtual protected IQuestEvent CreateRoundEndEvent(QuestInstance quest, int round) { return new DummyEvent(); }

        class DummyResult : IQuestEventResult
        {
            public IQuestEvent Event { get; set; }

            public bool IsSucc { get { return true; } }

            public bool Continue { get { return IsSucc; } }

            public void Serialize(IOutput output) { }

            public void DeSerialize(IInput input) { }

        }
        class DummyEvent : IQuestEvent
        {
            public event Action<IQuestEvent> OnPreload;

            public event Action<IQuestEvent> OnReady;

            public event Action<IQuestEventResult> OnFinish;

            public void Preload()
            {
                if (OnPreload != null) OnPreload(this);
            }

            public void Prepare(IQuestController controller)
            {
                if (OnReady != null) OnReady(this);
            }

            public void Start()
            {
                if (OnFinish != null) OnFinish(new DummyResult { Event = this });
            }

            public void Pause()
            {
            }

            public void Resume()
            {
            }

            public void Cancel()
            {
            }

            public void Serialize(IOutput output)
            {
            }

            public void DeSerialize(IInput input)
            {
            }

            public void Dispose()
            {
            }
        }
    }

    public class QuestController : QuestController<QuestInstance>
    {
        
    }
}