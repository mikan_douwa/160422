﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mikan.CSAGA.Calc
{
    public class Drop : Calculator
    {
        private static Dictionary<int, DropGroup> drops;

        public static DropGroup Get(int group)
        {
            return DictionaryUtils.SafeGetValueNullable(drops, group);
        }

        public static int Next(int group)
        {
            var drop = Get(group).Next();
            return drop != null ? drop.n_ID : 0;
        }

        public static Data.Gift ToGift(int dropId)
        {
            return ToGift(Tables.Drop[dropId]);
        }
        public static Data.Gift ToGift(ConstData.Drop drop)
        {
            var gift = new Data.Gift();
            gift.Type = drop.n_DROP_TYPE;
            if (gift.Type == DropType.Card)
                gift.Value1 = drop.n_COUNT * 1000 + drop.n_RARE * 100 + drop.n_LUCK;
            else
                gift.Value1 = drop.n_COUNT;
            gift.Value2 = drop.n_DROP_ID;

            return gift;
        }

        internal static void Setup()
        {
            drops = new Dictionary<int, DropGroup>();

            foreach (var item in Tables.Drop.Select())
            {
                var group = DictionaryUtils.SafeGetValue(drops, item.n_GROUP);
                group.Add(item);
            }
        }

        public class DropGroup
        {
            private int weights;

            private List<ConstData.Drop> list = new List<ConstData.Drop>();

            public ConstData.Drop First { get { return list.Count > 0 ? list[0] : null; } }

            public void Add(ConstData.Drop drop)
            {
                weights += drop.n_VALUE;
                list.Add(drop);
            }

            public ConstData.Drop Next()
            {
                var rand = MathUtils.Random.Next(weights);

                var weight = 0;
                foreach (var drop in list)
                {
                    weight += drop.n_VALUE;

                    if (rand < weight) return drop;
                }

                if (list.Count == 0) return null;

                return list[list.Count - 1];
            }
        }
    }
}
