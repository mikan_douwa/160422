﻿using System;
using System.Collections.Generic;
using System.Text;

#if SERVER_CODE
namespace Mikan.CSAGA.Calc
{


    public class Skill : Calculator
    {
        private const int PVP_PHASE1_2 = 101;
        private const int PVP_PHASE3 = 102;

        private static List<PVPSkill> pvpPhase1;
        private static List<PVPSkill> pvpPhase2;
        private static List<PVPSkill> pvpPhase3;

        public static List<PVPSkill> RandPVPSkill()
        {
            var list = new List<PVPSkill>();

            list.AddRange(RandPVPSkill(pvpPhase1, 2));
            list.AddRange(RandPVPSkill(pvpPhase2, 2));
            list.AddRange(RandPVPSkill(pvpPhase3, 2));

            return list;
        }

        private static IEnumerable<PVPSkill> RandPVPSkill(List<PVPSkill> provider, int count)
        {
            var copy = new List<PVPSkill>(provider);
            MathUtils.Shuffle(copy);
            for (int i = 0; i < count; ++i) yield return copy[i];
        }

        internal static void Setup()
        {
            FilterPVPSkill();
        }

        private static void FilterPVPSkill()
        {
            pvpPhase1 = new List<PVPSkill>();
            pvpPhase2 = new List<PVPSkill>();
            pvpPhase3 = new List<PVPSkill>();
            foreach (var item in Tables.Skill.Select(o => o.n_TYPE > 10000))
            {
                switch (item.n_TRIGGER)
                {
                    case PVP_PHASE1_2:
                        Add(pvpPhase1, item);
                        Add(pvpPhase2, item);
                        break;
                    case PVP_PHASE3:
                        Add(pvpPhase3, item);
                        break;
                }
            }
        }

        private static void Add(List<PVPSkill> list, ConstData.Skill data)
        {
            var skill = list.Find(o => o.Group == data.n_TYPE);
            if (skill == null)
            {
                skill = new PVPSkill();
                skill.Group = data.n_TYPE;
                list.Add(skill);
            }

            skill.Add(data);
        }

        

    }
}
namespace Mikan.CSAGA
{
    public class PVPSkill
    {
        public int Group { get; internal set; }
        private Dictionary<int, int> ids = new Dictionary<int, int>();
        public void Add(ConstData.Skill skill)
        {
            ids[skill.n_CD] = skill.n_ID;
        }
        public int GetID(int level)
        {
            var id = DictionaryUtils.SafeGetValueNullable(ids, level);

            if (id == 0) id = DictionaryUtils.SafeGetValueNullable(ids, 1);

            return id;
        }
    }
}
#else
namespace Mikan.CSAGA.Calc
{
    public class Skill : Calculator
    {
        internal static void Setup() { }
    }
}
#endif
