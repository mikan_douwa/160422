﻿using System;
using System.Collections.Generic;
using System.Text;


namespace Mikan.CSAGA.Calc
{
    using Provider = List<ConstData.Interactive>;
    using ProviderList = Dictionary<int, List<ConstData.Interactive>>;
    
    public class CardEvent : Calculator
    {
        public enum Filter
        {
            Common,
            Contact,
            Settlement,
        }

        private static Provider empty = new Provider();

        private static ProviderList common;
        private static ProviderList contact;
        private static ProviderList settlement;
        private static ProviderList events;

        internal static void Setup()
        {
            common = new ProviderList();

            contact = new ProviderList();

            settlement = new ProviderList();

            events = new ProviderList();

            foreach (var item in Tables.Interactive.Select())
            {
                switch (item.n_TRIGGER)
                {
                    case InteractiveTrigger.Common:
                        Add(common, item);
                        Add(contact, item);
                        break;
                    case InteractiveTrigger.Choice:
                        Add(contact, item);
                        break;
                    case InteractiveTrigger.Settlement:
                        Add(settlement, item);
                        break;
                    case InteractiveTrigger.None:
                        Add(events, item);
                        break;
                }
            }
        }

        private static void Add(ProviderList providerList, ConstData.Interactive data)
        {
            var provider = DictionaryUtils.SafeGetValue(providerList, data.n_CARDID[0]);
            provider.Add(data);
        }

        private static Provider GetProvider(ProviderList list, int cardId)
        {
            var provider = default(Provider);
            if (list.TryGetValue(cardId, out provider)) return provider;
            return empty;
        }

        public static IEnumerable<ConstData.Interactive> Select(int cardId)
        {
            return GetProvider(events, cardId);
        }

        public static int Next(Filter filter, int cardId, int favor, ICollection<int> cardList)
        {
            var list = default(ProviderList);

            switch (filter)
            {
                case Filter.Contact:
                    list = contact;
                    break;
                case Filter.Settlement:
                    list = settlement;
                    break;

                case Filter.Common:
                default:
                    list = common;
                    break;
            }

            var source = GetProvider(list, cardId);

            var provider = new Provider();

            var total = 0;

            foreach (var item in source)
            {

                if ((item.n_FAVOR_MIN > 0 && favor < item.n_FAVOR_MIN) ||
                   (item.n_FAVOR_MAX > 0 && favor > item.n_FAVOR_MAX)
                    ) continue;

                var contains = true;
                for (int i = 1; i < item.n_CARDID.Length; ++i)
                {
                    var id = item.n_CARDID[i];

                    if (id != 0 && !cardList.Contains(id))
                    {
                        contains = false;
                        break;
                    }
                }
                if (!contains) continue;
                total += item.n_RATE;
                provider.Add(item);
            }

            var rand = MathUtils.Random.Next(total);
            var weight = 0;

            foreach (var item in provider)
            {
                weight += item.n_RATE;
                if (weight > rand) return item.n_ID;
            }

            return 0;
        }

        public static int GetIndex(int cardId, int interactiveId)
        {
            var index = -1;

            var interactive = default(ConstData.Interactive);


            foreach (var item in Select(cardId))
            {
                ++index;
                if (item.n_ID == interactiveId) return index;
            }

            return -1;
        }

        public static ErrorCode Check<T>(ConstData.Interactive data, int kizuna, ICollection<int> ids, Finder<T> questFinder)
            where T : Internal.QuestStateObj
        {
            #region 檢查羈絆值夠不夠
            if (kizuna < data.n_FAVOR_MIN) return ErrorCode.KizunaNotEnough;
            #endregion

            #region 檢查觸發事件需要的卡片存不存在

            foreach (var id in data.n_CARDID)
            {
                if (id != 0 && !ids.Contains(id)) return ErrorCode.CardNotExists;
            }

            #endregion

            #region 檢查前置任務

            if (data.n_PRE_QUEST > 0)
            {
                var state = questFinder(data.n_PRE_QUEST);
                if (state == null || !state.IsCleared) return ErrorCode.QuestIllegal;
            }

            #endregion

            return ErrorCode.Success;
        }
    }
}
