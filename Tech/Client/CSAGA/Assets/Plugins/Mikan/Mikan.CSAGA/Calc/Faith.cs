﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mikan.CSAGA.Calc
{
    public class Faith : Calculator
    {
        public static float MaterialCount(DateTime claimTime, DateTime currentTime, float bonus)
        {
            var minutes = Math.Min(currentTime.Subtract(claimTime).TotalMinutes, Tables.GetVariablef(VariableID.FAITH_MAX));
            var count = (float)(minutes / Tables.GetVariablef(VariableID.FAITH_PERIOD));
            if (bonus > 0) return count * (1f + bonus);
            return count;
        }

        internal static void Setup()
        {
            
        }
    }
}
