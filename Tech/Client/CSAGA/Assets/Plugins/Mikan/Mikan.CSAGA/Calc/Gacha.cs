﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mikan.CSAGA.Calc
{
    public class Gacha : Calculator
    {
        public static GachaSchedule GetGachaSchdule()
        {
            var schedule = new GachaSchedule();

            foreach (var item in Tables.EventSchedule.Select(o => { return o.n_TYPE == EventScheduleType.Gacha && o.n_START_DAY > 0; }))
            {
                var tag = (GachaListTag)item.n_QUESTID;
                switch (tag)
                {
                    case GachaListTag.RegularGacha:
                        schedule.regular = item;
                        break;
                    case GachaListTag.ActivityGacha:
                        schedule.activity = item;
                        break;
                }
            }

            return schedule;
        }
    }

    public class GachaSchedule
    {
        internal ConstData.EventSchedule regular;

        internal ConstData.EventSchedule activity;

        public DateTime ActivityEndTime
        {
            get
            {
                if (activity == null) return DateTime.MinValue;

                var beginTime = Quest.ParseBeginTime(activity.n_START_DAY, activity.n_START_TIME);
                return beginTime.AddMinutes(activity.n_DURATION);
            }
        }

        public int GetGachaID(int gachaListId, DateTime currentTime)
        {
            if (ConstData.Tables.Instance.RegularGachaIDs.Contains(gachaListId))
            {
                if (!IsOverdue(regular, currentTime) && regular.n_WEEKLY > 0) return regular.n_WEEKLY;
            }
            else if (ConstData.Tables.Instance.ActivityGachaIDs.Contains(gachaListId))
            {
                if (IsOverdue(activity, currentTime)) return 0;
                if (activity.n_WEEKLY > 0) return activity.n_WEEKLY;

            }

            return ConstData.Tables.Instance.GachaList[gachaListId].n_GACHAID;
        }

        private static bool IsOverdue(ConstData.EventSchedule schedule, DateTime currentTime)
        {
            if (schedule == null) return true;
            var beginTime = Quest.ParseBeginTime(schedule.n_START_DAY, schedule.n_START_TIME);
            return currentTime < beginTime || currentTime > beginTime.AddMinutes(schedule.n_DURATION);
        }
    }
}
