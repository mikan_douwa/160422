﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mikan.CSAGA.Calc
{
    public class Equipment : Calculator
    {
        public static T Generate<T>(int id)
            where T : Internal.EquipmentObj, new()
        {
            var obj = new T { ID = id };

            Generate(obj);

            return obj;
        }
        

        public static void Generate<T>(T obj)
            where T : Internal.EquipmentObj
        {
            var data = Tables.Item[obj.ID];
            
            obj[PropertyID.Hp] = RandomProperty(100, data.n_HP_MIN, data.n_HP_MAX);
            obj[PropertyID.Atk] = RandomProperty(100, data.n_ATK_MIN, data.n_ATK_MAX);
            obj[PropertyID.Rev] = RandomProperty(100, data.n_REV_MIN, data.n_REV_MAX);
            obj[PropertyID.Chg] = RandomProperty(data.n_CHG_RATE, data.n_CHG_MIN, data.n_CHG_MAX);

            var counter = RandomProperty(data.n_COUNTER_RATE, data.n_COUNTER_MIN, data.n_COUNTER_MAX);

            if (counter != 0)
            {
                var candidates = new List<int>();
                var complex = new ComplexI32 { Value = data.n_COUNTER_TYPE };
                for (int i = 0; i < 5; ++i)
                {
                    if (complex[i]) candidates.Add((int)Math.Pow(2, i));
                }

                if (candidates.Count > 0)
                {
                    obj[PropertyID.CounterType] = candidates[MathUtils.Random.Next(candidates.Count)];
                    obj[PropertyID.Counter] = counter;
                }
            }
        }

        private static int RandomProperty(int rate, int min, int max)
        {
            if (rate > 0 && MathUtils.Random.Next(100) < rate)
            {
                return MathUtils.Random.Next(min, max + 1);
            }

            return 0;
        }
    }
}
