﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mikan.CSAGA.Calc
{
    abstract public class Calculator
    {
        protected static ConstData.Tables Tables { get { return ConstData.Tables.Instance; } }

        public static void Initialize()
        {
            Drop.Setup();
            CardEvent.Setup();
            Quest.Setup();
            Trophy.Setup();
            Daily.Setup();
            CoMission.Setup();
            Faith.Setup();
            Skill.Setup();
        }
    }
}
