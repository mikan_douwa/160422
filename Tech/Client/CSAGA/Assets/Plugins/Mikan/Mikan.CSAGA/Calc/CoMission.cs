using System;
using System.Collections.Generic;
using System.Text;

namespace Mikan.CSAGA.Calc
{
    public class CoMission : Calculator
    {

        public static void Setup()
        {
            

        }

        public static IEnumerable<SyncMission> TriggerQuest(IEnumerable<Data.MissionSubejct> subjects, int questId, List<int> ids)
        {
            var questData = Tables.QuestDesign[questId];

            foreach (var subject in Select(subjects, MissionType.Quest1, MissionType.Quest2))
            {
                if (!CheckBigger(subject.Data.n_CONDITION[0], questData.n_LEVEL)) continue;
                if (!Check(subject.Data.n_CONDITION[1], questId)) continue;

                if (subject.Data.n_TYPE == MissionType.Quest1)
                {
                    if (!CheckCard(subject.Data, ids[0]) && !CheckCard(subject.Data, ids[1])) continue;
                }
                else if (subject.Data.n_TYPE == MissionType.Quest2)
                {
                    var _ids = new List<int>();
                    var targetCount = 0;
                    
                    if (subject.Data.n_ID_[0] > 0 && subject.Data.n_ID_[1] > 0)
                    {
                        // 需要包含2個角色
                        targetCount = 2;

                        foreach (var id in ids)
                        {
                            if (_ids.Contains(id)) continue;
                            if (CheckCard(subject.Data, id)) _ids.Add(id);
                        }
                    }
                    else
                    {
                        targetCount = 0;

                        foreach (var id in ids)
                        {
                            if (id != 0 && !CheckCard(subject.Data, id, false)) break;
                            ++targetCount;
                        }

                        if (targetCount < ids.Count) continue;

                        targetCount = 1;

                        foreach (var id in ids)
                        {
                            if (id == 0 || !CheckCard(subject.Data, id)) continue;
                            _ids.Add(id);
                            break;
                        }
                    }

                    if (_ids.Count < targetCount) continue;
                }

                yield return new SyncMission { Serial = subject.Origin.MissionID, Index = subject.Origin.Index, SubjectID = subject.Origin.ID, Count = 1 };
            }
        }

        public static IEnumerable<SyncMission> TriggerGift(IEnumerable<Data.MissionSubejct> subjects, int cardId, int giftId, int count)
        {
            foreach (var subject in Select(subjects, MissionType.Gift))
            {
                if (!CheckCard(subject.Data, cardId)) continue;

                if (!Check(subject.Data.n_CONDITION[0], Tables.Item[giftId].n_EFFECT_Y)) continue;

                yield return new SyncMission { Serial = subject.Origin.MissionID, Index = subject.Origin.Index, SubjectID = subject.Origin.ID, Count = count };

            }
        }
        public static IEnumerable<SyncMission> TriggerTalk(IEnumerable<Data.MissionSubejct> subjects, int cardId)
        {
            foreach (var subject in Select(subjects, MissionType.Talk))
            {
                if (!CheckCard(subject.Data, cardId)) continue;
                yield return new SyncMission { Serial = subject.Origin.MissionID, Index = subject.Origin.Index, SubjectID = subject.Origin.ID, Count = 1 };
            }
        }

        private static bool CheckCard(ConstData.CoMission data, int cardId)
        {
            return CheckCard(data, cardId, true);
        }

        private static bool CheckCard(ConstData.CoMission data, int cardId, bool force)
        {
            if (cardId > 0)
            {
                var cardData = Tables.Card[cardId];

                if (!Check(data.n_TYPE_[0], cardData.n_TYPE) || !Check(data.n_TYPE_[1], cardData.n_GROUP)) return false;
            }

            if (!force) return true;

            if(data.n_TYPE != MissionType.Quest2 || data.n_ID_[1] == 0)
                return Check(data.n_ID_[0], cardId);

            return Check(data.n_ID_[0], cardId) || Check(data.n_ID_[1], cardId);
        }

        private static bool Check(int source, int target)
        {
            return (source == 0 || source == target);
        }

        private static bool CheckBigger(int source, int target)
        {
            return (source == 0 || target >= source);
        }

        private static IEnumerable<MissionSubject> Select(IEnumerable<Data.MissionSubejct> subjects, params MissionType[] filters)
        {
            var types = new List<MissionType>(filters);

            foreach (var subject in subjects)
            {
                var data = Tables.CoMission[subject.ID];

                if (data.n_MAX > 0 && subject.Progress >= data.n_MAX) continue;

                if (!types.Contains(data.n_TYPE)) continue;

                yield return new MissionSubject { Origin = subject, Data = data };

            }
        }

        class MissionSubject
        {
            public Data.MissionSubejct Origin;
            public ConstData.CoMission Data;
        }

    }
}
