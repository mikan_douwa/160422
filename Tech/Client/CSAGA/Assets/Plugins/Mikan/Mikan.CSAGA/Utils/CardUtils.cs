﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mikan.CSAGA
{
    public class CardUtils
    {
        public static List<int> GetIDs<T>(IEnumerable<T> list)
            where T : Internal.CardObj
        {
            var ids = new List<int>();

            GetIDs(list, ids);

            return ids;
        }

        public static void GetIDs<T, TList>(IEnumerable<T> list, TList ids)
            where T : Internal.CardObj
            where TList : ICollection<int>
        {
            foreach (var item in list)
            {
                if (!ids.Contains(item.ID)) ids.Add(item.ID);
            }
        }

        public static int CalcCrystal(int exp)
        {
            return (int)Math.Floor(exp * ConstData.Tables.Instance.ExpTransRate);
        }

        public static int CalcProperty(int lv, int lvMax, int min, int max, float factor, float upStyle, float charge)
        {
            var deltaLv = lvMax > 1 ? ((float)lv - 1) / ((float)lvMax - 1) : 1;
            var val = (float)min + (max - min) * Math.Pow(deltaLv, upStyle);
            return (int)Math.Ceiling((val * factor) * (charge / ConstData.Tables.Instance.BasicAspd));
        }

        public static int CalcExp(int id, int level)
        {
            return CalcExp(ConstData.Tables.Instance.Card[id], level);

        }

        public static int CalcExp(ConstData.CardData data, int level)
        {
            if (level <= 1) return 0;
            return (int)(data.n_EXPTYPE * Math.Pow(((double)level - 1.0) / (99.0 - 1.0), (double)ConstData.Tables.Instance.ExpLvUpStyle));
        }

        public static int CalcLV(int id, int exp)
        {
            var data = ConstData.Tables.Instance.Card[id];
            return CalcLV(data, data.n_RARE, exp);
        }

        public static int CalcLV(int id, int rare, int exp)
        {
            return CalcLV(ConstData.Tables.Instance.Card[id], rare, exp);
        }

        public static int CalcLV(ConstData.CardData data, int rare, int exp)
        {
            return CalcLV(data, ConstData.Tables.Instance.CardBalance[rare], exp);
        }

        public static int CalcLV(ConstData.CardData data, ConstData.CardBalance balance, int exp)
        {
            if (exp == 0) return 1;
            if (exp >= data.n_EXPTYPE) return balance.n_LVMAX;
            return CalcLVRecursive(data, balance, exp, 1, balance.n_LVMAX);
        }

        private static int CalcLVRecursive(ConstData.CardData data, ConstData.CardBalance balance, int exp, int min, int max)
        {
            if ((max - min) < 2)
            {
                var maxExp = CalcExp(data, max);
                return exp < maxExp ? min : max;
            }

            var mid = (int)((max + min) / 2);

            var midExp = CalcExp(data, mid);

            if (exp == midExp) return mid;
            if (exp > midExp) return CalcLVRecursive(data, balance, exp, mid, max);
            return CalcLVRecursive(data, balance, exp, min, mid - 1);
        }

        public static Data.Card GenerateDummy(int cardId, int rare, int exp)
        {
            var data = Global.Tables.Card[cardId];
            var card = new Data.Card { Complex = new Complex() };
            card.Serial = Serial.Empty;
            card.ID = data.n_ID;
            card.Exp = exp;
            card.Rare = rare > 0 ? rare : data.n_RARE;
            return card;
        }

        public static Data.Card GenerateDummy(int cardId)
        {
            return GenerateDummy(cardId, 0, 0);
        }
    }

    public class CardDiff : PropertyDiff
    {
        public Serial Serial = Serial.Empty;

        public override void Serialize(IOutput output)
        {
            base.Serialize(output);
            output.Write(Serial.Value);
        }

        public override void DeSerialize(IInput input)
        {
            base.DeSerialize(input);
            Serial = Serial.Create(input.ReadInt64());
        }

        public static CardDiff Create(Serial serial, PropertyID id, int oldValue, int newValue)
        {
            var diff = new CardDiff();
            diff.Serial = serial;
            diff.Add(id, oldValue, newValue);
            return diff;
        }
    }
}
