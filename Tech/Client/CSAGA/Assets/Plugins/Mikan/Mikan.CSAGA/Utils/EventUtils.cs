﻿using System;
using System.Collections.Generic;
using System.Text;
using Mikan.CSAGA.ConstData;

namespace Mikan.CSAGA
{
    public class EventUtils
    {
        public static EventShow CallEvent(int interactiveId)
        {
            var data = Tables.Instance.Interactive[interactiveId];
            if (data == null) return null;
            return CallEvent(data);
        }

        public static EventShow CallEvent(Interactive interactive)
        {
            return CallEventByGroup(interactive.n_CALL_EVENT);
        }

        public static EventShow CallEventByGroup(int group)
        {
            return Tables.Instance.EventShow.SelectFirst(o => { return o.n_GROUP == group; });
        }
    }
}
