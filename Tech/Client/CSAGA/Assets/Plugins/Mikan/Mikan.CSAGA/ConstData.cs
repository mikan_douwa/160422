﻿using System;
using System.Collections.Generic;
using System.Text;


namespace Mikan.CSAGA.ConstData
{
    #region CONST_DATA

    /// <summary>
    /// 變數
    /// </summary>
    public class Variable : ConstDataObject
    {
        /// <summary>
        /// 變數名稱
        /// </summary>
        public string s_NAME_VARIABLE;
        /// <summary>
        /// 值
        /// </summary>
        public float f_VALUE_VARIABLE = 0f;
    }

    /// <summary>
    /// 玩家基礎
    /// </summary>
    public class PlayerData : ConstDataObject
    {
        /// <summary>
        /// 經驗值
        /// </summary>
        public int n_EXP;
        /// <summary>
        /// 累積經驗值
        /// </summary>
        public int n_TOTALEXP;
        /// <summary>
        /// 行動力上限
        /// </summary>
        public int n_MAX_ACT;
        /// <summary>
        /// 隊伍數量
        /// </summary>
        public int n_TEAM;
        /// <summary>
        /// 紋章空間
        /// </summary>
        public int n_EQUIP_SPACE;
        /// <summary>
        /// 信仰點數
        /// </summary>
        public int n_FAITH_POINT;
        
        /// <summary>
        /// 神運能力
        /// </summary>
        public int n_LUCK_ABILITY;
        /// <summary>
        /// 神運掉寶
        /// </summary>
        public int n_LUCK_DROP;

        /// <summary>
        /// 關注上限
        /// </summary>
        public int n_FOLLOW;

        /// <summary>
        /// 粉絲上限
        /// </summary>
        public int n_FANS;

        /// <summary>
        /// BONUS積分上限
        /// </summary>
        public int n_BONUS_POINT;

    }
    /// <summary>
    /// 卡片
    /// </summary>
    public class CardData : ConstDataObject
    {
        /// <summary>
        /// 角色編碼
        /// </summary>
        public string s_FILENAME;
        /// <summary>
        /// 稀有度
        /// </summary>
        public int n_RARE;
        /// <summary>
        /// 屬性
        /// </summary>
        public int n_TYPE;
        /// <summary>
        /// 定位
        /// </summary>
        public int n_GROUP;
        /// <summary>
        /// Lv99EXP上限
        /// </summary>
        public int n_EXPTYPE;
        /// <summary>
        /// HP比例
        /// </summary>
        public float f_HP;
        /// <summary>
        /// ATK比例
        /// </summary>
        public float f_ATK;
        /// <summary>
        /// REV比例
        /// </summary>
        public float f_REV;
        /// <summary>
        /// 攻擊速度
        /// </summary>
        public float f_CHARGE;
        /// <summary>
        /// HP成長曲線
        /// </summary>
        public float f_HP_LVUP;
        /// <summary>
        /// ATK成長曲線
        /// </summary>
        public float f_ATK_LVUP;
        /// <summary>
        /// REV成長曲線
        /// </summary>
        public float f_REV_LVUP;
        /// <summary>
        /// 禮物傾向
        /// </summary>
        public int n_GIFT;
        /// <summary>
        /// 代幣
        /// </summary>
        public int n_TRADE_COIN;
        /// <summary>
        /// 神運上限
        /// </summary>
        public int n_LUCK_MAX;

        /// <summary>
        /// 神運轉換
        /// </summary>
        public int n_LUCK_GIFT;

        /// <summary>
        /// 圖鑑
        /// </summary>
        public int n_GALLERY;
        /// <summary>
        /// 錨點X
        /// </summary>
        public int n_CUTIN_X;
        /// <summary>
        /// 錨點Y
        /// </summary>
        public int n_CUTIN_Y;
        /// <summary>
        /// 隊長技
        /// </summary>
        public int n_LEADER_SKILL;
        /// <summary>
        /// 主動技(1~2)
        /// </summary>
        public int[] n_SKILL;

    }

    public class CardBalance : ConstDataObject
    {
        /// <summary>
        /// HP起始
        /// </summary>
        public int n_HP_MIN;
        /// <summary>
        /// ATK起始
        /// </summary>
        public int n_ATK_MIN;
        /// <summary>
        /// REV起始
        /// </summary>
        public int n_REV_MIN;
        /// <summary>
        /// HP終值
        /// </summary>
        public int n_HP_MAX;
        /// <summary>
        /// ATK終值
        /// </summary>
        public int n_ATK_MAX;
        /// <summary>
        /// REV終值
        /// </summary>
        public int n_REV_MAX;
        /// <summary>
        /// 等級上限
        /// </summary>
        public int n_LVMAX;

        /// <summary>
        /// 加值上限
        /// </summary>
        public int n_PLUS;

        /// <summary>
        /// PVP加權HP
        /// </summary>
        public int n_PVP_HP;

        /// <summary>
        /// PVP加權ATK
        /// </summary>
        public int n_PVP_ATK;

        /// <summary>
        /// PVP加權REV
        /// </summary>
        public int n_PVP_REV;
    }

    /// <summary>
    /// 怪物
    /// </summary>
    public class Mob : ConstDataObject
    {
        /// <summary>
        /// 怪物名稱
        /// </summary>
        public string s_MAIN;
        /// <summary>
        /// 怪物字串
        /// </summary>
        public string s_FILENAME;
        /// <summary>
        /// 戰鬥套圖
        /// </summary>
        public string s_BATTLE;

        /// <summary>
        /// 特殊標記
        /// </summary>
        public int n_CLASS;

        /// <summary>
        /// 顯示比例
        /// </summary>
        public int n_SIZE;

        /// <summary>
        /// 屬性
        /// </summary>
        public int n_TYPE;
        /// <summary>
        /// HP
        /// </summary>
        public int n_HP;
        /// <summary>
        /// ATK
        /// </summary>
        public int n_ATK;
        /// <summary>
        /// CHARGE
        /// </summary>
        public float f_CHARGE;

        /// <summary>
        /// 怪物AI
        /// </summary>
        public int n_MOBAI;

        /// <summary>
        /// 怪物技能(1~6)
        /// </summary>
        public int[] n_MOBSKILL;

        /// <summary>
        /// 先制技能
        /// </summary>
        public int n_FIRST_SKILL;
        /// <summary>
        /// 先制技能台詞
        /// </summary>
        public int n_FIRST_WORD;
        /// <summary>
        /// 死亡技能
        /// </summary>
        public int n_DEATH_SKILL;
        /// <summary>
        /// 死亡技能台詞
        /// </summary>
        public int n_DEATH_WORD;
    }

    /// <summary>
    /// 怪物AI
    /// </summary>
    public class MobAI : ConstDataObject
    {
        /// <summary>
        /// AI群組
        /// </summary>
        public int n_GROUP;
        /// <summary>
        /// 條件
        /// </summary>
        public int n_CONDITION;
        /// <summary>
        /// 條件參數X
        /// </summary>
        public int n_CONDITION_X;
        /// <summary>
        /// 條件參數Y
        /// </summary>
        public int n_CONDITION_Y;
        /// <summary>
        /// 執行命令
        /// </summary>
        public int n_ACT;
        /// <summary>
        /// 命令參數X
        /// </summary>
        public int n_ACT_X;
        /// <summary>
        /// 命令參數Y
        /// </summary>
        public int n_ACT_Y;

        /// <summary>
        /// 怪物發話
        /// </summary>
        public int n_MOB_SPEAK;

        /// <summary>
        /// 通過切換
        /// </summary>
        public int n_ACT_TRUE;
        /// <summary>
        /// 不通過切換
        /// </summary>
        public int n_ACT_FALSE;
    }

    /// <summary>
    /// 角色表
    /// </summary>
    public class Chara : ConstDataObject
    {
        /// <summary>
        /// 角色ID
        /// </summary>
        public int n_CHARAID = 0;
        /// <summary>
        /// 服裝ID
        /// </summary>
        public int n_AVATAR = 0;
        /// <summary>
        /// 喜好度
        /// </summary>
        public int n_LOVE = 0;
        /// <summary>
        /// 好感度群組
        /// </summary>
        public int n_FAVOR = 0;
        /// <summary>
        /// 必殺技
        /// </summary>
        public int n_SKILL = 0;
        /// <summary>
        /// 角色文字
        /// </summary>
        public int n_CHARA_NAME = 0;
        /// <summary>
        /// 好感度群組文字
        /// </summary>
        public int n_FAVOR_NAME = 0;
        /// <summary>
        /// 戰鬥套圖
        /// </summary>
        public string s_BATTLE;
        /// <summary>
        /// 服裝文字
        /// </summary>
        public int n_AVATAR_NAME = 0;

        /// <summary>
        /// 爆炸情緒值增加	
        /// </summary>
        public int n_STRESS_UP;

        /// <summary>
        /// 爆炸情緒值減少
        /// </summary>
        public int n_STRESS_DOWN;

        /// <summary>
        /// 垂直定位
        /// </summary>
        public int n_LOCATION;

        /// <summary>
        /// 初始好感度
        /// </summary>
        public int n_FAVOR_INIT;
    }

    /// <summary>
    /// 技能表
    /// </summary>
    public class Skill : ConstDataObject
    {
        /// <summary>
        /// 技能類型
        /// </summary>
        public int n_TYPE;
        /// <summary>
        /// CD時間
        /// </summary>
        public int n_CD;
        /// <summary>
        /// 觸發條件
        /// </summary>
        public int n_TRIGGER;
        /// <summary>
        /// 觸發機率
        /// </summary>
        public int n_TRIGGER_RATE;
        /// <summary>
        /// 觸發條件X
        /// </summary>
        public int n_TRIGGER_X;
        /// <summary>
        /// 觸發條件Y
        /// </summary>
        public int n_TRIGGER_Y;
        /// <summary>
        /// 作用目標
        /// </summary>
        public int n_TARGET;
        /// <summary>
        /// 作用目標參數
        /// </summary>
        public int n_TARGET_TYPE;
        /// <summary>
        /// 作用屬性
        /// </summary>
        public int n_TARGET_X;
        /// <summary>
        /// 作用類型
        /// </summary>
        public int n_TARGET_Y;
        /// <summary>
        /// 發動效果
        /// </summary>
        public int n_EFFECT;
        /// <summary>
        /// 效果參數X
        /// </summary>
        public int n_EFFECT_X;
        /// <summary>
        /// 效果參數Y
        /// </summary>
        public int n_EFFECT_Y;
        /// <summary>
        /// 效果參數Z
        /// </summary>
        public int n_EFFECT_Z;
        /// <summary>
        /// 觸發狀態機率
        /// </summary>
        public int n_STATUS_RATE;
        /// <summary>
        /// 觸發狀態類型
        /// </summary>
        public int n_STATUS_TYPE;
        /// <summary>
        /// 狀態參數X
        /// </summary>
        public int n_STATUS_X;
        /// <summary>
        /// 狀態參數Y
        /// </summary>
        public int n_STATUS_Y;
        /// <summary>
        /// 狀態參數Z
        /// </summary>
        public int n_STATUS_Z;
        /// <summary>
        /// 觸發目標
        /// </summary>
        public int n_STATUS_TARGET;
        /// <summary>
        /// 持續時間
        /// </summary>
        public int n_DURATION;
        /// <summary>
        /// 技能連結
        /// </summary>
        public int n_SKILL_LINK;

        /// <summary>
        /// 發動特效
        /// </summary>
        public string s_EFFECT_USE;

        /// <summary>
        /// 投射物
        /// </summary>
        public string s_PROJECTILE;

        /// <summary>
        /// 命中特效
        /// </summary>
        public string s_EFFECT_HIT;
	

    }

    /// <summary>
    /// 互動表
    /// </summary>
    public class Interactive : ConstDataObject
    {
        /// <summary>
        /// 觸發條件
        /// </summary>
        public InteractiveTrigger n_TRIGGER;

        /// <summary>
        /// 觸發卡片ID(1~3)
        /// </summary>
        public int[] n_CARDID;
        	
        /// <summary>
        /// 觸發好感度下限
        /// </summary>
        public int n_FAVOR_MIN;
        /// <summary>
        /// 觸發好感度上限
        /// </summary>
        public int n_FAVOR_MAX;

        /// <summary>
        /// 前置任務
        /// </summary>
        public int n_PRE_QUEST;
        /// <summary>
        /// 觸發任務
        /// </summary>
        public int n_QUEST;
        /// <summary>
        /// 觸發回合
        /// </summary>
        public int n_TURN;
        /// <summary>
        /// 觸發機率權值
        /// </summary>
        public int n_RATE;
        /// <summary>
        /// 呼叫事件
        /// </summary>
        public int n_CALL_EVENT;
        /// <summary>
        /// 事件回顧
        /// </summary>
        public int n_RECALL;

    }
									
    /// <summary>
    /// 事件表
    /// </summary>
    public class EventShow : ConstDataObject
    {
        /// <summary>
        /// 事件群組
        /// </summary>
        public int n_GROUP;
        /// <summary>
        /// 背景編碼
        /// </summary>
        public string s_BG;
        /// <summary>
        /// BGM編碼
        /// </summary>
        public string s_BGM;

        /// <summary>
        /// 立繪角色(1~3)
        /// </summary>
        public int[] n_CHARA;

        /// <summary>
        /// 立繪位置X(1~3)
        /// </summary>
        public int[] n_POSITIONX;

        /// <summary>
        /// 立繪位置Y(1~3)
        /// </summary>
        public int[] n_POSITIONY;

        /// <summary>
        /// 立繪表情(1~3)
        /// </summary>
        public int[] n_FACE;

        /// <summary>
        /// 立繪翻轉(1~3)
        /// </summary>
        public int[] n_FLIP;

        /// <summary>
        /// 立繪比例(1~3)
        /// </summary>
        public int[] n_SIZE;

        /// <summary>
        /// 主話者
        /// </summary>
        public int n_TALK;

        /// <summary>
        /// 震動時間
        /// </summary>
        public float f_QUAKE;

        /// <summary>
        /// 三擇事件(1~3)
        /// </summary>
        public int[] n_CHOICE;
        
        /// <summary>
        /// 事件結果
        /// </summary>
        public EventResult n_RESULT;
        /// <summary>
        /// 事件參數X
        /// </summary>
        public int n_RESULT_X;
        /// <summary>
        /// 事件參數Y	
        /// </summary>
        public int n_RESULT_Y;

        internal class Reader : ICustomObjectReader
        {
            private EventShow obj;
            public ConstDataObject CreateNewObj()
            {
                obj = new EventShow();
                obj.n_CHARA = new int[3];
                obj.n_POSITIONX = new int[3];
                obj.n_POSITIONY = new int[3];
                obj.n_FACE = new int[3];
                obj.n_FLIP = new int[3];
                obj.n_SIZE = new int[3];
                obj.n_CHOICE = new int[3];

                return obj;
            }

            public bool SetValue(System.IO.BinaryReader reader, string column)
            {
                switch (column)
                {
                    case "n_ID": obj.n_ID = reader.ReadInt32(); break;
                    case "n_GROUP": obj.n_GROUP = reader.ReadInt32(); break;
                    case "s_BG": obj.s_BG = ConstDataReader.ReadString(reader); break;
                    case "s_BGM": obj.s_BGM = ConstDataReader.ReadString(reader); break;
                    case "n_TALK": obj.n_TALK = reader.ReadInt32(); break;
                    case "f_QUAKE": obj.f_QUAKE = reader.ReadSingle(); break;
                    case "n_RESULT": obj.n_RESULT = (EventResult)reader.ReadInt32(); break;
                    case "n_RESULT_X": obj.n_RESULT_X = reader.ReadInt32(); break;
                    case "n_RESULT_Y": obj.n_RESULT_Y = reader.ReadInt32(); break;

                    case "n_CHARA_1": obj.n_CHARA[0] = reader.ReadInt32(); break;
                    case "n_CHARA_2": obj.n_CHARA[1] = reader.ReadInt32(); break;
                    case "n_CHARA_3": obj.n_CHARA[2] = reader.ReadInt32(); break;
                    case "n_POSITIONX_1": obj.n_POSITIONX[0] = reader.ReadInt32(); break;
                    case "n_POSITIONX_2": obj.n_POSITIONX[1] = reader.ReadInt32(); break;
                    case "n_POSITIONX_3": obj.n_POSITIONX[2] = reader.ReadInt32(); break;
                    case "n_POSITIONY_1": obj.n_POSITIONY[0] = reader.ReadInt32(); break;
                    case "n_POSITIONY_2": obj.n_POSITIONY[1] = reader.ReadInt32(); break;
                    case "n_POSITIONY_3": obj.n_POSITIONY[2] = reader.ReadInt32(); break;
                    case "n_FACE_1": obj.n_FACE[0] = reader.ReadInt32(); break;
                    case "n_FACE_2": obj.n_FACE[1] = reader.ReadInt32(); break;
                    case "n_FACE_3": obj.n_FACE[2] = reader.ReadInt32(); break;
                    case "n_FLIP_1": obj.n_FLIP[0] = reader.ReadInt32(); break;
                    case "n_FLIP_2": obj.n_FLIP[1] = reader.ReadInt32(); break;
                    case "n_FLIP_3": obj.n_FLIP[2] = reader.ReadInt32(); break;
                    case "n_SIZE_1": obj.n_SIZE[0] = reader.ReadInt32(); break;
                    case "n_SIZE_2": obj.n_SIZE[1] = reader.ReadInt32(); break;
                    case "n_SIZE_3": obj.n_SIZE[2] = reader.ReadInt32(); break;
                    case "n_CHOICE_1": obj.n_CHOICE[0] = reader.ReadInt32(); break;
                    case "n_CHOICE_2": obj.n_CHOICE[1] = reader.ReadInt32(); break;
                    case "n_CHOICE_3": obj.n_CHOICE[2] = reader.ReadInt32(); break;
                    default:
                        return false;

                }

                return true;
            }

            public void Clear()
            {
                obj = null;
            }
        }

        public static EventShow Create(Mikan.ConstData row)
        {
            var obj = new EventShow();
            obj.n_ID = row.Field<int>("n_ID");
            obj.n_GROUP = row.Field<int>("n_GROUP");
            obj.s_BG = row.Field<string>("s_BG");
            obj.s_BGM = row.Field<string>("s_BGM");
            obj.n_CHARA = ConvertArray<int>(row, "n_CHARA_1", "n_CHARA_2", "n_CHARA_3");
            obj.n_POSITIONX = ConvertArray<int>(row, "n_POSITIONX_1", "n_POSITIONX_2", "n_POSITIONX_3");
            obj.n_POSITIONY = ConvertArray<int>(row, "n_POSITIONY_1", "n_POSITIONY_2", "n_POSITIONY_3");
            obj.n_FACE = ConvertArray<int>(row, "n_FACE_1", "n_FACE_2", "n_FACE_3");
            obj.n_FLIP = ConvertArray<int>(row, "n_FLIP_1", "n_FLIP_2", "n_FLIP_3");
            obj.n_SIZE = ConvertArray<int>(row, "n_SIZE_1", "n_SIZE_2", "n_SIZE_3");
            obj.n_TALK = row.Field<int>("n_TALK");
            obj.f_QUAKE = row.Field<float>("f_QUAKE");
            obj.n_CHOICE = ConvertArray<int>(row, "n_CHOICE_1", "n_CHOICE_2", "n_CHOICE_3");
            obj.n_RESULT = (EventResult)row.Field<int>("n_RESULT");
            obj.n_RESULT_X = row.Field<int>("n_RESULT_X");
            obj.n_RESULT_Y = row.Field<int>("n_RESULT_Y");
            return obj;
        }
    }

    /// <summary>
    /// 好感表
    /// </summary>
    public class Favor : ConstDataObject
    {
        /// <summary>
        /// 類型
        /// </summary>
        public int n_TYPE;
        /// <summary>
        /// 好感度群組
        /// </summary>
        public int n_FAVOR;
        /// <summary>
        /// 精靈等級
        /// </summary>
        public int n_LV;
        /// <summary>
        /// 語音編碼
        /// </summary>
        public string s_VOICE;
        /// <summary>
        /// ???
        /// </summary>
        public int n_BONUS;
    }

    /// <summary>
    /// 任務設定
    /// </summary>
    public class QuestDesign : ConstDataObject
    {
        /// <summary>
        /// 任務類型
        /// </summary>
        public QuestType n_QUEST_TYPE;
        
        /// <summary>
        /// 章節
        /// </summary>
        public int n_CHAPTER;
        
        /// <summary>
        /// 難度
        /// </summary>
        public int n_DIFFICULTY;

        /// <summary>
        /// 主任務
        /// </summary>
        public int n_MAIN_QUEST;

        /// <summary>
        /// 主任務排序
        /// </summary>
        public int n_MAIN_SORT;

        /// <summary>
        /// 座標X
        /// </summary>
        public int n_POS_X;

        /// <summary>
        /// 座標Y
        /// </summary>
        public int n_POS_Y;

        /// <summary>
        /// 子任務排序
        /// </summary>
        public int n_SUB_SORT;
        
        /// <summary>
        /// 背景
        /// </summary>
        public string s_BG;

        /// <summary>
        /// 難度標示
        /// </summary>
        public int n_LEVEL;

        /// <summary>
        /// 子任務回合
        /// </summary>
        public int n_STEP;

        /// <summary>
        /// 前置任務
        /// </summary>
        public int n_PRE_QUEST;

        /// <summary>
        /// 每日任務
        /// </summary>
        public int n_RESET;

        /// <summary>
        /// 任務事件
        /// </summary>
        public int n_EVENT;

        /// <summary>
        /// 獲得EXP
        /// </summary>
        public int n_CLEAR_EXP;

        /// <summary>
        /// 獲得RANKEXP
        /// </summary>
        public int n_CLEAR_RANKEXP;
        
        /// <summary>
        /// 羈絆值
        /// </summary>
        public int n_KIZUNA;

        /// <summary>
        /// 掉落物
        /// </summary>
        public int n_DROP;

        /// <summary>
        /// 持續時間(分)
        /// </summary>
        public int n_DURATION;

        /// <summary>
        /// 挑戰次數
        /// </summary>
        public int n_COUNT;

        /// <summary>
        /// 版面配置
        /// </summary>
        public int n_BOARD;

        /// <summary>
        /// 活動積分
        /// </summary>
        public int n_POINT;

        /// <summary>
        /// 探索值
        /// </summary>
        public int n_DISCOVER;

        /// <summary>
        /// 觸發關卡機率
        /// </summary>
        public int n_SPSTAGE_RATE;

        /// <summary>
        /// 觸發關卡ID
        /// </summary>
        public int n_SPSTAGE_ID;

        /// <summary>
        /// 復活
        /// </summary>
        public int n_REVIVE;
    }


    /// <summary>
    /// 任務事件
    /// </summary>
    public class QuestEvent : ConstDataObject
    {
        /// <summary>
        /// 事件類型
        /// </summary>
        public enum Type
        {
            /// <summary>
            /// 約會
            /// </summary>
            DATE = 1,
            /// <summary>
            /// 戰鬥
            /// </summary>
            BATTLE = 2,
            /// <summary>
            /// 攻略
            /// </summary>
            QUIZ = 3
        }
        /// <summary>
        /// 事件群組
        /// </summary>
        public int n_GROUP;
        /// <summary>
        /// 指定關卡回合
        /// </summary>
        public int n_EVENT_STEP;
        /// <summary>
        /// 事件類型
        /// </summary>
        public Type n_TYPE;
        /// <summary>
        /// 事件權值
        /// </summary>
        public int n_VALUE;

        /// <summary>
        /// 事件參數1~6
        /// </summary>
        public int[] n_VALUE_;

        /// <summary>
        /// 事件位置1~6
        /// </summary>
        public int[] n_POS;
    }

    /// <summary>
    /// 協力任務
    /// </summary>
    public class CoMission : ConstDataObject
    {
        /// <summary>
        /// 任務群組
        /// </summary>
        public int n_GROUP;
        /// <summary>
        /// 圖像ID
        /// </summary>
        public int n_CARDID;
        /// <summary>
        /// 達成值
        /// </summary>
        public int n_GOAL;
        /// <summary>
        /// 項目群組
        /// </summary>
        public int n_GROUP2;
        /// <summary>
        /// 類型
        /// </summary>
        public MissionType n_TYPE;
        /// <summary>
        /// 限制屬性、類型
        /// </summary>
        public int[] n_TYPE_;

        /// <summary>
        /// 限制ID1~2
        /// </summary>
        public int[] n_ID_;
        /// <summary>
        /// 條件變數1~2
        /// </summary>
        public int[] n_CONDITION;

        /// <summary>
        /// 貢獻值
        /// </summary>
        public int n_EFF;
        /// <summary>
        /// 上限
        /// </summary>
        public int n_MAX;
    }

    /// <summary>
    /// 版面配置
    /// </summary>
    public class BoardSetup : ConstDataObject
    {
        /// <summary>
        /// 配置群組
        /// </summary>
        public int n_GROUP;
        /// <summary>
        /// 版類型
        /// </summary>
        public int n_TYPE;
        /// <summary>
        /// 版數量
        /// </summary>
        public int n_COUNT;
        /// <summary>
        /// 是否丟棄
        /// </summary>
        public int n_DROP;
    }

    /// <summary>
    /// 掉寶
    /// </summary>
    public class Drop : ConstDataObject
    {
        /// <summary>
        /// 掉落群組
        /// </summary>
        public int n_GROUP;
        /// <summary>
        /// 掉落類型
        /// </summary>
        public DropType n_DROP_TYPE;
        /// <summary>
        /// 掉落ID
        /// </summary>
        public int n_DROP_ID;
        /// <summary>
        /// 掉落數量
        /// </summary>
        public int n_COUNT;
        /// <summary>
        /// 卡片星級
        /// </summary>
        public int n_RARE;
        /// <summary>
        /// 卡片神運
        /// </summary>
        public int n_LUCK;
        /// <summary>
        /// 機率權值
        /// </summary>
        public int n_VALUE;
    }

    /// <summary>
    /// 道具表
    /// </summary>
    public class Item : ConstDataObject
    {
        /// <summary>
        /// 道具類型
        /// </summary>
        public ItemType n_TYPE;
        /// <summary>
        /// 道具名稱
        /// </summary>
        public string s_NAME;
        /// <summary>
        /// ICON
        /// </summary>
        public string s_ICON;
        /// <summary>
        /// 稀有度
        /// </summary>
        public int n_RARE;
        /// <summary>
        /// HP下限
        /// </summary>
        public int n_HP_MIN;
        /// <summary>
        /// HP上限
        /// </summary>
        public int n_HP_MAX;
        /// <summary>
        /// ATK下限
        /// </summary>
        public int n_ATK_MIN;
        /// <summary>
        /// ATK上限
        /// </summary>
        public int n_ATK_MAX;
        /// <summary>
        /// REV下限
        /// </summary>
        public int n_REV_MIN;
        /// <summary>
        /// REV上限
        /// </summary>
        public int n_REV_MAX;
        /// <summary>
        /// CHG出現率
        /// </summary>
        public int n_CHG_RATE;
        /// <summary>
        /// CHG下限
        /// </summary>
        public int n_CHG_MIN;
        /// <summary>
        /// CHG上限
        /// </summary>
        public int n_CHG_MAX;

        /// <summary>
        /// 剋制屬性機率
        /// </summary>
        public int n_COUNTER_RATE;
        /// <summary>
        /// 出現屬性類型
        /// </summary>
        public int n_COUNTER_TYPE;
        /// <summary>
        /// 剋制下限
        /// </summary>
        public int n_COUNTER_MIN;
        /// <summary>
        /// 剋制上限
        /// </summary>
        public int n_COUNTER_MAX;

        /// <summary>
        /// 特性類型
        /// </summary>
        public ItemEffect n_EFFECT;
        /// <summary>
        /// 特性參數X
        /// </summary>
        public int n_EFFECT_X;
        /// <summary>
        /// 特性參數Y
        /// </summary>
        public int n_EFFECT_Y;

        /// <summary>
        /// 專屬角色
        /// </summary>
        public int n_CARD;
        /// <summary>
        /// 特性2類型
        /// </summary>
        public ItemEffect n_EFFECT2;
        /// <summary>
        /// 特性2參數X
        /// </summary>
        public int n_EFFECT2_X;
        /// <summary>
        /// 特性2參數Y
        /// </summary>
        public int n_EFFECT2_Y;

        /// <summary>
        /// 好感度
        /// </summary>
        public int n_FAVOR;
        /// <summary>
        /// 獲得經驗水晶
        /// </summary>
        public int n_EXP_GAIN;

        /// <summary>
        /// 拆除需求貨幣
        /// </summary>
        public DropType n_TAKE_COINID;
        
        /// <summary>
        /// 拆除需求數量
        /// </summary>
        public int n_TAKE_COIN;
    }

    /// <summary>
    /// 覺醒表
    /// </summary>
    public class Awaken : ConstDataObject
    {
        /// <summary>
        /// 主體卡片ID
        /// </summary>
        public int n_CARDID;
        /// <summary>
        /// ICON
        /// </summary>
        public string s_ICON;
        /// <summary>
        /// 覺醒群組
        /// </summary>
        public int n_GROUP;
        /// <summary>
        /// 覺醒等級
        /// </summary>
        public int n_LV;
        /// <summary>
        /// 對應星級
        /// </summary>
        public int n_RARE;
        /// <summary>
        /// 需求羈絆值
        /// </summary>
        public int n_KIZUNA;
        /// <summary>
        /// HP提升
        /// </summary>
        public int n_HP;
        /// <summary>
        /// ATK提升
        /// </summary>
        public int n_ATK;
        /// <summary>
        /// REV提升
        /// </summary>
        public int n_REV;
        /// <summary>
        /// 攻擊速度提升	
        /// </summary>
        public int n_CHARGE;

        /// <summary>
        /// 隊長技
        /// </summary>
        public int n_LEADER_SKILL;

        /// <summary>
        /// 特技|必殺技
        /// </summary>
        public int[] n_SKILL;

        /// <summary>
        /// 材料1~5
        /// </summary>
        public int[] n_MATERIAL;

        /// <summary>
        /// 材料1~5數量
        /// </summary>
        public int[] n_COUNT;

    }

    /// <summary>
    /// 轉蛋列表
    /// </summary>
    public class GachaList : ConstDataObject
    {
        
        /// <summary>
        /// ICON編碼
        /// </summary>
        public string s_ICON;

        /// <summary>
        /// 商品頁籤
        /// </summary>
        public GachaListTag n_TAG;

        /// <summary>
        /// 時間限定
        /// </summary>
        public int n_TIME;

        /// <summary>
        /// 貢獻值需求
        /// </summary>
        public int n_DEVOTE;

        /// <summary>
        /// 商品ID
        /// </summary>
        public int n_GOODS;

        /// <summary>
        /// 貨幣類型
        /// </summary>
        public DropType n_COINTYPE;
        /// <summary>
        /// 貨幣ID
        /// </summary>
        public int n_COINID;
        /// <summary>
        /// 貨幣數值
        /// </summary>
        public int n_COIN;

        /// <summary>
        /// 二購貨幣數量
        /// </summary>
        public int n_COIN2;

        /// <summary>
        /// 連抽設定
        /// </summary>
        public int n_CONTI;

        /// <summary>
        /// 轉蛋ID
        /// </summary>
        public int n_GACHAID;
        /// <summary>
        /// 轉蛋名稱索引
        /// </summary>
        public int n_NAMEID;
    }

    /// <summary>
    /// 轉蛋
    /// </summary>
    public class Gacha : ConstDataObject
    {
        /// <summary>
        /// 群組
        /// </summary>
        public int n_GROUP;
        /// <summary>
        /// 卡片id
        /// </summary>
        public int n_CARDID;
        /// <summary>
        /// 稀有度
        /// </summary>
        public int n_RARE;
        /// <summary>
        /// 機率權值
        /// </summary>
        public int n_RATE;
        /// <summary>
        /// 卡片等級下限
        /// </summary>
        public int n_CARDLV_MIN;
        /// <summary>
        /// 卡片等級上限
        /// </summary>
        public int n_CARDLV_MAX;
        /// <summary>
        /// 推薦卡片
        /// </summary>
        public int n_SHOWCARD;
    }

    /// <summary>
    /// 圖鑑表
    /// </summary>
    public class Gallery : ConstDataObject
    {
        /// <summary>
        /// 對應卡片ID
        /// </summary>
        public int n_CARDID;
    }

    /// <summary>
    /// 成就表
    /// </summary>
    public class Trophy : ConstDataObject
    {
        /// <summary>
        /// 成就群組
        /// </summary>
        public TrophyIDSet n_IDSET;
        /// <summary>
        /// 條件敘述類型
        /// </summary>
        public string s_CONDITION_TYPE;
        /// <summary>
        /// 成就類型
        /// </summary>
        public TrophyType n_LEGACY_TYPE;
        /// <summary>
        /// 條件參數1~5
        /// </summary>
        public int[] n_EFFECT;
        /// <summary>
        /// 獎勵連結
        /// </summary>
        public int n_BONUS_LINK;
        /// <summary>
        /// 前置成就
        /// </summary>
        public int n_PREPOSITION;
    }

    public class EventSchedule : ConstDataObject
    {
        /// <summary>
        /// 主任務ID
        /// </summary>
        public int n_QUESTID;

        /// <summary>
        /// 類型
        /// </summary>
        public EventScheduleType n_TYPE;

        /// <summary>
        /// 每周任務
        /// </summary>
        public int n_WEEKLY;

        /// <summary>
        /// 開始日期
        /// </summary>
        public int n_START_DAY;

        /// <summary>
        /// 開始時間
        /// </summary>
        public int n_START_TIME;

        /// <summary>
        /// 持續時間(分)
        /// </summary>
        public int n_DURATION;

        /// <summary>
        /// 積分獎勵
        /// </summary>
        public int n_POINT;

        /// <summary>
        /// 排名獎勵
        /// </summary>
        public int n_RANKING;

        /// <summary>
        /// BANNER顯示
        /// </summary>
        public int n_SHOWBANNER;

        /// <summary>
        /// 主線積分
        /// </summary>
        public int n_MAIN_POINT;
    }

    /// <summary>
    /// 其他設定
    /// </summary>
    public class Setup : ConstDataObject
    {
        /// <summary>
        /// 主項目
        /// </summary>
        public int n_MAIN;
        /// <summary>
        /// 次項目
        /// </summary>
        public int n_SUB;
        /// <summary>
        /// 子項目
        /// </summary>
        public int n_EVENT;
        /// <summary>
        /// 功能選單
        /// </summary>
        public SetupFunction n_FUNCTION;
        /// <summary>
        /// 轉蛋公告
        /// </summary>
        public int n_GACHA;
        /// <summary>
        /// 下載訊息
        /// </summary>
        public int n_WAITING;

        public string s_STRING;
        public string s_STRING2;

    }

    /// <summary>
    /// 教學
    /// </summary>
    public class Tutorial : ConstDataObject
    {
        /// <summary>
        /// 場景ID
        /// </summary>
        public int n_SCENEID;
        /// <summary>
        /// 頁數
        /// </summary>
        public int n_PAGE;
        /// <summary>
        /// 圖檔名
        /// </summary>
        public string s_FILENAME;

    }

    /// <summary>
    /// 信仰融合
    /// </summary>
    public class FaithResult : ConstDataObject
    {
        /// <summary>
        /// 機率權值
        /// </summary>
        public int n_RATE;

        /// <summary>
        /// 道具1~4數量下限
        /// </summary>
        public int[] n_ITEMMIN;

        /// <summary>
        /// 道具1~4數量上限
        /// </summary>
        public int[] n_ITEMMAX;

        /// <summary>
        /// 所需時間(分)
        /// </summary>
        public int n_TIME;
        /// <summary>
        /// 結果
        /// </summary>
        public int n_RESULT;
    }

    public class Tactics : ConstDataObject
    {
        /// <summary>
        /// 群組
        /// </summary>
        public int n_GROUP;
        /// <summary>
        /// 類型
        /// </summary>
        public int n_TYPE;
        /// <summary>
        /// 等級
        /// </summary>
        public int n_LV;
        /// <summary>
        /// ICON
        /// </summary>
        public string s_ICON;
        /// <summary>
        /// 需求RANK
        /// </summary>
        public int n_RANK;
        /// <summary>
        /// 效果
        /// </summary>
        public TacticsEffect n_EFFECT;
        /// <summary>
        /// 效果參數X
        /// </summary>
        public int n_EFFECT_X;
        /// <summary>
        /// 效果參數Y
        /// </summary>
        public int n_EFFECT_Y;
        /// <summary>
        /// 效果參數Z
        /// </summary>
        public int n_EFFECT_Z;
        /// <summary>
        /// 材料1~3
        /// </summary>
        public int[] n_MATERIAL;
        /// <summary>
        /// 材料1~3數量
        /// </summary>
        public int[] n_COUNT;

    }

    public class Challenge : ConstDataObject
    {
        /// <summary>
        /// 子關卡ID
        /// </summary>
        public int n_STAGEID;
        /// <summary>
        /// 排序
        /// </summary>
        public int n_SORT;
        /// <summary>
        /// 類型
        /// </summary>
        public int n_TYPE;
        /// <summary>
        /// 條件參數X Y Z
        /// </summary>
        public int[] n_TYPE_;

    }


#if SERVER_CODE
    public class IAPList : ConstDataObject
    {
        /// <summary>
        /// 對應平台
        /// </summary>
        public int n_PLATFORM;
        /// <summary>
        /// 關鍵字串
        /// </summary>
        public string s_KEY;
        /// <summary>
        /// 獲得神石
        /// </summary>
        public int n_GEM;
        /// <summary>
        /// 首儲優惠
        /// </summary>
        public int n_GEM_BONUS;
        /// <summary>
        /// 價格
        /// </summary>
        public int n_PRICE;
        /// <summary>
        /// 文字
        /// </summary>
        public int n_NAME;
        /// <summary>
        /// 文字(價格)
        /// </summary>
        public int n_NAME_PRICE;
    }

    public class MyCardBonus : ConstDataObject
    {
        /// <summary>
        /// 活動代碼
        /// </summary>
        public string s_CODE;

        /// <summary>
        /// 回饋神石
        /// </summary>
        public int n_FEEDBACK;
	

    }

    public class SerialNumber : ConstDataObject
    {
        /// <summary>
        /// 群組
        /// </summary>
        public string s_GROUP;

        /// <summary>
        /// 序號
        /// </summary>
        public string s_NUMBER;

        /// <summary>
        /// 獎勵ID
        /// </summary>
        public int n_TROPHY;
    }

    public class Dummy : ConstDataObject
    {

        public string s_PLAYERNAME;


        public int n_LEADER;
    }
#endif

    #endregion

    #region TEXT_DATA

    /// <summary>
    /// 文字
    /// </summary>
    public class SystemText : ConstDataObject
    {
        /// <summary>
        /// 系統訊息
        /// </summary>
        public string s_TEXT;
    }

    /// <summary>
    /// 卡片文字
    /// </summary>
    public class CardText : ConstDataObject
    {
        /// <summary>
        /// 卡片名稱
        /// </summary>
        public string s_NAME;
        /// <summary>
        /// CUTIN台詞
        /// </summary>
        public string s_CUTIN_WORD;
    }

    /// <summary>
    /// 角色文字
    /// </summary>
    public class CharaText : ConstDataObject
    {
        /// <summary>
        /// 角色名稱
        /// </summary>
        public string s_TEXT;

    }

    /// <summary>
    /// 技能文字
    /// </summary>
    public class SkillText : ConstDataObject
    {
        /// <summary>
        /// 技能名稱
        /// </summary>
        public string s_NAME;
        /// <summary>
        /// 技能說明
        /// </summary>
        public string s_TIP;
        /// <summary>
        /// 技能簡稱
        /// </summary>
        public string s_SHORT_NAME;
    }

    /// <summary>
    /// 事件名稱
    /// </summary>
    public class EventName : ConstDataObject
    {
        /// <summary>
        /// 名稱
        /// </summary>
        public string s_NAME;
    }
    /// <summary>
    /// 事件文字
    /// </summary>
    public class EventShowText : ConstDataObject
    {
        /// <summary>
        /// 對話框顯示文字
        /// </summary>
        public string s_TEXT;

        /*
        /// <summary>
        /// 選項一顯示文字
        /// </summary>
        public string s_CHOICETEXT_1;
        /// <summary>
        /// 選項二顯示文字
        /// </summary>
        public string s_CHOICETEXT_2;
        /// <summary>
        /// 選項三顯示文字
        /// </summary>
        public string s_CHOICETEXT_3;
        */

        /// <summary>
        /// 選項顯示文字(1~3)
        /// </summary>
        public string[] s_CHOICETEXT;

        public static EventShowText Create(Mikan.ConstData row)
        {
            var obj = new EventShowText();
            obj.n_ID = row.Field<int>("n_ID");
            obj.s_TEXT = row.Field<string>("s_TEXT");
            obj.s_CHOICETEXT = ConvertArray<string>(row, "s_CHOICETEXT_1", "s_CHOICETEXT_2", "s_CHOICETEXT_3");
            return obj;
        }

        internal class Reader : ICustomObjectReader
        {
            private EventShowText obj;
            public ConstDataObject CreateNewObj()
            {
                obj = new EventShowText();
                obj.s_CHOICETEXT = new string[3];

                return obj;
            }

            public bool SetValue(System.IO.BinaryReader reader, string column)
            {
                switch (column)
                {
                    case "n_ID": obj.n_ID = reader.ReadInt32(); break;
                    case "s_TEXT": obj.s_TEXT = ConstDataReader.ReadString(reader); break;

                    case "s_CHOICETEXT_1": obj.s_CHOICETEXT[0] = ConstDataReader.ReadString(reader); break;
                    case "s_CHOICETEXT_2": obj.s_CHOICETEXT[1] = ConstDataReader.ReadString(reader); break;
                    case "s_CHOICETEXT_3": obj.s_CHOICETEXT[2] = ConstDataReader.ReadString(reader); break;
                    default:
                        return false;

                }

                return true;
            }

            public void Clear()
            {
                obj = null;
            }
        }
    }

    /// <summary>
    /// 好感度文字
    /// </summary>
    public class FavorText : ConstDataObject
    {
        /// <summary>
        /// 名稱
        /// </summary>
        public string s_NAME;
    }

    /// <summary>
    /// 任務名稱
    /// </summary>
    public class QuestName : ConstDataObject
    {
        /// <summary>
        /// 主任務名稱
        /// </summary>
        public string s_MAIN_NAME;
        /// <summary>
        /// 子任務
        /// </summary>
        public string s_SUB_NAME;
    }

    /// <summary>
    /// 道具文字
    /// </summary>
    public class ItemText : ConstDataObject
    {
        /// <summary>
        /// 道具名稱
        /// </summary>
        public string s_NAME;
        /// <summary>
        /// 特性說明
        /// </summary>
        public string s_SP;
        /// <summary>
        /// 特性說明
        /// </summary>
        public string s_SP2;
    }

    /// <summary>
    /// 覺醒文字
    /// </summary>
    public class AwakenText : ConstDataObject
    {
        /// <summary>
        /// 覺醒名稱
        /// </summary>
        public string s_NAME;

        public string s_TEXT;
    }

    /// <summary>
    /// 錯誤訊息
    /// </summary>
    public class ErrorCode : ConstDataObject
    {
        /// <summary>
        /// 回饋訊息
        /// </summary>
        public string s_TEXT;
    }

    /// <summary>
    /// 成就文字
    /// </summary>
    public class TrophyText : ConstDataObject
    {
        /// <summary>
        /// 成就名稱
        /// </summary>
        public string s_NAME;
    }

    /// <summary>
    /// 其他設定文字
    /// </summary>
    public class SetupText : ConstDataObject
    {
        /// <summary>
        /// 主項目
        /// </summary>
        public string s_MAIN_TEXT;
        /// <summary>
        /// 次項目
        /// </summary>
        public string s_SUB_TEXT;
        /// <summary>
        /// 子項目
        /// </summary>
        public string s_EVENT_TEXT;
        /// <summary>
        /// 功能選單
        /// </summary>
        public string s_FUNCTION_TEXT;
    }

    /// <summary>
    /// 成就名稱
    /// </summary>
    public class MobSpeak : ConstDataObject
    {
        /// <summary>
        /// 發話內容
        /// </summary>
        public string s_TEXT;

    }

    /// <summary>
    /// 怪物發話
    /// </summary>
    public class MissionText : ConstDataObject
    {
        public string s_NAME;

        public string s_TALK;

        public string s_TIP;
    }

    /// <summary>
    /// 戰術
    /// </summary>
    public class TacticsText : ConstDataObject
    {
        /// <summary>
        /// 戰術名稱
        /// </summary>
        public string s_NAME;
        /// <summary>
        /// 戰術說明
        /// </summary>
        public string s_TEXT;
    }

    /// 挑戰文字
    /// </summary>
    public class ChallengeText : ConstDataObject
    {
        /// <summary>
        /// 挑戰文字
        /// </summary>
        public string s_TEXT;
    }

    #endregion

    public class Tables
    {
        /// <summary>
        /// 隨機戰友數量
        /// </summary>
        public int FellowCount { get { return 5; } }

        public static Tables Instance { get; private set; }

        public ConstDataTable<PlayerData> Player { get; private set; }
        public ConstDataTable<CardData> Card { get; private set; }
        public ConstDataTable<CardBalance> CardBalance { get; private set; }
        public ConstDataTable<CardText> CardText { get; private set; }
        public ConstDataTable<Item> Item { get; private set; }
        public ConstDataTable<Variable> Variable { get; private set; }
        public ConstDataTable<QuestDesign> QuestDesign { get; private set; }
        public ConstDataTable<QuestEvent> QuestEvent { get; private set; }
        public ConstDataTable<Drop> Drop { get; private set; }
        public ConstDataTable<QuestName> QuestName { get; private set; }
        public ConstDataTable<EventShow> EventShow { get; private set; }
        public ConstDataTable<EventName> EventName { get; private set; }
        public ConstDataTable<EventShowText> EventShowText { get; private set; }
        public ConstDataTable<Interactive> Interactive { get; private set; }
        public ConstDataTable<Mob> Mob { get; private set; }
        public ConstDataTable<MobAI> MobAI { get; private set; }
        public ConstDataTable<MobSpeak> MobSpeak { get; private set; }
        public ConstDataTable<Skill> Skill { get; private set; }
        public ConstDataTable<SkillText> SkillText { get; private set; }
        public ConstDataTable<ItemText> ItemText { get; private set; }
        public ConstDataTable<Trophy> Trophy { get; private set; }
        public ConstDataTable<TrophyText> TrophyText { get; private set; }
        public ConstDataTable<GachaList> GachaList { get; private set; }
        public ConstDataTable<Gacha> Gacha { get; private set; }
        public ConstDataTable<SystemText> SystemText { get; private set; }
        public ConstDataTable<Awaken> Awaken { get; private set; }
        public ConstDataTable<AwakenText> AwakenText { get; private set; }

        public ConstDataTable<BoardSetup> BoardSetup { get; private set; }
        public ConstDataTable<Setup> Setup { get; private set; }
        public ConstDataTable<SetupText> SetupText { get; private set; }
        public ConstDataTable<EventSchedule> EventSchedule { get; private set; }
        public ConstDataTable<Tutorial> Tutorial { get; private set; }
        public ConstDataTable<FaithResult> FaithResult { get; private set; }
        public ConstDataTable<Tactics> Tactics { get; private set; }
        public ConstDataTable<TacticsText> TacticsText { get; private set; }
        public ConstDataTable<Challenge> Challenge { get; private set; }
        public ConstDataTable<ChallengeText> ChallengeText { get; private set; }

        public ConstDataTable<ErrorCode> ErrorCode { get; private set; }

        public ConstDataTable<CoMission> CoMission { get; private set; }
        public ConstDataTable<MissionText> MissionText { get; private set; }
#if SERVER_CODE
        public ConstDataTable<IAPList> IAPList { get; private set; }
        public ConstDataTable<MyCardBonus> MyCardBonus { get; private set; }
        public ConstDataTable<SerialNumber> SerialNumber { get; private set; }
        public ConstDataTable<Dummy> Dummy { get; private set; }
        public List<int> DefaultPVPSkills { get; private set; }
#endif
        public float ExpLvUpStyle { get; private set; }
        public float BasicAspd { get; private set; }
        public float ExpTransRate { get; private set; }

        public int TalkPointScale { get; private set; }

        public int TalkPointLimit { get; private set; }

        public int TalkKizuna { get; private set; }

        public int KizunaLimit { get; private set; }

        public float GiftBonus { get; private set; }

        public int ItemLimit { get { return int.MaxValue; } }

        public int PlayerLVMax { get; private set; }

        public int PlayerExpMax { get; private set; }

        public int CoinMax { get; private set; }
        public int CrystalMax { get; private set; }
        public int GemMax { get; private set; }
        public int FPMax { get; private set; }

        public int CardSpaceBase { get; private set; }
        public int CardSpaceLimit { get; private set; }
        public int CardSpaceAdd { get; private set; }
        public int CardSpaceCost { get; private set; }

        public int ChargeTPCost { get; private set; }
        public int ChargeQuestCost { get; private set; }
        public int ContinueCost { get; private set; }
        public int ChargeRescueCost { get; private set; }
        public int ChargeCrusadeCost { get; private set; }


        public int AwakenCount { get { return 3; } }

        public int TutorialQuestID { get { return 1; } }
        public int TutorialEventID1 { get { return 390501; } }
        public int TutorialEventID2 { get { return 391001; } }

        public int FirstGachaID { get { return 2; } }

        public List<int> RegularGachaIDs { get; private set; }
        public List<int> ActivityGachaIDs { get; private set; }

        public int DailyActiveCount { get; private set; }

        public int DailyActivePairCount { get { return 8; } }

        public int DailyActiveDropGroup { get { return 981001; } }
        public int DailyActiveResidentDropGroup { get { return 980001; } }

        public int DailyCounterTrophyID { get { return (int)TrophyIDSet.LOGIN_COUNTER; } }

        public int DailyCounterLastTrophyID { get; private set; }

        public int DailyFavoriteKizuna { get; private set; }

        public int RandomShopRefreshTime { get; private set; }

        public int RandomShopRefreshItemID { get { return 501; } }

        public int RescueRewardItemID { get { return 606; } }

        public int LuckItemID { get { return 608; } }

        public int RankingPlayerLimit { get; private set; }

        public int HPPlus { get; private set; }
        public int AtkPlus { get; private set; }
        public int RevPlus { get; private set; }

        public int HelpInital { get; private set; }
        public int HelpAdd { get; private set; }
        public int HelpMax { get; private set; }

        public int HelpScale { get { return 100000; } }

        public int CrusadeInital { get; private set; }
        public int CrusadeAdd { get; private set; }
        public int CrusadeMax { get; private set; }
        public int CrusadeTime { get; private set; }
        public int CrusadeScale { get { return 100000; } }
        public float CrusadeOwner { get; private set; }
        public int CrusadeOwnerLimit { get; private set; }

        public int CoMissionThreshold { get; set; }
        public int CoMissionItemID { get { return 606; } }

        public float TowerCountdown { get; private set; }

        public float TowerMinus { get; private set; }

        public float TowerBonus { get; private set; }

        public int GachaBoost { get; private set; }

        public ArticleCalculator CardSpaceCalc { get; private set; }

        public ArticleCalculator RandomShopSlotCalc { get; private set; }

        public Dictionary<int, List<Trophy>> RankingPeriods;

        public int DailyFansTrophyID { get; private set; }

        public List<int> FaithItemIDs { get; private set; }

        public List<int> FaithThresholds { get; private set; }

        public int FaithMax { get { return 99; } }

        public int GachaGiftID { get { return 612; } }

        public int PVPQuestID { get { return 999000; } }
        public int PVPMainQuestID { get { return 99900; } }
        public int PVPBannerID { get { return 90003; } }
        public int PVPBasePoint { get; private set; }
        public int PVPExtraPoint { get; private set; }

        public int BPLimit { get; private set; }
        public int BPCD { get; private set; }
        public int PVPSkillItemID { get; private set; }

        public static ConstDataReader CreateReader()
        {
            var reader = new ConstDataReader();
            reader.Register("EVENT_SHOW", new Mikan.CSAGA.ConstData.EventShow.Reader());
            reader.Register("EVENT_SHOW_TEXT", new Mikan.CSAGA.ConstData.EventShowText.Reader());
            return reader;
        }

        public static Tables Create(ConstDataProvider provider)
        {
            var inst = new Tables();
            inst.ReadAll(provider);
            Instance = inst;
            return inst;
        }

        public static string Format(string origin, params object[] args)
        {
            var res = origin;
            for (int i = 0; i < args.Length; ++i)
            {
                res = res.Replace(string.Format("$V{0}", i + 1), args[i].ToString());
            }

            return res;
        }

        public float GetVariablef(VariableID id)
        {
            var data = Variable[(int)id];
            if (data != null) return data.f_VALUE_VARIABLE;

            KernelService.Logger.Debug("variable not exists, id = {0}", id);
            return 0;
        }

        public int GetVariable(VariableID id)
        {
            return (int)GetVariablef(id);
        }

        public string GetSystemText(SystemTextID id, params object[] args)
        {
            return GetSystemText((int)id, args);
        }

        public string GetSystemText(int id, params object[] args)
        {
            var data = SystemText[id];
            if (data == null) return string.Format("TEXT({0})", id);
            return Format(data.s_TEXT, args);
        }

        public string GetErrorText(Mikan.CSAGA.ErrorCode errCode)
        {
            var data = ErrorCode[(int)errCode];
            if (data == null) data = ErrorCode[1];
            return ConstData.Tables.Format(data.s_TEXT, (int)errCode, errCode);
        }

        private static ConstDataTable<T> SafeConvert<T>(ConstDataProvider provider, string tableName)
            where T : ConstDataObject, new()
        {
#if PROFILE
            var beginTime = DateTime.Now;
#endif
            var table = provider.GetTable(tableName);
            if (table == null)
            {
                KernelService.Logger.Debug("table not exists, name = {0}", tableName);
                return default(ConstDataTable<T>);
            }

            var res = table.Convert<T>();
#if PROFILE
            var cost = DateTime.Now.Subtract(beginTime).TotalMilliseconds;
            var rows = res.Count;
            var perData = cost / rows;
            KernelService.Logger.Debug("load table {0}, rows = {1}, cost = {2}, per_row = {3}", tableName, rows, cost, perData);
#endif

            return res;
        }

        private static ConstDataTable<T> SafeConvert<T>(ConstDataProvider provider, string tableName, ConstDataTable.Converter<T> converter)
            where T : ConstDataObject, new()
        {
#if PROFILE
            var beginTime = DateTime.Now;
#endif
            var table = provider.GetTable(tableName);
            if (table == null)
            {
                KernelService.Logger.Debug("table not exists, name = {0}", tableName);
                return default(ConstDataTable<T>);
            }

            var res = table.Convert<T>(converter);
#if PROFILE
            var cost = DateTime.Now.Subtract(beginTime).TotalMilliseconds;
            var rows = res.Count;
            var perData = cost / rows;
            KernelService.Logger.Debug("load table {0}, rows = {1}, cost = {2}, per_row = {3}", tableName, rows, cost, perData);
#endif

            return res;
        }

        private void ReadAll(ConstDataProvider provider)
        {
            Player = SafeConvert<PlayerData>(provider, "PLAYER_DATA");
            Card = SafeConvert<CardData>(provider, "CARD_DATA");
            CardBalance = SafeConvert<CardBalance>(provider, "CARD_BALANCE");
            CardText = SafeConvert<CardText>(provider, "CARD_TEXT");
            Item = SafeConvert<Item>(provider, "ITEM");
            Variable = SafeConvert<Variable>(provider, "VARIABLE");
            QuestDesign = SafeConvert<QuestDesign>(provider, "QUEST_DESIGN");
            QuestEvent = SafeConvert<QuestEvent>(provider, "QUEST_EVENT");
            Drop = SafeConvert<Drop>(provider, "DROP");
            QuestName = SafeConvert<QuestName>(provider, "QUEST_NAME");
            EventShow = SafeConvert<EventShow>(provider, "EVENT_SHOW");
            EventShowText = SafeConvert<EventShowText>(provider, "EVENT_SHOW_TEXT");
            EventName = SafeConvert<EventName>(provider, "EVENT_NAME");
            Interactive = SafeConvert<Interactive>(provider, "INTERACTIVE");
            Mob = SafeConvert<Mob>(provider, "MOB");
            MobAI = SafeConvert<MobAI>(provider, "MOB_AI");
            MobSpeak = SafeConvert<MobSpeak>(provider, "MOB_SPEAK");
            Skill = SafeConvert<Skill>(provider, "SKILL");
            SkillText = SafeConvert<SkillText>(provider, "SKILL_TEXT");
            ItemText = SafeConvert<ItemText>(provider, "ITEM_TEXT");
            Trophy = SafeConvert<Trophy>(provider, "TROPHY");
            TrophyText = SafeConvert<TrophyText>(provider, "TROPHY_TEXT");
            GachaList = SafeConvert<GachaList>(provider, "GACHA_LIST");
            Gacha = SafeConvert<Gacha>(provider, "GACHA");
            SystemText = SafeConvert<SystemText>(provider, "SYSTEM_TEXT");
            BoardSetup = SafeConvert<BoardSetup>(provider, "BOARD_SETUP");
            Awaken = SafeConvert<Awaken>(provider, "AWAKEN");
            AwakenText = SafeConvert<AwakenText>(provider, "AWAKEN_TEXT");
            Setup = SafeConvert<Setup>(provider, "SETUP");
            SetupText = SafeConvert<SetupText>(provider, "SETUP_TEXT");
            EventSchedule = SafeConvert<EventSchedule>(provider, "EVENT_SCHEDULE");
            ErrorCode = SafeConvert<ErrorCode>(provider, "ERROR_CODE");
            CoMission = SafeConvert<CoMission>(provider, "CO_MISSION");
            MissionText = SafeConvert<MissionText>(provider, "MISSION_TEXT");

            FaithResult = SafeConvert<FaithResult>(provider, "FAITH_RESULT");
            Tactics = SafeConvert<Tactics>(provider,"TACTICS");
            TacticsText = SafeConvert<TacticsText>(provider, "TACTICS_TEXT");
            Challenge = SafeConvert<Challenge>(provider, "CHALLENGE");
            ChallengeText = SafeConvert<ChallengeText>(provider, "CHALLENGE_TEXT");

            Tutorial = SafeConvert<Tutorial>(provider, "TUTORIAL");

#if SERVER_CODE
            IAPList = SafeConvert<IAPList>(provider, "IAP_LIST");
            MyCardBonus = SafeConvert<MyCardBonus>(provider, "MYCARD_BONUS");
            SerialNumber = SafeConvert<SerialNumber>(provider, "SERIAL_NUMBER");
            Dummy = SafeConvert<Dummy>(provider, "DUMMY");
            DefaultPVPSkills = new List<int>(3);
            DefaultPVPSkills.Add(GetVariable(VariableID.PVP_SKILL_1));
            DefaultPVPSkills.Add(GetVariable(VariableID.PVP_SKILL_2));
            DefaultPVPSkills.Add(GetVariable(VariableID.PVP_SKILL_3));
            DefaultPVPSkills.Add(GetVariable(VariableID.PVP_SKILL_4));
            DefaultPVPSkills.Add(GetVariable(VariableID.PVP_SKILL_5));
            DefaultPVPSkills.Add(GetVariable(VariableID.PVP_SKILL_6));
#endif

            ExpLvUpStyle = GetVariablef(VariableID.EXP_LVUP_STYLE);
            BasicAspd = GetVariablef(VariableID.BASIC_ASPD);
            ExpTransRate = GetVariablef(VariableID.SELL_EXP_GAIN) / 100;

            TalkPointScale = GetVariable(VariableID.COMMU_CD) * 60;
            TalkPointLimit = GetVariable(VariableID.COMMU_LIMIT) * TalkPointScale;
            TalkKizuna = GetVariable(VariableID.COMMU_KIZUNA);
            KizunaLimit = GetVariable(VariableID.KIZUNA_LIMIT);
            GiftBonus = GetVariablef(VariableID.GIFT_BONUS);
            DailyActiveCount = GetVariable(VariableID.ACTIVE_BASIC_SCORE);

            GemMax = GetVariable(VariableID.DIAMOND_MAX);
            CrystalMax = GetVariable(VariableID.EXP_MAX);
            CoinMax = GetVariable(VariableID.TOKEN_MAX);
            FPMax = GetVariable(VariableID.FP_MAX);

            RankingPlayerLimit = GetVariable(VariableID.SHOW_RANKING);

            CoMissionThreshold = GetVariable(VariableID.CO_MISSION_ITEM);

            TowerCountdown = GetVariablef(VariableID.TOWER_COUNTDOWN);
            TowerMinus = GetVariablef(VariableID.TOWER_MINUS);
            TowerBonus = GetVariablef(VariableID.TOWER_BONUS);
            GachaBoost = GetVariable(VariableID.GACHA_BOOST);

            var trophy = Trophy.SelectFirst(o => o.s_CONDITION_TYPE == TrophyConditionType.DAILY_FAVORITE);
            if (trophy != null) DailyFavoriteKizuna = trophy.n_BONUS_LINK;

            trophy = Trophy.SelectFirst(o => o.s_CONDITION_TYPE == TrophyConditionType.DAILY_FP_FANS);

            if (trophy != null) DailyFansTrophyID = trophy.n_ID;

            foreach (var item in Trophy.Select(o => o.n_IDSET == TrophyIDSet.LOGIN_COUNTER))
            {
                if (DailyCounterLastTrophyID < item.n_ID) DailyCounterLastTrophyID = item.n_ID;
            }

            var playerDataMax = default(PlayerData);
            foreach (var item in Player.Select())
            {
                if (playerDataMax == null || playerDataMax.n_ID < item.n_ID) playerDataMax = item;
            }

            PlayerLVMax = playerDataMax.n_ID;
            PlayerExpMax = playerDataMax.n_TOTALEXP;

            foreach (var item in Item.Select(o => { return o.n_TYPE == ItemType.Gift; }))
            {
                HPPlus = Math.Max(HPPlus, item.n_HP_MAX);
                AtkPlus = Math.Max(AtkPlus, item.n_ATK_MAX);
                RevPlus = Math.Max(RevPlus, item.n_REV_MAX);
            }

            HelpInital = GetVariable(VariableID.HELP_INITIAL);
            HelpAdd = GetVariable(VariableID.HELP_ADD);
            HelpMax = GetVariable(VariableID.HELP_MAX);

            CrusadeInital = GetVariable(VariableID.CRUSADE_INITIAL);
            CrusadeAdd = GetVariable(VariableID.CRUSADE_ADD);
            CrusadeMax = GetVariable(VariableID.CRUSADE_MAX);
            CrusadeTime = GetVariable(VariableID.CRUSADE_TIME);
            CrusadeOwner = GetVariablef(VariableID.CRUSADE_OWNER) / 100f;
            CrusadeOwnerLimit = GetVariable(VariableID.CRUSADE_OWNER_LIMIT);

            ContinueCost = GetVariable(VariableID.REVIVE_DIAMOND);
            ChargeTPCost = GetVariable(VariableID.COMMU_COUNT_DIAMOND);
            ChargeQuestCost = GetVariable(VariableID.EVENT_COUNT_DIAMOND);
            ChargeRescueCost = GetVariable(VariableID.HELP_COUNT_DIAMOND);
            ChargeCrusadeCost = GetVariable(VariableID.CRUSADE_COUNT_DIAMOND);

            CardSpaceBase = GetVariable(VariableID.CARD_SPACE_BASE);
            CardSpaceLimit = GetVariable(VariableID.CARD_SPACE_LIMIT);
            CardSpaceAdd = GetVariable(VariableID.CARD_SPACE_ADD);
            CardSpaceCost = GetVariable(VariableID.CARD_SPACE_COST);

            BPLimit = GetVariable(VariableID.PVP_COUNT_LIMIT);
            BPCD = GetVariable(VariableID.PVP_COUNT_CD);

            PVPBasePoint = GetVariable(VariableID.PVP_BASE_POINT);
            PVPExtraPoint = GetVariable(VariableID.PVP_EXTRA_POINT);
            PVPSkillItemID = GetVariable(VariableID.PVP_SKILL_ITEM);

            CardSpaceCalc = new ArticleCalculator(CardSpaceBase, CardSpaceLimit, CardSpaceAdd, CardSpaceCost);

            RandomShopRefreshTime = GetVariable(VariableID.SHOP_UPDATE_TIME);

            RandomShopSlotCalc = new ArticleCalculator(GetVariable(VariableID.SHOP_SLOT_BASIC),
                                                        GetVariable(VariableID.SHOP_SLOT_MAX),
                                                        1,
                                                        GetVariable(VariableID.SHOP_SLOT_DIAMOND));

            RankingPeriods = new Dictionary<int, List<Trophy>>();
            foreach (var item in Trophy.Select(o => { return o.s_CONDITION_TYPE == TrophyConditionType.EVENT_RANKING; }))
            {
                var list = DictionaryUtils.SafeGetValue(RankingPeriods, (int)item.n_IDSET);

                var exists = false;

                foreach (var current in list)
                {
                    if (current.n_EFFECT[0] == item.n_EFFECT[0])
                    {
                        exists = true;
                        break;
                    }
                }

                if(!exists) list.Add(item);
            }

            foreach (var item in RankingPeriods) item.Value.Sort((lhs, rhs) => { return lhs.n_EFFECT[0].CompareTo(rhs.n_EFFECT[0]); });

            RegularGachaIDs = new List<int>(3);
            RegularGachaIDs.Add(11);
            RegularGachaIDs.Add(12);
            RegularGachaIDs.Add(13);

            ActivityGachaIDs = new List<int>(3);
            ActivityGachaIDs.Add(21);
            ActivityGachaIDs.Add(22);
            ActivityGachaIDs.Add(23);

            FaithItemIDs = new List<int>(4);
            FaithItemIDs.Add(101);
            FaithItemIDs.Add(102);
            FaithItemIDs.Add(103);
            FaithItemIDs.Add(104);

            FaithThresholds = new List<int>(4);
            FaithThresholds.Add(GetVariable(VariableID.FAITH_SLOT_1));
            FaithThresholds.Add(GetVariable(VariableID.FAITH_SLOT_2));
            FaithThresholds.Add(GetVariable(VariableID.FAITH_SLOT_3));
            FaithThresholds.Add(GetVariable(VariableID.FAITH_SLOT_4));
        } 
    }
}