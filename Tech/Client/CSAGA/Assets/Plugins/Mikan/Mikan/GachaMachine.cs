﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mikan
{
    public interface IGachaMachine
    {
        int Next();
        bool Jackpot { get; }
    }

    public class GachaMachineFactory
    {
        private Dictionary<int, GachaMachinData> list = new Dictionary<int, GachaMachinData>();

        public void Add(int key, GachaDataProvider provider)
        {
            var item = DictionaryUtils.SafeGetValue(list, key);
            item.DefaultProvider = provider;
            item.Current = null;
        }

        public void AddAppend(int key, GachaDataProvider provider)
        {
            var item = DictionaryUtils.SafeGetValue(list, key);
            item.AppendProvider = provider;
            item.Current = null;
        }

        public IGachaMachine Get(int key)
        {
            GachaMachinData item = null;

            if (!list.TryGetValue(key, out item)) return null;

            if (item.Current == null)
            {
                var machine = new GachaMachine();
                if (item.DefaultProvider != null) machine.Add(item.DefaultProvider);
                if (item.AppendProvider != null) machine.Add(item.AppendProvider);
                item.Current = machine;
            }

            return item.Current;
        }
    }

    public class GachaDataProvider
    {
        class Item
        {
            public int ID;
            public int Rate;
        }

        private List<Item> items;

        public int Basis { get; private set; }

        public bool Jackpot { get; private set; }

        /// <summary>
        /// 加入資料
        /// </summary>
        /// <param name="item"></param>
        /// <param name="rate"></param>
        public void Add(int item, int rate, bool jackpot)
        {
            if (items == null) items = new List<Item>();

            items.Add(new Item { ID = item, Rate = rate });
            Basis += rate;

            if (jackpot) Jackpot = true;
        }

        public int Next(int rand)
        {
            var sum = 0;

            foreach (var item in items)
            {
                sum += item.Rate;
                if (rand < sum) return item.ID;
            }

            throw new InvalidOperationException();
        }
    }

    class GachaMachinData
    {
        public IGachaMachine Current;
        public GachaDataProvider DefaultProvider;
        public GachaDataProvider AppendProvider;
    }

    class GachaMachine : IGachaMachine
    {
        private int basis;
        private List<GachaDataProvider> providers = new List<GachaDataProvider>();
        private Random random;

        public bool Jackpot { get; private set; }

        public GachaMachine()
        {
            random = new Random(MathUtils.Random.Next());
        }

        public void Add(GachaDataProvider provider)
        {
            providers.Add(provider);
            basis += provider.Basis;
            if (provider.Jackpot) Jackpot = true;
        }

        public int Next()
        {
            var rand = random.Next(basis);

            var sum = 0;
            foreach (var item in providers)
            {
                var tmp = sum + item.Basis;
                if (rand < tmp) return item.Next(rand - sum);
                sum = tmp;
            }

            throw new InvalidOperationException("gacha provider is empty.");
        }
    }
}
