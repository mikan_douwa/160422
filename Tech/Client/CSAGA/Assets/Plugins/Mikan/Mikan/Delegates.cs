﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mikan
{
    /// <summary>
    /// 回呼函式
    /// </summary>
    public delegate void Callback();

    /// <summary>
    /// 過濾函式
    /// </summary>
    /// <typeparam name="T">要過濾的物件型別</typeparam>
    /// <param name="obj">要判別的物件</param>
    /// <returns>回傳是否符合條件</returns>
    public delegate bool Filter<T>(T obj);

    /// <summary>
    /// 收到同步資料時要呼叫的方法
    /// </summary>
    /// <typeparam name="T">目標物件型別</typeparam>
    /// <param name="obj">目標物件</param>
    /// <param name="data">同步資料</param>
    public delegate void SyncDelegate<T>(T obj, ISyncData data);


    public delegate T Finder<T>(int id);

    
}
