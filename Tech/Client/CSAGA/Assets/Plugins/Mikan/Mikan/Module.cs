﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace Mikan
{
    /// <summary>
    /// 可用模組特徵
    /// </summary>
    public class ActiveModuleAttribute : Attribute { }

    /// <summary>
    /// 模組
    /// </summary>
    public interface IModule : IDisposable
    {
        /// <summary>
        /// 模組準備
        /// </summary>
        /// <param name="owner">模組擁有者</param>
        void Prepare(IModuleOwner owner);
        /// <summary>
        /// 執行模組
        /// </summary>
        void Execute();
    }

    /// <summary>
    /// 模組擁有者
    /// </summary>
    public interface IModuleOwner
    {
        /// <summary>
        /// 初始化
        /// </summary>
        /// <param name="manager">模組管理類別</param>
        void Initialize(ModuleManager manager);
    }

    /// <summary>
    /// 預設模組基底類別
    /// </summary>
    /// <typeparam name="TOwner">擁有者型別</typeparam>
    abstract public class ModuleBase<TOwner> : IModule
        where TOwner : IModuleOwner
    {
        /// <summary>
        /// 模組擁有者
        /// </summary>
        protected TOwner Owner { get; private set; }

        /// <inheritdoc />
        virtual public void Prepare(IModuleOwner owner)
        {
            Owner = (TOwner)owner;
        }

        /// <inheritdoc />
        abstract public void Execute();

        /// <inheritdoc />
        virtual public void Dispose() { }
    }

    /// <summary>
    /// 模組管理類別
    /// </summary>
    public class ModuleManager : IDisposable
    {
        private IModuleOwner owner;

        private Dictionary<Type, IModule> modules;

        /// <summary>
        /// 開始執行
        /// </summary>
        /// <param name="owner">模組擁有者</param>
        public void Execute(IModuleOwner owner)
        {
            Execute(owner, Assembly.GetEntryAssembly());
        }

        /// <summary>
        /// 開始執行
        /// </summary>
        /// <param name="owner">模組擁有者</param>
        /// <param name="asm">模組所在的組件</param>
        public void Execute(IModuleOwner owner, Assembly asm)
        {
            this.owner = owner;

            owner.Initialize(this);

            modules = new Dictionary<Type, IModule>();

            foreach(var type in TypeUtils.Select(asm, Filter))
            {
                var module = Activator.CreateInstance(type) as IModule;

                if (module != null) modules.Add(type, module);
            }

            foreach (var item in modules) item.Value.Prepare(owner);

            foreach (var item in modules) item.Value.Execute();
        }

        /// <summary>
        /// 取得模組
        /// </summary>
        /// <typeparam name="T">模組型別</typeparam>
        /// <returns>回傳模組</returns>
        public T GetModule<T>()
            where T : IModule
        {
            if (modules != null)
            {
                IModule module = null;

                if (modules.TryGetValue(typeof(T), out module)) return (T)module;
            }
            return default(T);
        }

        /// <inheritdoc />
        public void Dispose()
        {
            if (modules == null) return;

            foreach (var item in modules) item.Value.Dispose();

            modules.Clear();

            modules = null;
        }

        private bool Filter(Type type)
        {
            if (!TypeUtils.IsSubClassOf<IModule>(type)) return false;

            if (!TypeUtils.HasAttribute<ActiveModuleAttribute>(type)) return false;

            return true;
        }
    }
}
