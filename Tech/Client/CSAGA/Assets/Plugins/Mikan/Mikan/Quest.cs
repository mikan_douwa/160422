﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mikan
{
    /// <summary>
    /// [遊戲]任務實體
    /// </summary>
    public interface IQuest : ISerializable
    {
        /// <summary>
        /// 任務內含多少回合(事件)
        /// </summary>
        int Rounds { get; }
    }

    public interface IQuestResult : ISerializable
    {
        bool IsFinish { get; }
        IQuest Quest { get; }
        List<IQuestEventResult> Results { get; }
    }

    /// <summary>
    /// [遊戲]任務控制器
    /// </summary>
    public interface IQuestController : IDisposable
    {
        /// <summary>
        /// 任務準備完成事件
        /// </summary>
        event Action<IQuest> OnReady;

        /// <summary>
        /// 任務結束事件
        /// </summary>
        event Action<IQuestResult> OnQuestEnd;

        bool IsPaused { get; }

        /// <summary>
        /// 註冊事件產生器
        /// </summary>
        /// <param name="factory">任務事件產生器</param>
        void RegisterEventFactory(IQuestEventFactory factory);

        /// <summary>
        /// 任務準備/初始化
        /// </summary>
        /// <param name="quest"></param>
        void Prepare(IQuest quest);

        /// <summary>
        /// 開始進行任務
        /// </summary>
        void Start();

        /// <summary>
        /// 暫停任務
        /// </summary>
        void Pause();

        /// <summary>
        /// 繼續任務
        /// </summary>
        void Resume();

        /// <summary>
        /// 取消任務
        /// </summary>
        void Cancel();

        /// <summary>
        /// 儲存目前狀態
        /// </summary>
        void SaveState();
    }

    /// <summary>
    /// [遊戲]任務事件產生器
    /// </summary>
    public interface IQuestEventFactory
    {
        /// <summary>
        /// 準備完成事件
        /// </summary>
        event Action<IQuestEventFactory> OnReady;

        /// <summary>
        /// 為即將進行的任務做準備(預載資源)
        /// </summary>
        /// <param name="quest">即將進行的任務</param>
        void Prepare(IQuest quest);

        /// <summary>
        /// 創建任務事件列表
        /// </summary>
        /// <param name="quest">任務</param>
        /// <param name="round">第幾回合</param>
        /// <returns>回傳任務事件列表</returns>
        IEnumerable<IQuestEvent> Create(IQuest quest, int round);
    }

    /// <summary>
    /// [遊戲]任務事件
    /// </summary>
    public interface IQuestEvent : ISerializable, IDisposable
    {
        /// <summary>
        /// 事件預載完成事件
        /// </summary>
        event Action<IQuestEvent> OnPreload;

        /// <summary>
        /// 事件準備完成事件
        /// </summary>
        event Action<IQuestEvent> OnReady;

        /// <summary>
        /// 事件已結束事件
        /// </summary>
        event Action<IQuestEventResult> OnFinish;

        /// <summary>
        /// 任務開始前預載
        /// </summary>
        void Preload();

        /// <summary>
        /// 事件準備(初始化＆資源載入)
        /// </summary>
        /// <param name="controller">任務控制器</param>
        void Prepare(IQuestController controller);

        /// <summary>
        /// 開始進行
        /// </summary>
        void Start();

        /// <summary>
        /// 暫停事件
        /// </summary>
        void Pause();

        /// <summary>
        /// 繼續事件
        /// </summary>
        void Resume();

        /// <summary>
        /// 取消事件
        /// </summary>
        void Cancel();
    }

    /// <summary>
    /// [遊戲]任務事件結果
    /// </summary>
    public interface IQuestEventResult : IDynamicSerializable
    {
        /// <summary>
        /// 關聯的事件
        /// </summary>
        IQuestEvent Event { get; }

        /// <summary>
        /// 是否成功
        /// </summary>
        bool IsSucc { get; }

        bool Continue { get; }
    }


    abstract public class QuestController<T> : IQuestController
        where T : IQuest
    {
        class QuestResult : IQuestResult
        {
            public bool IsFinish { get; set; }
            public IQuest Quest { get; set; }
            public List<IQuestEventResult> Results { get; set; }

            public void Serialize(IOutput output)
            {
                throw new NotImplementedException();
            }

            public void DeSerialize(IInput input)
            {
                throw new NotImplementedException();
            }
        }

        public event Action<IQuest> OnReady;

        public event Action<IQuestResult> OnQuestEnd;

        private IQuestEventFactory factory;

        private T quest;

        private List<IQuestEvent> preloading;
        private Queue<IQuestEvent> queue;
        private List<IQuestEvent> events;
        private List<IQuestEvent> finished;

        private List<IQuestEventResult> results;

        private bool isSucc;

        private bool isPaused = false;

        private IQuestEvent current;

        public bool IsPaused { get { return isPaused; } }

        public void RegisterEventFactory(IQuestEventFactory factory)
        {
            this.factory = factory;
            factory.OnReady += OnFactoryReady;
        }

        public void Prepare(IQuest quest)
        {
            isPaused = false;

            this.quest = (T)quest;

            factory.Prepare(quest);
        }

        void OnFactoryReady(IQuestEventFactory obj)
        {
            events = new List<IQuestEvent>();
            queue = new Queue<IQuestEvent>();
            preloading = new List<IQuestEvent>();
            results = new List<IQuestEventResult>();
            finished = new List<IQuestEvent>();

            for (int i = 0; i < quest.Rounds; ++i)
            {
                foreach (var item in factory.Create(quest, i))
                {
                    item.OnPreload += OnEventPreload;
                    item.OnReady += OnEventReady;
                    item.OnFinish += OnEventFinish;
                    events.Add(item);
                    queue.Enqueue(item);
                    preloading.Add(item);
                }
            }

            foreach (var item in events) item.Preload();
        }

        void OnEventPreload(IQuestEvent obj)
        {
            preloading.Remove(obj);

            if (preloading.Count > 0) return;
            if (OnReady != null) OnReady(quest);
        }

        void OnEventReady(IQuestEvent obj)
        {
            current = obj;
            current.Start();
            if (isPaused) current.Pause();
        }

        void OnEventFinish(IQuestEventResult obj)
        {
            DisposeEvent(obj.Event);
            finished.Add(obj.Event);

            results.Add(obj);
            isSucc = obj.IsSucc;

            if (obj.Continue)
                Next();
            else
                EndQuest();
        }

        private void EndQuest()
        {
            ClearEvents();

            if (OnQuestEnd != null) OnQuestEnd(new QuestResult { Quest = quest, Results = results, IsFinish = isSucc });
        }

        private void Next()
        {
            if (queue.Count > 0)
            {
                var ev = queue.Dequeue();

                ev.Prepare(this);

            }
            else
                EndQuest();
        }

        private void ClearEvents()
        {
            if (current != null)
            {
                DisposeEvent(current);
                current = null;
            }

            if (events != null)
            {
                foreach (var item in events)
                {
                    if (!finished.Contains(item)) DisposeEvent(item);
                }

                events = null;
            }
        }

        private void DisposeEvent(IQuestEvent ev)
        {
            ev.OnPreload -= OnEventPreload;
            ev.OnReady -= OnEventReady;
            ev.OnFinish -= OnEventFinish;
            ev.Dispose();
        }

        public void Start()
        {
            Next();
        }

        public void Pause()
        {
            if (isPaused) return;
            isPaused = true;

            if (current != null) current.Pause();
        }

        public void Resume()
        {
            if (!isPaused) return;
            isPaused = false;

            if (current != null) current.Resume();
        }

        public void Cancel()
        {
            isSucc = false;
            if (current != null) current.Cancel();
            EndQuest();
        }

        virtual public void SaveState()
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            ClearEvents();

            OnReady = null;
            OnQuestEnd = null;
        }
    }
}
