﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace Mikan
{
    public interface IScheduleTask : IDisposable
    {
        bool Immediately { get; }
        void Execute(int times);
    }

    public class Scheduler
    {
        internal static List<Schedule> schedules;

        internal static TaskThread tasks;

        public static void Start()
        {
            if (tasks != null) return;
            schedules = new List<Schedule>();
            tasks = new TaskThread();
        }

        public static void Stop()
        {
            if (tasks == null) return;
            tasks.AddTask(new StopAllScheduleTask());
            tasks.Dispose();
            tasks = null;
            schedules = null;
        }

        public static void AddTask(IScheduleTask task, int intervalSecs, int loop = int.MaxValue)
        {
            var schedule = new Schedule(task, intervalSecs * 1000, loop);
            tasks.AddTask(new AddScheduleTask(schedule));
        }
    }

    class AddScheduleTask : ITask
    {
        private Schedule schedule;
        public AddScheduleTask(Schedule schedule)
        {
            this.schedule = schedule;
        }
        public void Execute()
        {
            schedule.Start();
            Scheduler.schedules.Add(schedule);
        }

        public void Dispose() { }
    }
    class RemoveScheduleTask : ITask
    {
        private Schedule schedule;
        public RemoveScheduleTask(Schedule schedule)
        {
            this.schedule = schedule;
        }
        public void Execute()
        {
            Scheduler.schedules.Remove(schedule);
            schedule.Dispose();
        }

        public void Dispose() { }
    }

    class StopAllScheduleTask : ITask
    {
        public void Execute()
        {
            foreach (var item in Scheduler.schedules)
            {
                item.Stop();
                item.Dispose();
            }

            Scheduler.schedules.Clear();
        }

        public void Dispose() { }
    }

    class Schedule : IDisposable
    {
        private Timer timer;
        private int times = 0;

        private IScheduleTask task;
        private int interval;
        private int loop;
        public Schedule(IScheduleTask task, int interval, int loop)
        {
            this.task = task;
            this.interval = interval;
            this.loop = loop;
        }
        public void Start()
        {
            timer = new Timer(OnTimer, null, task.Immediately ? 0 : interval, interval);
        }

        public void Stop()
        {
            timer.Change(Timeout.Infinite, Timeout.Infinite);
        }

        public void Dispose()
        {
            timer.Dispose();
            timer = null;
            task.Dispose();
            task = null;
        }

        private void OnTimer(object state)
        {
            task.Execute(++times);

            if (times >= loop)
            {
                Stop();
                Scheduler.tasks.AddTask(new RemoveScheduleTask(this));
            }
        }
    }
}
