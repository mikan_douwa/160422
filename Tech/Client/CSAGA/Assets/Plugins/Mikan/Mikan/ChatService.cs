﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mikan.Net
{
    public interface IChatService : IEventTrigger, IDisposable
    {
        bool IsAlive { get; }

        void RegiserEventListener(IChatServiceEventListener listener);

        void RegisterCommand<T>() where T : ISerializable;

        void Send(byte[] data);

        void Send(string data);

        void Connect(string url);

        void Disconnect();
    }

    public interface ISession
    {

    }

    public interface IChatServiceEventListener
    {
        void OnConnect(IChatService sender);
        void OnData(IChatService sender, byte[] data);
        void OnError(IChatService sender, string message);
        void OnClose(IChatService sender, string reason);
        void OnSent(IChatService sender, bool isSucc);
    }

}
