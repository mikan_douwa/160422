﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.IO;

namespace Mikan.Net
{
    /// <summary>
    /// HTTP連線服務提供者
    /// </summary>
    public interface IHttpServiceProvider
    {
        /// <summary>
        /// 收到遠端回應事件
        /// </summary>
        event Action<HttpResponse> OnResponse;

        /// <summary>
        /// 送出要求
        /// </summary>
        /// <param name="req">HTTP要求</param>
        void SendRequest(HttpRequest req);
    }
    
    /// <summary>
    /// HTTP要求
    /// </summary>
    public class HttpRequest
    {
        /// <summary>
        /// uri
        /// </summary>
        public string URI;

        /// <summary>
        /// HTTP方法
        /// </summary>
        public string Method;

        /// <summary>
        /// 資料緩衝
        /// </summary>
        public byte[] Data;
    }

    /// <summary>
    /// HTTP遠端回應
    /// </summary>
    public class HttpResponse
    {
        /// <summary>
        /// 發送的HTTP要求
        /// </summary>
        public HttpRequest Request;

        /// <summary>
        /// 是否成功
        /// </summary>
        public bool IsSucc;

        /// <summary>
        /// 資料緩衝
        /// </summary>
        public Stream Data;

        /// <summary>
        /// 錯誤訊息
        /// </summary>
        public string Error;

    }
    

    public class HttpClient : IHttpServiceProvider
    {
        public event Action<HttpResponse> OnResponse;

        private Uri baseURL;

        private HttpRequest current;

        private Queue<HttpRequest> queued = new Queue<HttpRequest>();

        public HttpClient(string baseURL)
        {
            this.baseURL = new Uri(baseURL);
            
        }

        public HttpClient(Uri baseURL)
        {
            this.baseURL = baseURL;
        }

        public void SendRequest(HttpRequest req)
        {
            lock (queued)
            {
                if (current != null)
                    queued.Enqueue(req);
                else
                {
                    current = req;
                    BeginSendRequest(current);
                }
            }
        }

        public void SendRequest(string uri, string method, byte[] data)
        {
            SendRequest(new HttpRequest { URI = uri, Method = method, Data = data });
        }

        private void BeginSendRequest(HttpRequest src)
        {
            SafeExecuteOperation((obj) =>
            {
                var request = (HttpWebRequest)HttpWebRequest.Create(new Uri(baseURL, src.URI));

                request.Method = src.Method;

                if (src.Method != "GET" && src.Method != "DELETE")
                {
                    request.BeginGetRequestStream(OnGetRequestStream, request);
                }
                else
                {
                    request.BeginGetResponse(OnGetResponse, request);
                }
            });
            
        }

        private void OnGetRequestStream(IAsyncResult res)
        {
            SafeExecuteOperation((obj) =>
            {
                var req = (HttpWebRequest)res.AsyncState;

                var stream = req.EndGetRequestStream(res);

                stream.Write(current.Data, 0, current.Data.Length);

                stream.Flush();

                req.BeginGetResponse(OnGetResponse, req);
            });
        }

        private void OnGetResponse(IAsyncResult res)
        {
            SafeExecuteOperation((obj) =>
            {
                var request = (HttpWebRequest)res.AsyncState;

                var response = (HttpWebResponse)request.EndGetResponse(res);

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    obj.IsSucc = true;
                    obj.Data = response.GetResponseStream();
                }
                else
                {
                    obj.IsSucc = false;
                    obj.Error = string.Format("[HTTP_FAILED] StatusCode= {0}, Description = {1}", response.StatusCode, response.StatusDescription);
                }

                TirggerOnResponse(obj);
            });

            
        }

        private void SafeExecuteOperation(Action<HttpResponse> operation)
        {
            var obj = new HttpResponse { Request = current };
            try
            {
                operation(obj);
            }
            catch (Exception e)
            {
                obj.IsSucc = false;
                obj.Error = "[HTTP_FAILED] " + e.ToString();

                TirggerOnResponse(obj);
            } 
        }

        private void TirggerOnResponse(HttpResponse obj)
        {
            if (OnResponse != null) OnResponse(obj);

            lock (queued)
            {
                if (queued.Count > 0)
                {
                    current = queued.Dequeue();
                    BeginSendRequest(current);
                }
                else
                    current = null;
            }
        }

    }
    
}
