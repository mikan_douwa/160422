﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mikan
{

    public static class DateTimeUtils
    {
        private static DateTime UNIX = new DateTime(1970, 1, 1);

        public static DateTime FromUnixTime(int time)
        {
            return UNIX.AddSeconds(time);
        }

        public static int ToUnixTime(DateTime time)
        {
            return (Int32)(time.Subtract(UNIX)).TotalSeconds;
        }
    }

    public class UnixTime : ISerializable
    {
        public int Value { get; private set; }
        public DateTime DateTime { get; private set; }
        
        public static UnixTime Convert(DateTime time)
        {
            var inst = new UnixTime();
            inst.DateTime = time;
            inst.Value = DateTimeUtils.ToUnixTime(time);
            return inst;
        }

        public static UnixTime Convert(int time)
        {
            var inst = new UnixTime();
            inst.Value = time;
            inst.DateTime = DateTimeUtils.FromUnixTime(time);
            return inst;
        }



        public void Serialize(IOutput output)
        {
            output.Write(Value);
        }

        public void DeSerialize(IInput input)
        {
            Value = input.ReadInt32();
            DateTime = DateTimeUtils.FromUnixTime(Value);
        }
    }
}
