﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;

namespace Mikan
{
    /// <summary>
    /// 型別工具
    /// </summary>
    public static class TypeUtils
    {
        /// <summary>
        /// 檢查是否有繼承關係
        /// </summary>
        /// <typeparam name="T">父類別型別</typeparam>
        /// <param name="type">子類別型別</param>
        /// <returns>回傳是否有繼承關係</returns>
        public static bool IsSubClassOf<T>(Type type)
        {
            var target = typeof(T);
            if (target.IsInterface)
            {
                foreach (var item in type.GetInterfaces())
                {
                    if (item.Equals(target)) return true;
                }

                return false;
            }
            
            return type.IsSubclassOf(typeof(T));
        }

        /// <summary>
        /// 檢查是否含自訂屬性
        /// </summary>
        /// <typeparam name="T">屬性型別</typeparam>
        /// <param name="provider">目標物件</param>
        /// <returns>回傳是否含自訂屬性</returns>
        public static bool HasAttribute<T>(ICustomAttributeProvider provider)
            where T : Attribute
        {
            return GetAttribute<T>(provider) != default(T);
        }

        /// <summary>
        /// 取得自訂屬性
        /// </summary>
        /// <typeparam name="T">屬性型別</typeparam>
        /// <param name="provider">目標物件</param>
        /// <returns>回傳屬性</returns>
        public static T GetAttribute<T>(ICustomAttributeProvider provider)
        {
            foreach (var item in provider.GetCustomAttributes(false))
            {
                if (item is T) return (T)item;
            }

            return default(T);
        }

        /// <summary>
        /// 列舉所有符合條件的型別
        /// </summary>
        /// <param name="asm">目標組件</param>
        /// <param name="filter">過濾方法</param>
        /// <returns>回傳符合條件的型別列舉</returns>
        public static IEnumerable<Type> Select(Assembly asm, Filter<Type> filter)
        {
            return FilterUtils.Select(asm.GetTypes(), filter);
        }

        /// <summary>
        /// 列舉在預設組件中所有符合條件的型別
        /// </summary>
        /// <param name="filter">過濾方法</param>
        /// <returns>回傳符合條件的型別列舉</returns>
        public static IEnumerable<Type> Select(Filter<Type> filter)
        {
            return Select(Assembly.GetEntryAssembly(), filter);
        }

        /// <summary>
        /// 取得目標組件中第一個符合條件的型別
        /// </summary>
        /// <param name="filter">過濾方法</param>
        /// <returns>回傳型別</returns>
        public static Type SelectFirst(Filter<Type> filter)
        {
            foreach (var type in Select(filter)) return type;

            return null;
        }

        /// <summary>
        /// 取得第一個符合條件的型別
        /// </summary>
        /// <param name="asm">目標組件</param>
        /// <param name="filter">過濾方法</param>
        /// <returns>回傳型別</returns>
        public static Type SelectFirst(Assembly asm, Filter<Type> filter)
        {
            return FilterUtils.SelectFirst(asm.GetTypes(), filter);
        }
    }
}
