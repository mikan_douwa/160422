﻿using System;
using System.IO;
using System.Threading;
using System.Collections.Generic;
using Mikan.Net;
using System.Runtime.CompilerServices;
using System.Security.Cryptography;
using System.Text;

namespace Mikan
{
    /// <summary>
    /// 遊戲服務
    /// </summary>
    public interface IGameService : IEventTrigger
    {
        /// <summary>
        /// 是否連線中變更事件
        /// </summary>
        event Action<bool> OnBusy;

        /// <summary>
        /// 發送命令
        /// </summary>
        /// <param name="cmd">命令</param>
        void SendCommand(ICommand cmd);
    }

    /// <summary>
    /// 事件觸發者
    /// </summary>
    public interface IEventTrigger
    {
        /// <summary>
        /// 觸發事件
        /// </summary>
        void TriggerEvent();
    }

    /// <summary>
    /// 系統服務提供者
    /// </summary>
    public interface ISystemServiceProvider
    {
        IHttpServiceProvider GetHttpServiceProvider();

        IWebSocketService CreateWebSocketService();

        event Action<byte[]> OnReadEnd;

        void CleanNotification();

        void AddNotification(string msg, DateTime time, int number);

        /// <summary>
        /// 唯一識別碼
        /// </summary>
        string UUID { get; }

        /// <summary>
        /// 裝置識別碼
        /// </summary>
        string DeviceID { get; }

        /// <summary>
        /// IAP類型
        /// </summary>
        string IAPName { get; }

        /// <summary>
        /// 儲存本地資料
        /// </summary>
        /// <param name="key">鍵值</param>
        /// <param name="value">值</param>
        void SaveLocalData(string key, string value);

        /// <summary>
        /// 儲存本地資料
        /// </summary>
        /// <param name="key">鍵值</param>
        /// <param name="value">值</param>
        void SaveLocalData(string key, int value);

        /// <summary>
        /// 儲存本地資料
        /// </summary>
        /// <param name="key">鍵值</param>
        /// <param name="value">值</param>
        void SaveLocalData(string key, float value);

        /// <summary>
        /// 讀取本地資料
        /// </summary>
        /// <param name="key">鍵值</param>
        /// <returns>回傳字串</returns>
        string LoadString(string key);
        /// <summary>
        /// 讀取本地資料
        /// </summary>
        /// <param name="key">鍵值</param>
        /// <returns>回傳整數</returns>
        int LoadInt(string key);
        /// <summary>
        /// 讀取本地資料
        /// </summary>
        /// <param name="key">鍵值</param>
        /// <returns>回傳浮點數</returns>
        float LoadFloat(string key);

        /// <summary>
        /// 清除本地資料
        /// </summary>
        void ClearLocalData(string key);

        /// <summary>
        /// 清除本地資料
        /// </summary>
        void ClearLocalData();

        /// <summary>
        /// 顯示資訊
        /// </summary>
        /// <param name="priority">重要度(值愈小愈高)</param>
        /// <param name="log">要顯示的資訊</param>
        void ShowLog(int priority, string log);

        void Read(params string[] args);
    }

    /// <summary>
    /// 預設遊戲服務基底類別
    /// </summary>
    abstract public class GameService : IGameService, IDisposable
    {
        /// <inheritdoc />
        public event Action<bool> OnBusy;

        private UnityCompatibleQueue<IEventTrigger> majorEvents = new UnityCompatibleQueue<IEventTrigger>();

        private UnityCompatibleQueue<IEventTrigger> minorEvents = new UnityCompatibleQueue<IEventTrigger>();

        private Thread thread;

        private Dictionary<object, ISyncData> cachedSyncList = new Dictionary<object, ISyncData>();

        private Queue<ICommand> queue = new Queue<ICommand>();

        private int retryCount = 0;

        private int busyCount = 0;

        private bool isCancelled = false;

        private bool isDisposing = false;

        private IHttpServiceProvider httpService;

        protected List<byte> Transaction { get; private set; }

        /// <summary>
        /// 建構子
        /// </summary>
        public GameService()
        {
#if UNITY_ANDROID && !UNITY_EDITOR
            Transaction = new List<byte>();
#else
            Transaction = new List<byte>(new byte[] { 0x56, 0x00, 0x12, 0xb7, 0x4a, 0x09, 0xf1, 0xc5, 0xa9, 0x67, 0x67, 0x3a });
#endif
            
            thread = new Thread(ThreadEntrance);
            thread.Start();
        }

        public bool IsBusy { get { return busyCount > 0; } }

        virtual public void OnApplicationPause(bool isPaused) { }

        /// <summary>
        /// 設定系統服務提供者
        /// </summary>
        /// <param name="serviceProvider">系統服務提供者</param>
        virtual public void Setup(ISystemServiceProvider serviceProvider)
        {
            ServiceProvider = serviceProvider;

            KernelService.Logger = new Logger(this);

            httpService = ServiceProvider.GetHttpServiceProvider();
            httpService.OnResponse += OnHttpResponse;
#if UNITY_ANDROID && !UNITY_EDITOR
            ServiceProvider.OnReadEnd += OnReadEnd;
            ServiceProvider.Read("L2Jpbi9EYXRhL01hbmFnZWQvQXNzZW1ibHktQ1NoYXJwLmRsbA==", "L2Jpbi9EYXRhL01hbmFnZWQvQXNzZW1ibHktQ1NoYXJwLWZpcnN0cGFzcy5kbGw=");
#endif
        }

        void OnReadEnd(byte[] bytes)
        {
            using (var md5 = MD5.Create()) Transaction.AddRange(md5.ComputeHash(bytes));
        }

        private void OnHttpResponse(HttpResponse res)
        {
            var wrapper = new CommandWrapper { Origin = res };
            if (res.IsSucc)
                wrapper.Command = DeSerialize(res.Data);

            Enqueue(new InvokeEvent<CommandWrapper>(SyncProcessCommand, wrapper));
        }

        private void SyncProcessCommand(CommandWrapper res)
        {
            if (res.Origin.IsSucc)
                OnCommand(res.Command);
            else
                OnHttpFail(res.Origin);
        }

        /// <inheritdoc />
        virtual public void Dispose()
        {
            isDisposing = true;
            thread.Join();
            if (httpService != null) httpService.OnResponse -= OnHttpResponse;
            if (ServiceProvider != null) ServiceProvider.OnReadEnd -= OnReadEnd;
        }

        /// <inheritdoc />
        virtual public void SendCommand(ICommand cmd)
        {
            isCancelled = false;

            if (!cmd.IsBackground) IncreaseBusyCount();

            queue.Enqueue(cmd);

            if (queue.Count == 1) DoSendCommand(cmd);
        }

        private void DoSendCommand(ICommand cmd)
        {
            KernelService.Logger.Trace("send cmd, {0}", cmd);
            InjectSyncData(cmd);
            SendCommand(cmd, 0);
        }

        private void SendCommand(ICommand cmd, double delaySec)
        {
            minorEvents.Enqueue(new CommandEvent(cmd, AsyncSendCommand, delaySec));
        }

        protected void IncreaseBusyCount()
        {
            if (++busyCount > 1) return;

            if (OnBusy != null) OnBusy(true);
        }

        protected void DecreaseBusyCount()
        {
            if (--busyCount > 0) return;
            if (OnBusy != null) OnBusy(false);
        }

        /// <summary>
        /// 取得系統服務
        /// </summary>
        protected ISystemServiceProvider ServiceProvider { get; private set; }

        /// <summary>
        /// 取得序列化工具
        /// </summary>
        abstract protected ISerializer<ICommand> Serializer { get; }

        /// <summary>
        /// 處理遠端傳過來的命令
        /// </summary>
        /// <param name="cmd"></param>
        abstract protected bool ProcessCommand(ICommand cmd);

        #region Sync

        virtual protected bool InjectSyncData(ICommand cmd)
        {
            if (cachedSyncList.Count == 0) return false;
            foreach (var item in cachedSyncList) cmd.AddSyncData(item.Value);
            cachedSyncList.Clear();
            return true;
        }

        protected void EnqueueSyncData(object sender, ISyncData data) { cachedSyncList[sender] = data; }

        virtual protected void DoSync(ICommand cmd) { }

        #endregion

        virtual protected void OnFail(ICommand req, ICommand res) { }
        virtual protected void OnCancel(ICommand req) { }

        protected void CancelQueuedCommands()
        {
            var cached = queue;

            queue = new Queue<ICommand>();

            while (cached.Count > 0) OnCancel(cached.Dequeue());

            while (busyCount > 0) DecreaseBusyCount();

            isCancelled = true;
        }

        /// <summary>
        /// 收到遠端命令
        /// </summary>
        /// <param name="cmd">遠端命令</param>
        virtual protected void OnCommand(ICommand cmd)
        {
            DoSync(cmd);

            if (!cmd.IsActivePush)
            {
                var sender = queue.Peek();

                if (!cmd.IsSucc)
                {
                    if (cmd.AutoRetry && retryCount < 3)
                    {
                        SendCommand(sender, 2);
                        ++retryCount;
                        return;
                    }
                    else
                    {
                        var cached = queue;

                        queue = new Queue<ICommand>();

                        OnFail(sender, cmd);

                        while (cached.Count > 0)
                        {
                            var item = cached.Dequeue();
                            if (!item.IsBackground) DecreaseBusyCount();

                            OnCancel(item);
                        }

                        retryCount = 0;

                        return;
                    }
                }
                else
                {
                    if (!ProcessCommand(cmd))
                        KernelService.Logger.Debug("未處理的命令 : {0}", cmd);
                }

                retryCount = 0;

                if (!isCancelled && cmd.IsSucc)
                {
                    queue.Dequeue();

                    if (queue.Count > 0) DoSendCommand(queue.Peek());
                }

                if (!sender.IsBackground) DecreaseBusyCount();
            }
            else
            {
                if (!ProcessCommand(cmd))
                    KernelService.Logger.Debug("未處理的命令 : {0}", cmd);
            }
        }

        private void OnHttpFail(HttpResponse res)
        {
            if (queue.Count > 0)
            {
                var sender = queue.Peek();

                /*if (retryCount < 3)
                {
                    SendCommand(sender, 2);
                    ++retryCount;
                    return;
                }
                else*/
                {
                    var cached = queue;

                    queue = new Queue<ICommand>();

                    OnFail(sender, null);

                    while (cached.Count > 0) OnCancel(cached.Dequeue());
                }

                retryCount = 0;

                if (!sender.IsBackground) DecreaseBusyCount();
            }
            
        }

        /// <inheritdoc />
        virtual public void TriggerEvent()
        {
            minorEvents.Integration();

            if (!majorEvents.IsEmpty) majorEvents.Dequeue().TriggerEvent();           
        }

        /// <summary>
        /// 新增一個等待處理的事件
        /// </summary>
        /// <param name="e">事件</param>
        protected void Enqueue(IEventTrigger e)
        {
            majorEvents.Enqueue(e);
        }

        /// <summary>
        /// 送出命令要進行的動作
        /// </summary>
        /// <param name="item">命令物件</param>
        virtual protected void AsyncSendCommand(ICommand cmd)
        {
            var request = new Request { Sender = cmd };

            request.URI = cmd.URI;
            request.Method = "POST";
            request.Data = Serialize(cmd);

            httpService.SendRequest(request);
        }

        virtual protected byte[] Serialize(ICommand cmd)
        {
            return Serializer.Serialize(cmd);
        }

        virtual protected ICommand DeSerialize(Stream rawdata)
        {
            return Serializer.DeSerialize(rawdata);
        }

        protected void DelayInvoke(Callback callback)
        {
            Enqueue(new InvokeEvent(callback));
        }



        /// <summary>
        /// 顯示資訊
        /// </summary>
        /// <param name="priority">重要度</param>
        /// <param name="log">資訊字串</param>
        protected void ShowLog(int priority, string log)
        {
            Enqueue(new ShowLogEvent(priority, log, ServiceProvider.ShowLog));
        }

        protected void ShowLog(int priority, string format, params object[] args)
        {
            Enqueue(new ShowLogEvent(priority, string.Format(format, args), ServiceProvider.ShowLog));
        }

        private void ThreadEntrance()
        {
            while (!isDisposing)
            {
                if (minorEvents.IsEmpty)
                {
                    Thread.Sleep(30);
                    majorEvents.Integration();
                }
                else
                {
                    try
                    {
                        minorEvents.Dequeue().TriggerEvent();
                    }
                    catch (Exception e)
                    {
                        ShowLog(0, "[InternalError] {0}", e);
                    }
                    
                    Thread.Sleep(1);
                }
            }
        }

        #region internal

        class Logger : LoggerBase
        {
            private GameService owner;
            public Logger(GameService owner)
            {
                this.owner = owner;
            }
            public override void AddLog(LogPriority priority, string log)
            {
                owner.ShowLog((int)priority, log);
            }
        }

        class Request : HttpRequest
        {
            public ICommand Sender;
        }

        class CommandEvent : IEventTrigger
        {
            private ICommand cmd;

            private Action<ICommand> handler;
            private double delaySec = 0;
            public CommandEvent(ICommand cmd, Action<ICommand> handler, double delaySec = 0)
            {
                this.cmd = cmd;
                this.handler = handler;
                this.delaySec = delaySec;
            }
            public void TriggerEvent() 
            {
                if (delaySec > 0) Thread.Sleep((int)(delaySec * 1000));
                try
                {
                    handler(cmd);
                }
                catch (Exception e)
                {
                    KernelService.Logger.Fatal("[InternalError] invoke fail, method = {0}, message = {1}", handler.Method, e);
                    throw;
                }
            }
        }

        class InvokeEvent : IEventTrigger
        {
            private Callback callback;
            public InvokeEvent(Callback callback)
            {
                this.callback = callback;
            }
            public void TriggerEvent()
            {
                callback();
            }
        }

        class InvokeEvent<T> : IEventTrigger
        {
            private Action<T> callback;
            private T param;
            public InvokeEvent(Action<T> callback, T param)
            {
                this.callback = callback;
                this.param = param;
            }
            public void TriggerEvent()
            {
                callback(param);
            }
        }

        class ShowLogEvent : IEventTrigger
        {
            public delegate void ShowLogDelegate(int priority, string log);

            private string log;
            private int priority;

            private ShowLogDelegate logger;
            public ShowLogEvent(int priority, string log, ShowLogDelegate logger)
            {
                this.priority = priority;
                this.log = log;
                this.logger = logger;
            }
            public void TriggerEvent() { logger(priority, log); }
        }

        class CommandWrapper
        {
            public HttpResponse Origin;
            public ICommand Command;
        }

        #endregion
    }

    public class EventTriggerList : IEventTrigger
    {
        class Node
        {
            public IEventTrigger Value;
            public Node Next;
            public Node Prev;
        }
        private Node firstNode = null;
        private Node lastNode = null;

        public void Add(IEventTrigger trigger)
        {
            var node = new Node { Value = trigger };

            if (firstNode == null)
                firstNode = node;
            else
                lastNode.Next = node;

            node.Prev = lastNode;
            lastNode = node;
        }

        public void Remove(IEventTrigger trigger)
        {
            var node = firstNode;

            while (node != null)
            {
                if (node.Value == trigger)
                {
                    if (node.Next != null)
                        node.Next.Prev = node.Prev;
                    else
                        lastNode = node.Prev;

                    if (node.Prev != null)
                        node.Prev.Next = node.Next;
                    else
                        firstNode = node.Next;
                    
                    break;
                }
                node = node.Next;
            }
        }

        public void TriggerEvent()
        {
            var node = firstNode;

            while (node != null)
            {
                node.Value.TriggerEvent();
                node = node.Next;
            }
        }
    }
}
