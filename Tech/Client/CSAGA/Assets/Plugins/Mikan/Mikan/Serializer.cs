﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Mikan
{
    public interface ISerializer<T>
    {
        byte[] Serialize(T data);

        void Serialize(T data, Stream output);

        void Serialize(T data, IOutput output);

        T DeSerialize(byte[] rawdata);

        T DeSerialize(Stream input);

        T DeSerialize(IInput input);
    }

    public interface ISerializable
    {
        void Serialize(IOutput output);
        void DeSerialize(IInput input);
    }

    public interface IDynamicSerializable : ISerializable { }

    /// <summary>
    /// 輸入資料串流
    /// </summary>
    public interface IInput
    {
        /// <summary>
        /// 讀取16位元整數
        /// </summary>
        /// <returns></returns>
        Int16 ReadInt16();

        /// <summary>
        /// 讀取32位元整數
        /// </summary>
        /// <returns>回傳32位元整數</returns>
        Int32 ReadInt32();

        /// <summary>
        /// 讀取64位元整數
        /// </summary>
        /// <returns>回傳64位元整數</returns>
        Int64 ReadInt64();

        /// <summary>
        /// 讀取32位元浮點數
        /// </summary>
        /// <returns>回傳32位元浮點數</returns>
        float ReadFloat();

        /// <summary>
        /// 讀取字串
        /// </summary>
        /// <returns>回傳字串</returns>
        string ReadString();

        /// <summary>
        /// 讀取位元組
        /// </summary>
        /// <returns>回傳位元組</returns>
        byte ReadByte();

        /// <summary>
        /// 讀取位元組陣列
        /// </summary>
        /// <returns>回傳位元組陣列</returns>
        byte[] ReadByteArray();

        /// <summary>
        /// 讀取布林值
        /// </summary>
        /// <returns>回傳布林值</returns>
        bool ReadBoolean();

        /// <summary>
        /// 讀取日期時間
        /// </summary>
        /// <returns>回傳日期時間</returns>
        DateTime ReadDateTime();

        /// <summary>
        /// 讀取32位元整數列表
        /// </summary>
        /// <returns>回傳列表</returns>
        List<int> ReadInt32List();

        /// <summary>
        /// 讀取64位元整數列表
        /// </summary>
        /// <returns>回傳列表</returns>
        List<Int64> ReadInt64List();

        /// <summary>
        /// 讀取字串列表
        /// </summary>
        /// <returns>回傳列表</returns>
        List<string> ReadStringList();

        /// <summary>
        /// 讀取物件
        /// </summary>
        /// <typeparam name="T">物件型別</typeparam>
        /// <returns>回傳物件</returns>
        T Read<T>() where T : ISerializable, new();

        /// <summary>
        /// 安全讀取物件,對應IOuput.SafeWrite
        /// </summary>
        /// <typeparam name="T">物件型別</typeparam>
        /// <returns>回傳物件</returns>
        T SafeRead<T>() where T : ISerializable, new();

        /// <summary>
        /// 讀取物件列表
        /// </summary>
        /// <typeparam name="T">物件型別</typeparam>
        /// <returns>回傳列表</returns>
        List<T> ReadList<T>() where T : ISerializable, new();

        /// <summary>
        /// 讀取Int32列表
        /// </summary>
        /// <returns>回傳列表</returns>
        Dictionary<int, int> ReadInt32Dictionary();

        /// <summary>
        /// 讀取物件列表
        /// </summary>
        /// <typeparam name="T">物件型別</typeparam>
        /// <returns>回傳列表</returns>
        Dictionary<int, T> ReadInt32Dictionary<T>() where T : ISerializable, new();

        /// <summary>
        /// 讀取物件列表
        /// </summary>
        /// <typeparam name="T">物件型別</typeparam>
        /// <returns>回傳列表</returns>
        Dictionary<Int64, T> ReadInt64Dictionary<T>() where T : ISerializable, new();

        /// <summary>
        /// 讀取動態型別物件列表
        /// </summary>
        /// <typeparam name="T">物件基底型別</typeparam>
        /// <returns>回傳列表</returns>
        List<T> ReadDynamicList<T>() where T : IDynamicSerializable;


        /// <summary>
        /// 讀取動態型別物件
        /// </summary>
        /// <typeparam name="T">物件基底型別</typeparam>
        /// <returns>回傳物件</returns>
        T ReadDynamic<T>() where T : IDynamicSerializable;
    }

    /// <summary>
    /// 輸出資料串流
    /// </summary>
    public interface IOutput
    {
        /// <summary>
        /// 寫入字串
        /// </summary>
        /// <param name="val">要寫入的值</param>
        void Write(string val);

        /// <summary>
        /// 寫入16位元整數
        /// </summary>
        /// <param name="val">要寫入的值</param>
        void Write(Int16 val);

        /// <summary>
        /// 寫入32位元整數
        /// </summary>
        /// <param name="val">要寫入的值</param>
        void Write(int val);

        /// <summary>
        /// 寫入64位元整數
        /// </summary>
        /// <param name="val">要寫入的值</param>
        void Write(Int64 val);

        /// <summary>
        /// 寫入32位元浮點數
        /// </summary>
        /// <param name="val">要寫入的值</param>
        void Write(float val);

        /// <summary>
        /// 寫入位元組
        /// </summary>
        /// <param name="val">要寫入的值</param>
        void Write(byte val);

        /// <summary>
        /// 寫入位元組陣列
        /// </summary>
        /// <param name="val">要寫入的值</param>
        void Write(byte[] val);

        /// <summary>
        /// 寫入布林值
        /// </summary>
        /// <param name="val">要寫入的值</param>
        void Write(bool val);

        /// <summary>
        /// 寫入日期時間
        /// </summary>
        /// <param name="val">要寫入的值</param>
        void Write(DateTime val);

        /// <summary>
        /// 寫入32位元整數列表
        /// </summary>
        /// <param name="val">要寫入的列表</param>
        void Write(List<int> val);

        /// <summary>
        /// 寫入64位元整數列表
        /// </summary>
        /// <param name="val">要寫入的列表</param>
        void Write(List<Int64> val);

        /// <summary>
        /// 寫入字串列表
        /// </summary>
        /// <param name="val">要寫入的列表</param>
        void Write(List<string> val);

        /// <summary>
        /// 寫入物件
        /// </summary>
        /// <param name="val">要寫入的物件</param>
        void Write(ISerializable val);

        /// <summary>
        /// 安全寫入物件,避免null參照
        /// </summary>
        /// <param name="val">要寫入的物件</param>
        void SafeWrite(ISerializable val);

        /// <summary>
        /// 寫入物件列表
        /// </summary>
        /// <typeparam name="T">物件型別</typeparam>
        /// <param name="val">要寫入的列表</param>
        void Write<T>(List<T> val) where T : ISerializable;

        /// <summary>
        /// 寫入Int32列表
        /// </summary>
        /// <param name="val">要寫入的列表</param>
        void Write(Dictionary<int, int> val);

        /// <summary>
        /// 寫入物件列表
        /// </summary>
        /// <typeparam name="T">物件型別</typeparam>
        /// <param name="val">要寫入的列表</param>
        void Write<T>(Dictionary<int, T> val) where T : ISerializable;

        /// <summary>
        /// 寫入物件列表
        /// </summary>
        /// <typeparam name="T">物件型別</typeparam>
        /// <param name="val">要寫入的列表</param>
        void Write<T>(Dictionary<Int64, T> val) where T : ISerializable;

        /// <summary>
        /// 寫入動態型別物件列表
        /// </summary>
        /// <typeparam name="T">物件基底型別</typeparam>
        /// <param name="val">要寫入的列表</param>
        void DynamicWrite<T>(List<T> val) where T : IDynamicSerializable;

        /// <summary>
        /// 寫入動態型別物件
        /// </summary>
        /// <typeparam name="T">物件基底型別</typeparam>
        /// <param name="val">要寫入的物件</param>
        void DynamicWrite<T>(T val) where T : IDynamicSerializable;
    }

    abstract public class DefaultSerializer<T> : ISerializer<T>
    {
        private Dictionary<int, Type> types;
        private Dictionary<Type, int> typeIDs;

        private int currentRegion = 0;

        private int currentIndex = 0;

        public DefaultSerializer()
        {
            Initialize();
        }

        public byte[] Serialize(T data)
        {
            using (var stream = new MemoryStream())
            {
                Serialize(data, stream);

                return stream.ToArray();
            }
        }

        public void Serialize(T data, Stream output)
        {
            using (var writer = new OutputStream(output)) Serialize(data, writer);
        }

        public void Serialize(T data, IOutput output)
        {
            int typeid = GetTypeID(data.GetType());

            output.Write(typeid);

            var obj = data as ISerializable;

            obj.Serialize(output);
        }

        public T DeSerialize(byte[] rawdata)
        {
            return DeSerialize(new MemoryStream(rawdata));
        }

        public T DeSerialize(Stream input)
        {
            using (var reader = new InputStream(input)) return DeSerialize(reader);
        }

        public T DeSerialize(IInput input)
        {
            var typeid = input.ReadInt32();

            var type = GetType(typeid);

            try
            {
                var obj = Activator.CreateInstance(type) as ISerializable;

                obj.DeSerialize(input);

                return (T)obj;
            }
            catch
            {
                KernelService.Logger.Debug("[DESERIALIZE_FAILED] ID = {0}, Type = {1}", typeid, type);
                throw;
            }
        }


        private void Initialize()
        {
            types = new Dictionary<int, Type>();
            typeIDs = new Dictionary<Type, int>();

            RegisterType();
        }

        abstract protected void RegisterType();

        protected void EndRegion()
        {
            currentRegion += 100;
            currentIndex = 0;
        }

        virtual protected void RegisterType<U>()
            where U : ISerializable, T, new()
        {
            var fistInst = new U();

            types.Add(currentRegion + currentIndex, typeof(U));
            typeIDs.Add(typeof(U), currentRegion + currentIndex);

            ++currentIndex;
        }

        virtual protected void RegisterDynamicType<UBase, UDerive>()
            where UBase : IDynamicSerializable
            where UDerive : UBase, new()
        {
            if (!ListSerializeUtil<UBase>.IsAvailable) ListSerializeUtil<UBase>.Initialize();
            ListSerializeUtil<UBase>.RegisterType<UDerive>();
        }

        private int GetTypeID(Type type)
        {
            var id = 0;

            if (typeIDs.TryGetValue(type, out id)) return id;

            return -1;
        }

        private Type GetType(int typeid)
        {
            Type type = null;

            if (types.TryGetValue(typeid, out type)) return type;

            return null;
        }
    }

    public class InputStream : IInput, IDisposable
    {
        private BinaryReader reader;

        public InputStream(byte[] rawdata)
        {
            reader = new BinaryReader(new MemoryStream(rawdata));
        }

        public InputStream(Stream stream)
        {
            reader = new BinaryReader(stream);
        }

        public Int16 ReadInt16()
        {
            return reader.ReadInt16();
        }

        public Int32 ReadInt32()
        {
            return reader.ReadInt32();
        }

        public Int64 ReadInt64()
        {
            return reader.ReadInt64();
        }

        public float ReadFloat()
        {
            return reader.ReadSingle();
        }

        public string ReadString()
        {
            return reader.ReadString();
        }

        public byte ReadByte()
        {
            return reader.ReadByte();
        }

        public byte[] ReadByteArray()
        {
            var count = reader.ReadInt32();
            return reader.ReadBytes(count);
        }

        public bool ReadBoolean()
        {
            return reader.ReadBoolean();
        }

        public DateTime ReadDateTime()
        {
            var ticks = ReadInt64();
            return new DateTime(ticks);
        }

        public List<int> ReadInt32List()
        {
            var count = ReadInt32();
            var list = new List<int>(count);
            for (int i = 0; i < count; ++i) list.Add(ReadInt32());

            return list;
        }

        public List<Int64> ReadInt64List()
        {
            var count = ReadInt32();
            var list = new List<Int64>(count);
            for (int i = 0; i < count; ++i) list.Add(ReadInt64());

            return list;
        }

        public List<string> ReadStringList()
        {
            var count = ReadInt32();
            var list = new List<string>(count);
            for (int i = 0; i < count; ++i) list.Add(ReadString());

            return list;
        }

        public T Read<T>() where T : ISerializable, new()
        {
            var obj = new T();
            obj.DeSerialize(this);
            return obj;
        }

        public T SafeRead<T>() where T : ISerializable, new()
        {
            var isExists = ReadBoolean();
            if (isExists)
            {
                var obj = new T();
                obj.DeSerialize(this);
                return obj;
            }
            return default(T);
        }

        public List<T> ReadList<T>() where T : ISerializable, new()
        {
            var count = ReadInt32();
            var list = new List<T>(count);
            for (int i = 0; i < count; ++i) list.Add(Read<T>());

            return list;
        }

        public Dictionary<int,int> ReadInt32Dictionary()
        {
            var count = ReadInt32();
            var list = new Dictionary<int, int>(count);
            for (int i = 0; i < count; ++i) list.Add(ReadInt32(), ReadInt32());

            return list;
        }

        public Dictionary<int, T> ReadInt32Dictionary<T>() where T : ISerializable, new()
        {
            var count = ReadInt32();
            var list = new Dictionary<int, T>(count);
            for (int i = 0; i < count; ++i) list.Add(ReadInt32(), Read<T>());

            return list;
        }

        public Dictionary<Int64, T> ReadInt64Dictionary<T>() where T : ISerializable, new()
        {
            var count = ReadInt32();
            var list = new Dictionary<Int64, T>(count);
            for (int i = 0; i < count; ++i) list.Add(ReadInt64(), Read<T>());

            return list;
        }

        public List<T> ReadDynamicList<T>() where T : IDynamicSerializable
        {
            return ListSerializeUtil<T>.DeSerialize(this);
        }

        public T ReadDynamic<T>() where T : IDynamicSerializable
        {
            return ListSerializeUtil<T>.DeSerializeOne(this);
        }

        public void Dispose()
        {
            reader.Close();
        }

    }

    public class OutputStream : IOutput, IDisposable
    {
        private Stream stream;
        private BinaryWriter writer;
        public OutputStream(Stream stream)
        {
            this.stream = stream;
            writer = new BinaryWriter(stream);
        }

        public void Write(string val)
        {
            writer.Write(string.IsNullOrEmpty(val) ? "" : val);
        }

        public void Write(short val)
        {
            writer.Write(val);
        }

        public void Write(int val)
        {
            writer.Write(val);
        }

        public void Write(long val)
        {
            writer.Write(val);
        }

        public void Write(float val)
        {
            writer.Write(val);
        }

        public void Write(byte val)
        {
            writer.Write(val);
        }

        public void Write(byte[] val)
        {
            writer.Write(val.Length);
            writer.Write(val);
        }

        public void Write(bool val)
        {
            writer.Write(val);
        }

        public void Write(DateTime val)
        {
            Write(val.Ticks);
        }

        public void Write(List<int> val)
        {
            if (val != null)
            {
                Write(val.Count);
                foreach (var item in val) Write(item);
            }
            else
                Write((int)0);
        }

        public void Write(List<Int64> val)
        {
            if (val != null)
            {
                Write(val.Count);
                foreach (var item in val) Write(item);
            }
            else
                Write((int)0);
        }

        public void Write(List<string> val)
        {
            if (val != null)
            {
                Write(val.Count);
                foreach (var item in val) Write(item);
            }
            else
                Write((int)0);
        }

        public void Write(ISerializable val)
        {
            val.Serialize(this);
        }

        public void SafeWrite(ISerializable val)
        {
            if (val == null)
                Write(false);
            else
            {
                Write(true);
                val.Serialize(this);
            }
        }

        public void Write<T>(List<T> val) where T : ISerializable
        {
            if (val != null)
            {
                Write(val.Count);
                foreach (var item in val) Write(item);
            }
            else
                Write((int)0);
        }

        public void Write(Dictionary<int, int> val)
        {
            if (val != null)
            {
                Write(val.Count);
                foreach (var item in val)
                {
                    Write(item.Key);
                    Write(item.Value);
                }
            }
            else
                Write((int)0);
        }

        public void Write<T>(Dictionary<int, T> val) where T : ISerializable
        {
            if (val != null)
            {
                Write(val.Count);
                foreach (var item in val)
                {
                    Write(item.Key);
                    Write(item.Value);
                }
            }
            else
                Write((int)0);
        }

        public void Write<T>(Dictionary<Int64, T> val) where T : ISerializable
        {
            if (val != null)
            {
                Write(val.Count);
                foreach (var item in val)
                {
                    Write(item.Key);
                    Write(item.Value);
                }
            }
            else
                Write((int)0);
        }

        public void DynamicWrite<T>(List<T> val) where T : IDynamicSerializable
        {
            ListSerializeUtil<T>.Serialize(val, this);
        }

        public void DynamicWrite<T>(T val) where T : IDynamicSerializable
        {
            ListSerializeUtil<T>.Serialize(val, this);
        }

        public void Dispose()
        {
            writer.Close();
        }

    }

    public static class ListSerializeUtil<T> where T : IDynamicSerializable
    {
        public static bool IsAvailable { get; private set; }

        private static Dictionary<Type, byte> ids;
        private static Dictionary<byte, Type> types;

        public static void Initialize()
        {
            ids = new Dictionary<Type, byte>();
            types = new Dictionary<byte, Type>();

            IsAvailable = true;
        }

        public static void RegisterType<U>() where U : T, new()
        {
            var id = (byte)(ids.Count + 1);
            var type = typeof(U);
            if (ids.ContainsKey(type)) KernelService.Logger.Debug("dynamic type already registered, type={0}", type);
            ids[type] = id;
            types[id] = type;
        }

        public static T DeSerializeOne(IInput input)
        {
            var id = input.ReadByte();

            var type = default(Type);

            if (!types.TryGetValue(id, out type)) return default(T);

            var item = (T)Activator.CreateInstance(type);
            item.DeSerialize(input);
            return item;
        }

        public static List<T> DeSerialize(IInput input)
        {
            var count = input.ReadInt32();
            var list = new List<T>(count);

            for (int i = 0; i < count; ++i)
            {
                var item = DeSerializeOne(input);
                if(item != null) list.Add(item);
            }

            return list;
        }

        public static void Serialize(List<T> list, IOutput output)
        {
            if (list == null || list.Count == 0)
            {
                output.Write(0);
                return;
            }


            output.Write(list.Count);

            foreach (var item in list) Serialize(item, output);
        }

        public static void Serialize(T item, IOutput output)
        {
            output.Write(ids[item.GetType()]);
            output.Write(item);
        }
    }

    public static class StringUtils
    {
        public static string ToString<T>(List<T> src)
        {
            if (src == null) return "";
            var slist = new List<string>(src.Count);
            foreach (var item in src) slist.Add(item.ToString());
            return string.Join(",", slist.ToArray());
        }

        public static List<Int64> ToInt64List(string src)
        {
            if (string.IsNullOrEmpty(src)) return new List<long>();

            var slist = src.Split(',');
            var list = new List<Int64>(slist.Length);
            foreach(var item in slist) list.Add(Int64.Parse(item));
            return list;
        }
        public static List<Int32> ToInt32List(string src)
        {
            if (string.IsNullOrEmpty(src)) return new List<int>();

            var slist = src.Split(',');
            var list = new List<Int32>(slist.Length);
            foreach (var item in slist) list.Add(Int32.Parse(item));
            return list;
        }

        public static byte[] GetBytes(string src)
        {
            return Encoding.UTF8.GetBytes(src);
        }
    }
}
