﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mikan
{
    /// <summary>
    /// 定義命令類型別的一般行為
    /// </summary>
    public interface ICommand
    {
        /// <summary>
        /// 是否成功
        /// </summary>
        bool IsSucc { get; }

        /// <summary>
        /// 錯誤訊息
        /// </summary>
        string ErrorMessage { get; }

        /// <summary>
        /// 是否是背景命令
        /// </summary>
        bool IsBackground { get; }
        /// <summary>
        /// 是否為SERVER主動推播的命令
        /// </summary>
        bool IsActivePush { get; }

        /// <summary>
        /// 是否為例行命令(不重要的)
        /// </summary>
        bool IsRegular { get; }
        
        /// <summary>
        /// 是否在失敗時自動重試
        /// </summary>
        bool AutoRetry { get; }

        void AddSyncData(ISyncData data);

        IEnumerable<ISyncData> SelectSyncData();

        int TransferID { get; }

        void InjectTransferID(int id);

        /// <summary>
        /// 取得用於和遠端溝通的URI
        /// </summary>
        string URI { get; }
    }
}
