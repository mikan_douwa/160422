﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mikan
{
    /// <summary>
    /// 同步資料
    /// </summary>
    public interface ISyncData : IDynamicSerializable
    {
        SyncType Type { get; }
    }

    public enum SyncType
    {
        Advance,
        Runtime,
        Delay,
    }

    public class SyncManager<T>
    {
        private Dictionary<Type, SyncDelegate<T>> handlers = new Dictionary<Type, SyncDelegate<T>>();
        public void RegisterHandler<U>(SyncDelegate<T> handler) where U : ISyncData
        {
            handlers[typeof(U)] = handler;
        }

        public void OnSyncData(T obj, ISyncData data)
        {
            SyncDelegate<T> handler = null;

            if (!handlers.TryGetValue(data.GetType(), out handler)) return;

            handler(obj, data);
        }
    }
}
