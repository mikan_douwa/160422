﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Reflection;

namespace Mikan
{
    /// <summary>
    /// 可序列化物件
    /// </summary>
    abstract public class SerializableObject
    {
        private const int STATIC_KEY = 87559;

        private static LCGCryptor cryptor = new LCGCryptor(22, 6641, 19923);

        public static int GetDataVersion(string base64)
        {
            if (base64.StartsWith("$"))
            {
                var version = base64.Substring(1, 2);

                return int.Parse(version);
            }

            return 0;
        }


        private bool isEmpty = true;

        public bool IsEmpty { get { return isEmpty; } }

        virtual public int Version { get { return 0; } }

        /// <summary>
        /// 序列化
        /// </summary>
        /// <returns>回傳序列化後的base64字串</returns>
        public string Serialize()
        {
            return Serialize(STATIC_KEY);
        }

        /// <summary>
        /// 序列化
        /// </summary>
        /// <param name="key">指定加密用的金鑰</param>
        /// <returns>回傳序列化後的base64字串</returns>
        public string Serialize(int key)
        {
            using (var stream = new MemoryStream())
            {
                using (var output = new OutputStream(stream))
                {
                    Serialize(output);

                    var rawdata = stream.ToArray();

                    cryptor.Encode(rawdata, key);

                    var base64 = Convert.ToBase64String(rawdata);

                    if (Version > 0)
                        return "$" + Version.ToString("00") + base64;
                    else
                        return base64;
                }
            }
        }

        /// <summary>
        /// 反序列化
        /// </summary>
        /// <param name="base64">base64字串</param>
        public bool DeSerialize(string base64)
        {
            try
            {
                return DeSerialize(base64, STATIC_KEY);
            }
            catch(Exception e)
            {
                KernelService.Logger.Debug("deserialize fail, message = {0}", e);
                return false;
            }
        }

        /// <summary>
        /// 反序列化
        /// </summary>
        /// <param name="base64">base64字串</param>
        /// <param name="key">解密用的金鑰</param>
        public bool DeSerialize(string base64, int key)
        {
            if (string.IsNullOrEmpty(base64)) return true;

            var version = GetDataVersion(base64);

            if (version != Version) return false;

            if (version > 0) base64 = base64.Substring(3);

            var rawdata = Convert.FromBase64String(base64);

            cryptor.Encode(rawdata, key);

            using (var input = new InputStream(rawdata))
            {
                DeSerialize(input);
            }

            isEmpty = false;

            return true;
        }

        /// <summary>
        /// 序列化
        /// </summary>
        /// <param name="output">要寫入的串流</param>
        abstract protected void Serialize(IOutput output);

        /// <summary>
        /// 反序列化
        /// </summary>
        /// <param name="input">資料來源串流</param>
        abstract protected void DeSerialize(IInput input);
    }

    public class ListObj<T> : SerializableObject
    where T : ISerializable, new()
    {
        public List<T> List;
        protected override void Serialize(IOutput output)
        {
            output.Write(List);
        }

        protected override void DeSerialize(IInput input)
        {
            List = input.ReadList<T>();
        }
    }

    public static class ListObj
    {
        public static string Serialize<T>(List<T> list)
            where T : ISerializable, new()
        {
            var obj = new ListObj<T> { List = list };
            return obj.Serialize();
        }

        public static List<T> DeSerialize<T>(string text)
            where T : ISerializable, new()
        {
            var obj = new ListObj<T>();
            obj.DeSerialize(text);
            return obj.List;
        }

    }

    public class DictionaryObj<T> : SerializableObject
        where T : ISerializable, new()
    {
        public Dictionary<int, T> List;

        public T this[int key]
        {
            get
            {
                return DictionaryUtils.SafeGetValueNullable(List, key);
            }
        }

        /// <inheritdoc />
        protected override void Serialize(IOutput output)
        {
            output.Write(List);
        }

        /// <inheritdoc />
        protected override void DeSerialize(IInput input)
        {
            List = input.ReadInt32Dictionary<T>();
        }
    }
}
