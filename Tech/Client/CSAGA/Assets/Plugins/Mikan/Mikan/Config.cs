﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mikan
{
    public interface IConfig
    {
        string this[string key] { get; }

        string TryGet(string key, string defaultVal);

        T TryGet<T>(string key, T defaultVal);
    }

    public static class ConfigParser
    {
        public static IConfig Parse(string rawdata)
        {
            return Parse(rawdata.Split('\r', '\n'));
        }

        public static IConfig Parse(string[] lines)
        {
            
            var config = new Config();

            foreach (var line in lines)
            {
                var split = line.IndexOf('=');

                if (split < 0) continue;

                var key = line.Substring(0, split);

                var val = line.Substring(split + 1);

                if ((val.StartsWith("'") && val.EndsWith("'")) || (val.StartsWith("\"") && val.EndsWith("\"")))
                {
                    val = val.Substring(1, val.Length - 2);
                }

                config[key] = val;
            }

            return config;
        }

        class Config : IConfig
        {
            private Dictionary<string, string> items = new Dictionary<string, string>();

            public string this[string key]
            {
                set { items[key] = value; }
                get
                {
                    string val = null;

                    if (items.TryGetValue(key, out val)) return val;

                    return null;
                }

            }

            public string TryGet(string key, string defaultVal)
            {
                var val = this[key];

                if (string.IsNullOrEmpty(val)) return defaultVal;

                return val;
            }

            public T TryGet<T>(string key, T defaultVal)
            {
                throw new NotImplementedException();
            }
        }
    }
}
