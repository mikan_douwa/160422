﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.IO;

namespace Mikan
{

    /// <summary>
    /// 可序列化資料類別
    /// </summary>
    /// <typeparam name="TKey">資料索引型別,通常使用列舉</typeparam>
    public class Package<TKey> : ISerializable
    {
        const int ObjectCount = 10001;

        enum ElementType
        {
            Value,
            String,
            Object,
        }

        private static ISerializer<ISerializable> serializer;

        private Dictionary<TKey, ElementType> preview = new Dictionary<TKey, ElementType>();

        internal Dictionary<TKey, Int64> _values;

        internal Dictionary<TKey, string> _strings;

        internal Dictionary<TKey, ISerializable> _objects;

        private bool isSerializeCalled = false;
        
        public static void SetObjectSerializer(ISerializer<ISerializable> _serializer)
        {
            serializer = _serializer;
        }

        protected Dictionary<TKey, Int64> Values
        {
            get
            {
                if (_values == null) _values = new Dictionary<TKey, Int64>();
                return _values;
            }
        }

        protected Dictionary<TKey, string> Strings
        {
            get
            {
                if (_strings == null) _strings = new Dictionary<TKey, string>();
                return _strings;
            }
        }

        protected Dictionary<TKey, ISerializable> Objects
        {
            get
            {
                if (_objects == null) _objects = new Dictionary<TKey, ISerializable>();
                return _objects;
            }
        }

        virtual protected int ToInt(TKey key)
        {
            object _key = key;

            return (int)_key;
        }

        virtual protected TKey ConvertKey(int rawdata)
        {
            object _rawdata = rawdata;

            return (TKey)_rawdata;
        }

        public bool IsExists(TKey key)
        {
            return preview.ContainsKey(key);
        }

        public DateTime GetDateTime(TKey key)
        {
            return GetDateTime(key, DateTime.MinValue);
        }

        public DateTime GetDateTime(TKey key, DateTime defaultValue)
        {
            var ticks = Get<Int64>(key, -1);

            if (ticks == -1) return defaultValue;

            return new DateTime(ticks);
        }

        public string GetString(TKey key)
        {
            return GetString(key, null);
        }

        public string GetString(TKey key, string defaultValue)
        {
            if (_strings == null) return defaultValue;

            string value;

            if (_strings.TryGetValue(key, out value)) return value;

            return defaultValue;
        }

        public T Get<T>(TKey key)
        {
            return Get(key, default(T));
        }

        public T Get<T>(TKey key, T defaultValue)
        {
            if (_values == null) return defaultValue;

            Int64 value;

            if (!_values.TryGetValue(key, out value)) return defaultValue;

            var type = typeof(T);

            if(type.IsEnum) return (T)Convert.ChangeType(value, typeof(int));

            return (T)Convert.ChangeType(value, type);
            //object valueObj = value;

            //return (T)valueObj;
        }

        public T GetObject<T>(TKey key)
            where T : ISerializable
        {
            if (_objects == null) return default(T);

            var obj = default(ISerializable);

            if (!_objects.TryGetValue(key, out obj)) return default(T);

            return (T)obj;
        }

        public ISerializable GetObject(TKey key)
        {
            if (_objects == null) return null;

            var obj = default(ISerializable);

            if (!_objects.TryGetValue(key, out obj)) return null;

            return obj;
        }

        public void Add(TKey key, string value)
        {
            AddString(key, value);
        }

        public void Add(TKey key, Int64 value)
        {
            AddValue(key, value);
        }

        public void Add(TKey key, DateTime value)
        {
            AddValue(key, value.Ticks);
        }

        public void Add(TKey key, bool value)
        {
            AddValue(key, value ? 1 : 0);
        }

        public void Add(TKey key, ISerializable value)
        {
            AddObject(key, value);
        }

        private void AddPreview(TKey key, ElementType type)
        {
            var _key = ToInt(key);
            if (_key == ObjectCount) throw new Exception("duplicate key");
            preview.Add(key, type);
        }

        private void AddValue(TKey key, Int64 value)
        {
            AddPreview(key, ElementType.Value);
            Values.Add(key, value);
        }

        private void AddString(TKey key, string value)
        {
            AddPreview(key, ElementType.String);
            Strings.Add(key, value);
        }

        private void AddObject(TKey key, ISerializable value)
        {
            AddPreview(key, ElementType.Object);
            Objects.Add(key, value);
        }

        /// <inheritdoc />
        public void Serialize(IOutput output)
        {
            OnSerialize();

            if (_objects != null)
            {
                var key = ConvertKey(ObjectCount);
                preview[key] = ElementType.Value;
                Values[key] = _objects.Count;
            }

            var numValue = _values != null ? _values.Count : 0;

            var numString = _strings != null ? _strings.Count : 0;

            var encoder = new Encoder();

            encoder.Add(numValue);

            encoder.Add(numString);

            if (numValue > 0)
            {
                foreach (var item in _values)
                {
                    encoder.Add(ToInt(item.Key));
                    encoder.Add(item.Value);
                }
            }

            if (numString > 0)
            {
                foreach (var item in _strings)
                {
                    encoder.Add(ToInt(item.Key));
                }
            }

            encoder.CommitSome();

            if (numString > 0)
            {
                foreach (var item in _strings)
                {
                    encoder.Add(item.Value);
                }
            }

            encoder.Write(output);

            #region objects

            if (_objects != null)
            {
                foreach (var item in _objects)
                {
                    output.Write(ToInt(item.Key));
                    serializer.Serialize(item.Value, output);
                }
            }

            #endregion

            CustomSerialize(output);
        }

        /// <inheritdoc />
        public void DeSerialize(IInput input)
        {
            var decoder = new Decoder();

            decoder.Read(input);

            var nlst = decoder.nlist;

            var slst = decoder.slist;

            var offset = 0;

            var numValue = nlst[offset];

            var numString = nlst[++offset];

            if (numValue > 0)
            {
                _values = new Dictionary<TKey, Int64>();

                for (int i = 0; i < numValue; ++i)
                {
                    var key = ConvertKey((int)nlst[++offset]);
                    var value = nlst[++offset];
                    _values.Add(key, value);
                }
            }

            if (numString > 0)
            {
                _strings = new Dictionary<TKey, string>();

                for (int i = 0; i < numString; ++i)
                {
                    var key = ConvertKey((int)nlst[++offset]);
                    var str = slst[i];

                    _strings.Add(key, str);
                }
            }

            var objectCount = Get<int>(ConvertKey(ObjectCount));

            if (objectCount > 0)
            {
                _objects = new Dictionary<TKey, ISerializable>(objectCount);

                for (int i = 0; i < objectCount; ++i)
                {
                    var key = ConvertKey(input.ReadInt32());
                    _objects.Add(key, serializer.DeSerialize(input));
                }
            }

            OnDeSerialize();

            CustomDeSerialize(input);
        }

        virtual protected void OnSerialize() { }

        virtual protected void OnDeSerialize() { }

        virtual protected void CustomSerialize(IOutput output) { }

        virtual protected void CustomDeSerialize(IInput input) { }

        class Encoder
        {
            private List<string> nlist = new List<string>();

            private List<string> slist = new List<string>();

            public void Add(Int64 val)
            {
                nlist.Add(val.ToString());
            }

            public void CommitSome()
            {
                Add(string.Join(",", nlist.ToArray()));

                nlist.Clear();
            }

            public void Add(string val)
            {
                slist.Add(val);
            }

            public void Write(IOutput output)
            {
                output.Write(slist.Count);
                foreach (var s in slist) output.Write(s);
            }
        }

        class Decoder
        {
            public List<Int64> nlist;

            public List<string> slist;

            public void Read(IInput input)
            {
                var count = input.ReadInt32();

                var numbers = input.ReadString().Split(',');

                nlist = new List<Int64>(numbers.Length);

                foreach (var item in numbers) nlist.Add(Int64.Parse(item));

                slist = new List<string>(count - 1);

                for (int i = 1; i < count; ++i) slist.Add(input.ReadString());
            }
        }
    }
}
