﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mikan
{
    /// <summary>
    /// 核心服務
    /// </summary>
    public static class KernelService
    {
        private static ILogger logger;

        public static ILogger Logger
        {
            get 
            {
                if (logger == null) return DefaultLogger.Instance;
                return logger; 
            }

            set
            {
                if (logger == null) DefaultLogger.PushAll(value);
                logger = value;
            }
        }

        /// <summary>
        /// 顯示資訊
        /// </summary>
        /// <param name="priority"></param>
        /// <param name="log"></param>
        public static void ShowLog(LogPriority priority, string log)
        {
            if (Logger != null) Logger.AddLog(priority, log);
        }

        /// <summary>
        /// 顯示資訊
        /// </summary>
        /// <param name="priority"></param>
        /// <param name="format"></param>
        /// <param name="args"></param>
        public static void ShowLog(LogPriority priority, string format, params object[] args)
        {
            if (Logger != null) Logger.AddLog(priority, format, args);
        }

        class DefaultLogger : LoggerBase
        {
            public static DefaultLogger Instance = new DefaultLogger();

            private List<Log> logs = new List<Log>();
            public override void AddLog(LogPriority priority, string log)
            {
                logs.Add(new Log { Priority = priority, Value = log });
            }

            public static void PushAll(ILogger target)
            {
                if (Instance == null) return;
                foreach (var item in Instance.logs) target.AddLog(item.Priority, item.Value);
                Instance = null;
            }

            class Log
            {
                public LogPriority Priority;
                public string Value;
            }
        }
    }

    public interface ILogger
    {
        void Debug(string log);
        void Debug(string format, params object[] args);

        void Fatal(string log);
        void Fatal(string format, params object[] args);

        void Trace(string log);
        void Trace(string format, params object[] args);

        void Info(string log);
        void Info(string format, params object[] args);

        void AddLog(LogPriority priority, string log);
        void AddLog(LogPriority priority, string format, params object[] args);
    }

    abstract public class LoggerBase : ILogger
    {
        public void Info(string log)
        {
            AddLog(LogPriority.Info, log);
        }

        public void Info(string format, params object[] args)
        {
            AddLog(LogPriority.Info, format, args);
        }

        public void Debug(string log)
        {
            AddLog(LogPriority.Debug, log);
        }

        public void Debug(string format, params object[] args)
        {
            AddLog(LogPriority.Debug, format, args);
        }

        public void Fatal(string log)
        {
            AddLog(LogPriority.Fatal, log);
        }

        public void Fatal(string format, params object[] args)
        {
            AddLog(LogPriority.Fatal, format, args);
        }

        public void Trace(string log)
        {
            AddLog(LogPriority.Trace, log);
        }

        public void Trace(string format, params object[] args)
        {
            AddLog(LogPriority.Trace, format, args);
        }

        public void AddLog(LogPriority priority, string format, params object[] args)
        {
            AddLog(priority, string.Format(format, args));
        }

        abstract public void AddLog(LogPriority priority, string log);
    }

    public enum LogPriority
    {
        Fatal = -1,
        Debug = 0,
        Info = 1,

        Trace = 99,
    }
}
