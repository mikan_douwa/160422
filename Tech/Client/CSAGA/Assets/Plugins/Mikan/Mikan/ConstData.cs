﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Mikan
{
    /// <summary>
    /// 靜態資料組提供類別
    /// </summary>
    public class ConstDataProvider
    {
        /// <summary>
        /// 資料列表
        /// </summary>
        public List<ConstDataTable> Tables { get; private set; }

        private Dictionary<int, ConstDataTable> idDic;

        private Dictionary<string, ConstDataTable> nameDic;
        
        /// <summary>
        /// 建構子
        /// </summary>
        public ConstDataProvider()
        {
            Tables = new List<ConstDataTable>();

            idDic = new Dictionary<int, ConstDataTable>();

            nameDic = new Dictionary<string, ConstDataTable>();
        }

        /// <summary>
        /// 依ID取得資料表
        /// </summary>
        /// <param name="tableID">資料表ID</param>
        /// <returns>回傳資料表</returns>
        public ConstDataTable GetTable(int tableID)
        {
            ConstDataTable table;

            if (idDic.TryGetValue(tableID, out table)) return table;

            KernelService.Logger.Debug("table not exists, id = {0}", tableID);

            return null;
        }

        /// <summary>
        /// 依名稱取得資料表
        /// </summary>
        /// <param name="tableName">資料表名稱</param>
        /// <returns>回傳資料表</returns>
        public ConstDataTable GetTable(string tableName)
        {
            ConstDataTable table;

            if (nameDic.TryGetValue(tableName, out table)) return table;

            KernelService.Logger.Debug("table not exists, name = {0}", tableName);

            return null;
        }

        /// <summary>
        /// 加入新資料表
        /// </summary>
        /// <param name="table">新資料表</param>
        public void AddTable(ConstDataTable table)
        {
            idDic.Add(table.ID, table);

            nameDic.Add(table.Name, table);

            Tables.Add(table);
        }
    }

    /// <summary>
    /// 靜態資料表
    /// </summary>
    public class ConstDataTable
    {
        /// <summary>
        /// 資料轉換方法
        /// </summary>
        /// <typeparam name="T">要轉換的目標型別</typeparam>
        /// <param name="rawdata">原始資料</param>
        /// <returns>回傳填入原始資料的目標型別實體</returns>
        public delegate T Converter<T>(ConstData rawdata);

        /// <summary>
        /// 資料表名稱
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// 資料表ID
        /// </summary>
        public int ID { get; private set; }

        /// <summary>
        /// 資料ID長度
        /// </summary>
        public int DataSize { get; private set; }

        /// <summary>
        /// 資料表欄位列表
        /// </summary>
        public List<ConstDataColumn> Columns { get; private set; }

        /// <summary>
        /// 資料列表
        /// </summary>
        public List<ConstData> Rows { get; private set; }

        /// <summary>
        /// 是否可存取
        /// </summary>
        public bool IsAccessable { get { return Columns != null; } }

        /// <summary>
        /// 是否尚未填入資料
        /// </summary>
        public bool IsEmpty { get { return Rows == null; } }

        /// <summary>
        /// 轉換整個資料表成目標型別
        /// </summary>
        /// <typeparam name="T">目標型別</typeparam>
        /// <param name="converter">轉換方法</param>
        /// <returns>回傳轉換過的資料表</returns>
        public ConstDataTable<T> Convert<T>(Converter<T> converter)
            where T : ConstDataObject, new()
        {
            var table = new ConstDataTable<T>(Rows.Count);

            foreach (var row in Rows) table.AddRow(converter(row));

            return table;
        }

        /// <summary>
        /// 使用預設行為轉換整個資料表成目標型別
        /// </summary>
        /// <typeparam name="T">目標型別</typeparam>
        /// <returns>回傳轉換過的資料表</returns>
        public ConstDataTable<T> Convert<T>()
            where T : ConstDataObject, new()
        {
            var table = new ConstDataTable<T>(Rows.Count);

            var fields = typeof(T).GetFields();

            foreach (var row in Rows)
            {
                if (row.Explicit != null)
                    table.AddRow((T)row.Explicit);
                else
                {
                    var obj = new T();

                    obj.Convert(row, fields);

                    table.AddRow(obj);
                }
            }

            return table;
        }

        /// <summary>
        /// 加入新欄位
        /// </summary>
        /// <param name="columnName">欄位名稱</param>
        /// <param name="index">索引</param>
        public void AddColumn(string columnName, int index)
        {
            var col = ConstDataColumn.Create(columnName, index);

            if (Columns == null) Columns = new List<ConstDataColumn>();

            Columns.Add(col);
        }

        public void InitColumns(int capacity)
        {
            Columns = new List<ConstDataColumn>(capacity);
        }

        public void InitRows(int capacity)
        {
            Rows = new List<ConstData>(capacity);
        }

        /// <summary>
        /// 加入新資料
        /// </summary>
        /// <param name="row">新資料</param>
        public void AddRow(ConstData row)
        {
            if (Rows == null) Rows = new List<ConstData>();

            Rows.Add(row);
        }

        /// <summary>
        /// 建立新的資料表
        /// </summary>
        /// <param name="tableName">資料表名稱</param>
        /// <param name="tableID">資料表ID</param>
        /// <param name="dataSize">資料表ID長度</param>
        /// <returns>回傳新建的資料表</returns>
        public static ConstDataTable Create(string tableName, int tableID, int dataSize)
        {
            var table = new ConstDataTable();

            table.Name = tableName;

            table.ID = tableID;

            table.DataSize = dataSize;

            return table;
        }
    }

    /// <summary>
    /// 靜態資料基底類別
    /// </summary>
    public class ConstDataObject
    {
        static string[] linkStrings = { "_", "" };

        /// <summary>
        /// 資料ID
        /// </summary>
        public int n_ID;

        /// <summary>
        /// 轉換資料
        /// </summary>
        /// <param name="row"></param>
        virtual public void Convert(ConstData row)
        {
            var fields = GetType().GetFields();

            Convert(row, fields);
        }

        /// <summary>
        /// 轉換資料
        /// </summary>
        /// <param name="row"></param>
        virtual public void Convert(ConstData row, System.Reflection.FieldInfo[] fields)
        {
            foreach (var field in fields)
            {
                try
                {
                    if (field.FieldType.IsArray)
                    {
                        if (field.Name.StartsWith("n_"))
                            field.SetValue(this, ConvertArray<int>(row, field.Name));
                        else if (field.Name.StartsWith("s_"))
                            field.SetValue(this, ConvertArray<string>(row, field.Name));
                        else if (field.Name.StartsWith("f_"))
                            field.SetValue(this, ConvertArray<float>(row, field.Name));
                    }
                    else if (row.Contains(field.Name))
                    {
                        field.SetValue(this, row[field.Name]);
                    }
                }
                catch (Exception e)
                {
                    throw new Exception(string.Format("constdata convert failed, row_id={0}, field_name={1}", n_ID, field.Name), e);
                }
            }
        }

        public static T[] ConvertArray<T>(ConstData row, string prefix)
        {
            var list = new List<T>();

            foreach (var linkString in linkStrings)
            {
                for (int i = 1; ; ++i)
                {
                    var column = prefix + linkString + i.ToString();
                    if (row.Contains(column))
                        list.Add(row.Field<T>(column));
                    else
                        break;
                }

                if (list.Count > 0) break;
            }

            return list.ToArray();
        }

        public static T[] ConvertArray<T>(ConstData row, params string[] columns)
        {
            var list = new T[columns.Length];

            for (int i = 0; i < columns.Length; ++i)
            {
                list[i] = row.Field<T>(columns[i]);
            }

            return list;
        }
    }

    public class IntWrapper
    {
        public int Value;


        public static IntWrapper Create(int value)
        {
            return new IntWrapper { Value = value };
        }

        private class Comparer : IEqualityComparer<IntWrapper>
        {
            public bool Equals(IntWrapper x, IntWrapper y)
            {
                return x.Value == y.Value;
            }

            public int GetHashCode(IntWrapper obj)
            {
                return obj.Value.GetHashCode();
            }
        }

        private static Comparer DefaultComparer = new Comparer();

        public static Dictionary<IntWrapper, T> CreateDictinary<T>()
        {
            return new Dictionary<IntWrapper, T>(DefaultComparer);
        }

        public static Dictionary<IntWrapper, T> CreateDictinary<T>(int capacity)
        {
            return new Dictionary<IntWrapper, T>(capacity, DefaultComparer);
        }
    }

    /// <summary>
    /// 轉換過的靜態資料表
    /// </summary>
    /// <typeparam name="T">資料型別</typeparam>
    public class ConstDataTable<T>
        where T : ConstDataObject
    {
        internal Dictionary<IntWrapper, T> rows;

        public ConstDataTable(int capacity)
        {
            rows = IntWrapper.CreateDictinary<T>(capacity);
        }

        /// <summary>
        /// 資料個數
        /// </summary>
        public int Count { get { return rows.Count; } }

        /// <summary>
        /// 檢查資料是否存在
        /// </summary>
        /// <param name="id">資料ID</param>
        /// <returns>資料是否存在</returns>
        public bool IsExists(int id) { return rows.ContainsKey(IntWrapper.Create(id)); }

        /// <summary>
        /// 列舉所有資料
        /// </summary>
        /// <returns>回傳資料列舉</returns>
        public IEnumerable<T> Select()
        {
            foreach (var item in rows) yield return item.Value;
        }

        /// <summary>
        /// 列舉所有符合條件的資料
        /// </summary>
        /// <param name="filter">過濾函式</param>
        /// <returns>回傳資料列舉</returns>
        public IEnumerable<T> Select(Filter<T> filter)
        {
            foreach (var item in rows)
            {
                if(filter(item.Value)) yield return item.Value;
            }
        }

        /// <summary>
        /// 取得第一個符合條件的資料
        /// </summary>
        /// <param name="filter">過濾函式</param>
        /// <returns>回傳第一個符合條件的資料</returns>
        public T SelectFirst(Filter<T> filter)
        {
            foreach (var item in Select(filter)) return item;

            return default(T);
        }

        /// <summary>
        /// 依ID取得資料
        /// </summary>
        /// <param name="id">資料ID</param>
        /// <returns></returns>
        public T this[int id]
        {
            get {
                T val;
                if (rows.TryGetValue(IntWrapper.Create(id), out val)) return val;
                return default(T);
            }
        }

        internal void AddRow(T row)
        {
            rows.Add(IntWrapper.Create(row.n_ID), row);
        }
    }

    /// <summary>
    /// 靜態資料
    /// </summary>
    public class ConstData
    {
        public ConstDataObject Explicit;
        private Dictionary<string, ConstDataValue> values = new Dictionary<string, ConstDataValue>();

        /// <summary>
        /// 是否包含該欄位
        /// </summary>
        /// <param name="column">欄位名稱</param>
        /// <returns>回傳是否包含</returns>
        public bool Contains(string column) { return values.ContainsKey(column); }

        /// <summary>
        /// 取得欄位內含的值
        /// </summary>
        /// <param name="column">欄位名稱</param>
        /// <returns>回傳值</returns>
        public object this[string column]
        {
            get
            {
                ConstDataValue val = GetValue(column);

                return val != null ? val.GetValue() : null;
            }
        }

        /// <summary>
        /// 依欄位取得值
        /// </summary>
        /// <typeparam name="T">值型別</typeparam>
        /// <param name="column">欄位名稱</param>
        /// <returns>回傳值</returns>
        public T Field<T>(string column)
        {
            return Field<T>(column, default(T));
        }

        /// <summary>
        /// 依欄位取得值
        /// </summary>
        /// <typeparam name="T">值型別</typeparam>
        /// <param name="column">欄位名稱</param>
        /// <param name="defaultValue">找不到資料時的預設回傳值</param>
        /// <returns>回傳值</returns>
        public T Field<T>(string column, T defaultValue)
        {
            ConstDataValue _val = GetValue(column);

            if (_val == null) return defaultValue;

            ConstDataValue<T> val = _val as ConstDataValue<T>;

            if (val == null) throw new InvalidCastException();

            return val.Value;
        }

        /// <summary>
        /// 設值
        /// </summary>
        /// <typeparam name="T">值型別</typeparam>
        /// <param name="column">欄位名稱</param>
        /// <param name="val">值</param>
        public void SetField<T>(string column, T val)
        {
            values[column] = new ConstDataValue<T> { Value = val };
        }

        private ConstDataValue GetValue(string column)
        {
            ConstDataValue val;

            if (!values.TryGetValue(column, out val)) return null;

            return val;
        }
    }

    /// <summary>
    /// 靜態資料欄位型別
    /// </summary>
    public enum ConstDataValueType
    {
        /// <summary>
        /// 字串
        /// </summary>
        STRING,
        /// <summary>
        /// 整數
        /// </summary>
        INTEGER,
        /// <summary>
        /// 浮點數
        /// </summary>
        FLOAT
    }

    /// <summary>
    /// 靜態資料欄位資訊
    /// </summary>
    public class ConstDataColumn
    {
        /// <summary>
        /// 欄位名稱
        /// </summary>
        public string Name { get; private set; }
        /// <summary>
        /// 欄位型別
        /// </summary>
        public ConstDataValueType ValueType { get; private set; }
        /// <summary>
        /// 欄位在原表格中的索引
        /// </summary>
        public int Index { get; private set; }

        /// <summary>
        /// 建立資料欄位實體
        /// </summary>
        /// <param name="columnName">欄位名稱</param>
        /// <param name="index">索引</param>
        /// <returns>回傳資料欄位實體</returns>
        public static ConstDataColumn Create(string columnName, int index)
        {
            var column = new ConstDataColumn { Name = columnName, Index = index };

            if (!string.IsNullOrEmpty(columnName))
            {
                if (columnName.StartsWith("s_"))
                    column.ValueType = ConstDataValueType.STRING;
                else if (columnName.StartsWith("n_"))
                    column.ValueType = ConstDataValueType.INTEGER;
                else if (columnName.StartsWith("f_"))
                    column.ValueType = ConstDataValueType.FLOAT;
                else
                    throw new NotSupportedException();

                return column;
            }

            throw new NotSupportedException();
        }
    }

    abstract internal class ConstDataValue
    {
        abstract public object GetValue();
    }

    internal class ConstDataValue<T> : ConstDataValue
    {
        public T Value;

        public override object GetValue()
        {
            return Value;
        }
    }

    public interface ICustomObjectReader
    {
        ConstDataObject CreateNewObj();
        bool SetValue(BinaryReader reader, string column);
        void Clear();

    }

    /// <summary>
    /// 靜態資料讀取器
    /// </summary>
    public class ConstDataReader : IDisposable
    {
        private ConstDataProvider provider = new ConstDataProvider();

        private Dictionary<string, ICustomObjectReader> readers;

        /// <inheritdoc />
        public void Dispose() { }

        public void Register(string tablename, ICustomObjectReader reader)
        {
            if (readers == null) readers = new Dictionary<string, ICustomObjectReader>();
            readers[tablename] = reader;
        }

        /// <summary>
        /// 整合資料
        /// </summary>
        /// <returns>回傳整合過的資料提供者</returns>
        public ConstDataProvider Combine()
        {
            return provider;
        }

        /// <summary>
        /// 讀入資料
        /// </summary>
        /// <param name="stream">資料串流</param>
        public void Load(Stream stream)
        {
            using (var reader = new BinaryReader(stream))
            {
                var tableCount = reader.ReadInt32();
                for (int i = 0; i < tableCount; ++i)
                {                    
                    // 讀取資料表資訊
                    var tableName = ReadString(reader);
                    var id = reader.ReadInt32();
                    var dataSize = reader.ReadInt32();

                    var table = ConstDataTable.Create(tableName, id, dataSize);

                    // 讀取欄位資訊
                    var columnCount = reader.ReadInt32();

                    table.InitColumns(columnCount);

                    for (int j = 0; j < columnCount; ++j)
                    {
                        var columnName = ReadString(reader);
                        var index = reader.ReadInt32();

                        table.AddColumn(columnName, index);
                    }

#if PROFILE
                    var beginTime = DateTime.Now;
#endif
                    // 讀取資料
                    var rowCount = reader.ReadInt32();

                    table.InitRows(rowCount);

                    var customReader = default(ICustomObjectReader);

                    if (readers != null)
                    {
                        if (!readers.TryGetValue(tableName, out customReader)) customReader = null;
                    }

                    for(int j = 0; j < rowCount; ++j)
                    {
                        var row = new ConstData();

                        if (customReader != null)
                        {
                            row.Explicit = customReader.CreateNewObj();

                            foreach (var column in table.Columns)
                            {
                                if (customReader.SetValue(reader, column.Name)) continue;

                                switch (column.ValueType)
                                {
                                    case ConstDataValueType.STRING:
                                        ReadString(reader);
                                        break;
                                    case ConstDataValueType.INTEGER:
                                        reader.ReadInt32();
                                        break;
                                    case ConstDataValueType.FLOAT:
                                        reader.ReadSingle();
                                        break;
                                }
                            }

                            customReader.Clear();
                        }
                        else
                        {

                            foreach (var column in table.Columns)
                            {
                                switch (column.ValueType)
                                {
                                    case ConstDataValueType.STRING:
                                        row.SetField<string>(column.Name, ReadString(reader));
                                        break;
                                    case ConstDataValueType.INTEGER:
                                        row.SetField<int>(column.Name, reader.ReadInt32());
                                        break;
                                    case ConstDataValueType.FLOAT:
                                        row.SetField<float>(column.Name, reader.ReadSingle());
                                        break;
                                }
                            }
                        }

                        table.AddRow(row);
                    }

#if PROFILE
                    var endTime = DateTime.Now;
                    KernelService.Logger.Debug("load rawdata table = {0}, cost = {1}", tableName, endTime.Subtract(beginTime).TotalMilliseconds);
#endif

                    provider.AddTable(table);
                }
            }
        }

        /// <summary>
        /// 讀入資料
        /// </summary>
        /// <param name="filename">檔案路徑</param>
        public void Load(string filename)
        {
            Load(new FileStream(filename, FileMode.Open, FileAccess.Read));
        }

        public static string ReadString(BinaryReader reader)
        {
            var length = reader.ReadInt32();
            var bytes = reader.ReadBytes(length);

            return Encoding.UTF8.GetString(bytes);
        }
    }
#if SERVER_CODE
    /// <summary>
    /// 靜態資料寫入類別
    /// </summary>
    public class ConstDataWriter : IDisposable
    {
        /// <inheritdoc />
        public void Dispose() { }

        /// <summary>
        /// 將資料寫入檔案
        /// </summary>
        /// <param name="provider">資料提供者</param>
        /// <param name="filename">檔案路徑</param>
        public void Write(ConstDataProvider provider, string filename)
        {
            using (var writer = new BinaryWriter(File.Create(filename)))
            {
                writer.Write(provider.Tables.Count);

                foreach (var table in provider.Tables)
                {
                    // 寫入資料表資訊
                    WriteString(writer, table.Name);
                    writer.Write(table.ID);
                    writer.Write(table.DataSize);

                    // 寫入欄位資訊
                    writer.Write(table.Columns.Count);
                    foreach (var column in table.Columns)
                    {
                        WriteString(writer, column.Name);
                        writer.Write(column.Index);
                    }

                    // 寫入資料
                    writer.Write(table.Rows.Count);
                    foreach (var row in table.Rows)
                    {
                        foreach (var column in table.Columns)
                        {
                            switch (column.ValueType)
                            {
                                case ConstDataValueType.STRING:
                                    WriteString(writer, row.Field<string>(column.Name));
                                    break;
                                case ConstDataValueType.INTEGER:
                                    writer.Write(row.Field<int>(column.Name));
                                    break;
                                case ConstDataValueType.FLOAT:
                                    writer.Write(row.Field<float>(column.Name));
                                    break;
                            }
                        }
                    }
                }
            }
        }

        private void WriteString(BinaryWriter writer, string val)
        {
            var bytes = Encoding.UTF8.GetBytes(val);
            writer.Write(bytes.Length);
            writer.Write(bytes);
        }
    }
#endif
}
