﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Text;

namespace Mikan
{
    /// <summary>
    /// 任務執行失敗要呼叫的方法
    /// </summary>
    /// <typeparam name="T">任務型別</typeparam>
    /// <param name="task">失敗的任務</param>
    /// <param name="e">拋出的例外</param>
    public delegate void TaskFailCallback<T>(T task, Exception e);

    /// <summary>
    /// 任務管理類別
    /// </summary>
    /// <typeparam name="T">任務型別</typeparam>
    public interface ITaskManger<T> : IDisposable
    {
        /// <summary>
        /// 任務執行失敗事件
        /// </summary>
        event TaskFailCallback<T> OnTaskFail;

        /// <summary>
        /// 加入新任務
        /// </summary>
        /// <param name="task"></param>
        void AddTask(T task);
    }

    /// <summary>
    /// 任務執行緒
    /// </summary>
    /// <typeparam name="T">任務型別</typeparam>
    abstract public class TaskThread<T> : ITaskManger<T>
        where T : IDisposable
    {
        /// <summary>
        /// 任務執行失敗事件
        /// </summary>
        public event TaskFailCallback<T> OnTaskFail;

        private ManualResetEvent eClose;

        private Semaphore semaphore;

        private Queue<T> queue;

        private List<Worker> workers;

        /// <summary>
        /// 建構子
        /// </summary>
        public TaskThread()
        {
            Initialize(1);
        }

        /// <summary>
        /// 建構子
        /// </summary>
        /// <param name="threadCount">要開啟的執行緒數量</param>
        public TaskThread(int threadCount)
        {
            Initialize(threadCount);
        }

        private void Initialize(int threadCount)
        {
            eClose = new ManualResetEvent(false);

            semaphore = new Semaphore(0, int.MaxValue);

            queue = new Queue<T>();

            workers = new List<Worker>(threadCount);

            for (int i = 0; i < threadCount; ++i)
            {
                var worker = CreateWorker();
                workers.Add(worker);
                worker.Run();
            }
        }

        /// <inheritdoc />
        public void AddTask(T task)
        {
            lock (queue) queue.Enqueue(task);

            semaphore.Release();
        }

        /// <inheritdoc />
        public void Dispose()
        {
            eClose.Set();

            foreach (var worker in workers) worker.Join();

            workers.Clear();

            workers = null;

            eClose.Close();

            semaphore.Close();

            queue.Clear();

            queue = null;
        }

        private Worker CreateWorker()
        {
            var worker = new Worker();

            worker.eClose = eClose;

            worker.semaphore = semaphore;

            worker.queue = queue;

            worker.OnStart = OnStart;

            worker.OnEnd = OnEnd;

            worker.OnTaskFail = onTaskFail;

            worker.ExecuteTask = ExecuteTask;

            return worker;
        }

        /// <summary>
        /// 執行任務,由子類別覆寫
        /// </summary>
        /// <param name="task">輪到要執行的任務</param>
        abstract protected void ExecuteTask(T task);

        /// <summary>
        /// 開始運行
        /// </summary>
        virtual protected void OnStart() { }

        /// <summary>
        /// 結束運行
        /// </summary>
        virtual protected void OnEnd() { }

        private void onTaskFail(T task, Exception e)
        {
            if (OnTaskFail != null) OnTaskFail(task, e);
        }

        class Worker
        {
            public delegate void Callback();

            public Callback OnStart;

            public Callback OnEnd;

            public TaskFailCallback<T> OnTaskFail;

            public Action<T> ExecuteTask;

            public Queue<T> queue;

            public Semaphore semaphore;

            public ManualResetEvent eClose;

            private Thread thread;

            public void Run()
            {
                thread = new Thread(ThreadEntrance);

                thread.Start();
            }

            public void Join()
            {
                thread.Join();
            }

            private void ThreadEntrance()
            {
                if (OnStart != null) OnStart();

                //const int E_CLOSE = 0;
                const int E_GOT_A_EVENT = 1;

                WaitHandle[] handles = { eClose, semaphore };

                while (true)
                {
                    var res = WaitHandle.WaitAny(handles);

                    if (res != E_GOT_A_EVENT) break;

                    using (var task = Dequeue())
                    {
                        try
                        {
                            if (ExecuteTask != null) ExecuteTask(task);
                        }
                        catch (Exception e)
                        {
                            if (OnTaskFail != null) OnTaskFail(task, e);
                        }
                    }

                }

                if (OnEnd != null) OnEnd();
                   
                
            }

            private T Dequeue()
            {
                lock (queue) return queue.Dequeue();
            }
        }
    }

    /// <summary>
    /// 簡易任務
    /// </summary>
    public interface ITask : IDisposable
    {
        /// <summary>
        /// 執行任務
        /// </summary>
        void Execute();
    }

    /// <summary>
    /// 簡易任務執行緒
    /// </summary>
    public class TaskThread : TaskThread<ITask>
    {
        /// <inheritdoc />
        protected override void ExecuteTask(ITask task)
        {
            task.Execute();
        }
    }

}
