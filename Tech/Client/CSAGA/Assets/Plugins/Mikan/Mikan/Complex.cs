﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mikan
{
    public class Complex : ISerializable
    {
        public Int64 Value;

        public bool this[int index]
        {
            get
            {
                return (Value & (1 << index)) != 0;
            }
            set
            {
                Int64 val = (Int64)1 << index;

                Value |= val;

                if (!value) Value ^= val;

            }
        }

        public int Get(int pos, int len)
        {
            var mask = (1 << len) - 1;
            var val = Value >> pos;
            return (int)(val & mask);
        }

        public void Set(int pos, int len, int val)
        {
            var mask = (1 << len) - 1;
            if ((val & mask) != val) throw new ArgumentOutOfRangeException();
            for (int i = 0; i < len; ++i) this[i + pos] = false;

            var newVal = (Int64)val << pos;
            Value |= newVal;
        }

        public void Serialize(IOutput output)
        {
            output.Write(Value);
        }

        public void DeSerialize(IInput input)
        {
            Value = input.ReadInt64();
        }

        public Int64 Combin(Complex obj)
        {
            return Value | obj.Value;
        }

        public static Complex Bit(int index)
        {
            var complex = new Complex();
            complex[index] = true;
            return complex;
        }
    }

    public class ComplexI32 : ISerializable
    {
        public int Value;

        public bool this[int index]
        {
            get
            {
                return Check(Value, index);
            }
            set
            {
                int val = 1 << index;

                Value |= val;

                if (!value) Value ^= val;

            }
        }

        public int Get(int pos, int len)
        {
            var mask = (1 << len) - 1;
            var val = Value >> pos;
            return (int)(val & mask);
        }

        public void Set(int pos, int len, int val)
        {
            var mask = (1 << len) - 1;
            if ((val & mask) != val) throw new ArgumentOutOfRangeException();
            for (int i = 0; i < len; ++i) this[i + pos] = false;

            var newVal = (int)val << pos;
            Value |= newVal;
        }

        public void Serialize(IOutput output)
        {
            output.Write(Value);
        }

        public void DeSerialize(IInput input)
        {
            Value = input.ReadInt32();
        }

        public int Combin(ComplexI32 obj)
        {
            return Value | obj.Value;
        }

        public int Separate(int index)
        {
            var copy = new ComplexI32 { Value = Value };
            copy[index] = false;
            return copy.Value;
        }

        public static ComplexI32 Bit(int index)
        {
            var complex = new ComplexI32();
            complex[index] = true;
            return complex;
        }

        public static bool Check(int value, int index)
        {
            return (value & (1 << index)) != 0;
        }
    }
}
