﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.CompilerServices;

namespace Mikan
{
    public static class DictionaryUtils
    {
        public static TValue SafeGetValue<TKey, TValue>(Dictionary<TKey, TValue> inst, TKey key)
            where TValue : new()
        {
            TValue val;
            if (!inst.TryGetValue(key, out val))
            {
                val = new TValue();
                inst.Add(key, val);
            }

            return val;
        }

        public static TValue SafeGetValueNullable<TKey, TValue>(Dictionary<TKey, TValue> inst, TKey key)
        {
            TValue val;
            if (inst.TryGetValue(key, out val)) return val;

            return default(TValue);
        }
    }
}
