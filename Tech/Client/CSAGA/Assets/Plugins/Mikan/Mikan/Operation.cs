﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mikan
{
    using OpList = List<IOperation>;

    /// <summary>
    /// 流程作業一般行為
    /// </summary>
    public interface IOperation : IDisposable
    {
        /// <summary>
        /// 作業完成事件
        /// </summary>
        event Action<IOperation> OnComplete;
        /// <summary>
        /// 開始執行
        /// </summary>
        void Execute();
        /// <summary>
        /// 停止執行
        /// </summary>
        void Stop();
    }

    /// <summary>
    /// 預設流程作業基底類別
    /// </summary>
    abstract public class OperationBase : IOperation
    {
        /// <inheritdoc />
        public event Action<IOperation> OnComplete;

        /// <inheritdoc />
        abstract public void Execute();

        /// <inheritdoc />
        virtual public void Stop() { }

        /// <inheritdoc />
        virtual public void Dispose() { OnComplete = null; }

        /// <summary>
        /// 作業完成時呼叫的方法
        /// </summary>
        protected void OperationComplete()
        {
            if (OnComplete != null) OnComplete(this);
        }
    }

    /// <summary>
    /// 流程作業鏈基底類別
    /// </summary>
    abstract public class OperationChain : OperationBase
    {
        /// <summary>
        /// 流程作業列表
        /// </summary>
        protected OpList list { get; private set; }

        /// <summary>
        /// 建構子
        /// </summary>
        /// <param name="list">流程作業列表</param>
        public OperationChain(OpList list)
        {
            this.list = list;

            foreach (var op in list) op.OnComplete += OnSubOperationComplete;
        }

        /// <summary>
        /// 建構子
        /// </summary>
        /// <param name="list">流程作業列表</param>
        public OperationChain(params IOperation[] list)
        {
            this.list = new OpList(list);

            foreach (var op in list) op.OnComplete += OnSubOperationComplete;
        }

        /// <inheritdoc />
        override public void Dispose()
        {
            Stop();

            foreach (var op in list)
            {
                op.OnComplete -= OnSubOperationComplete;
                op.Dispose();
            }

            list = null;

            base.Dispose();
        }

        /// <summary>
        /// 當一個作業完成時呼叫的方法
        /// </summary>
        /// <param name="obj">完成的作業</param>
        abstract protected void OnSubOperationComplete(IOperation obj);
    }

    /// <summary>
    /// 順序流程作業鍵
    /// </summary>
    public class SequentialOperationChain : OperationChain
    {
        private int curIndex = 0;

        /// <summary>
        /// 建構子
        /// </summary>
        /// <param name="list">流程作業列表</param>
        public SequentialOperationChain(OpList list)
            : base(list) { }

        /// <summary>
        /// 建構子
        /// </summary>
        /// <param name="list">流程作業列表</param>
        public SequentialOperationChain(params IOperation[] list)
            : base(list) { }

        /// <inheritdoc />
        override public void Execute()
        {
            Stop();
            Next();
        }

        /// <inheritdoc />
        override public void Stop()
        {
            if(curIndex >= 0 && curIndex < list.Count)
                list[curIndex].Stop();

            curIndex = -1;
        }

        /// <inheritdoc />
        protected override void OnSubOperationComplete(IOperation obj)
        {
            Next();
        }

        private void Next()
        {
            ++curIndex;

            if (curIndex < list.Count)
                list[curIndex].Execute();
            else
            {
                curIndex = -1;

                OperationComplete();
            }
        }

    }

    /// <summary>
    /// 同步流程作業鏈
    /// </summary>
    public class ParallelOperationChain : OperationChain
    {
        private int numComplete;

        /// <summary>
        /// 建構子
        /// </summary>
        /// <param name="list">流程作業列表</param>
        public ParallelOperationChain(params IOperation[] list)
            : base(list) { }

        /// <summary>
        /// 建構子
        /// </summary>
        /// <param name="list">流程作業列表</param>
        public ParallelOperationChain(OpList list)
            : base(list) { }

        /// <inheritdoc />
        public override void Execute()
        {
            Stop();

            foreach (var item in list) item.Execute();
        }

        /// <inheritdoc />
        public override void Stop()
        {
            foreach (var item in list) item.Stop();

            numComplete = 0;
        }

        /// <inheritdoc />
        protected override void OnSubOperationComplete(IOperation obj)
        {
            if (++numComplete == list.Count) OperationComplete();
        }
    }

    public class LoopOperation : OperationBase
    {
        private IOperation op;
        private int loop;

        private int count;

        public LoopOperation(IOperation op, int loop = int.MaxValue)
        {
            this.op = op;
            this.loop = loop;

            op.OnComplete += OnInternalOperationComplete;
        }

        void OnInternalOperationComplete(IOperation obj)
        {
            if (++count < loop)
                op.Execute();
            else
                OperationComplete();
        }

        /// <inheritdoc />
        public override void Execute()
        {
            op.Stop();
            count = 0;
            op.Execute();
        }

        public override void Stop()
        {
            op.Stop();
        }

        /// <inheritdoc />
        public override void Dispose()
        {
            op.Dispose();
            base.Dispose();
        }
    }

    public class CallbackOperation : OperationBase
    {
        private Callback callback;
        public CallbackOperation(Callback callback)
        {
            this.callback = callback;
        }

        /// <inheritdoc />
        public override void Execute()
        {
            callback();
            OperationComplete();
        }

        /// <inheritdoc />
        public override void Dispose()
        {
            callback = null;
            base.Dispose();
        }
    }

    public class CallbackOperation<T> : OperationBase
    {
        private Action<T> callback;
        private T param;
        public CallbackOperation(Action<T> callback, T param)
        {
            this.callback = callback;
            this.param = param;
        }

        /// <inheritdoc />
        public override void Execute()
        {
            callback(param);
            OperationComplete();
        }

        /// <inheritdoc />
        public override void Dispose()
        {
            callback = null;
            base.Dispose();
        }
    }

    public class CallbackOperation<T1, T2> : OperationBase
    {
        public delegate void Callback(T1 param1, T2 param2);
        private Callback callback;
        private T1 param1;
        private T2 param2;
        public CallbackOperation(Callback callback, T1 param1, T2 param2)
        {
            this.callback = callback;
            this.param1 = param1;
            this.param2 = param2;
        }

        /// <inheritdoc />
        public override void Execute()
        {
            callback(param1, param2);
            OperationComplete();
        }

        /// <inheritdoc />
        public override void Dispose()
        {
            callback = null;
            base.Dispose();
        }
    }

    public class CallbackOperation<T1, T2, T3> : OperationBase
    {
        public delegate void Callback(T1 param1, T2 param2, T3 param3);
        private Callback callback;
        private T1 param1;
        private T2 param2;
        private T3 param3;
        public CallbackOperation(Callback callback, T1 param1, T2 param2, T3 param3)
        {
            this.callback = callback;
            this.param1 = param1;
            this.param2 = param2;
            this.param3 = param3;
        }

        /// <inheritdoc />
        public override void Execute()
        {
            callback(param1, param2, param3);
            OperationComplete();
        }

        /// <inheritdoc />
        public override void Dispose()
        {
            callback = null;
            base.Dispose();
        }
    }

    /// <summary>
    /// 流程作業串列
    /// </summary>
    public class LiveOperationQueue : IDisposable
    {
        public delegate void NotifyDelegate(LiveOperationQueue sender, IOperation current);
        public delegate void ErrorDelegate(LiveOperationQueue sender, IOperation current, string errMsg);

        public static event NotifyDelegate OnOperationBegin;
        public static event NotifyDelegate OnOperationEnd;
        public static event ErrorDelegate OnError;

        public static event NotifyDelegate OnClear;

        public static void Reset()
        {
            OnOperationBegin = null;
            OnOperationEnd = null;
            OnError = null;
            OnClear = null;
        }

        private Queue<IOperation> queue = new Queue<IOperation>();

        private IOperation curOp = null;

        public bool IsEmpty { get { return curOp == null && queue.Count == 0; } }

        /// <summary>
        /// 加入新的作業
        /// </summary>
        /// <param name="op">流程作業實體</param>
        public void Enqueue(IOperation op)
        {
            queue.Enqueue(op);

            if (curOp == null) Next();
        }

        /// <summary>
        /// 停止執行中的作業並清空串列
        /// </summary>
        public void Clear()
        {
            if (curOp != null)
            {
                curOp.OnComplete -= OnComplete;
                curOp.Dispose();
                curOp = null;
            }

            while (queue.Count > 0) queue.Dequeue().Dispose();

            if (OnClear != null) OnClear(this, null);
        }

        /// <inheritdoc />
        public void Dispose()
        {
            Clear();
        }

        private void Next()
        {
            if (queue.Count == 0)
            {
                curOp = null;
                return;
            }

            curOp = queue.Dequeue();

            curOp.OnComplete += OnComplete;

            if (OnOperationBegin != null) OnOperationBegin(this, curOp);

            try
            {
                curOp.Execute();
            }
            catch (Exception ex)
            {
                if (OnError != null) OnError(this, curOp, ex.ToString());
            }
        }

        private void OnComplete(IOperation op)
        {
            op.OnComplete -= OnComplete;

            op.Dispose();

            if (OnOperationEnd != null) OnOperationEnd(this, op);

            Next();
        }
    }
}
