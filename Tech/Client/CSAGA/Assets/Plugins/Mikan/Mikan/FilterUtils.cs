﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mikan
{
    /// <summary>
    /// 過濾工具函式
    /// </summary>
    public static class FilterUtils
    {
        /// <summary>
        /// 列舉所有符合條件的物件
        /// </summary>
        /// <typeparam name="T">物件型別</typeparam>
        /// <param name="source">資料來源</param>
        /// <param name="filter">過濾函式</param>
        /// <returns>回傳所有符合條件的物件</returns>
        public static IEnumerable<T> Select<T>(IEnumerable<T> source, Filter<T> filter)
        {
            foreach (var item in source)
            {
                if (filter(item)) yield return item;
            }
        }

        /// <summary>
        /// 取得第一個符合條件的物件
        /// </summary>
        /// <typeparam name="T">物件型別</typeparam>
        /// <param name="source">資料來源</param>
        /// <param name="filter">過濾函式</param>
        /// <returns>回傳第一個符合條件的物件</returns>
        public static T SelectFirst<T>(IEnumerable<T> source, Filter<T> filter)
        {
            foreach (var item in Select<T>(source, filter)) return item;

            return default(T);
        }
    }
}
