﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mikan.Net
{
    public interface IWebSocketService : IEventTrigger, IDisposable
    {
        bool IsAlive { get; }

        void RegiserEventListener(IWebSocketServiceEventListener listener);

        void Send(byte[] data);

        void Send(string data);

        void Connect(string url);

        void Disconnect();
    }

    public interface IWebSocketServiceEventListener
    {
        void OnConnect(IWebSocketService sender);
        void OnData(IWebSocketService sender, byte[] data);
        void OnError(IWebSocketService sender, string message);
        void OnClose(IWebSocketService sender, string reason);
        void OnSent(IWebSocketService sender, bool isSucc);
    }

}
