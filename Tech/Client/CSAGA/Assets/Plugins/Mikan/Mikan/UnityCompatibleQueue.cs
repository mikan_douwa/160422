﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mikan
{
    /// <summary>
    /// 設計用來兼容Unity行為的串列(FIFO)
    /// </summary>
    /// <typeparam name="T">物件型別</typeparam>
    public class UnityCompatibleQueue<T>
    {
        private bool isEmpty = true;

        private Queue<T> current = new Queue<T>();

        private Queue<T> secondary = new Queue<T>();

        /// <summary>
        /// 是否為空
        /// </summary>
        public bool IsEmpty
        {
            get { return isEmpty; }
        }

        /// <summary>
        /// 取出串列中最前端的物件
        /// </summary>
        /// <returns>回傳物件</returns>
        public T Dequeue()
        {
            var item = current.Dequeue();

            if (current.Count == 0) isEmpty = true;

            return item;
        }

        /// <summary>
        /// 將物件加入串列的尾端
        /// </summary>
        /// <param name="item">要加入的物件</param>
        public void Enqueue(T item)
        {
            secondary.Enqueue(item);

            Integration();
        }

        /// <summary>
        /// 整合串列,由外部定期呼叫
        /// </summary>
        public void Integration()
        {
            if (isEmpty && secondary.Count > 0)
            {
                var tmp = current;
                current = secondary;
                secondary = tmp;

                isEmpty = false;
            }
        }
    }

    /*class InnerQueue<T>
    {
        private QueueItem<T> first = null;
        private QueueItem<T> last = null;
        public bool IsEmpty
        {
            get { return first == null; }
        }

        public T Dequeue()
        {
            var current = first.Current;
            first = first.Next;
            return current;
        }

        public void Enqueue(T item)
        {
            var newItem = new QueueItem<T> { Current = item };
            if (first == null)
            {
                first = last = newItem;
            }
            else
            {
                last.Next = newItem;
                last = last.Next;
            }
        }

    }

    class QueueItem<T>
    {
        public T Current;
        public QueueItem<T> Next;
    }*/
}
