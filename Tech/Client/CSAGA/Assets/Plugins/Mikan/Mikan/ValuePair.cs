﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mikan
{
    abstract public class ValuePair<T> : ISerializable
    {
        public T OriginValue;

        public T NewValue;

        public T Value1 { get { return OriginValue; } }

        public T Value2 { get { return NewValue; } }

        abstract public T Diff { get; }

        abstract public void Serialize(IOutput output);

        abstract public void DeSerialize(IInput input);

        public static U Create<U>(T originVal, T newVal)
            where U : ValuePair<T>, new()
        {
            return new U { OriginValue = originVal, NewValue = newVal };
        }

        public static U Create<U>(T val)
            where U : ValuePair<T>, new()
        {
            return new U { OriginValue = val, NewValue = val };
        }
    }

    public class ValuePair : ValuePair<int>
    {
        public override int Diff { get { return NewValue - OriginValue; } }

        public override void Serialize(IOutput output)
        {
            output.Write(OriginValue);
            output.Write(NewValue);
        }

        public override void DeSerialize(IInput input)
        {
            OriginValue = input.ReadInt32();
            NewValue = input.ReadInt32();
        }

        public static ValuePair Create(int originVal, int newVal)
        {
            return Create<ValuePair>(originVal, newVal);
        }

        public static ValuePair Create(int val)
        {
            return Create<ValuePair>(val);
        }
    }

    public class Int64ValuePair : ValuePair<Int64>
    {
        public override Int64 Diff { get { return NewValue - OriginValue; } }

        public override void Serialize(IOutput output)
        {
            output.Write(OriginValue);
            output.Write(NewValue);
        }

        public override void DeSerialize(IInput input)
        {
            OriginValue = input.ReadInt64();
            NewValue = input.ReadInt64();
        }

        public static Int64ValuePair Create(Int64 originVal, Int64 newVal)
        {
            return Create<Int64ValuePair>(originVal, newVal);
        }

        public static Int64ValuePair Create(Int64 val)
        {
            return Create<Int64ValuePair>(val);
        }
    }

    public class FloatValuePair : ValuePair<float>
    {
        public override float Diff { get { return NewValue - OriginValue; } }

        public override void Serialize(IOutput output)
        {
            output.Write(OriginValue);
            output.Write(NewValue);
        }

        public override void DeSerialize(IInput input)
        {
            OriginValue = input.ReadFloat();
            NewValue = input.ReadFloat();
        }

        public static FloatValuePair Create(float originVal, float newVal)
        {
            return Create<FloatValuePair>(originVal, newVal);
        }

        public static FloatValuePair Create(float val)
        {
            return Create<FloatValuePair>(val);
        }
    }

}
