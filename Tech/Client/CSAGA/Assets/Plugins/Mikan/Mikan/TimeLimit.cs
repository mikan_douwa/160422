﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mikan
{
    public class TimeLimit
    {
        private bool isOverdue = true;
        private DateTime lastUpdateTime = DateTime.Now;
        private int timeLimit;

        private TimeLimit(int timeLimit)
            : this(timeLimit, true) { }

        private TimeLimit(int timeLimit, bool defaultState)
        {
            this.timeLimit = timeLimit;
            isOverdue = defaultState;
            if(!isOverdue) lastUpdateTime = DateTime.Now;
        }

        private static Dictionary<object, TimeLimit> items = new Dictionary<object, TimeLimit>();

        public static bool IsOverdue(object key)
        {
            TimeLimit item = null;
            if (items.TryGetValue(key, out item))
            {
                if (item.isOverdue) return true;
                if (item.timeLimit == int.MaxValue) return false;
                return (item.lastUpdateTime.AddSeconds(item.timeLimit) < DateTime.Now);
            }

            return true;
        }
        public static void SetIsOverdue(object key, bool isOverdue)
        {
            TimeLimit item = null;
            if (!items.TryGetValue(key, out item))
            {
                item = new TimeLimit(int.MaxValue);
                items.Add(key, item);
            }

            item.isOverdue = isOverdue;
            item.lastUpdateTime = DateTime.Now;
        }

        public static void ClearAll()
        {
            items = new Dictionary<object, TimeLimit>();
        }

        public static void Add(object key, int timeLimit, bool defaultState)
        {
            items[key] = new TimeLimit(timeLimit, defaultState);
        }
        public static void Add(object key, int timeLimit)
        {
            items[key] = new TimeLimit(timeLimit);
        }
        public static void Add(object key)
        {
            Add(key, int.MaxValue);
        }
        
    }
}
