﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mikan
{
    public class LCGCryptor
    {
        public const int DEFAULT_KEY = 1724;

        private int A;
        private int B;
        private int M;
        public LCGCryptor(int a, int b, int m)
        {
            A = a;
            B = b;
            M = m;
        }

        public void Encode(byte[] buffer, int key)
        {
            Encode(buffer, 0, buffer.Length, key);
        }

        public void Encode(byte[] buffer, int offset, int length, int key)
        {
            int N0 = buffer.Length % M;

            var staticKey = BitConverter.GetBytes(key);

            var variableKey = BitConverter.GetBytes(N0);

            for (int i = 0; i < length; ++i)
            {
                var index = i + offset;

                if (index % 4 == 0)
                {
                    N0 = (A * N0 + B) % M;
                    variableKey = BitConverter.GetBytes(N0);
                }

                buffer[index] = (byte)(buffer[index] ^ staticKey[index % 4] ^ variableKey[index % 4]);
            }
        }
    }
}
