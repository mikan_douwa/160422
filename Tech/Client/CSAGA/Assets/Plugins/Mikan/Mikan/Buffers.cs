﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mikan
{
    public class Buffers
    {
        private List<Byte[]> buffers = new List<byte[]>();

        public IEnumerable<Byte[]> Select() { return buffers; }

        private int count = 0;

        public byte[] Merge()
        {
            var bytes = new byte[count];
            var offset = 0;
            foreach (var item in buffers)
            {
                Array.Copy(item, 0, bytes, offset, item.Length);
                offset += item.Length;
            }

            return bytes;
        }

        public void Add(byte[] data)
        {
            buffers.Add(data);
            count += data.Length;
        }

        public void Add(byte data)
        {
            Add(new byte[] { data });
        }

        public void Add(int data)
        {
            Add(BitConverter.GetBytes(data));
        }

        public void Add(string data)
        {
            Add(Encoding.UTF8.GetBytes(data));
        }
    }
}
