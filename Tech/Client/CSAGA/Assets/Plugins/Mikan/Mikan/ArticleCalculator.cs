﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mikan
{
    public class ArticleCalculator
    {
        private int baseVal;
        private int limitVal;
        private int addVal;
        private int costVal;
        private int costVal2;
        public ArticleCalculator(int baseVal, int limitVal, int addVal, int costVal)
        {
            this.baseVal = baseVal;
            this.limitVal = limitVal;
            this.addVal = addVal;
            this.costVal = costVal;
        }

        public ArticleCalculator(int baseVal, int limitVal, int addVal, int costVal, int costVal2)
            : this(baseVal, limitVal, addVal, costVal)
        {
            this.costVal2 = costVal2;
        }

        public int Cost { get { return costVal; } }

        public int Limit { get { return limitVal; } }

        public int Add { get { return addVal; } }

        public int Count(int current)
        {
            return baseVal + addVal * current;
        }

        public int StepCost(int current)
        {
            return Cost + current * costVal2;
        }
    }
}
