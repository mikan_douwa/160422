﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mikan
{
    /// <summary>
    /// 數學工具類別
    /// </summary>
    public class MathUtils
    {
        /// <summary>
        /// 洗牌
        /// </summary>
        /// <typeparam name="T">列表物件型別</typeparam>
        /// <param name="list">要洗牌的列表</param>
        public static void Shuffle<T>(IList<T> list)
        {
            for (int i = 0; i < list.Count; ++i)
            {
                var rand = Random.Next(0, list.Count);
                var tmp = list[i];
                list[i] = list[rand];
                list[rand] = tmp;
            }
        }

        public static int Restrict(int val, int min, int max)
        {
            return Math.Min(Math.Max(min, val), max);
        }

        public static Int64 Restrict(Int64 val, Int64 min, Int64 max)
        {
            return Math.Min(Math.Max(min, val), max);
        }

        public static class Seed
        {
            private const Int64 M = 524288;
            private const Int64 A = 155237;
            private const Int64 B = 379977;

            public static int Next()
            {
                return Random.Next() % (int)M;
            }

            public static int Next(int current)
            {
                return (int)((A * current + B) % M);
            }
        }
        /// <summary>
        /// 亂數產生器
        /// </summary>
        /// <threadsafe />
        public static class Random
        {
            private static System.Random rand;

            static Random()
            {
                rand = new System.Random();
            }

            /// <summary>
            /// 傳回小於指定最大值的非負值亂數
            /// </summary>
            /// <param name="maxValue">最大值</param>
            /// <returns>回傳產生的亂數</returns>
            public static int Next(int maxValue)
            {
                lock(rand) return rand.Next(maxValue);
            }

            /// <summary>
            /// 傳回非負值亂數
            /// </summary>
            /// <returns>回傳產生的亂數</returns>
            public static int Next()
            {
                lock (rand) return rand.Next();
            }

            /// <summary>
            /// 傳回指定範圍內的亂數
            /// </summary>
            /// <param name="minValue">最大值</param>
            /// <param name="maxValue">最小值</param>
            /// <returns>回傳產生的亂數</returns>
            public static int Next(int minValue, int maxValue)
            {
                lock (rand) return rand.Next(minValue, maxValue);
            }

            /// <summary>
            /// 傳回0.0和1.0之間的亂數
            /// </summary>
            /// <returns>回傳產生的亂數</returns>
            public static double NextDouble()
            {
                lock(rand) return rand.NextDouble();
            }

            /// <summary>
            /// 以亂數填入指定位元組陣列的元素
            /// </summary>
            /// <param name="buffer">目標位元組陣列</param>
            public static void NextBytes(byte[] buffer)
            {
                lock (rand) rand.NextBytes(buffer);
            }
        }
    }
}
