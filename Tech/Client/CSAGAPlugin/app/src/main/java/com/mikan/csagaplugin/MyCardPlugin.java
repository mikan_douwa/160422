package com.mikan.csagaplugin;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;

import com.unity3d.player.UnityPlayer;

/**
 * Created by severus_hsiao on 2016/4/14.
 */
public class MyCardPlugin {

    public static void InitMyCard(Activity a, String gameObjectName)
    {
        //       Activity a = UnityPlayer.currentActivity;
        //       a.runOnUiThread(new Runnable() {
        //          public void run() {
        Context context = a.getApplicationContext();
        PackageManager pm = context.getPackageManager();
        String str = "";
        try {
            android.content.pm.Signature[] signs = pm.getPackageInfo(context.getPackageName(), PackageManager.GET_SIGNATURES).signatures;
            for (android.content.pm.Signature signature : signs) {
                str += ";" + signature.toCharsString();
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        //Callback Unity
        UnityPlayer.UnitySendMessage(gameObjectName, "InitResult", "0" + str);
        //          }
        //       });

    }
}
