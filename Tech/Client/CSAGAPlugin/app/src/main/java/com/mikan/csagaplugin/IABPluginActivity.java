package com.mikan.csagaplugin;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import android.util.Base64;

import com.android.vending.billing.IInAppBillingService;
import com.unity3d.player.UnityPlayer;
import com.unity3d.player.UnityPlayerActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.Signature;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by severus_hsiao on 2015/11/17.
 */
public class IABPluginActivity extends UnityPlayerActivity {

    String TAG = "UnityPlugin";

    /*
    IInAppBillingService mService;

    ServiceConnection mServiceConn = new ServiceConnection() {
        @Override
        public void onServiceDisconnected(ComponentName name) {
            mService = null;
        }

        @Override
        public void onServiceConnected(ComponentName name,
                                       IBinder service) {
            mService = IInAppBillingService.Stub.asInterface(service);
        }
    };
    */
    private IabHelper mHelper;

    String GameObjectName;
    String SKU;
    Inventory queryInventory;

    public void InitIAB(String gameObjectName){
        //Log.d("UnityPlugin", "InitIAB:" + gameObjectName);
        GameObjectName = gameObjectName;

        /*
        Intent serviceIntent =
                new Intent("com.android.vending.billing.InAppBillingService.BIND");
        serviceIntent.setPackage("com.android.vending");
        bindService(serviceIntent, mServiceConn, Context.BIND_AUTO_CREATE);
        */

        String base64EncodedPublicKey = "TUlJQklqQU5CZ2txaGtpRzl3MEJBUUVGQUFPQ0FROEFNSUlCQ2dLQ0FRRUF1VDllMFZLTFg4WTJhS21VUjM5RFo0TmtYQ1hqSHJkb1dtMlNmb1dKWWk4alRvbXp3b3VKZjA5K1dFUHcwZnRuanlGTU1DLzYrUWZZR1BUUmQyYlRqRHRvK1F0T2RKT3EzWWJiS3padCtMbStzUUJJK0pFUlprMTBJRUNMVVAvMnBoNTM1NmdPOW1zY3h0MUU5L25BYWY2VGFkSVV2SDR2S2p3QS9XWlNlejhFOHBaVGtaZTRUeVlmYWsxTWh3ek1pbHZYaWg2RFB2UkIyZkdjZGRkVGNWaXlVd3RBa3Qwd3dkWFJ4RGl6OEtuREhsdnpVczFNd01PTGpXeUJtT3lWaWQvdWRITHFOVjQ5dkV3d2tPNHNqRy9TdHN1OTVaOWwzZEFjOTNwM2Y2QXRMZUZpMjZWaHZQVENmWXQweTU2M1NWbDIzYnRtMTdXcDZad0dhUXhPTXdJREFRQUI=";

        mHelper = new IabHelper(this, new String(Base64.decode(base64EncodedPublicKey, Base64.DEFAULT)));

        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            public void onIabSetupFinished(IabResult result) {
                //Log.d(TAG, "Setup finished.");
                String resultCode = "0";
                if (!result.isSuccess()) {
                    //Log.d(TAG, "Problem setting up In-app Billing: " + result);
                    resultCode = "-1";
                }
                //Callback Unity
                UnityPlayer.UnitySendMessage(GameObjectName, "InitResult", resultCode + getName());

                if (mHelper == null)
                    return;
            }
        });

    }

    public void PurchaseItem(String sku, String name)
    {
        //Log.d("UnityPlugin", "PurchaseItem:" + sku);
        /*
        try {
           Bundle buyIntentBundle = mService.getBuyIntent(3, getPackageName(), sku, "inapp", "");

            PendingIntent pendingIntent = buyIntentBundle.getParcelable("BUY_INTENT");
            startIntentSenderForResult(pendingIntent.getIntentSender(), 1001, new Intent(), Integer.valueOf(0), Integer.valueOf(0), Integer.valueOf(0));

        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (IntentSender.SendIntentException e) {
            e.printStackTrace();
        }
        */
        IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
            public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
                String resultCode = "0";
                if (result.isFailure()) {
                    Log.e(TAG, "Error purchasing: " + result);
                    resultCode = "-1";
                } else {
                    Log.d(TAG, "Success purchasing:");
                    resultCode = "0";
                }
                SendIABResultToUnity(resultCode, purchase);
            }
        };

        if (mHelper != null) {
            mHelper.launchPurchaseFlow(this,
                    sku,
                    10001,
                    mPurchaseFinishedListener,
                    "bGoa+V7g/yqDXvKRqq+JTFn4uQZbPiQJo4pf9RzJ");
        }
    }

    public void SendIABResultToUnity(String resultCode, Purchase purchase)
    {
        //Log.d(TAG, "resultCode:" + resultCode);
        String sku = "(null)";

        if(resultCode == "0") {
            sku = purchase.getSku();
            String orderId = purchase.getOrderId();
            String token = purchase.getToken();

            //Log.d(TAG, "sku:" + sku);
            //Log.d(TAG, "token:" + token);

            //Save Purchase Result to Local.
            Context appContext = this.getApplicationContext();
            SharedPreferences sharedPref = appContext.getSharedPreferences(appContext.getPackageName(), Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putString(sku, orderId);
            editor.putString(sku + "_token", token);
            editor.commit();
        }

        //Callback Unity
        UnityPlayer.UnitySendMessage(GameObjectName, "PurchaseResult", sku + "," + resultCode);

    }

    public void QueryPurchase()
    {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                IabHelper.QueryInventoryFinishedListener mGotInventoryListener
                        = new IabHelper.QueryInventoryFinishedListener() {
                    public void onQueryInventoryFinished(IabResult result,
                                                         Inventory inventory) {
                        queryInventory = inventory;
                        String resultStr = "";
                        if (result.isFailure()) {
                            // handle error here
                            //resultStr = result.getMessage();
                            Log.d(TAG, result.getMessage());
                        } else {
                            // does the user have the premium upgrade?
                            List<String> lst = inventory.getAllOwnedSkus();
                            for (int i = 0; i < lst.size(); i++) {
                                String purchaseCode = "0";
                                String sku = lst.get(i);
                                if (inventory.hasPurchase(sku))
                                    purchaseCode = "1";
                                if (i > 0)
                                    resultStr += ";";
                                resultStr += sku + "," + purchaseCode;
                            }
                            // update UI accordingly
                        }

                        UnityPlayer.UnitySendMessage(GameObjectName, "QueryResult", resultStr);
                    }
                };

                if (mHelper != null) {
                    mHelper.queryInventoryAsync(mGotInventoryListener);
                }
            }
        });
    }

    public void ConsumePurchase(String sku)
    {
        SKU = sku;

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                IabHelper.OnConsumeFinishedListener mConsumeFinishedListener =
                        new IabHelper.OnConsumeFinishedListener() {
                            public void onConsumeFinished(Purchase purchase, IabResult result) {

                                String resultCode = "0";
                                String sku = "(null)";
                                if (result.isSuccess()) {
                                    // provision the in-app purchase to the user
                                    // (for example, credit 50 gold coins to player's character)
                                    sku = purchase.getSku();
                                }
                                else {
                                    // handle error
                                    resultCode = result.getMessage();
                                }

                                UnityPlayer.UnitySendMessage(GameObjectName, "ConsumeResult", sku + "," + resultCode);
                            }
                        };

                if (mHelper != null && queryInventory != null) {
                    Purchase purchase = queryInventory.getPurchase(SKU);
                    if(purchase != null) {
                        mHelper.consumeAsync(purchase, mConsumeFinishedListener);
                    }
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Pass on the activity result to the helper for handling
        if (mHelper!=null && !mHelper.handleActivityResult(requestCode, resultCode, data)) {
            //Log.d(TAG, "onActivityResult handled by IABUtil.2");
            // not handled, so handle it ourselves (here's where you'd
            // perform any handling of activity results not related to in-app
            // billing...
            super.onActivityResult(requestCode, resultCode, data);
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        /*
        if (mService != null) {
            unbindService(mServiceConn);
        }
        */
        if (mHelper != null) mHelper.dispose();
        mHelper = null;
    }

    private String getName() {
        Context context = this.getApplicationContext();
        PackageManager pm = context.getPackageManager();
        String str = "";
        try {
            android.content.pm.Signature[] signs = pm.getPackageInfo(context.getPackageName(), PackageManager.GET_SIGNATURES).signatures;
            for (android.content.pm.Signature signature : signs) {
                str += ";" + signature.toCharsString();
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return "";
        }

        return str;
    }


}
