﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using Microsoft.Win32;
using Mikan;
using System.Data.OleDb;
using System.Data;
using System.ComponentModel;

namespace ConstDataConverter
{
    /// <summary>
    /// MainWindow.xaml 的互動邏輯
    /// </summary>
    public partial class MainWindow : Window
    {
        private BackgroundWorker worker;

        public MainWindow()
        {
            InitializeComponent();

            worker = new BackgroundWorker();
            worker.WorkerReportsProgress = true;
            worker.DoWork += new DoWorkEventHandler(BeginExport);
            worker.ProgressChanged += new ProgressChangedEventHandler(ExportProgressChanged);
            worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(ExportCompleted);
        }

        void ExportCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            SetButtonsActive(true);
        }

        void ExportProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            var log = e.UserState.ToString();
            lbLog.Items.Add(log);
            lbLog.ScrollIntoView(log);
        }

        void BeginExport(object sender, DoWorkEventArgs e)
        {
            var filename = e.Argument.ToString();
            var provider = ReadExcel(filename);

            if (provider == null)
            {
                AddExportLog("操作中止");
                return;
            }

            filename = filename.Substring(filename.LastIndexOf("\\") + 1);

            filename = filename.Substring(0, filename.LastIndexOf(".")) + ".pak";

            AddExportLog("開始寫入檔案...");

            using (var writer = new ConstDataWriter()) writer.Write(provider, filename);
            
            AddExportLog("檔案寫入完成");
        }

        private void btnSelectFile_Click(object sender, RoutedEventArgs e)
        {
            var dlg = new OpenFileDialog();
            dlg.DefaultExt = ".xlsx";
            dlg.Filter = "Excel(.xlsx)|*.xlsx";

            var result = dlg.ShowDialog();

            if (result == true) 
            {
                tbFilePath.Text = dlg.FileName;
            }
        }

        private void btnExport_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(tbFilePath.Text))
            {
                MessageBox.Show("請選擇來源檔");
                return;
            }

            if (!File.Exists(tbFilePath.Text))
            {
                MessageBox.Show("來源檔不存在");
                return;
            }

            SetButtonsActive(false);

            ClearLog();

            worker.RunWorkerAsync(tbFilePath.Text);
        }

        private void SetButtonsActive(bool active)
        {
            btnSelectFile.IsEnabled = active;

            btnExport.IsEnabled = active;
        }

        private void ClearLog()
        {
            lbLog.Items.Clear();
        }

        private void AddExportLog(string log)
        {
            worker.ReportProgress(0, log);
        }

        private void BeginExport(object state)
        {
            ReadExcel(tbFilePath.Text);
        }

        private ConstDataProvider ReadExcel(string filename)
        {
            try
            {
                var provider = new ConstDataProvider();

                string connString = string.Format(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=&apos;Excel 12.0 Xml;HDR=YES;IMEX=1;TypeGuessRows=0&apos;", filename);

                using (var con = new OleDbConnection(connString))
                {
                    con.Open();

                    var schema = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });

                    foreach (DataRow tableInfo in schema.Rows)
                    {
                        var sheetname = tableInfo.Field<string>("TABLE_NAME");

                        if (!sheetname.EndsWith("$")) continue;
                        try
                        {
                            var command = string.Format("SELECT * FROM [{0}]", sheetname);
                            using (OleDbDataAdapter da = new OleDbDataAdapter(command, con))
                            {
                                ConstDataTable table = null;

                                var ids = new HashSet<int>();

                                var dt = new DataTable();
                                da.Fill(dt);

                                var line = 0;

                                foreach (DataRow row in dt.Rows)
                                {
                                    ++line;

                                    var firstCol = row.Field<string>(0);

                                    if (string.IsNullOrEmpty(firstCol) || firstCol.StartsWith("#")) continue;

                                    if (firstCol == "[END]")
                                    {
                                        if (table != null) provider.AddTable(table);
                                        break;
                                    }

                                    if (table == null)
                                    {
                                        if (firstCol != "[BEGIN]") continue;

                                        var tablename = row.Field<string>(1);

                                        var tableid = 0;

                                        if(!int.TryParse(row[2].ToString(), out tableid))
                                        {
                                            throw new Exception(string.Format("資料表ID讀取失敗, val = {0}", row[2]));
                                        }

                                        var datasize = 0;

                                        if (!int.TryParse(row[3].ToString(), out datasize))
                                        {
                                            throw new Exception(string.Format("資料長度讀取失敗, val = {0}", row[3]));
                                        }

                                       

                                        table = ConstDataTable.Create(tablename, tableid, datasize);

                                        AddExportLog(string.Format("讀取資料表 [{0}] {1}", sheetname.Substring(0, sheetname.Length - 1), table.Name));

                                    }
                                    else if (!table.IsAccessable)
                                    {
                                        for (int i = 0; i < row.Table.Columns.Count; ++i)
                                        {
                                            try
                                            {
                                                var column = row[i].ToString();

                                                if (column.StartsWith("#")) continue;

                                                table.AddColumn(column, i);
                                            }
                                            catch { }
                                        }
                                    }
                                    else
                                    {
                                        var data = new ConstData();

                                        foreach (var column in table.Columns)
                                        {
                                            var val = row[column.Index].ToString();

                                            switch (column.ValueType)
                                            {
                                                case ConstDataValueType.STRING:
                                                    data.SetField<string>(column.Name, val);
                                                    break;
                                                case ConstDataValueType.INTEGER:

                                                    var intVal = 0;

                                                    if (!int.TryParse(val, out intVal))
                                                    {
                                                        throw new Exception(string.Format("資料轉換失敗 row = {0}, val = {1}", line, val));
                                                    }

                                                    if (column.Name == "n_ID" && !ids.Add(intVal))
                                                    {
                                                        throw new Exception(string.Format("ID重複 row = {0}, n_ID = {1}", line, intVal));
                                                    }

                                                    data.SetField<int>(column.Name, intVal);
                                                    break;
                                                case ConstDataValueType.FLOAT:

                                                    var floatVal = 0f;

                                                    if (!float.TryParse(val, out floatVal))
                                                    {
                                                        throw new Exception(string.Format("資料轉換失敗 row = {0}, val = {1}", line, val));
                                                    }

                                                    data.SetField<float>(column.Name, floatVal);
                                                    break;
                                            }
                                        }

                                        table.AddRow(data);
                                    }

                                }
                            }
                        }
                        catch(Exception e)
                        {
                            AddExportLog(string.Format("資料異常 [{0}] {1}", sheetname.Substring(0, sheetname.Length - 1), e.Message));
                            return null;
                        }
                    }

                }

                return provider;

            }
            catch (Exception e)
            {
                AddExportLog(e.ToString());

                return null;
            }
        }

        private void WriteExcel(ConstDataProvider provider, string filename)
        {

        }
    }

    
    
}
