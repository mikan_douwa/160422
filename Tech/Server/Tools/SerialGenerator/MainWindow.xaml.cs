﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Mikan;
using System.Security.Cryptography;
using System.IO;

namespace SerialGenerator
{
    /// <summary>
    /// MainWindow.xaml 的互動邏輯
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void tb_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if ((e.Key >= Key.NumPad0 && e.Key <= Key.NumPad9) ||
                (e.Key >= Key.D0 && e.Key <= Key.D9) ||
                e.Key == Key.Back ||
                e.Key == Key.Left || e.Key == Key.Right)
            {
                if (e.KeyboardDevice.Modifiers != ModifierKeys.None)
                {
                    e.Handled = true;
                }
            }
            else
            {
                e.Handled = true;
            }
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            const byte KEY = 0xA0;

            var from = int.Parse(tbFrom.Text);
            var count = int.Parse(tbCount.Text);

            textBox1.Clear();

            using (var md5 = MD5.Create())
            {
                for (int i = 0; i < count; ++i)
                {
                    var original = MathUtils.Seed.Next(from + i).ToString("000000");
                    
                    var val = Encoding.UTF8.GetBytes(original);

                    for (int j = 0; j < val.Length; ++j) val[j] ^= KEY;
                    textBox1.AppendText(Convert.ToBase64String(val) + "\r\n");
                }
            }
            
        }

        static byte[] EncryptStringToBytes_Aes(string plainText, byte[] Key, byte[] IV)
        {
            // Check arguments.
            if (plainText == null || plainText.Length <= 0)
                throw new ArgumentNullException("plainText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("Key");
            byte[] encrypted;
            // Create an AesCryptoServiceProvider object
            // with the specified key and IV.
            using (var aesAlg = new DESCryptoServiceProvider())
            {
                aesAlg.Key = Key;
                aesAlg.IV = IV;

                // Create a decrytor to perform the stream transform.
                ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);

                // Create the streams used for encryption.
                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                        {

                            //Write all data to the stream.
                            swEncrypt.Write(plainText);
                        }
                        encrypted = msEncrypt.ToArray();
                    }
                }
            }


            // Return the encrypted bytes from the memory stream.
            return encrypted;

        }

        static string DecryptStringFromBytes_Aes(byte[] cipherText, byte[] Key, byte[] IV)
        {
            // Check arguments.
            if (cipherText == null || cipherText.Length <= 0)
                throw new ArgumentNullException("cipherText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("IV");

            // Declare the string used to hold
            // the decrypted text.
            string plaintext = null;

            // Create an AesCryptoServiceProvider object
            // with the specified key and IV.
            using (var aesAlg = new DESCryptoServiceProvider())
            {
                aesAlg.Key = Key;
                aesAlg.IV = IV;

                // Create a decrytor to perform the stream transform.
                ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);

                // Create the streams used for decryption.
                using (MemoryStream msDecrypt = new MemoryStream(cipherText))
                {
                    using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader srDecrypt = new StreamReader(csDecrypt))
                        {

                            // Read the decrypted bytes from the decrypting stream
                            // and place them in a string.
                            plaintext = srDecrypt.ReadToEnd();
                        }
                    }
                }

            }

            return plaintext;

        }

        private void button2_Click(object sender, RoutedEventArgs e)
        {
            var count = int.Parse(tbCount.Text);

            textBox1.Clear();

            var list = new HashSet<string>();

            for (int i = 0; i < count; ++i)
            {
                while (true)
                {
                    var rand = new List<char>(8);

                    while (rand.Count < 8) rand.Add(Random());

                    var val = rand.ToArray();

                    var code = new string(val);

                    if (list.Contains(code)) continue;

                    list.Add(code);

                    textBox1.AppendText(code + "\r\n");

                    break;
                }
            }
        }
        private List<char> provider;
        private char Random()
        {
            if (provider == null)
            {
                provider = new List<char>();
                for (int i = 0; i <= 9; ++i) provider.Add((char)('0' + i));

                for (int i = 0; i < 26; ++i)
                {
                    provider.Add((char)('a' + i));
                    provider.Add((char)('A' + i));
                }
            }

            return provider[MathUtils.Random.Next(provider.Count)];
        }
    }
}
