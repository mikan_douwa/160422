﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Mikan.CSAGA.Server;
using Mikan;
using Mikan.Server;

public partial class gift : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        var type = Parse(DropDownList1.SelectedValue);
        var value1 = Parse(TextBox1.Text);
        var value2 = Parse(TextBox2.Text);
        

        if (value1 == 0 && value2 == 0)
        {
            ShowResult(string.Format("無效的數值, Value1={0}, Value2={1}", TextBox1.Text, TextBox2.Text));
            return;
        }
        else
        {
            

            switch (DropDownList2.SelectedValue)
            {
                case "1":
                {
                    var list = new List<ResultData>();

                    var invalid = new List<string>();

                    if (!string.IsNullOrEmpty(TextBox3.Text))
                    {
                        var ids = TextBox3.Text.Split(',');
                        if (ids.Length == 0)
                        {
                            ShowResult(string.Format("找不到有效ID, IDs={0}", TextBox3.Text));
                            return;
                        }
                        foreach (var item in ids)
                        {
                            var id = PublicID.FromString(item);

                            if (id == null || !id.IsValid)
                                invalid.Add(item);
                            else
                            {
                                InsertGift(id, type, value1, value2);
                                list.Add(new ResultData { ID = item });
                            }
                        }

                        if (list.Count > 0 && invalid.Count > 0)
                        {
                            list.Add(new ResultData { ID = "以下為無效的ID" });
                            foreach (var item in invalid)
                            {
                                list.Add(new ResultData { ID = item });
                            }
                        }

                    }
                    else
                    {
                        ShowResult("未輸入ID");
                        return;
                    }

                    GridView1.DataSource = list;
                    GridView1.DataBind();
                    break;
                }
                case "2":
                {
                    var date = DateTime.Parse(TextBox3.Text);
                    var count = 0;
                    foreach (var group in DBUtils.Groups)
                    {
                        var list = default(List<DBPlayer>);
                        using (var db = DBUtils.OpenDB(group.Value.ConnStr))
                        {
                            var cmd = DBUtils.SelectPlayer(group.Key, date);
                            using (var res = db.DoQuery(cmd))
                            {
                                list = DBObjectConverter.Convert<DBPlayer>(res).ToList();
                            }

                            foreach (var item in list)
                            {
                                InsertGift(db, PublicID.Create(group.Key, item.PlayerID), type, value1, value2);
                                ++count;
                            }
                        }
                    }
                    ShowResult(string.Format("符合條件的玩家數量:{0}", count));
                    break;
                }
            }


            
        }

        
        
    }

    private void InsertGift(TestPlayerData player, int type, int value1, int value2)
    {
        using (var playerDb = DBUtils.OpenDB(DBUtils.Groups[player.Origin.GroupID].ConnStr))
        {
            var id = PublicID.Create(player.Origin.GroupID, player.Origin.PlayerID);
            var cmd = DBCommandBuilder.InsertGift(id, Mikan.CSAGA.RewardSource.Debug, TextBox4.Text, (Mikan.CSAGA.DropType)type, value1, value2);
            playerDb.SimpleQuery(cmd);
        }
    }

    private void InsertGift(PublicID id, int type, int value1, int value2)
    {
        using (var playerDb = DBUtils.OpenDB(DBUtils.Groups[id.Group].ConnStr)) InsertGift(playerDb, id, type, value1, value2);
    }

    private void InsertGift(IDBConnection db, PublicID id, int type, int value1, int value2)
    {
        var cmd = DBCommandBuilder.InsertGift(id, Mikan.CSAGA.RewardSource.Gift, TextBox4.Text, (Mikan.CSAGA.DropType)type, value1, value2);
        db.SimpleQuery(cmd);
    }

    private int Parse(string origin)
    {
        var val = 0;
        if(int.TryParse(origin, out val)) return val;
        return 0;
    }

    private void ShowResult(string msg)
    {
        var data = new MessageData { Message = msg };
        var list = new List<MessageData>();
        list.Add(data);
        GridView1.DataSource = list;
        GridView1.DataBind();
    }
}

class MessageData
{
    public string Message { get; set; }
}

class ResultData
{
    public string ID { get; set; }
}
