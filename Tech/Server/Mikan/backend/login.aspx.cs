﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class login : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        var lastLogin = Context.Session["last_login_gwen324rcc"];

        if (lastLogin is DateTime)
        {
            var _lastLogin = (DateTime)lastLogin;

            if (_lastLogin.AddMinutes(1) > DateTime.Now) return;
        }

        Context.Session["last_login_gwen324rcc"] = DateTime.Now;
        Context.Session["user_id"] = TextBox1.Text;
        Context.Session["password"] = TextBox2.Text;
        Server.Transfer("default.aspx");
    }
}