﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Mikan.CSAGA.Server;
using Mikan;
using Mikan.Server;

public partial class iap : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        QueryPeriod(ParseDate(TextBox1.Text, DateTime.MinValue), ParseDate(TextBox2.Text, DateTime.MaxValue)); 
    }

    private void QueryPeriod(DateTime beginTime, DateTime endTime)
    {
        var tables = new string[] { "ios_receipt", "mycard_receipt", "google_play_receipt" };
        var map = PublicID.CreateDictinary<QueryIAPData>();

        var min = 0;

        if(!int.TryParse(TextBox3.Text, out min)) min = 0;

        using (var db = DBUtils.OpenUniqueDB())
        {
            foreach (var table in tables)
            {
                var cmd = IAPUtils.SelectReceipt(table, beginTime, endTime);
                
                using (var res = db.DoQuery(cmd))
                {
                    foreach (var item in DBObjectConverter.Convert<DBIAPReceipt>(res))
                    {
                        var id = PublicID.Create(item.GroupID, item.PlayerID);

                        var obj = default(QueryIAPData);
                        if (!map.TryGetValue(id, out obj))
                        {
                            obj = new QueryIAPData();
                            obj.PublicID = id;
                            obj.ID = id.ToString();
                            map.Add(id, obj);
                        }

                        obj.IAPGem += item.Gem;
                        ++obj.Count;
                    }
                }
            }
            
        }

        var list = new List<QueryIAPData>();

        foreach (var item in map)
        {
            if (item.Value.IAPGem < min) continue;
            using (var db = DBUtils.OpenDB(DBUtils.Groups[item.Key.Group].ConnStr))
            {
                if (Query(db, item.Value)) list.Add(item.Value);
            }
        }

        list.Sort((lhs, rhs) => { return lhs.IAPGem.CompareTo(rhs.IAPGem); });

        Bind(list);
    }

    private DateTime ParseDate(string date, DateTime defaultDate)
    {
        var result = default(DateTime);
        if (DateTime.TryParse(date, out result)) return result;
        return defaultDate;
    }

    private QueryIAPData Query(IDBConnection db, PublicID id)
    {
        var data = new QueryIAPData();
        data.PublicID = id;
        data.ID = id.ToString();

        if (!Query(db, data)) return null;

        return data;
    }

    private bool Query(IDBConnection db, QueryIAPData data)
    {
        var cmd = default(IDBCommand);

        if (data.Origin == null)
        {
            cmd = DBCommandBuilder.SelectPlayer(data.PublicID);

            using (var res = db.DoQuery(cmd))
            {
                var origin = DBObjectConverter.ConvertCurrent<DBPlayer>(res);

                if (origin == null || origin.Complex == 0) return false;

                data.Origin = origin;
            }
        }

        return true;
    }

    private void Bind(List<QueryIAPData> list)
    {
        if (list.Count > 0)
            GridView1.DataSource = list;
        else
            GridView1.DataSource = new NoData[] { new NoData() };

        GridView1.DataBind();
    }
}
class NoData
{
    public string Message { get { return "查無資料"; } }
}
class QueryIAPData
{
    public PublicID PublicID;
    public DBPlayer Origin;
    public string DBID { get { return string.Format("{0},{1}", PublicID.Group, PublicID.Serial); } }
    public string ID { get; set; }
    public string Name { get { return Origin.Name; } }
    public int IAPGem { get; set; }
    public int Count { get; set; }
}

class IAPUtils
{
    public static IDBCommand SelectReceipt(string tablename, DateTime beginTime, DateTime endTime)
    {
        var text = "SELECT * FROM `" + tablename + "` WHERE `timestamp`>=@begin_time AND `timestamp`<@end_time;";

        var cmd = DBCommandBuilder.CreateNoLockDBCommand(text);
        cmd.AddParameter("@begin_time", beginTime);
        cmd.AddParameter("@end_time", endTime);

        return cmd;
    }
}