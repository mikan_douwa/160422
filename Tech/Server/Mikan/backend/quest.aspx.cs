﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Mikan.CSAGA;
using Mikan.CSAGA.Server;
using Mikan.Server;
using Mikan;

public partial class quest : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        var map = new Dictionary<int, LogQuestData>();

        var list = new List<LogQuestData>(Mikan.CSAGA.ConstData.Tables.Instance.QuestDesign.Count);

        foreach (var item in Mikan.CSAGA.ConstData.Tables.Instance.QuestDesign.Select())
        {
            var text = Mikan.CSAGA.ConstData.Tables.Instance.QuestName[item.n_ID];
            var obj = new LogQuestData { QuestID = item.n_ID };
            if (text == null)
                obj.Name = "[無對應資料]";
            else
                obj.Name = "[" + text.s_MAIN_NAME + "] " + text.s_SUB_NAME;

            obj.UpdateTime = "--";

            map.Add(item.n_ID, obj);
            list.Add(obj);
        }

        using (var db = DBUtils.OpenLogDB())
        {
            var cmd = DBUtils.SelectLogQuest();
            using (var res = db.DoQuery(cmd))
            {
                foreach (var item in DBObjectConverter.Convert<DBLogQuest>(res))
                {
                    if (!map.ContainsKey(item.QuestID)) continue;

                    var obj = map[item.QuestID];
                    obj.PlayCount = item.PlayCount;
                    obj.ClearCount = item.ClearCount;
                    obj.ContinueCount = item.ContinueCount;
                    obj.UpdateTime = item.UpdateTime.ToShortDateString();
                }
            }
        }

        GridView1.DataSource = list;
        GridView1.DataBind();
    }
}

public class LogQuestData
{
    public int QuestID { get; set; }
    public string Name { get; set; }
    public int PlayCount { get; set; }
    public int ClearCount { get; set; }
    public int ContinueCount { get; set; }
    public string UpdateTime { get; set; }
}