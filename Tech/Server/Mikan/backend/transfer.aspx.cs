﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Mikan.CSAGA.Server;
using Mikan;
using Mikan.Server;
using Mikan.CSAGA.Server.Tasks;

public partial class transfer : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        switch (DropDownList1.SelectedValue)
        {
            case "1": 
                QueryByID(TextBox1.Text); 
                break;
            case "2":
                QueryByCode(TextBox1.Text); 
                break;
        }
    }

    private void QueryByID(string text)
    {
        var list = new List<TransferPlayerData>();
        var id = PublicID.FromString(text);

        if (id != null && id.IsValid)
        {
            var data = default(TransferPlayerData);
            using (var db = DBUtils.OpenDB(DBUtils.Groups[id.Group].ConnStr))
            {
                data = Query(db, id);
            }
            if (data != null)
            {
                using (var uniqueDb = DBUtils.OpenUniqueDB())
                {
                    var cmd = DBUtils.SelectAccount(id);
                    using (var res = uniqueDb.DoQuery(cmd))
                    {
                        data.Account = DBObjectConverter.ConvertCurrent<DBAccount>(res);
                    }
                }

                if (data.Account != null)
                {
                    list.Add(data);

                    var cache = default(DBTransferCache);

                    var currentTime = DateTime.Now.AddHours(8);

                    using (var db = DBUtils.OpenUniqueDB())
                    {
                        var cmd = DBCommandBuilder.SelectTransferCache(id);

                        using (var dbRes = db.DoQuery(cmd)) cache = DBObjectConverter.ConvertCurrent<DBTransferCache>(dbRes);

                        var key = MathUtils.Random.Next();

                        while (key <= 0) key = MathUtils.Random.Next();

                        cmd = cache == null ? DBCommandBuilder.InsertTransferCache(id, data.Account.UUID, key, currentTime) : DBCommandBuilder.UpdateTransferCache(id, key, currentTime);

                        db.SimpleQuery(cmd);

                        data.Code = AccountTransfer.Generate(id, data.Account.UUID, key, currentTime);
                    }
                }
            }

        }

        Bind(list);
        

        
    }

    private void QueryByCode(string text)
    {
        DBUtils.QuestTestPlayers();

        foreach (var item in DBUtils.Players)
        {
            if (item.Code != text) continue;
            QueryByID(item.GetRawPlayerID());
            return;
        }
        Bind(new List<TransferPlayerData>());
    }

    private TransferPlayerData Query(IDBConnection db, PublicID id)
    {
        var data = new TransferPlayerData();
        data.PublicID = id;
        data.ID = id.ToString();

        if (!Query(db, data)) return null;

        return data;
    }

    private bool Query(IDBConnection db, TransferPlayerData data)
    {
        var cmd = default(IDBCommand);

        if (data.Origin == null)
        {
            cmd = DBCommandBuilder.SelectPlayer(data.PublicID);

            using (var res = db.DoQuery(cmd))
            {
                var origin = DBObjectConverter.ConvertCurrent<DBPlayer>(res);

                if (origin == null || origin.Complex == 0) return false;

                data.Origin = origin;
            }
        }


        return true;
    }

    private void Bind(List<TransferPlayerData> list)
    {
        if (list.Count > 0)
            GridView1.DataSource = list;
        else
            GridView1.DataSource = new TransferNoData[] { new TransferNoData() };

        GridView1.DataBind();
    }
}
class TransferNoData
{
    public string Message { get { return "查無資料"; } }
}
class TransferPlayerData
{
    public PublicID PublicID;
    public DBAccount Account;
    public DBPlayer Origin;
    public string ID { get; set; }
    public string Name { get { return Origin.Name; } }
    public int Crystal { get { return Origin.Crystal; } }
    public int Coin { get { return Origin.Coin; } }
    public string Code { get; set; }
}