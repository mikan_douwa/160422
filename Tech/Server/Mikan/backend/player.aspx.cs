﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Mikan.CSAGA.Server;
using Mikan;
using Mikan.Server;

public partial class player : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        switch (DropDownList1.SelectedValue)
        {
            case "1": 
                QueryByID(TextBox1.Text); 
                break;
            case "2":
                QueryByName(TextBox1.Text);
                break;
            case "3":
                QueryByCreateTime(ParseDate(TextBox1.Text, DateTime.MinValue), ParseDate(TextBox2.Text, DateTime.MaxValue)); 
                break;
            case "4":
                QueryByPlayTime(ParseDate(TextBox1.Text, DateTime.MinValue), ParseDate(TextBox2.Text, DateTime.MaxValue)); 
                break;
            case "5":
                QueryByDBID(TextBox1.Text, TextBox2.Text);
                break;
        }
    }
    private void QueryByName(string name)
    {
        var list = new List<string>();
        foreach(var group in DBUtils.Groups)
        {
            using (var db = DBUtils.OpenDB(group.Value.ConnStr))
            {
                var cmd = DBUtils.SelectPlayer(group.Key, name);
                using (var res = db.DoQuery(cmd))
                {
                    foreach (var item in DBObjectConverter.Convert<DBPlayer>(res))
                    {
                        list.Add(PublicID.Create(group.Key, item.PlayerID).ToString());
                    }
                }
            }
        }
       // GridView1.DataSource = list;

        //GridView1.DataBind();
        QueryByID(list);

    }
    private void QueryByDBID(string group, string id)
    {
        QueryByID(PublicID.Create(Int32.Parse(group), Int64.Parse(id)).ToString());
    }

    private void QueryByID(string text)
    {
        QueryByID(new List<string>(new string[] { text }));
    }

    private void QueryByID(List<string> ids)
    {
        var list = new List<QueryPlayerData>();

        foreach (var text in ids)
        {
            var id = PublicID.FromString(text);

            if (id != null && id.IsValid)
            {
                var data = default(QueryPlayerData);
                using (var db = DBUtils.OpenDB(DBUtils.Groups[id.Group].ConnStr))
                {
                    data = Query(db, id);
                }
                if (data != null)
                {
                    using (var uniqueDb = DBUtils.OpenUniqueDB())
                    {
                        var cmd = DBUtils.SelectAccount(id);
                        using (var res = uniqueDb.DoQuery(cmd))
                        {
                            data.Account = DBObjectConverter.ConvertCurrent<DBAccount>(res);
                        }
                    }

                    if (data.Account != null) list.Add(data);
                }

            }
        }

        Bind(list);
        

        
    }
    private void QueryByCreateTime(DateTime beginTime, DateTime endTime)
    {
        var list = new List<QueryPlayerData>();

        using (var uniqueDb = DBUtils.OpenUniqueDB())
        {
            var cmd = DBUtils.SelectAccount(beginTime, endTime);

            using (var res = uniqueDb.DoQuery(cmd))
            {
                foreach (var item in DBObjectConverter.Convert<DBAccount>(res))
                {
                    var obj = new QueryPlayerData { Account = item };
                    obj.PublicID = PublicID.Create(item.GroupID, item.SubID);
                    obj.ID = obj.PublicID.ToString();
                    list.Add(obj);
                }
            }
        }

        var invalid = new List<QueryPlayerData>();

        foreach (var item in list)
        {
            using (var db = DBUtils.OpenDB(DBUtils.Groups[item.Account.GroupID].ConnStr))
            {
                if (!Query(db, item)) invalid.Add(item);
            }
        }

        foreach (var item in invalid) list.Remove(item);

        Bind(list);
    }
    private void QueryByPlayTime(DateTime beginTime, DateTime endTime)
    {
        var list = new List<QueryPlayerData>();

        foreach (var group in DBUtils.Groups)
        {
            using (var db = DBUtils.OpenDB(group.Value.ConnStr))
            {
                var cmd = DBUtils.SelectPlayer(group.Key, beginTime, endTime);

                var _list = new List<QueryPlayerData>();

                using (var res = db.DoQuery(cmd))
                {
                    foreach(var item in DBObjectConverter.Convert<DBPlayer>(res))
                    {
                        var obj = new QueryPlayerData { Origin = item };
                        obj.PublicID = PublicID.Create(group.Key, item.PlayerID);
                        obj.ID = obj.PublicID.ToString();
                        _list.Add(obj);
                    }
                }

                foreach (var item in _list)
                {
                    if (Query(db, item)) list.Add(item);
                }
            }
        }

        


        list.Sort((lhs, rhs) => { return lhs.Origin.LastPlayTime.CompareTo(rhs.Origin.LastPlayTime); });

        Bind(list);
    }

    private DateTime ParseDate(string date, DateTime defaultDate)
    {
        var result = default(DateTime);
        if (DateTime.TryParse(date, out result)) return result;
        return defaultDate;
    }

    private QueryPlayerData Query(IDBConnection db, PublicID id)
    {
        var data = new QueryPlayerData();
        data.PublicID = id;
        data.ID = id.ToString();

        if (!Query(db, data)) return null;

        return data;
    }

    private bool Query(IDBConnection db, QueryPlayerData data)
    {
        var cmd = default(IDBCommand);

        if (data.Origin == null)
        {
            cmd = DBCommandBuilder.SelectPlayer(data.PublicID);

            using (var res = db.DoQuery(cmd))
            {
                var origin = DBObjectConverter.ConvertCurrent<DBPlayer>(res);

                if (origin == null || origin.Complex == 0) return false;

                data.Origin = origin;
            }
        }

        if (!CheckBox1.Checked)
            data.LastQuestID = "(未取回)";
        else
        {
            cmd = DBUtils.SelectLastQuestID(data.PublicID.Group, data.PublicID.Serial, 10000);
            data.LastQuestID = db.DoQuery<Decimal>(cmd).ToString();
        }

        if (!CheckBox2.Checked)
            data.IAPGem = "(未取回)";
        else
        {
            cmd = DBCommandBuilder.SelectIAPGem(data.PublicID);
            data.IAPGem = db.DoQuery<Decimal>(cmd).ToString();
        }


        return true;
    }

    private void Bind(List<QueryPlayerData> list)
    {
        if (list.Count > 0)
            GridView1.DataSource = list;
        else
            GridView1.DataSource = new NoData[] { new NoData() };

        GridView1.DataBind();
    }
}
class NoData
{
    public string Message { get { return "查無資料"; } }
}
class QueryPlayerData
{
    public PublicID PublicID;
    public DBAccount Account;
    public DBPlayer Origin;
    public string DBID { get { return string.Format("{0},{1}", PublicID.Group, PublicID.Serial); } }
    public string ID { get; set; }
    public string Name { get { return Origin.Name; } }
    public int Crystal { get { return Origin.Crystal; } }
    public int Coin { get { return Origin.Coin; } }
    public string CreateTime { get { return Account != null ? Account.CreateTime.ToString() : "(未取回)"; } }
    public string LastPlay { get { return Origin.LastPlayTime.ToString(); } }
    public string LastQuestID { get; set; }
    public string IAPGem { get; set; }
}