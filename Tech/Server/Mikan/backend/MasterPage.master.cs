﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Mikan.Server;
using Mikan.CSAGA.Server;
using Mikan;
using System.Text;
using Mikan.CSAGA;

public partial class MasterPage : System.Web.UI.MasterPage
{
    public static Dictionary<string, DateTime> LoginTimestamp = new Dictionary<string, DateTime>();

    protected void Page_Load(object sender, EventArgs e)
    {
        var userId = string.Format("{0}", Context.Session["user_id"]);
        var password = string.Format("{0}", Context.Session["password"]);

        if (userId != "talesingames@gmail.com" || password != "mikan_douwa3433")
        {
            Server.Transfer("login.aspx");
            return;
        }
        
        System.Reflection.Assembly.Load("Mikan");
        System.Reflection.Assembly.Load("Mikan.Server");
        System.Reflection.Assembly.Load("Mikan.CSAGA");
        System.Reflection.Assembly.Load("Mikan.CSAGA.Server");

        using (var reader = new ConstDataReader())
        {
            var path = "/usr/csaga/";
            reader.Load(path + "CSAGA_CONSTDATA.pak");
            reader.Load(path + "CSAGA_TEXTDATA.pak");
            reader.Load(path + "SERIAL_NUMBER.pak");

            Mikan.CSAGA.ConstData.Tables.Create(reader.Combine());
            Mikan.CSAGA.Calc.Calculator.Initialize();

            DBUtils.Init();
        }
    }
}


public class DBUtils
{
    public static Dictionary<int, GroupData> Groups;

    public static List<TestPlayerData> Players;

    public static void Init()
    {
        if (Groups == null)
        {
            using (var db = DBUtils.OpenUniqueDB())
            {
                var cmd = DBCommandBuilder.SelectGroups();
                using (var res = db.DoQuery(cmd))
                {
                    Groups = new Dictionary<int, GroupData>();
                    foreach (var item in DBObjectConverter.Convert<DBGroup>(res).ToList())
                    {
                        Groups.Add(item.ID, new GroupData { Origin = item });
                    }
                }
            }
        }

        if (Players == null)
        {
            var count = 4000;
            Players = new List<TestPlayerData>(count);
            for (int i = 0; i < count; ++i) Players.Add(new TestPlayerData { Index = i });
        }
    }

    public static void QuestTestPlayers()
    {
        using (var db = DBUtils.OpenUniqueDB())
        {
            var cmd = DBUtils.SelectTestPlayer();
            using (var res = db.DoQuery(cmd))
            {
                foreach (var item in DBObjectConverter.Convert<DBTestPlayer>(res))
                {
                    if (item.Key >= 1 && item.Key <= DBUtils.Players.Count) DBUtils.Players[item.Key - 1].Origin = item;
                }
            }
        }
    }

    public static IDBConnection OpenUniqueDB()
    {
        var db = new MySqlDBConnection();
        //db.Open("Server=127.0.0.1;Port=3306;Database=csaga_unique;Uid=root;Pwd=miKan.!@#;CharSet=utf8;");
        db.Open("Server=172.31.7.157;Port=3306;Database=csaga_unique;Uid=root;Pwd=miKan.!@#;CharSet=utf8;");
        return db;
    }
    public static IDBConnection OpenLogDB()
    {
        var db = new MySqlDBConnection();
        //db.Open("Server=127.0.0.1;Port=3306;Database=csaga_unique;Uid=root;Pwd=miKan.!@#;CharSet=utf8;");
        db.Open("Server=172.31.7.157;Port=3306;Database=csaga_log;Uid=root;Pwd=miKan.!@#;CharSet=utf8;");
        return db;
    }

    public static IDBConnection OpenDB(string connStr)
    {
        var db = new MySqlDBConnection();
        db.Open(connStr);
        return db;
    }
    public static IDBCommand CreateNoLockDBCommand(string statement)
    {
        return DBCommandBuilder.CreateNoLockDBCommand(statement);
    }

    public static IDBCommand CreateNoLockDBCommand(string statement, int group)
    {
        return DBCommandBuilder.CreateNoLockDBCommand(statement, group);
    }

    public static IDBCommand SelectTestPlayer()
    {
        const string text = "SELECT * FROM `test_player`;";

        var cmd = CreateNoLockDBCommand(text);

        return cmd;
    }

    public static IDBCommand SelectAccount(PublicID id)
    {
        const string text = "SELECT * FROM `account` WHERE `group_id`=@group_id AND `sub_id`=@sub_id;";
        var cmd = CreateNoLockDBCommand(text);

        cmd.AddParameter("@group_id", id.Group);
        cmd.AddParameter("@sub_id", id.Serial);

        return cmd;
    }

    public static IDBCommand SelectAccountCount()
    {
        const string text = "SELECT COUNT(*) FROM `account` WHERE `sub_id` != 0;";

        var cmd = CreateNoLockDBCommand(text);

        return cmd;
    }

    public static IDBCommand SelectAccount(int from, int count)
    {
        const string text = "SELECT * FROM `account` WHERE `sub_id` != 0 LIMIT @from, @to;";

        var cmd = CreateNoLockDBCommand(text);

        cmd.AddParameter("@from", from);
        cmd.AddParameter("@to", from + count);

        return cmd;
    }

    public static IDBCommand SelectAccount(DateTime from, DateTime to)
    {
        const string text = "SELECT * FROM `account` WHERE `sub_id` != 0 AND `create_time` >= @from AND `create_time` < @to;";
        var cmd = CreateNoLockDBCommand(text);

        cmd.AddParameter("@from", from);
        cmd.AddParameter("@to", to);

        return cmd;
    }

    public static IDBCommand SelectPlayer(int group, DateTime from, DateTime to)
    {
        const string text = "SELECT * FROM `player_{0:00}` WHERE `complex` != 0 AND `last_play_time` >= @from AND `last_play_time` < @to;";
        var cmd = CreateNoLockDBCommand(text, group);

        cmd.AddParameter("@from", from);
        cmd.AddParameter("@to", to);

        return cmd;
    }

    public static IDBCommand SelectPlayer(int group, string name)
    {
        const string text = "SELECT * FROM `player_{0:00}` WHERE `name` LIKE @name;";
        var cmd = CreateNoLockDBCommand(text, group);

        cmd.AddParameter("@name", name);

        return cmd;
    }

    public static IDBCommand SelectLastQuestID(int group, Int64 playerId, int limit)
    {
        const string text = "SELECT COALESCE(MAX(quest_id), 0) FROM `quest_{0:00}` WHERE `player_id`=@player_id AND complex!=0 AND quest_id<@limit;";
        var cmd = DBCommandBuilder.CreateNoLockDBCommand(text, group);

        cmd.AddParameter("@player_id", playerId);
        cmd.AddParameter("@limit", limit);

        return cmd;
    }
    public static IDBCommand SelectLogQuest()
    {
        const string text = "SELECT * FROM `log_quest`;";

        var cmd = DBCommandBuilder.CreateNoLockDBCommand(text);

        return cmd;
    }

    public static IDBCommand SelectPurchase(int group)
    {
        const string text = "SELECT * FROM `purchase_{0:00}` ORDER BY `id` DESC LIMIT 10000;";

        var cmd = DBCommandBuilder.CreateNoLockDBCommand(text, group);

        return cmd;
    }

    public static IDBCommand SelectPlayer(int group, DateTime lastPlay)
    {
        const string text = "SELECT * FROM `player_{0:00}` WHERE `complex`!=0 AND `last_play_time`>=@last_play;";

        var cmd = DBCommandBuilder.CreateNoLockDBCommand(text, group);

        cmd.AddParameter("@last_play", lastPlay);

        return cmd;
    }

    public static IDBCommand InsertSuperAccount(PublicID id)
    {
        const string text = "INSERT INTO `gm`(`group_id`,`player_id`) VALUES (@group_id, @player_id);";

        var cmd = DBCommandBuilder.CreateDBCommand(text);

        cmd.AddParameter("@group_id", id.Group);
        cmd.AddParameter("@player_id", id.Serial);

        return cmd;
    }

    public static IDBCommand SelectSuperAccount()
    {
        const string text = "SELECT * FROM `gm`;";

        var cmd = DBCommandBuilder.CreateNoLockDBCommand(text);

        return cmd;
    }
}

public class GroupData
{
    public DBGroup Origin;
    public int ID { get { return Origin.ID; } }
    public string ConnStr { get { return Origin.ConnectionString; } }
}

public class TestPlayerData
{
    const byte KEY = 0xA0;

    public DBPlayer Detail;
    public DBTestPlayer Origin;

    private bool IsValid { get { return Origin != null; } }

    public int Index;

    public int ID { get { return Index + 1; } }

    public string Code
    {
        get
        {
            var original = MathUtils.Seed.Next(ID).ToString("000000");

            var val = Encoding.UTF8.GetBytes(original);

            for (int j = 0; j < val.Length; ++j) val[j] ^= KEY;
            return Convert.ToBase64String(val);
        }
    }

    public string GetRawPlayerID()
    {
        if (!IsValid) return "--";
        var id = PublicID.Create(Origin.GroupID, Origin.PlayerID).ToString();
        return id;
    }

    public string PlayerID
    {
        get
        {
            if (!IsValid) return "--";
            var id = PublicID.Create(Origin.GroupID, Origin.PlayerID).ToString();
            id = id.Insert(6, ",");
            id = id.Insert(3, ",");
            return id;
        }
    }

    public string CreateTime { get { return IsValid ? Origin.Timestamp.ToString("yyyy/MM/dd HH:mm:ss") : "--"; } }


    public string Name { get { return Detail != null ? Detail.Name : "--"; } }

    public int LV { get { return PlayerUtils.CalcLV(Exp); } }

    public int Exp { get { return Detail != null ? Detail.Exp : -1; } }

    public int Crystal { get { return Detail != null ? Detail.Crystal : -1; } }

    public int Coin { get { return Detail != null ? Detail.Coin : -1; } }

    public int QuestID { get; set; }

    public string LastPlay { get { return Detail != null ? Detail.LastPlayTime.ToString("yyyy/MM/dd HH:mm:ss") : "--"; } }
}