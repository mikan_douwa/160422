﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Mikan.CSAGA;
using Mikan.CSAGA.Server;
using Mikan.Server;
using Mikan;
using System.Text;

public partial class cb : System.Web.UI.Page
{
    
    private void QueryPlayerData(int exp, int questId)
    {
        DBUtils.QuestTestPlayers();

        var list = new List<TestPlayerData>();

        for (int i = 0; i < DBUtils.Players.Count; ++i)
        {
            var obj = DBUtils.Players[i];
            if (obj.Origin == null) continue;
            if (obj.Exp < exp) continue;
            using (var playerDb = DBUtils.OpenDB(DBUtils.Groups[obj.Origin.GroupID].ConnStr))
            {
                var cmd = DBCommandBuilder.SelectPlayer(PublicID.Create(obj.Origin.GroupID, obj.Origin.PlayerID));
                using (var playerRes = playerDb.DoQuery(cmd)) obj.Detail = DBObjectConverter.ConvertCurrent<DBPlayer>(playerRes);
                cmd = DBUtils.SelectLastQuestID(obj.Origin.GroupID, obj.Origin.PlayerID, 10000);

                obj.QuestID = playerDb.DoQuery<int>(cmd);

                if (obj.QuestID < questId) continue;
            }

            list.Add(obj);
        }

        GridView1.DataSource = list;
        GridView1.DataBind();
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        QueryPlayerData(int.Parse(TextBox1.Text), int.Parse(TextBox2.Text));
    }
}

