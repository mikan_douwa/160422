﻿<%@ Page Language="C#" MasterPageFile="MasterPage.master" AutoEventWireup="true" CodeFile="player.aspx.cs" Inherits="player" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <!--  page-wrapper -->
        <div id="page-wrapper">
            <div class="row">
                 <!--  page header -->
                <div class="col-lg-12">
                    <h1 class="page-header">玩家查詢</h1>
                </div>
                 <!-- end  page header -->
            </div>
            <div class="row">
                <div class="col-lg-12">
                     <!--   Basic Table  -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            玩家查詢
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                查詢類型
                                <asp:DropDownList ID="DropDownList1" runat="server">
                                    <asp:ListItem Value="1">玩家ID</asp:ListItem>
                                    <asp:ListItem Value="2">玩家名稱</asp:ListItem>
                                    <asp:ListItem Value="3">創建日期(yyyy/MM/dd)</asp:ListItem>
                                    <asp:ListItem Value="4">登入日期(yyyy/MM/dd)</asp:ListItem>
                                    <asp:ListItem Value="5">DBID</asp:ListItem>
                                </asp:DropDownList>
                            &nbsp; 條件1
                                <asp:TextBox ID="TextBox1" runat="server" Width="100px"></asp:TextBox>
                            &nbsp; 條件2
                                <asp:TextBox ID="TextBox2" runat="server" Width="100px"></asp:TextBox>
                            &nbsp; 
                                <asp:Button ID="Button1" runat="server" Text="送出查詢" onclick="Button1_Click" />
                                <asp:CheckBox ID="CheckBox1" runat="server" Text="包含任務進度" />
                                <asp:CheckBox ID="CheckBox2" runat="server" Text="包含購買寶石數量" />
                            </div> 
                            <div class="table-responsive">
                                
                                <asp:GridView ID="GridView1" runat="server" Width="1280px" >
                                    <RowStyle HorizontalAlign="Center" />
                                </asp:GridView>
                                
                            </div>
                        </div>
                    </div>
                      <!-- End  Basic Table  -->
                </div>
            </div>
            

        </div>
        <!-- end page-wrapper -->
   
</asp:Content>
