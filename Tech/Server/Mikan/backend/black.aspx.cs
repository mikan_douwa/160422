﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Mikan.CSAGA;
using Mikan.CSAGA.Server;
using Mikan.Server;
using Mikan;

public partial class black : System.Web.UI.Page
{

    private Dictionary<PublicID, BlackData> QueryBlack()
    {
        var map = PublicID.CreateDictinary<BlackData>();

        using (var db = DBUtils.OpenUniqueDB())
        {
            var cmd = BlackUtils.SelectBlack();
            using (var res = db.DoQuery(cmd))
            {
                foreach (var item in DBObjectConverter.Convert<DBBlack>(res))
                {
                    var id = PublicID.Create(item.GroupID, item.PlayerID);

                    var data = map.SafeGetValue(id);

                    if (data == null)
                    {
                        data = new BlackData();
                        data.ID = id.ToString();
                        map.Add(id, data);
                    }

                    if (data.Origin == null || data.Origin.Timestamp < item.Timestamp) data.Origin = item;

                    ++data.Count;

                }
            }
        }

        return map;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        var map = QueryBlack();
        
        var list = new List<BlackData>();

        foreach (var item in map)
        {
            var data = item.Value;

            using (var db = DBUtils.OpenDB(DBUtils.Groups[item.Key.Group].ConnStr))
            {
                var cmd = DBCommandBuilder.SelectEventPoint(item.Key, 0);

                var ep = default(DBEventPoint);

                using (var res = db.DoQuery(cmd))
                {
                    foreach (var obj in DBObjectConverter.Convert<DBEventPoint>(res))
                    {
                        if (ep == null || ep.EventID < obj.EventID) ep = obj;
                    }                    
                }

                if (ep == null) continue;
                data.EventID = ep.EventID;
                data.Point = ep.Point;

                cmd = DBCommandBuilder.SelectPlayer(item.Key);

                using (var res = db.DoQuery(cmd))
                {
                    var dbPlayer = DBObjectConverter.ConvertCurrent<DBPlayer>(res);
                    data.Name = dbPlayer.Name;
                    data.LastPlay = dbPlayer.LastPlayTime.ToString();

                }

                cmd = DBCommandBuilder.SelectIAPGem(item.Key);

                using (var res = db.DoQuery(cmd))
                {
                    data.IAPGem = Decimal.ToInt32(res.Field<Decimal>());
                }

                list.Add(data);
            }
        }

        list.Sort((lhs, rhs) => { return rhs.Point.CompareTo(lhs.Point); });

        GridView1.DataSource = list;
        GridView1.DataBind();
        
    }
}

public class BlackData
{
    public DBBlack Origin;
    public string DBID { get { return string.Format("{0},{1}", Origin.GroupID, Origin.PlayerID); } }

    public string ID { get; set; }

    public string Name { get; set; }

    public int Count { get; set; }

    public Int64 EventID { get; set; }

    public int Point { get; set; }

    public int IAPGem { get; set; }

    public string LastPlay { get; set; }

    public string LastUse { get { return Origin.Timestamp.ToString(); } }


}

class BlackUtils
{
    public static IDBCommand SelectBlack()
    {
        const string text = "SELECT * FROM `black`;";

        var cmd = DBCommandBuilder.CreateNoLockDBCommand(text);

        return cmd;
    }
}