﻿<%@ Page Language="C#" MasterPageFile="MasterPage.master" AutoEventWireup="true" CodeFile="iap.aspx.cs" Inherits="iap" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <!--  page-wrapper -->
        <div id="page-wrapper">
            <div class="row">
                 <!--  page header -->
                <div class="col-lg-12">
                    <h1 class="page-header">IAP查詢</h1>
                </div>
                 <!-- end  page header -->
            </div>
            <div class="row">
                <div class="col-lg-12">
                     <!--   Basic Table  -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            IAP查詢
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                            &nbsp; 起始日期(>=)
                                <asp:TextBox ID="TextBox1" runat="server" Width="100px"></asp:TextBox>
                            &nbsp; 結束日期(<)
                                <asp:TextBox ID="TextBox2" runat="server" Width="100px"></asp:TextBox>
                            &nbsp; 數量
                                <asp:TextBox ID="TextBox3" runat="server" Width="100px"></asp:TextBox>
                            &nbsp; 
                                <asp:Button ID="Button1" runat="server" Text="送出查詢" onclick="Button1_Click" />
                            </div> 
                            <div class="table-responsive">
                                
                                <asp:GridView ID="GridView1" runat="server" Width="1280px" >
                                    <RowStyle HorizontalAlign="Center" />
                                </asp:GridView>
                                
                            </div>
                        </div>
                    </div>
                      <!-- End  Basic Table  -->
                </div>
            </div>
            

        </div>
        <!-- end page-wrapper -->
   
</asp:Content>
