﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Mikan.CSAGA;
using Mikan.CSAGA.Server;
using Mikan.Server;
using Mikan;
using System.Text;

public partial class tables2 : System.Web.UI.Page
{
    private bool isPageLoaded;

    protected void Page_Load(object sender, EventArgs e)
    {

        if (!isPageLoaded)
        {
            QueryPlayerData(0);

            GridView1.DataSource = DBUtils.Players;
            GridView1.DataBind();

            isPageLoaded = true;
        }

    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        QueryPlayerData(e.NewPageIndex);

        GridView1.PageIndex = e.NewPageIndex;

        GridView1.DataSource = DBUtils.Players;
        GridView1.DataBind();
    }

    private void QueryPlayerData(int pageIndex)
    {
        DBUtils.QuestTestPlayers();

        var count = GridView1.PageSize;

        for (int i = pageIndex * count; i < pageIndex * count + count; ++i)
        {
            var obj = DBUtils.Players[i];
            if (obj.Origin == null) continue;

            using (var playerDb = DBUtils.OpenDB(DBUtils.Groups[obj.Origin.GroupID].ConnStr))
            {
                var cmd = DBCommandBuilder.SelectPlayer(PublicID.Create(obj.Origin.GroupID, obj.Origin.PlayerID));
                using (var playerRes = playerDb.DoQuery(cmd)) obj.Detail = DBObjectConverter.ConvertCurrent<DBPlayer>(playerRes);
                cmd = DBUtils.SelectLastQuestID(obj.Origin.GroupID, obj.Origin.PlayerID, 10000);

                obj.QuestID = playerDb.DoQuery<int>(cmd);
            }
        }
        /*
        using (var db = DBUtils.OpenUniqueDB())
        {
            var cmd = DBUtils.SelectAccount(pageIndex * count, count);

            using (var res = db.DoQuery(cmd))
            {
                var index = pageIndex * count;
                foreach (var item in DBObjectConverter.Convert<DBAccount>(res))
                {
                    var obj = players[index];

                    ++index;
                    
                    using (var playerDb = DBUtils.OpenDB(groups[item.GroupID].ConnStr))
                    {
                        cmd = DBCommandBuilder.SelectPlayer(item.ID, item.GroupID);
                        using (var playerRes = playerDb.DoQuery(cmd)) obj.Detail = DBObjectConverter.ConvertCurrent<DBPlayer>(playerRes);
                        cmd = DBUtils.SelectLastQuestID(item.GroupID, item.SubID, 10000);

                        obj.QuestID = playerDb.DoQuery<int>(cmd);
                    }
                    
                }
            }
        }
        */
    }
}

