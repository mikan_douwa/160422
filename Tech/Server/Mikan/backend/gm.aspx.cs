﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Mikan.CSAGA.Server;
using Mikan;
using Mikan.Server;
using Mikan.CSAGA.Server.Tasks;

public partial class gm : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        using (var db = DBUtils.OpenUniqueDB())
        {
            ListAccount(db);
        }
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        var id = PublicID.FromString(TextBox1.Text);

        if (id == null || !id.IsValid)
        {
            ShowError("無效的ID");
            return;
        }

        using (var db = DBUtils.OpenUniqueDB())
        {
            var cmd = DBCommandBuilder.SelectSuperAccount(id);

            using (var res = db.DoQuery(cmd))
            {
                if (!res.IsEmpty) return;
            }

            cmd = DBUtils.InsertSuperAccount(id);

            db.SimpleQuery(cmd);

            ListAccount(db);
        }
        
    }

    private void ShowError(string msg)
    {
        GridView1.DataSource = new GMNoData[] { new GMNoData{ Message = msg } }; 
        GridView1.DataBind();
    }

    private void ListAccount(IDBConnection db)
    {
        var cmd = DBUtils.SelectSuperAccount();

        using (var res = db.DoQuery(cmd))
        {
            var list = new List<GMData>();
            foreach (var item in DBObjectConverter.Convert<DBGM>(res))
            {
                var obj = PublicID.Create(item.GroupID, item.PlayerID);
                list.Add(new GMData { PublicID = obj });
            }
            Bind(list);
        }
    }
    
    private void Bind(List<GMData> list)
    {
        if (list.Count > 0)
        {
            GridView1.DataSource = list;
            GridView1.DataBind();
        }
        else
            ShowError("查無資料");
    }
}
class GMNoData
{
    public string Message { get; set; }
}
class GMData
{
    public PublicID PublicID;
    public int DB { get { return PublicID.Group; } }
    public Int64 Serial { get { return PublicID.Serial; } }
    public string ID { get { return PublicID.ToString(); } }
}