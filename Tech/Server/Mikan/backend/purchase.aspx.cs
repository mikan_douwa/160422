﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Mikan.CSAGA;
using Mikan.CSAGA.Server;
using Mikan.Server;
using Mikan;

public partial class purchase : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        var map = new Dictionary<string, PurchaseData>();

        

        foreach (var group in DBUtils.Groups)
        {
            using (var db = DBUtils.OpenDB(group.Value.ConnStr))
            {
                var cmd = DBUtils.SelectPurchase(group.Value.ID);

                using (var res = db.DoQuery(cmd))
                {
                    foreach (var item in DBObjectConverter.Convert<DBPurchase>(res))
                    {
                        var date = item.Timestamp.ToString("yyyy/MM/dd");
                        var obj = DictionaryUtils.SafeGetValue(map, date);
                        obj.Date = date;
                        var values = DictionaryUtils.SafeGetValue(obj.List, (ArticleID)item.ArticleID);
                        values.OriginValue += item.Cost;
                        values.NewValue += item.IAPCost;
                    }
                }
            }
        }
        var list = new List<PurchaseData>(map.Count);

        foreach (var item in map) list.Add(item.Value);

        list.Sort((lhs, rhs) => { return rhs.Date.CompareTo(lhs.Date); });

        GridView1.DataSource = list;
        GridView1.DataBind();
    }
}

public class PurchaseData
{
    public string Date { get; set; }

    public Dictionary<ArticleID, Mikan.ValuePair> List = new Dictionary<ArticleID, Mikan.ValuePair>();

    public string CardSpace { get { return Get(ArticleID.CardSpace); } }

    public string Continue { get { return Get(ArticleID.Continue); } }

    public string QuestCount { get { return Get(ArticleID.QuestCount); } }

    public string ShopSlot { get { return Get(ArticleID.RandomShopSlot); } }

    public string Commu { get { return Get(ArticleID.TalkPoint); } }

    public string Help { get { return Get(ArticleID.RescuePoint); } }

    public string Gacha1 { get { return Get(ArticleID.RegularGacha, ArticleID.RegularGachaDaily); } }

    public string Gacha2 { get { return Get(ArticleID.ActivityGacha, ArticleID.ActivityGachaDaily); } }

    private string Get(params ArticleID[] ids)
    {
        var cost = 0;
        var iapCost = 0;
        foreach(var id in ids)
        {
            var values = DictionaryUtils.SafeGetValueNullable(List, id);
            if (values != null)
            {
                cost += values.OriginValue;
                iapCost += values.NewValue;
            }
        }

        return string.Format("{0}({1})", cost, iapCost);
    }
}