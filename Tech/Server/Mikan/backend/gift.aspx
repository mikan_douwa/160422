﻿<%@ Page Language="C#" MasterPageFile="MasterPage.master" AutoEventWireup="true" CodeFile="gift.aspx.cs" Inherits="gift" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <!--  page-wrapper -->
        <div id="page-wrapper">
            <div class="row">
                 <!--  page header -->
                <div class="col-lg-12">
                    <h1 class="page-header">贈獎</h1>
                </div>
                 <!-- end  page header -->
            </div>
            <div class="row">
                <div class="col-lg-12">
                     <!--   Basic Table  -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            贈獎
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                獎勵類型
                                <asp:DropDownList ID="DropDownList1" runat="server">
                                    <asp:ListItem Value="1">水晶</asp:ListItem>
                                    <asp:ListItem Value="3">卡片</asp:ListItem>
                                    <asp:ListItem Value="4">道具</asp:ListItem>
                                    <asp:ListItem Value="5">代幣</asp:ListItem>
                                    <asp:ListItem Value="6">寶石</asp:ListItem>
                                </asp:DropDownList>
                            &nbsp; 數值1
                                <asp:TextBox ID="TextBox1" runat="server" Width="100px"></asp:TextBox>
                            &nbsp; 數值2
                                <asp:TextBox ID="TextBox2" runat="server" Width="100px"></asp:TextBox>
                            &nbsp; 獎勵描述
                                <asp:TextBox ID="TextBox4" runat="server">贈獎</asp:TextBox>
                            &nbsp; 對象
                                <asp:DropDownList ID="DropDownList2" runat="server">
                                    <asp:ListItem Value="1">ID列表(ID1,ID2,ID3...)</asp:ListItem>
                                    <asp:ListItem Value="2">登入日期(yyyy/MM/dd)</asp:ListItem>
                                </asp:DropDownList>
                                <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
                                <asp:Button ID="Button1" runat="server" Text="送出" onclick="Button1_Click" />
                            </div> 
                            <div class="table-responsive">
                                
                                <asp:GridView ID="GridView1" runat="server" Width="512px">
                                </asp:GridView>
                                
                            </div>
                        </div>
                    </div>
                      <!-- End  Basic Table  -->
                </div>
            </div>
            

        </div>
        <!-- end page-wrapper -->
   
</asp:Content>
