﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Reflection;
using Mikan;
using Mikan.Server;
using System.IO;
namespace Host
{
    public static class Kernel
    {
        private static List<IHttpService> services;

        private static Host host;

        private static bool CheckIsActiveService(Type type)
        {
            var exists = TypeUtils.HasAttribute<ActiveServiceAttribute>(type);
            return exists;
        }

        public static void Execute()
        {
            Scheduler.Start();

            try
            {
                if(host == null) host = new Host();

                host.Logger.Info("[Application Start]");

                if (services != null) return;

                services = new List<IHttpService>();

                var asm = Assembly.Load(host.Config["assembly"]);

                foreach (var serviceType in TypeUtils.Select(asm, CheckIsActiveService))
                {
                    var serviceObj = Activator.CreateInstance(serviceType);

                    var service = serviceObj as IHttpService;

                    if (service != null)
                    {
                        service.Execute(host);

                        service.Host.Logger.Info("service [{0}] executing...", service);

                        services.Add(service);
                    }
                }
                
            }
            catch (Exception e)
            {
                Console.Error.Write(e);
                End();
            }
        }

        public static void End()
        {
            Scheduler.Stop();

            if (services != null)
            {

                foreach (var service in services)
                {
                    service.Host.Logger.Info("service [{0}] disposing...", service);

                    service.Dispose();
                }

                services.Clear();

                services = null;
            }

            if (host != null)
            {
                host.Dispose();
                host = null;
            }
        }
    }

    class Host : IHost, IDisposable
    {
        
        public IConfig Config { get; private set; }

        public ILogger Logger { get { return KernelService.Logger; } }

        private Logger logger;

        public Host()
        {
            logger = new Logger();
            KernelService.Logger = logger;
            Config = ConfigParser.Parse(File.ReadAllLines(AppDomain.CurrentDomain.BaseDirectory + "configuration.ini"));
        }



        public void Dispose()
        {
            logger.Info("[Application End]", 0);
            logger.Dispose();
        }
    }

    class Logger : LoggerBase, IDisposable
    {
        private StreamWriter writer;
        private DateTime fileDate;

        public Logger()
        {
            //CreateNewFile(DateTime.Now.Date);
        }

        public override void AddLog(LogPriority priority, string log)
        {
            //*/
            Console.Error.WriteLine("{0} - {1}", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), log);
            /*/
            lock (this)
            {
                var now = DateTime.Now;

                if (now.Date != fileDate) CreateNewFile(now);

                writer.WriteLine("{0} - {1}", now.ToString("yyyy-MM-dd HH:mm:ss"), log);
            }
            ///*/
        }

        public void Dispose()
        {
            if (writer != null)
            {
                writer.Dispose();
                writer = null;
            }
        }

        private void CreateNewFile(DateTime now)
        {
            if (writer != null)
            {
                writer.Flush();
                writer.Dispose();
            }

            fileDate = now;

            var dateStr = fileDate.ToString("yyMMdd");

            var filename = string.Format("C:\\debug\\{0}.log", dateStr);

            var count = 0;

            while (File.Exists(filename)) filename = string.Format("C:\\debug\\{0}-{1:00}.log", dateStr, ++count);

            writer = new StreamWriter(File.Open(filename, FileMode.CreateNew, FileAccess.ReadWrite, FileShare.Read));
            writer.AutoFlush = true;
        }
    }
}