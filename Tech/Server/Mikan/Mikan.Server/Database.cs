﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Mikan.Server
{
    public interface IDBConnection : IDisposable
    {
        void Open(string connectionString);

        IDBTransaction BeginTransaction();
        IDBTransaction BeginTransaction(IsolationLevel level);

        IDBQueryResult DoQuery(IDBCommand cmd);

        T DoQuery<T>(IDBCommand cmd);

        IDBQueryResult SafeDoQuery(IDBCommand cmd);

        void SimpleQuery(IDBCommand cmd);

        bool SafeSimpleQuery(IDBCommand cmd);
    }

    public interface IDBTransaction : IDisposable
    {
        void Commit();
        void Rollback();
    }

    public interface IDBQuery
    {
        void BeginQuery(IDBConnection db);
    }

    public interface IDBQueryResult : IDisposable
    {
        string Error { get; }

        bool IsEmpty { get; }

        bool IsSucc { get; }

        bool IsDbNull(string column);

        T Field<T>(string column);

        T Field<T>();

        object this[string column] { get; }

        bool NextRow();

        int RecordsAffected { get; }
    }
}