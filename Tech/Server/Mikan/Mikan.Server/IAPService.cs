﻿using System;
using System.Net;
using System.IO;
using System.Collections.Generic;
using System.Web;
using System.Text;
using System.Web.Script.Serialization;

namespace Mikan.Server.IAP.iOS
{
    class ReceiptData
    {
        public string bundle_id;
        public string bid;
        public string product_id;
        public string transaction_id;
    }
    class VarifyResultData
    {
        public ReceiptData receipt;
        public int status = -1;
        public bool IsSucc = false;
        public bool IsNetFail = false;
    }

    public class IAPCheckResult
    {
        internal VarifyResultData Rawdata;

        public int Status { get { return Rawdata.status; } }
        public bool IsSucc { get { return Rawdata.IsSucc; } }
        public bool IsSandbox { get { return Rawdata.status == 21007; } }
        public bool IsNetFail { get { return Rawdata.IsNetFail; } }

        public string BundleID
        {
            get
            {
                if (!string.IsNullOrEmpty(Rawdata.receipt.bundle_id)) return Rawdata.receipt.bundle_id;
                if (!string.IsNullOrEmpty(Rawdata.receipt.bid)) return Rawdata.receipt.bid;

                return "";
            }
        }

        public string ProductID { get { return Rawdata.receipt.product_id; } }

        public string TransactionID { get { return Rawdata.receipt.transaction_id; } }
    }

    public class IAPService
    {
        private string bundleId;
        public IAPService(string bundleId)
        {
            this.bundleId = bundleId;
        }
        public IAPCheckResult Check(string receipt, string productId)
        {
            var res = new IAPCheckResult();

            var json = "{\"receipt-data\":\"" + receipt + "\" }";

            res.Rawdata = RemoteCheck("https://buy.itunes.apple.com/verifyReceipt", json);

            if (!res.Rawdata.IsSucc || res.Rawdata.status == 21007)
                res.Rawdata = RemoteCheck("https://sandbox.itunes.apple.com/verifyReceipt", json);

            if (res.Rawdata.IsSucc)
            {
                if (res.BundleID != bundleId || res.ProductID != productId)
                    res.Rawdata.IsSucc = false;
            }

            return res;
        }

        private VarifyResultData RemoteCheck(string url, string json)
        {
            try
            {
                var req = (HttpWebRequest)WebRequest.Create(url);
                req.Method = "POST";

                using (var writer = new StreamWriter(req.GetRequestStream()))
                {
                    writer.Write(json);
                }

                var res = (HttpWebResponse)req.GetResponse();

                using(var reader = new StreamReader(res.GetResponseStream()))
                {
                    var text = reader.ReadToEnd();
                  
                    var result = Newtonsoft.Json.JsonConvert.DeserializeObject<VarifyResultData>(text);

                    result.IsSucc = result.status == 0 || result.status == 21006 || result.status == 21007;

                    return result;
                }
            }
            catch (Exception ex)
            {
                KernelService.Logger.Fatal("IAP failed, error = {0}", ex.ToString());
                return new VarifyResultData{ IsNetFail = true };
            }
        }
    }

}

namespace Mikan.Server.IAP.MyCard
{
    public class Setting
    {
        public const int IS_VALID = 0;
        public const int IS_TRADING = 1;
        public const int IS_TRANS_COMPLET = 2;

        private const string OfficalAuthUrl = "https://b2b.mycard520.com.tw/MyBillingPay/api/Auth";
        private const string OfficalTradeUrl = "https://b2b.mycard520.com.tw/MyBillingPay/api/TradeQuery";
        private const string OfficalConfirmUrl = "https://b2b.mycard520.com.tw/MyBillingPay/api/PaymentConfirm";
        private const string OfficalQueryUrl = "https://b2b.mycard520.com.tw/MyBillingPay/api/SDKTradeQuery";

        private const string SandboxAuthUrl = "http://test.b2b.mycard520.com.tw/MyBillingPay/api/Auth";
        private const string SandboxTradeUrl = "http://test.b2b.mycard520.com.tw/MyBillingPay/api/TradeQuery";
        private const string SandboxConfirmUrl = "http://test.b2b.mycard520.com.tw/MyBillingPay/api/PaymentConfirm";
        private const string SandboxQueryUrl = "http://test.b2b.mycard520.com.tw/MyBillingPay/api/SDKTradeQuery";

        public string AuthUrl { get { return Sandbox ? SandboxAuthUrl : OfficalAuthUrl; } }
        public string TradeUrl { get { return Sandbox ? SandboxTradeUrl : OfficalTradeUrl; } }
        public string ConfirmUrl { get { return Sandbox ? SandboxConfirmUrl : OfficalConfirmUrl; } }
        public string QueryUrl { get { return Sandbox ? SandboxQueryUrl : OfficalQueryUrl; } }

        public string ServerID { get; set; }
        public string Key { get; set; }
        public bool Sandbox { get; set; }
    }
    public class Transaction
    {
        public bool IsValid { get { return Rawdata != null && Rawdata.ReturnCode == "1"; } }
        public string AuthCode { get { return Rawdata.AuthCode; } }
        public string TradeSeq { get { return Rawdata.TradeSeq; } }
        internal AuthResultData Rawdata;
    }

    public class IAPReceipt
    {
        public bool IsSucc { get; internal set; }

        public bool IsCancelled { get; internal set; }

        public bool IsTradeComplete { get; internal set; }

        public Int64 Serial { get; internal set; }
    }

    public class IAPCheckResult
    {
        public ReceiptData Receipt { get; internal set; }

        public Int64 Serial { get; internal set; }

        public bool IsSucc { get; internal set; }
        public bool IsSandbox { get; internal set; }
        public bool IsNetFail { get; internal set; }

        public string OrderID { get { return Receipt.MyCardTradeNo; } }

        public int Bonus { get; internal set; }
    }

    public class IAPRestoreList
    {
        internal RestoreData Rawdata;

        public bool IsValid { get; internal set; }

        public List<Int64> Transactions { get; internal set; }

    }

    public class IAPService
    {
        private const string TRADE_TYPE = "1";
        private const string CURRENCY = "TWD";

        private string key;

        private Setting setting;
        
        private Dictionary<int, Product> products;

        private Dictionary<string, int> promoCodes;

        public bool Sandbox { get { return setting.Sandbox; } }

        public IAPService(Setting setting)
        {
            this.setting = setting;

            var hash = Sha256(setting.Key);
            key = hash.Substring(0, 10) + hash.Substring(31, 10) + hash.Substring(50, 6) + hash.Substring(18, 2);
            products = new Dictionary<int, Product>();
            promoCodes = new Dictionary<string, int>();
        }

        public void AddProduct(int id, string name, int price)
        {
            products[id] = new Product { Name = name, Price = price };
        }

        public void AddPromoCode(string code, int bonus)
        {
            promoCodes[code] = bonus;
        }

        public Transaction BeginTransaction(PublicID playerId, Int64 tradeSeq, int productId)
        {
            var transaction = new Transaction();

            var product = products[productId];

            var prefix = Sandbox ? "S" : "T";
            var _tradeSeq = prefix + tradeSeq.ToString("0000000000");
            var _playerId = playerId.ToString();
            var _price = product.Price.ToString();
            var _sandbox = Sandbox ? "true" : "false";

            string ParaValue = "FacServiceId=" + setting.ServerID + "&FacTradeSeq=" + _tradeSeq + "&TradeType=" + TRADE_TYPE +
                "&CustomerId=" + _playerId +
                "&ProductName=" + product.Name + "&Amount=" + _price +
                "&Currency=" + CURRENCY + "&SandBoxMode=" + _sandbox;

            string preHashValue = setting.ServerID + _tradeSeq + TRADE_TYPE + _playerId +
                product.Name + _price + CURRENCY + _sandbox + key;

            string EncodeHashValue = HttpUtility.UrlEncode(preHashValue);
            var hash = Sha256(EncodeHashValue);

            ParaValue += "&Hash=" + hash;

            KernelService.Logger.Debug("[MYCARD] begin transaction, params = {0}", ParaValue);

            string returnMsg = "";
            //Step3.1 Call Auth
            bool SendStatus = Send_MyCard_b2b(setting.AuthUrl, ParaValue, out returnMsg);

            if (SendStatus && !string.IsNullOrEmpty(returnMsg))
            {
                var result = Newtonsoft.Json.JsonConvert.DeserializeObject<AuthResultData>(returnMsg);
                transaction.Rawdata = result;
            }

            return transaction;
        }

        public IAPReceipt Convert(string receipt)
        {
            WriteLog("", string.Format("convert receipt, rawdata = {0}", receipt));

            var res = new IAPReceipt();
            var rawdata = Newtonsoft.Json.JsonConvert.DeserializeObject<ClientReceiptData>(receipt);

            if (rawdata.returnCode == "1")
            {
                var prefix = Sandbox ? "S" : "T";

                if (string.IsNullOrEmpty(rawdata.facTradeSeq) || !rawdata.facTradeSeq.StartsWith(prefix))
                {
                    WriteLog("illegal trade seq format, value={0}, expect={1}###########", rawdata.facTradeSeq, prefix);
                }
                else
                {
                    res.Serial = Int64.Parse(rawdata.facTradeSeq.Substring(1));
                    res.IsSucc = true;
                    res.IsTradeComplete = (rawdata.payResult == "3");
                }
            }
            else
            {
                res.IsSucc = true;
                res.IsCancelled = true;
            }

            return res;
        }

        public IAPCheckResult Check(PublicID id, string authCode)
        {
            string ParaValue = "AuthCode=" + authCode;

            string returnMsg = "";
            //Step3.3 Call TradeQuery
            bool SendStatus = Send_MyCard_b2b(setting.TradeUrl, ParaValue, out returnMsg);

            var res = new IAPCheckResult();

            if (SendStatus)
            {
                if (!string.IsNullOrEmpty(returnMsg))
                {
                    res.Receipt = Newtonsoft.Json.JsonConvert.DeserializeObject<ReceiptData>(returnMsg);

                    if (res.Receipt.ReturnCode == "1" && res.Receipt.PayResult == "3")
                    {
                        var prefix = res.Receipt.FacTradeSeq.Substring(0, 1);

                        res.IsSandbox = prefix != "T";

                        if (res.IsSandbox == Sandbox)
                        {
                            res.IsSucc = true;
                            res.Serial = Int64.Parse(res.Receipt.FacTradeSeq.Substring(1));

                            var bonus = 0;
                            if (promoCodes.TryGetValue(res.Receipt.PromoCode, out bonus))
                            {
                                WriteLog("promode code = {0}, bonus = {1}", res.Receipt.PromoCode, bonus);
                                res.Bonus = bonus;
                            }
                            
                        }
                        else
                            WriteLog("illegal receipt, sandbox={0}, receipt.sandbox={1}", Sandbox, res.IsSandbox);

                    }
                }

            }
            else
                res.IsNetFail = true;

         
            return res;
        }

        public bool Confirm(string authCode)
        {
            string ParaValue = "AuthCode=" + authCode;

            string returnMsg = "";

            //Step3.4 Call PaymentConfirm
            var SendStatus = Send_MyCard_b2b(setting.ConfirmUrl, ParaValue, out returnMsg);
            if (SendStatus)
            {
                if (!string.IsNullOrEmpty(returnMsg))
                {
                    var result = Newtonsoft.Json.JsonConvert.DeserializeObject<ConfirmResultData>(returnMsg);

                    return result.ReturnCode == "1";
                }
            }

            return false;
        }

        public IAPRestoreList Restore(string data)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            
            var list = new IAPRestoreList();

            list.Rawdata = serializer.Deserialize<RestoreData>(data);
            
            if (list.Rawdata.ReturnCode == "1" && list.Rawdata.FacServiceId == setting.ServerID)
            {
                list.Transactions = new List<long>();

                foreach (var item in list.Rawdata.FacTradeSeq)
                {
                    var prefix = item.Substring(0, 1);
                    var sandbox = prefix != "T";
                    if (sandbox != Sandbox) continue;
                    list.Transactions.Add(Int64.Parse(item.Substring(1)));
                }

                list.IsValid = list.Transactions.Count > 0;
            }

            WriteLog("restore data = {0}, is_valid = {1}", data, list.IsValid);

            return list;
        }
        public void Query()
        {

        }

        /// <summary>
        /// 傳送參數至MyCard B2B網址
        /// </summary>
        /// <param name="b2b_url">網址</param>
        /// <param name="param">參數</param>
        /// <param name="returnMsg">回傳訊息</param>
        /// <returns></returns>
        public static bool Send_MyCard_b2b(string b2b_url, string param, out string returnMsg)
        {
            returnMsg = "";
            try
            {
                byte[] bs = Encoding.UTF8.GetBytes(param);

                Uri myUri = new Uri(b2b_url);
                HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(b2b_url);
                //request.Timeout = 5000;
                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded";
                //request.ContentType = "text/plain";
                request.ContentLength = bs.Length;
                //request.Timeout = System.Threading.Timeout.Infinite;
                request.KeepAlive = false;
                //request.ProtocolVersion = HttpVersion.Version10;
                request.ProtocolVersion = HttpVersion.Version11;
                HttpWebResponse response = null;

                using (Stream reqStream = request.GetRequestStream())
                {
                    reqStream.Write(bs, 0, bs.Length);
                }

                using (response = (HttpWebResponse)request.GetResponse())
                {
                    //在這裡對接收到的頁面內容進行處理
                    if (response != null)
                    {
                        Stream stream = response.GetResponseStream();
                        StreamReader sr = new StreamReader(stream);
                        string result_data = sr.ReadToEnd();

                        switch (response.StatusCode)
                        {
                            case HttpStatusCode.OK:
                                //200:正常
                                if (result_data == null)
                                {
                                    WriteLog("", "HttpStatusCode.OK but result_data = null|" + b2b_url + "?" + param);
                                }
                                else if (result_data == "")
                                {
                                    WriteLog("", "HttpStatusCode.OK but result_data = (empty)|" + b2b_url + "?" + param);
                                }
                                else
                                    WriteLog("", "HttpStatusCode.OK|result_data=" + result_data + "|" + b2b_url + "?" + param);
                                sr.Close();

                                returnMsg = result_data;
                                return true;
                            case HttpStatusCode.BadRequest:
                                //400:錯誤
                                WriteLog("", "BadRequest|" + b2b_url + "?" + param);
                                sr.Close();
                                return false;
                            default:
                                //其他狀態
                                WriteLog("", "Other|" + response.StatusCode.ToString() + "|" + b2b_url + "?" + param);
                                sr.Close();
                                return false;
                        }
                    }
                    else
                    {
                        WriteLog("", "response = null|" + b2b_url + "?" + param);
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                //網站回應錯誤(連不上等等其他原因)
                WriteLog("", "Exception|" + ex.Message + "|" + b2b_url + "?" + param);
                return false;
            }
        }

        private static void WriteLog(string Desc, params object[] args)
        {
            WriteLog("", string.Format(Desc, args));
        }

        private static void WriteLog(string KeyWord, string Desc)
        {
            KernelService.Logger.Debug("[MYCARD] " + Desc);
        }

        protected static string Sha256(string input)
        {
            System.Security.Cryptography.SHA256 sha = new System.Security.Cryptography.SHA256CryptoServiceProvider(); Byte[] inputBytes = Encoding.UTF8.GetBytes(input);
            Byte[] hashedBytes = sha.ComputeHash(inputBytes);
            return BitConverter.ToString(hashedBytes).Replace("-", "").ToLower();
        }
    }

    class Product
    {
        public string Name;
        public int Price;
    }

    class AuthResultData
    {
        public string ReturnCode;
        public string ReturnMsg;
        public string AuthCode;
        public string TradeSeq;
    }

    class ConfirmResultData
    {
        public string ReturnCode;
        public string ReturnMsg;
        public string FacTradeSeq;
        public string TradeSeq;
        public string SerialId;
    }

    class ClientReceiptData
    {
        public string returnCode;
        public string returnMsg;
        public string payResult;
        public string facTradeSeq;
        public string paymentType;
        public string amount;
        public string currency;
        public string myCardTradeNO;
        public string myCardType;
        public string promoCode;
        public string serialId;
    }

    public class ReceiptData
    {
        public string ReturnCode;
        public string ReturnMsg = "";
        public string PayResult;
        public string FacTradeSeq;
        public string PaymentType;
        public string Amount;
        public string Currency;
        public string MyCardTradeNo;
        public string MyCardType;
        public string PromoCode = "";
        public string SerialId = "";
    }
    class RestoreData
    {
        public string ReturnCode;
        public string ReturnMsg;
        public string FacServiceId;
        public int TotalNum;
        public string[] FacTradeSeq;
    }
}

