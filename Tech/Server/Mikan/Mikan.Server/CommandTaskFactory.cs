﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.IO;

namespace Mikan.Server
{
    public class CommandTaskFactory<T>
    {
        private Dictionary<Type, Type> cmdToTask;

        public CommandTaskFactory()
        {
            cmdToTask = new Dictionary<Type, Type>();
        }

        public void Register(Assembly asm)
        {
            foreach (var type in TypeUtils.Select(asm, Filter))
            {
                var attr = TypeUtils.GetAttribute<ActiveTaskAttribute>(type);

                if (attr == null) continue;
                KernelService.Logger.Trace("register command handler, cmd = {0}, handler = {1}", attr.CommandType, type);
                cmdToTask.Add(attr.CommandType, type);
            }
        }

        public ICommandTask<T> Create(IInputContext context)
        {
            if (context == null || context.Command == null) return null;
            Type taskType = null;

            if (!cmdToTask.TryGetValue(context.Command.GetType(), out taskType)) return null;
            var task = Activator.CreateInstance(taskType) as ICommandTask<T>;
            if (task != null) task.Setup(context);

            return task;
        }

        private bool Filter(Type type)
        {
            if (!TypeUtils.IsSubClassOf<ICommandTask<T>>(type)) return false;

            return true;
        }
    }

}
