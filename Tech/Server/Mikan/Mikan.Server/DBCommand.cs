﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mikan.Server
{
    public enum DBCommandType : byte
    {
        Common,
        CreateStoreProcedure,
    }

    public interface IDBCommand
    {
        DBCommandType Type { get; }

        string CommandText { get; }

        IEnumerable<KeyValuePair<string, object>> Parameters { get; }

        void AddParameter(string key, object value);
    }

    public class DBCommand : IDBCommand
    {
        private Dictionary<string, object> parameters;

        virtual public DBCommandType Type { get; set; }

        public string CommandText { get; set; }

        public IEnumerable<KeyValuePair<string, object>> Parameters
        {
            get { return SelectParameters(); }
        }

        public void AddParameter(string key, object value)
        {
            if (parameters == null) parameters = new Dictionary<string, object>();

            parameters.Add(key, value);
        }

        private IEnumerable<KeyValuePair<string, object>> SelectParameters()
        {
            if (parameters == null) yield break;

            foreach (var item in parameters) yield return item;
        }
    }
}
