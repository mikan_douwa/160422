﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.Threading;
using System.IO;
using System.Threading.Tasks;
namespace Mikan.Server
{
    public interface IHost
    {
        IConfig Config { get; }

        ILogger Logger { get; }
    }

    public interface IServerService : IDisposable
    {
        IHost Host { get; }
        void Execute(IHost host);
    }

    abstract public class ServerService<T> : IServerService
    {
        private Workers workers;

        private bool isDisposing = false;

        private KernelEventManager<T> eventManager;

        public CommandTaskFactory<T> TaskFactory { get; private set; }

        public IHost Host { get; private set; }

        virtual public int WorkerCount { get { return 128; } }

        virtual public void Execute(IHost host)
        {
            Host = host;

            TaskFactory = new CommandTaskFactory<T>();

            TaskFactory.Register(GetTasksAssembly());

            workers = new Workers(WorkerCount);

            eventManager = new KernelEventManager<T>();

            workers.OnTaskFail += new TaskFailCallback<ITask>(workers_OnTaskFail);

            eventManager.OnTaskFail += new TaskFailCallback<IKernelEvent<T>>(eventManager_OnTaskFail);
        }

        void eventManager_OnTaskFail(IKernelEvent<T> task, Exception e)
        {
            Host.Logger.Fatal("kernal task failed : {0},{1}", task, e);
        }

        void workers_OnTaskFail(ITask task, Exception e)
        {
            Host.Logger.Fatal("task failed : {0},{1}", task, e);
        }

        virtual public void Dispose()
        {
            if (isDisposing) return;

            isDisposing = true;

            if (eventManager != null)
            {
                eventManager.OnTaskFail -= eventManager_OnTaskFail;
                eventManager.Dispose();
                eventManager = null;
            }

            if (workers != null)
            {
                workers.OnTaskFail -= workers_OnTaskFail;
                workers.Dispose();
                workers = null;
            }
        }

        public void AddKernelEvent(IKernelEvent<T> evt)
        {
            if (isDisposing) return;

            eventManager.AddTask(evt);
        }

        public void AddTask(ITask task)
        {
            if (isDisposing) return;

            workers.AddTask(task);
        }

        public void AddCommandTask(ICommandTask<T> task)
        {
            AddKernelEvent(new PrepareTaskEvent<T>(this, task));
        }

        virtual protected Assembly GetTasksAssembly() { return Assembly.GetEntryAssembly(); }
    }

    public interface IKernelEvent<T> : IDisposable
    {
        void Execute(T context);
    }
    /*
    public interface ITask : IDisposable
    {
        void Execute();
    }
    */
    public interface ICommandTask<T> : IDisposable
    {
        void Setup(IInputContext input);

        void Prepare(T context);

        void Commit(T context);

        void Execute();
    }

    abstract public class CommandTask<T, TCommand> : ICommandTask<T>
        where TCommand : ICommand
    {
        private ManualResetEvent e = new ManualResetEvent(false);

        protected TCommand Command { get; private set; }

        protected IInputContext CommandContext { get; private set; }

        virtual public void Prepare(T context) { }

        virtual public void Commit(T context) { }

        public void Setup(IInputContext input)
        {
            CommandContext = input;
            Command = (TCommand)input.Command;
        }

        public void Execute()
        {
            BeginProcessCommand();

            e.WaitOne();
        }

        virtual public void Dispose() 
        { 
            e.Close();
            e.Dispose();
        }

        abstract protected void BeginProcessCommand();

        protected void EndProcessCommand(ICommand feedback)
        {
            Feedback(feedback);
            e.Set();
        }

        abstract protected void Feedback(ICommand cmd);
    }

    public class ActiveTaskAttribute : Attribute
    {
        public Type CommandType;
    }

    #region internal classes

    class PrepareTaskEvent<T> : IKernelEvent<T>
    {
        private ServerService<T> service;
        private ICommandTask<T> target;

        public PrepareTaskEvent(ServerService<T> service, ICommandTask<T> target)
        {
            this.service = service;
            this.target = target;
        }

        public void Execute(T context)
        {
            target.Prepare(context);

            service.AddTask(new ExecuteCommandTaskTask<T>(service, target));
        }

        public void Dispose()
        {
            service = null;
            target = null;
        }
    }

    class ExecuteCommandTaskTask<T> : ITask
    {
        private ServerService<T> service;
        private ICommandTask<T> target;

        public ExecuteCommandTaskTask(ServerService<T> service, ICommandTask<T> target)
        {
            this.service = service;
            this.target = target;
        }

        public void Execute()
        {
            target.Execute();

            service.AddKernelEvent(new CommitTaskEvent<T>(target));
        }

        public void Dispose()
        {
            service = null;
            target = null;
        }
    }

    class CommitTaskEvent<T> : IKernelEvent<T>
    {
        private ICommandTask<T> target;

        public CommitTaskEvent(ICommandTask<T> target)
        {
            this.target = target;
        }

        public void Execute(T context)
        {
            target.Commit(context);
            target.Dispose();
        }

        public void Dispose()
        {
            target = null;
        }
    }

    class KernelEventManager<T> : TaskThread<IKernelEvent<T>>
    {
        private T context;
        protected override void OnStart()
        {
            context = Activator.CreateInstance<T>();
        }
        protected override void ExecuteTask(IKernelEvent<T> task)
        {
            task.Execute(context);
        }
    }

    class Workers : TaskThread<ITask>
    {
        public Workers(int count)
            : base(count)
        {
        }

        protected override void ExecuteTask(ITask task)
        {
            task.Execute();
        }
    }

    #endregion
}
