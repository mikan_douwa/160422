﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mikan.Server;
using MySql.Data.MySqlClient;
using System.Data;
using System.Diagnostics;

namespace Mikan.Server
{
    public class MySqlDBConnection : IDBConnection
    {
        private MySqlConnection connection;

        public void Open(string connectionString)
        {
            if (connection == null)
            {
                connection = new MySqlConnection(connectionString);

                connection.Open();
            }
            else if (connection.State == ConnectionState.Broken || connection.State == ConnectionState.Closed)
            {
                connection.Open();
            }
        }

        public IDBTransaction BeginTransaction()
        {
            return new MySqlDBTransaction(this, connection.BeginTransaction());
        }
        public IDBTransaction BeginTransaction(IsolationLevel level)
        {
            return new MySqlDBTransaction(this, connection.BeginTransaction(level));
        }

        public IDBQueryResult SafeDoQuery(IDBCommand cmd)
        {
            try
            {
                return DoQuery(cmd);
            }
            catch (Exception e)
            {
                return new DBQueryFailed(e);
            }
        }

        public IDBQueryResult DoQuery(IDBCommand _cmd)
        {
            var retryCount = 0;

            while(true)
            {
                try
                {
                    var cmd = CreateCommand(_cmd);

                    return new MySQLDBQueryResult(cmd);
                }
                catch (Exception e)
                {
                    if (e.GetBaseException() is System.IO.EndOfStreamException && ++retryCount < 3) continue;
                    KernelService.Logger.Fatal("[DB_FAIL] statement = {0}, {1}", _cmd.CommandText, e);
                    throw;
                }
            }
        }

        public T DoQuery<T>(IDBCommand cmd)
        {
            using (var res = DoQuery(cmd))
            {
                if (res.IsEmpty) return default(T);
                return res.Field<T>();
            }
        }

        public void SimpleQuery(IDBCommand _cmd)
        {
            try
            {
                using (var cmd = CreateCommand(_cmd)) cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                if (_cmd.Type == DBCommandType.CreateStoreProcedure) return;

                KernelService.Logger.Fatal("[DB_FAIL] {0}, statement = {1}", e, _cmd.CommandText);
                throw;
            }
        }

        public bool SafeSimpleQuery(IDBCommand cmd)
        {
            try
            {
                SimpleQuery(cmd);
            }
            catch { return false; }

            return true;
        }

        public void Dispose()
        {
            if (connection != null) connection.Dispose();

            connection = null;
        }

        private MySqlCommand CreateCommand(IDBCommand _cmd)
        {
            //KernelService.Logger.Trace("[DO_QUERY] {0}", _cmd.CommandText);

            if (connection.State != ConnectionState.Open) connection.Open();

            var cmd = connection.CreateCommand();

            cmd.CommandText = _cmd.CommandText;

            foreach (var item in _cmd.Parameters)
            {
                cmd.Parameters.AddWithValue(item.Key, item.Value);
            }

            return cmd;
        }
    }

    class MySqlDBTransaction : IDBTransaction
    {
        private MySqlDBConnection owner;
        private IDbTransaction trans;

        private bool isComplete = false;

        public MySqlDBTransaction(MySqlDBConnection owner, IDbTransaction trans)
        {
            this.owner = owner;
            this.trans = trans;
        }
        public void Commit()
        {
            trans.Commit();
            isComplete = true;
        }

        public void Rollback()
        {
            trans.Rollback();
            isComplete = true;
        }

        public void Dispose()
        {
            if (!isComplete)
            {
                trans.Rollback();
                KernelService.Logger.Debug("db transaction auto rollback.\n{0}", new StackTrace().ToString());
            }

            trans.Dispose();
        }
    }

    class MySQLDBQueryResult : IDBQueryResult
    {
        private MySqlDataReader reader;
        private MySqlCommand cmd;
        internal MySQLDBQueryResult(MySqlCommand cmd)
        {
            this.cmd = cmd;
            reader = cmd.ExecuteReader();
            if (reader.HasRows) reader.Read();
        }

        public string Error { get { return null; } }

        public bool IsEmpty
        {
            get { return !reader.HasRows; }
        }

        public bool IsSucc { get { return true; } }

        public int RecordsAffected { get { return reader.RecordsAffected; } }


        public bool IsDbNull(string column)
        {
            return reader.IsDBNull(reader.GetOrdinal(column));
        }

        public object this[string column]
        {
            get { return reader[column]; }
        }

        public T Field<T>(string column)
        {
            return ConvertValue<T>(reader[column]);
        }

        public T Field<T>()
        {
            return ConvertValue<T>(reader[0]);
        }

        private T ConvertValue<T>(object obj)
        {
            T val;
            try
            {
                val = (T)Convert.ChangeType(obj, typeof(T));
            }
            catch
            {
                KernelService.Logger.Debug("convert failed, source type = {0}, target type = {1}", obj.GetType(), typeof(T));
                throw;
            }

            return val;
        }

        public bool NextRow()
        {
            return reader.Read();
        }

        public void Dispose()
        {
            reader.Dispose();
            cmd.Dispose();
        }
    }

    class DBQueryFailed : IDBQueryResult
    {
        private Exception e;
        public DBQueryFailed(Exception e)
        {
            this.e = e;
        }
        public string Error { get { return e.ToString(); } }

        public bool IsEmpty { get { return true; } }

        public bool IsSucc { get { return false; } }

        public int RecordsAffected { get { return 0; } }

        public bool IsDbNull(string column) { return true; }

        public T Field<T>(string column)
        {
            throw new NotImplementedException();
        }

        public T Field<T>()
        {
            throw new NotImplementedException();
        }

        public object this[string column]
        {
            get { throw new NotImplementedException(); }
        }

        public bool NextRow()
        {
            throw new NotImplementedException();
        }

        public void Dispose() { }

    }
}
