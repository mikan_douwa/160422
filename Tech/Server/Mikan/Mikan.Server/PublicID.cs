﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Mikan
{
    public interface IPublicIDConverter
    {
        bool IsValid(PublicID id);
        PublicID FromString(string id);
        string ToString(PublicID id);
    }

    public class DefaultPublicIConverter : IPublicIDConverter
    {
        private const int DIGIT1 = 444;
        private const int DIGIT2 = 291;
        private const int DIGIT3 = 886;

        private List<int> groups;

        internal static IPublicIDConverter GetDefault()
        {
            var converter = new DefaultPublicIConverter();
            converter.SetGroups(new int[] { 0, 1, 2, 3, 4 });
            return converter;
        }

        public void SetGroups(IEnumerable<int> groups)
        {
            groups = groups.ToList();
        }

        public bool IsValid(PublicID id)
        {
            return (groups == null || groups.Contains(id.Group)) && id.Serial > 0;
        }

        public PublicID FromString(string publicId)
        {
            if (string.IsNullOrEmpty(publicId) || publicId.Length < 9) return null;

            var split1 = ((int.Parse(publicId.Substring(0, 3)) - DIGIT1 + 999) % 999).ToString("000");
            var split2 = ((int.Parse(publicId.Substring(3, 3)) - DIGIT2 + 999) % 999).ToString("000");
            var split3 = ((int.Parse(publicId.Substring(6, 3)) - DIGIT3 + 999) % 999).ToString("000");
            var split4 = publicId.Substring(9);

            var origin = string.Format("{0}{1}{2}{3}", split1, split2, split3, split4);

            var s_group = string.Format("{0}{1}", split2[2], split3[2]);
            var s_id = string.Format("{0}{1}{2}{3}{4}{5}{6}{7}", split1[0], split2[0], split3[0], split1[1], split2[1], split3[1], split1[2], split4);

            var c_id = s_id.Reverse().ToArray();

            s_id = string.Join("", c_id);

            return PublicID.Create(int.Parse(s_group), Int64.Parse(s_id));
        }

        public string ToString(PublicID id)
        {
            var s_id = id.Serial.ToString("0000000");
            var c_id = s_id.Reverse().ToArray();
            var s_group = id.Group.ToString("00");

            var split1 = string.Format("{0}{1}{2}", c_id[0], c_id[3], c_id[6]);
            var split2 = string.Format("{0}{1}{2}", c_id[1], c_id[4], s_group[0]);
            var split3 = string.Format("{0}{1}{2}", c_id[2], c_id[5], s_group[1]);
            var split4 = s_id.Substring(7);

            return string.Format("{0}{1}{2}{3}", ((int.Parse(split1) + DIGIT1) % 999).ToString("000"), ((int.Parse(split2) + DIGIT2) % 999).ToString("000"), ((int.Parse(split3) + DIGIT3) % 999).ToString("000"), split4);
        }
    }

    public class PublicID : IEquatable<PublicID>
    {
        private static LCGCryptor cryptor = new LCGCryptor(13901, 359, 37072);

        private static IPublicIDConverter converter = DefaultPublicIConverter.GetDefault();

        public int Group { get; private set; }
        public Int64 Serial { get; private set; }

        public static void Setup(IPublicIDConverter converter)
        {
            PublicID.converter = converter;
        }

        private PublicID() { }

        public bool IsValid { get { return converter.IsValid(this); } }

        public override string ToString()
        {
            return converter.ToString(this);
        }

        public static PublicID Create(int group, Int64 serial)
        {
            return new PublicID { Group = group, Serial = serial };
        }
        
        public static PublicID FromString(string publicId)
        {
            return converter.FromString(publicId);
        }

        public Token CreateToken(DateTime expireTime)
        {
            var rand = new Random();
            
            var seed = (byte)rand.Next(256);
            var stuffSize = CalcStuffSize(seed);
            var stuff = new byte[stuffSize];
            rand.NextBytes(stuff);

            var stream = new MemoryStream(21 + stuffSize);

            using (var writer = new BinaryWriter(stream))
            {
                writer.Write(seed);
                writer.Write(stuff);
                writer.Write(expireTime.Ticks);
                writer.Write(Serial);
                writer.Write(Group);
            }

            var token = stream.ToArray();

            Encode(token);

            return new Token(this, Convert.ToBase64String(token), expireTime);
        }

        public static Token ParseToken(string token)
        {
            var bytes = Convert.FromBase64String(token);

            Encode(bytes);

            var stream = new MemoryStream(bytes);

            using (var reader = new BinaryReader(stream))
            {
                var seed = reader.ReadByte();
                var stuff = reader.ReadBytes(CalcStuffSize(seed));
                var expireTime = new DateTime(reader.ReadInt64());
                var serial = reader.ReadInt64();
                var group = reader.ReadInt32();

                var id = Create(group, serial);

                if (id == null || !id.IsValid) return null;

                return new Token(id, token, expireTime);
            }
        }

        private static int CalcStuffSize(byte seed)
        {
            return (seed ^ 0x7D) % 7;
        }

        private static void Encode(byte[] rawdata)
        {
            cryptor.Encode(rawdata, 0xE1CC906);
        }

        public static Dictionary<PublicID, T> CreateDictinary<T>()
        {
            return new Dictionary<PublicID, T>(comparer);
        }

        public static HashSet<PublicID> CreateHashSet()
        {
            return new HashSet<PublicID>(comparer);
        }

        private class Comparer : IEqualityComparer<PublicID>
        {
            public bool Equals(PublicID x, PublicID y)
            {
                return x.Group == y.Group && x.Serial == y.Serial;
            }

            public int GetHashCode(PublicID obj)
            {
                return obj.Group.GetHashCode() ^ obj.Serial.GetHashCode();
            }
        }

        private static Comparer comparer = new Comparer();

        public bool Equals(PublicID other)
        {
            return Group == other.Group && Serial == other.Serial;
        }
    }

   

    public class Token
    {
        public PublicID ID { get; private set; }
        public DateTime ExpireTime { get; private set; }
        public string Text { get; private set; }
        public Token(PublicID id, string text, DateTime expireTime)
        {
            ID = id;
            Text = text;
            ExpireTime = expireTime;
        }
    }
}
