﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Mikan.Server
{
    public enum NodeJsType
    {
        None = 0,
        Module = 1,
        Function = 2,
        DynamicObj = 3,
    }

    public interface IEdgeJsService : IServerService
    {
        IAsyncResult ProcessRequest(EdgeJsHttpRequest req);
        void Add(string name, System.Threading.Tasks.Task<object> func);
        void Add(string name, IDictionary<string, object> obj);
        IInputContext Resolve(byte[] input);
        IOutputContext Resolve(IInputContext input, ICommand output);
    }

    public class EdgeJsHttpRequest
    {
        public string Client;
        public string Url;
        public IDictionary<string, object> Query;
        public byte[] Data;
    }

    public class NodeJsModule
    {
        private static Dictionary<string, NodeJsModule> modules = new Dictionary<string, NodeJsModule>();

        private Dictionary<string, Func<object, Task<object>>> funcs;

        public static NodeJsModule Create(string name, dynamic input)
        {
            var module = new NodeJsModule();

            module.funcs = new Dictionary<string, Func<object, Task<object>>>();
            foreach (var item in (IDictionary<string, object>)input)
            {
                if (item.Value is Func<object, Task<object>>)
                    module.funcs.Add(item.Key, (Func<object, Task<object>>)item.Value);
            }

            modules[name] = module;

            return module;
        }

        public static NodeJsModule Get(string name)
        {
            return modules.SafeGetValue(name);
        }

        public void Execute(string name, object arg0)
        {
            /*var ev = new ManualResetEvent(false);
            Console.WriteLine(name);
            Task.Run(async () =>
            {
                var res = await ExecuteAsync(name, arg0);
                Console.WriteLine("1");
                ev.Set();
                return res;
            });

            ev.WaitOne();
            */
            
            var func = funcs.SafeGetValue(name);
            if (func != null)
            {
                var task = func(arg0);
                if (task.Status == TaskStatus.WaitingForActivation)
                    task.RunSynchronously();
                else if(task.Status != TaskStatus.RanToCompletion)
                    task.Wait();
            }
            else
                Console.WriteLine("Execute failed, func not exists, name={0}, arg={1}", name, arg0);
            
        }

        public async Task<object> ExecuteAsync(string name, object arg0)
        {
            var func = funcs.SafeGetValue(name);
            if (func != null) return await func(arg0);
            Console.WriteLine("ExecuteAsync failed, func not exists, name={0}, arg={1}", name, arg0);
            return null;
        }
    }

    
}
