﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Mikan.Server
{
    public class DBPool
    {
        private static DBPool instance;
        public static DBPool Instance { get { return instance; } }
        private Dictionary<int, DBItem> dbs = new Dictionary<int, DBItem>();

        private IHost host;

        private int connLimit;

        public static DBPool Create(IHost host)
        {
            instance = new DBPool(host);
            return instance;
        }

        private DBPool(IHost host)
        {
            this.host = host;

            connLimit = int.Parse(host.Config.TryGet("conn_limit", "16"));

            Add(-1, host.Config["unique_db"]);
            Add(-2, host.Config["log_db"]);
        }

        public void Add(int id, string connstr)
        {
            foreach (var db in dbs)
            {
                if (db.Value.ConnectionString != connstr) continue;

                dbs[id] = db.Value;

                return;
            }

            dbs[id] = DBItem.Create(connstr, connLimit);
        }

        public IDBConnection Acquire(int id)
        {
            DBItem db = null;

            if (dbs.TryGetValue(id, out db)) return db.Acquire();

            return null;
        }

        public IDBConnection AcquireUnique()
        {
            return Acquire(-1);
        }

        public IDBConnection AcquireLog()
        {
            return Acquire(-2);
        }

        class DBItem
        {
            public string ConnectionString { get; private set; }

            private Stack<IDBConnection> connections;

            public Semaphore semaphore;

            public IDBConnection Acquire()
            {
                if (!semaphore.WaitOne(10)) return null;

                IDBConnection connection = null;

                lock (connections)
                {
                    connection = new RecyclableDBConnection(connections.Pop(), semaphore, connections);
                }

                connection.Open(ConnectionString);

                return connection;
            }

            public static DBItem Create(string connStr, int connLimit)
            {
                var item = new DBItem();

                item.ConnectionString = connStr;

                item.semaphore = new Semaphore(connLimit, connLimit);


                item.connections = new Stack<IDBConnection>(connLimit);

                for (int i = 0; i < connLimit; ++i)
                {
                    var connection = new MySqlDBConnection();

                    item.connections.Push(connection);
                }

                return item;
            }
        }

        class RecyclableDBConnection : IDBConnection
        {
            private IDBConnection target;

            private Semaphore semaphore;

            private Stack<IDBConnection> stack;

            public RecyclableDBConnection(IDBConnection target, Semaphore semaphore, Stack<IDBConnection> stack)
            {
                this.target = target;
                this.semaphore = semaphore;
                this.stack = stack;
            }

            public void Open(string connstr)
            {
                target.Open(connstr);
            }

            public IDBTransaction BeginTransaction()
            {
                return target.BeginTransaction();
            }

            public IDBTransaction BeginTransaction(System.Data.IsolationLevel level)
            {
                return target.BeginTransaction(level);
            }

            public IDBQueryResult DoQuery(IDBCommand query)
            {
                return target.DoQuery(query);
            }

            public T DoQuery<T>(IDBCommand cmd)
            {
                return target.DoQuery<T>(cmd);
            }

            public IDBQueryResult SafeDoQuery(IDBCommand query)
            {
                return target.SafeDoQuery(query);
            }

            public void SimpleQuery(IDBCommand query)
            {
                target.SimpleQuery(query);
            }

            public bool SafeSimpleQuery(IDBCommand query)
            {
                return target.SafeSimpleQuery(query);
            }

            public void Dispose()
            {
                if (target == null) return;

                lock (stack) stack.Push(target);

                semaphore.Release();

                target = null;
            }
        }
    }

    abstract public class DBTask : ITask
    {
        public static T Create<T>()
            where T : DBTask
        {
            var inst = Activator.CreateInstance<T>();

            return inst;
        }
        public void Execute()
        {
            try
            {
                SafeDoQuery();
            }
            catch (Exception e)
            {
                KernelService.Logger.Fatal("db fail, error = {0}", e);
            }
        }

        public void Dispose()
        {

        }

        abstract protected void SafeDoQuery();

        protected IDBConnection AcquireUniqueDB()
        {
            return AcquireDB(-1);
        }

        protected IDBConnection AcquireDB(int group)
        {
            return DBPool.Instance.Acquire(group);
        }

        protected IDBConnection AcquireDB(PublicID id)
        {
            return AcquireDB(id.Group);
        }

    }
}
