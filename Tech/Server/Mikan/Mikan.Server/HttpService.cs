﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Routing;
using System.Threading;
using System.IO;
using System.Collections.Specialized;

namespace Mikan.Server
{
    public class ActiveServiceAttribute : Attribute { }

    public interface IHttpService : IServerService
    {
        ISerializer<ICommand> Serializer { get; }

        IEnumerable<string> GetURIList();

        IInputContext Resolve(Stream input);

        IOutputContext Resolve(IInputContext input, ICommand output);
    }

    abstract public class HttpService<T> : ServerService<T>, IHttpService
    {
        private ICommandResolver resolver;

        private ISerializer<ICommand> serializer;

        public abstract IEnumerable<string> GetURIList();

        public ISerializer<ICommand> Serializer { get { return serializer; } }

        public override void Execute(IHost host)
        {
            foreach (var uri in GetURIList())
                RouteTable.Routes.Add(new Route(uri, new HttpHandler<T>(this)));

            var handler = GetUnmanagedHttpHandler();

            if (handler != null)
            {
                foreach (var uri in GetUnmanagedURIList()) RouteTable.Routes.Add(new Route(uri, handler));
            }
            

            serializer = CreateSerializer();
            resolver = CreateResolver();

            base.Execute(host);
        }

        virtual protected IEnumerable<string> GetUnmanagedURIList()
        {
            yield break;
        }

        virtual protected UnmanagedHttpHandler GetUnmanagedHttpHandler()
        {
            return null;
        }

        abstract public ICommandTask<T> ProcessInformalCommand(ICommand cmd);

        abstract public IHttpTask ProcessInformalCommand();

        public void RegisterKey(PublicID id, int key)
        {
            resolver.RegisterKey(id, key);
        }

        public IInputContext Resolve(Stream input)
        {
            return resolver.Resolve(input);
        }

        public IOutputContext Resolve(IInputContext input, ICommand output)
        {
            return resolver.Resolve(input, output);
        }

        abstract protected ISerializer<ICommand> CreateSerializer();

        virtual protected ICommandResolver CreateResolver()
        {
            return new CommandResolver(serializer);
        }

    }

    public interface IHttpTask : IAsyncResult
    {
        void Setup(IHttpService service, HttpContext context, AsyncCallback cb, object extraData);
    }

    abstract public class UnmanagedHttpTask : IHttpTask
    {
        private IHttpService service;

        private ManualResetEvent e;

        public object AsyncState
        {
            get { return extraData; }
        }

        public WaitHandle AsyncWaitHandle
        {
            get { return e; }
        }

        public bool CompletedSynchronously
        {
            get { return false; }
        }

        public bool IsCompleted
        {
            get { return e.WaitOne(0); }
        }

        protected IHttpService Service { get { return service; } }

        private HttpContext context;

        private AsyncCallback cb;

        private object extraData;

        public void Setup(IHttpService service, HttpContext context, AsyncCallback cb, object extraData)
        {
            e = new ManualResetEvent(false);
            this.service = service;
            this.context = context;
            this.cb = cb;
            this.extraData = extraData;

            ProcessRequest(context.Request.Url, context.Request.QueryString);
        }

        protected string this[string key] { get { return context.Request[key]; } }

        protected string UserHostAddress { get { return context.Request.UserHostAddress; } }

        abstract protected void ProcessRequest(Uri url, NameValueCollection queryString);

        protected Stream RequestStream 
        { 
            get {
                if (context.Request.HttpMethod != "GET" && context.Request.HttpMethod != "DELETE")
                    return context.Request.GetBufferedInputStream();
                else
                    return null;
            } 
        }

        protected void Feedback(IOutputContext output)
        {
            foreach (var buffer in output.Buffers.Select()) context.Response.BinaryWrite(buffer);

            EndRequest();
        }

        protected void Feedback(int status)
        {
            context.Response.StatusCode = status;

            EndRequest();
        }

        private void EndRequest()
        {
            context.Response.End();

            e.Set();

            if (cb != null) cb(this);
        }
    }

    abstract public class HttpTask<T, TCommand> : CommandTask<T, TCommand>, IHttpTask
        where TCommand : ICommand
    {
        private IHttpService service;

        private ManualResetEvent e;

        public object AsyncState
        {
            get { return extraData; }
        }

        public WaitHandle AsyncWaitHandle
        {
            get { return e; }
        }

        public bool CompletedSynchronously
        {
            get { return false; }
        }

        public bool IsCompleted
        {
            get { return e.WaitOne(0); }
        }

        protected IHttpService Service { get { return service; } }

        private HttpContext context;

        private AsyncCallback cb;

        private object extraData;

        public string UserHostAddress { get { return context.Request.UserHostAddress; } }

        public void Setup(IHttpService service, HttpContext context, AsyncCallback cb, object extraData)
        {
            e = new ManualResetEvent(false);
            this.service = service;
            this.context = context;
            this.cb = cb;
            this.extraData = extraData;
        }

        protected override void Feedback(ICommand cmd)
        {
            var output = service.Resolve(CommandContext, cmd);

            foreach (var buffer in output.Buffers.Select()) context.Response.BinaryWrite(buffer);
            
            context.Response.End();
            
            e.Set();

            if (cb != null) cb(this);
        }

    }

    abstract public class UnmanagedHttpHandler : IHttpAsyncHandler, IRouteHandler
    {
        private IHttpService service;

        public UnmanagedHttpHandler(IHttpService service)
        {
            this.service = service;
        }

        public IAsyncResult BeginProcessRequest(HttpContext context, AsyncCallback cb, object extraData)
        {
            var externalTask = CreateTask();
            externalTask.Setup(service, context, cb, extraData);
            return externalTask;

        }

        #region 保留程式碼區塊

        public void EndProcessRequest(IAsyncResult result)
        {
            //throw new NotImplementedException();
        }

        public bool IsReusable
        {
            get { throw new NotImplementedException(); }
        }

        public void ProcessRequest(HttpContext context)
        {
            throw new NotImplementedException();
        }

        #endregion

        public IHttpHandler GetHttpHandler(RequestContext requestContext)
        {
            return this;
        }

        abstract protected UnmanagedHttpTask CreateTask();
    }

    class HttpHandler<T> : IHttpAsyncHandler, IRouteHandler
    {
        private HttpService<T> service;

        public HttpHandler(HttpService<T> service)
        {
            this.service = service;
        }

        public IAsyncResult BeginProcessRequest(HttpContext context, AsyncCallback cb, object extraData)
        {
            IInputContext input = null;

            if (context.Request.HttpMethod != "GET" && context.Request.HttpMethod != "DELETE")
            {
                input = service.Resolve(context.Request.GetBufferlessInputStream());
            }
            
            var task = service.TaskFactory.Create(input);

            if (task == null)
            {
                if (input != null && input.Command != null)
                    task = service.ProcessInformalCommand(input.Command);
                else
                {
                    var externalTask = service.ProcessInformalCommand();
                    externalTask.Setup(service, context, cb, extraData);
                    return externalTask;
                }
            }
            var _task = task as IHttpTask;

            if (_task == null) throw new InvalidOperationException("task must implement IHttpTask interface.");

            _task.Setup(service, context, cb, extraData);
            service.AddCommandTask(task);

            return _task;

        }

        #region 保留程式碼區塊

        public void EndProcessRequest(IAsyncResult result)
        {
            //throw new NotImplementedException();
        }

        public bool IsReusable
        {
            get { throw new NotImplementedException(); }
        }

        public void ProcessRequest(HttpContext context)
        {
            throw new NotImplementedException();
        }

        #endregion

        public IHttpHandler GetHttpHandler(RequestContext requestContext)
        {
            return this;
        }
    }
}
