﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Mikan.Server
{
    public interface IInputContext
    {
        Token Token { get; }
        ICommand Command { get; }
    }

    public interface IOutputContext
    {
        Buffers Buffers { get; }
    }

    public interface ICommandResolver
    {
        IInputContext Resolve(Stream input);

        IInputContext Resolve(byte[] input);

        IOutputContext Resolve(Token token, ICommand output);

        IOutputContext Resolve(IInputContext input, ICommand output);

        void RegisterKey(PublicID id, int key);
    }

    public class InputContext : IInputContext
    {
        public Token Token { get; private set; }
        public ICommand Command { get; private set; }

        public InputContext(Token token, ICommand cmd)
        {
            Token = token;
            Command = cmd;
        }
    }

    public class OutputContext : IOutputContext
    {
        public Buffers Buffers { get; set; }
    }

    public class CommandResolver : ICommandResolver
    {
        private object lck = new object();

        private Dictionary<PublicID, int> keys;

        private LCGCryptor cryptor = new LCGCryptor(157, 77873, 110592);

        public ISerializer<ICommand> Serializer { get; private set; }

        public bool IsDebug { get; set; }

        public CommandResolver(ISerializer<ICommand> serializer)
        {
            Serializer = serializer;
            keys = PublicID.CreateDictinary<int>();
        }

        public void RegisterKey(PublicID id, int key)
        {
            lock (lck) keys[id] = key;
        }

        public int GetKey(PublicID id)
        {
            lock (lck)
            {
                var key = 0;
                if (keys.TryGetValue(id, out key)) return key;
                return 0; ;
            }
        }

        public IInputContext Resolve(byte[] input)
        {
            using (var stream = new MemoryStream(input)) return Resolve(stream);
        }

        public IInputContext Resolve(Stream input)
        {
            try
            {
                var beginTime = DateTime.Now;

                using (var reader = new BinaryReader(input))
                {
                    var trait = reader.ReadByte();
                    switch (trait)
                    {
                        case 0x18:
                            {
                                var token = reader.ReadString();

                                var buffer = Read(reader);

                                if (buffer == null) return null;

                                return Decrypt(token, buffer);
                            }
                        case 0x9C:
                            {
                                var buffer = Read(reader);

                                if (buffer == null) return null;

                                return Decrypt(buffer);
                            }
                        default:
                            return null;

                    }
                }
            }
            catch (Exception e)
            {
                KernelService.Logger.Fatal("[RESOLVE_FAILED] " + e.ToString());
                return null;
            }
        }

        private byte[] Read(BinaryReader reader)
        {
            var beginTime = DateTime.Now;

            var size = reader.ReadInt32();
            var buffer = new byte[size];

            var bytesRead = 0;

            var chunkSize = Math.Min(1024, size);

            while (true)
            {
                var length = reader.Read(buffer, bytesRead, chunkSize);

                bytesRead += length;

                if (bytesRead >= size) break;

                if (DateTime.Now.Subtract(beginTime).TotalSeconds > 10)
                {
                    KernelService.Logger.Fatal("[RESOLVE_TIMEOUT]");
                    return null;
                }

                chunkSize = Math.Min(1024, size - bytesRead);
            }

            return buffer;
        }

        public IOutputContext Resolve(Token token, ICommand output)
        {

            if (IsDebug || !output.IsRegular || !output.IsSucc) KernelService.Logger.Trace("[OUTGOING] Type = {0}, Status = {1}", output.GetType().ToString(), output.ErrorMessage);

            var buffers = new Buffers();

            var bytes = Serializer.Serialize(output);

            if (token != null)
            {
                buffers.Add((byte)0x18);
                cryptor.Encode(bytes, GetKey(token.ID));
            }
            else
            {
                buffers.Add((byte)0x9C);
                cryptor.Encode(bytes, LCGCryptor.DEFAULT_KEY);
            }

            buffers.Add(bytes.Length);
            buffers.Add(bytes);

            return new OutputContext { Buffers = buffers };
        }

        public IOutputContext Resolve(IInputContext input, ICommand output)
        {
            var _input = input as InputContext;

            var token = _input != null ? _input.Token : null;

            return Resolve(token, output);
        }

        private InputContext Decrypt(string tokenText, byte[] buffer)
        {
            var token = PublicID.ParseToken(tokenText);

            if (token == null) return null;

            var key = GetKey(token.ID);

            if (key == 0) return new InputContext(token, new NotLoginYetCommand());

            cryptor.Encode(buffer, key);

            var cmd = Serializer.DeSerialize(buffer);

            if (IsDebug || !cmd.IsRegular) KernelService.Logger.Trace("[INCOMING] Type = {0}, ID = {1}", cmd.GetType().ToString(), token.ID);

            return new InputContext(token, cmd);
        }

        private InputContext Decrypt(byte[] buffer)
        {
            cryptor.Encode(buffer, LCGCryptor.DEFAULT_KEY);

            var cmd = Serializer.DeSerialize(buffer);

            if(!cmd.IsRegular) KernelService.Logger.Trace("[INCOMING] Type = {0}", cmd.GetType().ToString());

            return new InputContext(null, cmd);
        }
    }

    public class NotLoginYetCommand : ICommand
    {

        public bool IsSucc
        {
            get { return false; }
        }

        public string ErrorMessage
        {
            get { return "NotLoginYet"; }
        }

        public bool IsBackground
        {
            get { return false; }
        }

        public bool IsActivePush
        {
            get { return false; }
        }

        public bool AutoRetry
        {
            get { return false; }
        }

        public void AddSyncData(ISyncData data)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<ISyncData> SelectSyncData()
        {
            throw new NotImplementedException();
        }


        public int TransferID
        {
            get { return 0; }
        }

        public void InjectTransferID(int id)
        {
            
        }


        public bool IsRegular
        {
            get { return false; }
        }


        public string URI
        {
            get { throw new NotImplementedException(); }
        }
    }
}
