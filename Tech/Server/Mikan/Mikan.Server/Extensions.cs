﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mikan
{
    public static class DictionaryExtends
    {
        public static TValue SafeGetValue<TKey, TValue>(this Dictionary<TKey, TValue> inst, TKey key)
        {
            TValue val;
            if (inst.TryGetValue(key, out val)) return val;

            return default(TValue);
        }

        
    }

    public static class DatetimeExtends
    {
        public static int ToUnixTime(this DateTime inst)
        {
            return DateTimeUtils.ToUnixTime(inst);
        }

        
    }
}
