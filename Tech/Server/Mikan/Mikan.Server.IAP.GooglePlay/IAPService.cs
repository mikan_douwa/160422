﻿using Google.Apis.AndroidPublisher.v2;
using Google.Apis.Auth.OAuth2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Mikan.Server.IAP.GooglePlay
{
    public class IAPCheckResult
    {
        internal Google.Apis.AndroidPublisher.v2.Data.ProductPurchase Purchase;

        public bool IsSucc { get { return Purchase != null; } }

        public bool Timeout;

        public int? PurchaseState = -1;

        public int? ConsumptionState = -1;
    }

    public class IAPContext
    {
        public ServiceAccountCredential Credential;

        public AndroidPublisherService Service;

        public string Email { get; private set; }

        public string PackageName { get; private set; }

        public X509Certificate2 Certificate { get; private set; }

        public void Initialize(string package, string email, X509Certificate2 certificate)
        {
            PackageName = package;
            Email = email;
            Certificate = certificate;

            Credential = new ServiceAccountCredential(
                new ServiceAccountCredential.Initializer(Email)
                {
                    Scopes = new[] { AndroidPublisherService.Scope.Androidpublisher }
                }.FromCertificate(Certificate)
            );

            Service = new AndroidPublisherService(new Google.Apis.Services.BaseClientService.Initializer()
            {
                HttpClientInitializer = Credential,
                ApplicationName = "Mikan.IAPServer"
            });
        }

        public bool RefreshToken()
        {
            try
            {
                if (Credential.Token == null || Credential.Token.IsExpired(Credential.Clock))
                {
                    using (var task = Credential.RequestAccessTokenAsync(CancellationToken.None)) task.Wait();
                }
            }
            catch (Exception ex)
            {
                KernelService.Logger.Fatal("iap refresh token failed, msg={0}", ex.ToString());
                return false;
            }

            return true;
        }

        public void Dispose() { }
    }

    public class IAPService : TaskThread<IIAPTask>
    {
        private IAPContext context = new IAPContext();

        public IAPService()
            : base(8) { }

        protected override void OnStart()
        {
            System.Reflection.Assembly.Load("System.Net.Http");
            System.Reflection.Assembly.Load("System.Net.Http.Extensions");
            System.Reflection.Assembly.Load("System.Net.Http.WebRequest");
            System.Reflection.Assembly.Load("System.Net.Http.Primitives");

            context = new IAPContext();
        }

        protected override void OnEnd()
        {
            context.Dispose();
        }

        public void Initialize(string packageName, string email, X509Certificate2 certificate)
        {
            AddTask(new InitTask { PackageName = packageName, Email = email, Certificate = certificate });
        }

        public IAPCheckResult Check(string productId, string orderId, string token)
        {

            var res = new IAPCheckResult();

            var task = new CheckTask { ProductID = productId, OrderID = orderId, Token = token };

            AddTask(task);

            if (task.WaitFor())
            {
                res.Purchase = task.Result;

                if (res.Purchase != null)
                {
                    res.PurchaseState = res.Purchase.PurchaseState;
                    res.ConsumptionState = res.Purchase.ConsumptionState;
                }

            }
            else
                res.Timeout = true;

            return res;

        }

        protected override void ExecuteTask(IIAPTask task)
        {
            task.Execute(context);
        }

        class InitTask : IIAPTask
        {
            public string PackageName;
            public string Email;
            public X509Certificate2 Certificate;
            public void Execute(IAPContext context)
            {
                context.Initialize(PackageName, Email, Certificate);
            }

            public void Dispose() { }
        }



        class CheckTask : IIAPTask
        {
            public string OrderID;

            public string ProductID;

            public string Token;

            public Google.Apis.AndroidPublisher.v2.Data.ProductPurchase Result { get; private set; }

            private ManualResetEvent eFinish;

            private bool cancelled;

            public CheckTask()
            {
                eFinish = new ManualResetEvent(false);
            }

            public void Execute(IAPContext context)
            {
                if (!cancelled)
                {
                    try
                    {
                        if (RefreshToken(context))
                        {
                            var req = context.Service.Purchases.Products.Get(context.PackageName, ProductID, Token);
                            Result = req.Execute();
                        }
                    }
                    catch (Exception ex)
                    {
                        Result = null;
                        KernelService.Logger.Fatal("IAP failed, product_id = {0}, order_id = {1}, token = {2}, error = {3}", ProductID, OrderID, Token, ex.ToString());
                    }
                }

                if (!cancelled) eFinish.Set();
            }

            public bool WaitFor()
            {
                var res = eFinish.WaitOne(1000 * 15);
                if (!res) cancelled = true;
                return res;
            }

            public void Dispose()
            {
                eFinish.Close();
            }

            private bool RefreshToken(IAPContext context)
            {
                lock (context) return context.RefreshToken();
            }
        }
    }

    public interface IIAPTask : IDisposable
    {
        void Execute(IAPContext context);
    }
}
