﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using Mikan.Server;

namespace Mikan.CSAGA.Server
{
    public static class DBObjectConverter
    {
        private static Dictionary<Type, TypeInfo> types;

        class TypeInfo
        {
            public List<IFieldAdaptor> TypeFields = new List<IFieldAdaptor>();

            public List<string> TableFields = new List<string>();
        }

        interface IFieldAdaptor
        {
            void SetValue(object obj, object value);
        }
        class FieldAdaptor : IFieldAdaptor
        {
            public FieldInfo Field;
            public void SetValue(object obj, object value)
            {
                Field.SetValue(obj, value);
            }
        }

        class BooleanFieldAdaptor : IFieldAdaptor
        {
            public FieldInfo Field;
            public void SetValue(object obj, object value)
            {
                var flag = (UInt64)value;
                Field.SetValue(obj, (bool)(flag != 0));
            }
        }

        static DBObjectConverter()
        {
            types = new Dictionary<Type, TypeInfo>();
            var asm = Assembly.GetExecutingAssembly();
            if (asm == null) asm = Assembly.Load("Mikan.CSAGA.Server");
            foreach (var item in TypeUtils.Select(asm, (obj) => { return TypeUtils.HasAttribute<DBObjectAttribute>(obj); }))
            {
                var info = new TypeInfo();

                var fields = item.GetFields();

                foreach (var field in fields)
                {
                    var attr = TypeUtils.GetAttribute<DBFieldAttribute>(field);

                    if (attr == null) continue;

                    if (field.FieldType == typeof(Boolean))
                        info.TypeFields.Add(new BooleanFieldAdaptor { Field = field });
                    else
                        info.TypeFields.Add(new FieldAdaptor { Field = field });

                    info.TableFields.Add(attr.ColumnName);
                }

                types[item] = info;
            }
        }

        public static IEnumerable<T> Convert<T>(IDBQueryResult provider)
            where T : IDBObject
        {
            if (!provider.IsEmpty)
            {
                TypeInfo info = null;

                if (types.TryGetValue(typeof(T), out info))
                {
                    while (true)
                    {
                        var obj = Activator.CreateInstance<T>();

                        for (int i = 0; i < info.TypeFields.Count; ++i)
                        {
                            var column = info.TableFields[i];
                            try
                            {
                                info.TypeFields[i].SetValue(obj, provider[column]);
                            }
                            catch
                            {
                                if (!provider.IsDbNull(column))
                                {
                                    KernelService.Logger.Fatal("db conversion failed, column = {1}, field = {0}, value = {2}, value_type = {3}", info.TypeFields[i], info.TableFields[i], provider[info.TableFields[i]], provider[info.TableFields[i]].GetType());
                                    throw;
                                }
                            }
                        }

                        yield return obj;

                        if (!provider.NextRow()) break;
                    }
                }
                else
                    throw new InvalidOperationException(string.Format("db object not register, type = {0}", typeof(T)));
            }
        }

        public static T ConvertCurrent<T>(IDBQueryResult provider)
            where T : IDBObject
        {
            if (!provider.IsEmpty)
            {
                foreach (var item in Convert<T>(provider)) return item;
            }

            return default(T);
        }

    }

    /// <summary>
    /// 代表DB資料類別
    /// </summary>
    public class DBObjectAttribute : Attribute { }

    /// <summary>
    /// 代表資料類別欄位所對應到的DB資料欄位
    /// </summary>
    public class DBFieldAttribute : Attribute
    {
        /// <summary>
        /// 欄位名稱
        /// </summary>
        public string ColumnName { get; private set; }
        public DBFieldAttribute(string columnName)
        {
            ColumnName = columnName;
        }
    }

    public interface IDBObject { }
}
