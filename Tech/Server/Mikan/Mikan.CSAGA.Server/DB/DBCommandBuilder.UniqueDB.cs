﻿using Mikan.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mikan.CSAGA.Server
{
    public static partial class DBCommandBuilder
    {
        #region unique db

        public static IDBCommand CreateAccountTable()
        {
            const string text = @"CREATE TABLE IF NOT EXISTS `account` (
                                  `id` bigint(20) NOT NULL AUTO_INCREMENT,
                                  `group_id` int(11) NOT NULL DEFAULT '0',
                                  `sub_id` bigint(20) NOT NULL DEFAULT '0',
                                  `uuid` varchar(20) NOT NULL,
                                  `key` int(11) NOT NULL,
                                  `create_time` datetime NOT NULL,
                                  PRIMARY KEY (`id`),
                                  UNIQUE KEY `uuid_UNIQUE` (`uuid`)
                                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

            return CreateDBCommand(text);
        }

        public static IDBCommand CreateSignatureTable()
        {
            const string text = @"CREATE TABLE IF NOT EXISTS `signature` (
                                  `id` int(11) NOT NULL AUTO_INCREMENT,
                                  `type` int(11) NOT NULL,
                                  `timestamp` datetime NOT NULL,
                                  PRIMARY KEY (`id`)
                                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

            return CreateDBCommand(text);
        }

        public static IDBCommand CreateDataCacheTable()
        {
            const string text = @"CREATE TABLE IF NOT EXISTS `data_cache` (
                                  `signature` int(11) NOT NULL,
                                  `data_id` int(11) NOT NULL,
                                  `value` int(11) NOT NULL,
                                  PRIMARY KEY (`signature`,`data_id`)
                                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

            return CreateDBCommand(text);
        }

        public static IDBCommand CreateEventScheduleTable()
        {
            const string text = @"CREATE TABLE IF NOT EXISTS `event_schedule` (
                                  `id` bigint(20) NOT NULL AUTO_INCREMENT,
                                  `data_id` int(11) NOT NULL,
                                  `begin_time` datetime NOT NULL,
                                  `complex` int(11) NOT NULL DEFAULT '0',
                                  `player_count` int(11) NOT NULL DEFAULT '0',
                                  PRIMARY KEY (`id`)
                                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

            return CreateDBCommand(text);

        }

        public static IDBCommand CreateCoMissionTable()
        {
            const string text = @"CREATE TABLE IF NOT EXISTS `co_mission` (
                                  `id` bigint(20) NOT NULL AUTO_INCREMENT,
                                  `seed` bigint(20) NOT NULL,
                                  `begin_time` datetime NOT NULL,
                                  `complex` int(11) NOT NULL DEFAULT '0',
                                  PRIMARY KEY (`id`)
                                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

            return CreateDBCommand(text);
        }

        public static IDBCommand CreateSharedQuestTable()
        {
            const string text = @"CREATE TABLE IF NOT EXISTS `shared_quest` (
                                  `id` bigint NOT NULL AUTO_INCREMENT,
                                  `group_id` int NOT NULL,
                                  `player_id` bigint NOT NULL,
                                  `quest_id` int NOT NULL,
                                  `play_count` int NOT NULL DEFAULT '0',
                                  `end_time` datetime NOT NULL,
                                  PRIMARY KEY (`id`),
                                  KEY `i_player_id` (`group_id`, `player_id`)
                                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

            return CreateDBCommand(text);
        }

        #region Sys

        public static IDBCommand CreateDummyTable()
        {
            const string text = @"CREATE TABLE IF NOT EXISTS `dummy` (
                                  `key` int(11) NOT NULL,
                                  `group_id` int(11) NOT NULL,
                                  `player_id` bigint(20) NOT NULL,
                                  `favorite` int(11) NOT NULL,
                                  PRIMARY KEY (`key`)
                                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

            return CreateDBCommand(text);
        }

        public static IDBCommand CreateTestPlayerTable()
        {
            const string text = @"CREATE TABLE IF NOT EXISTS `test_player` (
                                  `key` int(11) NOT NULL,
                                  `group_id` int(11) NOT NULL,
                                  `player_id` bigint(20) NOT NULL,
                                  `timestamp` datetime NOT NULL,
                                  PRIMARY KEY (`key`)
                                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

            return CreateDBCommand(text);
        }

        public static IDBCommand CreateSerialNumberTable()
        {
            const string text = @"CREATE TABLE IF NOT EXISTS `serial_number` (
                                  `id` int(11) NOT NULL,
                                  `group_id` int(11) NOT NULL,
                                  `player_id` bigint(20) NOT NULL,
                                  `timestamp` datetime NOT NULL,
                                  `trophy_id` int(11) NOT NULL,
                                  PRIMARY KEY (`id`)
                                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

            return CreateDBCommand(text);
        }

        public static IDBCommand CreateIAPProductTable()
        {
            const string text = @"CREATE TABLE IF NOT EXISTS `iap_product` (
                                  `id` int(11) NOT NULL,
                                  `product_id` varchar(30) NOT NULL,
                                  `gem` int(11) NOT NULL,
                                  `bonus` int(11) NOT NULL,
                                  `name` varchar(20) NOT NULL,
                                  `price` varchar(20) NOT NULL
                                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

            return CreateDBCommand(text);
        }

        public static IDBCommand CreateGooglePlayReceiptTable()
        {
            const string text = @"CREATE TABLE IF NOT EXISTS `google_play_receipt` (
                                  `id` bigint(20) NOT NULL AUTO_INCREMENT,
                                  `group_id` int(11) NOT NULL,
                                  `player_id` bigint(20) NOT NULL,
                                  `hash` bigint(20) NOT NULL,
                                  `order_id` varchar(45) NOT NULL,
                                  `product_id` int(11) NOT NULL,
                                  `token` text NOT NULL,
                                  `gem` int(11) NOT NULL,
                                  `bonus` int(11) NOT NULL DEFAULT '0',
                                  `timestamp` datetime NOT NULL,
                                  PRIMARY KEY (`id`),
                                  KEY `i_hash` (`hash`)
                                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

            return CreateDBCommand(text);
        }

        public static IDBCommand CreateIOSReceiptTable()
        {
            const string text = @"CREATE TABLE IF NOT EXISTS `ios_receipt` (
                                  `id` bigint(20) NOT NULL AUTO_INCREMENT,
                                  `group_id` int(11) NOT NULL,
                                  `player_id` bigint(20) NOT NULL,
                                  `hash` bigint(20) NOT NULL,
                                  `order_id` varchar(45) NOT NULL,
                                  `product_id` int(11) NOT NULL,
                                  `token` text NOT NULL,
                                  `gem` int(11) NOT NULL,
                                  `bonus` int(11) NOT NULL DEFAULT '0',
                                  `timestamp` datetime NOT NULL,
                                  PRIMARY KEY (`id`),
                                  KEY `i_hash` (`hash`)
                                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

            return CreateDBCommand(text);
        }

        public static IDBCommand CreateMyCardReceiptTable()
        {
            const string text = @"CREATE TABLE IF NOT EXISTS `mycard_receipt` (
                                  `id` bigint(20) NOT NULL AUTO_INCREMENT,
                                  `group_id` int(11) NOT NULL,
                                  `player_id` bigint(20) NOT NULL,
                                  `hash` bigint(20) NOT NULL,
                                  `order_id` varchar(45) NOT NULL,
                                  `product_id` int(11) NOT NULL,
                                  `token` text NOT NULL,
                                  `gem` int(11) NOT NULL,
                                  `bonus` int(11) NOT NULL DEFAULT '0',
                                  `timestamp` datetime NOT NULL,
                                  PRIMARY KEY (`id`),
                                  KEY `i_hash` (`hash`)
                                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

            return CreateDBCommand(text);
        }

        public static IDBCommand CreateMyCardTransactionTable()
        {
            const string text = @"CREATE TABLE IF NOT EXISTS `mycard_transaction` (
                                  `id` bigint(20) NOT NULL AUTO_INCREMENT,
                                  `group_id` int(11) NOT NULL,
                                  `player_id` bigint(20) NOT NULL,
                                  `product_id` int(11) NOT NULL,
                                  `auth_code` varchar(256) NOT NULL DEFAULT '',
                                  `trade_seq` varchar(20) NOT NULL DEFAULT '',
                                  `complex` int(11) NOT NULL DEFAULT '0',
                                  `timestamp` datetime NOT NULL,
                                  PRIMARY KEY (`id`),
                                  KEY `i_player_id` (`group_id`, `player_id`)
                                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

            return CreateDBCommand(text);
        }

        public static IDBCommand CreateMyCardHistoryTable()
        {
            const string text = @"CREATE TABLE IF NOT EXISTS `mycard_history` (
                                  `id` bigint(20) NOT NULL AUTO_INCREMENT,
                                  `transaction_id` bigint(20) NOT NULL,
                                  `group_id` int(11) NOT NULL,
                                  `player_id` bigint(20) NOT NULL,
                                  `return_code` varchar(10) NOT NULL,
                                  `return_msg` varchar(50) NOT NULL,
                                  `pay_result` varchar(10) NOT NULL,
                                  `fac_trade_seq` varchar(20) NOT NULL,
                                  `payment_type` varchar(10) NOT NULL,
                                  `amount` varchar(10) NOT NULL,
                                  `currency` varchar(10) NOT NULL,
                                  `trade_seq` varchar(20) NOT NULL,
                                  `mycard_trade_no` varchar(30) NOT NULL,
                                  `mycard_type` varchar(10) NOT NULL,
                                  `promo_code` varchar(30) NOT NULL,
                                  `serial_id` varchar(30) NOT NULL,
                                  `timestamp` datetime NOT NULL,
                                  PRIMARY KEY (`id`),
                                  KEY `i_transaction_id` (`transaction_id`)
                                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

            return CreateDBCommand(text);
        }

        public static IDBCommand CreateTransferCacheTable()
        {
            const string text = @"CREATE TABLE IF NOT EXISTS `transfer_cache` (
                                  `group_id` int(11) NOT NULL,
                                  `player_id` bigint(20) NOT NULL,
                                  `uuid` varchar(20) NOT NULL,
                                  `key` int(11) NOT NULL,
                                  `timestamp` datetime NOT NULL,
                                  `status` int(11) NOT NULL DEFAULT '0',
                                  `transfer_key` int(11) NOT NULL DEFAULT '0',
                                  PRIMARY KEY (`group_id`, `player_id`)
                                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

            return CreateDBCommand(text);
        }

        public static IDBCommand CreateGMTable()
        {
            const string text = @"CREATE TABLE IF NOT EXISTS `gm` (
                                  `group_id` int(11) NOT NULL,
                                  `player_id` bigint(20) NOT NULL,
                                  PRIMARY KEY (`group_id`, `player_id`)
                                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

            return CreateDBCommand(text);
        }

        public static IDBCommand CreateBlackTable()
        {
            const string text = @"CREATE TABLE IF NOT EXISTS `black` (
                                  `id` bigint(20) NOT NULL AUTO_INCREMENT,
                                  `group_id` int(11) NOT NULL,
                                  `player_id` bigint(20) NOT NULL,
                                  `platform` varchar(10) NOT NULL,
                                  `app_ver` varchar(10) NOT NULL,
                                  `signature` varchar(50) NOT NULL,
                                  `timestamp` datetime NOT NULL,
                                  PRIMARY KEY (`id`),
                                  KEY `i_player_id` (`group_id`, `player_id`)
                                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

            return CreateDBCommand(text);
        }
        public static IDBCommand CreateBlackGemTable()
        {
            const string text = @"CREATE TABLE IF NOT EXISTS `black_gem` (
                                  `group_id` int(11) NOT NULL,
                                  `player_id` bigint(20) NOT NULL,
                                  `value` int(11) NOT NULL,
                                  PRIMARY KEY (`group_id`, `player_id`)
                                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

            return CreateDBCommand(text);
        }

        public static IDBCommand CreatePVPIdlePlayerTable()
        {
            const string text = @"CREATE TABLE IF NOT EXISTS `pvp_idle_player` (
                                  `group` int NOT NULL,
                                  `player_id` bigint NOT NULL,
                                  `card_id` int NOT NULL,
                                  `quest_design` text NOT NULL
                                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

            return CreateDBCommand(text);
        }

        #endregion

        #endregion

        #region log db
        public static IDBCommand CreateLogQuestTable()
        {
            const string text = @"CREATE TABLE IF NOT EXISTS `log_quest` (
                                  `quest_id` int NOT NULL,
                                  `play_count` int NOT NULL DEFAULT '0',
                                  `clear_count` int NOT NULL DEFAULT '0',
                                  `continue_count` int NOT NULL DEFAULT '0', 
                                  `update_time` datetime NOT NULL,
                                  PRIMARY KEY (`quest_id`)
                                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

            return CreateDBCommand(text);
        }

        public static IDBCommand CreateLogSignatureTable()
        {
            const string text = @"CREATE TABLE IF NOT EXISTS `log_signature` (
                                  `group_id` int NOT NULL,
                                  `player_id` bigint NOT NULL,
                                  `ip` nvarchar(20) NOT NULL,
                                  `signature` text NOT NULL,
                                  `timestamp` datetime NOT NULL
                                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

            return CreateDBCommand(text);
        }

        public static IDBCommand CreateLogEventTable()
        {
            const string text = @"CREATE TABLE IF NOT EXISTS `log_event` (
                                  `id` int NOT NULL,
                                  `current` int NOT NULL DEFAULT '0',
                                  `history` int NOT NULL DEFAULT '0',
                                  `update_time` datetime NOT NULL,
                                  `history_time` datetime NOT NULL,
                                  PRIMARY KEY (`id`)
                                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

            return CreateDBCommand(text);
        }

        public static IDBCommand CreateLogEventEachTable()
        {
            const string text = @"CREATE TABLE IF NOT EXISTS `log_event_each` (
                                  `id` int NOT NULL,
                                  `player_id` bigint NOT NULL,
                                  `current` int NOT NULL DEFAULT '0',
                                  `update_time` datetime NOT NULL,
                                  KEY `i_id` (`id`),
                                  KEY `i_player_id` (`player_id`)
                                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

            return CreateDBCommand(text);
        }
        #endregion
    }
}
