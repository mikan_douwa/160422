﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mikan.CSAGA.Server
{
    [DBObject]
    public class DBConfig : IDBObject
    {
        [DBField("key")]
        public int Key;

        [DBField("value")]
        public string Value;
    }

    [DBObject]
    public class DBGroup : IDBObject
    {
        [DBField("id")]
        public int ID;

        [DBField("db_connstr")]
        public string ConnectionString;
    }

    [DBObject]
    public class DBAccount : IDBObject
    {
        [DBField("id")]
        public Int64 ID;

        [DBField("group_id")]
        public int GroupID;

        [DBField("sub_id")]
        public Int64 SubID;

        [DBField("uuid")]
        public string UUID;

        [DBField("key")]
        public int Key;

        [DBField("create_time")]
        public DateTime CreateTime;
    }

    [DBObject]
    public class DBSignature : IDBObject
    {
        [DBField("id")]
        public int ID;

        [DBField("type")]
        public int Type;

        [DBField("timestamp")]
        public DateTime Timestamp;
    }

    [DBObject]
    public class DBDataCache : IDBObject
    {
        [DBField("signature")]
        public int Signature;

        [DBField("data_id")]
        public int DataID;

        [DBField("value")]
        public int Value;
    }

    [DBObject]
    public class DBEventSchedule : IDBObject
    {
        [DBField("id")]
        public Int64 ID;

        [DBField("data_id")]
        public int DataID;

        [DBField("begin_time")]
        public DateTime BeginTime;

        [DBField("complex")]
        public int Complex;

        [DBField("player_count")]
        public int PlayerCount;
    }

    [DBObject]
    public class DBCoMission : IDBObject
    {
        [DBField("id")]
        public Int64 ID;

        [DBField("seed")]
        public Int64 Seed;

        [DBField("begin_time")]
        public DateTime BeginTime;

        [DBField("complex")]
        public int Complex;
    }

    [DBObject]
    public class DBTestPlayer : IDBObject
    {
        [DBField("key")]
        public int Key;
        [DBField("group_id")]
        public int GroupID;
        [DBField("player_id")]
        public Int64 PlayerID;
        [DBField("timestamp")]
        public DateTime Timestamp;
    }

    [DBObject]
    public class DBSerialNumber : IDBObject
    {
        [DBField("id")]
        public int ID;
        [DBField("trophy_id")]
        public int TrophyID;
        [DBField("group_id")]
        public int GroupID;
        [DBField("player_id")]
        public Int64 PlayerID;
        [DBField("timestamp")]
        public DateTime Timestamp;
    }

    [DBObject]
    public class DBIAPProduct : IDBObject
    {
        [DBField("id")]
        public int ID;
        [DBField("product_id")]
        public string ProductID;
        [DBField("gem")]
        public int Gem;
        [DBField("bonus")]
        public int Bonus;
        [DBField("name")]
        public string Name;
        [DBField("price")]
        public string Price;
        [DBField("dollars")]
        public int Dollars;
    }

    [DBObject]
    public class DBIAPReceipt : IDBObject
    {
        [DBField("id")]
        public Int64 ID;

        [DBField("group_id")]
        public int GroupID;

        [DBField("player_id")]
        public Int64 PlayerID;

        [DBField("hash")]
        public Int64 Hash;

        [DBField("order_id")]
        public string OrderID;

        [DBField("product_id")]
        public int ProductID;

        [DBField("gem")]
        public int Gem;
    }

    [DBObject]
    public class DBTransferCache : IDBObject
    {
        [DBField("group_id")]
        public int GroupID;

        [DBField("player_id")]
        public Int64 PlayerID;

        [DBField("uuid")]
        public string UUID;

        [DBField("key")]
        public int Key;

        [DBField("timestamp")]
        public DateTime Timestamp;

        [DBField("status")]
        public int Status;

        [DBField("transfer_key")]
        public int TransferKey;
    }

    [DBObject]
    public class DBGM : IDBObject
    {
        [DBField("group_id")]
        public int GroupID;

        [DBField("player_id")]
        public Int64 PlayerID;
    }

    [DBObject]
    public class DBBlack : IDBObject
    {
        [DBField("group_id")]
        public int GroupID;

        [DBField("player_id")]
        public Int64 PlayerID;

        [DBField("app_ver")]
        public string AppVer;

        [DBField("timestamp")]
        public DateTime Timestamp;
    }

    [DBObject]
    public class DBMyCardTransaction : IDBObject
    {
        [DBField("id")]
        public Int64 ID;

        [DBField("group_id")]
        public int GroupID;

        [DBField("player_id")]
        public Int64 PlayerID;

        [DBField("product_id")]
        public int ProductID;

        [DBField("auth_code")]
        public string AuthCode;

        [DBField("trade_seq")]
        public string TradeSeq;

        [DBField("complex")]
        public int Complex;

        [DBField("timestamp")]
        public DateTime Timestamp;
    }

    [DBObject]
    public class DBMyCardHistory : IDBObject
    {
        [DBField("transaction_id")]
        public Int64 TransactionID;
        [DBField("group_id")]
        public int GroupID;
        [DBField("player_id")]
        public Int64 PlayerID;
        [DBField("return_code")]
        public string ReturnCode;
        [DBField("return_msg")]
        public string ReturnMsg;
        [DBField("pay_result")]
        public string PayResult;
        [DBField("fac_trade_seq")]
        public string FacTradeSeq;
        [DBField("payment_type")]
        public string PaymentType;
        [DBField("amount")]
        public string Amount;
        [DBField("currency")]
        public string Currency;
        [DBField("trade_seq")]
        public string TradeSeq;
        [DBField("mycard_trade_no")]
        public string MyCardTradeNo;
        [DBField("mycard_type")]
        public string MyCardType;
        [DBField("promo_code")]
        public string PromoCode;
        [DBField("serial_id")]
        public string SerialId;
        [DBField("timestamp")]
        public DateTime Timestamp;
    }

    [DBObject]
    public class DBBlackGem : IDBObject
    {
        [DBField("group_id")]
        public int GroupID;

        [DBField("player_id")]
        public Int64 PlayerID;

        [DBField("value")]
        public int Value;
    }

    [DBObject]
    public class DBDummy : IDBObject
    {
        [DBField("key")]
        public int Key;
        [DBField("group_id")]
        public int GroupID;

        [DBField("player_id")]
        public Int64 PlayerID;

        [DBField("favorite")]
        public int Favorite;
    }

    [DBObject]
    public class DBSharedQuest : IDBObject
    {
        [DBField("id")]
        public Int64 ID;

        [DBField("group_id")]
        public int GroupID;

        [DBField("player_id")]
        public Int64 PlayerID;

        [DBField("quest_id")]
        public int QuestID;

        [DBField("play_count")]
        public int PlayCount;

        [DBField("end_time")]
        public DateTime EndTime;
    }

    [DBObject]
    public class DBPVPIdlePlayer : IDBObject
    {
        [DBField("group")]
        public int Group;

        [DBField("player_id")]
        public Int64 PlayerID;

        [DBField("card_id")]
        public int CardID;

        [DBField("quest_design")]
        public string QuestDesign;

    }
}
