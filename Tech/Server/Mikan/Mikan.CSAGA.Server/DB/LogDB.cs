﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mikan.CSAGA.Server
{
    [DBObject]
    public class DBLogQuest : IDBObject
    {
        [DBField("quest_id")]
        public int QuestID;
        [DBField("play_count")]
        public int PlayCount;
        [DBField("clear_count")]
        public int ClearCount;
        [DBField("continue_count")]
        public int ContinueCount;
        [DBField("update_time")]
        public DateTime UpdateTime;
    }

    [DBObject]
    public class DBLogEvent : IDBObject
    {
        [DBField("id")]
        public int ID;
        [DBField("current")]
        public int Current;
        [DBField("history")]
        public int History;
        [DBField("update_time")]
        public DateTime UpdateTime;
        [DBField("history_time")]
        public DateTime HistoryTime;
    }
}
