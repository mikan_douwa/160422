﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mikan.CSAGA.Server
{
    public class DBPlayerObj : IDBObject
    {
        [DBField("player_id")]
        public Int64 PlayerID;
    }

    [DBObject]
    public class DBBackup : DBPlayerObj
    {
        [DBField("key")]
        public int Key;

        [DBField("data")]
        public string Data;
    }

    [DBObject]
    public class DBPlayer : DBPlayerObj
    {
        [DBField("id")]
        public Int64 ID;

        [DBField("name")]
        public string Name;

        [DBField("exp")]
        public int Exp;

        [DBField("crystal")]
        public int Crystal;

        [DBField("coin")]
        public int Coin;

        [DBField("fp")]
        public int FP;

        [DBField("tp_consumed")]
        public int TPConsumed;

        [DBField("tp_change_time")]
        public DateTime TPChangeTime;

        [DBField("last_play_time")]
        public DateTime LastPlayTime;

        [DBField("complex")]
        public Int64 Complex;
    }

    [DBObject]
    public class DBCard : DBPlayerObj
    {
        [DBField("id")]
        public Int64 ID;

        [DBField("card_id")]
        public int CardID;

        [DBField("exp")]
        public int Exp;

        [DBField("kizuna")]
        public int Kizuna;

        [DBField("complex")]
        public Int64 Complex;

    }

    [DBObject]
    public class DBItem : DBPlayerObj
    {
        [DBField("item_id")]
        public int ItemID;

        [DBField("count")]
        public int Count;
    }

    [DBObject]
    public class DBEquipment : DBPlayerObj
    {
        [DBField("id")]
        public Int64 ID;

        [DBField("card_id")]
        public Int64 CardID;

        [DBField("slot")]
        public int Slot;

        [DBField("item_id")]
        public int ItemID;

        [DBField("hp")]
        public int Hp;

        [DBField("atk")]
        public int Atk;

        [DBField("rev")]
        public int Rev;

        [DBField("chg")]
        public int Chg;

        [DBField("effect_id")]
        public int EffectID;

        [DBField("counter")]
        public int Counter;

        [DBField("counter_type")]
        public int CounterType;
    }

    [DBObject]
    public class DBCardEventCache : DBPlayerObj
    {
        [DBField("card_id")]
        public Int64 CardID;
        [DBField("interactive_id")]
        public int InteractiveID;
        [DBField("quest_id")]
        public Int64 QuestID;
    }

    [DBObject]
    public class DBQuest : DBPlayerObj
    {
        [DBField("quest_id")]
        public int QuestID;
        [DBField("complex")]
        public int Complex;
        [DBField("last_play")]
        public DateTime LastPlayTime;
    }

    [DBObject]
    public class DBQuestCache : DBPlayerObj
    {
        [DBField("id")]
        public Int64 ID;

        [DBField("quest_id")]
        public int QuestID;

        [DBField("data")]
        public string Data;

        [DBField("complex")]
        public int Complex;

        [DBField("timestamp")]
        public DateTime Timestamp;

    }

    public class QuestCacheData : SerializableObject
    {
        public int Drop;

        public int LuckDrop;

        public int FP;

        public int EP;

        public int Discover;

        public Int64 Crusade;

        public Int64 CrusadePoint;

        public PublicID Fellow;

        private Dictionary<int, string> values = new Dictionary<int, string>();

        public static QuestCacheData Create(QuestInstance quest)
        {
            var data = new QuestCacheData();
            data.Drop = quest.Drop;
            data.LuckDrop = quest.LuckDrop;
            data.EP = quest.Point;
            data.Discover = quest.Discover;
            return data;
        }

        protected override void Serialize(IOutput output)
        {
            OnSerialize();

            output.Write(values.Count);

            foreach (var item in values)
            {
                output.Write(item.Key.ToString());
                output.Write(item.Value);
            }
        }

        protected override void DeSerialize(IInput input)
        {
            var count = input.ReadInt32();
            for (int i = 0; i < count; ++i)
            {
                var key = int.Parse(input.ReadString());
                var value = input.ReadString();
                values[key] = value;
            }

            OnDeSerialize();
        }

        private void OnSerialize()
        {
            Add(1, Drop);
            Add(2, LuckDrop);
            Add(3, FP);
            Add(4, EP);
            Add(5, Discover);
            Add(6, Crusade);
            Add(7, CrusadePoint);
            if (Fellow != null)
                Add(8, Fellow.ToString());
        }

        private void OnDeSerialize()
        {
            Drop = GetInt32(1);
            LuckDrop = GetInt32(2);
            FP = GetInt32(3);
            EP = GetInt32(4);
            Discover = GetInt32(5);
            Crusade = GetInt64(6);
            CrusadePoint = GetInt32(7);
            Fellow = PublicID.FromString(GetString(8));

        }

        private int GetInt32(int key)
        {
            var str = GetString(key);
            if (string.IsNullOrEmpty(str)) return 0;
            return Int32.Parse(str);
        }

        private Int64 GetInt64(int key)
        {
            var str = GetString(key);
            if (string.IsNullOrEmpty(str)) return 0;
            return Int64.Parse(str);
        }

        private string GetString(int key)
        {
            var val = default(string);
            if (values.TryGetValue(key, out val)) return val;
            return "";
        }

        private void Add(int key, Int64 val)
        {
            if (val == 0) return;
            Add(key, val.ToString());
        }

        private void Add(int key, string val)
        {
            if (string.IsNullOrEmpty(val)) return;
            values[key] = val;
        }

        
    }

    [DBObject]
    public class DBLimitedQuest : DBPlayerObj
    {
        [DBField("id")]
        public Int64 ID;

        [DBField("quest_id")]
        public int QuestID;

        [DBField("play_count")]
        public int PlayCount;

        [DBField("end_time")]
        public DateTime EndTime;
    }

    [DBObject]
    public class DBPurchase : DBPlayerObj
    {
        [DBField("article_id")]
        public byte ArticleID;
        [DBField("cost")]
        public int Cost;
        [DBField("iap_cost")]
        public int IAPCost;
        [DBField("timestamp")]
        public DateTime Timestamp;
    }

    [DBObject]
    public class DBReserved : DBPlayerObj
    {
        [DBField("id")]
        public Int64 ID;
        [DBField("type")]
        public byte Type;
        [DBField("value_1")]
        public int Value1;
        [DBField("value_2")]
        public int Value2;
        [DBField("effective_time")]
        public DateTime EffectiveTime;
    }

    [DBObject]
    public class DBGift : DBPlayerObj
    {
        [DBField("id")]
        public Int64 ID;

        [DBField("type")]
        public int Type;

        [DBField("description")]
        public string Description;

        [DBField("value_1")]
        public int Value1;

        [DBField("value_2")]
        public int Value2;

        [DBField("begin_time")]
        public DateTime BeginTime;

        [DBField("end_time")]
        public DateTime EndTime;

        [DBField("source")]
        public byte Source;
    }

    [DBObject]
    public class DBTrophy : DBPlayerObj
    {
        [DBField("group")]
        public int Group;

        [DBField("current")]
        public int Current;

        [DBField("progress")]
        public int Progress;

        [DBField("update_time")]
        public DateTime UpdateTime;
    }

    [DBObject]
    public class DBSimpleTrophy : DBPlayerObj
    {
        [DBField("trophy_id")]
        public int TrophyID;
    }

    [DBObject]
    public class DBDaily : DBPlayerObj
    {
        [DBField("seed")]
        public Int64 Seed;

        [DBField("state")]
        public int State;

        [DBField("consumed")]
        public int Consumed;

        [DBField("add")]
        public int Add;

        [DBField("cached")]
        public int Cached;

        [DBField("update_time")]
        public DateTime UpdateTime;
    }

    [DBObject]
    public class DBRandomShop : DBPlayerObj
    {
        [DBField("seed")]
        public Int64 Seed;
        [DBField("state")]
        public int State;
        [DBField("update_time")]
        public DateTime UpdateTime;

    }

    [DBObject]
    public class DBGallery : DBPlayerObj
    {
        [DBField("card_id")]
        public int CardID;

        [DBField("luck")]
        public int Luck;
    }

    [DBObject]
    public class DBEventPoint : DBPlayerObj
    {
        [DBField("event_id")]
        public Int64 EventID;
        [DBField("point")]
        public int Point;
        [DBField("complex")]
        public int Complex;
        [DBField("ranking")]
        public int Ranking;
        [DBField("card_id")]
        public int CardID;
    }

    [DBObject]
    public class DBInfinity : DBPlayerObj
    {
        [DBField("id")]
        public Int64 ID;
        [DBField("event_id")]
        public Int64 EventID;
        [DBField("point")]
        public int Point;
        [DBField("update_time")]
        public DateTime UpdateTime;
    }

    [DBObject]
    public class DBRescue : DBPlayerObj
    {
        [DBField("count")]
        public int Count;

        [DBField("update_time")]
        public DateTime UpdateTime;
    }

    [DBObject]
    public class DBIAPHistory : DBPlayerObj
    {
        [DBField("product_id")]
        public int ProductID;

        [DBField("receipt_id")]
        public Int64 ReceiptID;

        [DBField("update_time")]
        public DateTime UpdateTime;
    }

    [DBObject]
    public class DBCoMissionState : DBPlayerObj
    {
        [DBField("mission_id")]
        public Int64 MissionID;

        [DBField("subject_1")]
        public int Subject1;
        [DBField("subject_2")]
        public int Subject2;
        [DBField("subject_3")]
        public int Subject3;
        [DBField("subject_4")]
        public int Subject4;
        [DBField("subject_5")]
        public int Subject5;

        [DBField("complex")]
        public int Complex;
    }

    [DBObject]
    public class DBSumCoMissionState : IDBObject
    {
        [DBField("subject_1")]
        public Decimal Subject1;
        [DBField("subject_2")]
        public Decimal Subject2;
        [DBField("subject_3")]
        public Decimal Subject3;
        [DBField("subject_4")]
        public Decimal Subject4;
        [DBField("subject_5")]
        public Decimal Subject5;
    }

    [DBObject]
    public class DBMyLord : DBPlayerObj
    {
        [DBField("rescue_miss")]
        public int RescueMiss;
        [DBField("max_rescue_miss")]
        public int MaxRescueMiss;
        [DBField("gacha_miss")]
        public int GachaMiss;
        [DBField("max_gacha_miss")]
        public int MaxGachaMiss;
    }

    [DBObject]
    public class DBSellCache : DBPlayerObj
    {
        [DBField("card_id")]
        public int CardID;

        [DBField("kizuna")]
        public int Kizuna;
    }

    [DBObject]
    public class DBFellow : DBPlayerObj
    {
        [DBField("id")]
        public Int64 ID;

        [DBField("fellow_group")]
        public int FellowGroup;

        [DBField("fellow_id")]
        public Int64 FellowID;

        [DBField("complex")]
        public int Complex;

        [DBField("update_time")]
        public DateTime UpdateTime;

    }

    [DBObject]
    public class DBSnapshot : DBPlayerObj
    {
        [DBField("data")]
        public string Data;

        [DBField("update_time")]
        public DateTime UpdateTime;

    }

    [DBObject]
    public class DBSocialPurchase : DBPlayerObj
    {
        [DBField("article_id")]
        public int ArticleID;

        [DBField("cost")]
        public int Cost;

        [DBField("timestamp")]
        public DateTime Timestamp;
    }

    [DBObject]
    public class DBDailyEP : DBPlayerObj
    {
        [DBField("id")]
        public Int64 ID;
        [DBField("point")]
        public int Point;
        [DBField("update_time")]
        public DateTime UpdateTime;
    }

    [DBObject]
    public class DBCrusade : DBPlayerObj
    {
        [DBField("id")]
        public Int64 ID;
        [DBField("discover")]
        public int Discover;
        [DBField("point")]
        public int Point;
        [DBField("update_time")]
        public DateTime UpdateTime;
    }

    [DBObject]
    public class DBCrusadeCache : DBPlayerObj
    {
        [DBField("id")]
        public Int64 ID;
        [DBField("quest_id")]
        public Int64 QuestID;
        [DBField("is_self")]
        public bool IsSelf;
    }

    [DBObject]
    public class DBFaith : DBPlayerObj
    {
        [DBField("id")]
        public Int64 ID;

        [DBField("index")]
        public int Index;

        [DBField("data_ref")]
        public int DataRef;

        [DBField("result")]
        public int Result;

        [DBField("end_time")]
        public DateTime EndTime;
    }

    [DBObject]
    public class DBFaithClaim : DBPlayerObj
    {
        [DBField("claim_time")]
        public DateTime ClaimTime;
    }

    [DBObject]
    public class DBTactics : DBPlayerObj
    {
        [DBField("tactics_id")]
        public int TacticsID;

        [DBField("timestamp")]
        public DateTime Timestamp;
    }

    [DBObject]
    public class DBPVPContext : DBPlayerObj
    {
        [DBField("bp_consumed")]
        public int BPConsumed;

        [DBField("bp_update_time")]
        public DateTime BPUpdateTime;

        [DBField("quest_design")]
        public string QuestDesign;

        [DBField("update_time")]
        public DateTime UpdateTime;

        [DBField("card_id")]
        public int CardID;
    }

    [DBObject]
    public class DBPVPSkill : DBPlayerObj
    {
        [DBField("skill_id")]
        public int SkillID;

        [DBField("timestamp")]
        public DateTime Timestamp;
    }

    [DBObject]
    public class DBPVPBattle : DBPlayerObj
    {
        [DBField("id")]
        public Int64 ID;

        [DBField("quest_id")]
        public Int64 QuestID;

        [DBField("target_group")]
        public int TargetGroup;

        [DBField("target_id")]
        public Int64 TargetID;

        [DBField("win")]
        public int Win;

        [DBField("lose")]
        public int Lose;

        [DBField("result")]
        public int Result;

        [DBField("timestamp")]
        public DateTime Timestamp;
    }
}
