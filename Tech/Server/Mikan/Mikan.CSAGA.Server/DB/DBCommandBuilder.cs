﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mikan.Server;

namespace Mikan.CSAGA.Server
{
    public static partial class DBCommandBuilder
    {
        #region Internal
        private static string BuildNoLockStatemenet(string statement)
        {
            return string.Format("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;\n{0}\nCOMMIT;", statement);
        }

        public static IDBCommand CreateDBCommand(DBCommandType type, string statement)
        {
            return new DBCommand { Type = type, CommandText = statement };
        }

        public static IDBCommand CreateCreateStoreProcedureDBCommand(string statement)
        {
            return CreateDBCommand(DBCommandType.CreateStoreProcedure, statement);
        }

        public static IDBCommand CreateCreateStoreProcedureDBCommand(string statement, int group)
        {
            return CreateDBCommand(DBCommandType.CreateStoreProcedure, string.Format(statement, group));
        }

        public static IDBCommand CreateDBCommand(string statement)
        {
            return CreateDBCommand(DBCommandType.Common, statement);
        }

        public static IDBCommand CreateDBCommand(string statement, PublicID id)
        {
            return CreateDBCommand(statement, id.Group);
        }

        public static IDBCommand CreateDBCommand(string statement, int group)
        {
            return CreateDBCommand(DBCommandType.Common, string.Format(statement, group));
        }

        public static IDBCommand CreateNoLockDBCommand(string statement)
        {
            return CreateDBCommand(BuildNoLockStatemenet(statement));
        }

        public static IDBCommand CreateNoLockDBCommand(string statement, PublicID id)
        {
            return CreateNoLockDBCommand(statement, id.Group);
        }

        public static IDBCommand CreateNoLockDBCommand(string statement, int group)
        {
            return CreateNoLockDBCommand(string.Format(statement, group));
        }

        private static IDBCommand SelectAll(PublicID id, string tablename)
        {
            var text = "SELECT * FROM `" + tablename + "_{0:00}` WHERE player_id=@player_id;";

            var cmd = CreateNoLockDBCommand(text, id);

            cmd.AddParameter("@player_id", id.Serial);

            return cmd;
        }

        private static IDBCommand SelectAll(int group, string tablename)
        {
            var text = "SELECT * FROM `" + tablename + "_{0:00}`;";

            return CreateNoLockDBCommand(text, group);
        }

        private static IDBCommand SelectAll(string tablename)
        {
            var text = "SELECT * FROM `" + tablename + "`;";

            return CreateNoLockDBCommand(text);
        }

        private static IDBCommand DeleteAll(PublicID id, string tablename)
        {
            string text = "DELETE FROM `" + tablename + "_{0:00}` WHERE player_id=@player_id;";

            var cmd = CreateDBCommand(text, id);

            cmd.AddParameter("@player_id", id.Serial);

            return cmd;
        }

        #endregion

        #region System

        public static IDBCommand SelectGroups()
        {
            const string text = "SELECT * FROM `group`;";

            return CreateNoLockDBCommand(text);
        }

        public static IDBCommand SelectCurrentTime()
        {
            const string text = "SELECT CURRENT_TIMESTAMP;";

            return CreateDBCommand(text);
        }

        public static IDBCommand SelectSuperAccount(PublicID id)
        {
            const string text = "SELECT * FROM `gm` WHERE `group_id`=@group_id AND `player_id`=@player_id;";

            var cmd = CreateNoLockDBCommand(text);

            cmd.AddParameter("@group_id", id.Group);
            cmd.AddParameter("@player_id", id.Serial);

            return cmd;
        }

        #endregion

        #region Account

        public static IDBCommand InsertTestPlayer(PublicID id, int key)
        {
            const string text = @"INSERT INTO `test_player` (`key`, `group_id`, `player_id`, `timestamp`) VALUES (@key, @group_id, @player_id, @timestamp);
                                  SELECT * FROM `account` WHERE id=LAST_INSERT_ID() LIMIT 1;";

            var cmd = CreateDBCommand(text);

            cmd.AddParameter("@group_id", id.Group);
            cmd.AddParameter("@player_id", id.Serial);
            cmd.AddParameter("@key", key);
            cmd.AddParameter("@timestamp", Global.CurrentTime);

            return cmd;
        }

        public static IDBCommand CreateAccount(string uuid, int key)
        {
            const string text = @"INSERT INTO `account` (`uuid`, `key`, `create_time`) VALUES (@uuid, @key, @now);
                                  SELECT * FROM `account` WHERE id=LAST_INSERT_ID() LIMIT 1;";

            var cmd = CreateDBCommand(text);

            cmd.AddParameter("@uuid", uuid);
            cmd.AddParameter("@key", key);
            cmd.AddParameter("@now", Global.CurrentTime);

            return cmd;
        }
        public static IDBCommand SelectAccount(string uuid)
        {
            const string text = "SELECT * FROM `account` WHERE uuid=@uuid LIMIT 1;";

            var cmd = CreateNoLockDBCommand(text);

            cmd.AddParameter("@uuid", uuid);

            return cmd;
        }

        public static IDBCommand SettleAccount(Int64 id, int group, Int64 subId)
        {
            const string text = "UPDATE `account` SET group_id=@group_id, sub_id=@sub_Id  WHERE id=@id;";

            var cmd = CreateDBCommand(text);

            cmd.AddParameter("@id", id);
            cmd.AddParameter("@group_id", group);
            cmd.AddParameter("@sub_id", subId);

            return cmd;
        }

        public static IDBCommand SelectTransferCache(PublicID id)
        {
            const string text = "SELECT * FROM `transfer_cache` WHERE group_id=@group_id AND player_id=@player_id LIMIT 1;";

            var cmd = CreateNoLockDBCommand(text);

            cmd.AddParameter("@group_id", id.Group);
            cmd.AddParameter("@player_id", id.Serial);

            return cmd;
        }

        public static IDBCommand InsertTransferCache(PublicID id, string uuid, int key, DateTime timestamp)
        {
            const string text = @"INSERT INTO `transfer_cache` (`group_id`, `player_id`, `uuid`, `key`, `timestamp`) VALUES (@group_id, @player_id, @uuid, @key, @timestamp);";

            var cmd = CreateDBCommand(text);

            cmd.AddParameter("@group_id", id.Group);
            cmd.AddParameter("@player_id", id.Serial);
            cmd.AddParameter("@uuid", uuid);
            cmd.AddParameter("@key", key);
            cmd.AddParameter("@timestamp", timestamp);

            return cmd;
        }

        public static IDBCommand UpdateTransferCache(PublicID id, int key, DateTime timestamp)
        {
            const string text = "UPDATE `transfer_cache` SET `key`=@key, timestamp=@timestamp, status=0  WHERE group_id=@group_id AND player_id=@player_id;";

            var cmd = CreateDBCommand(text);

            cmd.AddParameter("@group_id", id.Group);
            cmd.AddParameter("@player_id", id.Serial);
            cmd.AddParameter("@key", key);
            cmd.AddParameter("@timestamp", timestamp);

            return cmd;
        }

        public static IDBCommand UpdateTransferCacheTransferKey(PublicID id, int key)
        {
            const string text = "UPDATE `transfer_cache` SET transfer_key=@transfer_key  WHERE group_id=@group_id AND player_id=@player_id;";

            var cmd = CreateDBCommand(text);

            cmd.AddParameter("@group_id", id.Group);
            cmd.AddParameter("@player_id", id.Serial);
            cmd.AddParameter("@transfer_key", key);

            return cmd;
        }

        public static IDBCommand UpdateTransferCacheStatus(PublicID id, int status)
        {
            const string text = "UPDATE `transfer_cache` SET status=@status  WHERE group_id=@group_id AND player_id=@player_id;";

            var cmd = CreateDBCommand(text);

            cmd.AddParameter("@group_id", id.Group);
            cmd.AddParameter("@player_id", id.Serial);
            cmd.AddParameter("@status", status);

            return cmd;
        }

        public static IDBCommand TransferAccount(string uuid, int key)
        {
            const string text = "UPDATE `account` SET `key`=@key  WHERE uuid=@uuid;";

            var cmd = CreateDBCommand(text);

            cmd.AddParameter("@uuid", uuid);
            cmd.AddParameter("@key", key);

            return cmd;
        }

        #endregion

        #region Backup
        public static IDBCommand SelectBackup(PublicID id)
        {
            return SelectAll(id, "backup");
        }

        public static IDBCommand UpdateBackup(PublicID id, int key, string data)
        {
            const string text = "CALL update_backup_{0:00}(@player_id, @key, @data);";

            var cmd = CreateDBCommand(text, id);
            cmd.AddParameter("@player_id", id.Serial);
            cmd.AddParameter("@key", key);
            cmd.AddParameter("@data", data);

            return cmd;
        }
        #endregion

        #region Player

        public static IDBCommand SelectPlayer(Int64 id, int group)
        {
            const string text = "SELECT * FROM `player_{0:00}` WHERE `id`=@id;";

            var cmd = CreateNoLockDBCommand(text, group);

            cmd.AddParameter("@id", id);

            return cmd;
        }

        public static IDBCommand SelectPlayer(PublicID id)
        {
            const string text = "SELECT * FROM `player_{0:00}` WHERE `player_id`=@player_id;";

            var cmd = CreateNoLockDBCommand(text, id);

            cmd.AddParameter("@player_id", id.Serial);

            return cmd;
        }

        public static IDBCommand CreatePlayer(Int64 id, int group)
        {
            const string text = "INSERT INTO `player_{0:00}` (`id`, `tp_change_time`, `last_play_time`) VALUES (@id, @now, @now);";

            var cmd = CreateDBCommand(text, group);

            cmd.AddParameter("@id", id);
            cmd.AddParameter("@now", Global.CurrentTime);

            return cmd;
        }

        public static IDBCommand AlterPlayerName(PublicID id, string name, DateTime now)
        {
            const string text = "UPDATE `player_{0:00}` SET name=@name, last_play_time=@now WHERE player_id=@player_id;";

            var cmd = CreateDBCommand(text, id);

            cmd.AddParameter("@player_id", id.Serial);
            cmd.AddParameter("@name", name);
            cmd.AddParameter("@now", now);

            return cmd;
        }

        public static IDBCommand AlterPlayerNameFristTime(PublicID id, string name, DateTime now)
        {
            const string text = @"UPDATE `player_{0:00}` SET name=@name, complex=complex|@flag, last_play_time=@now WHERE player_id=@player_id;
                                  SELECT * FROM `player_{0:00}` WHERE player_id=@player_id;";

            var cmd = CreateDBCommand(text, id);

            cmd.AddParameter("@player_id", id.Serial);
            cmd.AddParameter("@name", name);
            cmd.AddParameter("@flag", 1);
            cmd.AddParameter("@now", now);

            return cmd;
        }

        public static IDBCommand UpdatePlayerLastPlayTime(PublicID id)
        {
            const string text = "UPDATE `player_{0:00}` SET last_play_time=@now WHERE player_id=@player_id;";

            var cmd = CreateDBCommand(text, id);

            cmd.AddParameter("@player_id", id.Serial);
            cmd.AddParameter("@now", Global.CurrentTime);

            return cmd;
        }

        public static IDBCommand UpdatePlayerComplex(PublicID id, Int64 complex)
        {
            const string text = "UPDATE `player_{0:00}` SET complex=complex|@complex WHERE player_id=@player_id;";

            var cmd = CreateDBCommand(text, id);

            cmd.AddParameter("@player_id", id.Serial);
            cmd.AddParameter("@complex", complex);

            return cmd;
        }

        public static IDBCommand ReplacePlayerComplex(PublicID id, Int64 complex)
        {
            const string text = "UPDATE `player_{0:00}` SET complex=@complex WHERE player_id=@player_id;";

            var cmd = CreateDBCommand(text, id);

            cmd.AddParameter("@player_id", id.Serial);
            cmd.AddParameter("@complex", complex);

            return cmd;
        }

        public static IDBCommand AddCrystal(PublicID id, int add)
        {
            return AddPlayerValue(id, add, "crystal");
        }

        public static IDBCommand AddCoin(PublicID id, int add)
        {
            return AddPlayerValue(id, add, "coin");
        }

        public static IDBCommand AddFP(PublicID id, int add)
        {
            return AddPlayerValue(id, add, "fp");
        }


        public static IDBCommand AddExp(PublicID id, int add)
        {
            return AddPlayerValue(id, add, "exp");
        }

        private static IDBCommand AddPlayerValue(PublicID id, int add, string field)
        {
            var text = "UPDATE `player_{0:00}` SET " + field + "=" + field + "+@add WHERE player_id=@player_id;";

            var cmd = CreateDBCommand(text, id);

            cmd.AddParameter("@player_id", id.Serial);
            cmd.AddParameter("@add", add);

            return cmd;
        }

        public static IDBCommand UpdateTPConsumed(PublicID id, int val, DateTime now)
        {
            const string text = "UPDATE `player_{0:00}` SET tp_consumed=@tp_consumed, tp_change_time=@now  WHERE player_id=@player_id;";

            var cmd = CreateDBCommand(text, id);

            cmd.AddParameter("@player_id", id.Serial);
            cmd.AddParameter("@tp_consumed", val);
            cmd.AddParameter("@now", now);

            return cmd;
        }

        #endregion

        #region Card

        public static IDBCommand SelectCardList(PublicID id)
        {
            return SelectAll(id, "card");
        }

        public static IDBCommand SelectCardSellCache(PublicID id)
        {
            return SelectAll(id, "sell_cache");
        }

        public static IDBCommand InsertCard(PublicID id, int cardId, int exp = 0)
        {
            const string text = @"INSERT INTO `card_{0:00}` (player_id, card_id, exp) VALUES (@player_id, @card_id, @exp);
                                  SELECT CAST(LAST_INSERT_ID() AS SIGNED);";

            var cmd = CreateDBCommand(text, id);

            cmd.AddParameter("@player_id", id.Serial);
            cmd.AddParameter("@card_id", cardId);
            cmd.AddParameter("@exp", exp);

            return cmd;
        }

        public static IDBCommand InsertCard(PublicID id, int cardId, Int64 complex, int exp)
        {
            const string text = @"INSERT INTO `card_{0:00}` (player_id, card_id, complex, exp) VALUES (@player_id, @card_id, @complex, @exp);
                                  SELECT CAST(LAST_INSERT_ID() AS SIGNED);";

            var cmd = CreateDBCommand(text, id);

            cmd.AddParameter("@player_id", id.Serial);
            cmd.AddParameter("@card_id", cardId);
            cmd.AddParameter("@complex", complex);
            cmd.AddParameter("@exp", exp);

            return cmd;
        }

        public static IDBCommand InsertCardSellCache(PublicID id, int cardId, int kizuna)
        {
            const string text = @"INSERT INTO `sell_cache_{0:00}` (player_id, card_id, kizuna) VALUES (@player_id, @card_id, @kizuna);";

            var cmd = CreateDBCommand(text, id);

            cmd.AddParameter("@player_id", id.Serial);
            cmd.AddParameter("@card_id", cardId);
            cmd.AddParameter("@kizuna", kizuna);

            return cmd;
        }

        public static IDBCommand AddCardExp(PublicID id, Serial serial, int add)
        {
            const string text = "UPDATE `card_{0:00}` SET exp=exp+@add WHERE id=@id;";

            var cmd = CreateDBCommand(text, id);

            cmd.AddParameter("@id", serial.Value);
            cmd.AddParameter("@add", add);

            return cmd;
        }

        public static IDBCommand AddCardKizuna(PublicID id, Serial serial, int add)
        {
            const string text = "UPDATE `card_{0:00}` SET kizuna=kizuna+@add WHERE id=@id;";

            var cmd = CreateDBCommand(text, id);

            cmd.AddParameter("@id", serial.Value);
            cmd.AddParameter("@add", add);

            return cmd;
        }

        public static IDBCommand AddCardValue(PublicID id, Serial serial, int expAdd, int kizunaAdd)
        {
            const string text = "UPDATE `card_{0:00}` SET exp=exp+@exp_add, kizuna=kizuna+@kizuna_add WHERE id=@id;";

            var cmd = CreateDBCommand(text, id);

            cmd.AddParameter("@id", serial.Value);
            cmd.AddParameter("@exp_add", expAdd);
            cmd.AddParameter("@kizuna_add", kizunaAdd);

            return cmd;
        }

        public static IDBCommand UpdateCardComplex(PublicID id, Serial serial, Int64 complex)
        {
            const string text = "UPDATE `card_{0:00}` SET complex=complex|@complex WHERE id=@id;";

            var cmd = CreateDBCommand(text, id);

            cmd.AddParameter("@id", serial.Value);
            cmd.AddParameter("@complex", complex);

            return cmd;
        }

        public static IDBCommand ReplaceCardComplex(PublicID id, Serial serial, Int64 complex)
        {
            return ReplaceCardComplex(id, serial.Value, complex);
        }

        public static IDBCommand ReplaceCardComplex(PublicID id, Int64 serial, Int64 complex)
        {
            const string text = "UPDATE `card_{0:00}` SET complex=@complex WHERE id=@id;";

            var cmd = CreateDBCommand(text, id);

            cmd.AddParameter("@id", serial);
            cmd.AddParameter("@complex", complex);

            return cmd;
        }

        public static IDBCommand DeleteCard(PublicID id, List<Int64> list)
        {
            var text = "DELETE FROM `card_{0:00}` WHERE id IN(" + string.Join(",", list) + ");";

            var cmd = CreateDBCommand(text, id);

            return cmd;
        }

        public static IDBCommand SellCard(PublicID id, int crystalDiff, int coinDiff, DateTime now)
        {
            const string text = "UPDATE `player_{0:00}` SET crystal=crystal+@crystal_diff, coin=coin+@coin_diff, last_play_time=@now WHERE player_id=@player_id;";

            var cmd = CreateDBCommand(text, id);

            cmd.AddParameter("@player_id", id.Serial);
            cmd.AddParameter("@crystal_diff", crystalDiff);
            cmd.AddParameter("@coin_diff", coinDiff);
            cmd.AddParameter("@now", now);

            return cmd;
        }

        public static IDBCommand EnhanceCard(PublicID id, Serial cardSerial, int diff, DateTime now)
        {
            const string text = @"UPDATE `player_{0:00}` SET crystal=crystal-@diff, last_play_time=@now WHERE player_id=@player_id;
                                  UPDATE `card_{0:00}` SET exp=exp+@diff WHERE id=@card_serial;";

            var cmd = CreateDBCommand(text, id);

            cmd.AddParameter("@player_id", id.Serial);
            cmd.AddParameter("@card_serial", cardSerial.Value);
            cmd.AddParameter("@diff", diff);
            cmd.AddParameter("@now", now);

            return cmd;
        }

        public static IDBCommand SelectCard(PublicID id, Serial serial)
        {
            const string text = "SELECT * FROM `card_{0:00}` WHERE id=@card_serial;";

            var cmd = CreateNoLockDBCommand(text, id);

            cmd.AddParameter("@card_serial", serial.Value);

            return cmd;
        }

        public static IDBCommand SelectCardEventCacheList(PublicID id)
        {
            return SelectAll(id, "card_event_cache");
        }
        public static IDBCommand InsertCardEventCache(PublicID id, Serial serial, int interactiveId)
        {
            return InsertCardEventCache(id, serial, interactiveId, 0);
        }
        public static IDBCommand InsertCardEventCache(PublicID id, Serial serial, int interactiveId, Int64 questId)
        {
            const string text = @"INSERT INTO `card_event_cache_{0:00}` (player_id, card_id, interactive_id, quest_id) VALUES (@player_id, @card_id, @interactive_id, @quest_id);
                                  SELECT CAST(LAST_INSERT_ID() AS SIGNED);";

            var cmd = CreateDBCommand(text, id);

            cmd.AddParameter("@player_id", id.Serial);
            cmd.AddParameter("@card_id", serial.Value);
            cmd.AddParameter("@interactive_id", interactiveId);
            cmd.AddParameter("@quest_id", questId);

            return cmd;
        }

        public static IDBCommand DeleteCardEventCache(PublicID id, Serial serial)
        {
            var text = "DELETE FROM `card_event_cache_{0:00}` WHERE card_id=@serial;";

            var cmd = CreateDBCommand(text, id);

            cmd.AddParameter("@serial", serial.Value);

            return cmd;
        }

        public static IDBCommand SelectGallery(PublicID id)
        {
            return SelectAll(id, "gallery");
        }
        public static IDBCommand InsertGallery(PublicID id, int cardId, int luck)
        {
            const string text = "CALL insert_gallery_{0:00}(@player_id, @card_id, @luck);";

            var cmd = CreateDBCommand(text, id);
            cmd.AddParameter("@player_id", id.Serial);
            cmd.AddParameter("@card_id", cardId);
            cmd.AddParameter("@luck", luck);

            return cmd;
        }
        #endregion

        #region Item

        public static IDBCommand SelectItemList(PublicID id)
        {
            return SelectAll(id, "item");
        }

        public static IDBCommand IncreaseItemCount(PublicID id, int itemId, int diff)
        {
            const string text = "CALL increase_item_{0:00}(@player_id, @item_id, @diff);";

            var cmd = CreateDBCommand(text, id);

            cmd.AddParameter("@player_id", id.Serial);

            cmd.AddParameter("@item_id", itemId);

            cmd.AddParameter("@diff", diff);

            return cmd;
        }

        public static IDBCommand DeleteItem(PublicID id, int itemId)
        {
            const string text = "DELETE FROM `item_{0:00}` WHERE player_id=@player_id AND item_id=@item_id;";

            var cmd = CreateDBCommand(text, id);

            cmd.AddParameter("@player_id", id.Serial);

            cmd.AddParameter("@item_id", itemId);

            return cmd;
        }

        #endregion

        #region Equipment

        public static IDBCommand SelectEquipmentList(PublicID id)
        {
            return SelectAll(id, "equipment");
        }

        public static IDBCommand InsertEquipment(PublicID id, int itemId, int hp, int atk, int rev, int chg, int effectId, int counter, int counterType)
        {
            const string text = @"INSERT INTO `equipment_{0:00}` (player_id, item_id, hp, atk, rev, chg, effect_id, counter, counter_type) VALUES (@player_id, @item_id, @hp, @atk, @rev, @chg, @effect_id, @counter, @counter_type);
                                  SELECT CAST(LAST_INSERT_ID() AS SIGNED);";

            var cmd = CreateDBCommand(text, id);

            cmd.AddParameter("@player_id", id.Serial);

            cmd.AddParameter("@item_id", itemId);

            cmd.AddParameter("@hp", hp);
            cmd.AddParameter("@atk", atk);
            cmd.AddParameter("@rev", rev);
            cmd.AddParameter("@chg", chg);
            cmd.AddParameter("@effect_id", effectId);
            cmd.AddParameter("@counter", counter);
            cmd.AddParameter("@counter_type", counterType);

            return cmd;
        }

        public static IDBCommand DeleteEquipment(PublicID id, List<Int64> list)
        {
            var text = "DELETE FROM `equipment_{0:00}` WHERE id IN(" + string.Join(",", list) + ");"; ;

            var cmd = CreateDBCommand(text, id);

            return cmd;
        }

        public static IDBCommand DeleteEquipment(PublicID id, Int64 serial)
        {
            const string text = "DELETE FROM `equipment_{0:00}` WHERE id=@id;"; ;

            var cmd = CreateDBCommand(text, id);

            cmd.AddParameter("@id", serial);

            return cmd;
        }

        public static IDBCommand UpdateEquipment(PublicID id, Int64 serial, Int64 cardSerial, int slot)
        {
            const string text = "UPDATE `equipment_{0:00}` SET card_id=@card_id, slot=@slot WHERE id=@id;";

            var cmd = CreateDBCommand(text, id);

            cmd.AddParameter("@id", serial);
            cmd.AddParameter("@card_id", cardSerial);
            cmd.AddParameter("@slot", slot);

            return cmd;
        }
        #endregion

        #region Purchase

        public static IDBCommand SelectBlackGem()
        {
            const string text = "SELECT * FROM `black_gem`;";

            return CreateNoLockDBCommand(text);
        }

        public static IDBCommand SelectGem(PublicID id)
        {
            const string text = "SELECT COALESCE(SUM(`add`), 0) FROM `gem_{0:00}` WHERE player_id=@player_id;";

            var cmd = CreateNoLockDBCommand(text, id);

            cmd.AddParameter("@player_id", id.Serial);

            return cmd;
        }
        public static IDBCommand SelectIAPGem(PublicID id)
        {
            const string text = "SELECT COALESCE(SUM(`add`), 0) FROM `gem_{0:00}` WHERE player_id=@player_id AND source=@source;";

            var cmd = CreateNoLockDBCommand(text, id);

            cmd.AddParameter("@player_id", id.Serial);

            cmd.AddParameter("@source", (byte)RewardSource.IAP);

            return cmd;
        }

        public static IDBCommand SelectPurchase(PublicID id)
        {
            return SelectAll(id, "purchase");
        }

        public static IDBCommand InsertPurchase(PublicID id, ArticleID articleId, int cost, int iapCost, DateTime now)
        {
            const string text = "INSERT INTO `purchase_{0:00}` (player_id, article_id, cost, iap_cost, timestamp) VALUES (@player_id, @article_id, @cost, @iap_cost, @timestamp);";

            var cmd = CreateDBCommand(text, id);

            cmd.AddParameter("@player_id", id.Serial);

            cmd.AddParameter("@article_id", (byte)articleId);

            cmd.AddParameter("@cost", cost);

            cmd.AddParameter("@iap_cost", iapCost);

            cmd.AddParameter("@timestamp", now);

            return cmd;
        }

        public static IDBCommand InsertGem(PublicID id, RewardSource source, int addValue, DateTime now, int addition = 0)
        {
            const string text = "INSERT INTO `gem_{0:00}` (player_id, source, `add`, timestamp, addition) VALUES (@player_id, @source, @add, @timestamp, @addition);";

            var cmd = CreateDBCommand(text, id);

            cmd.AddParameter("@player_id", id.Serial);

            cmd.AddParameter("@source", (byte)source);

            cmd.AddParameter("@add", addValue);

            cmd.AddParameter("@timestamp", now);

            cmd.AddParameter("@addition", addition);

            return cmd;
        }

        #endregion

        #region Quest

        public static IDBCommand DeleteOverdueQuestCache(PublicID id, Int64 lastSerial)
        {
            const string text = "DELETE FROM `quest_cache_{0:00}` WHERE player_id=@player_id AND id<@id;";

            var cmd = CreateDBCommand(text, id);

            cmd.AddParameter("@player_id", id.Serial);
            cmd.AddParameter("@id", lastSerial);

            return cmd;
        }

        public static IDBCommand SelectQuestCache(PublicID id)
        {
            return SelectAll(id, "quest_cache");
        }

        public static IDBCommand SelectQuestList(PublicID id)
        {
            return SelectAll(id, "quest");
        }

        public static IDBCommand UpdateQuestLastPlayTime(PublicID id, int questId, DateTime now)
        {
            return UpdateQuestLastPlayTime(id, questId, 0, now);
        }

        public static IDBCommand UpdateQuestLastPlayTime(PublicID id, int questId, int complex, DateTime now)
        {
            const string text = "CALL update_quest_{0:00}(@player_id, @quest_id, @complex, @now);";

            var cmd = CreateDBCommand(text, id);

            cmd.AddParameter("@player_id", id.Serial);
            cmd.AddParameter("@quest_id", questId);
            cmd.AddParameter("@complex", complex);
            cmd.AddParameter("@now", now);

            return cmd;
        }

        public static IDBCommand UpdateQuest(PublicID id, int questId, int complex)
        {
            const string text = "UPDATE `quest_{0:00}` SET complex=complex|@complex WHERE player_id=@player_id AND quest_id=@quest_id;";

            var cmd = CreateDBCommand(text, id);
            cmd.AddParameter("@player_id", id.Serial);
            cmd.AddParameter("@quest_id", questId);
            cmd.AddParameter("@complex", complex);

            return cmd;
        }

        public static IDBCommand BeginQuest(PublicID id, Mikan.CSAGA.Server.Tasks.BeginQuestObj obj, int complex, DateTime timestamp)
        {
            const string text = @"INSERT INTO `quest_cache_{0:00}` (`player_id`, `quest_id`, `data`, `complex`, `timestamp`) VALUES (@player_id, @quest_id, @data, @complex, @timestamp);
                                  SELECT * FROM `quest_cache_{0:00}` WHERE id=LAST_INSERT_ID();";

            var cmd = CreateDBCommand(text, id);

            cmd.AddParameter("@player_id", id.Serial);
            cmd.AddParameter("@quest_id", obj.Quest.QuestID);
            cmd.AddParameter("@data", obj.Data.Serialize());
            cmd.AddParameter("@complex", complex);
            cmd.AddParameter("@timestamp", timestamp);

            return cmd;
        }

        public static IDBCommand EndQuest(PublicID id, Int64 serial)
        {
            const string text = @"UPDATE `quest_cache_{0:00}` SET complex=complex|@complex WHERE id=@id AND player_id=@player_id;";

            var cmd = CreateDBCommand(text, id);
            cmd.AddParameter("@player_id", id.Serial);
            cmd.AddParameter("@id", serial);
            cmd.AddParameter("@complex", 1);

            return cmd;
        }

        public static IDBCommand EndQuest(PublicID id, int exp, int fp)
        {
            var text = "UPDATE `player_{0:00}` SET exp=exp+@exp, fp=fp+@fp WHERE player_id=@player_id;";

            var cmd = CreateDBCommand(text, id);

            cmd.AddParameter("@player_id", id.Serial);
            cmd.AddParameter("@exp", exp);
            cmd.AddParameter("@fp", fp);

            return cmd;
        }

        public static IDBCommand SelectLimitedQuestList(PublicID id)
        {
            return SelectAll(id, "limited_quest");
        }

        public static IDBCommand InsertLimitedQuest(PublicID id, int questId, DateTime endTime, int playCount = 0)
        {
            const string text = @"INSERT INTO `limited_quest_{0:00}`(player_id, quest_id, play_count, end_time) VALUES(@player_id, @quest_id, @play_count, @end_time);
                                  SELECT CAST(LAST_INSERT_ID() AS SIGNED);";

            var cmd = CreateDBCommand(text, id);
            cmd.AddParameter("@player_id", id.Serial);
            cmd.AddParameter("@quest_id", questId);
            cmd.AddParameter("@play_count", playCount);
            cmd.AddParameter("@end_time", endTime);

            return cmd;    
        }

        public static IDBCommand IncreaseLimitedQuestPlayCount(PublicID id, Int64 serial)
        {
            const string text = @"UPDATE `limited_quest_{0:00}` SET play_count=play_count+1 WHERE id=@id AND player_id=@player_id;";

            var cmd = CreateDBCommand(text, id);
            cmd.AddParameter("@player_id", id.Serial);
            cmd.AddParameter("@id", serial);

            return cmd;
        }

        public static IDBCommand ResetLimitedQuest(PublicID id, Int64 serial, DateTime endTime, int playCount = 0)
        {
            const string text = "UPDATE `limited_quest_{0:00}` SET play_count=@play_count, end_time=@end_time WHERE id=@id AND player_id=@player_id;";

            var cmd = CreateDBCommand(text, id);
            cmd.AddParameter("@player_id", id.Serial);
            cmd.AddParameter("@id", serial);
            cmd.AddParameter("@play_count", playCount);
            cmd.AddParameter("@end_time", endTime);

            return cmd;
        }

        #region Rescue

        public static IDBCommand SelectRescue(PublicID id)
        {
            const string text = "SELECT * FROM `rescue_{0:00}` WHERE `player_id`=@player_id;";

            var cmd = CreateNoLockDBCommand(text, id);

            cmd.AddParameter("@player_id", id.Serial);

            return cmd;
        }

        public static IDBCommand UpdateRescue(PublicID id, int count, DateTime updateTime)
        {
            const string text = "CALL update_rescue_{0:00}(@player_id, @count, @update_time);";

            var cmd = CreateDBCommand(text, id);

            cmd.AddParameter("@player_id", id.Serial);
            cmd.AddParameter("@count", count);
            cmd.AddParameter("@update_time", updateTime);

            return cmd;
        }

        #endregion

        #region Crusade

        public static IDBCommand SelectSharedQuest()
        {
            return SelectAll("shared_quest");
        }

        public static IDBCommand SelectSharedQuest(PublicID id)
        {
            const string text = "SELECT * FROM `shared_quest` WHERE group_id=@group_id AND player_id=@player_id;";

            var cmd = CreateNoLockDBCommand(text);

            cmd.AddParameter("@group_id", id.Group);
            cmd.AddParameter("@player_id", id.Serial);

            return cmd;
        }

        public static IDBCommand InsertSharedQuest(PublicID id, int questId, DateTime endTime)
        {
            const string text = @"INSERT INTO `shared_quest` (`group_id`, `player_id`, `quest_id`, `end_time`) VALUES (@group_id, @player_id, @quest_id, @end_time);
                                  SELECT CAST(LAST_INSERT_ID() AS SIGNED);";
            
            var cmd = CreateDBCommand(text);

            cmd.AddParameter("@group_id", id.Group);
            cmd.AddParameter("@player_id", id.Serial);
            cmd.AddParameter("@quest_id", questId);
            cmd.AddParameter("@end_time", endTime);

            return cmd;
        }

        public static IDBCommand SelectCrusade(PublicID id)
        {
            return SelectAll(id, "crusade");
        }

        public static IDBCommand InsertCrusade(PublicID id, int inital, DateTime currentTime)
        {
            const string text = @"INSERT INTO `crusade_{0:00}` (`player_id`, `point`, `update_time`) VALUES (@player_id, @point, @update_time);";

            var cmd = CreateDBCommand(text, id);

            cmd.AddParameter("@player_id", id.Serial);
            cmd.AddParameter("@point", inital);
            cmd.AddParameter("@update_time", currentTime);

            return cmd;
        }

        public static IDBCommand AddCrusadeDiscover(PublicID id, int add)
        {
            var text = "UPDATE `crusade_{0:00}` SET discover=discover+@add WHERE player_id=@player_id;";

            var cmd = CreateDBCommand(text, id);

            cmd.AddParameter("@player_id", id.Serial);
            cmd.AddParameter("@add", add);

            return cmd;
        }

        public static IDBCommand UpdateCrusade(PublicID id, int point, DateTime updateTime)
        {
            var text = "UPDATE `crusade_{0:00}` SET point=@point, update_time=@update_time WHERE player_id=@player_id;";

            var cmd = CreateDBCommand(text, id);

            cmd.AddParameter("@player_id", id.Serial);
            cmd.AddParameter("@point", point);
            cmd.AddParameter("@update_time", updateTime);

            return cmd;
        }

        public static IDBCommand UpdateCrusadePlayCount(PublicID id, Int64 serial, int add)
        {
            var text = "UPDATE `shared_quest` SET play_count=play_count+@add WHERE `player_id`=@player_id AND `id`=@id;";

            var cmd = CreateDBCommand(text, id);

            cmd.AddParameter("@player_id", id.Serial);
            cmd.AddParameter("@id", serial);
            cmd.AddParameter("@add", add);

            return cmd;
        }

        public static IDBCommand SelectCrusadeCache(PublicID id, bool isSelf)
        {
            const string text = "SELECT * FROM `crusade_cache_{0:00}` WHERE `player_id`=@player_id AND is_self=@is_self;";

            var cmd = CreateNoLockDBCommand(text, id);

            cmd.AddParameter("@player_id", id.Serial);
            cmd.AddParameter("@is_self", isSelf ? 1 : 0);

            return cmd;
        }

        public static IDBCommand InsertCrusadeCache(PublicID id, Int64 questId, bool isSelf)
        {
            const string text = @"INSERT INTO `crusade_cache_{0:00}` (`player_id`, `quest_id`, `is_self`) VALUES (@player_id, @quest_id, @is_self);";

            var cmd = CreateDBCommand(text, id);

            cmd.AddParameter("@player_id", id.Serial);
            cmd.AddParameter("@quest_id", questId);
            cmd.AddParameter("@is_self", isSelf);

            return cmd;
        }

        public static IDBCommand DeleteCrusadeCache(PublicID id, List<Int64> list)
        {
            var _list = string.Join(",", list);

            var text = "DELETE FROM `crusade_cache_{0:00}` WHERE `player_id`=@player_id AND `id` IN(" + _list + ");";

            var cmd = CreateDBCommand(text, id);

            cmd.AddParameter("@player_id", id.Serial);

            return cmd;
        }

        public static IDBCommand DeleteCrusade(int group)
        {
            string text = @"DELETE FROM `crusade_{0:00}`;
                            DELETE FROM `crusade_cache_{0:00}`;";

            var cmd = CreateDBCommand(text, group);

            return cmd;
        }


        public static IDBCommand DeleteSharedQuest()
        {
            string text = "DELETE FROM `shared_quest`;";

            var cmd = CreateDBCommand(text);

            return cmd;
        }

        #endregion

        #endregion

        #region Gift

        public static IDBCommand InsertGift(PublicID id, RewardSource source, string description, DropType type, int value1, int value2)
        {
            return InsertGift(id, source, description, type, value1, value2, Global.CurrentTime, DateTime.MaxValue);
        }

        public static IDBCommand InsertGift(PublicID id, RewardSource source, string description, DropType type, int value1, int value2, DateTime beginTime, DateTime endTime)
        {
            const string text = @"INSERT INTO `gift_{0:00}`(player_id, description, source, type, value_1, value_2, begin_time, end_time) VALUES (@player_id,@description,@source,@type,@value_1,@value_2,@begin_time,@end_time);
                                  SELECT CAST(LAST_INSERT_ID() AS SIGNED);";

            var cmd = CreateDBCommand(text, id);

            cmd.AddParameter("@player_id", id.Serial);
            cmd.AddParameter("@description", description);
            cmd.AddParameter("@source", (byte)source);
            cmd.AddParameter("@type", (int)type);
            cmd.AddParameter("@value_1", value1);
            cmd.AddParameter("@value_2", value2);
            cmd.AddParameter("@begin_time", beginTime);
            cmd.AddParameter("@end_time", endTime);

            return cmd;
        }

        public static IDBCommand SelectGiftList(PublicID id)
        {
            const string text = "SELECT * FROM `gift_{0:00}` WHERE player_id=@player_id ORDER BY `id` LIMIT 51;";

            var cmd = CreateDBCommand(text, id);

            cmd.AddParameter("@player_id", id.Serial);

            return cmd;
        }

        public static IDBCommand DeleteGift(PublicID id, Int64 serial)
        {
            const string text = "DELETE FROM `gift_{0:00}` WHERE id=@id;";

            var cmd = CreateDBCommand(text, id);

            cmd.AddParameter("@id", serial);

            return cmd;
        }

        #endregion

        #region Trophy

        public static IDBCommand SelectTrophyList(PublicID id)
        {
            return SelectAll(id, "trophy");
        }

        public static IDBCommand SelectSimpleTrophyList(PublicID id)
        {
            return SelectAll(id, "simple_trophy");
        }

        public static IDBCommand UpdateTrophy(PublicID id, TrophyType type, int group, int current, int progress, DateTime now)
        {
            if (type == TrophyType.Immediately || type == TrophyType.CreatePlayer)
            {
                const string text = "INSERT INTO `simple_trophy_{0:00}`(player_id, trophy_id) VALUES (@player_id, @trophy_id);";

                var cmd = CreateDBCommand(text, id);
                cmd.AddParameter("@player_id", id.Serial);
                cmd.AddParameter("@trophy_id", current);

                return cmd;
            }
            else
            {
                const string text = "CALL update_trophy_{0:00}(@player_id, @group, @current, @progress, @update_time);";

                var cmd = CreateDBCommand(text, id);
                cmd.AddParameter("@player_id", id.Serial);
                cmd.AddParameter("@group", group);
                cmd.AddParameter("@current", current);
                cmd.AddParameter("@progress", progress);
                cmd.AddParameter("@update_time", now);

                return cmd;
            }


        }

        #endregion

        #region Daily

        public static IDBCommand SelectSignature()
        {
            return SelectAll("signature");
        }

        public static IDBCommand InsertSignature(SignatureType type, DateTime timestamp)
        {
            const string text = @"INSERT INTO `signature`(type, timestamp) VALUES (@type, @timestamp);
                                  SELECT CAST(LAST_INSERT_ID() AS SIGNED);";

            var cmd = CreateDBCommand(text);

            cmd.AddParameter("@type", (int)type);
            cmd.AddParameter("@timestamp", timestamp);

            return cmd;
        }

        public static IDBCommand SelectDataCache()
        {
            return SelectAll("data_cache");
        }

        public static IDBCommand InsertDataCache(int signature, int dataId, int value)
        {
            const string text = "INSERT INTO `data_cache`(signature, data_id, value) VALUES (@signature,@data_id,@value);";
            
            var cmd = CreateDBCommand(text);

            cmd.AddParameter("@signature", signature);
            cmd.AddParameter("@data_id", dataId);
            cmd.AddParameter("@value", value);

            return cmd;
        }

        public static IDBCommand SelectDaily(PublicID id)
        {
            const string text = "SELECT * FROM `daily_{0:00}` WHERE `player_id`=@player_id;";

            var cmd = CreateNoLockDBCommand(text, id);

            cmd.AddParameter("@player_id", id.Serial);

            return cmd;
        }

        public static IDBCommand ResetDaily(PublicID id, Int64 seed, DateTime updateTime)
        {
            const string text = "CALL reset_daily_{0:00}(@player_id, @seed, @update_time);";

            var cmd = CreateNoLockDBCommand(text, id);

            cmd.AddParameter("@player_id", id.Serial);
            cmd.AddParameter("@seed", seed);
            cmd.AddParameter("@update_time", updateTime);

            return cmd;
        }

        public static IDBCommand UpdateDaily(PublicID id, int state, int consumed, int cached)
        {
            const string text = "UPDATE `daily_{0:00}` SET state=@state, consumed=consumed+@consumed, cached=@cached  WHERE player_id=@player_id;";

            var cmd = CreateDBCommand(text, id);

            cmd.AddParameter("@player_id", id.Serial);
            cmd.AddParameter("@state", state);
            cmd.AddParameter("@consumed", consumed);
            cmd.AddParameter("@cached", cached);

            return cmd;
        }

        public static IDBCommand AddDailyCount(PublicID id, int add)
        {
            const string text = "UPDATE `daily_{0:00}` SET `add`=`add`+@add  WHERE player_id=@player_id;";

            var cmd = CreateDBCommand(text, id);

            cmd.AddParameter("@player_id", id.Serial);
            cmd.AddParameter("@add", add);

            return cmd;
        }

        #endregion

        #region RandomShop

        public static IDBCommand SelectRandomShop(PublicID id)
        {
            return SelectAll(id, "random_shop");
        }

        public static IDBCommand InsertRandomShop(PublicID id, Int64 seed, DateTime now)
        {
            const string text = "INSERT INTO `random_shop_{0:00}`(player_id, seed, update_time) VALUES (@player_id,@seed,@update_time);";

            var cmd = CreateDBCommand(text, id);

            cmd.AddParameter("@player_id", id.Serial);
            cmd.AddParameter("@seed", seed);
            cmd.AddParameter("@update_time", now);

            return cmd;
        }

        public static IDBCommand UpdateRandomShopState(PublicID id, int state)
        {
            const string text = "UPDATE `random_shop_{0:00}` SET state=@state WHERE player_id=@player_id;";

            var cmd = CreateDBCommand(text, id);

            cmd.AddParameter("@player_id", id.Serial);
            cmd.AddParameter("@state", state);

            return cmd;
        }

        public static IDBCommand UpdateRandomShopSeed(PublicID id, Int64 seed, DateTime now)
        {
            const string text = "UPDATE `random_shop_{0:00}` SET seed=@seed, state=0, update_time=@update_time WHERE player_id=@player_id;";

            var cmd = CreateDBCommand(text, id);

            cmd.AddParameter("@player_id", id.Serial);
            cmd.AddParameter("@seed", seed);
            cmd.AddParameter("@update_time", now);

            return cmd;
        }
        #endregion

        #region EventSchedule
        public static IDBCommand SelectEventSchedule()
        {
            return SelectAll("event_schedule");
        }

        public static IDBCommand InsertEventSchedule(int dataId, DateTime beginTime)
        {
            const string text = @"INSERT INTO `event_schedule`(data_id, begin_time) VALUES(@data_id, @begin_time);
                                  SELECT CAST(LAST_INSERT_ID() AS SIGNED);";

            var cmd = CreateDBCommand(text);
            cmd.AddParameter("@data_id", dataId);
            cmd.AddParameter("@begin_time", beginTime);

            return cmd;    
        }

        public static IDBCommand UpdateEventScheduleComplex(Int64 serial, int complex)
        {
            const string text = "UPDATE `event_schedule` SET complex=complex|@complex WHERE `id`=@id;";

            var cmd = CreateDBCommand(text);
            cmd.AddParameter("@id", serial);
            cmd.AddParameter("@complex", complex);

            return cmd;
        }

        public static IDBCommand SelectEventPoint(int group, Int64 minSerial)
        {
            const string text = "SELECT * FROM `event_point_{0:00}` WHERE `event_id`>=@event_id;";

            var cmd = CreateNoLockDBCommand(text, group);

            cmd.AddParameter("@event_id", minSerial);

            return cmd;
        }

        public static IDBCommand SelectEventPoint(PublicID id, Int64 minSerial)
        {
            const string text = "SELECT * FROM `event_point_{0:00}` WHERE player_id=@player_id AND `event_id`>=@event_id;";

            var cmd = CreateNoLockDBCommand(text, id);

            cmd.AddParameter("@player_id", id.Serial);
            cmd.AddParameter("@event_id", minSerial);

            return cmd;
        }

        public static IDBCommand UpdateEventPoint(PublicID id, Int64 eventId, int add, int cardId)
        {
            const string text = "CALL update_event_point_{0:00}(@player_id, @event_id, @add, @card_id);";

            var cmd = CreateDBCommand(text, id);
            cmd.AddParameter("@player_id", id.Serial);
            cmd.AddParameter("@event_id", eventId);
            cmd.AddParameter("@add", add);
            cmd.AddParameter("@card_id", cardId);

            return cmd;
        }

        public static IDBCommand SetEventPoint(PublicID id, Int64 eventId, int point, int cardId)
        {
            const string text = "CALL set_event_point_{0:00}(@player_id, @event_id, @point, @card_id);";

            var cmd = CreateDBCommand(text, id);
            cmd.AddParameter("@player_id", id.Serial);
            cmd.AddParameter("@event_id", eventId);
            cmd.AddParameter("@point", point);
            cmd.AddParameter("@card_id", cardId);

            return cmd;
        }

        public static IDBCommand UpdateEventPointComplex(PublicID id, Int64 eventId, int complex)
        {
            const string text = "UPDATE `event_point_{0:00}` SET complex=complex|@complex WHERE player_id=@player_id AND event_id=@event_id;";

            var cmd = CreateDBCommand(text, id);
            cmd.AddParameter("@player_id", id.Serial);
            cmd.AddParameter("@event_id", eventId);
            cmd.AddParameter("@complex", complex);

            return cmd;
        }

        public static IDBCommand SelectDailyEventPoint(PublicID id)
        {
            return SelectAll(id, "daily_ep");
        }

        public static IDBCommand InsertDailyEventPoint(PublicID id, int value, DateTime updateTime)
        {
            const string text = @"INSERT INTO `daily_ep_{0:00}`(`player_id`, `point`, `update_time`) VALUES(@player_id, @point, @update_time);
                                  SELECT CAST(LAST_INSERT_ID() AS SIGNED);";

            var cmd = CreateDBCommand(text, id);
            cmd.AddParameter("@player_id", id.Serial);
            cmd.AddParameter("@point", value);
            cmd.AddParameter("@update_time", updateTime);

            return cmd;
        }

        public static IDBCommand UpdateDailyEventPoint(PublicID id, Int64 serial, int value, DateTime updateTime)
        {
            const string text = "UPDATE `daily_ep_{0:00}` SET `point`=@value, `update_time`=@update_time WHERE `id`=@id AND `player_id`=@player_id;";

            var cmd = CreateDBCommand(text, id);
            cmd.AddParameter("@player_id", id.Serial);
            cmd.AddParameter("@id", serial);
            cmd.AddParameter("@value", value);
            cmd.AddParameter("@update_time", updateTime);

            return cmd;
        }

        #region Infinity

        public static IDBCommand SelectInfinity(PublicID id)
        {
            return SelectAll(id, "infinity");
        }

        public static IDBCommand InsertInfinity(PublicID id, Int64 eventId, int point, DateTime updateTime)
        {
            const string text = @"INSERT INTO `infinity_{0:00}`(`player_id`, `event_id`, `point`, `update_time`) VALUES(@player_id, @event_id, @point, @update_time);
                                  SELECT CAST(LAST_INSERT_ID() AS SIGNED);";

            var cmd = CreateDBCommand(text, id);
            cmd.AddParameter("@player_id", id.Serial);
            cmd.AddParameter("@event_id", eventId);
            cmd.AddParameter("@point", point);
            cmd.AddParameter("@update_time", updateTime);

            return cmd;  

        }
        public static IDBCommand UpdateInfinity(PublicID id, Int64 serial, int point, DateTime updateTime)
        {
            const string text = "UPDATE `infinity_{0:00}` SET point=@point, update_time=@update_time WHERE id=@id;";

            var cmd = CreateDBCommand(text, id);
            cmd.AddParameter("@player_id", id.Serial);
            cmd.AddParameter("@id", serial);
            cmd.AddParameter("@point", point);
            cmd.AddParameter("@update_time", updateTime);

            return cmd;
        }

        #endregion

        #endregion

        #region IAP

        public static IDBCommand SelectIAPReceipt(string table, Int64 hash)
        {
            const string text = "SELECT `id`, `group_id`, `player_id`, `hash`, `order_id`, `product_id`, `gem` FROM `{0}` WHERE `hash`=@hash;";
            
            var cmd = CreateNoLockDBCommand(string.Format(text, table));

            cmd.AddParameter("@hash", hash);

            return cmd;
        }

        public static IDBCommand InsertIAPReceipt(string table, PublicID id, int productId, string orderId, Int64 hash, string token, int gem, int bonus, DateTime timestamp)
        {
            const string text = @"INSERT INTO `{0}`(group_id, player_id, order_id, hash, product_id, token, gem, bonus, timestamp) VALUES(@group_id, @player_id, @order_id, @hash, @product_id, @token, @gem, @bonus, @timestamp);
                                  SELECT CAST(LAST_INSERT_ID() AS SIGNED);";

            var cmd = CreateDBCommand(string.Format(text, table));
            cmd.AddParameter("@group_id", id.Group);
            cmd.AddParameter("@player_id", id.Serial);
            cmd.AddParameter("@order_id", orderId);
            cmd.AddParameter("@hash", hash);
            cmd.AddParameter("@product_id", productId);
            cmd.AddParameter("@token", token);
            cmd.AddParameter("@gem", gem);
            cmd.AddParameter("@bonus", bonus);
            cmd.AddParameter("@timestamp", timestamp);

            return cmd;
        }

        public static IDBCommand SelectGooglePlayReceipt(Int64 hash)
        {
            return SelectIAPReceipt("google_play_receipt", hash);
        }

        public static IDBCommand SelectMyCardReceipt(Int64 hash)
        {
            return SelectIAPReceipt("mycard_receipt", hash);
        }

        public static IDBCommand InsertGooglePlayReceipt(PublicID id, int productId, string orderId, Int64 hash, string token, int gem, int bonus, DateTime timestamp)
        {
            return InsertIAPReceipt("google_play_receipt", id, productId, orderId, hash, token, gem, bonus, timestamp);
        }

        public static IDBCommand InsertMyCardReceipt(PublicID id, int productId, string orderId, Int64 hash, string token, int gem, int bonus, DateTime timestamp)
        {
            return InsertIAPReceipt("mycard_receipt", id, productId, orderId, hash, token, gem, bonus, timestamp);
        }

        public static IDBCommand SelectIOSReceipt(Int64 hash)
        {
            return SelectIAPReceipt("ios_receipt", hash);
        }

        public static IDBCommand InsertIOSReceipt(PublicID id, int productId, string orderId, Int64 hash, string token, int gem, int bonus, DateTime timestamp)
        {
            return InsertIAPReceipt("ios_receipt", id, productId, orderId, hash, token, gem, bonus, timestamp);
        }

        public static IDBCommand SelectIAPProduct()
        {
            return SelectAll("iap_product");
        }

        public static IDBCommand SelectIAPHistory(PublicID id)
        {
            return SelectAll(id, "iap_history");
        }

        public static IDBCommand InsertIAPProduct(int id, string productId, int gem, int bonus, string name, string price, int dollars)
        {
            const string text = @"INSERT INTO `iap_product`(id, product_id, gem, bonus, name, price, dollars) VALUES(@id, @product_id, @gem, @bonus, @name, @price, @dollars);";

            var cmd = CreateDBCommand(text);
            cmd.AddParameter("@id", id);
            cmd.AddParameter("@product_id", productId);
            cmd.AddParameter("@gem", gem);
            cmd.AddParameter("@bonus", bonus);
            cmd.AddParameter("@name", name);
            cmd.AddParameter("@price", price);
            cmd.AddParameter("@dollars", dollars);

            return cmd;    
        }

        public static IDBCommand UpdateIAPHistory(PublicID id, int productId, Int64 receiptId, DateTime updateTime)
        {
            const string text = "CALL update_iap_history_{0:00}(@player_id, @product_id, @receipt_id, @update_time);";

            var cmd = CreateDBCommand(text, id);
            cmd.AddParameter("@player_id", id.Serial);
            cmd.AddParameter("@product_id", productId);
            cmd.AddParameter("@receipt_id", receiptId);
            cmd.AddParameter("@update_time", updateTime);

            return cmd;
        }

        #region MyCard

        public static IDBCommand InsertMyCardTransaction(PublicID id, int productId, DateTime timesetamp)
        {
            const string text = @"INSERT INTO `mycard_transaction`(group_id, player_id, product_id, timestamp) VALUES(@group_id, @player_id, @product_id, @timestamp);
                                  SELECT CAST(LAST_INSERT_ID() AS SIGNED);";

            var cmd = CreateDBCommand(text);

            cmd.AddParameter("@group_id", id.Group);
            cmd.AddParameter("@player_id", id.Serial);
            cmd.AddParameter("@product_id", productId);
            cmd.AddParameter("@timestamp", timesetamp);

            return cmd;  
        }

        public static IDBCommand UpdateMyCardTransaction(Int64 serial, string authCode, string tradeSeq, int complex)
        {
            const string text = "UPDATE `mycard_transaction` SET auth_code=@auth_code, trade_seq=@trade_seq, complex=@complex WHERE id=@serial;";

            var cmd = CreateDBCommand(text);
            cmd.AddParameter("@serial", serial);
            cmd.AddParameter("@auth_code", authCode);
            cmd.AddParameter("@trade_seq", tradeSeq);
            cmd.AddParameter("@complex", complex);

            return cmd;
        }

        public static IDBCommand UpdateMyCardTransaction(Int64 serial, int complex)
        {
            const string text = "UPDATE `mycard_transaction` SET complex=complex|@complex WHERE id=@serial;";

            var cmd = CreateDBCommand(text);
            cmd.AddParameter("@serial", serial);
            cmd.AddParameter("@complex", complex);

            return cmd;
        }

        public static IDBCommand SelectMyCardTransaction(Int64 serial)
        {
            const string text = "SELECT * FROM `mycard_transaction` WHERE `id`=@serial;";

            var cmd = CreateNoLockDBCommand(text);

            cmd.AddParameter("@serial", serial);

            return cmd;
        }

        public static IDBCommand SelectMyCardTransaction(PublicID id, Int64 serial)
        {
            const string text = "SELECT * FROM `mycard_transaction` WHERE `id`=@serial AND `group_id`=@group_id AND player_id=@player_id;";

            var cmd = CreateNoLockDBCommand(text);

            cmd.AddParameter("@serial", serial);
            cmd.AddParameter("@group_id", id.Group);
            cmd.AddParameter("@player_id", id.Serial);

            return cmd;
        }

        public static IDBCommand InsertMyCardHistory(PublicID id, Int64 transId, string tradeSeq, Mikan.Server.IAP.MyCard.ReceiptData receipt, DateTime timestamp)
        {
            const string text = @"INSERT INTO `mycard_history`(`transaction_id`, `group_id`, `player_id`, `return_code`, `return_msg`, `pay_result`, `fac_trade_seq`, `payment_type`, `amount`, `currency`, `trade_seq`, `mycard_trade_no`, `mycard_type`, `promo_code`, `serial_id`, `timestamp`) 
                                  VALUES(@transaction_id, @group_id, @player_id, @return_code, @return_msg, @pay_result, @fac_trade_seq, @payment_type, @amount, @currency, @trade_seq, @mycard_trade_no, @mycard_type, @promo_code, @serial_id, @timestamp);";

            var cmd = CreateDBCommand(text);
            cmd.AddParameter("@group_id", id.Group);
            cmd.AddParameter("@player_id", id.Serial);
            cmd.AddParameter("@transaction_id", transId);
            cmd.AddParameter("@return_code", receipt.ReturnCode);
            cmd.AddParameter("@return_msg", receipt.ReturnMsg);
            cmd.AddParameter("@pay_result", receipt.PayResult);
            cmd.AddParameter("@fac_trade_seq", receipt.FacTradeSeq);
            cmd.AddParameter("@payment_type", receipt.PaymentType);
            cmd.AddParameter("@amount", receipt.Amount);
            cmd.AddParameter("@currency", receipt.Currency);
            cmd.AddParameter("@trade_seq", tradeSeq);
            cmd.AddParameter("@mycard_trade_no", receipt.MyCardTradeNo);
            cmd.AddParameter("@mycard_type", receipt.MyCardType);
            cmd.AddParameter("@promo_code", receipt.PromoCode);
            cmd.AddParameter("@serial_id", receipt.SerialId);
            cmd.AddParameter("@timestamp", timestamp);
            return cmd;
        }

        public static IDBCommand SelectMyCardHistory(DateTime beginTime, DateTime endTime)
        {
            const string text = "SELECT * FROM `mycard_history` WHERE `timestamp`>=@begin_time AND `timestamp`<=@end_time;";

            var cmd = CreateNoLockDBCommand(text);

            cmd.AddParameter("@begin_time", beginTime);
            cmd.AddParameter("@end_time", endTime);

            return cmd;
        }

        public static IDBCommand SelectMyCardHistory(string tradeNo)
        {
            const string text = "SELECT * FROM `mycard_history` WHERE `mycard_trade_no`=@mycard_trade_no;";

            var cmd = CreateNoLockDBCommand(text);

            cmd.AddParameter("@mycard_trade_no", tradeNo);

            return cmd;
        }

        #endregion

        #endregion

        #region CoMission

        public static IDBCommand SelectCoMission(DateTime begintime)
        {
            const string text = "SELECT * FROM `co_mission` WHERE `begin_time`>@begin_time;";

            var cmd = CreateNoLockDBCommand(text);

            cmd.AddParameter("@begin_time", begintime);

            return cmd;
        }

        public static IDBCommand InsertCoMission(Int64 seed, int complex, DateTime beginTime)
        {
            const string text = @"INSERT INTO `co_mission`(seed, complex, begin_time) VALUES(@seed, @complex, @begin_time);
                                  SELECT CAST(LAST_INSERT_ID() AS SIGNED);";

            var cmd = CreateDBCommand(text);
            cmd.AddParameter("@seed", seed);
            cmd.AddParameter("@complex", complex);
            cmd.AddParameter("@begin_time", beginTime);

            return cmd;    
        }

        public static IDBCommand SumCoMissionState(int groupId, Int64 serial)
        {
            const string text = @"SELECT COALESCE(SUM(`subject_1`)) AS `subject_1`,
                                         COALESCE(SUM(`subject_2`)) AS `subject_2`,
                                         COALESCE(SUM(`subject_3`)) AS `subject_3`,
                                         COALESCE(SUM(`subject_4`)) AS `subject_4`,
                                         COALESCE(SUM(`subject_5`)) AS `subject_5`
                                  FROM `co_mission_state_{0:00}` WHERE `mission_id`=@serial;";

            var cmd = CreateNoLockDBCommand(text, groupId);

            cmd.AddParameter("@serial", serial);

            return cmd;
        }

        public static IDBCommand SelectCoMissionState(PublicID id, Int64 from)
        {
            const string text = "SELECT * FROM `co_mission_state_{0:00}` WHERE `player_id`=@player_id AND `mission_id`>=@from;";

            var cmd = CreateNoLockDBCommand(text, id);

            cmd.AddParameter("@player_id", id.Serial);
            cmd.AddParameter("@from", from);

            return cmd;
        }

        public static IDBCommand UpdateCoMissionState(PublicID id, Int64 missionId, int[] subjects)
        {
            const string text = "CALL update_co_mission_state_{0:00}(@player_id, @mission_id, @subject_1, @subject_2, @subject_3, @subject_4, @subject_5);";

            var cmd = CreateDBCommand(text, id);
            cmd.AddParameter("@player_id", id.Serial);
            cmd.AddParameter("@mission_id", missionId);
            for (int i = 0; i < 5; ++i) cmd.AddParameter("@subject_" + (i + 1).ToString(), subjects[i]);
 
            return cmd;
        }

        public static IDBCommand UpdateCoMissionState(PublicID id, Int64 missionId, int complex)
        {
            const string text = "UPDATE `co_mission_state_{0:00}` SET complex=@complex WHERE player_id=@player_id AND mission_id=@mission_id;";

            var cmd = CreateDBCommand(text, id);
            cmd.AddParameter("@player_id", id.Serial);
            cmd.AddParameter("@mission_id", missionId);
            cmd.AddParameter("@complex", complex);

            return cmd;
        }

        #endregion

        #region Security

        public static IDBCommand InsertBlack(PublicID id, string platform, string appVer, string signature, DateTime timestamp)
        {
            const string text = @"INSERT INTO `black`(`group_id`, `player_id`, `platform`, `app_ver`, `signature`, `timestamp`) VALUES(@group_id, @player_id, @platform, @app_ver, @signature, @timestamp);
                                  SELECT CAST(LAST_INSERT_ID() AS SIGNED);";

            var cmd = CreateDBCommand(text);
            cmd.AddParameter("@group_id", id.Group);
            cmd.AddParameter("@player_id", id.Serial);
            cmd.AddParameter("@platform", platform);
            cmd.AddParameter("@app_ver", appVer);
            cmd.AddParameter("@signature", signature);
            cmd.AddParameter("@timestamp", timestamp);

            return cmd;  
        }

        #endregion

        #region MyLord

        public static IDBCommand SelectMyLord(PublicID id)
        {
            return SelectAll(id, "mylord");
        }

        public static IDBCommand InsertMyLord(PublicID id)
        {
            const string text = @"INSERT INTO `mylord_{0:00}`(player_id) VALUES(@player_id);";

            var cmd = CreateDBCommand(text, id);
            cmd.AddParameter("@player_id", id.Serial);

            return cmd;   
        }

        public static IDBCommand UpdateMyLordRescueMiss(PublicID id, int rescueMiss, int rescueMissMax)
        {
            const string text = "UPDATE `mylord_{0:00}` SET rescue_miss=@rescue_miss, max_rescue_miss=@max_rescue_miss WHERE player_id=@player_id;";

            var cmd = CreateDBCommand(text, id);

            cmd.AddParameter("@player_id", id.Serial);
            cmd.AddParameter("@rescue_miss", rescueMiss);
            cmd.AddParameter("@max_rescue_miss", rescueMissMax);

            return cmd;
        }

        public static IDBCommand UpdateMyLordGachaMiss(PublicID id, int gachaMiss, int gachaMissMax)
        {
            const string text = "UPDATE `mylord_{0:00}` SET gacha_miss=@gacha_miss, max_gacha_miss=@max_gacha_miss WHERE player_id=@player_id;";

            var cmd = CreateDBCommand(text, id);

            cmd.AddParameter("@player_id", id.Serial);
            cmd.AddParameter("@gacha_miss", gachaMiss);
            cmd.AddParameter("@max_gacha_miss", gachaMissMax);

            return cmd;
        }

        #endregion

        #region Social

        public static IDBCommand SelectFellowList(PublicID id)
        {
            return SelectAll(id, "fellow");
        }
        public static IDBCommand SelectFellow(PublicID id, PublicID fellow)
        {
            const string text = "SELECT * FROM `fellow_{0:00}` WHERE `player_id`=@player_id AND fellow_group=@fellow_group AND fellow_id=@fellow_id;";

            var cmd = CreateNoLockDBCommand(text, id);

            cmd.AddParameter("@player_id", id.Serial);
            cmd.AddParameter("@fellow_group", fellow.Group);
            cmd.AddParameter("@fellow_id", fellow.Serial);

            return cmd;
        }

        public static IDBCommand DeleteFellow(PublicID id, PublicID fellow, bool foreign)
        {
            const string text = "UPDATE `fellow_{0:00}` SET complex=complex&@complex WHERE player_id=@player_id AND fellow_group=@fellow_group AND fellow_id=@fellow_id;";

            var cmd = CreateDBCommand(text, id);

            cmd.AddParameter("@player_id", id.Serial);
            cmd.AddParameter("@fellow_group", fellow.Group);
            cmd.AddParameter("@fellow_id", fellow.Serial);

            if(foreign)
                cmd.AddParameter("@complex", 0x0000FFFF);
            else
                cmd.AddParameter("@complex", 0xFFFF0000);

            return cmd;
        }

        public static IDBCommand UpdateFellow(PublicID id, PublicID fellow, int complex, DateTime updateTime, bool foreign)
        {
            const string text1 = "CALL update_fellow_{0:00}(@player_id, @fellow_group, @fellow_id, @complex, @update_time);";
            const string text2 = "CALL update_fellow_foreign_{0:00}(@player_id, @fellow_group, @fellow_id, @complex, @update_time);";

            var cmd = CreateDBCommand(foreign ? text2 : text1, id);

            cmd.AddParameter("@player_id", id.Serial);
            cmd.AddParameter("@fellow_group", fellow.Group);
            cmd.AddParameter("@fellow_id", fellow.Serial);
            cmd.AddParameter("@complex", complex);
            cmd.AddParameter("@update_time", updateTime);

            return cmd;
        }

        public static IDBCommand SelectSnapshot(PublicID id)
        {
            const string text = "SELECT * FROM `snapshot_{0:00}` WHERE `player_id`=@player_id;";

            var cmd = CreateNoLockDBCommand(text, id);

            cmd.AddParameter("@player_id", id.Serial);

            return cmd;
        }

        public static IDBCommand UpdateSnapshot(PublicID id, DateTime updateTime)
        {
            const string text = "UPDATE `snapshot_{0:00}` SET update_time=@update_time WHERE player_id=@player_id;";

            var cmd = CreateDBCommand(text, id);

            cmd.AddParameter("@player_id", id.Serial);
            cmd.AddParameter("@update_time", updateTime);

            return cmd;
        }

        public static IDBCommand UpdateSnapshot(PublicID id, string data, DateTime updateTime)
        {
            const string text = "CALL update_snapshot_{0:00}(@player_id, @data, @update_time);";
           

            var cmd = CreateDBCommand(text, id);

            cmd.AddParameter("@player_id", id.Serial);
            cmd.AddParameter("@data", data);
            cmd.AddParameter("@update_time", updateTime);

            return cmd;
        }

        public static IDBCommand SelectSnapshot(PublicID id, int count)
        {
            const string text = "SELECT * FROM `snapshot_{0:00}` WHERE `player_id`!=@player_id ORDER BY `update_time` DESC LIMIT @count;";

            var cmd = CreateNoLockDBCommand(text, id);

            cmd.AddParameter("@player_id", id.Serial);
            cmd.AddParameter("@count", count);

            return cmd;
        }

        public static IDBCommand SelectSocialPurchaseList(PublicID id)
        {
            return SelectAll(id, "social_purchase");
        }

        public static IDBCommand InsertSocialPurchase(PublicID id, int articleId, int cost, DateTime now)
        {
            const string text = @"INSERT INTO `social_purchase_{0:00}`(`player_id`, `article_id`, `cost`, `timestamp`) VALUES(@player_id, @article_id, @cost, @timestamp);";

            var cmd = CreateDBCommand(text, id);
            cmd.AddParameter("@player_id", id.Serial);
            cmd.AddParameter("@article_id", articleId);
            cmd.AddParameter("@cost", cost);
            cmd.AddParameter("@timestamp", now);

            return cmd;   
        }

        #endregion

        #region Faith

        public static IDBCommand SelectFaithList(PublicID id)
        {
            return SelectAll(id, "faith");
        }

        public static IDBCommand DebugDeleteFaith(PublicID id)
        {
            const string text = "DELETE FROM `faith_{0:00}` WHERE player_id=@id;";

            var cmd = CreateDBCommand(text, id);

            cmd.AddParameter("@id", id.Serial);

            return cmd;

        }
        public static IDBCommand DeleteFaith(PublicID id, Int64 serial)
        {
            const string text = "DELETE FROM `faith_{0:00}` WHERE id=@id;";

            var cmd = CreateDBCommand(text, id);

            cmd.AddParameter("@id", serial);

            return cmd;
        }

        public static IDBCommand InsertFaith(PublicID id, int index, int dataRef, int result, DateTime endTime)
        {
            const string text = @"INSERT INTO `faith_{0:00}`(`player_id`, `index`, `data_ref`, `result`, `end_time`) VALUES(@player_id, @index, @data_ref, @result, @end_time);
                                  SELECT CAST(LAST_INSERT_ID() AS SIGNED);";

            var cmd = CreateDBCommand(text, id);
            cmd.AddParameter("@player_id", id.Serial);
            cmd.AddParameter("@index", index);
            cmd.AddParameter("@data_ref", dataRef);
            cmd.AddParameter("@result", result);
            cmd.AddParameter("@end_time", endTime);

            return cmd;
        }

        public static IDBCommand SelectFaithClaim(PublicID id)
        {
            return SelectAll(id, "faith_claim");
        }

        public static IDBCommand UpdateFaithClaim(PublicID id, DateTime claimTime)
        {
            const string text = "UPDATE `faith_claim_{0:00}` SET claim_time=@claim_time WHERE player_id=@player_id;";

            var cmd = CreateDBCommand(text, id);

            cmd.AddParameter("@player_id", id.Serial);
            cmd.AddParameter("@claim_time", claimTime);

            return cmd;
        }

        public static IDBCommand InsertFaithClaim(PublicID id, DateTime claimTime)
        {
            const string text = @"INSERT INTO `faith_claim_{0:00}`(`player_id`, `claim_time`) VALUES(@player_id, @claim_time);";

            var cmd = CreateDBCommand(text, id);
            cmd.AddParameter("@player_id", id.Serial);
            cmd.AddParameter("@claim_time", claimTime);

            return cmd; 
        }

        #endregion

        #region Tactics

        public static IDBCommand SelectTacticsList(PublicID id)
        {
            return SelectAll(id, "tactics");
        }

        public static IDBCommand InsertTactics(PublicID id, int tacticsId, DateTime timestamp)
        {
            const string text = @"INSERT INTO `tactics_{0:00}`(`player_id`, `tactics_id`, `timestamp`) VALUES(@player_id, @tactics_id, @timestamp);";

            var cmd = CreateDBCommand(text, id);
            cmd.AddParameter("@player_id", id.Serial);
            cmd.AddParameter("@tactics_id", tacticsId);
            cmd.AddParameter("@timestamp", timestamp);

            return cmd; 
        }

        #endregion

        #region PVP

        public static IDBCommand SelectPVPContextList(int group)
        {
            const string text = "SELECT * FROM `pvp_context_{0:00}`;";

            var cmd = CreateDBCommand(text, group);

            return cmd;
        }

        public static IDBCommand SelectPVPSkillList(PublicID id)
        {
            return SelectAll(id, "pvp_skill");
        }

        public static IDBCommand SelectPVPBattle(PublicID id, Int64 questId)
        {
            const string text = "SELECT * FROM `pvp_battle_{0:00}` WHERE player_id=@player_id AND quest_id=@quest_id;";

            var cmd = CreateDBCommand(text, id);
            cmd.AddParameter("@player_id", id.Serial);
            cmd.AddParameter("@quest_id", questId);

            return cmd;
        }

        public static IDBCommand InsertPVPSkill(PublicID id, int skillId, DateTime timestamp)
        {
            const string text = @"INSERT INTO `pvp_skill_{0:00}`(`player_id`, `skill_id`, `timestamp`) VALUES(@player_id, @skill_id, @timestamp);";

            var cmd = CreateDBCommand(text, id);
            cmd.AddParameter("@player_id", id.Serial);
            cmd.AddParameter("@skill_id", skillId);
            cmd.AddParameter("@timestamp", timestamp);

            return cmd;
        }

        public static IDBCommand InsertPVPContext(PublicID id, int cardId, string questDesign, DateTime updateTime)
        {
            const string text = @"INSERT INTO `pvp_context_{0:00}`(`player_id`, `card_id`, `quest_design`, `bp_update_time`, `update_time`) VALUES(@player_id, @card_id, @quest_design, @update_time, @update_time);";

            var cmd = CreateDBCommand(text, id);
            cmd.AddParameter("@player_id", id.Serial);
            cmd.AddParameter("@card_id", cardId);
            cmd.AddParameter("@quest_design", questDesign);
            cmd.AddParameter("@update_time", updateTime);

            return cmd;
        }

        public static IDBCommand UpdatePVPContextCardID(PublicID id, int cardId, DateTime updateTime)
        {
            const string text = "UPDATE `pvp_context_{0:00}` SET `card_id`=@card_id, `update_time`=@update_time WHERE player_id=@player_id;";

            var cmd = CreateDBCommand(text, id);

            cmd.AddParameter("@player_id", id.Serial);
            cmd.AddParameter("@card_id", cardId);
            cmd.AddParameter("@update_time", updateTime);

            return cmd;
        }

        public static IDBCommand UpdatePVPContext(PublicID id, string questDesign, DateTime updateTime)
        {
            const string text = "UPDATE `pvp_context_{0:00}` SET `quest_design`=@quest_design, `update_time`=@update_time WHERE player_id=@player_id;";

            var cmd = CreateDBCommand(text, id);

            cmd.AddParameter("@player_id", id.Serial);
            cmd.AddParameter("@quest_design", questDesign);
            cmd.AddParameter("@update_time", updateTime);

            return cmd;
        }

        public static IDBCommand InsertPVPBattle(PublicID id, PublicID targetId, Int64 questId, int win, int lose, DateTime timestamp)
        {
            const string text = @"INSERT INTO `pvp_battle_{0:00}`(`player_id`, `quest_id`, `target_group`, `target_id`, `win`, `lose`, `timestamp`) VALUES(@player_id, @quest_id, @target_group, @target_id, @win, @lose, @timestamp);";

            var cmd = CreateDBCommand(text, id);
            cmd.AddParameter("@player_id", id.Serial);
            cmd.AddParameter("@quest_id", questId);
            cmd.AddParameter("@target_group", targetId.Group);
            cmd.AddParameter("@target_id", targetId.Serial);
            cmd.AddParameter("@win", win);
            cmd.AddParameter("@lose", lose);
            cmd.AddParameter("@timestamp", timestamp);

            return cmd;
        }

        public static IDBCommand UpdatePVPBattle(PublicID id, Int64 serial, int result)
        {
            throw new NotImplementedException();
        }

        public static IDBCommand UpdateBP(PublicID id, int consumed, DateTime updateTime)
        {
            const string text = "UPDATE `pvp_context_{0:00}` SET `bp_consumed`=@bp_consumed, `bp_update_time`=@bp_update_time WHERE player_id=@player_id;";

            var cmd = CreateDBCommand(text, id);

            cmd.AddParameter("@player_id", id.Serial);
            cmd.AddParameter("@bp_consumed", consumed);
            cmd.AddParameter("@bp_update_time", updateTime);

            return cmd;
        }

        #endregion

        #region DEBUG

        public static IDBCommand DebugSelectPlayerCount(int group)
        {
            const string text = "SELECT COUNT(*) FROM `player_{0:00}` WHERE `complex`!=0;";

            var cmd = CreateNoLockDBCommand(text, group);

            return cmd;
        }
        public static IDBCommand DebugSelectRandomPlayer(int group, int playerCount)
        {
            const string text = "SELECT * FROM `player_{0:00}` WHERE `complex`!=0 LIMIT @from,1;";

            var cmd = CreateNoLockDBCommand(text, group);

            cmd.AddParameter("@from", MathUtils.Random.Next(playerCount));

            return cmd;
        }

        public static IDBCommand DebugUpdatePlayer(PublicID id, string field, string val, DateTime now)
        {
            string text;

            if (field == "ap_consumed")
                text = "UPDATE `player_{0:00}` SET ap_consumed=@val, ap_change_time=@now WHERE player_id=@player_id;";
            else
                text = "UPDATE `player_{0:00}` SET " + field + "=@val WHERE player_id=@player_id;";


            var cmd = CreateDBCommand(text, id);

            cmd.AddParameter("@player_id", id.Serial);
            cmd.AddParameter("@val", field == "name" ? (object)val : (object)int.Parse(val));
            cmd.AddParameter("@now", now);

            return cmd;
        }

        public static IDBCommand DebugUpdateQuest(PublicID id, int questId, bool isDone, DateTime now)
        {
            if (!isDone)
            {
                const string text = @"DELETE FROM `quest_{0:00}` WHERE player_id=@player_id AND quest_id=@quest_id;";

                var cmd = CreateDBCommand(text, id);

                cmd.AddParameter("@player_id", id.Serial);
                cmd.AddParameter("@quest_id", questId);


                return cmd;
            }
            else
            {

                return UpdateQuestLastPlayTime(id, questId, ComplexI32.Bit(0).Value, now);
            }
        }

        public static IDBCommand DebugUpdateCard(PublicID id, Int64 serial, string field, string val)
        {
            var text = "UPDATE `card_{0:00}` SET " + field + "=@val WHERE player_id=@player_id AND id=@id;";

            var cmd = CreateDBCommand(text, id);

            cmd.AddParameter("@player_id", id.Serial);
            cmd.AddParameter("@id", serial);
            cmd.AddParameter("@val", int.Parse(val));

            return cmd;
        }

        public static IDBCommand DebugUpdateCardComplex(PublicID id, Int64 serial, Int64 val)
        {
            var text = "UPDATE `card_{0:00}` SET complex=@val WHERE player_id=@player_id AND id=@id;";

            var cmd = CreateDBCommand(text, id);

            cmd.AddParameter("@player_id", id.Serial);
            cmd.AddParameter("@id", serial);
            cmd.AddParameter("@val", val);

            return cmd;
        }

        public static IDBCommand DebugRemoveExpand(PublicID id, ArticleID articleId)
        {
            const string text = "DELETE FROM `purchase_{0:00}` WHERE player_id=@player_id AND article_id=@article_id;";

            var cmd = CreateDBCommand(text, id);

            cmd.AddParameter("@player_id", id.Serial);
            cmd.AddParameter("@article_id", (byte)articleId);

            return cmd;
        }

        public static IDBCommand DebugResetDaily(PublicID id)
        {
            const string text = "DELETE FROM `daily_{0:00}` WHERE player_id=@player_id;";

            var cmd = CreateDBCommand(text, id);

            cmd.AddParameter("@player_id", id.Serial);

            return cmd;
        }

        public static IDBCommand DebugDeleteTrophy(PublicID id, int trophyId)
        {
            const string text = "DELETE FROM `trophy_{0:00}` WHERE `player_id`=@player_id AND `group`=@group;";

            var cmd = CreateDBCommand(text, id);

            cmd.AddParameter("@player_id", id.Serial);
            cmd.AddParameter("@group", trophyId);

            return cmd;
        }

        public static IDBCommand InsertDummy(PublicID id, int key, int favorite)
        {
            const string text = @"INSERT INTO `dummy`(`key`, `group_id`, `player_id`, `favorite`) VALUES(@key, @group_id, @player_id, @favorite);";

            var cmd = CreateDBCommand(text);
            cmd.AddParameter("@key", key);
            cmd.AddParameter("@group_id", id.Group);
            cmd.AddParameter("@player_id", id.Serial);
            cmd.AddParameter("@favorite", favorite);

            return cmd;   
        }

        public static IDBCommand DebugSelectIdlePlayer(int group, DateTime from)
        {
            const string text = "SELECT * FROM `player_{0:00}` WHERE `last_play_time`<@from ORDER BY `id` DESC LIMIT 1000;";

            var cmd = CreateNoLockDBCommand(text, group);

            cmd.AddParameter("@from", from);

            return cmd;
        }

        public static IDBCommand DebugInsertPVPContxt(PublicID id, int cardId, string questDesign)
        {
            const string text = @"INSERT INTO `pvp_idle_player`(`group`, `player_id`, `card_id`, `quest_design`) VALUES(@group, @player_id, @card_id, @quest_design);";

            var cmd = CreateDBCommand(text);
            cmd.AddParameter("@group", id.Group);
            cmd.AddParameter("@player_id", id.Serial);
            cmd.AddParameter("@card_id", cardId);
            cmd.AddParameter("@quest_design", questDesign);

            return cmd;
        }

        public static IDBCommand DebugSelectPVPContext()
        {
            return SelectAll("pvp_idle_player");
        }
        public static IDBCommand DebugDeletePVPContext()
        {
            const string text = @"TRUNCATE TABLE `pvp_idle_player`;";

            return CreateDBCommand(text); 
        }
        #endregion

        #region LOG
        public static IDBCommand LogQuest(int questId, int playCount, int clearCount, int continueCount, DateTime updateTime)
        {
            const string text = "CALL update_log_quest(@quest_id, @play_count, @clear_count, @continue_count, @update_time);";

            var cmd = CreateDBCommand(text);
            cmd.AddParameter("@quest_id", questId);
            cmd.AddParameter("@play_count", playCount);
            cmd.AddParameter("@clear_count", clearCount);
            cmd.AddParameter("@continue_count", continueCount);
            cmd.AddParameter("@update_time", updateTime);

            return cmd;
        }
        public static IDBCommand LogSignature(PublicID id, string ip, string signature, DateTime timestamp)
        {
            const string text = @"INSERT INTO `log_signature`(`group_id`, `player_id`, `ip`, `signature`, `timestamp`) VALUES(@group_id, @player_id, @ip, @signature, @timestamp);";

            var cmd = CreateDBCommand(text);
            cmd.AddParameter("@group_id", id.Group);
            cmd.AddParameter("@player_id", id.Serial);
            cmd.AddParameter("@ip", ip);
            cmd.AddParameter("@signature", signature);
            cmd.AddParameter("@timestamp", timestamp);

            return cmd;   
        }
        public static IDBCommand LogEvent(PublicID id, int eventId, int point, DateTime updateTime)
        {
            const string text = "CALL update_log_event_each(@id, @point, @update_time, @player_id);";

            var cmd = CreateDBCommand(text);
            cmd.AddParameter("@id", eventId);
            cmd.AddParameter("@point", point);
            cmd.AddParameter("@update_time", updateTime);
            cmd.AddParameter("@player_id", Int64.Parse(id.ToString()));

            return cmd;
        }
        public static IDBCommand SyncLogEvent(int id)
        {
            const string text = "UPDATE `log_event` SET history=@point, history_time=@update_time WHERE id=@id;";

            var cmd = CreateDBCommand(text);

            cmd.AddParameter("@id", id);

            return cmd;
        }
        #endregion

        #region TABLE

        #region PUBLIC

        public static IEnumerable<IDBCommand> SetupDatabase()
        {
            yield return CreateAccountTable();
            yield return CreateSignatureTable();
            yield return CreateDataCacheTable();
            yield return CreateEventScheduleTable();
            yield return CreateTestPlayerTable();
            yield return CreateSerialNumberTable();
            yield return CreateIAPProductTable();
            yield return CreateGooglePlayReceiptTable();
            yield return CreateIOSReceiptTable();
            yield return CreateTransferCacheTable();
            yield return CreateCoMissionTable();
            yield return CreateGMTable();
            yield return CreateBlackTable();
            yield return CreateBlackGemTable();
            yield return CreateMyCardReceiptTable();
            yield return CreateMyCardTransactionTable();
            yield return CreateMyCardHistoryTable();
            yield return CreateDummyTable();
            yield return CreateSharedQuestTable();
            yield return CreatePVPIdlePlayerTable();
        }

        public static IEnumerable<IDBCommand> SetupLogDatabase()
        {
            yield return CreateLogQuestTable();
            yield return CreateLogSignatureTable();
            yield return CreateLogEventTable();
            yield return CreateLogEventEachTable();

            yield return LogQuestStoreProcedure();
            yield return LogEventStoreProcedure();
            yield return LogEventEachStoreProcedure();
        }

        public static IEnumerable<IDBCommand> SetupDatabase(int group)
        {
            #region 建表
            yield return CreatePlayerTable(group);
            yield return CreateBackupTable(group);
            yield return CreateCardTable(group);
            yield return CreateItemTable(group);
            yield return CreateEquipmentTable(group);
            yield return CreateCardEventCacheTable(group);
            yield return CreateQuestTable(group);
            yield return CreateQuestCacheTable(group);
            yield return CreateReservedTable(group);
            yield return CreateGemTable(group);
            yield return CreatePurchaseTable(group);
            yield return CreateGiftTable(group);
            yield return CreateLimitedQuestTable(group);
            yield return CreateTrophyTable(group);
            yield return CreateSimpleTrophyTable(group);
            yield return CreateDailyTable(group);
            yield return CreateRandomShopTable(group);
            yield return CreateGalleryTable(group);
            yield return CreateEventPointTable(group);
            yield return CreateRescueTable(group);
            yield return CreateIAPHistoryTable(group);
            yield return CreateCoMissionStateTable(group);
            yield return CreateMyLordTable(group);
            yield return CreateSellCacheTable(group);
            yield return CreateInfinityTable(group);
            yield return CreateFellowTable(group);
            yield return CreateSnapshotTable(group);
            yield return CreateSocialPurchaseTable(group);
            yield return CreateDailyEPTable(group);
            yield return CreateCrusadeTable(group);
            yield return CreateCrusadeCacheTable(group);
            yield return CreateFaithTable(group);
            yield return CreateFaithClaimTable(group);
            yield return CreateTacticsTable(group);
            yield return CreatePVPContextTable(group);
            yield return CreatePVPSkillTable(group);
            yield return CreatePVPBattleTable(group);
            #endregion

            #region 建立預存程序
            yield return UpdateBackupStoreProcedure(group);
            yield return IncreaseItemStoreProcedure(group);
            yield return UpdateQuestLastPlayStoreProcedure(group);
            yield return UpdateTrophyStoreProcedure(group);
            yield return ResetDailyStoreProcedure(group);
            yield return InsertGalleryStoreProcedure(group);
            yield return UpdateEventPointStoreProcedure(group);
            yield return SetEventPointStoreProcedure(group);
            yield return UpdateRescueStoreProcedure(group);
            yield return UpdateIAPHistoryStoreProcedure(group);
            yield return UpdateCoMissionStateStoreProcedure(group);
            yield return UpdateFellowStoreProcedure(group);
            yield return UpdateFellowForeignStoreProcedure(group);
            yield return UpdateSnapshotStoreProcedure(group);

            #endregion

            #region HotFix
            /* overdue
            yield return HotFixQuestCache(group);
            yield return HotFixEquipment(group);
            */
            #endregion
        }

        #endregion

        #region StoreProcedure

        private static IDBCommand UpdateBackupStoreProcedure(int group)
        {
            const string text = @"CREATE PROCEDURE `update_backup_{0:00}` (
	                                IN p_player_id BIGINT,
	                                IN p_key INT,
	                                IN p_data text
                                )
                                BEGIN

                                DECLARE AffectedRows INT;

                                UPDATE `backup_{0:00}` SET `data`=p_data WHERE `player_id`=p_player_id AND `key`=p_key;

                                SET AffectedRows = (SELECT row_count());

                                IF AffectedRows = 0 
                                THEN
                                 INSERT INTO `backup_{0:00}` (`player_id`, `key`, `data`) VALUES (p_player_id, p_key, p_data);
                                END IF;

                                END";

            return CreateCreateStoreProcedureDBCommand(text, group);
        }

        private static IDBCommand IncreaseItemStoreProcedure(int group)
        {
            const string text = @"CREATE PROCEDURE `increase_item_{0:00}` (
	                                IN p_player_id BIGINT,
	                                IN p_item_id INT,
	                                IN p_diff INT
                                )
                                BEGIN

                                DECLARE AffectedRows INT;

                                UPDATE `item_{0:00}` SET count=count+p_diff WHERE player_id=p_player_id AND item_id=p_item_id;

                                SET AffectedRows = (SELECT row_count());

                                IF AffectedRows = 0 
                                THEN
                                 INSERT INTO `item_{0:00}` (player_id, item_id, count) VALUES (p_player_id, p_item_id, p_diff);
                                END IF;

                                END";

            return CreateCreateStoreProcedureDBCommand(text, group);
        }

        private static IDBCommand UpdateQuestLastPlayStoreProcedure(int group)
        {
            const string text = @"CREATE PROCEDURE `update_quest_{0:00}` (
	                                IN p_player_id BIGINT,
	                                IN p_quest_id INT,
                                    IN p_complex INT,
	                                IN p_now datetime
                                )
                                BEGIN

                                DECLARE AffectedRows INT;

                                UPDATE `quest_{0:00}` SET complex=complex|p_complex, last_play=p_now WHERE player_id=p_player_id AND quest_id=p_quest_id;

                                SET AffectedRows = (SELECT row_count());

                                IF AffectedRows = 0 
                                THEN
                                 INSERT INTO `quest_{0:00}` (player_id, quest_id, complex, last_play) VALUES (p_player_id, p_quest_id, p_complex, p_now);
                                END IF;

                                END";

            return CreateCreateStoreProcedureDBCommand(text, group);
        }

        private static IDBCommand UpdateTrophyStoreProcedure(int group)
        {
            const string text = @"CREATE PROCEDURE `update_trophy_{0:00}` (
	                                IN p_player_id BIGINT,
	                                IN p_group INT,
                                    IN p_current INT,
                                    IN p_progress INT,
	                                IN p_now datetime
                                )
                                BEGIN

                                DECLARE AffectedRows INT;

                                UPDATE `trophy_{0:00}` SET current=p_current, progress=p_progress, update_time=p_now WHERE player_id=p_player_id AND `group`=p_group;

                                SET AffectedRows = (SELECT row_count());

                                IF AffectedRows = 0 
                                THEN
                                 INSERT INTO `trophy_{0:00}` (player_id, `group`, current, progress, update_time) VALUES (p_player_id, p_group, p_current, p_progress, p_now);
                                END IF;

                                END";

            return CreateCreateStoreProcedureDBCommand(text, group);
        }

        private static IDBCommand ResetDailyStoreProcedure(int group)
        {
            const string text = @"CREATE PROCEDURE `reset_daily_{0:00}` (
	                                IN p_player_id BIGINT,
	                                IN p_seed BIGINT,
	                                IN p_now datetime
                                )
                                BEGIN

                                DECLARE AffectedRows INT;

                                UPDATE `daily_{0:00}` SET `seed`=p_seed, `state`=0, `consumed`=0, `add`=0, `cached`=-1, `update_time`=p_now WHERE player_id=p_player_id;

                                SET AffectedRows = (SELECT row_count());

                                IF AffectedRows = 0 
                                THEN
                                 INSERT INTO `daily_{0:00}` (player_id, seed, update_time) VALUES (p_player_id, p_seed, p_now);
                                END IF;

                                END";

            return CreateCreateStoreProcedureDBCommand(text, group);
        }

        private static IDBCommand InsertGalleryStoreProcedure(int group)
        {
            const string text = @"CREATE PROCEDURE `insert_gallery_{0:00}` (
	                                IN p_player_id BIGINT,
	                                IN p_card_id INT,
	                                IN p_diff INT
                                )
                                BEGIN

                                DECLARE AffectedRows INT;

                                UPDATE `gallery_{0:00}` SET luck=luck+p_diff WHERE player_id=p_player_id AND card_id=p_card_id;

                                SET AffectedRows = (SELECT row_count());

                                IF AffectedRows = 0 
                                THEN
                                 INSERT INTO `gallery_{0:00}` (player_id, card_id, luck) VALUES (p_player_id, p_card_id, p_diff);
                                END IF;

                                END";

            return CreateCreateStoreProcedureDBCommand(text, group);
        }

        private static IDBCommand UpdateEventPointStoreProcedure(int group)
        {
            const string text = @"CREATE PROCEDURE `update_event_point_{0:00}` (
	                                IN p_player_id BIGINT,
	                                IN p_event_id BIGINT,
	                                IN p_add INT,
	                                IN p_card_id INT
                                )
                                BEGIN

                                DECLARE AffectedRows INT;

                                UPDATE `event_point_{0:00}` SET point=point+p_add, card_id=p_card_id WHERE player_id=p_player_id AND event_id=p_event_id;

                                SET AffectedRows = (SELECT row_count());

                                IF AffectedRows = 0 
                                THEN
                                 INSERT INTO `event_point_{0:00}` (player_id, event_id, point, card_id) VALUES (p_player_id, p_event_id, p_add, p_card_id);
                                END IF;

                                END";

            return CreateCreateStoreProcedureDBCommand(text, group);
        }

        private static IDBCommand SetEventPointStoreProcedure(int group)
        {
            const string text = @"CREATE PROCEDURE `set_event_point_{0:00}` (
	                                IN p_player_id BIGINT,
	                                IN p_event_id BIGINT,
	                                IN p_point INT,
	                                IN p_card_id INT
                                )
                                BEGIN

                                DECLARE AffectedRows INT;

                                UPDATE `event_point_{0:00}` SET point=p_point, card_id=p_card_id WHERE player_id=p_player_id AND event_id=p_event_id;

                                SET AffectedRows = (SELECT row_count());

                                IF AffectedRows = 0 
                                THEN
                                 INSERT INTO `event_point_{0:00}` (player_id, event_id, point, card_id) VALUES (p_player_id, p_event_id, p_point, p_card_id);
                                END IF;

                                END";

            return CreateCreateStoreProcedureDBCommand(text, group);
        }

        private static IDBCommand UpdateRescueStoreProcedure(int group)
        {
            const string text = @"CREATE PROCEDURE `update_rescue_{0:00}` (
	                                IN p_player_id BIGINT,
	                                IN p_count INT,
	                                IN p_update_time datetime
                                )
                                BEGIN

                                DECLARE AffectedRows INT;

                                UPDATE `rescue_{0:00}` SET count=p_count, update_time=p_update_time WHERE player_id=p_player_id;

                                SET AffectedRows = (SELECT row_count());

                                IF AffectedRows = 0 
                                THEN
                                 INSERT INTO `rescue_{0:00}` (player_id, count, update_time) VALUES (p_player_id, p_count, p_update_time);
                                END IF;

                                END";

            return CreateCreateStoreProcedureDBCommand(text, group);
        }

        private static IDBCommand LogQuestStoreProcedure()
        {
            const string text = @"CREATE PROCEDURE `update_log_quest` (
                                    IN p_quest_id INT,
	                                IN p_play_count INT,
	                                IN p_clear_count INT,
	                                IN p_continue_count INT,
                                    IN p_update_time datetime
                                )
                                BEGIN

                                DECLARE AffectedRows INT;

                                UPDATE `log_quest` SET play_count=play_count+p_play_count, clear_count=clear_count+p_clear_count, continue_count=continue_count+p_continue_count, update_time=p_update_time WHERE quest_id=p_quest_id;

                                SET AffectedRows = (SELECT row_count());

                                IF AffectedRows = 0 
                                THEN
                                 INSERT INTO `log_quest` (quest_id, play_count, clear_count, continue_count, update_time) VALUES (p_quest_id, p_play_count, p_clear_count, p_continue_count, p_update_time);
                                END IF;

                                END";

            return CreateCreateStoreProcedureDBCommand(text);
        }

        private static IDBCommand LogEventStoreProcedure()
        {
            const string text = @"CREATE PROCEDURE `update_log_event` (
                                    IN p_id INT,
	                                IN p_point INT,
                                    IN p_update_time datetime
                                )
                                BEGIN

                                DECLARE AffectedRows INT;

                                UPDATE `log_event` SET current=current+p_point, update_time=p_update_time WHERE id=p_id;

                                SET AffectedRows = (SELECT row_count());

                                IF AffectedRows = 0 
                                THEN
                                 INSERT INTO `log_event` (id, current, history, update_time, history_time) VALUES (p_id, p_point, 0, p_update_time, p_update_time);
                                END IF;

                                END";

            return CreateCreateStoreProcedureDBCommand(text);
        }

        private static IDBCommand LogEventEachStoreProcedure()
        {
            const string text = @"CREATE PROCEDURE `update_log_event_each` (
                                    IN p_id INT,
	                                IN p_point INT,
                                    IN p_update_time datetime,
                                    IN p_player_id BIGINT
                                )
                                BEGIN

                                DECLARE AffectedRows INT;

                                UPDATE `log_event_each` SET current=current+p_point, update_time=p_update_time WHERE id=p_id AND player_id=p_player_id;

                                SET AffectedRows = (SELECT row_count());

                                IF AffectedRows = 0 
                                THEN
                                 INSERT INTO `log_event_each` (id, player_id, current, update_time) VALUES (p_id, p_player_id, p_point, p_update_time);
                                END IF;

                                END";

            return CreateCreateStoreProcedureDBCommand(text);
        }

        private static IDBCommand UpdateIAPHistoryStoreProcedure(int group)
        {
            const string text = @"CREATE PROCEDURE `update_iap_history_{0:00}` (
	                                IN p_player_id BIGINT,
	                                IN p_product_id INT,
                                    IN p_receipt_id BIGINT,
	                                IN p_update_time datetime
                                )
                                BEGIN

                                DECLARE AffectedRows INT;

                                UPDATE `iap_history_{0:00}` SET receipt_id=p_receipt_id, update_time=p_update_time WHERE player_id=p_player_id AND product_id=p_product_id;

                                SET AffectedRows = (SELECT row_count());

                                IF AffectedRows = 0 
                                THEN
                                 INSERT INTO `iap_history_{0:00}` (player_id, product_id, receipt_id, update_time) VALUES (p_player_id, p_product_id, p_receipt_id, p_update_time);
                                END IF;

                                END";

            return CreateCreateStoreProcedureDBCommand(text, group);
        }

        private static IDBCommand UpdateCoMissionStateStoreProcedure(int group)
        {
            const string text = @"CREATE PROCEDURE `update_co_mission_state_{0:00}` (
	                                IN p_player_id BIGINT,
	                                IN p_mission_id BIGINT,
                                    IN p_subject_1 INT,
                                    IN p_subject_2 INT,
                                    IN p_subject_3 INT,
                                    IN p_subject_4 INT,
                                    IN p_subject_5 INT
                                )
                                BEGIN

                                DECLARE AffectedRows INT;

                                UPDATE `co_mission_state_{0:00}` SET subject_1=subject_1+p_subject_1, subject_2=subject_2+p_subject_2, subject_3=subject_3+p_subject_3, subject_4=subject_4+p_subject_4, subject_5=subject_5+p_subject_5 WHERE player_id=p_player_id AND mission_id=p_mission_id;

                                SET AffectedRows = (SELECT row_count());

                                IF AffectedRows = 0 
                                THEN
                                 INSERT INTO `co_mission_state_{0:00}` (player_id, mission_id, subject_1, subject_2, subject_3, subject_4, subject_5) VALUES (p_player_id, p_mission_id, p_subject_1, p_subject_2, p_subject_3, p_subject_4, p_subject_5);
                                END IF;

                                END";

            return CreateCreateStoreProcedureDBCommand(text, group);
        }

        private static IDBCommand UpdateFellowStoreProcedure(int group)
        {
            const string text = @"CREATE PROCEDURE `update_fellow_{0:00}` (
	                                IN p_player_id BIGINT,
	                                IN p_fellow_group INT,
	                                IN p_fellow_id BIGINT,
                                    IN p_complex INT,
                                    IN p_now datetime
                                )
                                BEGIN

                                DECLARE AffectedRows INT;

                                UPDATE `fellow_{0:00}` SET complex=complex|p_complex, update_time=p_now WHERE player_id=p_player_id AND fellow_group=p_fellow_group AND fellow_id=p_fellow_id;

                                SET AffectedRows = (SELECT row_count());

                                IF AffectedRows = 0 
                                THEN
                                 INSERT INTO `fellow_{0:00}` (player_id, fellow_group, fellow_id, complex, update_time) VALUES (p_player_id, p_fellow_group, p_fellow_id, p_complex, p_now);
                                END IF;

                                END";

            return CreateCreateStoreProcedureDBCommand(text, group);
        }

        private static IDBCommand UpdateFellowForeignStoreProcedure(int group)
        {
            const string text = @"CREATE PROCEDURE `update_fellow_foreign_{0:00}` (
	                                IN p_player_id BIGINT,
	                                IN p_fellow_group INT,
	                                IN p_fellow_id BIGINT,
                                    IN p_complex INT,
                                    IN p_now datetime
                                )
                                BEGIN

                                DECLARE AffectedRows INT;

                                UPDATE `fellow_{0:00}` SET complex=complex|p_complex WHERE player_id=p_player_id AND fellow_group=p_fellow_group AND fellow_id=p_fellow_id;

                                SET AffectedRows = (SELECT row_count());

                                IF AffectedRows = 0 
                                THEN
                                 INSERT INTO `fellow_{0:00}` (player_id, fellow_group, fellow_id, complex, update_time) VALUES (p_player_id, p_fellow_group, p_fellow_id, p_complex, p_now);
                                END IF;

                                END";

            return CreateCreateStoreProcedureDBCommand(text, group);
        }

        private static IDBCommand UpdateSnapshotStoreProcedure(int group)
        {
            const string text = @"CREATE PROCEDURE `update_snapshot_{0:00}` (
	                                IN p_player_id BIGINT,
	                                IN p_data text,
                                    IN p_now datetime
                                )
                                BEGIN

                                DECLARE AffectedRows INT;

                                UPDATE `snapshot_{0:00}` SET `data`=p_data, `update_time`=p_now WHERE player_id=p_player_id;

                                SET AffectedRows = (SELECT row_count());

                                IF AffectedRows = 0 
                                THEN
                                 INSERT INTO `snapshot_{0:00}`(`player_id`, `data`, `update_time`) VALUES(p_player_id, p_data, p_now);
                                END IF;

                                END";

            return CreateCreateStoreProcedureDBCommand(text, group);
        }

        #endregion

        #endregion
    }
}
