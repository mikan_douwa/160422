﻿using Mikan.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mikan.CSAGA.Server
{
    public static partial class DBCommandBuilder
    {
        #region player db

        public static IDBCommand CreatePlayerTable(int group)
        {
            const string text = @"CREATE TABLE IF NOT EXISTS `player_{0:00}` (
                                  `id` bigint NOT NULL,
                                  `player_id` bigint NOT NULL AUTO_INCREMENT,
                                  `category` int NOT NULL DEFAULT '0',
                                  `name` nvarchar(20) NOT NULL DEFAULT 'new player',
                                  `exp` int NOT NULL DEFAULT '0',
                                  `crystal` int NOT NULL DEFAULT '0',
                                  `coin` int NOT NULL DEFAULT '0',
                                  `fp` int NOT NULL DEFAULT '0',
                                  `tp_consumed` int NOT NULL DEFAULT '0',
                                  `tp_change_time` datetime NOT NULL,
                                  `last_play_time` datetime NOT NULL,
                                  `complex` bigint NOT NULL DEFAULT '0',
                                  PRIMARY KEY (`id`),
                                  UNIQUE KEY `i_player_id` (`player_id`)
                                ) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;";

            return CreateDBCommand(text, group);
        }

        public static IDBCommand CreateBackupTable(int group)
        {
            const string text = @"CREATE TABLE IF NOT EXISTS `backup_{0:00}` (
                                  `player_id` bigint(20) NOT NULL,
                                  `key` int(11) NOT NULL,
                                  `data` text NOT NULL,
                                  KEY `i_player_id` (`player_id`),
                                  KEY `i_key` (`key`)
                                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

            return CreateDBCommand(text, group);
        }

        public static IDBCommand CreateCardTable(int group)
        {
            const string text = @"CREATE TABLE IF NOT EXISTS `card_{0:00}` (
                                  `id` bigint NOT NULL AUTO_INCREMENT,
                                  `player_id` bigint NOT NULL,
                                  `card_id` int NOT NULL,
                                  `exp` int NOT NULL DEFAULT '0',
                                  `kizuna` int NOT NULL DEFAULT '0',
                                  `complex` bigint NOT NULL DEFAULT '0',
                                  PRIMARY KEY (`id`),
                                  KEY `i_player_id` (`player_id`)
                                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

            return CreateDBCommand(text, group);
        }

        public static IDBCommand CreateQuestTable(int group)
        {
            const string text = @"CREATE TABLE IF NOT EXISTS `quest_{0:00}` (
                                  `player_id` bigint NOT NULL,
                                  `quest_id` int NOT NULL,
                                  `last_play` datetime NOT NULL,
                                  `complex` int NOT NULL,
                                  PRIMARY KEY (`player_id`,`quest_id`)
                                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

            return CreateDBCommand(text, group);
        }

        public static IDBCommand CreateQuestCacheTable(int group)
        {
            const string text = @"CREATE TABLE IF NOT EXISTS `quest_cache_{0:00}` (
                                  `id` bigint NOT NULL AUTO_INCREMENT,
                                  `player_id` bigint NOT NULL,
                                  `quest_id` int NOT NULL,
                                  `drop` int NOT NULL,
                                  `luck_drop` int NOT NULL,
                                  `data` text NOT NULL DEFAULT '',
                                  `complex` int NOT NULL DEFAULT '0',
                                  `timestamp` datetime NOT NULL,
                                  PRIMARY KEY (`id`),
                                  KEY `i_player_id` (`player_id`)
                                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

            return CreateDBCommand(text, group);
        }

        public static IDBCommand CreateItemTable(int group)
        {
            const string text = @"CREATE TABLE IF NOT EXISTS `item_{0:00}` (
                                  `player_id` bigint NOT NULL,
                                  `item_id` int NOT NULL,
                                  `count` int NOT NULL,
                                  PRIMARY KEY (`player_id`,`item_id`)
                                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

            return CreateDBCommand(text, group);
        }

        public static IDBCommand CreateEquipmentTable(int group)
        {
            const string text = @"CREATE TABLE IF NOT EXISTS `equipment_{0:00}` (
                                  `id` bigint NOT NULL AUTO_INCREMENT,
                                  `player_id` bigint NOT NULL,
                                  `item_id` int NOT NULL,
                                  `card_id` bigint NOT NULL DEFAULT '0',
                                  `slot` tinyint unsigned NOT NULL DEFAULT '0',
                                  `hp` int NOT NULL,
                                  `atk` int NOT NULL,
                                  `rev` int NOT NULL,
                                  `chg` int NOT NULL,
                                  `effect_id` int NOT NULL,
                                  `counter` int NOT NULL DEFAULT '0',
                                  `counter_type` int NOT NULL DEFAULT '0',
                                  PRIMARY KEY (`id`),
                                  KEY `i_player_id` (`player_id`)
                                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

            return CreateDBCommand(text, group);
        }

        public static IDBCommand CreateCardEventCacheTable(int group)
        {
            const string text = @"CREATE TABLE IF NOT EXISTS `card_event_cache_{0:00}` (
                                  `player_id` bigint NOT NULL,
                                  `card_id` bigint NOT NULL,
                                  `interactive_id` int NOT NULL,
                                  `quest_id` bigint NOT NULL,
                                  PRIMARY KEY (`player_id`,`card_id`)
                                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

            return CreateDBCommand(text, group);
        }

        public static IDBCommand CreateReservedTable(int group)
        {
            const string text = @"CREATE TABLE IF NOT EXISTS `reserved_{0:00}` (
                                  `id` bigint NOT NULL AUTO_INCREMENT,
                                  `player_id` bigint NOT NULL,
                                  `type` tinyint unsigned NOT NULL,
                                  `value_1` int NOT NULL,
                                  `value_2` int NOT NULL,
                                  `effective_time` datetime NOT NULL,
                                  PRIMARY KEY (`id`),
                                  KEY `i_player_id` (`player_id`)
                                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

            return CreateDBCommand(text, group);
        }

        public static IDBCommand CreatePurchaseTable(int group)
        {
            const string text = @"CREATE TABLE IF NOT EXISTS `purchase_{0:00}` (
                                  `id` bigint NOT NULL AUTO_INCREMENT,
                                  `player_id` bigint NOT NULL,
                                  `article_id` tinyint unsigned NOT NULL,
                                  `cost` int NOT NULL,
                                  `iap_cost` int NOT NULL,
                                  `timestamp` datetime NOT NULL,
                                  PRIMARY KEY (`id`),
                                  KEY `i_player_id` (`player_id`)
                                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

            return CreateDBCommand(text, group);
        }

        public static IDBCommand CreateGemTable(int group)
        {
            const string text = @"CREATE TABLE IF NOT EXISTS `gem_{0:00}` (
                                  `id` bigint NOT NULL AUTO_INCREMENT,
                                  `player_id` bigint NOT NULL,
                                  `source` tinyint unsigned NOT NULL,
                                  `add` int NOT NULL,
                                  `timestamp` datetime NOT NULL,
                                  `addition` int NOT NULL DEFAULT '0',
                                  PRIMARY KEY (`id`),
                                  KEY `i_player_id` (`player_id`)
                                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

            return CreateDBCommand(text, group);
        }

        public static IDBCommand CreateGiftTable(int group)
        {
            const string text = @"CREATE TABLE IF NOT EXISTS `gift_{0:00}` (
                                  `id` bigint NOT NULL AUTO_INCREMENT,
                                  `player_id` bigint NOT NULL,
                                  `type` int NOT NULL,
                                  `description` nvarchar(100) NOT NULL,
                                  `value_1` int NOT NULL,
                                  `value_2` int NOT NULL,
                                  `begin_time` datetime NOT NULL,
                                  `end_time` datetime NOT NULL,
                                  `source` tinyint unsigned NOT NULL,
                                  PRIMARY KEY (`id`),
                                  KEY `i_player_id` (`player_id`)
                                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

            return CreateDBCommand(text, group);
        }

        public static IDBCommand CreateLimitedQuestTable(int group)
        {
            const string text = @"CREATE TABLE IF NOT EXISTS `limited_quest_{0:00}` (
                                  `id` bigint NOT NULL AUTO_INCREMENT,
                                  `player_id` bigint NOT NULL,
                                  `quest_id` int NOT NULL,
                                  `play_count` int NOT NULL DEFAULT '0',
                                  `end_time` datetime NOT NULL,
                                  PRIMARY KEY (`id`),
                                  KEY `i_player_id` (`player_id`)
                                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

            return CreateDBCommand(text, group);
        }
        public static IDBCommand CreateTrophyTable(int group)
        {
            const string text = @"CREATE TABLE IF NOT EXISTS `trophy_{0:00}` (
                                  `player_id` bigint NOT NULL,
                                  `group` int NOT NULL,
                                  `current` int NOT NULL,
                                  `progress` int NOT NULL,
                                  `update_time` datetime NOT NULL,
                                  PRIMARY KEY (`player_id`,`group`)
                                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

            return CreateDBCommand(text, group);
        }
        public static IDBCommand CreateSimpleTrophyTable(int group)
        {
            const string text = @"CREATE TABLE IF NOT EXISTS `simple_trophy_{0:00}` (
                                  `player_id` bigint NOT NULL,
                                  `trophy_id` int NOT NULL,
                                  PRIMARY KEY (`player_id`,`trophy_id`)
                                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

            return CreateDBCommand(text, group);
        }

        public static IDBCommand CreateDailyTable(int group)
        {
            const string text = @"CREATE TABLE IF NOT EXISTS `daily_{0:00}` (
                                  `player_id` bigint NOT NULL,
                                  `seed` bigint NOT NULL,
                                  `state` int NOT NULL DEFAULT '0',
                                  `consumed` int NOT NULL DEFAULT '0',
                                  `add` int NOT NULL DEFAULT '0',
                                  `cached` int NOT NULL DEFAULT '-1',
                                  `update_time` datetime NOT NULL,
                                  PRIMARY KEY (`player_id`)
                                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

            return CreateDBCommand(text, group);
        }

        public static IDBCommand CreateRandomShopTable(int group)
        {
            const string text = @"CREATE TABLE IF NOT EXISTS `random_shop_{0:00}` (
                                  `player_id` bigint NOT NULL,
                                  `seed` bigint NOT NULL,
                                  `state` int NOT NULL DEFAULT '0',
                                  `update_time` datetime NOT NULL,
                                  PRIMARY KEY (`player_id`)
                                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

            return CreateDBCommand(text, group);
        }

        public static IDBCommand CreateGalleryTable(int group)
        {
            const string text = @"CREATE TABLE IF NOT EXISTS `gallery_{0:00}` (
                                  `player_id` bigint NOT NULL,
                                  `card_id` int NOT NULL,
                                  `luck` int NOT NULL,
                                  PRIMARY KEY (`player_id`, `card_id`)
                                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

            return CreateDBCommand(text, group);
        }

        public static IDBCommand CreateEventPointTable(int group)
        {
            const string text = @"CREATE TABLE IF NOT EXISTS `event_point_{0:00}` (
                                  `player_id` bigint NOT NULL,
                                  `event_id` bigint NOT NULL,
                                  `point` int NOT NULL,
                                  `card_id` int NOT NULL,
                                  `complex` int NOT NULL DEFAULT '0',
                                  `ranking` int NOT NULL DEFAULT '0',
                                  PRIMARY KEY (`player_id`, `event_id`)
                                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

            return CreateDBCommand(text, group);
        }

        public static IDBCommand CreateRescueTable(int group)
        {
            const string text = @"CREATE TABLE IF NOT EXISTS `rescue_{0:00}` (
                                  `player_id` bigint NOT NULL,
                                  `count` int NOT NULL DEFAULT '0',
                                  `update_time` datetime NOT NULL,
                                  PRIMARY KEY (`player_id`)
                                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

            return CreateDBCommand(text, group);
        }

        public static IDBCommand CreateIAPHistoryTable(int group)
        {
            const string text = @"CREATE TABLE IF NOT EXISTS `iap_history_{0:00}` (
                                  `player_id` bigint NOT NULL,
                                  `product_id` int NOT NULL,
                                  `receipt_id` bigint NOT NULL,
                                  `update_time` datetime NOT NULL,
                                  PRIMARY KEY (`player_id`, `product_id`)
                                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

            return CreateDBCommand(text, group);
        }

        public static IDBCommand CreateCoMissionStateTable(int group)
        {
            const string text = @"CREATE TABLE IF NOT EXISTS `co_mission_state_{0:00}` (
                                  `player_id` bigint NOT NULL,
                                  `mission_id` bigint NOT NULL,
                                  `subject_1` int NOT NULL,
                                  `subject_2` int NOT NULL,
                                  `subject_3` int NOT NULL,
                                  `subject_4` int NOT NULL,
                                  `subject_5` int NOT NULL,
                                  `complex` int NOT NULL DEFAULT '0',
                                  PRIMARY KEY (`player_id`, `mission_id`)
                                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

            return CreateDBCommand(text, group);
        }

        public static IDBCommand CreateMyLordTable(int group)
        {
            const string text = @"CREATE TABLE IF NOT EXISTS `mylord_{0:00}` (
                                  `player_id` bigint NOT NULL,
                                  `rescue_miss` int NOT NULL DEFAULT '0',
                                  `max_rescue_miss` int NOT NULL DEFAULT '0',
                                  PRIMARY KEY (`player_id`)
                                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

            return CreateDBCommand(text, group);
        }

        public static IDBCommand CreateSellCacheTable(int group)
        {
            const string text = @"CREATE TABLE IF NOT EXISTS `sell_cache_{0:00}` (
                                  `player_id` bigint NOT NULL,
                                  `card_id` int NOT NULL,
                                  `kizuna` int NOT NULL,
                                  KEY `i_player_id` (`player_id`)
                                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

            return CreateDBCommand(text, group);
        }

        public static IDBCommand CreateInfinityTable(int group)
        {
            const string text = @"CREATE TABLE IF NOT EXISTS `infinity_{0:00}` (
                                  `id` bigint NOT NULL AUTO_INCREMENT,
                                  `player_id` bigint NOT NULL,
                                  `event_id` bigint NOT NULL,
                                  `point` int NOT NULL,
                                  `update_time` datetime NOT NULL,
                                  PRIMARY KEY (`id`),
                                  KEY `i_player_id` (`player_id`)
                                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

            return CreateDBCommand(text, group);
        }

        public static IDBCommand CreateFellowTable(int group)
        {
            const string text = @"CREATE TABLE IF NOT EXISTS `fellow_{0:00}` (
                                  `id` bigint NOT NULL AUTO_INCREMENT,
                                  `player_id` bigint NOT NULL,
                                  `fellow_group` int NOT NULL,
                                  `fellow_id` bigint NOT NULL,
                                  `complex` int NOT NULL DEFAULT '0',
                                  `update_time` datetime NOT NULL,
                                  PRIMARY KEY (`id`),
                                  KEY `i_player_id` (`player_id`)
                                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

            return CreateDBCommand(text, group);
        }

        public static IDBCommand CreateSnapshotTable(int group)
        {
            const string text = @"CREATE TABLE IF NOT EXISTS `snapshot_{0:00}` (
                                  `player_id` bigint NOT NULL,
                                  `data` text NOT NULL,
                                  `update_time` datetime NOT NULL,
                                  PRIMARY KEY (`player_id`)
                                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

            return CreateDBCommand(text, group);
        }

        public static IDBCommand CreateSocialPurchaseTable(int group)
        {
            const string text = @"CREATE TABLE IF NOT EXISTS `social_purchase_{0:00}` (
                                  `id` bigint NOT NULL AUTO_INCREMENT,
                                  `player_id` bigint NOT NULL,
                                  `article_id` int NOT NULL,
                                  `cost` int NOT NULL,
                                  `timestamp` datetime NOT NULL,
                                  PRIMARY KEY (`id`),
                                  KEY `i_player_id` (`player_id`)
                                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

            return CreateDBCommand(text, group);
        }

        public static IDBCommand CreateDailyEPTable(int group)
        {
            const string text = @"CREATE TABLE IF NOT EXISTS `daily_ep_{0:00}` (
                                  `id` bigint NOT NULL AUTO_INCREMENT,
                                  `player_id` bigint NOT NULL,
                                  `point` int NOT NULL,
                                  `update_time` datetime NOT NULL,
                                  PRIMARY KEY (`id`),
                                  KEY `i_player_id` (`player_id`)
                                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

            return CreateDBCommand(text, group);
        }

        public static IDBCommand CreateCrusadeTable(int group)
        {
            const string text = @"CREATE TABLE IF NOT EXISTS `crusade_{0:00}` (
                                  `id` bigint NOT NULL AUTO_INCREMENT,
                                  `player_id` bigint NOT NULL,
                                  `discover` int NOT NULL DEFAULT '0',
                                  `point` int NOT NULL,
                                  `update_time` datetime NOT NULL,
                                  PRIMARY KEY (`id`),
                                  KEY `i_player_id` (`player_id`)
                                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

            return CreateDBCommand(text, group);
        }

        public static IDBCommand CreateCrusadeCacheTable(int group)
        {
            const string text = @"CREATE TABLE IF NOT EXISTS `crusade_cache_{0:00}` (
                                  `id` bigint NOT NULL AUTO_INCREMENT,
                                  `player_id` bigint NOT NULL,
                                  `quest_id` bigint NOT NULL,
                                  `is_self` bit NOT NULL,
                                  PRIMARY KEY (`id`),
                                  KEY `i_player_id` (`player_id`)
                                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

            return CreateDBCommand(text, group);
        }

        public static IDBCommand HotFixQuestCache(int group)
        {
            const string text = @"ALTER TABLE `quest_cache_{0:00}` ADD `data` text NOT NULL DEFAULT '';";

            return CreateDBCommand(text, group);
        }

        public static IDBCommand HotFixEquipment(int group)
        {
            const string text = @"ALTER TABLE `equipment_{0:00}` 
                                  ADD `counter` int NOT NULL DEFAULT '0',
                                  ADD `counter_type` int NOT NULL DEFAULT '0';";

            return CreateDBCommand(text, group);
        }

        public static IDBCommand CreateFaithTable(int group)
        {
            const string text = @"CREATE TABLE IF NOT EXISTS `faith_{0:00}` (
                                  `id` bigint NOT NULL AUTO_INCREMENT,
                                  `player_id` bigint NOT NULL,
                                  `index` int NOT NULL,
                                  `data_ref` int NOT NULL,
                                  `result` int NOT NULL,
                                  `end_time` datetime NOT NULL,
                                  PRIMARY KEY (`id`),
                                  KEY `i_player_id` (`player_id`)
                                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

            return CreateDBCommand(text, group);
        }

        public static IDBCommand CreateFaithClaimTable(int group)
        {
            const string text = @"CREATE TABLE IF NOT EXISTS `faith_claim_{0:00}` (
                                  `player_id` bigint NOT NULL,
                                  `claim_time` datetime NOT NULL,
                                  KEY `i_player_id` (`player_id`)
                                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

            return CreateDBCommand(text, group);
        }

        public static IDBCommand CreateTacticsTable(int group)
        {
            const string text = @"CREATE TABLE IF NOT EXISTS `tactics_{0:00}` (
                                  `player_id` bigint NOT NULL,
                                  `tactics_id` int NOT NULL,
                                  `timestamp` datetime NOT NULL,
                                  KEY `i_player_id` (`player_id`)
                                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

            return CreateDBCommand(text, group);
        }

        public static IDBCommand CreatePVPContextTable(int group)
        {
            const string text = @"CREATE TABLE IF NOT EXISTS `pvp_context_{0:00}` (
                                  `player_id` bigint NOT NULL,
                                  `card_id` int NOT NULL,
                                  `quest_design` text NOT NULL,
                                  `bp_consumed` int NOT NULL DEFAULT '0',
                                  `bp_update_time` datetime NOT NULL,
                                  `update_time` datetime NOT NULL,
                                  PRIMARY KEY (`player_id`)
                                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

            return CreateDBCommand(text, group);
        }

        public static IDBCommand CreatePVPSkillTable(int group)
        {
            const string text = @"CREATE TABLE IF NOT EXISTS `pvp_skill_{0:00}` (
                                  `player_id` bigint NOT NULL,
                                  `skill_id` int NOT NULL,
                                  `timestamp` datetime NOT NULL,
                                  KEY `i_player_id` (`player_id`)
                                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

            return CreateDBCommand(text, group);
        }

        public static IDBCommand CreatePVPBattleTable(int group)
        {
            const string text = @"CREATE TABLE IF NOT EXISTS `pvp_battle_{0:00}` (
                                  `id` bigint NOT NULL AUTO_INCREMENT,
                                  `player_id` bigint NOT NULL,
                                  `quest_id` bigint NOT NULL,
                                  `target_group` int NOT NULL,
                                  `target_id` bigint NOT NULL,
                                  `win` int NOT NULL,
                                  `lose` int NOT NULL,
                                  `result` int NOT NULL DEFAULT '0',
                                  `timestamp` datetime NOT NULL,
                                  PRIMARY KEY (`id`),
                                  KEY `i_player_id` (`player_id`),
                                  KEY `i_quest_id` (`quest_id`)
                                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

            return CreateDBCommand(text, group);
        }

        #endregion
    }
}
