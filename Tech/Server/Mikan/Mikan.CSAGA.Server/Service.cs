﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mikan;
using Mikan.Server;
using System.Reflection;

namespace Mikan.CSAGA.Server
{
    [ActiveService]
    public class Service : HttpService<Context>
    {
        public override IEnumerable<string> GetURIList()
        {
            yield return Config.URI;
        }

        public override ICommandTask<Context> ProcessInformalCommand(ICommand cmd)
        {
            if (cmd is NotLoginYetCommand) return new Tasks.NotLoginYetCommandTask();

            return new Tasks.IllegalCommandTask();
        }

        protected override UnmanagedHttpHandler GetUnmanagedHttpHandler()
        {
            return new Tasks.ExternalHttpHandler(this);
        }

        protected override IEnumerable<string> GetUnmanagedURIList()
        {
            yield return "mycard/restore";
            yield return "mycard/inquire";
        }

        public override IHttpTask ProcessInformalCommand()
        {
            return new Tasks.ExternalCommandTask();
        }

        protected override ISerializer<ICommand> CreateSerializer()
        {
            return new Serializer();
        }

        protected override Assembly GetTasksAssembly()
        {
            return Assembly.GetExecutingAssembly();
        }

        public override void Execute(IHost host)
        {
            base.Execute(host);

            AddKernelEvent(new Tasks.InitialTask(this));
        }

        public override void Dispose()
        {
            AddKernelEvent(new Tasks.FinalizeTask());

            base.Dispose();

        }
    }
}
