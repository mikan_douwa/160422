﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mikan.CSAGA.ConstData;

namespace Mikan.CSAGA.Server
{
    public class RandomShop : DataCacheList
    {

        public override IEnumerable<int> Select(long seed)
        {
            var id = (int)(seed >> 32);
            var N0 = (int)(seed & int.MaxValue);

            Cache cache = null;

            if (!list.TryGetValue(id, out cache) || cache.Default.Total == 0) yield break;
            
            for (int i = 0; i < Global.Tables.RandomShopSlotCalc.Limit; ++i)
            {
                N0 = MathUtils.Seed.Next(N0);
                var rand = (int)(N0 % cache.Default.Total);

                var dataId = cache.Default.Get(rand);
                if (dataId != 0) yield return dataId;
            }
        }

        public bool Equals(List<ConstData.GachaList> newList)
        {
            if (CurrentID == 0) return false;

            var current = list.SafeGetValue(CurrentID);

            if (current == null) return false;

            foreach (var item in newList)
            {
                var data = Global.Tables.Drop[item.n_GOODS];
                if (!current.Default.Equals(item.n_ID, data.n_VALUE)) return false;
            }

            return current.Default.Count == newList.Count;
        }
    }
}
