﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mikan.CSAGA.Server
{
    abstract public class ScheduleTask : IScheduleTask
    {
        virtual public bool Immediately { get { return false; } }

        abstract public void Execute(int times);

        virtual public void Dispose()
        {
            
        }
    }
}
