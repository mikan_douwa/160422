﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mikan.Server;

namespace Mikan.CSAGA.Server.Tasks
{

    public interface IAssistObj
    {
        void Sync(CSAGACommand syncTarget);
        void Apply(Player player, IDBConnection db, DateTime currentTime);
        void Commit(Player player);
    }

    #region 掉落物/獎勵
    public class DropObj : IAssistObj
    {
        public List<Card> NewCardList = new List<Card>();
        public List<Equipment> NewEquipmentList = new List<Equipment>();
        public Dictionary<int, ValuePair> ItemDiffs = new Dictionary<int, ValuePair>();
        public Dictionary<int, int> LuckDiffs = new Dictionary<int, int>();
        public ValuePair CrystalDiff;
        public ValuePair CoinDiff;
        public ValuePair GemDiff;
        public ValuePair FPDiff;
        private Player player;
        private RewardSource source;
        private int addition;

        private LuckObj luckObj;

        private LimitedQuest quest;

        private HashSet<int> tactics = new HashSet<int>();
        
        public DropObj(Player player, RewardSource source, int addition = 0, int crystalAdd = 0)
        {
            this.player = player;
            this.source = source;
            this.addition = addition;

            if (crystalAdd != 0)
            {
                var origin = player.PlayerInfo[PropertyID.Crystal];
                CrystalDiff = ValuePair.Create(origin, Math.Min(origin + crystalAdd, Global.Tables.CrystalMax));
            }
        }
        public bool Add(int id)
        {
            return Add(Global.Tables.Drop[id]);
        }

        public bool Add(int itemId, int itemCount)
        {
            var origin = player.ItemInfo.Count(itemId);
            var diff = Math.Min(origin + itemCount, Global.Tables.ItemLimit) - origin;

            var obj = ItemDiffs.SafeGetValue(itemId);
            if (obj != null)
            {
                obj.NewValue = Math.Min(obj.NewValue + itemCount, Global.Tables.ItemLimit);
            }
            else
            {
                obj = ValuePair.Create(origin, Math.Min(origin + itemCount, Global.Tables.ItemLimit));
                ItemDiffs.Add(itemId, obj);
            }

            if (diff < itemCount) return false;

            return true;
        }

        public bool Add(ConstData.Drop data)
        {
            switch (data.n_DROP_TYPE)
            {
                case DropType.Crystal:
                    {
                        CrystalDiff = AddValue(CrystalDiff, player.PlayerInfo[PropertyID.Crystal], data.n_COUNT, Global.Tables.CrystalMax);
                        if (CrystalDiff.Diff < data.n_COUNT) return false;
                        break;
                    }
                case DropType.FP:
                    {
                        FPDiff = AddValue(FPDiff, player.PlayerInfo[PropertyID.FP], data.n_COUNT, Global.Tables.FPMax);
                        if (FPDiff.Diff < data.n_COUNT) return false;
                        break;
                    }
                case DropType.Coin:
                    {
                        CoinDiff = AddValue(CoinDiff, player.PlayerInfo[PropertyID.Coin], data.n_COUNT, Global.Tables.CoinMax);
                        if (CoinDiff.Diff < data.n_COUNT) return false;
                        break;
                    }
                case DropType.Gem:
                    {
                        GemDiff = AddValue(GemDiff, player.PurchaseInfo.Gem, data.n_COUNT, Global.Tables.GemMax);
                        if (GemDiff.Diff < data.n_COUNT) return false;
                        break;
                    }
                case DropType.Card:
                    {
                        var card = new Card { ID = data.n_DROP_ID, Exp = CardUtils.CalcExp(data.n_DROP_ID, data.n_COUNT) };
                        card.Complex = new Complex();
                        card.Rare = data.n_RARE;
                        NewCardList.Add(card);

                        if (luckObj == null) luckObj = new LuckObj();
                        luckObj.Add(data.n_DROP_ID, data.n_LUCK + 1);
                        break;
                    }
                case DropType.Item:
                    {
                        var itemData = Global.Tables.Item[data.n_DROP_ID];
                        if (itemData.n_TYPE == ItemType.Equipment)
                        {
                            NewEquipmentList.Add(Calc.Equipment.Generate<Equipment>(data.n_DROP_ID));
                        }
                        else
                        {

                            if (!Add(data.n_DROP_ID, data.n_COUNT)) return false;
                        }
                        break;
                    }
                case DropType.Quest:
                    {
                        var questId = data.n_DROP_ID;
                        var questData = Global.Tables.QuestDesign[questId];

                        quest = new LimitedQuest { QuestID = questId, EndTime = Global.CurrentTime.AddMinutes(questData.n_DURATION) };

                        var origin = player.QuestInfo.GetLimited(questId);
                        if (origin != null) quest.Serial = origin.Serial;
                        /*
                        var trigger = Calc.Quest.TriggerLimited(data.n_ID);

                        if (trigger > 0)
                        {
                            var triggerData = Global.Tables.QuestDesign[trigger];

                            obj.TriggerQuest = new LimitedQuest { QuestID = trigger, EndTime = Global.CurrentTime.AddMinutes(triggerData.n_DURATION) };

                            var origin = player.QuestInfo.GetLimited(trigger);
                            if (origin != null) obj.TriggerQuest.Serial = origin.Serial;

                            syncTarget.LimitedQuestSync.List.Add(obj.TriggerQuest);
                        }
                        */
                        break;
                    }
                case DropType.Tactics:
                    {
                        if (!player.TacticsInfo.Contains(data.n_DROP_ID))
                            tactics.Add(data.n_DROP_ID);
                        break;
                    }
            }

            return true;

        }

        private ValuePair AddValue(ValuePair current, int origin, int add, int max)
        {
            if (current != null)
            {
                current.NewValue = Math.Min(current.NewValue + add, max);
                return current;
            }

            return ValuePair.Create(origin, Math.Min(origin + add, max));
        }

        
        public void Sync(CSAGACommand syncTarget)
        {
            SyncProperty(syncTarget, PropertyID.Crystal, CrystalDiff);
            SyncProperty(syncTarget, PropertyID.Coin, CoinDiff);
            SyncProperty(syncTarget, PropertyID.Gem, GemDiff);
            SyncProperty(syncTarget, PropertyID.FP, FPDiff);

            foreach (var item in ItemDiffs)
            {
                if (item.Value.Diff == 0) continue;
                syncTarget.AddSyncData(new SyncItem { Diff = Diff.Create(item.Key, item.Value.OriginValue, item.Value.NewValue) });
            }

            if (NewCardList.Count > 0)
            {
                if (syncTarget.CardSync.AddList == null) syncTarget.CardSync.AddList = new List<Internal.CardObj>(NewCardList.Count);
                foreach (var item in NewCardList) syncTarget.CardSync.AddList.Add(item);
            }

            if (NewEquipmentList.Count > 0)
            {
                if (syncTarget.EquipmentSync.AddList == null) syncTarget.EquipmentSync.AddList = new List<Internal.EquipmentObj>(NewEquipmentList.Count);
                foreach (var item in NewEquipmentList) syncTarget.EquipmentSync.AddList.Add(item);
            }

            if (luckObj != null) luckObj.Sync(syncTarget);

            if (quest != null) syncTarget.LimitedQuestSync.List.Add(quest);

            foreach (var item in tactics)
            {
                var syncData = new SyncTactics { TacticsID = item };
                syncTarget.AddSyncData(syncData);
            }
        }

        private void SyncProperty(CSAGACommand syncTarget, PropertyID id, ValuePair value)
        {
            if (value != null && value.Diff != 0)
                syncTarget.PlayerSync(id, value.OriginValue, value.NewValue);
        }

        public void Apply(Player player, IDBConnection db, DateTime currentTime)
        {
            if (CrystalDiff != null && CrystalDiff.Diff != 0)
            {
                var cmd = DBCommandBuilder.AddCrystal(player.ID, CrystalDiff.Diff);
                db.SimpleQuery(cmd);
            }
            if (CoinDiff != null && CoinDiff.Diff != 0)
            {
                var cmd = DBCommandBuilder.AddCoin(player.ID, CoinDiff.Diff);
                db.SimpleQuery(cmd);
            }
            if (FPDiff != null && FPDiff.Diff != 0)
            {
                var cmd = DBCommandBuilder.AddFP(player.ID, FPDiff.Diff);
                db.SimpleQuery(cmd);
            }
            if (GemDiff != null && GemDiff.Diff != 0)
            {
                var cmd = DBCommandBuilder.InsertGem(player.ID, source, GemDiff.Diff, currentTime, addition);
                db.SimpleQuery(cmd);
            }

            foreach (var item in ItemDiffs)
            {
                if (item.Value.Diff == 0) continue;
                var cmd = DBCommandBuilder.IncreaseItemCount(player.ID, item.Key, item.Value.Diff);
                db.SimpleQuery(cmd);
            }

            foreach (var item in NewCardList)
            {
                var cmd = DBCommandBuilder.InsertCard(player.ID, item.ID, item.Complex.Value, item.Exp);
                item.Serial = Serial.Create(db.DoQuery<Int64>(cmd));
            }

            foreach (var item in NewEquipmentList)
            {
                var cmd = DBCommandBuilder.InsertEquipment(player.ID, item.ID, item.Hp, item.Atk, item.Rev, item.Chg, item[PropertyID.EffectID], item[PropertyID.Counter], item[PropertyID.CounterType]);
                item.Serial = db.DoQuery<Int64>(cmd);
            }

            if (luckObj != null) luckObj.Apply(player, db, currentTime);

            if (quest != null)
            {
                if (quest.Serial == 0)
                {
                    var cmd = DBCommandBuilder.InsertLimitedQuest(player.ID, quest.QuestID, quest.EndTime);
                    quest.Serial = db.DoQuery<Int64>(cmd);
                }
                else
                {
                    var cmd = DBCommandBuilder.ResetLimitedQuest(player.ID, quest.Serial, quest.EndTime);
                    db.SimpleQuery(cmd);
                }
            }

            foreach (var item in tactics)
            {
                var cmd = DBCommandBuilder.InsertTactics(player.ID, item, currentTime);
                db.SimpleQuery(cmd);
            }
        }

        public void Commit(Player player)
        {
            if (CrystalDiff != null && CrystalDiff.Diff != 0)
                player.PlayerInfo[PropertyID.Crystal] += CrystalDiff.Diff;

            if (CoinDiff != null && CoinDiff.Diff != 0)
                player.PlayerInfo[PropertyID.Coin] += CoinDiff.Diff;

            if (GemDiff != null && GemDiff.Diff != 0)
                player.PurchaseInfo.AddGem(GemDiff.Diff);

            foreach (var item in NewCardList) player.CardInfo.Add(item);

            foreach (var item in NewEquipmentList) player.EquipmentInfo.Add(item);

            foreach (var item in ItemDiffs)
            {
                if (item.Value.Diff != 0) player.ItemInfo.Add(item.Key, item.Value.Diff);
            }

            if (luckObj != null) luckObj.Commit(player);

            if (quest != null) player.QuestInfo.UpdateLimitedQuest(quest);

            foreach (var item in tactics) player.TacticsInfo.Add(item);

        }
    }
    #endregion

    #region 扣錢
    public class PaymentObj : IAssistObj
    {
        private ArticleID article;
        private DropType coinType;
        private int cost;
        private int coinId;

        private int origin;

        private PropertyID propertyId;

        private int iapCost;
        private int iapGem;

        private bool iapLimited = false;

        public int Origin { get { return iapLimited ? iapGem : origin; } }

        public int IAPCost { get { return iapCost; } }

        public PaymentObj(Player player, ArticleID article, DropType coinType, int cost, int coinId = 0)
        {
            this.article = article;
            this.coinType = coinType;
            this.cost = cost;
            this.coinId = coinId;

            switch (coinType)
            {
                case DropType.Gem:
                case DropType.IAPGem:
                    propertyId = PropertyID.Gem;
                    origin = player.PurchaseInfo.Gem;
                    iapGem = player.PurchaseInfo.IAPGem;

                    if (coinType == DropType.IAPGem)
                    {
                        this.coinType = DropType.Gem;
                        iapCost = cost;
                        iapLimited = true;
                    }
                    else
                    {
                        var free = origin - iapGem;
                        if (cost > free)
                            iapCost = cost - free;
                    }
                    return;
                case DropType.Item:
                    propertyId = PropertyID.Invalid;
                    origin = player.ItemInfo.Count(coinId);
                    return;
                case DropType.Crystal:
                    propertyId = PropertyID.Crystal;
                    coinType = DropType.Crystal;
                    break;
                case DropType.Coin:
                    propertyId = PropertyID.Coin;
                    coinType = DropType.Coin;
                    break;
                case DropType.FP:
                    propertyId = PropertyID.FP;
                    coinType = DropType.FP;
                    break;
            }

            origin = player.PlayerInfo[this.coinType];
        }



        public void Sync(CSAGACommand syncTarget)
        {
            if (coinType == DropType.Item)
                syncTarget.AddSyncData(new SyncItem { Diff = Diff.Create(coinId, origin, origin - cost) });
            else if (propertyId != PropertyID.Invalid)
                syncTarget.PlayerSync(propertyId, origin, origin - cost);

            if (iapCost > 0) syncTarget.PlayerSync(PropertyID.IAPGem, iapGem, iapGem - iapCost);
        }

        public void Apply(Player player, IDBConnection db, DateTime currentTime)
        {
            var cmd = default(IDBCommand);

            switch (coinType)
            {
                case DropType.Gem:
                    cmd = DBCommandBuilder.InsertPurchase(player.ID, article, cost, iapCost, currentTime);
                    break;
                case DropType.Crystal:
                    cmd = DBCommandBuilder.AddCrystal(player.ID, -cost);
                    break;
                case DropType.Coin:
                    cmd = DBCommandBuilder.AddCoin(player.ID, -cost);
                    break;
                case DropType.FP:
                    cmd = DBCommandBuilder.AddFP(player.ID, -cost);
                    break;
                case DropType.Item:
                    cmd = DBCommandBuilder.IncreaseItemCount(player.ID, coinId, -cost);
                    break;
            }

            if (cmd != null) db.SimpleQuery(cmd);
        }

        public void Commit(Player player)
        {
            switch (coinType)
            {
                case DropType.Crystal:
                    player.PlayerInfo[PropertyID.Crystal] -= cost;
                    break;
                case DropType.Coin:
                    player.PlayerInfo[PropertyID.Coin] -= cost;
                    break;
                case DropType.Gem:
                    player.PurchaseInfo.AddArticle(article, cost, iapCost, Global.CurrentTime);
                    break;
                case DropType.FP:
                    player.PlayerInfo[PropertyID.FP] -= cost;
                    break;
                case DropType.Item:
                    player.ItemInfo.Add(coinId, -cost);
                    break;
            }
        }
    }

    #endregion

    #region 神運
    public class LuckObj : IAssistObj
    {
        public bool IsGacha = false;

        public Dictionary<int, ValuePair> LuckDiffs = new Dictionary<int, ValuePair>();
        private Dictionary<int, int> tmpLuckDiffs = new Dictionary<int, int>();

        private List<int> ids = new List<int>();

        public void Add(int cardId, int luck)
        {
            var origin = 0;
            if (tmpLuckDiffs.TryGetValue(cardId, out origin))
                tmpLuckDiffs[cardId] = origin + luck;
            else
                tmpLuckDiffs[cardId] = luck;

            if (IsGacha) ids.Add(cardId);
        }

        public void Sync(CSAGACommand syncTarget)
        {
            if (LuckDiffs.Count == 0) return;
            if (syncTarget.CardSync.UpdateList == null) syncTarget.CardSync.UpdateList = new List<CardDiff>(LuckDiffs.Count);
            foreach(var item in LuckDiffs) syncTarget.CardSync.UpdateList.Add(CardDiff.Create(Serial.Create(item.Key), PropertyID.Luck, item.Value.OriginValue, item.Value.NewValue));
        }

        public void Apply(Player player, IDBConnection db, DateTime currentTime)
        {
            var cache = new int[ids.Count];

            foreach (var item in tmpLuckDiffs)
            {
                var origin = player.CardInfo.GetLuck(item.Key);
                var data = Global.Tables.Card[item.Key];
                var newVal = Math.Min(origin + item.Value, data.n_LUCK_MAX);
                if (newVal > origin)
                {
                    var diff = newVal - origin;
                    LuckDiffs[item.Key] = ValuePair.Create(origin, newVal);
                    var cmd = DBCommandBuilder.InsertGallery(player.ID, item.Key, diff);
                    db.SimpleQuery(cmd);
                }

                if (IsGacha)
                {
                    if (origin + item.Value > newVal)
                    {
                        var diff = (origin + item.Value) - newVal;

                        // 丟神運道具進禮物箱
                        var count = data.n_LUCK_GIFT % 100;

                        if (count > 0)
                        {
                            for (int i = ids.Count - 1; i >= 0; --i)
                            {
                                if (ids[i] != item.Key) continue;

                                if (--diff < 0) break;

                                cache[i] = count;
                            }
                        }
                    }
                }
            }

            if (IsGacha)
            {
                for (int i = 0; i < ids.Count; ++i)
                {
                    var count = cache[i];
                    if (count == 0) continue;

                    var cmd = DBCommandBuilder.InsertGift(player.ID, RewardSource.GachaFeedback, Global.Tables.GetSystemText(SystemTextID._461_GACHA_FEEDBACK), DropType.Item, count, Global.Tables.LuckItemID);
                    db.SimpleQuery(cmd);
                }
            }
        }

        public void Commit(Player player)
        {
            if (LuckDiffs.Count == 0) return;
            foreach (var item in LuckDiffs) player.CardInfo.AddLuck(item.Key, item.Value.Diff);
        }
    }
    #endregion
}
