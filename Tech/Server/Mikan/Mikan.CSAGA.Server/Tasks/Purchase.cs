﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mikan.CSAGA.Commands.Purchase;
using Mikan.Server;

namespace Mikan.CSAGA.Server.Tasks
{
    [ActiveTask(CommandType = typeof(InfoRequest))]
    public class PurchaseInfoTask : PlayerTask<InfoRequest, InfoResponse>
    {
        protected override ErrorCode SafeProcessCommand(InfoResponse res)
        {
            res.PurchaseInfo = Player.PurchaseInfo.Serialize();

            return ErrorCode.Success;
        }
    }

    [ActiveTask(CommandType = typeof(ArticleRequest))]
    public class PurchaseArticleTask : PlayerTask<ArticleRequest, ArticleResponse>
    {
        private LimitedQuest limited;
        protected override void OnCommit(Context context)
        {
            switch (Command.ArticleID)
            {
                case ArticleID.TalkPoint:
                    Player.PlayerInfo.UpdateTPConsumed(0, CurrentTime);
                    break;
                case ArticleID.RescuePoint:
                    Player.QuestInfo.RescueCount = Global.Tables.HelpMax * Global.Tables.HelpScale;
                    Player.QuestInfo.RescueUpdateTime = CurrentTime;
                    break;
                case ArticleID.Continue:
                    context.LogQuest(Player.QuestInfo.Current.Obj.QuestID, LogQuestType.Continue);
                    break;
                case ArticleID.CrusadePoint:
                    Player.QuestInfo.Crusade.Point = Global.Tables.CrusadeMax * Global.Tables.CrusadeScale;
                    Player.QuestInfo.Crusade.UpdateTime = CurrentTime;
                    break;
            }

            if (limited != null) limited.PlayCount = 0;

            base.OnCommit(context);
        }
        protected override ErrorCode SafeProcessCommand(ArticleResponse res)
        {
            var cost = 0;

            switch(Command.ArticleID)
            {
                case ArticleID.CardSpace:
                    cost = Global.Tables.CardSpaceCost;
                    break;
                case ArticleID.TalkPoint:
                    cost = Global.Tables.ChargeTPCost;
                    break;
                case ArticleID.Continue:
                    cost = Global.Tables.ContinueCost;
                    break;
                case ArticleID.QuestCount:
                    limited = Player.QuestInfo.GetLimited(Command.Param);
                    if (limited == null || limited.PlayCount == 0 || limited.EndTime < CurrentTime.AddMinutes(3)) return ErrorCode.QuestIllegal;
                    cost = Global.Tables.ChargeQuestCost;
                    break;
                case ArticleID.RandomShopSlot:
                    cost = Global.Tables.RandomShopSlotCalc.Cost;
                    break;
                case ArticleID.RescuePoint:
                    cost = Global.Tables.ChargeRescueCost;
                    break;
                case ArticleID.CrusadePoint:
                    cost = Global.Tables.ChargeCrusadeCost;
                    break;
            }

            var payment = new PaymentObj(Player, Command.ArticleID, DropType.Gem, cost);

            if (payment.Origin < cost) return ErrorCode.GemNotEnough;

            AddAssistObj(payment);

            using (var db = AcquireDB())
            {
                using (var trans = db.BeginTransaction())
                {
                    ApplyAssists(db);

                    switch (Command.ArticleID)
                    {
                        case ArticleID.TalkPoint:
                        {
                            var cmd = DBCommandBuilder.UpdateTPConsumed(PublicID, 0, CurrentTime);
                            db.SimpleQuery(cmd);
                            break;
                        }
                        case ArticleID.QuestCount:
                        {
                            var cmd = DBCommandBuilder.ResetLimitedQuest(PublicID, limited.Serial, limited.EndTime, 0);
                            db.SimpleQuery(cmd);
                            break;
                        }
                        case ArticleID.RescuePoint:
                        {
                            var cmd = DBCommandBuilder.UpdateRescue(PublicID, Global.Tables.HelpMax * Global.Tables.HelpScale, CurrentTime);
                            db.SimpleQuery(cmd);
                            break;
                        }
                        case ArticleID.CrusadePoint:
                        {
                            var cmd = DBCommandBuilder.UpdateCrusade(PublicID, Global.Tables.CrusadeMax * Global.Tables.CrusadeScale, CurrentTime);
                            db.SimpleQuery(cmd);
                            break;
                        }
                    }
                    trans.Commit();
                }
            }

            res.ArticleID = Command.ArticleID;
            res.Count = Player.PurchaseInfo.Count(Command.ArticleID) + 1;

            switch (Command.ArticleID)
            {
                case ArticleID.TalkPoint:
                    res.PlayerSync(PropertyID.TPConsumed, Player.PlayerInfo.TPConsumed, 0);
                    break;
                case ArticleID.QuestCount:
                    res.LimitedQuestSync.List.Add(new Internal.LimitedQuestObj{ QuestID = limited.QuestID, PlayCount = 0, EndTime = limited.EndTime });
                    break;
                case ArticleID.RescuePoint:
                    res.AddSyncData(new SyncRescue { Count = Global.Tables.HelpMax * Global.Tables.HelpScale, UpdateTime = CurrentTime });
                    break;
                case ArticleID.CrusadePoint:
                    res.AddSyncData(new SyncCrusade { Point = Global.Tables.CrusadeMax * Global.Tables.CrusadeScale, UpdateTime = CurrentTime });
                    break;
            }

            return ErrorCode.Success;
        }
    }
}
