﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mikan.Server;
using Mikan.CSAGA.Commands.Quest;

namespace Mikan.CSAGA.Server.Tasks
{
    #region Obj

    public class EndPVPBattleOBj
    {
        public int Point;
        public PVPContextList PVPList;
        public DBPVPBattle Cache;
        public PublicID TargetID;
        public int TargetCardID;
    }

    public class EndQuestObj
    {
        public class CardDiffObj
        {
            public Card Card;
            public int ExpDiff;
            public int KizunaDiff;
        }
        public bool IsSucc;
        public QuestCache Cache;
        public int ExpDiff;
        public List<CardDiffObj> Cards;
        public DropObj Drop;
        public Card TargetCard;
        public Complex TargetCardComlexDiff;
        /// <summary>
        /// 進行中的任務
        /// </summary>
        public LimitedQuest LimitedQuest;
        /// <summary>
        /// 觸發的任務
        /// </summary>
        public LimitedQuest TriggerQuest;

        public EventScheduleList EventScheduleList;
        public EventScheduleObj EventSchedule;
        public int EventPoint;

        public SharedQuestList SharedQuestList;
        public List<SharedQuest> MySharedQuestList;

        public bool TriggerRescue;
        public int RescueCount;
        public DateTime RescueUpdateTime;

        public DBMyLord MyLord;
        public int AddRescueMiss;

        public Infinity Infinity;

        public PublicID Fellow;
        public int FPDiff;

        public int SupportCardID;
        public DBDailyEP DailyEP;
        public SyncDailyEventPoint SyncDailyEP = null;

        public List<int> History;

        public SharedQuest Crusade;
        public int CrusadePoint;
        public DateTime CrusadeUpdateTime;
        public bool IsCrusadeFirstTime;
        public SyncChallenge SyncChallenge = null;

        public int NewComplex = 0;
        public int ChallengeCount = 0;

        public EndPVPBattleOBj Battle;
    }

    #endregion

    #region Task

    [ActiveTask(CommandType = typeof(EndRequest))]
    public class QuestEndTask : PlayerTask<EndRequest, EndResponse>
    {
        private EndQuestObj obj = new EndQuestObj();

        protected override void OnPrepare(Context context)
        {
            base.OnPrepare(context);
            obj.Cache = Player.QuestInfo.Current;

            obj.EventScheduleList = context.EventScheduleList;

            obj.DailyEP = Player.DailyEP;

            obj.History = Command.History;

            obj.SharedQuestList = context.SharedQuestList;

            obj.MySharedQuestList = Player.QuestInfo.SharedList;

            if (Command.IsQuestSucc && obj.Cache != null && obj.Cache.Obj.QuestID == Tables.PVPQuestID)
                obj.Battle = new EndPVPBattleOBj { PVPList = context.PVPList };
        }
        protected override void OnCommit(Context context)
        {
            QuestHelper.CommitQuest(Player, obj);

            if (obj.IsSucc)
            {
                AddTrophyTask(new QuestTrophyTask { PublicID = PublicID, QuestID = obj.Cache.Obj.QuestID, ChallengeCount = obj.ChallengeCount });

                Player.UpdateSnapshot(CurrentTime);

                context.LogQuest(obj.Cache.Obj.QuestID, LogQuestType.Clear);

                AddLog(LogType.Quest, obj.Cache.Obj.QuestID);
            }


            base.OnCommit(context);
        }
        protected override ErrorCode SafeProcessCommand(EndResponse res)
        {
            obj.SupportCardID = Command.SupportCard;
            obj.SyncChallenge = Command.SyncList.Find(o => o is SyncChallenge) as SyncChallenge;


            var errCode = SetupPVP();

            if (errCode != ErrorCode.Success) return errCode;

            try
            {
                return QuestHelper.EndQuest(Player, Command.Serial, Command.List, Command.IsQuestSucc, Command.IsRescueSucc, Command.IsAutoBattleEnabled, obj, res);
            }
            catch
            {
                KernelService.Logger.Debug("EndQuest.[Retry]");
                return QuestHelper.EndQuest(Player, Command.Serial, Command.List, Command.IsQuestSucc, Command.IsRescueSucc, Command.IsAutoBattleEnabled, obj, res, true);
            }
        }

        private ErrorCode SetupPVP()
        {
            if (obj.Battle != null)
            {
                var battle = PVPServerUtils.QueryBattle(PublicID, obj.Cache.Obj.ID);

                if (battle != null)
                {
                    if (Player.Context.BlackList.Contains(PublicID)) return ErrorCode.Forbidden;

                    if (!Global.IsDevServer && Player.Forbidden) return ErrorCode.Forbidden;

                    obj.Battle.Cache = battle;
                    obj.Battle.Point = Command.IsPVPWin ? battle.Win : battle.Lose;
                    obj.Battle.TargetID = PublicID.Create(battle.TargetGroup, battle.TargetID);

                    obj.Battle.PVPList.SafeCall(() =>
                    {
                        var target = obj.Battle.PVPList.GetUnsafe(obj.Battle.TargetID);
                        obj.Battle.TargetCardID = target.CardID;
                    });
                }
                else
                    obj.Battle = null;
            }

            return ErrorCode.Success;
        }
    }

    #endregion

    #region Utils

    public partial class QuestHelper
    {
        public static ErrorCode EndQuest(Player player, Int64 questSerial, List<Int64> cards, bool isSucc, bool isRescueSucc, bool isAutoBattleEnabled, EndQuestObj obj, EndResponse syncTarget, bool debug = false)
        {
            Debug(debug, "EndQuest.[Begin]");
            Debug(debug, string.Format("EndQuest.[pass 0], {0}, {1}, {2}, {3}", obj.Cache, obj.Cache.Obj.ID, obj.Cache.Complex.Value, obj.Cache.Obj.QuestID));

            if (obj.Cache == null || obj.Cache.Obj.ID != questSerial || obj.Cache.Complex[0]) return ErrorCode.QuestIllegal;
            Debug(debug, "EndQuest.[pass 0.1]");
            var state = player.QuestInfo[obj.Cache.Obj.QuestID];
            var isCleared = state != null ? state.IsCleared : false;
            Debug(debug, "EndQuest.[pass 0.2]");
            var originComplex = state != null ? state.Complex.Value : 0;
            var complex = new ComplexI32();

            obj.IsSucc = isSucc;
            Debug(debug, "EndQuest.[pass 0.3]");
            var isCardEvent = player.CardInfo.Current != null && player.CardInfo.Current.QuestID == questSerial;
            Debug(debug, "EndQuest.[pass 0.4]");
            var data = Global.Tables.QuestDesign[obj.Cache.Obj.QuestID];

            var currentTime = Global.CurrentTime;
            Debug(debug, "EndQuest.[pass 0.5]");

            var originExp = player.PlayerInfo[PropertyID.Exp];
            var originCystal = player.PlayerInfo[PropertyID.Crystal];
            var originCoin = player.PlayerInfo[PropertyID.Coin];
            var originFP = player.PlayerInfo[PropertyID.FP];
            Debug(debug, string.Format("EndQuest.[pass 0.6], {0}", data != null));
            var current = obj.EventScheduleList != null ? obj.EventScheduleList.GetCurrent(data.n_MAIN_QUEST) : null;
            var infinity = default(Infinity);
            var isRegular = false;

            Debug(debug, "EndQuest.[pass 1]");
            var extraCrystal = 0;
            var extraRankExp = 0;
            foreach (var item in player.TacticsInfo.Select())
            {
                switch (item.n_EFFECT)
                {
                    case TacticsEffect.Crystal:
                        extraCrystal += item.n_EFFECT_Z;
                        break;
                    case TacticsEffect.RankExp:
                        extraRankExp += item.n_EFFECT_Z;
                        break;
                }
            }


            if (isSucc)
            {
                Debug(debug, "EndQuest.[pass 2]");

                complex[0] = true;

                if (obj.SyncChallenge != null)
                {
                    var challenge = player.Context.ChallengeList.SafeGetValue(data.n_ID);



                    if (challenge != null)
                    {
                        Debug(debug, "EndQuest.[pass 2.1]");

                        var isIllegal = false;
                        foreach (var item in obj.SyncChallenge.List)
                        {
                            var index = challenge.IndexOf(item.ID);
                            if (index < 0)
                            {
                                isIllegal = true;
                                LogChallenge("NotMatch", player.ID, data.n_ID, obj.SyncChallenge);
                                break;
                            }
                        }

                        Debug(debug, "EndQuest.[pass 2.2]");

                        if (!isIllegal)
                        {
                            foreach (var item in obj.SyncChallenge.List)
                            {
                                var index = challenge.IndexOf(item.ID);
                                complex[index + 1] = true;
                            }

                            Debug(debug, "EndQuest.[pass 2.2.1]");
                        }

                        Debug(debug, "EndQuest.[pass 2.3]");
                    }
                    else
                    {
                        LogChallenge("Empty", player.ID, data.n_ID, obj.SyncChallenge);
                    }
                }

                Debug(debug, "EndQuest.[pass 3]");

                var clearExp = data.n_CLEAR_EXP;
                var clearRankExp = data.n_CLEAR_RANKEXP;

                if (isAutoBattleEnabled)
                {
                    clearExp /= 2;
                    clearRankExp /= 2;
                }

                if (extraCrystal > 0)
                    clearExp += (int)Math.Floor(clearExp * extraCrystal * 0.01f);

                if (extraRankExp > 0)
                    clearRankExp += (int)Math.Floor(clearRankExp * extraRankExp * 0.01f);

                obj.Drop = new DropObj(player, RewardSource.Quest, obj.Cache.Obj.QuestID, clearExp);

                Debug(debug, "EndQuest.[pass 4]");

                if (!isCleared)
                {
                    Debug(debug, "EndQuest.[pass 4.1]");
                    foreach (var interactive in Calc.Quest.SelectEvents(obj.Cache.Obj.QuestID))
                    {
                        foreach (var item in Global.Tables.EventShow.Select(o => { return o.n_GROUP == interactive.n_CALL_EVENT && o.n_RESULT == EventResult.Gift; }))
                        {
                            obj.Drop.Add(item.n_RESULT_X);
                            syncTarget.Drops.Add(item.n_RESULT_X);
                        }
                    }
                }

                Debug(debug, "EndQuest.[pass 5]");

                if (obj.Cache.Data.Drop > 0)
                {
                    obj.Drop.Add(obj.Cache.Data.Drop);
                    syncTarget.Drops.Add(obj.Cache.Data.Drop);
                }

                if (obj.Cache.Data.LuckDrop > 0)
                {
                    obj.Drop.Add(obj.Cache.Data.LuckDrop);
                    syncTarget.Drops.Add(obj.Cache.Data.LuckDrop);
                }

                if (obj.Cache.Complex[RESCUE_INDEX])
                {
                    obj.TriggerRescue = true;

                    if (isRescueSucc && obj.EventScheduleList.Rescue != null && obj.Cache.Obj.Timestamp >= obj.EventScheduleList.Rescue.BeginTime)
                    {
                        var drop = Calc.Drop.Next(obj.EventScheduleList.Rescue.Origin.n_POINT);

                        if (player.PurchaseInfo.MyLord)
                            drop = MyLordUtils.PleasureMyLord(player, obj, drop);

                        obj.Drop.Add(drop);
                        syncTarget.Drops.Add(drop);
                    }

                    obj.RescueCount = obj.EventScheduleList.CalcRescueCount(player.QuestInfo.RescueCount, player.QuestInfo.RescueUpdateTime, obj.Cache.Obj.Timestamp) - Global.Tables.HelpScale;
                    obj.RescueUpdateTime = obj.Cache.Obj.Timestamp;

                    var syncData = new SyncRescue { Count = obj.RescueCount, UpdateTime = obj.RescueUpdateTime };
                    syncTarget.AddSyncData(syncData);
                }
                else
                {
                    var syncData = new SyncRescue { Count = player.QuestInfo.RescueCount, UpdateTime = player.QuestInfo.RescueUpdateTime };
                    syncTarget.AddSyncData(syncData);
                }

                Debug(debug, "EndQuest.[pass 6]");

                #region essential

                obj.Cards = new List<EndQuestObj.CardDiffObj>(cards.Count);

                if (syncTarget.CardSync.UpdateList == null) syncTarget.CardSync.UpdateList = new List<CardDiff>();

                #endregion

                Debug(debug, "EndQuest.[pass 7]");

                if (isCardEvent)
                {
                    obj.TargetCard = player.CardInfo[player.CardInfo.Current.CardID];
                    var eventIndex = ComplexIndex.CardEvent + Calc.CardEvent.GetIndex(obj.TargetCard.ID, player.CardInfo.Current.InteractiveID);
                    syncTarget.CardSync.UpdateList.Add(CardDiff.Create(obj.TargetCard.Serial, PropertyID.Complex, eventIndex, 1));

                    obj.TargetCardComlexDiff = Complex.Bit(eventIndex);
                }

                Debug(debug, "EndQuest.[pass 8]");

                #region 卡片數值變化
                foreach (var serial in cards)
                {
                    if (serial == 0) continue;
                    var diffObj = new EndQuestObj.CardDiffObj { Card = player.CardInfo[serial] };
                    diffObj.ExpDiff = Math.Min(diffObj.Card.Exp + clearExp, Global.Tables.Card[diffObj.Card.ID].n_EXPTYPE) - diffObj.Card.Exp;
                    diffObj.KizunaDiff = Math.Min(diffObj.Card.Kizuna + data.n_KIZUNA, Global.Tables.KizunaLimit) - diffObj.Card.Kizuna;

                    var diff = new CardDiff { Serial = Serial.Create(serial) };
                    if (diffObj.ExpDiff != 0) diff.Add(PropertyID.Exp, diffObj.Card.Exp, diffObj.Card.Exp + diffObj.ExpDiff);
                    if (diffObj.KizunaDiff != 0) diff.Add(PropertyID.Kizuna, diffObj.Card.Kizuna, diffObj.Card.Kizuna + diffObj.KizunaDiff);

                    if (!diff.IsEmpty)
                    {
                        obj.Cards.Add(diffObj);
                        syncTarget.CardSync.UpdateList.Add(diff);
                    }
                }
                #endregion

                #region 玩家經驗變化
                obj.ExpDiff = Math.Min(originExp + clearRankExp, Global.Tables.PlayerExpMax) - originExp;
                if (obj.ExpDiff > 0) syncTarget.PlayerSync(PropertyID.Exp, originExp, originExp + obj.ExpDiff);
                #endregion

                #region 友情點

                if (obj.Cache.Data.FP > 0)
                {
                    obj.Fellow = obj.Cache.Data.Fellow;
                    obj.FPDiff = Math.Min(originFP + obj.Cache.Data.FP, Global.Tables.FPMax) - originFP;
                    if (obj.FPDiff > 0) syncTarget.PlayerSync(PropertyID.FP, originFP, originFP + obj.FPDiff);
                }
                #endregion

                Debug(debug, "EndQuest.[pass 9]");

                var limited = player.QuestInfo.GetLimited(obj.Cache.Obj.QuestID);

                switch (data.n_QUEST_TYPE)
                {
                    case QuestType.Limited:
                        {
                            if (limited == null) return ErrorCode.QuestIllegal;
                            obj.LimitedQuest = new LimitedQuest { Serial = limited.Serial, QuestID = limited.QuestID, PlayCount = limited.PlayCount + 1, EndTime = limited.EndTime };
                            break;
                        }
                    case QuestType.Schedule:
                        {
                            if (limited == null) limited = new LimitedQuest { Serial = 0, EndTime = DateTime.MinValue };

                            obj.LimitedQuest = new LimitedQuest { QuestID = obj.Cache.Obj.QuestID, Serial = limited.Serial };

                            if (limited.EndTime < obj.Cache.Obj.Timestamp)
                            {
                                // 結束時間是任務開始時間的隔日0:00
                                obj.LimitedQuest.EndTime = obj.Cache.Obj.Timestamp.Date.AddDays(1);
                                obj.LimitedQuest.PlayCount = 1;
                            }
                            else
                            {
                                obj.LimitedQuest.EndTime = limited.EndTime;
                                obj.LimitedQuest.PlayCount = limited.PlayCount + 1;
                            }
                            break;
                        }
                }

                if (obj.LimitedQuest != null) syncTarget.LimitedQuestSync.List.Add(obj.LimitedQuest);

                var trigger = Calc.Quest.TriggerLimited(data.n_ID);

                if (trigger > 0)
                {
                    var triggerData = Global.Tables.QuestDesign[trigger];

                    obj.TriggerQuest = new LimitedQuest { QuestID = trigger, EndTime = Global.CurrentTime.AddMinutes(triggerData.n_DURATION) };

                    var origin = player.QuestInfo.GetLimited(trigger);
                    if (origin != null) obj.TriggerQuest.Serial = origin.Serial;

                    syncTarget.LimitedQuestSync.List.Add(obj.TriggerQuest);
                }

                Debug(debug, "EndQuest.[pass 10]");

                #region 討伐

                if (obj.Cache.Data.Discover > 0)
                {
                    syncTarget.PlayerSync(PropertyID.Discover, player.QuestInfo.Crusade.Discover, player.QuestInfo.Crusade.Discover + obj.Cache.Data.Discover);
                }

                #endregion

                Debug(debug, "EndQuest.[pass 11]");

                #region 活動積分

                if (obj.Cache.Data.EP > 0 || obj.Battle != null)
                {
                    if (obj.Battle != null)
                    {
                        obj.EventPoint = obj.Battle.Point;
                    }
                    else if (obj.Cache.Data.Crusade > 0)
                    {
                        var crusade = obj.SharedQuestList[obj.Cache.Data.Crusade];
                        if (crusade == null)
                        {
                            if (obj.MySharedQuestList == null) obj.MySharedQuestList = SharedQuestList.Query(player.ID);
                            crusade = obj.MySharedQuestList.Find(o => o.QuestID == obj.Cache.Obj.QuestID);
                        }

                        if (crusade != null)
                        {
                            obj.EventSchedule = obj.EventScheduleList.Crusade;

                            obj.EventPoint = obj.Cache.Data.EP;

                            obj.Crusade = crusade;
                            obj.CrusadePoint = obj.EventScheduleList.CalcCrusadeCount(player.QuestInfo.Crusade.Point, player.QuestInfo.Crusade.UpdateTime, currentTime) - Global.Tables.CrusadeScale;
                            obj.CrusadeUpdateTime = currentTime;
                            obj.IsCrusadeFirstTime = !player.QuestInfo.CrusadeCacheList.Contains(obj.Crusade.Serial);

                            syncTarget.AddSyncData(new SyncCrusade { Point = obj.CrusadePoint, UpdateTime = obj.CrusadeUpdateTime });
                        }
                    }
                    else
                    {
                        if (current == null && obj.EventScheduleList != null)
                        {
                            foreach (var item in obj.EventScheduleList.SelectActiveSchedule(currentTime))
                            {
                                if (item.Origin.n_MAIN_POINT != 1) continue;
                                isRegular = true;
                                current = item;
                                break;
                            }
                        }

                        if (current != null && current.EndTime >= currentTime)
                        {
                            obj.EventSchedule = current;

                            if (current.IsInfinity)
                            {
                                if (player.Context.BlackList.Contains(player.ID)) return ErrorCode.Forbidden;

                                if (!Global.IsDevServer && player.Forbidden) return ErrorCode.Forbidden;

                                infinity = player.QuestInfo.InfinityList.SafeGetValue(current.Relative.ID);

                                var minus = (0.01f * Global.Tables.TowerMinus) * Math.Min(Global.Tables.TowerCountdown, currentTime.Subtract(obj.Cache.Obj.Timestamp).TotalSeconds);

                                double _point = obj.Cache.Data.EP;

                                var bonus = Calc.Quest.CalcInfinityBonus(current.InfinityBonus, current.BeginTime, currentTime);

                                var cardIds = new List<int>(GetIDs(player.CardInfo, cards));
                                if (obj.SupportCardID > 0) cardIds.Add(obj.SupportCardID);
                                var factor = Calc.Quest.CalcInfinityBonus(cardIds, bonus);

                                _point *= factor;

                                var point = (int)Math.Floor(_point * (1f - minus));

                                obj.Infinity = new Infinity { Schedule = current };
                                obj.Infinity.EventID = current.EventID;
                                obj.Infinity.UpdateTime = currentTime;
                                obj.Infinity.Point = point;

                                if (infinity != null) obj.Infinity.ID = infinity.ID;

                                if (infinity == null)
                                    obj.EventPoint = point;
                                else if (point > infinity.Point)
                                    obj.EventPoint = point - infinity.Point;
                                else
                                    obj.Infinity = null;

                                var syncData = new SyncInfinityPoint { Point = point, UpdateTime = currentTime };
                                syncTarget.AddSyncData(syncData);

                                try
                                {
                                    if (point > 14000)
                                    {
                                        KernelService.Logger.Debug("[HighScore] id={0}, point={1}, cards={2}, history={3}", player.ID, point, string.Join(",", cardIds), string.Join(",", obj.History));
                                    }
                                }
                                catch (Exception ex)
                                {
                                    KernelService.Logger.Debug("[Exception] ", ex.ToString());
                                }
                            }
                            else
                            {
                                obj.EventPoint = obj.Cache.Data.EP;

                                if (isRegular)
                                {
                                    if (isAutoBattleEnabled)
                                        obj.EventPoint = (int)Math.Ceiling((float)obj.EventPoint * 0.5f);

                                    if (obj.DailyEP == null || obj.DailyEP.UpdateTime.Date != currentTime.Date)
                                    {
                                        using (var db = Global.DBPool.Acquire(player.ID.Group))
                                        {
                                            var cmd = DBCommandBuilder.SelectDailyEventPoint(player.ID);
                                            using (var dbRes = db.DoQuery(cmd))
                                            {
                                                if (!dbRes.IsEmpty) obj.DailyEP = DBObjectConverter.ConvertCurrent<DBDailyEP>(dbRes);
                                            }
                                        }
                                    }

                                    var daily = (obj.DailyEP != null && obj.DailyEP.UpdateTime.Date == currentTime.Date) ? obj.DailyEP.Point : 0;

                                    var lv = PlayerUtils.CalcLV(originExp);
                                    var bonusMax = Global.Tables.Player[lv].n_BONUS_POINT;

                                    if (obj.EventPoint + daily > bonusMax)
                                        obj.EventPoint = bonusMax - daily;

                                    if (obj.EventPoint != 0)
                                    {
                                        obj.SyncDailyEP = new SyncDailyEventPoint { Current = daily + obj.EventPoint, UpdateTime = currentTime };
                                        syncTarget.AddSyncData(obj.SyncDailyEP);
                                    }
                                }
                            }
                        }
                    }

                    if (obj.EventPoint != 0)
                    {
                        var syncData = new SyncEventPoint { EventID = current.EventID, Add = obj.EventPoint };
                        syncTarget.AddSyncData(syncData);
                    }
                }
                #endregion

            }

            Debug(debug, "EndQuest.[pass 12]");

            using (var db = Global.DBPool.Acquire(player.ID.Group))
            {
                using (var trans = db.BeginTransaction())
                {
                    var cmd = DBCommandBuilder.EndQuest(player.ID, obj.Cache.Obj.ID);
                    db.SimpleQuery(cmd);

                    if (isSucc)
                    {
                        /*
                        if (!isCleared)
                        {
                            cmd = DBCommandBuilder.UpdateQuest(player.ID, obj.Cache.Obj.QuestID, ComplexI32.Bit(0).Value);
                            db.SimpleQuery(cmd);
                        }
                        */

                        obj.NewComplex = complex.Value;

                        Debug(debug, "EndQuest.[pass 12.1]");

                        if (complex.Value != originComplex)
                        {
                            cmd = DBCommandBuilder.UpdateQuest(player.ID, obj.Cache.Obj.QuestID, complex.Value);
                            db.SimpleQuery(cmd);

                            if (complex.Value != 1)
                            {
                                LogChallenge("Update", player.ID, data.n_ID, obj.SyncChallenge);
                            }
                        }

                        Debug(debug, "EndQuest.[pass 12.2]");

                        if (complex.Value != 1)
                        {
                            obj.ChallengeCount = 0;
                            var group = player.Context.QuestGroup.SafeGetValue(data.n_MAIN_QUEST);
                            if (group != null)
                            {
                                var newComplex = new ComplexI32 { Value = complex.Value | originComplex };

                                for (int i = 0; i < 3; ++i)
                                {
                                    if (newComplex[i + 1]) ++obj.ChallengeCount;
                                }

                                foreach (var item in group)
                                {
                                    if (item == data.n_ID) continue;

                                    var otherQuest = player.QuestInfo[item];
                                    if (otherQuest != null)
                                    {
                                        for (int i = 0; i < 3; ++i)
                                        {
                                            if (otherQuest.Complex[i + 1]) ++obj.ChallengeCount;
                                        }
                                    }
                                }
                            }
                        }

                        Debug(debug, "EndQuest.[pass 12.3]");

                        if (player.CardInfo.Current != null && player.CardInfo.Current.QuestID == questSerial)
                        {
                            cmd = DBCommandBuilder.DeleteCardEventCache(player.ID, obj.TargetCard.Serial);
                            db.SimpleQuery(cmd);

                            cmd = DBCommandBuilder.UpdateCardComplex(player.ID, obj.TargetCard.Serial, obj.TargetCardComlexDiff.Value);
                            db.SimpleQuery(cmd);
                        }

                        if (obj.ExpDiff > 0 || obj.FPDiff > 0)
                        {
                            cmd = DBCommandBuilder.EndQuest(player.ID, obj.ExpDiff, obj.FPDiff);
                            db.SimpleQuery(cmd);
                        }

                        if (obj.Drop != null) obj.Drop.Apply(player, db, Global.CurrentTime);

                        if (obj.Cards != null)
                        {
                            foreach (var item in obj.Cards)
                            {
                                cmd = DBCommandBuilder.AddCardValue(player.ID, item.Card.Serial, item.ExpDiff, item.KizunaDiff);
                                db.SimpleQuery(cmd);
                            }
                        }

                        if (obj.LimitedQuest != null)
                        {
                            if (obj.LimitedQuest.Serial == 0)
                            {
                                cmd = DBCommandBuilder.InsertLimitedQuest(player.ID, obj.LimitedQuest.QuestID, obj.LimitedQuest.EndTime, obj.LimitedQuest.PlayCount);
                                obj.LimitedQuest.Serial = db.DoQuery<Int64>(cmd);
                            }
                            else
                            {
                                cmd = DBCommandBuilder.ResetLimitedQuest(player.ID, obj.LimitedQuest.Serial, obj.LimitedQuest.EndTime, obj.LimitedQuest.PlayCount);
                                db.SimpleQuery(cmd);
                            }
                        }

                        if (obj.TriggerQuest != null)
                        {
                            if (obj.TriggerQuest.Serial == 0)
                            {
                                cmd = DBCommandBuilder.InsertLimitedQuest(player.ID, obj.TriggerQuest.QuestID, obj.TriggerQuest.EndTime);
                                obj.TriggerQuest.Serial = db.DoQuery<Int64>(cmd);
                            }
                            else
                            {
                                cmd = DBCommandBuilder.ResetLimitedQuest(player.ID, obj.TriggerQuest.Serial, obj.TriggerQuest.EndTime);
                                db.SimpleQuery(cmd);
                            }
                        }

                        if (obj.EventPoint != 0)
                        {
                            var eventId = obj.Battle != null ? obj.EventScheduleList.PVP.Relative.ID : obj.EventSchedule.Relative.ID;
                            cmd = DBCommandBuilder.UpdateEventPoint(player.ID, eventId, obj.EventPoint, player.CardInfo.Favorite.ID);
                            db.SimpleQuery(cmd);

                            if (isRegular)
                            {
                                if (obj.DailyEP != null)
                                {
                                    if (obj.DailyEP.UpdateTime.Date == currentTime.Date)
                                        cmd = DBCommandBuilder.UpdateDailyEventPoint(player.ID, obj.DailyEP.ID, obj.DailyEP.Point + obj.EventPoint, currentTime);
                                    else
                                        cmd = DBCommandBuilder.UpdateDailyEventPoint(player.ID, obj.DailyEP.ID, obj.EventPoint, currentTime);

                                    db.SimpleQuery(cmd);
                                }
                                else
                                {
                                    cmd = DBCommandBuilder.InsertDailyEventPoint(player.ID, obj.EventPoint, currentTime);
                                    var serial = db.DoQuery<Int64>(cmd);
                                    obj.DailyEP = new DBDailyEP { ID = serial, Point = obj.EventPoint, UpdateTime = currentTime };
                                }
                            }
                        }

                        if (obj.Infinity != null)
                        {
                            if (obj.Infinity.ID > 0)
                            {
                                cmd = DBCommandBuilder.UpdateInfinity(player.ID, obj.Infinity.ID, obj.Infinity.Point, currentTime);
                                db.SimpleQuery(cmd);
                            }
                            else
                            {
                                cmd = DBCommandBuilder.InsertInfinity(player.ID, obj.Infinity.Schedule.Relative.ID, obj.Infinity.Point, currentTime);
                                obj.Infinity.ID = db.DoQuery<Int64>(cmd);
                            }

                        }

                        if (obj.Cache.Data.Discover > 0)
                        {
                            cmd = DBCommandBuilder.AddCrusadeDiscover(player.ID, obj.Cache.Data.Discover);
                            db.SimpleQuery(cmd);
                        }

                        if (obj.Crusade != null)
                        {
                            cmd = DBCommandBuilder.UpdateCrusade(player.ID, obj.CrusadePoint, obj.CrusadeUpdateTime);
                            db.SimpleQuery(cmd);

                            if (obj.IsCrusadeFirstTime)
                            {
                                cmd = DBCommandBuilder.InsertCrusadeCache(player.ID, obj.Crusade.Serial, true);
                                db.SimpleQuery(cmd);
                            }
                        }

                        Debug(debug, "EndQuest.[pass 12.4]");
                    }

                    if (obj.TriggerRescue)
                    {
                        cmd = DBCommandBuilder.UpdateRescue(player.ID, obj.RescueCount, obj.RescueUpdateTime);
                        db.SimpleQuery(cmd);

                        if (obj.AddRescueMiss != 0)
                        {
                            if (obj.MyLord == null)
                            {
                                cmd = DBCommandBuilder.InsertMyLord(player.ID);
                                db.SafeSimpleQuery(cmd);
                                obj.MyLord = new DBMyLord();
                            }
                            var rescueMiss = obj.MyLord.RescueMiss + obj.AddRescueMiss;
                            cmd = DBCommandBuilder.UpdateMyLordRescueMiss(player.ID, rescueMiss, Math.Max(rescueMiss, obj.MyLord.MaxRescueMiss));
                            db.SafeSimpleQuery(cmd);
                        }
                    }

                    Debug(debug, "EndQuest.[pass 12.5]");

                    trans.Commit();
                }
            }

            Debug(debug, "EndQuest.[pass 13]");

            if (obj.Crusade != null && !obj.Crusade.PublicID.Equals(player.ID) && obj.IsCrusadeFirstTime)
            {
                using (var db = Global.DBPool.Acquire(obj.Crusade.PublicID.Group))
                {
                    var cmd = DBCommandBuilder.InsertCrusadeCache(obj.Crusade.PublicID, obj.Crusade.Serial, false);
                    db.SimpleQuery(cmd);
                }
            }

            if (obj.Battle != null)
            {
                var eventId = obj.EventScheduleList.PVP.Relative.ID;
                using (var db = Global.DBPool.Acquire(obj.Battle.TargetID.Group))
                {
                    var cmd = DBCommandBuilder.UpdateEventPoint(obj.Battle.TargetID, eventId, -obj.EventPoint, obj.Battle.TargetCardID);
                    db.SimpleQuery(cmd);
                }

                obj.Battle.PVPList.SafeCall(() =>
                {
                    obj.Battle.PVPList.AddPointUnsafe(player.ID, eventId, obj.EventPoint);
                    obj.Battle.PVPList.AddPointUnsafe(obj.Battle.TargetID, eventId, -obj.EventPoint);
                });
            }

            if (obj.Drop != null) obj.Drop.Sync(syncTarget);

            if (obj.NewComplex != 0)
            {
                var syncQuest = new SyncQuest { QuestID = data.n_ID, Complex = obj.NewComplex };
                syncTarget.AddSyncData(syncQuest);
            }

            syncTarget.IsQuestSucc = isSucc;
            syncTarget.IsAutoBattleEnabled = isAutoBattleEnabled;

            Debug(debug, "EndQuest.[pass 14]");

            return ErrorCode.Success;
        }

        public static void CommitQuest(Player player, EndQuestObj obj)
        {
            player.QuestInfo.UpdateQuest(obj.Cache.Obj.QuestID, obj.IsSucc, obj.NewComplex, Global.CurrentTime);

            player.QuestInfo.SharedList = obj.MySharedQuestList;

            if (obj.IsSucc)
            {
                player.QuestInfo.Commit(obj.Cache);

                player.PlayerInfo[PropertyID.Exp] += obj.ExpDiff;
                if (obj.FPDiff > 0) player.PlayerInfo[PropertyID.FP] += obj.FPDiff;

                foreach (var item in obj.Cards)
                {
                    item.Card.Exp += item.ExpDiff;
                    item.Card.Kizuna += item.KizunaDiff;
                }

                if (obj.TargetCard != null) obj.TargetCard.Complex.Value = obj.TargetCard.Complex.Combin(obj.TargetCardComlexDiff);

                if (obj.Drop != null) obj.Drop.Commit(player);

                if (obj.LimitedQuest != null) player.QuestInfo.UpdateLimitedQuest(obj.LimitedQuest);
                if (obj.TriggerQuest != null) player.QuestInfo.UpdateLimitedQuest(obj.TriggerQuest);

                if (obj.Infinity != null)
                    player.QuestInfo.InfinityList[obj.Infinity.Schedule.Relative.ID] = obj.Infinity;

                if (obj.DailyEP != null)
                {
                    if (obj.SyncDailyEP != null)
                    {
                        obj.DailyEP.Point = obj.SyncDailyEP.Current;
                        obj.DailyEP.UpdateTime = obj.SyncDailyEP.UpdateTime;
                    }

                    player.DailyEP = obj.DailyEP;
                }

                if (obj.Crusade != null)
                {
                    if (obj.IsCrusadeFirstTime)
                        player.QuestInfo.CrusadeCacheList.Add(obj.Crusade.Serial);

                    player.QuestInfo.Crusade.Point = obj.CrusadePoint;
                    player.QuestInfo.Crusade.UpdateTime = obj.CrusadeUpdateTime;
                }

                if (obj.Cache.Data.Discover > 0)
                    player.QuestInfo.Crusade.Discover += obj.Cache.Data.Discover;
            }

            if (obj.TriggerRescue)
            {
                player.QuestInfo.RescueCount = obj.RescueCount;
                player.QuestInfo.RescueUpdateTime = obj.RescueUpdateTime;

                if (obj.AddRescueMiss != 0)
                {
                    player.MyLord = obj.MyLord;

                    if (player.MyLord != null)
                    {
                        player.MyLord.RescueMiss += obj.AddRescueMiss;
                        player.MyLord.MaxRescueMiss = Math.Max(player.MyLord.RescueMiss, player.MyLord.MaxRescueMiss);
                    }
                }
            }
        }
    }

    #endregion
}
