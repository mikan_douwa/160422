﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mikan.CSAGA.Commands.Faith;
using Mikan.Server;

namespace Mikan.CSAGA.Server.Tasks
{

    [ActiveTask(CommandType = typeof(InfoRequest))]
    public class FaithInfoTask : PlayerTask<InfoRequest, InfoResponse>
    {
        protected override ErrorCode SafeProcessCommand(InfoResponse res)
        {
            res.FaithInfo = Player.FaithInfo.Serialize();
            return ErrorCode.Success;
        }
    }

    [ActiveTask(CommandType = typeof(ClaimRequest))]
    public class FaithClaimTask : PlayerTask<ClaimRequest, ClaimResponse>
    {
        protected override void OnCommit(Context context)
        {
            Player.FaithInfo.ClaimTime = CurrentTime;
            Player.FaithInfo.Origin = new DBFaithClaim { ClaimTime = CurrentTime };

            base.OnCommit(context);
        }

        protected override ErrorCode SafeProcessCommand(ClaimResponse res)
        {
            #region 計算總共要給幾個道具

            var bonus = MathUtils.Random.Next(0, Tables.GetVariable(VariableID.FAITH_BONUS_MAX) + 1) / 100f;

            var calcCount = Calc.Faith.MaterialCount(Player.FaithInfo.ClaimTime, CurrentTime, bonus);

            if (calcCount < 1) return ErrorCode.FaithEmpty;

            var count = (int)Math.Floor(calcCount);

            #endregion

            var ids = new List<int>(Tables.FaithItemIDs);
            MathUtils.Shuffle(ids);

            var list = new Dictionary<int, int>();

            var isFirst = true;

            foreach (var id in ids)
            {
                var max = count;
                if (isFirst)
                {
                    max = max / 2;
                    isFirst = false;
                }

                var quota = MathUtils.Random.Next(0, max + 1);

                list.Add(id, quota);
                count -= quota;
                if (count <= 0) break;
            }

            if (count > 0)
            {
                var last = list.Last();
                list[last.Key] = last.Value + count;
            }

            var drop = new DropObj(Player, RewardSource.Faith);

            foreach (var item in list)
                drop.Add(item.Key, item.Value);

            AddAssistObj(drop);

            using (var db = AcquireDB())
            {
                using (var trans = db.BeginTransaction())
                {
                    var cmd = Player.FaithInfo.Origin != null ? DBCommandBuilder.UpdateFaithClaim(PublicID, CurrentTime) : DBCommandBuilder.InsertFaithClaim(PublicID, CurrentTime);

                    db.SimpleQuery(cmd);

                    ApplyAssists(db);
                    trans.Commit();
                }
            }

            res.ClaimTime = CurrentTime;

            return ErrorCode.Success;

        }

    }

    [ActiveTask(CommandType = typeof(BeginProduceRequest))]
    public class FaithBeginProduceTask : PlayerTask<BeginProduceRequest, BeginProduceResponse>
    {
        private Faith faith;

        protected override void OnCommit(Context context)
        {
            Player.FaithInfo.List.Add(faith);

            AddLog(LogType.Faith, Command.List.ToArray());

            base.OnCommit(context);
        }

        protected override ErrorCode SafeProcessCommand(BeginProduceResponse res)
        {
            var origin = Player.FaithInfo.List.Find(o => o.Index == Command.Index);

            if (origin != null) return ErrorCode.IllegalOpertaion;

            var candidates = new List<ConstData.FaithResult>();

            for (int i = 0; i < 4; ++i)
            {
                var itemId = Tables.FaithItemIDs[i];
                var count = Command.List[i];

                var payment = new PaymentObj(Player, ArticleID.Faith, DropType.Item, count, itemId);

                if (payment.Origin < count) return ErrorCode.ItemNotEnough;

                AddAssistObj(payment);
            }

            foreach (var item in Tables.FaithResult.Select())
            {
                var isAllFit = true;
                for (int i = 0; i < item.n_ITEMMIN.Length; ++i)
                {
                    var min = item.n_ITEMMIN[i];
                    var max = item.n_ITEMMAX[i];
                    var count = Command.List[i];
                    if((min > 0 && count < min) || (max > 0 && count > max))
                    {
                        isAllFit = false;
                        break;
                    }
                }

                if (isAllFit) candidates.Add(item);
            }

            var data = default(ConstData.FaithResult);

            if (candidates.Count < 1)
                data = Tables.FaithResult[1];
            else
            {
                var rand = MathUtils.Random.Next(candidates.Count);
                // 固定取第一個
                data = candidates[0];
            }

            faith = new Faith();
            faith.Index = Command.Index;
            faith.EndTime = CurrentTime.AddMinutes(data.n_TIME);
            faith.Result = Calc.Drop.Next(data.n_RESULT);

            for(int i = 0; i < 100; ++i)
            {
                var drop = Tables.Drop[faith.Result];

                if (drop.n_DROP_TYPE != DropType.Tactics) break;

                if (!Player.TacticsInfo.Contains(drop.n_DROP_ID))
                {
                    var tactics = Tables.Tactics[drop.n_DROP_ID];

                    var current = Player.TacticsInfo[tactics.n_GROUP];

                    if (current == null || current.n_LV < tactics.n_LV) break;
                }

                faith.Result = Calc.Drop.Next(data.n_RESULT);
            }
            
            using (var db = AcquireDB())
            {
                using (var trans = db.BeginTransaction())
                {
                    var cmd = DBCommandBuilder.InsertFaith(PublicID, faith.Index, data.n_ID, faith.Result, faith.EndTime);

                    faith.Serial = db.DoQuery<Int64>(cmd);
                    faith.Origin = new DBFaith { ID = faith.Serial, DataRef = data.n_ID };

                    ApplyAssists(db);

                    trans.Commit();
                }
            }

            var syncData = new SyncFaith();
            syncData.List = new List<Internal.FaithObj>(1);
            syncData.List.Add(faith);
            res.AddSyncData(syncData);

            return ErrorCode.Success;
        }
    }

    [ActiveTask(CommandType = typeof(EndProduceRequest))]
    public class FaithEndProduceTask : PlayerTask<EndProduceRequest, EndProduceResponse>
    {
        private Faith origin;

        protected override void OnCommit(Context context)
        {
            Player.FaithInfo.List.Remove(origin);
            base.OnCommit(context);
        }
        protected override ErrorCode SafeProcessCommand(EndProduceResponse res)
        {
            origin = Player.FaithInfo.List.Find(o => o.Origin.ID == Command.Serial);

            if (origin == null || origin.EndTime > CurrentTime.AddMinutes(3)) return ErrorCode.IllegalOpertaion;

            var drop = new DropObj(Player, RewardSource.Faith, origin.Origin.DataRef);

            drop.Add(origin.Result);

            AddAssistObj(drop);

            using (var db = AcquireDB())
            {
                using (var trans = db.BeginTransaction())
                {
                    var cmd = DBCommandBuilder.DeleteFaith(PublicID, origin.Serial);
                    db.SimpleQuery(cmd);

                    ApplyAssists(db);
                    trans.Commit();
                }
            }

            res.DropID = origin.Result;

            var syncData = new SyncFaith();
            syncData.List = new List<Internal.FaithObj>(1);
            syncData.List.Add(new Faith { Index = origin.Index });
            res.AddSyncData(syncData);

            return ErrorCode.Success;
        }
    }

}
