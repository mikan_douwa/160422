﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mikan.Server;
using Mikan.CSAGA.Commands.Quest;

namespace Mikan.CSAGA.Server.Tasks
{
    [ActiveTask(CommandType = typeof(InfoRequest))]
    public class QuestInfoTask : PlayerTask<InfoRequest, InfoResponse>
    {
        protected override ErrorCode SafeProcessCommand(InfoResponse res)
        {
            res.QuestInfo = Player.QuestInfo.Serialize();
            res.Limited = new List<Internal.LimitedQuestObj>(Player.QuestInfo.SelectLimited());

            return ErrorCode.Success;
        }
    }

    public partial class QuestHelper
    {
        const int RESCUE_INDEX = 1;

        private static void Debug(bool debug, string log)
        {
            if (debug) KernelService.Logger.Debug(log);
        }

        private static void LogChallenge(string title, PublicID playerId, int questId, SyncChallenge challenge)
        {
            var text = "";
            foreach (var item in challenge.List)
            {
                text += "(" + string.Join(",", item.Values) + ")";
            }

            KernelService.Logger.Debug("[Challenge] msg = {0}, id = {1}, quest_id = {2}, detail = {3}", title, playerId, questId, text);
        }

        private static IEnumerable<int> GetIDs(CardInfo info, List<Int64> cards)
        {
            foreach (var item in cards)
            {
                var card = info[item];
                if (card != null) yield return card.ID;
            }
        }
    }

    public class MyLordUtils
    {
        public static DBMyLord QueryMyLord(PublicID id)
        {
            using (var db = Global.DBPool.Acquire(id.Group))
            {
                var cmd = DBCommandBuilder.SelectMyLord(id);

                using (var dbRes = db.DoQuery(cmd))
                {
                    if (dbRes.IsEmpty)
                        return null;
                    else
                        return DBObjectConverter.ConvertCurrent<DBMyLord>(dbRes);
                }
            }
        }
        public static int PleasureMyLord(Player player, EndQuestObj obj, int origin)
        {
            obj.MyLord = player.MyLord;

            if (obj.MyLord == null) obj.MyLord = QueryMyLord(player.ID);

            var drop = origin;

            var retryCount = 0;

            while (true)
            {
                var dropData = Global.Tables.Drop[drop];

                if (dropData.n_DROP_TYPE == DropType.Card)
                {
                    if (obj.MyLord != null)
                        obj.AddRescueMiss = -obj.MyLord.RescueMiss;
                    break;
                }

                if (obj.MyLord == null || retryCount > obj.MyLord.RescueMiss)
                {
                    obj.AddRescueMiss = 1;
                    break;
                }

                drop = Calc.Drop.Next(obj.EventScheduleList.Rescue.Origin.n_POINT);

                ++retryCount;
            }
            KernelService.Logger.Debug("PleasureMyLord,{0},{1}", player.ID.ToString(), obj.AddRescueMiss);
            return drop;
        }
    }

}
