﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mikan.CSAGA.Commands.Card;
using Mikan.Server;

namespace Mikan.CSAGA.Server.Tasks
{
    [ActiveTask(CommandType = typeof(InfoRequest))]
    public class CardInfoTask : PlayerTask<InfoRequest, InfoResponse>
    {
        protected override ErrorCode SafeProcessCommand(InfoResponse res)
        {
            if (Player.Platform != Platform.iOS && string.IsNullOrEmpty(Player.Transaction) && !Player.UseSuperPrivilege) return ErrorCode.AppNotMatch;

            res.CardInfo = Player.CardInfo.Serialize();
            return ErrorCode.Success;
        }
    }

    [ActiveTask(CommandType = typeof(EnhanceRequest))]
    public class CardEnhanceTask : PlayerTask<EnhanceRequest, EnhanceResponse>
    {
        private Card card;
        private int diff;

        protected override void OnCommit(Context context)
        {
            Player.PlayerInfo[PropertyID.Crystal] -= diff;
            card.Exp += diff;
            
            if (card == Player.CardInfo.Header) Player.UpdateSnapshot(CurrentTime);

            base.OnCommit(context);
        }
        protected override ErrorCode SafeProcessCommand(EnhanceResponse res)
        {
            var originCrystal = Player.PlayerInfo[PropertyID.Crystal];

            if (originCrystal < Command.Exp) return ErrorCode.CrystalNotEnough;

            card = Player.CardInfo[Command.Serial];

            diff = Command.Exp;

            using (var db = AcquireDB())
            {
                using (var trans = db.BeginTransaction())
                {
                    var cmd = DBCommandBuilder.EnhanceCard(PublicID, card.Serial, diff, CurrentTime);
                    db.SimpleQuery(cmd);

                    trans.Commit();
                }
            }

            res.PlayerSync(PropertyID.Crystal, originCrystal, originCrystal - diff);
            var updateList = new List<CardDiff>(1);
            updateList.Add(CardDiff.Create(card.Serial, PropertyID.Exp, card.Exp, card.Exp + diff));
            res.AddSyncData(new SyncCard { UpdateList = updateList });

            return ErrorCode.Success;
        }
    }

    [ActiveTask(CommandType = typeof(SellRequest))]
    public class CardSellTask : PlayerTask<SellRequest, SellResponse>
    {
        private List<Card> list;
        private List<Card> sellCache;
        private int crystal;
        private int coin;

        protected override void OnCommit(Context context)
        {
            if (list != null)
            {
                Player.PlayerInfo[PropertyID.Crystal] += crystal;
                Player.PlayerInfo[PropertyID.Coin] += coin;
                Player.CardInfo.List.RemoveAll(o => { return list.Contains(o); });

                if (sellCache != null)
                {
                    if (Player.CardInfo.SellCacheList == null) Player.CardInfo.SellCacheList = new List<DBSellCache>();
                    foreach (var item in sellCache)
                        Player.CardInfo.SellCacheList.Add(new DBSellCache { CardID = item.ID, Kizuna = item.Kizuna });
                }
            }
            base.OnCommit(context);
        }
        protected override ErrorCode SafeProcessCommand(SellResponse res)
        {
            if (Command.List.Count > 0)
            {
                list = Player.CardInfo.List.FindAll(o => { return Command.List.Contains(o.Serial.Value); });

                if (list.Count != Command.List.Count) return ErrorCode.CardNotExists;

                foreach (var item in list)
                {
                    var data = Global.Tables.Card[item.ID];
                    crystal += CardUtils.CalcCrystal(item.Exp);
                    coin += data.n_TRADE_COIN;

                    KernelService.Logger.Trace("[SELL] player_id=[{0},{1}|{2}], card_serial={3}, card_id={4}, exp={5}, kizuna={6}, complex={7}", PublicID.Group, PublicID.Serial, PublicID.ToString(), item.Serial, item.ID, item.Exp, item.Kizuna, item.Complex.Value);
                }

                KernelService.Logger.Trace("[SELL_PRICE] player_id=[{0},{1}|{2}], crystal={3}, coin={4}", PublicID.Group, PublicID.Serial, PublicID.ToString(), crystal, coin);

                using (var db = AcquireDB())
                {
                    using (var trans = db.BeginTransaction())
                    {
                        var cmd = DBCommandBuilder.SellCard(PublicID, crystal, coin, CurrentTime);
                        db.SimpleQuery(cmd);
                        cmd = DBCommandBuilder.DeleteCard(PublicID, Command.List);
                        db.SimpleQuery(cmd);

                        foreach (var item in list)
                        {
                            if (item.Kizuna < 500) continue;

                            if (sellCache == null) sellCache = new List<Card>();
                            sellCache.Add(item);

                            cmd = DBCommandBuilder.InsertCardSellCache(PublicID, item.ID, item.Kizuna);
                            db.SimpleQuery(cmd);
                        }

                        trans.Commit();
                    }
                }

                var originCrystal = Player.PlayerInfo[DropType.Crystal];
                var originCoin = Player.PlayerInfo[DropType.Coin];

                res.PlayerSync(PropertyID.Crystal, originCrystal, originCrystal + crystal);
                res.PlayerSync(PropertyID.Coin, originCoin, originCoin + coin);
                res.AddSyncData(new SyncCard { DeleteList = Command.List });
            }

            return ErrorCode.Success;
        }
    }

    [ActiveTask(CommandType = typeof(BeginTalkRequest))]
    public class CardBeginTalkTask : PlayerTask<BeginTalkRequest, BeginTalkResponse>
    {
        private Card card;
        private int diff;
        private int tpConsumed;
        private int interactiveId;
        private bool isChoice = false;
        protected override void OnCommit(Context context)
        {
            Player.PlayerInfo.UpdateTPConsumed(tpConsumed, CurrentTime);
            if (isChoice)
                Player.CardInfo.AddEventCache(card.Serial, interactiveId);

            if (diff != 0)
            {
                card.Kizuna += diff;
                AddTrophyTask(new KizunaTrophyTask { PublicID = PublicID });
            }

            AddTrophyTask(new TalkTrophy { PublicID = PublicID });
            
            base.OnCommit(context);
        }
        protected override ErrorCode SafeProcessCommand(BeginTalkResponse res)
        {
            card = Player.CardInfo[Command.Serial];

            diff = Math.Min(card.Kizuna + Global.Tables.TalkKizuna, Global.Tables.KizunaLimit) - card.Kizuna;

            var ids = new HashSet<int>();
            CardUtils.GetIDs(Player.CardInfo.List, ids);

            interactiveId = Calc.CardEvent.Next(Calc.CardEvent.Filter.Contact, card.ID, card.Kizuna, ids);

            isChoice = EventUtils.CallEvent(interactiveId).n_CHOICE[0] != 0;

            var originTPConsumed = Player.PlayerInfo.TalkPointConsumed(CurrentTime);
            tpConsumed = originTPConsumed + Global.Tables.TalkPointScale;

            if (tpConsumed > Global.Tables.TalkPointLimit) return ErrorCode.IllegalOpertaion;

            using (var db = AcquireDB())
            {
                using (var trans = db.BeginTransaction())
                {
                    var cmd = DBCommandBuilder.UpdateTPConsumed(PublicID, tpConsumed, CurrentTime);
                    db.SimpleQuery(cmd);
                    cmd = DBCommandBuilder.AddCardKizuna(PublicID, card.Serial, diff);
                    db.SimpleQuery(cmd);
                    if (isChoice)
                    {
                        cmd = DBCommandBuilder.InsertCardEventCache(PublicID, card.Serial, interactiveId);
                        db.SimpleQuery(cmd);
                    }
                    
                    trans.Commit();
                }
            }

            res.Serial = Command.Serial;
            res.InteractiveID = interactiveId;

            res.CardSync.UpdateList = new List<CardDiff>(1);
            res.CardSync.UpdateList.Add(CardDiff.Create(card.Serial, PropertyID.Kizuna, card.Kizuna, card.Kizuna + diff));

            res.PlayerSync(PropertyID.TPConsumed, originTPConsumed, tpConsumed);

            return ErrorCode.Success;
        }
    }

    [ActiveTask(CommandType = typeof(EndTalkRequest))]
    public class CardEndTalkTask : PlayerTask<EndTalkRequest, EndTalkResponse>
    {
        private Card card;
        private int kizunaDiff;
        protected override void OnCommit(Context context)
        {
            Player.CardInfo.RemoveEventCache(card.Serial);
            if (kizunaDiff != 0)
            {
                card.Kizuna += kizunaDiff;
                AddTrophyTask(new KizunaTrophyTask { PublicID = PublicID });
            }
            base.OnCommit(context);
        }
        protected override ErrorCode SafeProcessCommand(EndTalkResponse res)
        {
            card = Player.CardInfo[Command.Serial];

            var interactiveId = Player.CardInfo.GetEventCache(Command.Serial);

            if (interactiveId == 0) return ErrorCode.CardIllegal;

            var ev = EventUtils.CallEvent(interactiveId);
            ev = EventUtils.CallEventByGroup(ev.n_CHOICE[Command.Choice]);

            switch (ev.n_RESULT)
            {
                case EventResult.Kizuna:
                    kizunaDiff = Math.Min(card.Kizuna + ev.n_RESULT_X, Global.Tables.KizunaLimit) - card.Kizuna;
                    break;
                case EventResult.Gift:
                    res.DropID = Calc.Drop.Next(ev.n_RESULT_X);
                    if (res.DropID > 0)
                    {
                        var drop = new DropObj(Player, RewardSource.CardEvent, interactiveId);
                        drop.Add(res.DropID);
                        AddAssistObj(drop);
                    }
                    break;
            }

            using (var db = AcquireDB())
            {
                using (var trans = db.BeginTransaction())
                {
                    var cmd = default(IDBCommand);

                    if (kizunaDiff != 0)
                    {
                        cmd = DBCommandBuilder.AddCardKizuna(PublicID, card.Serial, kizunaDiff);
                        db.SimpleQuery(cmd);
                    }

                    cmd = DBCommandBuilder.DeleteCardEventCache(PublicID, card.Serial);
                    db.SimpleQuery(cmd);

                    ApplyAssists(db);

                    trans.Commit();
                }
            }

            res.Serial = Command.Serial;
            res.EventID = ev.n_ID;

            if (kizunaDiff != 0)
            {
                if (res.CardSync.UpdateList == null) res.CardSync.UpdateList = new List<CardDiff>(1);
                res.CardSync.UpdateList.Add(CardDiff.Create(card.Serial, PropertyID.Kizuna, card.Kizuna, card.Kizuna + kizunaDiff));
            }

            return ErrorCode.Success;
        }
    }

    [ActiveTask(CommandType = typeof(GiftRequest))]
    public class CardGiftTask : PlayerTask<GiftRequest, GiftResponse>
    {
        private Card card;
        private int targetCount;
        private int kizunaDiff;
        private Complex complex;
        protected override void OnCommit(Context context)
        {
            Player.ItemInfo.Add(Command.ItemID, -targetCount);

            if (kizunaDiff != 0)
            {
                card.Kizuna += kizunaDiff;
                AddTrophyTask(new KizunaTrophyTask { PublicID = PublicID });

                AddLog(LogType.Gift, kizunaDiff);
            }

            if (complex != null)
            {
                card.Complex.Value = complex.Value;
            }

            AddTrophyTask(new GiftTrophy { PublicID = PublicID });

            base.OnCommit(context);
        }
        protected override ErrorCode SafeProcessCommand(GiftResponse res)
        {
            var count = Player.ItemInfo.Count(Command.ItemID);

            targetCount = Command.Count > 0 ? Command.Count : 1;

            if (count < targetCount) return ErrorCode.ItemNotEnough;

            card = Player.CardInfo[Command.Serial];

            var cardData = Global.Tables.Card[card.ID];
            
            var itemData = Global.Tables.Item[Command.ItemID];

            var errCode = ErrorCode.Success;

            if (itemData.n_FAVOR > 0)
            {
                var factor = 1f;

                if (cardData.n_GIFT == itemData.n_EFFECT_Y) factor = Global.Tables.GiftBonus;

                var equipFactor = 0;

                foreach (var item in Player.EquipmentInfo.List)
                {
                    if (item.CardSerial.Value != Command.Serial) continue;
                    var equipData = Global.Tables.Item[item.ID];
                    if (equipData.n_EFFECT == ItemEffect.Gift && equipFactor < equipData.n_EFFECT_X)
                        equipFactor = equipData.n_EFFECT_X;
                }

                kizunaDiff = Math.Min(card.Kizuna + (int)Math.Ceiling(itemData.n_FAVOR * targetCount * factor * (1f + equipFactor * 0.01f)), Global.Tables.KizunaLimit) - card.Kizuna;

                using (var db = AcquireDB())
                {
                    using (var trans = db.BeginTransaction())
                    {
                        var cmd = default(IDBCommand);

                        if (kizunaDiff != 0)
                        {
                            cmd = DBCommandBuilder.AddCardKizuna(PublicID, card.Serial, kizunaDiff);
                            db.SimpleQuery(cmd);
                        }

                        cmd = DBCommandBuilder.IncreaseItemCount(PublicID, Command.ItemID, -targetCount);
                        db.SimpleQuery(cmd);

                        trans.Commit();
                    }
                }

                if (kizunaDiff != 0)
                {
                    if (res.CardSync.UpdateList == null) res.CardSync.UpdateList = new List<CardDiff>(1);
                    res.CardSync.UpdateList.Add(CardDiff.Create(card.Serial, PropertyID.Kizuna, card.Kizuna, card.Kizuna + kizunaDiff));
                }
            }
            else if (itemData.n_HP_MAX > 0)
                errCode = AddPlus(0, targetCount, res);
            else if (itemData.n_ATK_MAX > 0)
                errCode = AddPlus(1, targetCount, res);
            else if (itemData.n_REV_MAX > 0)
                errCode = AddPlus(2, targetCount, res);

            if (errCode != ErrorCode.Success) return errCode;

            res.Serial = Command.Serial;

            res.AddSyncData(new SyncItem { Diff = Diff.Create(Command.ItemID, count, count - targetCount) });

            return ErrorCode.Success;
        }

        private ErrorCode AddPlus(int index, int count, CSAGACommand syncTarget)
        {
            complex = new Complex { Value = card.Complex.Value };
            
            var pos = ComplexIndex.CardPlus + ComplexIndex.CardPlusLen * index;
            
            var current = complex.Get(pos, ComplexIndex.CardPlusLen);

            var rare = card.Rare > 0 ? card.Rare : Global.Tables.Card[card.ID].n_RARE;
            var balance = Global.Tables.CardBalance[rare];
            
            if (current >= balance.n_PLUS) return ErrorCode.Success;
            
            var newValue = Math.Min(current + count, balance.n_PLUS);
            
            complex.Set(pos, ComplexIndex.CardPlusLen, newValue);
            
            using (var db = AcquireDB())
            {
                using (var trans = db.BeginTransaction())
                {
                    var cmd = DBCommandBuilder.ReplaceCardComplex(PublicID, card.Serial, complex.Value);
                    db.SimpleQuery(cmd);

                    cmd = DBCommandBuilder.IncreaseItemCount(PublicID, Command.ItemID, -targetCount);
                    db.SimpleQuery(cmd);

                    trans.Commit();
                }
            }

            if (syncTarget.CardSync.UpdateList == null) syncTarget.CardSync.UpdateList = new List<CardDiff>(1);
            syncTarget.CardSync.UpdateList.Add(CardDiff.Create(card.Serial, PropertyID.Plus, index, newValue));
            
            return ErrorCode.Success;
        }
    }

    [ActiveTask(CommandType = typeof(EventRequest))]
    public class CardEventTask : PlayerTask<EventRequest, EventResponse>
    {
        private Card card;
        private Complex complex;
        private int kizunaDiff;
        protected override void OnCommit(Context context)
        {
            if (complex != null) card.Complex.Value = card.Complex.Combin(complex);;
            if (kizunaDiff != 0)
            {
                card.Kizuna += kizunaDiff;
                AddTrophyTask(new KizunaTrophyTask { PublicID = PublicID });
            }

            base.OnCommit(context);
        }
        protected override ErrorCode SafeProcessCommand(EventResponse res)
        {
            card = Player.CardInfo[Command.Serial];

            var index = Calc.CardEvent.GetIndex(card.ID, Command.InteractiveID);

            var isFinished = card.IsEventFinished(index);

            if (!isFinished)
            {
                var data = Global.Tables.Interactive[Command.InteractiveID];

                var ids = new HashSet<int>();
                CardUtils.GetIDs(Player.CardInfo.List, ids);

                var errCode = Calc.CardEvent.Check(data, card.Kizuna, ids, Player.QuestInfo.Get);

                if (errCode != ErrorCode.Success) return errCode;

                complex = new Complex();
                complex[ComplexIndex.CardEvent + index] = true;

                foreach (var item in Global.Tables.EventShow.Select(o => { return o.n_GROUP == data.n_CALL_EVENT; }))
                {
                    switch (item.n_RESULT)
                    {
                        case EventResult.Gift:
                            res.DropID = Calc.Drop.Next(item.n_RESULT_X);
                            if (res.DropID > 0)
                            {
                                var drop = new DropObj(Player, RewardSource.CardEvent, Command.InteractiveID);
                                drop.Add(res.DropID);
                                AddAssistObj(drop);
                            }
                            break;
                        case EventResult.Kizuna:
                            kizunaDiff = Math.Min(card.Kizuna + item.n_RESULT_X, Global.Tables.KizunaLimit) - card.Kizuna;
                            break;
                        case EventResult.Quest:
                            break;
                    }
                }

                using (var db = AcquireDB())
                {
                    using (var trans = db.BeginTransaction())
                    {
                        var cmd = default(IDBCommand);

                        cmd = DBCommandBuilder.UpdateCardComplex(PublicID, card.Serial, complex.Value);

                        db.SimpleQuery(cmd);

                        if (kizunaDiff != 0)
                        {
                            cmd = DBCommandBuilder.AddCardKizuna(PublicID, card.Serial, kizunaDiff);
                            db.SimpleQuery(cmd);
                        }

                        ApplyAssists(db);

                        trans.Commit();
                    }
                }
            }

     

            res.Serial = Command.Serial;
            res.InteractiveID = Command.InteractiveID;

            if (complex != null || kizunaDiff != 0)
            {
                var diff = new CardDiff { Serial = card.Serial };
                if(complex != null) diff.Add(PropertyID.Complex, ComplexIndex.CardEvent + index, 1);
                if(kizunaDiff != 0) diff.Add(PropertyID.Kizuna, card.Kizuna, card.Kizuna + kizunaDiff);

                if (res.CardSync.UpdateList == null) res.CardSync.UpdateList = new List<CardDiff>(1);
                res.CardSync.UpdateList.Add(diff);
            }

            return ErrorCode.Success;
        }
    }

    [ActiveTask(CommandType = typeof(AwakenRequest))]
    public class CardAwakenTask : PlayerTask<AwakenRequest, AwakenResponse>
    {
        private Card card;
        private Complex complex;
        private Dictionary<int, ValuePair> items;

        protected override void OnCommit(Context context)
        {
            card.Complex.Value = complex.Value;

            if (Command.Group == 0) card.Exp = 0;

            foreach (var item in items) Player.ItemInfo.Add(item.Key, item.Value.Diff);

            AddTrophyTask(new AwakenTrophyTask { PublicID = PublicID, Awaken = Command.Group });

            if (card == Player.CardInfo.Header) Player.UpdateSnapshot(CurrentTime);

            base.OnCommit(context);
        }
        protected override ErrorCode SafeProcessCommand(AwakenResponse res)
        {
            card = Player.CardInfo[Command.Serial];
            
            complex = new Complex { Value = card.Complex.Value };

            var diff = default(CardDiff);

            var data = default(ConstData.Awaken);

            if (Command.Group == 0)
            {
                var rare = card.Rare;
                if (rare == 0) rare = Global.Tables.Card[card.ID].n_RARE;

                var balance = Global.Tables.CardBalance[rare];

                if (CardUtils.CalcLV(Global.Tables.Card[card.ID], balance, card.Exp) < (balance.n_LVMAX - 1))
                {
                    KernelService.Logger.Trace("[AWAKEN_FAIL1] player_id=[{0},{1}|{2}], card_serial={3}, card_id={4}, exp={5}, rare={6}, complex={7}", PublicID.Group, PublicID.Serial, PublicID.ToString(), card.Serial, card.ID, card.Exp, card.Rare, card.Complex.Value);
                    return ErrorCode.CardIllegal;
                }

                data = Global.Tables.Awaken.SelectFirst(o => { return o.n_CARDID == card.ID && o.n_GROUP == 0 && o.n_RARE == rare; });
                if (data != null)
                {
                    complex.Set(ComplexIndex.CardRare, ComplexIndex.CardRareLen, rare + 1);
                    diff = CardDiff.Create(card.Serial, PropertyID.Rare, rare, rare + 1);
                    diff.Add(PropertyID.Exp, card.Exp, 0);
                }
            }
            else
            {
                var nextLv = card.GetAwakenLV(Command.Group) + 1;
                data = Global.Tables.Awaken.SelectFirst(o => { return o.n_CARDID == card.ID && o.n_GROUP == Command.Group && o.n_LV == nextLv; });
                if (data != null)
                {
                    complex.Set(ComplexIndex.CardAwaken + Command.Group * ComplexIndex.CardAwakenLen, ComplexIndex.CardAwakenLen, nextLv);
                    diff = CardDiff.Create(card.Serial, PropertyID.Awaken, Command.Group, nextLv);
                }
            }

            if (data == null)
            {
                KernelService.Logger.Trace("[AWAKEN_FAIL2] player_id=[{0},{1}|{2}], card_serial={3}, card_id={4}, exp={5}, rare={6}, complex={7}", PublicID.Group, PublicID.Serial, PublicID.ToString(), card.Serial, card.ID, card.Exp, card.Rare, card.Complex.Value);
                return ErrorCode.CardIllegal;
            }

            if (card.Kizuna < data.n_KIZUNA) return ErrorCode.KizunaNotEnough;

            items = new Dictionary<int, ValuePair>();

            for (int i = 0; i < data.n_MATERIAL.Length; ++i)
            {
                var itemId = data.n_MATERIAL[i];
                var itemCount = data.n_COUNT[i];
                if (itemId == 0 || itemCount == 0) continue;

                var originCount = Player.ItemInfo.Count(itemId);
                if (itemCount > originCount) return ErrorCode.ItemNotEnough;
                items.Add(itemId, ValuePair.Create(originCount, originCount - itemCount));
            }

           
            using (var db = AcquireDB())
            {
                using (var trans = db.BeginTransaction())
                {
                    var cmd = DBCommandBuilder.ReplaceCardComplex(PublicID, Command.Serial, complex.Value);
                    db.SimpleQuery(cmd);

                    if (Command.Group == 0)
                    {
                        cmd = DBCommandBuilder.AddCardExp(PublicID, card.Serial, -card.Exp);
                        db.SimpleQuery(cmd);
                    }

                    foreach (var item in items)
                    {
                        cmd = DBCommandBuilder.IncreaseItemCount(PublicID, item.Key, item.Value.Diff);
                        db.SimpleQuery(cmd);
                    }

                    trans.Commit();
                }
            }

            res.Serial = Command.Serial;
            
            if (res.CardSync.UpdateList == null) res.CardSync.UpdateList = new List<CardDiff>(1);
            res.CardSync.UpdateList.Add(diff);

            foreach (var item in items) res.AddSyncData(new SyncItem { Diff = Diff.Create(item.Key, item.Value.OriginValue, item.Value.NewValue) });
           

            return ErrorCode.Success;
        }
    }

    [ActiveTask(CommandType = typeof(LockRequest))]
    public class CardLockTask : PlayerTask<LockRequest, LockResponse>
    {
        private List<Card> locked;
        private List<Card> unlocked;
        protected override void OnCommit(Context context)
        {
            foreach (var item in locked) item.IsLocked = true;
            foreach (var item in unlocked) item.IsLocked = false;

            base.OnCommit(context);
        }
        protected override ErrorCode SafeProcessCommand(LockResponse res)
        {
            locked = Player.CardInfo.List.FindAll(o => { return Command.Locked.Contains(o.Serial.Value); });

            unlocked = Player.CardInfo.List.FindAll(o => { return Command.Unlocked.Contains(o.Serial.Value); });

            if (locked.Count != Command.Locked.Count || unlocked.Count != Command.Unlocked.Count) return ErrorCode.CardNotExists;

            if (res.CardSync.UpdateList == null) res.CardSync.UpdateList = new List<CardDiff>();

            using (var db = AcquireDB())
            {
                UpdateLocked(db, locked, true, res);
                UpdateLocked(db, unlocked, false, res);
                foreach (var item in unlocked)
                {
                    if (!item.IsLocked) continue;
                    var complex = new Complex { Value = item.Complex.Value };
                    complex[ComplexIndex.CardIsLocked] = false;
                    var cmd = DBCommandBuilder.ReplaceCardComplex(PublicID, item.Serial, complex.Value);
                    db.SimpleQuery(cmd);
                }
            }

            return ErrorCode.Success;
        }

        private void UpdateLocked(IDBConnection db, List<Card> list, bool locked, CSAGACommand syncTarget)
        {
            foreach (var item in list)
            {
                if (item.IsLocked == locked) continue;
                var complex = new Complex { Value = item.Complex.Value };
                complex[ComplexIndex.CardIsLocked] = locked;
                var cmd = DBCommandBuilder.ReplaceCardComplex(PublicID, item.Serial, complex.Value);
                db.SimpleQuery(cmd);

                var diff = CardDiff.Create(item.Serial, PropertyID.Complex, ComplexIndex.CardIsLocked, locked ? 1 : 0);
                syncTarget.CardSync.UpdateList.Add(diff);
            }
        }
    }

    [ActiveTask(CommandType = typeof(HeaderRequest))]
    public class CardHeaderTask : PlayerTask<HeaderRequest, HeaderResponse>
    {
        
        protected override void OnCommit(Context context)
        {
            Player.CardInfo.SetHeader(Command.Serial);
            Player.UpdateSnapshot(CurrentTime);

            base.OnCommit(context);
        }
        protected override ErrorCode SafeProcessCommand(HeaderResponse res)
        {
            
            return ErrorCode.Success;
        }
    }

    [ActiveTask(CommandType = typeof(LuckRequest))]
    public class CardLuckTask : PlayerTask<LuckRequest, LuckResponse>
    {
        
        protected override ErrorCode SafeProcessCommand(LuckResponse res)
        {
            var card = Player.CardInfo[Command.Serial];

            if (card == null) return ErrorCode.CardNotExists;

            var data = Tables.Card[card.ID];

            var cost = data.n_LUCK_GIFT / 100;

            var payment = new PaymentObj(Player, ArticleID.AddLuck, DropType.Item, cost, Tables.LuckItemID);

            if (payment.Origin < cost) return ErrorCode.ItemNotEnough;

            AddAssistObj(payment);

            var luckObj = new LuckObj();
            luckObj.Add(card.ID, 1);

            AddAssistObj(luckObj);

            using (var db = AcquireDB())
            {
                using (var trans = db.BeginTransaction())
                {
                    ApplyAssists(db);
                    trans.Commit();
                }
            }

            res.Serial = Command.Serial;

            return ErrorCode.Success;
        }

    }

}
