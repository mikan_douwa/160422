﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mikan.Server;
using Mikan.CSAGA.Commands.RandomShop;

namespace Mikan.CSAGA.Server.Tasks
{
    [ActiveTask(CommandType = typeof(InfoRequest))]
    public class RandomShopInfoTask : PlayerTask<InfoRequest, InfoResponse>
    {
        private RandomShopInfo randomShopInfo;

        protected override void OnCommit(Context context)
        {
            Player.RandomShopInfo = randomShopInfo;

            if (Command.ForceRefresh) Player.ItemInfo.Add(Global.Tables.RandomShopRefreshItemID, -1);

            base.OnCommit(context);
        }

        protected override ErrorCode SafeProcessCommand(InfoResponse res)
        {
            var existing = Player.RandomShopInfo != null;

            if (!existing)
            {
                using (var db = AcquireDB())
                {
                    var cmd = DBCommandBuilder.SelectRandomShop(PublicID);
                    using (var dbRes = db.DoQuery(cmd))
                    {
                        var obj = DBObjectConverter.ConvertCurrent<DBRandomShop>(dbRes);
                        if (obj != null)
                        {
                            existing = true;

                            if (obj.UpdateTime.AddMinutes(Global.Tables.RandomShopRefreshTime) > CurrentTime)
                            {
                                randomShopInfo = new RandomShopInfo();
                                randomShopInfo.List = Player.Context.RandomShop.Select(obj.Seed).ToList();
                                randomShopInfo.State = new ComplexI32 { Value = obj.State };
                                randomShopInfo.UpdateTime = obj.UpdateTime;
                            }
                        }
                    }
                }
            }


            if (Command.ForceRefresh)
            {
                var count = Player.ItemInfo.Count(Global.Tables.RandomShopRefreshItemID);
                if(count == 0) return ErrorCode.ItemNotEnough;

                using (var db = AcquireDB())
                {
                    using (var trans = db.BeginTransaction())
                    {
                        var cmd = DBCommandBuilder.IncreaseItemCount(PublicID, Global.Tables.RandomShopRefreshItemID, -1);
                        db.SimpleQuery(cmd);
                        randomShopInfo = Player.RefreshRandomShop(db, CurrentTime, existing);
                        trans.Commit();
                    }
                }

                res.AddSyncData(new SyncItem { Diff = Diff.Create(Global.Tables.RandomShopRefreshItemID, count, count - 1) });
            }
            else
            {

                if (Player.RandomShopInfo != null)
                {
                    if (CurrentTime < Player.RandomShopInfo.UpdateTime.AddMinutes(Global.Tables.RandomShopRefreshTime))
                        randomShopInfo = Player.RandomShopInfo;
                }

                if (randomShopInfo == null)
                {
                    using (var db = AcquireDB()) randomShopInfo = Player.RefreshRandomShop(db, CurrentTime, existing);
                }
            }

            res.RandomShopInfo = randomShopInfo.Serialize();
            return ErrorCode.Success;
        }
    }

    [ActiveTask(CommandType = typeof(PurchaseRequest))]
    public class RandomShopPurchaseTask : PlayerTask<PurchaseRequest, PurchaseResponse>
    {
        private int newState;

        private RandomShopInfo randomShopInfo;

        private GachaListTag tag;

        private List<DBSocialPurchase> list;

        private DBSocialPurchase purchase;

        protected override void OnPrepare(Context context)
        {
            base.OnPrepare(context);
            randomShopInfo = Player.RandomShopInfo;
            list = Player.SocialPurchaseList;
        }

        protected override void OnCommit(Context context)
        {
            switch(tag)
            {
                case GachaListTag.SocialShop:
                {
                    Player.SocialPurchaseList = list;
                    Player.SocialPurchaseList.Add(purchase);
                    break;
                }
                case GachaListTag.Random:
                case GachaListTag.Static:
                {
                    Player.RandomShopInfo = randomShopInfo;
                    Player.RandomShopInfo.State.Value = newState;
                    break;
                }
                default:
                    break;
            }

            AddLog(LogType.Shop);

            base.OnCommit(context);
        }

        protected override ErrorCode SafeProcessCommand(PurchaseResponse res)
        {

            var data = Global.Tables.GachaList[Command.ArticleID];
            tag = data.n_TAG;

            var articleId = default(ArticleID);
            var source = default(RewardSource);

            switch (tag)
            {
                case GachaListTag.SocialShop:
                {
                    articleId = ArticleID.SocialShop;
                    source = RewardSource.SocialShop;

                    if (list == null) list = SocialUtils.QueryPurchaseList(PublicID, CurrentTime.Date).ToList();

                    foreach (var item in list)
                    {
                        if (item.ArticleID == Command.ArticleID && item.Timestamp >= CurrentTime.Date) return ErrorCode.ArticleNotAvailable;
                    }
                    break;
                }
                case GachaListTag.Random:
                case GachaListTag.Static:
                {
                    if (randomShopInfo == null)
                    {
                        randomShopInfo = RandomShopUtils.Query(Player, CurrentTime);
                    }

                    var limit = Global.Tables.RandomShopSlotCalc.Count(Player.PurchaseInfo.Count(ArticleID.RandomShopSlot));
                    if (!CheckArticle(Command.ArticleID, Command.Index, limit)) return ErrorCode.ArticleNotAvailable;

                    articleId = ArticleID.RandomShop;
                    source = RewardSource.RandomShop;
                    break;
                }
                case GachaListTag.CommonShop:
                {
                    articleId = ArticleID.CommonShop;
                    source = RewardSource.CommonShop;
                    break;
                }
            }

            /*
            if (tag == GachaListTag.SocialShop)
            {
                articleId = ArticleID.SocialShop;
                source = RewardSource.SocialShop;

                if (list == null) list = SocialUtils.QueryPurchaseList(PublicID, CurrentTime.Date).ToList();

                foreach (var item in list)
                {
                    if (item.ArticleID == Command.ArticleID && item.Timestamp >= CurrentTime.Date) return ErrorCode.ArticleNotAvailable;
                }
            }
            else
            {
                if (randomShopInfo == null)
                {
                    randomShopInfo = RandomShopUtils.Query(Player, CurrentTime);
                }

                var limit = Global.Tables.RandomShopSlotCalc.Count(Player.PurchaseInfo.Count(ArticleID.RandomShopSlot));
                if (!CheckArticle(Command.ArticleID, Command.Index, limit)) return ErrorCode.ArticleNotAvailable;

                articleId = ArticleID.RandomShop;
                source = RewardSource.RandomShop;
            }
            */

            var coinType = data.n_COINTYPE;
            var coinId = data.n_COINID;
            var cost = data.n_COIN;

            if (tag == GachaListTag.Static)
            {
                var _drop = Global.Tables.Drop[data.n_GOODS];
                if (_drop.n_DROP_TYPE == DropType.Card && Player.CardInfo.GetLuck(_drop.n_DROP_ID) > 0)
                    cost = data.n_COIN2;
            }

            var payment = new PaymentObj(Player, articleId, coinType, cost, coinId);

            if (payment.Origin < cost)
            {
                switch (coinType)
                {
                    case DropType.Gem:
                        return ErrorCode.GemNotEnough;
                    case DropType.Crystal:
                        return ErrorCode.CrystalNotEnough;
                    case DropType.Coin:
                        return ErrorCode.CoinNotEnough;
                    case DropType.FP:
                        return ErrorCode.FPNotEnough;
                    case DropType.Item:
                        return ErrorCode.ItemNotEnough;
                }
            }
            AddAssistObj(payment);

            var drop = new DropObj(Player, source);

            if (!drop.Add(data.n_GOODS)) return ErrorCode.ArticleOverflow;

            AddAssistObj(drop);

            res.ArticleID = Command.ArticleID;
            res.DropID = data.n_GOODS;

            using (var db = AcquireDB())
            {
                using (var trans = db.BeginTransaction())
                {
                    if (tag == GachaListTag.SocialShop)
                    {
                        var cmd = DBCommandBuilder.InsertSocialPurchase(PublicID, Command.ArticleID, cost, CurrentTime);
                        db.SimpleQuery(cmd);

                        purchase = new DBSocialPurchase { ArticleID = Command.ArticleID, Cost = cost, Timestamp = CurrentTime };

                        res.AddSyncData(new SyncSocialPurchase { AricleID = Command.ArticleID, Timestamp = CurrentTime });
                    }
                    else if(tag == GachaListTag.Random || tag == GachaListTag.Static)
                    {
                        if (Command.Index < 0)
                            newState = randomShopInfo.State.Value;
                        else
                        {
                            newState = randomShopInfo.State.Combin(ComplexI32.Bit(Command.Index));
                            var cmd = DBCommandBuilder.UpdateRandomShopState(PublicID, newState);
                            db.SimpleQuery(cmd);
                        }

                        res.State = newState;
                        
                    }
                    
                    ApplyAssists(db);

                    trans.Commit();
                }
            }

            if (Command.Index < 0) KernelService.Logger.Debug("[RANDOMSHOP] item_id={0}, coin_type={1}, cost={2}", data.n_ID, coinType, cost);

            return ErrorCode.Success;
        }

        private bool CheckArticle(int id, int index, int limit)
        {
            if (index >= limit) return false;

            if (index < 0)
            {
                var data = Global.Tables.GachaList[id];
                if (data.n_TAG != GachaListTag.Static) return false;

                if (data.n_TIME > 0)
                {
                    var year = 2000 + data.n_TIME / 10000;
                    var month = (data.n_TIME % 10000) / 100;
                    var day = data.n_TIME % 100;
                    var endTime = new DateTime(year, month, day).AddDays(1);
                    if (endTime < CurrentTime) return false;
                }

                return true;
            }
            else
            {
                if (randomShopInfo.List[index] != id) return false;
                return !randomShopInfo.State[index];
            }
        }
    }

    public class RandomShopUtils
    {
        public static RandomShopInfo Query(Player player, DateTime currentTime)
        {
            using (var db = Global.DBPool.Acquire(player.ID.Group))
            {
                var cmd = DBCommandBuilder.SelectRandomShop(player.ID);
                using (var dbRes = db.DoQuery(cmd))
                {
                    var obj = DBObjectConverter.ConvertCurrent<DBRandomShop>(dbRes);
                    if (obj != null)
                    {
                        if (obj.UpdateTime.AddMinutes(Global.Tables.RandomShopRefreshTime) > currentTime)
                        {
                            var randomShopInfo = new RandomShopInfo();
                            randomShopInfo.List = player.Context.RandomShop.Select(obj.Seed).ToList();
                            randomShopInfo.State = new ComplexI32 { Value = obj.State };
                            randomShopInfo.UpdateTime = obj.UpdateTime;
                            return randomShopInfo;
                        }
                    }

                    return null;
                }
            }
        }
    }
}
