﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mikan.CSAGA.Commands.Equipment;
using Mikan.Server;

namespace Mikan.CSAGA.Server.Tasks
{
    [ActiveTask(CommandType = typeof(InfoRequest))]
    public class EquipmentInfoTask : PlayerTask<InfoRequest, InfoResponse>
    {
        protected override ErrorCode SafeProcessCommand(InfoResponse res)
        {
            res.EquipmentInfo = Player.EquipmentInfo.Serialize();
            return ErrorCode.Success;
        }
    }

    [ActiveTask(CommandType = typeof(ReplaceRequest))]
    public class EquipmentReplaceTask : PlayerTask<ReplaceRequest, ReplaceResponse>
    {
        private Equipment oldObj;
        private Equipment newObj;

        protected override void OnCommit(Context context)
        {
            if (oldObj != null) Player.EquipmentInfo.Remove(oldObj);
            newObj.CardSerial = Serial.Create(Command.CardSerial);
            newObj[PropertyID.Slot] = Command.Slot;

            AddTrophyTask(new EquipTrophy { PublicID = PublicID });

            if (Command.CardSerial == Player.CardInfo.Header.Serial.Value) Player.UpdateSnapshot(CurrentTime);

            base.OnCommit(context);
        }
        
        protected override ErrorCode SafeProcessCommand(ReplaceResponse res)
        {
            newObj = Player.EquipmentInfo[Command.Serial];

            if (newObj == null) return ErrorCode.EquipmentNotExists;

            if (!newObj.CardSerial.IsEmpty) return ErrorCode.EquipmentIllegal;

            var card = Player.CardInfo[Command.CardSerial];

            var data = Tables.Item[newObj.ID];

            var isBind = (data.n_CARD == card.ID);

            foreach (var item in Player.EquipmentInfo.List)
            {
                if (item.CardSerial.Value != Command.CardSerial) continue;

                if (item[PropertyID.Slot] == Command.Slot)
                {
                    oldObj = item;
                    if (!isBind) break;
                }
                else if (isBind && item.ID == newObj.ID)
                    return ErrorCode.EquipmentDuplicate;
            }            

            using (var db = AcquireDB())
            {
                using (var trans = db.BeginTransaction())
                {
                    var cmd = default(IDBCommand);
                    if (oldObj != null)
                    {
                        KernelService.Logger.Trace("[REPLACE] player_id=[{0},{1}], equip_serial={2}, equip_id={3}, hp={4}, atk={5}, rev={6}, chg={7}", Player.ID.Group, Player.ID.Serial, oldObj.Serial, oldObj.ID, oldObj.Hp, oldObj.Atk, oldObj.Rev, oldObj.Chg);
                        
                        cmd = DBCommandBuilder.DeleteEquipment(PublicID, oldObj.Serial);
                        db.SimpleQuery(cmd);
                    }
                    cmd = DBCommandBuilder.UpdateEquipment(PublicID, newObj.Serial, Command.CardSerial, Command.Slot);
                    db.SimpleQuery(cmd);

                    trans.Commit();
                }
            }

            #region Sync

            if (oldObj != null)
            {
                res.EquipmentSync.DeleteList = new List<long>(1);
                res.EquipmentSync.DeleteList.Add(oldObj.Serial);
            }

            res.EquipmentSync.UpdateList = new List<EquipmentDiff>(1);

            var diff = new EquipmentDiff { Serial = newObj.Serial, CardSerial = Int64ValuePair.Create(newObj.CardSerial.Value, Command.CardSerial) };
            diff.Add(PropertyID.Slot, newObj[PropertyID.Slot], Command.Slot);

            res.EquipmentSync.UpdateList.Add(diff);

            #endregion


            return ErrorCode.Success;
        }
    }

    [ActiveTask(CommandType = typeof(SellRequest))]
    public class EquipmentSellTask : PlayerTask<SellRequest, SellResponse>
    {
        private List<Equipment> list;
        private int origin;
        private int crystal;
        protected override void OnCommit(Context context)
        {
            if (list != null)
            {
                var ids = new List<int>();
                Player.PlayerInfo[PropertyID.Crystal] = origin + crystal;
                foreach (var item in list)
                {
                    Player.EquipmentInfo.Remove(item);
                    ids.Add(item.ID);
                   
                }

                AddLog(LogType.Sell, ids.ToArray());
            }

            base.OnCommit(context);
        }

        protected override ErrorCode SafeProcessCommand(SellResponse res)
        {
            if (Command.List.Count > 0)
            { 
                origin = Player.PlayerInfo[PropertyID.Crystal];

                list = new List<Equipment>(Command.List.Count);

                foreach (var item in Command.List)
                {
                    var obj = Player.EquipmentInfo[item];
                    if (obj == null) return ErrorCode.EquipmentNotExists;
                    if (!obj.CardSerial.IsEmpty || obj.Slot != 0) return ErrorCode.EquipmentIllegal;
                    crystal += Global.Tables.Item[obj.ID].n_EXP_GAIN;
                    list.Add(obj);

                    KernelService.Logger.Trace("[SELL] player_id=[{0},{1}|{2}], equip_serial={3}, equip_id={4}, hp={5}, atk={6}, rev={7}, chg={8}", PublicID.Group, PublicID.Serial, PublicID.ToString(), obj.Serial, obj.ID, obj.Hp, obj.Atk, obj.Rev, obj.Chg);
                        
                }

                KernelService.Logger.Trace("[SELL_PRICE] player_id=[{0},{1}|{2}], crystal={3}", PublicID.Group, PublicID.Serial, PublicID.ToString(), crystal);
                    

                using (var db = AcquireDB())
                {
                    using (var trans = db.BeginTransaction())
                    {
                        var cmd = DBCommandBuilder.DeleteEquipment(PublicID, Command.List);
                        db.SimpleQuery(cmd);

                        cmd = DBCommandBuilder.AddCrystal(PublicID, crystal);
                        db.SimpleQuery(cmd);

                        trans.Commit();
                    }
                }

                res.PlayerSync(PropertyID.Crystal, origin, origin + crystal);
                res.EquipmentSync.DeleteList = Command.List;
            }

            return ErrorCode.Success;
        }
    }

    [ActiveTask(CommandType = typeof(RemoveRequest))]
    public class EquipmentRemoveTask : PlayerTask<RemoveRequest, RemoveResponse>
    {
        private Equipment obj;

        protected override void OnCommit(Context context)
        {
            var cardSerial = obj.CardSerial;

            obj.CardSerial = Serial.Empty;
            obj[PropertyID.Slot] = 0;

            if (cardSerial == Player.CardInfo.Header.Serial) Player.UpdateSnapshot(CurrentTime);

            base.OnCommit(context);
        }

        protected override ErrorCode SafeProcessCommand(RemoveResponse res)
        {
            obj = Player.EquipmentInfo[Command.Serial];

            if (obj == null || obj.CardSerial.IsEmpty) return ErrorCode.EquipmentIllegal;

            var data = Global.Tables.Item[obj.ID];

            var payment = new PaymentObj(Player, ArticleID.RandomShop, data.n_TAKE_COINID, data.n_TAKE_COIN);

            if (payment.Origin < data.n_TAKE_COIN)
            {
                switch (data.n_TAKE_COINID)
                {
                    case DropType.Gem:
                        return ErrorCode.GemNotEnough;
                    case DropType.Crystal:
                        return ErrorCode.CrystalNotEnough;
                    case DropType.Coin:
                        return ErrorCode.CoinNotEnough;
                }
            }

            AddAssistObj(payment);

            using (var db = AcquireDB())
            {
                using (var trans = db.BeginTransaction())
                {
                    var cmd = DBCommandBuilder.UpdateEquipment(PublicID, obj.Serial, 0, 0);

                    db.SimpleQuery(cmd);
                    

                    ApplyAssists(db);

                    trans.Commit();
                }
            }

            #region Sync

            res.Serial = obj.Serial;
            res.CardSerial = obj.CardSerial.Value;

            res.EquipmentSync.UpdateList = new List<EquipmentDiff>(1);

            var diff = new EquipmentDiff { Serial = obj.Serial, CardSerial = Int64ValuePair.Create(obj.CardSerial.Value, 0) };
            diff.Add(PropertyID.Slot, obj[PropertyID.Slot], 0);

            res.EquipmentSync.UpdateList.Add(diff);

            #endregion


            return ErrorCode.Success;
        }
    }

    [ActiveTask(CommandType = typeof(LockRequest))]
    public class EquipmentLockTask : PlayerTask<LockRequest, LockResponse>
    {
        private Equipment obj;

        private int flag;

        protected override void OnCommit(Context context)
        {
            obj[PropertyID.Slot] = flag;

            base.OnCommit(context);
        }

        protected override ErrorCode SafeProcessCommand(LockResponse res)
        {
            obj = Player.EquipmentInfo[Command.Serial];

            if (obj == null || !obj.CardSerial.IsEmpty) return ErrorCode.EquipmentIllegal;

            flag = Command.IsLocked ? 1 : 0;

            if (obj.Slot != flag)
            {
                using (var db = AcquireDB())
                {
                    var cmd = DBCommandBuilder.UpdateEquipment(PublicID, obj.Serial, 0, flag);
                    db.SimpleQuery(cmd);

                }
            }

            #region Sync

            res.Serial = obj.Serial;

            res.EquipmentSync.UpdateList = new List<EquipmentDiff>(1);

            var diff = EquipmentDiff.Create(obj.Serial, PropertyID.Slot, obj.Slot, flag);
            res.EquipmentSync.UpdateList.Add(diff);

            #endregion


            return ErrorCode.Success;
        }
    }
}
