﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mikan.Server;
using Mikan.CSAGA.Commands.Gacha;

namespace Mikan.CSAGA.Server.Tasks
{
    [ActiveTask(CommandType = typeof(DrawRequest))]
    public class GachaDrawTask : PlayerTask<DrawRequest, DrawResponse>
    {
        private ConstData.GachaList data;

        private IGachaMachine gacha;

        private IGachaMachine rareGacha;

        private IGachaMachine jackpot;

        private DBMyLord mylord;

        private bool europe = false;

        private PaymentObj payment;

        private List<Card> list;

        private bool isFirstGacha;

        private int gachaBoost;

        private int count;

        protected override void OnPrepare(Context context)
        {
            base.OnPrepare(context);

            data = Global.Tables.GachaList[Command.GachaID];

            var gachaId = context.GachaSchedule.GetGachaID(Command.GachaID, CurrentTime);

            if (gachaId == 0) return;

            gacha = context.GachaMachine.Get(gachaId);

            if(data.n_CONTI == 10)
                rareGacha = context.GachaMachine.Get(-gachaId);

            jackpot = context.GachaJackpot.Get(gachaId);

            mylord = Player.MyLord;
        }

        protected override void OnCommit(Context context)
        {
            Player.CardInfo.AddRange(list);

            if (mylord != null)
            {
                if (europe)
                    mylord.GachaMiss = 0;
                else if(payment.IAPCost > 0)
                {
                    mylord.GachaMiss += payment.IAPCost;
                    if (mylord.MaxGachaMiss < mylord.GachaMiss)
                        mylord.MaxGachaMiss = mylord.GachaMiss;
                }

                Player.MyLord = mylord;
            }

            if (isFirstGacha)
                Player.PlayerInfo.UpdateComplex(ComplexIndex.PlayerFirstGacha, true);
            else
            {
                Player.PlayerInfo.UpdateComplex(ComplexIndex.GachaBoost, ComplexIndex.GachaBoostLen, gachaBoost);
                AddLog(LogType.Gacha, count);
            }

            AddTrophyTask(new GachaTrophy { PublicID = PublicID });

            base.OnCommit(context);
        }

        protected override ErrorCode SafeProcessCommand(DrawResponse res)
        {
            if (gacha == null) return ErrorCode.GachaIllegal;

            isFirstGacha = Command.GachaID == Global.Tables.FirstGachaID;

            if (isFirstGacha && Player.PlayerInfo.IsFirstGachaDone)
            {
                return ErrorCode.PlayerIllegal;
            }

            var coinType = data.n_COINTYPE;

            var cost = data.n_COIN;

            count = data.n_CONTI;

            if (count < 1) count = 1;

            var iapLimited = coinType == DropType.Gem && (data.n_ID == Global.Tables.RegularGachaIDs[0] || data.n_ID == Global.Tables.ActivityGachaIDs[0]);

            var articleId = iapLimited? ArticleID.RegularGachaDaily : ArticleID.RegularGacha;

            var isRegular = true;

            if (Global.Tables.ActivityGachaIDs.Contains(data.n_ID))
            {
                if (data.n_ID == Global.Tables.ActivityGachaIDs[0])
                    articleId = ArticleID.ActivityGachaDaily;
                else
                    articleId = ArticleID.ActivityGacha;

                isRegular = false;
            }

            if (iapLimited)
            {
                var article = Player.PurchaseInfo.GetArticle(articleId);
                if (article != null && article.LastPurchase.Date == CurrentTime.Date) return ErrorCode.GachaIllegal;
            }

            payment = new PaymentObj(Player, articleId, iapLimited ? DropType.IAPGem : coinType, cost);

            if (payment.Origin < cost)
            {
                switch (coinType)
                {
                    case DropType.Gem:
                        return ErrorCode.GemNotEnough;
                    case DropType.Crystal:
                        return ErrorCode.CrystalNotEnough;
                    case DropType.Coin:
                        return ErrorCode.CoinNotEnough;
                }
            }
            
            AddAssistObj(payment);

            list = new List<Card>(count);

            res.CardSync.AddList = new List<Internal.CardObj>(count);

            var luckObj = new LuckObj();
            luckObj.IsGacha = true;
            AddAssistObj(luckObj);

            if (mylord == null) mylord = MyLordUtils.QueryMyLord(PublicID);

            var africa = true;

            var retryCount = 0;

            while (true)
            {
                africa = true;
                list.Clear();

                for (int i = 0; i < count; ++i)
                {
                    var id = gacha.Next();

                    var gachaData = Global.Tables.Gacha[id];
                    if (gachaData.n_SHOWCARD != 0)
                    {
                        africa = false;
                        if (gachaData.n_SHOWCARD == 2) europe = true;
                    }

                    var card = Convert(gachaData);


                    list.Add(card);
                }

                if (!gacha.Jackpot || europe || mylord == null) break;

                if (mylord.GachaMiss >= 3000)
                {
                    // 花費3000有償石以上,每多1000多一次機會
                    var chance = (1 + (int)Math.Floor((mylord.GachaMiss - 3000) / 1000f)) * 4;
                    if (retryCount >= chance) break;
                    KernelService.Logger.Debug("[GACHA] retry = {0}", retryCount + 1);
                }
                else
                    break;

                ++retryCount;
            }

            if (africa && rareGacha != null)
            {
                if (Replace(rareGacha, list)) europe = true;
            }

            gachaBoost = Player.PlayerInfo.GachaBoost;

            if (!isFirstGacha)
            {
                // 情人節PICKUP(2/8 16:00~2/15 15:59)：期間內每天會有不同的神運必中角色，且抽到必中角色也不重置神運值
                var feverBegin = new DateTime(2017, 2, 8, 16, 0, 0);
                var feverEnd = new DateTime(2017, 2, 15, 16, 0, 0);

                if (isRegular && CurrentTime > feverBegin && CurrentTime < feverEnd)
                {
                    gachaBoost += count;

                    if (jackpot != null && gachaBoost > Tables.GachaBoost)
                    {
                        var boostEnd = new DateTime(2017, 2, 14, 16, 0, 0);

                        if (CurrentTime < boostEnd)
                        {
                            if (ReplaceViaBoost(jackpot, list))
                            {
                                europe = true;
                                gachaBoost -= (Tables.GachaBoost + 1);
                            }
                        }
                        else
                        {
                            if (Replace(jackpot, list))
                            {
                                europe = true;
                                gachaBoost -= (Tables.GachaBoost + 1);
                            }
                        }
                    }

                    if (gachaBoost > Tables.GachaBoost) gachaBoost = Tables.GachaBoost;
                }
                else
                {
                    if (europe)
                    {
                        gachaBoost = 0;
                    }
                    else
                    {
                        gachaBoost += count;

                        if (jackpot != null && gachaBoost > Tables.GachaBoost)
                        {
                            if (Replace(jackpot, list))
                            {
                                europe = true;
                                gachaBoost -= (Tables.GachaBoost + 1);
                            }
                        }

                        if (gachaBoost > Tables.GachaBoost) gachaBoost = Tables.GachaBoost;
                    }
                }
                
            }

            using (var db = AcquireDB())
            {
                using (var trans = db.BeginTransaction())
                {
                    foreach (var item in list)
                    {
                        var cmd = DBCommandBuilder.InsertCard(PublicID, item.ID, item.Complex.Value, item.Exp);
                        item.Serial = Serial.Create(db.DoQuery<Int64>(cmd));
                        res.CardSync.AddList.Add(item);

                        luckObj.Add(item.ID, 1);
                    }

                    ApplyAssists(db);

                    if (isFirstGacha)
                    {
                        var cmd = DBCommandBuilder.UpdatePlayerComplex(PublicID, Complex.Bit(ComplexIndex.PlayerFirstGacha).Value);
                        db.SimpleQuery(cmd);

                        cmd = DBCommandBuilder.UpdateSnapshot(PublicID, Player.Snapshot().Serialize(), CurrentTime);
                        db.SimpleQuery(cmd);
                    }
                    else
                    {
                        if (gachaBoost != Player.PlayerInfo.GachaBoost)
                        {
                            var complex = new Complex { Value = Player.PlayerInfo.Complex };
                            complex.Set(ComplexIndex.GachaBoost, ComplexIndex.GachaBoostLen, gachaBoost);

                            var cmd = DBCommandBuilder.ReplacePlayerComplex(PublicID, complex.Value);
                            db.SimpleQuery(cmd);
                        }

                        if (CurrentTime < new DateTime(2017, 2, 15, 16, 0, 0) && count == 10/* && Tables.ActivityGachaIDs.Contains(Command.GachaID)*/)
                        {
                            /*
                            var giftCount = count;
                            if (giftCount == 10) giftCount += 5;
                            */
                            var giftCount = 1;
                            var cmd = DBCommandBuilder.InsertGift(PublicID, RewardSource.GachaFeedback, "新年活動", DropType.Item, giftCount, Global.Tables.GachaGiftID);
                            db.SimpleQuery(cmd);
                        }
                    }

                    if (mylord == null && payment.IAPCost > 0)
                    {
                        var cmd = DBCommandBuilder.InsertMyLord(PublicID);
                        db.SafeSimpleQuery(cmd);
                        mylord = new DBMyLord();
                    }

                    if (mylord != null)
                    {
                        if (europe)
                        {
                            var cmd = DBCommandBuilder.UpdateMyLordGachaMiss(PublicID, 0, mylord.MaxGachaMiss);
                            db.SafeSimpleQuery(cmd);
                        }
                        else
                        {
                            var gachaMiss = mylord.GachaMiss + payment.IAPCost;
                            var gachaMissMax = (gachaMiss > mylord.MaxGachaMiss) ? gachaMiss : mylord.MaxGachaMiss;

                            var cmd = DBCommandBuilder.UpdateMyLordGachaMiss(PublicID, gachaMiss, gachaMissMax);
                            db.SafeSimpleQuery(cmd);
                        }
                    }


                    trans.Commit();
                }
            }

            res.GachaID = Command.GachaID;
            res.Count = count;

            if (isFirstGacha) res.PlayerSync(PropertyID.Complex, ComplexIndex.PlayerFirstGacha, 1);
            if (gachaBoost != Player.PlayerInfo.GachaBoost) res.PlayerSync(PropertyID.Complex, ComplexIndex.GachaBoost, gachaBoost);

            return ErrorCode.Success;
        }

        private bool Replace(IGachaMachine gacha, List<Card> list)
        {
            var id = gacha.Next();
            var data = Global.Tables.Gacha[id];
            var card = Convert(data);

            var replaceIndex = MathUtils.Random.Next(list.Count);
            list[replaceIndex] = card;
            return (data.n_SHOWCARD == 2);
        }

        private bool ReplaceViaBoost(IGachaMachine gacha, List<Card> list)
        {
            var id = gacha.Next();

            var firstDay = new DateTime(2017, 2, 8, 16, 0, 0);

            /*
            1/18 16:00~ 1/19 15:59玉藻前(25009)
            1/19 16:00~ 1/20 15:59雅典娜(25008)
            1/20 16:00~ 1/21 15:59伊邪那美(25007)
            1/21 16:00~ 1/22 15:59拉結爾(25006)
            1/22 16:00~ 1/23 15:59巴爾(25005)
            1/23 16:00~ 1/24 15:59薩多基爾(25004)
            1/24 16:00~ 1/25 15:59基加美修(25003)

            2/8 16:00~ 2/12 15:59情人節潘朵拉(26501)4天
            2/12 16:00~ 2/13 15:59七夕女媧(26502)1天
            2/13 16:00~ 2/14 15:59情人節蚩尤(26503)1天
            */
            var ids = new int[] { 26501, 26501, 26501, 26501, 26502, 26503 };

            for (int i = 0; i < ids.Length; ++i)
            {
                if (CurrentTime < firstDay.AddDays(i)) break;
                id = ids[i];
            }

            var data = Global.Tables.Gacha[id];
            var card = Convert(data);

            var replaceIndex = MathUtils.Random.Next(list.Count);
            list[replaceIndex] = card;
            return (data.n_SHOWCARD == 2);
        }

        private Card Convert(ConstData.Gacha gachaData)
        {
            var lv = MathUtils.Random.Next(gachaData.n_CARDLV_MIN, gachaData.n_CARDLV_MAX + 1);
            var exp = CardUtils.CalcExp(gachaData.n_CARDID, lv);

            var card = new Card
            {
                ID = gachaData.n_CARDID,
                Exp = exp
            };
            card.Complex = new Complex();
            card.Rare = gachaData.n_RARE;

            return card;
        }
    }
}
