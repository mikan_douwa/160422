﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mikan.Server;
using Mikan.CSAGA.Commands.Ranking;

namespace Mikan.CSAGA.Server.Tasks
{
    [ActiveTask(CommandType = typeof(InfoRequest))]
    public class RankingInfoTask : PlayerTask<InfoRequest, InfoResponse>
    {
        private EventScheduleList schedule;
        protected override void OnPrepare(Context context)
        {
            base.OnPrepare(context);
            schedule = context.EventScheduleList;
        }

        protected override void OnCommit(Context context)
        {
            Player.TrophyInfo.Commit();
            base.OnCommit(context);
        }

        protected override ErrorCode SafeProcessCommand(InfoResponse res)
        {
            var rankingInfo = default(RankingInfo);
            using (var db = AcquireDB()) rankingInfo = RankingInfo.Query(db, schedule, PublicID);
            
            foreach (var item in rankingInfo.List)
            {
                var obj = item.Value;
                if (obj.Point == 0/* || obj.Complex[0]*/) continue;
                Player.Trophy.OnEventPoint(obj.EventID, obj.Point);
            }

            res.RankingInfo = rankingInfo.Serialize();

            return ErrorCode.Success;
        }
    }

    [ActiveTask(CommandType = typeof(QueryRequest))]
    public class RankingQueryTask : PlayerTask<QueryRequest, QueryResponse>
    {
        private EventScheduleList schedule;
        protected override void OnPrepare(Context context)
        {
            base.OnPrepare(context);
            schedule = context.EventScheduleList;
        }

        protected override ErrorCode SafeProcessCommand(QueryResponse res)
        {
            var mainQuestId = Global.Tables.EventSchedule[Command.EventID].n_QUESTID;

            var ev = schedule.GetCurrent(mainQuestId);

            var from = Command.From;
            var count = 10;

            if (from == -1)
            {
                var self = ev.Players.SafeGetValue(PublicID);

                if (self != null)
                {
                    from = Math.Max(0, self.SortRanking - 5);
                    count = 11;
                }
                else
                    count = 0;

            }

            count = Math.Min(count, ev.RankingPlayer.Count - from);

            var list = new List<Internal.RankingPlayerObj>(count);

            for (int i = 0; i < count; ++i)
            {
                var item = ev.RankingPlayer[from + i];
                list.Add(item);
            }

            res.EventID = Command.EventID;
            res.From = Command.From;
            res.List = list;

            return ErrorCode.Success;
        }
    }
}
