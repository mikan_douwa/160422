﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mikan.Server;
using Mikan.CSAGA.Commands.Gallery;

namespace Mikan.CSAGA.Server.Tasks
{
    [ActiveTask(CommandType = typeof(SyncRequest))]
    public class GallerySyncTask : PlayerTask<SyncRequest, SyncResponse>
    {
        protected override void OnCommit(Context context)
        {
            Player.CommitCache();
            Player.TrophyInfo.Commit();
            base.OnCommit(context);
        }
        protected override ErrorCode SafeProcessCommand(SyncResponse res)
        {
            Player.Trophy.OnGalleryCountChange(Command.Count);
            return ErrorCode.Success;
        }
    }
}
