﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mikan.Server;
using Mikan.CSAGA.Commands.Quest;

namespace Mikan.CSAGA.Server.Tasks
{
    [ActiveTask(CommandType = typeof(OpenRequest))]
    public class QuestOpenTask : PlayerTask<OpenRequest, OpenResponse>
    {
        private int cost;

        private List<SharedQuest> list;

        protected override void OnPrepare(Context context)
        {
            base.OnPrepare(context);
        }

        protected override void OnCommit(Context context)
        {
            Player.QuestInfo.SharedList = list;
            Player.QuestInfo.Crusade.Discover -= cost;

            base.OnCommit(context);
        }

        protected override ErrorCode SafeProcessCommand(OpenResponse res)
        {
            var data = Global.Tables.QuestDesign[Command.QuestID];

            if (data == null || data.n_QUEST_TYPE != QuestType.Crusade) return ErrorCode.QuestNotExists;

            list = SharedQuestList.Query(PublicID);

            var oldList = list.FindAll(o => o.QuestID == Command.QuestID);

            foreach (var item in oldList)
            {
                if (item.EndTime >= CurrentTime) return ErrorCode.QuestExists;
            }

            var point = Player.QuestInfo.Crusade == null ? 0 : Player.QuestInfo.Crusade.Discover;

            cost = data.n_DISCOVER;

            if (point < cost) return ErrorCode.QuestPointNotEnough;

            var endTime = CurrentTime.AddMinutes(Global.Tables.CrusadeTime);

            var current = default(SharedQuest);

            using (var db = AcquireUniqueDB())
            {
                var cmd = DBCommandBuilder.InsertSharedQuest(PublicID, Command.QuestID, endTime);
                var obj = new DBSharedQuest { QuestID = Command.QuestID, EndTime = endTime, GroupID = PublicID.Group, PlayerID = PublicID.Serial };
                obj.ID = db.DoQuery<Int64>(cmd);
                current = SharedQuest.Create(obj);
            }

            using (var db = AcquireDB())
            {
                var cmd = DBCommandBuilder.AddCrusadeDiscover(PublicID, -cost);
                db.SimpleQuery(cmd);
            }

            res.PlayerSync(PropertyID.Discover, point, point - cost);

            var syncData = new SyncSharedQuest();
            syncData.Add(current);
            res.AddSyncData(syncData);

            return ErrorCode.Success;
        }
    }

    [ActiveTask(CommandType = typeof(QueryCrusadeRequest))]
    public class QuestQueryCrusadeTask : PlayerTask<QueryCrusadeRequest, QueryCrusadeResponse>
    {
        class SharedQuestWrapper
        {
            public int Index;
            public SharedQuest Quest;
            public int PlayCount;
        }

        private EventScheduleList schedule;

        private Crusade crusade;

        private SharedQuestList sharedList;

        private List<SharedQuest> list;

        private Dictionary<Int64, SharedQuestWrapper> map;

        private int addPoint = 0;

        protected override void OnPrepare(Context context)
        {
            base.OnPrepare(context);

            list = Player.QuestInfo.SharedList;

            schedule = context.EventScheduleList;

            sharedList = context.SharedQuestList;

            crusade = Player.QuestInfo.Crusade.Clone();
        }

        protected override void OnCommit(Context context)
        {
            Player.QuestInfo.Crusade = crusade;
            Player.QuestInfo.SharedList = list;

            if (map != null)
            {
                foreach (var item in map)
                {
                    if (item.Value.PlayCount > 0) item.Value.Quest.PlayCount += item.Value.PlayCount;
                }
            }

            base.OnCommit(context);
        }

        protected override ErrorCode SafeProcessCommand(QueryCrusadeResponse res)
        {
            var ids = new List<Int64>();
            var reserved = new List<Int64>();

            if (list == null) list = SharedQuestList.Query(PublicID);

            using (var db = AcquireDB())
            {
                if (crusade == null) crusade = Crusade.Query(db, PublicID, schedule.Crusade, CurrentTime);

                if (crusade.IsValid)
                    crusade.QuestList = new List<SharedQuest>(list);
                else
                    crusade.QuestList = new List<SharedQuest>();

                var cmd = DBCommandBuilder.SelectCrusadeCache(PublicID, false);

                using (var dbRes = db.DoQuery(cmd))
                {
                    foreach (var item in DBObjectConverter.Convert<DBCrusadeCache>(dbRes))
                    {
                        ids.Add(item.ID);
                        reserved.Add(item.QuestID);
                    }
                }
            }

            if (ids.Count > 0)
            {

                if (crusade.Schedule != null)
                {
                    map = new Dictionary<Int64, SharedQuestWrapper>();

                    foreach (var item in reserved)
                    {
                        var obj = map.SafeGetValue(item);

                        if (obj == null)
                        {
                            obj = new SharedQuestWrapper();
                            for (int i = 0; i < crusade.QuestList.Count; ++i)
                            {
                                var quest = crusade.QuestList[i];
                                if (quest.Serial == item)
                                {
                                    obj.Quest = quest;
                                    obj.Index = i;
                                    break;
                                }
                            }

                            if (obj.Quest == null) continue;

                            map.Add(item, obj);
                        }

                        ++obj.PlayCount;
                    }

                    foreach (var item in map)
                    {
                        var obj = item.Value;

                        var count = Math.Min(obj.Quest.PlayCount + obj.PlayCount, Global.Tables.CrusadeOwnerLimit);
                        obj.PlayCount = count - obj.Quest.PlayCount;
                        if (obj.PlayCount > 0)
                        {
                            var point = (int)Math.Floor(Global.Tables.QuestDesign[item.Value.Quest.QuestID].n_POINT * Global.Tables.CrusadeOwner);
                            addPoint += obj.PlayCount * point;

                            // 複製一份出來再把次數加上去
                            var copy = obj.Quest.Clone();
                            copy.PlayCount = obj.Quest.PlayCount + obj.PlayCount;
                            crusade.QuestList[obj.Index] = copy;
                        }

                    }

                    using (var db = AcquireDB())
                    {
                        using (var trans = db.BeginTransaction())
                        {
                            var cmd = DBCommandBuilder.DeleteCrusadeCache(PublicID, ids);

                            db.SimpleQuery(cmd);

                            if (addPoint > 0)
                            {
                                cmd = DBCommandBuilder.UpdateEventPoint(PublicID, crusade.Schedule.Relative.ID, addPoint, Player.CardInfo.Favorite.ID);

                                db.SimpleQuery(cmd);

                                res.AddSyncData(new SyncEventPoint { EventID = crusade.Schedule.EventID, Add = addPoint });
                            }

                            trans.Commit();
                        }
                    }

                    using (var db = AcquireUniqueDB())
                    {
                        using (var trans = db.BeginTransaction())
                        {
                            foreach (var item in map)
                            {
                                if (item.Value.PlayCount <= 0) continue;

                                var cmd = DBCommandBuilder.UpdateCrusadePlayCount(PublicID, item.Key, item.Value.PlayCount);
                                db.SimpleQuery(cmd);
                            }

                            trans.Commit();
                        }
                    }
                }
            }

            crusade.QuestList.AddRange(sharedList.Random(PublicID, CurrentTime));
            crusade.PlayList = Player.QuestInfo.CrusadeCacheList.ToList();

            res.Crusade = crusade.Serialize();

            return ErrorCode.Success;
        }
    }



}
