﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mikan.Server;

namespace Mikan.CSAGA.Server.Tasks
{
    /// <summary>
    /// 初始化SERVER任務
    /// </summary>
    public class InitialTask : KernelTask
    {
        private Service owner;

        public InitialTask(Service owner)
        {
            this.owner = owner;
        }
        public override void Execute(Context context)
        {
            context.Owner = owner;

            var dbPool = DBPool.Create(owner.Host);

            DateTime dbTime;

            var groups = new List<DBGroup>();

            using (var db = dbPool.Acquire(-1))
            {
                // 取得unique db的時間作為基準點
                var cmd = DBCommandBuilder.SelectCurrentTime();

                using (var res = db.DoQuery(cmd)) dbTime = res.Field<DateTime>();

                // 取回所有帳號群組的db連線字串
                cmd = DBCommandBuilder.SelectGroups();

                using (var res = db.DoQuery(cmd)) groups = DBObjectConverter.Convert<DBGroup>(res).ToList();
            }

            var path = owner.Host.Config["data_path"];

            context.DataPath = path;

            //context.AppVersion = System.IO.File.ReadAllText(path + "app_version.txt");

            AddAppVersion(context, Platform.iOS, path + "ios/app_version.txt");
            AddAppVersion(context, Platform.Android, path + "android/app_version.txt");
            AddAppVersion(context, Platform.Unknown, path + "app_version.txt");

            using (var reader = new ConstDataReader())
            {
                reader.Load(path + "CSAGA_CONSTDATA.pak");
                reader.Load(path + "CSAGA_TEXTDATA.pak");
                reader.Load(path + "DUMMY.pak");

                Global.Initialize(owner.Host.Config, reader.Combine(), groups, dbTime);
            }

            AddSignature(context, Platform.iOS, path + "ios/signature.txt");
            AddSignature(context, Platform.Android, path + "android/signature.txt");
            AddSignature(context, Platform.Unknown, path + "signature.txt");

            if (System.IO.File.Exists(path + "maintenance.txt"))
            {
                var maintenance = System.IO.File.ReadAllLines(path + "maintenance.txt");

                if (maintenance.Length > 1)
                {
                    var split = maintenance[0].Split('~');
                    var beginTime = DateTime.Parse(split[0]);
                    var endTime = DateTime.Parse(split[1]);

                    if (Global.CurrentTime >= beginTime && Global.CurrentTime < endTime)
                    {
                        context.IsMaintaining = true;
                        var msg = new StringBuilder();
                        for (int i = 1; i < maintenance.Length; ++i) msg.AppendLine(maintenance[i]);
                        Commands.MaintenanceMessage.DefaultMessage = msg.ToString();
                    }
                }
            }

            if (System.IO.File.Exists(path + "apk_list.txt"))
            {
                var apkList = System.IO.File.ReadAllLines(path + "apk_list.txt");
                foreach (var item in apkList) context.AvailableAPKList.Add(item);
            }

            context.BlackList = PublicID.CreateHashSet();

            if (System.IO.File.Exists(path + "black_list.txt"))
            {
                var blackList = System.IO.File.ReadAllLines(path + "black_list.txt");
                foreach (var item in blackList)
                {
                    var id = PublicID.FromString(item);
                    if(id != null && id.IsValid) context.BlackList.Add(id);
                } 
            }

            context.Owner.Host.Logger.Trace("GroupCount = {0}", groups.Count);


            using (var db = dbPool.AcquireUnique())
            {
                foreach (var cmd in DBCommandBuilder.SetupDatabase()) db.SafeSimpleQuery(cmd);
            }

            using (var db = dbPool.AcquireLog())
            {
                foreach (var cmd in DBCommandBuilder.SetupLogDatabase()) db.SafeSimpleQuery(cmd);
            }

            foreach (var group in groups)
            {
                context.Owner.Host.Logger.Trace("GroupID = {0}", group.ID);

                dbPool.Add(group.ID, group.ConnectionString);

                using (var db = dbPool.Acquire(group.ID))
                {
                    foreach (var cmd in DBCommandBuilder.SetupDatabase(group.ID)) db.SafeSimpleQuery(cmd);
                }
            }

            Calc.Calculator.Initialize();

            context.Setup();

            
            using (var db = dbPool.AcquireUnique())
            {
                var cmd = DBCommandBuilder.SelectBlackGem();
                using (var res = db.DoQuery(cmd))
                {
                    foreach (var item in DBObjectConverter.Convert<DBBlackGem>(res))
                    {
                        var id = PublicID.Create(item.GroupID, item.PlayerID);
                        context.BlackGem.Add(id, item);
                    }
                }
            }

            context.EventScheduleList.TryCommit(Global.CurrentTime);

            context.Owner.AddTask(new CreateDummyTask());

            Scheduler.AddTask(new RefreshEventScheduleSchedule { Owner = owner }, 60 * 30);
            Scheduler.AddTask(new RefreshShareedQuestListSchedule { Owner = owner }, 60 * 5);
            Scheduler.AddTask(new LogQuestPrepareSchedule { Owner = owner }, 60);
            Scheduler.AddTask(new RefreshCooperationMissionSchedule { Owner = owner, Provider = context.CooperateMissionProvider }, 60 * 3);
            Scheduler.AddTask(new ClearOverdueCrusadeSchedule { Owner = owner }, 60 * 6);

            Scheduler.AddTask(new SyncLogSchedule(), 60 * 60);
        }

        private static void AddSignature(Context context, Platform platform, string file)
        {
            if (System.IO.File.Exists(file))
            {
                var signature = System.IO.File.ReadAllText(file);
                context.Signature.Add(platform, signature);
                context.Owner.Host.Logger.Trace("signature = {0}, platform = {1}", signature, platform);
            }
        }
        private static void AddAppVersion(Context context, Platform platform, string file)
        {
            if (System.IO.File.Exists(file))
            {
                var ver = System.IO.File.ReadAllText(file);
                context.AppVersion.Add(platform, ver);
                context.Owner.Host.Logger.Trace("app_ver = {0}, platform = {1}", ver, platform);
            }
        }
    }

    public class FinalizeTask : KernelTask
    {
        public override void Execute(Context context)
        {
            context.Clear();
        }
    }

    public class CreateDummyTask : ITask
    {
        public void Execute()
        {
            KernelService.Logger.Debug("create dummy begin");
            try
            {
                foreach (var item in Global.Tables.Dummy.Select())
                {
                    var id = CreateAccount(string.Format("dummy_{0:0000}", item.n_ID));

                    if (id == null) continue;

                    CreatePlayer(id, item.n_ID, item.s_PLAYERNAME, item.n_LEADER);
                }
            }
            catch (Exception e)
            {
                KernelService.Logger.Debug(e.ToString());
            }
        }

        protected PublicID CreateAccount(string uuid)
        {
            DBAccount account = null;
            DBPlayer player = null;

            #region 檢查帳號是否已存在

            using (var db = AcquireUniqueDB())
            {

                var cmd = DBCommandBuilder.SelectAccount(uuid);

                using (var res = db.DoQuery(cmd))
                {
                    if (!res.IsEmpty) account = DBObjectConverter.ConvertCurrent<DBAccount>(res);
                }
            }

            #endregion

            if (account == null)
            {
                using (var db = AcquireUniqueDB())
                {
                    var key = MathUtils.Random.Next();

                    while (key <= 0) key = MathUtils.Random.Next();

                    var cmd = DBCommandBuilder.CreateAccount(uuid, key);

                    using (var res = db.DoQuery(cmd))
                    {
                        account = DBObjectConverter.ConvertCurrent<DBAccount>(res);
                    }
                }
            }

            if (account.SubID != 0)
            {
                return null;
            }

            var group = Global.Groups[(int)(account.ID % Global.Groups.Count)];

            using (var db = AcquireDB(group))
            {
                var cmd = DBCommandBuilder.CreatePlayer(account.ID, group);

                using (var res = db.SafeDoQuery(cmd))
                {
                    if (res.IsSucc) player = DBObjectConverter.ConvertCurrent<DBPlayer>(res);
                }

                if (player == null)
                {
                    cmd = DBCommandBuilder.SelectPlayer(account.ID, group);

                    using (var res = db.DoQuery(cmd))
                    {
                        player = DBObjectConverter.ConvertCurrent<DBPlayer>(res);

                        if (player == null)
                        {
                            return null;
                        }
                    }
                }
            }

            using (var db = AcquireUniqueDB())
            {
                var cmd = DBCommandBuilder.SettleAccount(account.ID, group, player.PlayerID);

                using (var res = db.DoQuery(cmd))
                {
                    var publicId = PublicID.Create(group, player.PlayerID);

                    return publicId;
                }
            }
        }

        protected ErrorCode CreatePlayer(PublicID id, int key, string name, int favorite)
        {
            KernelService.Logger.Debug("create dummy player, id={0}, key={1}, name={2}, favorite={3}", id, key, name, favorite);
            using (var db = AcquireDB(id.Group))
            {
                using (var trans = db.BeginTransaction())
                {
                    var cmd = DBCommandBuilder.AlterPlayerNameFristTime(id, name, Global.CurrentTime);

                    db.SimpleQuery(cmd);

                    const int DUMMY = 2;
                    cmd = DBCommandBuilder.UpdatePlayerComplex(id, Complex.Bit(DUMMY).Value);
                    db.SimpleQuery(cmd);

                    #region 塞初始卡片

                    foreach (var item in Global.GetDefaultCardList(0))
                    {
                        cmd = DBCommandBuilder.InsertCard(id, item);
                        db.SimpleQuery(cmd);
                        cmd = DBCommandBuilder.InsertGallery(id, item, 1);
                        db.SimpleQuery(cmd);
                    }

                    #endregion

                    trans.Commit();
                }
            }

            using (var db = AcquireUniqueDB())
            {
                var cmd = DBCommandBuilder.InsertDummy(id, key, favorite);
                db.SafeSimpleQuery(cmd);
            }

            return ErrorCode.Success;
        }

        private IDBConnection AcquireUniqueDB()
        {
            return Global.DBPool.AcquireUnique();
        }

        private IDBConnection AcquireDB(int group)
        {
            return Global.DBPool.Acquire(group);
        }


        public void Dispose()
        {
            
        }
    }
}
