﻿using Mikan.CSAGA.Commands.PVP;
using Mikan.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mikan.CSAGA.Server.Tasks
{

    [ActiveTask(CommandType = typeof(InfoRequest))]
    public class PVPInfoTask : PlayerTask<InfoRequest, InfoResponse>
    {
        private EventScheduleList eventSchedule;
        private EventScheduleList eventScheduleTmp;
        private bool replaceSchedule;
        private PVPInfo pvpInfo;
        private PVPContextList list;
        protected override void OnPrepare(Context context)
        {
            base.OnPrepare(context);
            eventSchedule = context.EventScheduleList;
            eventScheduleTmp = context.EventScheduleListTmp;
            pvpInfo = Player.PVPInfo;
            list = context.PVPList;
        }

        protected override void OnCommit(Context context)
        {
            if (replaceSchedule) context.EventScheduleListTmp = eventSchedule;
            Player.PVPInfo = pvpInfo;
            base.OnCommit(context);
        }
        protected override ErrorCode SafeProcessCommand(InfoResponse res)
        {
            var info = new Data.PVPInfo();

            pvpInfo = list.Convert(PublicID, pvpInfo);

            if (pvpInfo != null)
            {
                info.BPConsumed = pvpInfo.BPConsumed;
                info.BPUpdateTime = pvpInfo.BPUpdateTime;
                info.Skills = new List<int>(pvpInfo.Skills);

                info.Origins = new List<Internal.PVPWaveDiff>(5);
                foreach (var wave in pvpInfo.Waves.List)
                {
                    var obj = new Internal.PVPWaveDiff();
                    obj.Serial = wave.Card.Serial;
                    obj.Skills = wave.Skills;
                    obj.Selections = wave.Selections;
                    obj.Prologs = wave.Prologs;
                    info.Origins.Add(obj);
                }

                if (Command.Initial)
                {
                    using (var db = AcquireDB())
                    {
                        var cmd = DBCommandBuilder.UpdatePVPContextCardID(PublicID, Player.CardInfo.Favorite.ID, CurrentTime);
                        db.SimpleQuery(cmd);
                    }
                }
                else if (eventSchedule.PVPNext != null && eventSchedule.PVPNext.BeginTime <= CurrentTime)
                {
                    if (eventScheduleTmp != null)
                        eventSchedule = eventScheduleTmp;
                    else
                    {
                        eventSchedule = EventScheduleList.Generate(CurrentTime);
                        replaceSchedule = true;
                    }
                }

                if (eventSchedule.PVP != null)
                {
                    info.EventID = eventSchedule.PVP.EventID;
                    if (pvpInfo.EventID == eventSchedule.PVP.Relative.ID)
                        info.Point = pvpInfo.Point;
                }
            }

            res.PVPInfo = info.Serialize();

            return ErrorCode.Success;
        }
    }

    [ActiveTask(CommandType = typeof(UpdateRequest))]
    public class PVPUpdateTask : PlayerTask<UpdateRequest, UpdateResponse>
    {
        private PVPInfo pvpInfo;
        private PVPWaves pvpWaves;

        private PVPContextList list;

        protected override void OnPrepare(Context context)
        {
            base.OnPrepare(context);

            list = context.PVPList;
            pvpInfo = Player.PVPInfo;
        }

        protected override void OnCommit(Context context)
        {
            pvpInfo.Waves = pvpWaves;
            Player.PVPInfo = pvpInfo;
            base.OnCommit(context);
        }
        protected override ErrorCode SafeProcessCommand(UpdateResponse res)
        {
            
            pvpWaves = new PVPWaves();
            pvpWaves.List = new List<PVPWave>(5);

            if (pvpInfo == null)
            {
                foreach (var diff in Command.Diffs)
                {
                    var wave = new PVPWave();
                    wave.ChangeCard(Player, diff.Serial);
                    diff.Skills = wave.Skills = new List<int>(Tables.DefaultPVPSkills);
                    diff.Selections = wave.Selections = new List<int>(new int[] { 0, 2, 4 });
                    pvpWaves.List.Add(wave);
                }

                using (var db = AcquireDB())
                {
                    var cmd = DBCommandBuilder.InsertPVPContext(PublicID, Player.CardInfo.Favorite.ID, pvpWaves.Serialize(), CurrentTime);
                    db.SimpleQuery(cmd);
                }

                var pvpContext = new PVPContext { Waves = pvpWaves };
                pvpInfo = new PVPInfo();
                pvpInfo.Setup(pvpContext);

                list.Insert(PublicID, pvpContext);
            }
            else
            {
                for (int i = 0; i < pvpInfo.Waves.List.Count; ++i)
                {
                    var obj = pvpInfo.Waves.List[i].Clone();

                    var diff = Command.Diffs[i];
                    obj.ChangeCard(Player, diff.Serial);
                    obj.Selections = new List<int>(diff.Selections);
                    obj.Prologs = diff.Prologs;

                    for (int j = 0; j < 3; ++j)
                    {
                        var selection = obj.Selections[j];
                        var min = j * 2;
                        var max = (j + 1) * 2;
                        obj.Selections[j] = MathUtils.Restrict(selection, min, max);
                    }
                    pvpWaves.List.Add(obj);
                }

                using (var db = AcquireDB())
                {
                    var cmd = DBCommandBuilder.UpdatePVPContext(PublicID, pvpWaves.Serialize(), CurrentTime);
                    db.SimpleQuery(cmd);
                }

                list.Update(PublicID, pvpWaves);
            }

            res.Diffs = Command.Diffs;

            return ErrorCode.Success;
        }
    }

    [ActiveTask(CommandType = typeof(RerollSkillRequest))]
    public class PVPRerollSkillTask : PlayerTask<RerollSkillRequest, RerollSkillResponse>
    {
        private PVPContextList pvp;
        private PVPInfo info;

        protected override void OnPrepare(Context context)
        {
            base.OnPrepare(context);
            pvp = context.PVPList;
            info = Player.PVPInfo;
        }

        protected override void OnCommit(Context context)
        {
            Player.PVPInfo = info;
            base.OnCommit(context);
        }

        protected override ErrorCode SafeProcessCommand(RerollSkillResponse res)
        {
            info = pvp.Convert(PublicID, info);

            var skills = Calc.Skill.RandPVPSkill();
            var ids = new List<int>(skills.Count);
            foreach (var item in skills)
            {
                var lv = info.SkillLevels.SafeGetValue(item.Group);
                ids.Add(item.GetID(lv));
            }

            var waves = info.Waves.Clone();

            var wave = waves.List[Command.Index].Clone();
            wave.Skills = ids;
            waves.List[Command.Index] = wave;

            var payment = new PaymentObj(Player, ArticleID.RerollSkill, DropType.Item, 1, Tables.PVPSkillItemID);
            if (payment.Origin < 1) return ErrorCode.ItemNotEnough;

            AddAssistObj(payment);

            using (var db = AcquireDB())
            {
                using (var trans = db.BeginTransaction())
                {
                    var cmd = DBCommandBuilder.UpdatePVPContext(PublicID, waves.Serialize(), CurrentTime);
                    db.SimpleQuery(cmd);
                    ApplyAssists(db);
                    trans.Commit();
                }
                   
            }

            pvp.Update(PublicID, waves);

            res.Index = Command.Index;
            res.Skills = ids;

            return ErrorCode.Success;
        }
    }

    [ActiveTask(CommandType = typeof(QueryRequest))]
    public class PVPQueryTask : PlayerTask<QueryRequest, QueryResponse>
    {
        private PVPContextList list;
        private PVPInfo info;
        private DateTime queryTime;
        protected override void OnPrepare(Context context)
        {
            base.OnPrepare(context);
            list = context.PVPList;
            info = Player.PVPInfo;
        }

        protected override void OnCommit(Context context)
        {
            Player.PVPQueryTime = queryTime;

            base.OnCommit(context);
        }

        protected override ErrorCode SafeProcessCommand(QueryResponse res)
        {
            if (Player.PVPQueryTime.AddSeconds(5) > CurrentTime) return ErrorCode.PVPQueryCD;

            queryTime = CurrentTime;

            var point = info != null ? info.Point : list.GetPoint(PublicID);

            var players = list.Query(PublicID, point);

            foreach (var item in players)
            {
                item.Ticket = PVPTicket.Create(Token, item.ID).Serialize();

                using (var db = AcquireDB(item.ID))
                {
                    var cmd = DBCommandBuilder.SelectPlayer(item.ID);
                    using (var dbRes = db.DoQuery(cmd))
                    {
                        var player = DBObjectConverter.ConvertCurrent<DBPlayer>(dbRes);
                        if (player != null) item.Name = player.Name;
                    }
                }
            }

            res.ListObj = ListObj.Serialize(players);

            return ErrorCode.Success;
        }
    }
}
