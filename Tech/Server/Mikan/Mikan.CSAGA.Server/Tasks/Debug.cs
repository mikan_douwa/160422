﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mikan.Server;
using Mikan.CSAGA.Commands;

namespace Mikan.CSAGA.Server.Tasks
{
    [ActiveTask(CommandType = typeof(ReportRequest))]
    public class ReportTask : PlayerTask<ReportRequest, ReportResponse>
    {
        protected override ErrorCode SafeProcessCommand(ReportResponse res)
        {
            KernelService.Logger.Debug("[REPORT] PlayerID = {0}, Type = {1}, Message = {2}", Player.ID.ToString(), Command.Type, Command.Message);
            res.Type = Command.Type;
            return ErrorCode.Success;
        }
    }

    [ActiveTask(CommandType = typeof(DebugRequest))]
    public class DebugTask : PlayerTask<DebugRequest>
    {
        private string[] split;

        private DebugImp debug;

        protected override void OnCommit(Context context)
        {
            debug.Commit(Player, split, CurrentTime);
            base.OnCommit(context);
        }

        protected override ICommand SafeProcessCommand()
        {
            if (!Player.UseSuperPrivilege && !Global.IsDevServer) return new RegularError { Status = ErrorCode.InvalidOperation };

            var res = new DebugResponse();

            split = Command.GetString(FieldID.DebugCode).Split(' ');

            switch (split[0])
            {
                case "player": debug = new DebugPlayer(); break;
                case "item": debug = new DebugItem(); break;
                case "addcard": debug = new DebugAddCard(); break;
                case "addcard_range": debug = new DebugAddCardRange(); break;
                case "card": debug = new DebugCard(); break;
                case "reset_talk": debug = new DebugResetTalk(); break;
                case "add_equip": debug = new DebugAddEquip(); break;
                case "quest_state": debug = new DebugQuestState(); break;
                //case "remove_card": debug = new DebugRemoveCard(); break;
                case "add_gift": debug = new DebugAddGift(); break;
                case "reset_daily": debug = new DebugResetDaily(); break;
                case "iap_purchase": debug = new DebugIAPPurchase(); break;
                case "reset_rescue": debug = new DebugResetRescue(); break;
                case "add_event_point": debug = new DebugAddEventPoint(); break;
                case "set_event_point": debug = new DebugSetEventPoint(); break;
                case "reset_trophy": debug = new DebugResetTrophy(); break;
                case "random_faith": debug = new DebugRandomFaith(); break;
                case "cancel_faith": debug = new DebugCancelFaith(); break;
                case "pvp_init": debug = new DebugPVPInit(); break;
                case "pvp_delete": debug = new DebugPVPDelete(); break;
                default:
                    break;
            }

            if (debug == null)
                res.Status = ErrorCode.NotImplemented;
            else
            {
                using (var db = AcquireDB()) debug.Process(Player, split, CurrentTime, PublicID, db);
                res.Add(FieldID.DebugCode, Command.GetString(FieldID.DebugCode));
            }

            return res;
        }


    }

    abstract class DebugImp
    {
        abstract internal void Process(Player player, string[] split, DateTime now, PublicID id, IDBConnection db);

        abstract internal void Commit(Player player, string[] split, DateTime now);
    }

    class DebugPlayer : DebugImp
    {

        internal override void Process(Player player, string[] split, DateTime now, PublicID id, IDBConnection db)
        {
            var field = split[1];

            IDBCommand cmd;

            if (field == "gem")
            {
                var gem = int.Parse(split[2]);

                if (gem > player.PurchaseInfo.Gem)
                    cmd = DBCommandBuilder.InsertGem(id, RewardSource.Debug, gem - player.PurchaseInfo.Gem, now);
                else if (gem < player.PurchaseInfo.Gem)
                    cmd = DBCommandBuilder.InsertPurchase(id, ArticleID.Debug, player.PurchaseInfo.Gem - gem, 0, now);
                else
                    return;
            }
            else
                cmd = DBCommandBuilder.DebugUpdatePlayer(id, field, split[2], now);

            db.SimpleQuery(cmd);
        }

        internal override void Commit(Player player, string[] split, DateTime now)
        {
            if (split[1] != "gem")
                player.PlayerInfo.DebugEdit(split[1], split[2]);
            else
            {
                var gem = int.Parse(split[2]);
                if (gem > player.PurchaseInfo.Gem)
                    player.PurchaseInfo.AddGem(gem - player.PurchaseInfo.Gem);
                else if (gem < player.PurchaseInfo.Gem)
                    player.PurchaseInfo.AddArticle(new DBPurchase { ArticleID = (byte)ArticleID.Debug, Cost = player.PurchaseInfo.Gem - gem, Timestamp = now });
            }
        }
    }

    class DebugItem : DebugImp
    {
        internal override void Process(Player player, string[] split, DateTime now, PublicID id, IDBConnection db)
        {
            var itemId = int.Parse(split[1]);
            var count = int.Parse(split[2]);

            var diff = count;

            foreach (var item in player.ItemInfo.List)
            {
                if (item.ID == itemId)
                {
                    diff = count - item.Count;
                    break;
                }
            }

            var cmd = DBCommandBuilder.IncreaseItemCount(id, itemId, diff);

            db.SimpleQuery(cmd);
        }

        internal override void Commit(Player player, string[] split, DateTime now)
        {
            var itemId = int.Parse(split[1]);
            var count = int.Parse(split[2]);

            foreach (var item in player.ItemInfo.List)
            {
                if (item.ID == itemId)
                {
                    item.Count = count;
                    return;
                }
            }

            player.ItemInfo.List.Add(new Item { ID = itemId, Count = count });
        }
    }

    class DebugCard : DebugImp
    {
        internal override void Process(Player player, string[] split, DateTime now, PublicID id, IDBConnection db)
        {
            var card = player.CardInfo.List[int.Parse(split[1])];

            if (split[2] == "rare")
            {
                var complex = new Complex { Value = card.Complex.Value };
                complex.Set(ComplexIndex.CardAwaken, ComplexIndex.CardAwakenLen, int.Parse(split[3]));
                var cmd = DBCommandBuilder.DebugUpdateCardComplex(id, card.Serial.Value, complex.Value);
                db.SimpleQuery(cmd);
            }
            else
            {
                var cmd = DBCommandBuilder.DebugUpdateCard(id, card.Serial.Value, split[2], split[3]);
                db.SimpleQuery(cmd);
            }
        }

        internal override void Commit(Player player, string[] split, DateTime now)
        {
            player.CardInfo.DebugEdit(split[1], split[2], split[3]);
        }
    }

    class DebugAddCard : DebugImp
    {
        private List<Card> cards = new List<Card>();
        internal override void Process(Player player, string[] split, DateTime now, PublicID id, IDBConnection db)
        {
            var cardId = int.Parse(split[1]);
            var count = split.Length > 2 ? int.Parse(split[2]) : 1;

            for (int i = 0; i < count; ++i)
            {
                var card = new Card();
                card.ID = cardId;
                var cmd = DBCommandBuilder.InsertCard(id, card.ID);
                using (var res = db.DoQuery(cmd)) card.Serial = Serial.Create(res.Field<Int64>());
            }
        }

        internal override void Commit(Player player, string[] split, DateTime now)
        {
            player.CardInfo.AddRange(cards);
        }
    }

    class DebugAddCardRange : DebugImp
    {
        private List<Card> cards = new List<Card>();
        internal override void Process(Player player, string[] split, DateTime now, PublicID id, IDBConnection db)
        {
            var begin = int.Parse(split[1]);
            var end = int.Parse(split[2]);

            var count = split.Length > 4 ? int.Parse(split[3]) : 1;

            for (int i = begin; i <= end; ++i)
            {
                var data = ConstData.Tables.Instance.Card[i];
                if (data == null) continue;
                for (int j = 0; j < count; ++j)
                {
                    var card = new Card();
                    card.ID = i;
                    var cmd = DBCommandBuilder.InsertCard(id, card.ID);
                    using (var res = db.DoQuery(cmd)) card.Serial = Serial.Create(res.Field<Int64>());
                }
            }
        }

        internal override void Commit(Player player, string[] split, DateTime now)
        {
            player.CardInfo.List.AddRange(cards);
        }
    }

    class DebugResetTalk : DebugImp
    {
        internal override void Process(Player player, string[] split, DateTime now, PublicID id, IDBConnection db)
        {
            var cmd = DBCommandBuilder.UpdateTPConsumed(id, 0, now);
            db.SimpleQuery(cmd);
        }

        internal override void Commit(Player player, string[] split, DateTime now)
        {
            player.PlayerInfo.UpdateTPConsumed(0, now);
        }
    }

    class DebugAddEquip : DebugImp
    {
        private List<Equipment> list = new List<Equipment>();
        internal override void Process(Player player, string[] split, DateTime now, PublicID id, IDBConnection db)
        {
            var itemId = int.Parse(split[1]);
            var count = split.Length > 2 ? int.Parse(split[2]) : 1;

            for (int i = 0; i < count; ++i)
            {
                var obj = Calc.Equipment.Generate<Equipment>(itemId);

                var cmd = DBCommandBuilder.InsertEquipment(id, itemId, obj.Hp, obj.Atk, obj.Rev, obj.Chg, obj[PropertyID.EffectID], obj[PropertyID.Counter], obj[PropertyID.CounterType]);

                obj.Serial = db.DoQuery<Int64>(cmd);
            }
        }

        internal override void Commit(Player player, string[] split, DateTime now)
        {
            foreach (var item in list) player.EquipmentInfo.Add(item);
        }
    }

    class DebugQuestState : DebugImp
    {
        internal override void Process(Player player, string[] split, DateTime now, PublicID id, IDBConnection db)
        {
            if (split[1] == "all")
            {
                var state = int.Parse(split[2]);

                foreach (var item in Global.Tables.QuestDesign.Select(o => { return o.n_QUEST_TYPE == QuestType.Regular; }))
                {
                    var cmd = DBCommandBuilder.DebugUpdateQuest(id, item.n_ID, state != 0, now);

                    db.SimpleQuery(cmd);
                }
            } 
            else if (split[1] == "extra")
            {
                var state = int.Parse(split[2]);

                foreach (var item in Global.Tables.QuestDesign.Select(o => { return o.n_QUEST_TYPE == QuestType.Extra; }))
                {
                    var cmd = DBCommandBuilder.DebugUpdateQuest(id, item.n_ID, state != 0, now);

                    db.SimpleQuery(cmd);
                }
            }
            else
            {
                var questId = int.Parse(split[1]);
                var state = int.Parse(split[2]);

                var cmd = DBCommandBuilder.DebugUpdateQuest(id, questId, state != 0, now);

                db.SimpleQuery(cmd);
            }
        }

        internal override void Commit(Player player, string[] split, DateTime now)
        {
            var questId = int.Parse(split[1]);
            var state = int.Parse(split[2]);

            
            if (state != 0)
            {
                player.QuestInfo.UpdateQuest(questId, true, state, now);
            }
            else
            {
                player.QuestInfo.Remove(questId);
            }
            
        }
    }

    class DebugAddGift : DebugImp
    {
        internal override void Process(Player player, string[] split, DateTime now, PublicID id, IDBConnection db)
        {
            var type = (DropType)int.Parse(split[1]);
            var value1 = int.Parse(split[2]);
            var value2 = split.Length > 3 ? int.Parse(split[3]) : 0;
            var value3 = split.Length > 4 ? int.Parse(split[4]) : 0;
            var value4 = split.Length > 5 ? int.Parse(split[5]) : 0;

            if (type == DropType.Item)
            {
                var data = Global.Tables.Item[value1];
                if (data != null && data.n_TYPE == ItemType.Equipment)
                {
                    var itemId = value1;
                    value1 = value2;
                    value2 = itemId;
                }
            }

            if (split.Length > 4)
            {
                var cmd = DBCommandBuilder.InsertGift(id, RewardSource.Debug, "系統獎勵測試", type, value1, value2, Global.CurrentTime, DateTime.Parse(split[4]));
                db.SimpleQuery(cmd);
            }
            else
            {
                var cmd = DBCommandBuilder.InsertGift(id, RewardSource.Debug, "系統獎勵測試", type, value1, value2);
                db.SimpleQuery(cmd);
            }
        }

        internal override void Commit(Player player, string[] split, DateTime now)
        {

        }
    }

    class DebugResetDaily : DebugImp
    {
        internal override void Process(Player player, string[] split, DateTime now, PublicID id, IDBConnection db)
        {
            var cmd = DBCommandBuilder.DebugResetDaily(id);

            db.SimpleQuery(cmd);
        }

        internal override void Commit(Player player, string[] split, DateTime now)
        {
            player.DailyInfo = new DailyInfo();
        }
    }

    class DebugIAPPurchase : DebugImp
    {
        private int count;
        internal override void Process(Player player, string[] split, DateTime now, PublicID id, IDBConnection db)
        {
            count = int.Parse(split[1]);
            var cmd = DBCommandBuilder.InsertGem(id, RewardSource.IAP, count, now);

            db.SimpleQuery(cmd);
        }

        internal override void Commit(Player player, string[] split, DateTime now)
        {
            player.PurchaseInfo.AddIAPGem(count);
        }
    }

    class DebugResetRescue : DebugImp
    {
        internal override void Process(Player player, string[] split, DateTime now, PublicID id, IDBConnection db)
        {
            var cmd = DBCommandBuilder.UpdateRescue(id, Global.Tables.HelpMax * Global.Tables.HelpScale, now);

            db.SimpleQuery(cmd);
        }

        internal override void Commit(Player player, string[] split, DateTime now)
        {
            player.QuestInfo.RescueCount = Global.Tables.HelpMax * Global.Tables.HelpScale;
            player.QuestInfo.RescueUpdateTime = now;
        }
    }

    class DebugAddEventPoint : DebugImp
    {
        internal override void Process(Player player, string[] split, DateTime now, PublicID id, IDBConnection _db)
        {
            var questId = int.Parse(split[1]);
            var count = int.Parse(split[2]);
            var min = int.Parse(split[3]);
            var max = int.Parse(split[4]);

            var current = player.Context.EventScheduleList.GetCurrent(questId);

            while (count > 0)
            {
                var groupIndex = MathUtils.Random.Next(Global.Groups.Count);
                var group = Global.Groups[groupIndex];

                if (group == id.Group) continue;

                var playerCount = 0;
                using (var db = Global.DBPool.Acquire(group))
                {
                    var cmd = DBCommandBuilder.DebugSelectPlayerCount(group);
                    playerCount = db.DoQuery<int>(cmd);

                    var _count = MathUtils.Random.Next(1, 6);

                    for (int i = 0; i < _count; ++i)
                    {
                        cmd = DBCommandBuilder.DebugSelectRandomPlayer(group, playerCount);

                        var playerId = default(PublicID);
                        using (var res = db.DoQuery(cmd))
                        {
                            var obj = DBObjectConverter.ConvertCurrent<DBPlayer>(res);

                            if (obj == null) continue;

                            playerId = PublicID.Create(group, obj.PlayerID);
                        }

                        var point = MathUtils.Random.Next(min, max + 1);

                        var cardId = MathUtils.Random.Next(2001, 2034 + 1);

                        cmd = DBCommandBuilder.UpdateEventPoint(playerId, current.Relative.ID, point, cardId);

                        db.SimpleQuery(cmd);

                        if (--count <= 0) break;
                    }
                }

                
            }

        }

        internal override void Commit(Player player, string[] split, DateTime now) { }
    }

    class DebugSetEventPoint : DebugImp
    {
        internal override void Process(Player player, string[] split, DateTime now, PublicID id, IDBConnection _db)
        {
            var questId = int.Parse(split[1]);
            var count = int.Parse(split[2]);
            var min = int.Parse(split[3]);
            var max = int.Parse(split[4]);

            var current = player.Context.EventScheduleList.GetCurrent(questId);

            while (count > 0)
            {
                var groupIndex = MathUtils.Random.Next(Global.Groups.Count);
                var group = Global.Groups[groupIndex];

                if (group == id.Group) continue;

                var playerCount = 0;
                using (var db = Global.DBPool.Acquire(group))
                {
                    var cmd = DBCommandBuilder.DebugSelectPlayerCount(group);
                    playerCount = db.DoQuery<int>(cmd);

                    var _count = MathUtils.Random.Next(1, 6);

                    for (int i = 0; i < _count; ++i)
                    {
                        cmd = DBCommandBuilder.DebugSelectRandomPlayer(group, playerCount);

                        var playerId = default(PublicID);
                        using (var res = db.DoQuery(cmd))
                        {
                            var obj = DBObjectConverter.ConvertCurrent<DBPlayer>(res);

                            if (obj == null) continue;

                            playerId = PublicID.Create(group, obj.PlayerID);
                        }

                        var point = MathUtils.Random.Next(min, max + 1);

                        var cardId = MathUtils.Random.Next(2001, 2034 + 1);

                        cmd = DBCommandBuilder.UpdateEventPoint(playerId, current.Relative.ID, point, cardId);

                        db.SimpleQuery(cmd);

                        if (--count <= 0) break;
                    }
                }


            }

        }

        internal override void Commit(Player player, string[] split, DateTime now) { }
    }

    class DebugResetTrophy : DebugImp
    {
        internal override void Process(Player player, string[] split, DateTime now, PublicID id, IDBConnection db)
        {
            var trophyId = int.Parse(split[1]);
            var cmd = DBCommandBuilder.DebugDeleteTrophy(id, trophyId);

            db.SimpleQuery(cmd);
        }

        internal override void Commit(Player player, string[] split, DateTime now) { }
    }

    class DebugRandomFaith : DebugImp
    {
        internal override void Process(Player player, string[] split, DateTime now, PublicID id, IDBConnection db)
        {
            var minutes = MathUtils.Random.Next(1, Global.Tables.GetVariable(VariableID.FAITH_MAX));

            var claimTime = now.AddMinutes(-minutes);

            var cmd = player.FaithInfo.Origin != null ? DBCommandBuilder.UpdateFaithClaim(id, claimTime) : DBCommandBuilder.InsertFaithClaim(id, claimTime);

            db.SimpleQuery(cmd);
        }

        internal override void Commit(Player player, string[] split, DateTime now) { }
    }

    class DebugCancelFaith : DebugImp
    {
        internal override void Process(Player player, string[] split, DateTime now, PublicID id, IDBConnection db)
        {
            var cmd = DBCommandBuilder.DebugDeleteFaith(id);

            db.SimpleQuery(cmd);
        }

        internal override void Commit(Player player, string[] split, DateTime now) { }
    }

    class DebugPVPInit : DebugImp
    {
        private HashSet<PublicID> ids;
        internal override void Process(Player player, string[] split, DateTime now, PublicID id, IDBConnection db)
        {
            ids = PublicID.CreateHashSet();

            using (var uniqueDb = Global.DBPool.AcquireUnique())
            {
                var cmd = DBCommandBuilder.DebugSelectPVPContext();
                using (var res = uniqueDb.DoQuery(cmd))
                {
                    foreach (var item in DBObjectConverter.Convert<DBPVPIdlePlayer>(res))
                    {
                        ids.Add(PublicID.Create(item.Group, item.PlayerID));
                    }
                }
            }

            var dayBefore = int.Parse(split[1]);
            var from = now.AddDays(-dayBefore);
            foreach (var group in Global.Groups)
            {
                if (group == id.Group)
                    QueryIdlePlayers(group, db, from);
                else
                {
                    using (var otherDB = Global.DBPool.Acquire(group)) QueryIdlePlayers(group, otherDB, from);
                }
            }
        }

        private void QueryIdlePlayers(int group, IDBConnection db, DateTime from)
        {
            var cmd = DBCommandBuilder.DebugSelectIdlePlayer(group, from);

            var list = default(List<DBPlayer>);

            using (var res = db.DoQuery(cmd))
            {
                list = DBObjectConverter.Convert<DBPlayer>(res).ToList();
            }

            foreach (var item in list)
            {
                var id = PublicID.Create(group, item.PlayerID);

                if (ids.Contains(id)) continue;

                cmd = DBCommandBuilder.SelectCardList(id);

                var cards = default(List<DBCard>);

                using (var res = db.DoQuery(cmd))
                {
                    if (res.IsEmpty) continue;

                    cards = DBObjectConverter.Convert<DBCard>(res).ToList();
                }

                if (cards.Count < 20) continue;

                cards.Sort((lhs, rhs) => rhs.Exp.CompareTo(lhs.Exp));

                var pvpWaves = new PVPWaves { List = new List<PVPWave>() };

                for (int i = 4; i >= 0; --i)
                {
                    var obj = cards[i];

                    var wave = new PVPWave();

                    var card = new Card
                    {
                        ID = obj.CardID,
                        Serial = Serial.Create(obj.ID),
                        Complex = new Complex { Value = obj.Complex },
                        Exp = item.Exp,
                        Kizuna = obj.Kizuna
                    };

                    wave.Card = card;
                    wave.Equipments = new List<Equipment>();
                    wave.Skills = new List<int>(Global.Tables.DefaultPVPSkills);
                    wave.Selections = new List<int>(new int[] { 0, 2, 4 });
                    pvpWaves.List.Add(wave);
                }

                var cardId = pvpWaves.List[4].Card.ID;

                using (var uniqueDb = Global.DBPool.AcquireUnique())
                {
                    cmd = DBCommandBuilder.DebugInsertPVPContxt(id, cardId, pvpWaves.Serialize());
                    uniqueDb.SimpleQuery(cmd);
                }

            }
        }

        internal override void Commit(Player player, string[] split, DateTime now) { }
    }

    class DebugPVPDelete : DebugImp
    {
        internal override void Process(Player player, string[] split, DateTime now, PublicID id, IDBConnection db)
        {
            using (var uniqueDb = Global.DBPool.AcquireUnique())
            {
                var cmd = DBCommandBuilder.DebugDeletePVPContext();
                uniqueDb.SimpleQuery(cmd);
            }
        }

        internal override void Commit(Player player, string[] split, DateTime now) { }
    }

    /*
    class DebugRemoveCard : DebugImp
    {
        private List<Int64> list;
        internal override void Process(Player player, string[] split, DateTime now, PublicID id, IDBConnection db)
        {
            list = new List<Int64>();

            foreach (var item in player.CardInfo.List)
            {
                bool isInTeam = false;
                foreach (var team in player.TeamInfo.Select())
                {
                    if (team.Members.Contains(item.serial))
                    {
                        isInTeam = true;
                        break;
                    }
                }

                if (!isInTeam) list.Add(item.serial);
            }

            var cmd = DBCommandBuilder.DeleteCard(id, list);
            db.SimpleQuery(cmd);
        }

        internal override void Commit(Player player, string[] split, DateTime now)
        {
            foreach (var item in list)
            {
                var card = player.CardInfo[item];
                player.CardInfo.List.Remove(card);
            }
        }
    }

    
    */
}