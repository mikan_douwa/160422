﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mikan.Server;
using Mikan.CSAGA.Commands.Quest;

namespace Mikan.CSAGA.Server.Tasks
{
    #region Obj

    public class BeginQuestObj
    {
        public EventScheduleList EventScheduleList;
        public QuestInstance Quest;
        public SharedQuest Crusade;

        public Serial CardSerial = Serial.Empty;
        public int InteractiveID;

        public bool TriggerRescue;

        public DBQuestCache Cache;
        public DBCardEventCache EventCache;
        //public int RescueCount;
        //public DateTime RescueUpdateTime;
        //public PublicID Fellow;
        //public int FP;

        public QuestCacheData Data;

        public PVPBattleObj PVPBattle;
    }

    public class PVPBattleObj
    {
        public PVPTicket Ticket;
        public int Win;
        public int Lose;

        public int OriginBPConsumed;
        public DateTime OriginBPUpdateTime;
        public int BPConsumed;
        public DateTime BPUpdateTime;
    }

    #endregion

    #region Task

    [ActiveTask(CommandType = typeof(BeginRequest))]
    public class QuestBeginTask : PlayerTask<BeginRequest, BeginResponse>
    {
        protected override Priority UpdatePriority { get { return Priority.Delay; } }

        private BeginQuestObj obj = new BeginQuestObj();

        private int questId;

        private QuestScheduleList questSchedule;

        private SharedQuestList sharedList;
        private List<SharedQuest> list;
        private PVPContextList pvpList;
        private PVPInfo pvpInfo;

        protected override void OnPrepare(Context context)
        {
            base.OnPrepare(context);

            questSchedule = context.QuestScheduleList;
            obj.EventScheduleList = context.EventScheduleList;
            sharedList = context.SharedQuestList;
            list = Player.QuestInfo.SharedList;
            if (!string.IsNullOrEmpty(Command.Ticket))
            {
                pvpList = context.PVPList;
                pvpInfo = Player.PVPInfo;
            }

        }
        protected override void OnCommit(Context context)
        {
            Player.QuestInfo.Current = new QuestCache(obj.Cache);
            Player.QuestInfo.SharedList = list;
            if (questSchedule != null) context.QuestScheduleList = questSchedule;
            if (pvpList != null)
            {
                context.PVPList = pvpList;
            }

            if (pvpInfo != null)
            {
                if (obj.PVPBattle != null)
                {
                    pvpInfo.BPConsumed = obj.PVPBattle.BPConsumed;
                    pvpInfo.BPUpdateTime = obj.PVPBattle.BPUpdateTime;
                }

                Player.PVPInfo = pvpInfo;
            }

            context.LogQuest(Command.QuestID, LogQuestType.Play);

            base.OnCommit(context);
        }

        protected override ErrorCode SafeProcessCommand(BeginResponse res)
        {
            questId = Command.QuestID;

            var errCode = ParsePVPTicket();

            if (errCode != ErrorCode.Success) return errCode;

            var data = Global.Tables.QuestDesign[questId];

            if (data == null) return ErrorCode.QuestNotExists;

            var limited = Player.QuestInfo.GetLimited(questId);

            var count = data.n_COUNT / 10;

            var isInfinity = data.n_COUNT == 0;

            if (questId != Tables.PVPQuestID)
            {
                switch (data.n_QUEST_TYPE)
                {
                    case QuestType.Limited:
                        {
                            if (limited == null || limited.EndTime.AddMinutes(3) < CurrentTime) return ErrorCode.QuestOverdue;
                            break;
                        }
                    case QuestType.Schedule:
                        {
                            if (questSchedule == null || questSchedule.Date < CurrentTime.Date) questSchedule = QuestScheduleList.Generate(CurrentTime);
                            if (!questSchedule.IsAvailable(Command.QuestID, CurrentTime)) return ErrorCode.QuestOverdue;
                            // 每日重置次數,故昨日的統計資料無視
                            if (limited != null && limited.EndTime < CurrentTime) limited = null;
                            break;
                        }
                    case QuestType.Crusade:
                        {
                            if (Command.Crusade > 0)
                            {
                                var crusade = sharedList[Command.Crusade];

                                if (crusade == null)
                                {
                                    if (list != null)
                                        crusade = list.Find(o => o.Serial == Command.Crusade);

                                    if (crusade == null)
                                    {
                                        list = SharedQuestList.Query(PublicID);
                                        crusade = list.Find(o => o.Serial == Command.Crusade);
                                    }

                                    if (crusade != null && crusade.QuestID != Command.QuestID) return ErrorCode.QuestNotMatch;
                                }

                                if (crusade != null && crusade.EndTime.AddMinutes(1) >= CurrentTime)
                                {
                                    if (obj.EventScheduleList.Crusade == null || obj.EventScheduleList.Crusade.EndTime.AddMinutes(1) < CurrentTime)
                                        return ErrorCode.QuestOverdue;

                                    var point = obj.EventScheduleList.CalcCrusadeCount(Player.QuestInfo.Crusade.Point, Player.QuestInfo.Crusade.UpdateTime, CurrentTime);
                                    if (point < Global.Tables.CrusadeScale) return ErrorCode.QuestPointNotEnough;

                                    obj.Crusade = crusade;

                                    break;
                                }
                            }
                            return ErrorCode.QuestNotExists;
                        }
                }

                if (limited != null && !isInfinity && limited.PlayCount >= count) return ErrorCode.QuestExhausted;
            }

            var luck = 0;

            var leader = Player.CardInfo[Command.Leader];
            if (leader != null)
            {
                var luckData = Global.Tables.Player[Player.CardInfo.GetLuck(leader.ID)];
                if (luckData != null) luck += luckData.n_LUCK_DROP;
            }
            leader = Player.CardInfo[Command.SubLeader];
            if (leader != null)
            {
                var luckData = Global.Tables.Player[Player.CardInfo.GetLuck(leader.ID)];
                if (luckData != null) luck += luckData.n_LUCK_DROP;
            }

            var tactics = Player.TacticsInfo.Select(TacticsEffect.Drop);
            if (tactics != null) luck += tactics.n_EFFECT_Z;

            obj.Quest = Global.GenerateQuest(questId, luck);

            if (!Command.DisableRescue && questId != Tables.TutorialQuestID && questId != Tables.PVPQuestID && obj.Quest.Rounds > 1 && obj.EventScheduleList.IsRescueAvailable(Player.QuestInfo.RescueCount, Player.QuestInfo.RescueUpdateTime, CurrentTime))
            {
                var rate = data.n_LEVEL * 0.01f * obj.EventScheduleList.Rescue.Origin.n_QUESTID * 0.01f;

                // 只在主線關卡做保底
                if ((data.n_QUEST_TYPE == QuestType.Regular || data.n_QUEST_TYPE == QuestType.Extra) && rate < 0.1f) rate = 0.1f;

                if (MathUtils.Random.NextDouble() < rate)
                {
                    obj.TriggerRescue = true;
                    var round = MathUtils.Random.Next(0, obj.Quest.Rounds - 1);
                    obj.Quest.Events[round] = obj.EventScheduleList.Rescue.Origin.n_WEEKLY;
                }
            }

            if (obj.PVPBattle != null)
            {
                obj.Quest.Point = 0;

                if (obj.EventScheduleList.PVP != null && obj.EventScheduleList.PVP.BeginTime < CurrentTime && obj.EventScheduleList.PVP.EndTime > CurrentTime)
                {
                    if (pvpList.EventID != obj.EventScheduleList.PVP.Relative.ID)
                        pvpList = PVPContextList.Generate(obj.EventScheduleList, CurrentTime);

                    var mine = pvpList.GetPoint(PublicID);
                    var target = pvpList.GetPoint(obj.PVPBattle.Ticket.PlayerID);

                    KernelService.Logger.Debug("mine:{0}, target:{1}", mine, target);

                    obj.PVPBattle.Win = PVPServerUtils.Calc(mine, target, Tables.PVPBasePoint, Tables.PVPExtraPoint);
                    obj.PVPBattle.Lose = -PVPServerUtils.Calc(target, mine, Tables.PVPBasePoint, Tables.PVPExtraPoint);

                    KernelService.Logger.Debug("win:{0}, lose:{1}", obj.PVPBattle.Win, obj.PVPBattle.Lose);
                }
            }
            else if (obj.Quest.Point > 0)
            {
                var current = obj.EventScheduleList.GetCurrent(data.n_MAIN_QUEST);
                if (current == null || current.EndTime < CurrentTime)
                    obj.Quest.Point = 0;
            }
            else if ((data.n_QUEST_TYPE == QuestType.Regular && data.n_ID > 125) || data.n_QUEST_TYPE == QuestType.Extra)
            {
                foreach (var item in obj.EventScheduleList.SelectActiveSchedule(CurrentTime))
                {
                    if (item.Origin.n_MAIN_POINT != 1) continue;
                    obj.Quest.Point = data.n_KIZUNA;
                    break;
                }
            }

            if (obj.EventScheduleList.Crusade == null || obj.EventScheduleList.Crusade.BeginTime > CurrentTime || obj.EventScheduleList.Crusade.EndTime.AddMinutes(1) < CurrentTime || obj.Crusade != null)
                obj.Quest.Discover = 0;

            obj.Data = QuestCacheData.Create(obj.Quest);

            if (obj.Crusade != null) obj.Data.Crusade = obj.Crusade.Serial;

            if (!string.IsNullOrEmpty(Command.Support))
            {
                var fellow = PublicID.FromString(Command.Support);

                if (fellow.IsValid)
                {
                    obj.Data.Fellow = fellow;

                    if (!Player.QuestInfo.IsUsed(fellow))
                    {
                        var fp = Global.Tables.GetVariable(VariableID.FP_NORMAL);

                        using (var db = AcquireDB())
                        {
                            var cmd = DBCommandBuilder.SelectFellow(PublicID, fellow);
                            using (var dbRes = db.DoQuery(cmd))
                            {
                                if (!dbRes.IsEmpty)
                                {
                                    var fellowObj = DBObjectConverter.ConvertCurrent<DBFellow>(dbRes);
                                    var complex = new ComplexI32 { Value = fellowObj.Complex };
                                    if (complex[0] && complex[16]) fp = Global.Tables.GetVariable(VariableID.FP_FOLLOW);
                                }
                            }
                        }

                        obj.Data.FP = fp;
                    }

                }
            }

            QuestHelper.BeginQuest(Player, obj, CurrentTime);

            res.Quest = obj.Quest;

            if (obj.PVPBattle != null) SyncPVP(res);

            return ErrorCode.Success;
        }

        private ErrorCode ParsePVPTicket()
        {
            if (!string.IsNullOrEmpty(Command.Ticket))
            {
                questId = Tables.PVPQuestID;

                var schedule = obj.EventScheduleList.PVP;

                if (schedule == null || schedule.BeginTime > CurrentTime || schedule.EndTime < CurrentTime) return ErrorCode.Success;

                var ticket = PVPTicket.FromString(Command.Ticket);
                if (!ticket.Token.ID.Equals(PublicID)) return ErrorCode.PVPQuestInvalid;

                if (pvpInfo == null) pvpInfo = pvpList.Convert(PublicID, pvpInfo);

                var bp = PVPUtils.CalcBP(pvpInfo.BPConsumed, pvpInfo.BPUpdateTime, CurrentTime);

                if (bp <= 0) return ErrorCode.BPNotEnough;

                obj.PVPBattle = new PVPBattleObj { Ticket = ticket };

                obj.PVPBattle.OriginBPConsumed = pvpInfo.BPConsumed;
                obj.PVPBattle.OriginBPUpdateTime = pvpInfo.BPUpdateTime;

                if (bp < Tables.BPLimit)
                {
                    obj.PVPBattle.BPConsumed = pvpInfo.BPConsumed + 1;
                    obj.PVPBattle.BPUpdateTime = pvpInfo.BPUpdateTime;
                }
                else
                {
                    obj.PVPBattle.BPConsumed = 1;
                    obj.PVPBattle.BPUpdateTime = CurrentTime;
                }
            }

            return ErrorCode.Success;
        }

        private void SyncPVP(BeginResponse syncTarget)
        {
            pvpList.Update(PublicID, obj.PVPBattle.BPConsumed, obj.PVPBattle.BPUpdateTime);

            syncTarget.PlayerSync(PropertyID.BPConsumed, obj.PVPBattle.OriginBPConsumed, obj.PVPBattle.BPConsumed);
            syncTarget.PlayerSync(PropertyID.BPUpdateTime, DateTimeUtils.ToUnixTime(obj.PVPBattle.OriginBPUpdateTime), DateTimeUtils.ToUnixTime(obj.PVPBattle.BPUpdateTime));
        }
    }

    [ActiveTask(CommandType = typeof(BeginEventRequest))]
    public class QuestBeginEventTask : PlayerTask<BeginEventRequest, BeginResponse>
    {
        private BeginQuestObj obj = new BeginQuestObj();

        protected override void OnPrepare(Context context)
        {
            base.OnPrepare(context);
        }
        protected override void OnCommit(Context context)
        {
            Player.QuestInfo.Current = new QuestCache(obj.Cache);
            if (obj.EventCache != null) Player.CardInfo.Current = obj.EventCache;
            base.OnCommit(context);
        }

        protected override ErrorCode SafeProcessCommand(BeginResponse res)
        {
            #region 檢查觸發卡片狀態合不合法
            var card = Player.CardInfo[Command.Serial];

            var index = Calc.CardEvent.GetIndex(card.ID, Command.InteractiveID);

            if (index < 0) return ErrorCode.QuestNotExists;

            var interactive = Global.Tables.Interactive[Command.InteractiveID];

            var isFinished = false;

            // 已經完成過的事件就不檢查了
            if (card.IsEventFinished(index))
                isFinished = true;
            else
            {
                var ids = new HashSet<int>();
                CardUtils.GetIDs(Player.CardInfo.List, ids);

                var errCode = Calc.CardEvent.Check(interactive, card.Kizuna, ids, Player.QuestInfo.Get);

                if (errCode != ErrorCode.Success) return errCode;
            }

            var ev = EventUtils.CallEvent(interactive);

            if (ev.n_RESULT != EventResult.Quest) return ErrorCode.QuestIllegal;

            #endregion

            var data = Global.Tables.QuestDesign[ev.n_RESULT_X];

            if (data == null) return ErrorCode.QuestNotExists;

            obj.Quest = Global.GenerateQuest(ev.n_RESULT_X, 0);

            if (isFinished)
            {
                // 有完成過就不給掉落物了
                obj.Quest.Drop = 0;
            }
            else
            {
                obj.CardSerial = Serial.Create(Command.Serial);
                obj.InteractiveID = Command.InteractiveID;
            }

            obj.Data = QuestCacheData.Create(obj.Quest);

            QuestHelper.BeginQuest(Player, obj, CurrentTime);

            res.Quest = obj.Quest;

            return ErrorCode.Success;
        }
    }

    #endregion

    #region Utils
    public partial class QuestHelper
    {
        public static void BeginQuest(Player player, BeginQuestObj obj, DateTime now)
        {
            var rescueComplex = obj.TriggerRescue ? ComplexI32.Bit(RESCUE_INDEX).Value : 0;

            using (var db = Global.DBPool.Acquire(player.ID.Group))
            {
                using (var trans = db.BeginTransaction())
                {
                    var cmd = DBCommandBuilder.BeginQuest(player.ID, obj, rescueComplex, now);

                    using (var result = db.DoQuery(cmd)) obj.Cache = DBObjectConverter.ConvertCurrent<DBQuestCache>(result);

                    obj.Quest.Serial = obj.Cache.ID;

                    cmd = DBCommandBuilder.UpdateQuestLastPlayTime(player.ID, obj.Quest.QuestID, now);

                    db.SimpleQuery(cmd);

                    if (!obj.CardSerial.IsEmpty)
                    {
                        cmd = DBCommandBuilder.InsertCardEventCache(player.ID, obj.CardSerial, obj.InteractiveID);
                        db.SimpleQuery(cmd);

                        obj.EventCache = new DBCardEventCache { PlayerID = player.ID.Serial, CardID = obj.CardSerial.Value, InteractiveID = obj.InteractiveID, QuestID = obj.Quest.Serial };
                    }

                    if (obj.PVPBattle != null)
                    {
                        cmd = DBCommandBuilder.UpdateBP(player.ID, obj.PVPBattle.BPConsumed, obj.PVPBattle.BPUpdateTime);
                        db.SimpleQuery(cmd);

                        cmd = DBCommandBuilder.InsertPVPBattle(player.ID, obj.PVPBattle.Ticket.PlayerID, obj.Cache.ID, obj.PVPBattle.Win, obj.PVPBattle.Lose, now);
                        db.SimpleQuery(cmd);
                    }

                    trans.Commit();
                }
            }
        }
    }
    #endregion
}
