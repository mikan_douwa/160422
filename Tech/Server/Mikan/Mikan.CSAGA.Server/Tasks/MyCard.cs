﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mikan.CSAGA.Commands.MyCard;
using Mikan.Server;
using System.Security.Cryptography;

namespace Mikan.CSAGA.Server.Tasks
{
    [ActiveTask(CommandType = typeof(BeginTransactionRequest))]
    public class MyCardBeginTransactionTask : PlayerTask<BeginTransactionRequest, BeginTransactionResponse>
    {

        protected override ErrorCode SafeProcessCommand(BeginTransactionResponse res)
        {
            /*
            if (!Global.MyCardSetting.Sandbox)
            {
                using (var db = AcquireUniqueDB())
                {
                    var cmd = DBCommandBuilder.SelectSuperAccount(PublicID);
                    using (var result = db.DoQuery(cmd))
                    {
                        if (result.IsEmpty) return ErrorCode.IAPIllegal;
                    }
                }
            }
            */

            var product = default(IAPProduct);

            if (!Player.Context.IAPProductList.TryGetValue(Command.ProductID, out product))
            {
                return ErrorCode.IAPIllegal;
            }

            var transSerial = default(Int64);

            using (var db = AcquireUniqueDB())
            {
                var cmd = DBCommandBuilder.InsertMyCardTransaction(PublicID, product.ID, CurrentTime);

                transSerial = db.DoQuery<Int64>(cmd);
            }

            var mycardTrans = Player.Context.MyCardIAPService.BeginTransaction(PublicID, transSerial, product.ID);

            if (!mycardTrans.IsValid) return ErrorCode.IAPIllegal;

            using (var db = AcquireUniqueDB())
            {
                var cmd = DBCommandBuilder.UpdateMyCardTransaction(transSerial, mycardTrans.AuthCode, mycardTrans.TradeSeq, ComplexI32.Bit(0).Value);
                db.SimpleQuery(cmd);
            }

            res.ProductID = Command.ProductID;
            res.AuthCode = mycardTrans.AuthCode;
            res.Sandbox = Player.Context.MyCardIAPService.Sandbox;

            return ErrorCode.Success;
        }
    }


    public class MyCardRestoreTask : ICommandTask<Context>
    {
        public string Data;

        private Mikan.Server.IAP.MyCard.IAPService iapService;
        private Dictionary<string, IAPProduct> productList;

        public void Setup(IInputContext input)
        {
            throw new NotImplementedException();
        }

        public void Prepare(Context context)
        {
            iapService = context.MyCardIAPService;
            productList = context.IAPProductList;
        }

        public void Commit(Context context) { }

        public void Dispose() { }

        public void Execute()
        {
            var list = iapService.Restore(Data);
            if (!list.IsValid) return;


            foreach (var item in list.Transactions)
            {
                var obj = new MyCardClaimObj(iapService, productList);

                var mycardTrans = obj.QueryTransaction(item);

                if (mycardTrans == null)
                {
                    KernelService.Logger.Debug("[MYCARD] restore transaction not exists, id={0}", item);
                    continue;
                }

                var err = obj.ProcessTransaction(mycardTrans, "restore", null, null);

                if (err != ErrorCode.Success)
                {
                    KernelService.Logger.Debug("[MYCARD] restore invalid transaction, id={0}", item);
                    continue;
                }
                else
                    obj.AddGem();

                obj.CommitTrans();
            }
        }
    }

    public class MyCardClaimObj
    {
        public bool IsTransComplete { get; private set; }

        public Int64 ReceiptID { get { return receiptId; } }

        public int Gem { get { return gem; } }

        public int Bonus { get { return bonus; } }

        public int MyCardBonus { get { return mycardBonus; } }

        private PublicID id;

        private Int64 receiptId;
        private int gem;
        private int bonus;
        private int mycardBonus;
        private IAPProduct product;

        private IAPInfo iapInfo;

        private Mikan.Server.IAP.MyCard.IAPService iapService;

        private Dictionary<string, IAPProduct> productList;

        private DBMyCardTransaction mycardTrans;

        public MyCardClaimObj(Mikan.Server.IAP.MyCard.IAPService iapService, Dictionary<string, IAPProduct> productList)
        {
            this.iapService = iapService;
            this.productList = productList;
        }

        public DBMyCardTransaction QueryTransaction(PublicID id, Int64 serial)
        {
            using (var db = Global.DBPool.AcquireUnique())
            {
                var cmd = DBCommandBuilder.SelectMyCardTransaction(id, serial);

                using (var dbRes = db.DoQuery(cmd))
                {
                    if (!dbRes.IsEmpty) return DBObjectConverter.ConvertCurrent<DBMyCardTransaction>(dbRes);
                }
            }

            return null;
        }

        public DBMyCardTransaction QueryTransaction(Int64 serial)
        {
            using (var db = Global.DBPool.AcquireUnique())
            {
                var cmd = DBCommandBuilder.SelectMyCardTransaction(serial);

                using (var dbRes = db.DoQuery(cmd))
                {
                    if (!dbRes.IsEmpty) return DBObjectConverter.ConvertCurrent<DBMyCardTransaction>(dbRes);
                }
            }

            return null;
        }

        public ErrorCode ProcessTransaction(DBMyCardTransaction mycardTrans, string token, IAPInfo iapInfo, IAPProduct product)
        {
            this.mycardTrans = mycardTrans;
            this.iapInfo = iapInfo;

            if (mycardTrans == null) return ErrorCode.IAPIllegal;

            var complex = new ComplexI32 { Value = mycardTrans.Complex };

            if (!complex[Mikan.Server.IAP.MyCard.Setting.IS_VALID]) return ErrorCode.IAPIllegal;

            IsTransComplete = complex[Mikan.Server.IAP.MyCard.Setting.IS_TRANS_COMPLET];

            if (IsTransComplete || complex[Mikan.Server.IAP.MyCard.Setting.IS_TRADING]) return ErrorCode.Success;

            id = PublicID.Create(mycardTrans.GroupID, mycardTrans.PlayerID);

            if (product == null)
            {
                foreach (var item in productList)
                {
                    if (item.Value.ID == mycardTrans.ProductID)
                    {
                        product = item.Value;
                        break;
                    }
                }
            }

            if (product == null) return ErrorCode.IAPIllegal;

            this.product = product;

            var checkRes = iapService.Check(id, mycardTrans.AuthCode);

            if (checkRes.IsSucc)
            {
                var hash = CalcHash(checkRes.OrderID);

                using (var db = Global.DBPool.AcquireUnique())
                {
                    var cmd = DBCommandBuilder.SelectMyCardReceipt(hash);

                    using (var dbRes = db.DoQuery(cmd))
                    {
                        foreach (var item in DBObjectConverter.Convert<DBIAPReceipt>(dbRes))
                        {
                            if (item.OrderID != checkRes.OrderID) continue;
                            receiptId = item.ID;
                            break;
                        }
                    }
                }

                if (receiptId == 0)
                {
                    gem = CalcGem(product, receiptId, out bonus);

                    if(gem > 0) mycardBonus = checkRes.Bonus;

                    using (var db = Global.DBPool.AcquireUnique())
                    {
                        using (var trans = db.BeginTransaction())
                        {
                            var cmd = DBCommandBuilder.UpdateMyCardTransaction(mycardTrans.ID, ComplexI32.Bit(Mikan.Server.IAP.MyCard.Setting.IS_TRADING).Value);
                            db.SimpleQuery(cmd);

                            cmd = DBCommandBuilder.InsertMyCardReceipt(id, product.ID, checkRes.OrderID, hash, token, gem, bonus, Global.CurrentTime);
                            receiptId = db.DoQuery<Int64>(cmd);

                            cmd = DBCommandBuilder.InsertMyCardHistory(id, mycardTrans.ID, mycardTrans.TradeSeq, checkRes.Receipt, Global.CurrentTime);
                            db.SimpleQuery(cmd);

                            trans.Commit();
                        }
                    }
                }
            }
            else if (checkRes.IsNetFail)
                return ErrorCode.IAPTimeout;
            else
                return ErrorCode.IAPIllegal;

            return ErrorCode.Success;
        }

        public void AddGem()
        {
            IAPUtils.AddGem(id, product, receiptId, gem, bonus, mycardBonus, Global.CurrentTime);
        }

        public void CommitTrans()
        {
            if (iapService.Confirm(mycardTrans.AuthCode))
            {
                if (IsTransComplete) return;

                using (var db = Global.DBPool.AcquireUnique())
                {
                    var cmd = DBCommandBuilder.UpdateMyCardTransaction(mycardTrans.ID, ComplexI32.Bit(Mikan.Server.IAP.MyCard.Setting.IS_TRANS_COMPLET).Value);
                    db.SimpleQuery(cmd);
                }
            }
        }

        private int CalcGem(IAPProduct product, Int64 receiptId, out int bonus)
        {
            var currentTime = Global.CurrentTime;

            bonus = 0;

            if (iapInfo == null) iapInfo = IAPUtils.QueryIAPInfo(id);

            var history = default(DBIAPHistory);

            if (iapInfo.History.TryGetValue(product.ID, out history))
            {
                if (receiptId != 0 && receiptId == history.ReceiptID)
                    return 0;
                else
                {
                    if (product.IsMyCard && product.ID == 11 && history.UpdateTime.Date != currentTime.Date)
                        bonus = product.Bonus;
                    else if (history.UpdateTime.Year != currentTime.Year || history.UpdateTime.Month != currentTime.Month)
                        bonus = product.Bonus;
                }
            }
            else
                bonus = product.Bonus;

            return product.Gem;
        }

        private Int64 CalcHash(string text)
        {
            using (var md5 = MD5.Create())
            {
                var bytes = md5.ComputeHash(Encoding.UTF8.GetBytes(text));

                var hashBytes = new byte[] { bytes[1], bytes[13], bytes[9], bytes[3], bytes[2], bytes[10], bytes[6], bytes[11] };
                return BitConverter.ToInt64(hashBytes, 0);
            }
        }
    }
}
