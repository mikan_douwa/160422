﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mikan.CSAGA.Commands.Item;
using Mikan.Server;

namespace Mikan.CSAGA.Server.Tasks
{
    [ActiveTask(CommandType = typeof(InfoRequest))]
    public class ItemInfoTask : PlayerTask<InfoRequest, InfoResponse>
    {
        protected override ErrorCode SafeProcessCommand(InfoResponse res)
        {
            res.ItemInfo = Player.ItemInfo.Serialize();
            return ErrorCode.Success;
        }
    }

    [ActiveTask(CommandType = typeof(UseRequest))]
    public class ItemUseTask : PlayerTask<UseRequest, UseResponse>
    {
        private DropObj dropObj;
        private int diff;
        protected override void OnCommit(Context context)
        {
            dropObj.Commit(Player);
            Player.ItemInfo.Add(Command.ItemID, diff);
            base.OnCommit(context);
        }
        protected override ErrorCode SafeProcessCommand(UseResponse res)
        {
            var itemData = Global.Tables.Item[Command.ItemID];
            if (itemData.n_EFFECT != ItemEffect.Usable) return ErrorCode.ItemIllegal;

            var count = Player.ItemInfo.Count(Command.ItemID);
            if (count < itemData.n_EFFECT_Y) return ErrorCode.ItemNotEnough;

            diff = -itemData.n_EFFECT_Y;

            var dropId = Calc.Drop.Next(itemData.n_EFFECT_X);

            dropObj = new DropObj(Player, RewardSource.Item);
            dropObj.Add(dropId);

            using (var db = AcquireDB())
            {
                using (var trans = db.BeginTransaction())
                {
                    var cmd = DBCommandBuilder.IncreaseItemCount(PublicID, Command.ItemID, diff);
                    db.SimpleQuery(cmd);
                    dropObj.Apply(Player, db, CurrentTime);
                    trans.Commit();
                }
            }

            res.DropID = dropId;
            dropObj.Sync(res);
            res.AddSyncData(new SyncItem { Diff = Diff.Create(Command.ItemID, count, count + diff) });
            
            return ErrorCode.Success;
        }
    }
}
