﻿using Mikan.CSAGA.Commands.Tactics;
using Mikan.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mikan.CSAGA.Server.Tasks
{
    [ActiveTask(CommandType = typeof(InfoRequest))]
    public class TacticsInfoTask : PlayerTask<InfoRequest, InfoResponse>
    {
        protected override ErrorCode SafeProcessCommand(InfoResponse res)
        {
            res.TacticsInfo = Player.TacticsInfo.Serialize();

            return ErrorCode.Success;
        }

    }

    [ActiveTask(CommandType = typeof(EnableRequest))]
    public class TacticsEnableTask : PlayerTask<EnableRequest, EnableResponse>
    {
        private Dictionary<int, ValuePair> items;
        protected override void OnCommit(Context context)
        {
            Player.TacticsInfo.Add(Command.TacticsID);
            
            foreach (var item in items) Player.ItemInfo.Add(item.Key, item.Value.Diff);

            base.OnCommit(context);
        }

        protected override ErrorCode SafeProcessCommand(EnableResponse res)
        {
            var data = Tables.Tactics[Command.TacticsID];

            var current = Player.TacticsInfo[data.n_GROUP];

            // 1級要先開放且只能升到下一級
            if (current == null || current.n_LV != data.n_LV - 1) return ErrorCode.TacticsIllegal;

            items = new Dictionary<int, ValuePair>();

            for (int i = 0; i < data.n_MATERIAL.Length; ++i)
            {
                var itemId = data.n_MATERIAL[i];
                var itemCount = data.n_COUNT[i];
                if (itemId == 0 || itemCount == 0) continue;

                var originCount = Player.ItemInfo.Count(itemId);
                if (itemCount > originCount) return ErrorCode.ItemNotEnough;
                items.Add(itemId, ValuePair.Create(originCount, originCount - itemCount));
            }


            using (var db = AcquireDB())
            {
                using (var trans = db.BeginTransaction())
                {
                    var cmd = DBCommandBuilder.InsertTactics(PublicID, Command.TacticsID, CurrentTime);
                    db.SimpleQuery(cmd);

                    foreach (var item in items)
                    {
                        cmd = DBCommandBuilder.IncreaseItemCount(PublicID, item.Key, item.Value.Diff);
                        db.SimpleQuery(cmd);
                    }

                    trans.Commit();
                }
            }

            res.TacticsID = Command.TacticsID;

            var syncData = new SyncTactics { TacticsID = Command.TacticsID };
            res.AddSyncData(syncData);

            foreach (var item in items) res.AddSyncData(new SyncItem { Diff = Diff.Create(item.Key, item.Value.OriginValue, item.Value.NewValue) });

            return ErrorCode.Success;
        }

    }
}
