﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mikan.Server;
using Mikan.CSAGA.Commands.Player;

namespace Mikan.CSAGA.Server.Tasks
{
    [ActiveTask(CommandType = typeof(InitRequest))]
    public class PlayerLoginTask : PlayerTask<InitRequest, InitResponse>
    {
        private EventScheduleList schedule;
        private Card favorite;
        private bool resetRescue;
        private DBDailyEP dailyEp;

        private bool hasGachaRefund = false;

        protected override void OnPrepare(Context context)
        {
            base.OnPrepare(context);

            if (Player.CardInfo != null) favorite = Player.CardInfo.Favorite;

            schedule = context.EventScheduleList;
        }
        protected override void OnCommit(Context context)
        {
            if (favorite != null && Player.Unsafe.KizunaDiff > 0)
            {
                favorite.Kizuna += Player.Unsafe.KizunaDiff;
            }

            if (Player.Unsafe.FPDiff != null)
                Player.PlayerInfo[PropertyID.FP] += Player.Unsafe.FPDiff.Diff;

            if (hasGachaRefund)
                Player.PlayerInfo.UpdateComplex(ComplexIndex.PlayerGachaRefund, true);

            Player.DailyEP = dailyEp;

            Player.Unsafe.KizunaDiff = 0;
            Player.Unsafe.FPAdd = 0;
            Player.Unsafe.FPDiff = null;

            Player.CommitCache();
            Player.TrophyInfo.Commit();

            //if (resetRescue) Player.ItemInfo.Delete(Global.Tables.RescueRewardItemID);

            if (Player.CardInfo != null) Player.CardInfo.SetHeader(Command.Header);
            context.Owner.AddTask(new UpdateSnapshotTask { ID = PublicID, UpdateTime = CurrentTime });

            base.OnCommit(context);
        }
        protected override ErrorCode SafeProcessCommand(InitResponse res)
        {
            //if (string.IsNullOrEmpty(Player.Transaction)) return ErrorCode.AppNotMatch;

            if (Player.PlayerInfo.IsAvailable)
            {
                Player.Trophy.Init();

                if (favorite != null && Player.Unsafe.KizunaDiff > 0)
                {
                    res.CardSync.UpdateList = new List<CardDiff>(1);
                    res.CardSync.UpdateList.Add(CardDiff.Create(favorite.Serial, PropertyID.Kizuna, favorite.Kizuna, favorite.Kizuna + Player.Unsafe.KizunaDiff));
                }

                if (Player.QuestInfo.QuestCacheBeginSerial > 0)
                {
                    using (var db = AcquireDB())
                    {
                        var cmd = DBCommandBuilder.DeleteOverdueQuestCache(PublicID, Player.QuestInfo.QuestCacheBeginSerial);
                        db.SimpleQuery(cmd);
                    }
                }

                if (Player.Unsafe.FPAdd > 0)
                    res.AddSyncData(new SyncDailyFP { Add = Player.Unsafe.FPAdd });

                #region 計算積分獎勵
                var rankingInfo = default(RankingInfo);
                using (var db = AcquireDB()) rankingInfo = RankingInfo.Query(db, schedule, PublicID);

                foreach (var item in rankingInfo.List)
                {
                    var obj = item.Value;
                    if (obj.Point == 0/* || obj.Complex[0]*/) continue;
                    Player.Trophy.OnEventPoint(obj.EventID, obj.Point);
                }
                #endregion
            }

            var playerInfo = Player.PlayerInfo.Clone();

            if (Player.Unsafe.FPDiff != null)
            {
                playerInfo[PropertyID.FP] = Player.Unsafe.FPDiff.NewValue;
            }

            if (playerInfo.IsAvailable && !playerInfo.GachaRefund && Player.AccountInfo.CreateTime < new DateTime(2016, 7, 14))
            {
                if (Player.UseSuperPrivilege || !Player.Context.IsMaintaining)
                {
                    var giftCount = 0;

                    using (var db = AcquireDB())
                    {
                        var cmd = DBCommandBuilder.SelectPurchase(PublicID);
                        var gachaCount = 0;
                        using (var dbRes = db.DoQuery(cmd))
                        {
                            foreach (var item in DBObjectConverter.Convert<DBPurchase>(dbRes))
                            {
                                if (item.Cost == 30 || item.Cost == 100)
                                    gachaCount += 1;
                                else if (item.Cost == 900 || item.Cost == 1000)
                                    gachaCount += 10;
                            }
                        }

                        if (gachaCount >= 50 && gachaCount <= 300)
                            giftCount = gachaCount / 4;
                        else if (gachaCount > 300)
                            giftCount = gachaCount / 3;
                    }

                    using (var db = AcquireDB())
                    {
                        using (var trans = db.BeginTransaction())
                        {
                            var cmd = DBCommandBuilder.UpdatePlayerComplex(PublicID, Complex.Bit(ComplexIndex.PlayerGachaRefund).Value);
                            db.SimpleQuery(cmd);

                            if (giftCount > 0)
                            {
                                cmd = DBCommandBuilder.InsertGift(PublicID, RewardSource.GachaFeedback, "神運道具派發", DropType.Item, giftCount, Tables.LuckItemID);
                                db.SimpleQuery(cmd);
                            }

                            trans.Commit();
                        }

                        playerInfo.UpdateComplex(ComplexIndex.PlayerGachaRefund, true);
                    }

                    hasGachaRefund = true;
                }
            }


            #region Infinity

            var infinity = schedule.GetCurrentInfinity(CurrentTime);

            if (infinity != null)
            {
                var syncData = new SyncInfinity { EventID = infinity.EventID, Seed = infinity.Relative.ID, BeginTime = infinity.BeginTime };
                
                if (Player.PlayerInfo.IsAvailable)
                {
                    var current = Player.QuestInfo.InfinityList.SafeGetValue(infinity.Relative.ID);
                    if (current != null)
                    {
                        syncData.Point = current.Point;
                        syncData.UpdateTime = CurrentTime;//current.UpdateTime;
                    }
                }
                
                res.AddSyncData(syncData);
            }
            
            #endregion

            #region 每日積分

            using (var db = AcquireDB())
            {
                var cmd = DBCommandBuilder.SelectDailyEventPoint(PublicID);
                using (var dbRes = db.DoQuery(cmd))
                {
                    if (!dbRes.IsEmpty)
                    {
                        dailyEp = DBObjectConverter.ConvertCurrent<DBDailyEP>(dbRes);
                        var syncData = new SyncDailyEventPoint { Current = dailyEp.Point, UpdateTime = dailyEp.UpdateTime };
                        res.AddSyncData(syncData);
                    }
                }
            }

            #endregion

            res.PlayerInfo = playerInfo.Serialize();

            return ErrorCode.Success;
        }
    }

    [ActiveTask(CommandType = typeof(CreateRequest))]
    public class PlayerCreateTask : PlayerTask<CreateRequest, CreateResponse>
    {
        protected override Priority UpdatePriority { get { return Priority.Immediately; } }

        protected override void OnCommit(Context context)
        {
            // client行為是在創角後會再重新登入一次,就不在這裡更新玩家資料了

            base.OnCommit(context);
        }

        protected override ErrorCode SafeProcessCommand(CreateResponse res)
        {
            if (!Player.PlayerInfo.IsAvailable)
            {
                using (var db = AcquireDB())
                {
                    using (var trans = db.BeginTransaction())
                    {
                        var cmd = DBCommandBuilder.AlterPlayerNameFristTime(PublicID, Command.Name, CurrentTime);

                        db.SimpleQuery(cmd);

                        #region 塞初始卡片

                        foreach (var item in Global.GetDefaultCardList(Command.Category))
                        {
                            cmd = DBCommandBuilder.InsertCard(PublicID, item);
                            db.SimpleQuery(cmd);
                            cmd = DBCommandBuilder.InsertGallery(PublicID, item, 1);
                            db.SimpleQuery(cmd);
                        }

                        #endregion

                        /*
#warning [DEV]
                        #region 初始+金錢&寶石(測試用)
                        cmd = DBCommandBuilder.InsertGift(PublicID, RewardSource.Debug, "測試用神石", DropType.Gem, 25000, 0);
                        db.SimpleQuery(cmd);
                        cmd = DBCommandBuilder.InsertGift(PublicID, RewardSource.Debug, "測試用經驗水晶", DropType.Crystal, 1000000, 0);
                        db.SimpleQuery(cmd);
                        cmd = DBCommandBuilder.InsertGift(PublicID, RewardSource.Debug, "測試用始源之塵", DropType.Coin, 10000, 0);
                        db.SimpleQuery(cmd);
                        //cmd = DBCommandBuilder.InsertGift(PublicID, RewardSource.Debug, "測試用卡片", DropType.Card, 20, 9001);
                        //db.SimpleQuery(cmd);
                        #endregion
                        

#warning [DEV] 初始所有禮物數量30個(測試用)
                        #region 修改好感度禮物數量(測試用)
                        
                        foreach (var item in Global.Tables.Item.Select(o => { return o.n_TYPE == ItemType.Gift; }))
                        {
                            cmd = DBCommandBuilder.IncreaseItemCount(PublicID, item.n_ID, 30);
                            db.SimpleQuery(cmd);
                        }

                        #endregion
                        */

                        trans.Commit();
                    }
                }
            }

            return ErrorCode.Success;
        }
    }

    [ActiveTask(CommandType = typeof(NameRequest))]
    public class PlayerNameTask : PlayerTask<NameRequest, NameResponse>
    {

        protected override void OnCommit(Context context)
        {
            Player.PlayerInfo.Name = Command.Name;
            Player.UpdateSnapshot(CurrentTime);

            base.OnCommit(context);
        }
        protected override ErrorCode SafeProcessCommand(NameResponse res)
        {
            using (var db = AcquireDB())
            {
                var cmd = DBCommandBuilder.AlterPlayerName(PublicID, Command.Name, CurrentTime);
                db.SimpleQuery(cmd);
            }

            res.Name = Command.Name;

            return ErrorCode.Success;
        }
    }

    [ActiveTask(CommandType = typeof(Commands.RestoreRequest))]
    public class BackupRestoreTask : PlayerTask<Commands.RestoreRequest, Commands.RestoreResponse>
    {

        protected override ErrorCode SafeProcessCommand(Commands.RestoreResponse res)
        {
            res.List = new List<Backup>();

            using (var db = AcquireDB())
            {
                var cmd = DBCommandBuilder.SelectBackup(PublicID);
                using (var dbRes = db.DoQuery(cmd))
                {
                    foreach (var item in DBObjectConverter.Convert<DBBackup>(dbRes))
                        res.List.Add(new Backup { Key = item.Key, Data = item.Data });
                }
            }

            

            return ErrorCode.Success;
        }
    }
}
