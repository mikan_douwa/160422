﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mikan.CSAGA.Commands.Social;
using Mikan.Server;

namespace Mikan.CSAGA.Server.Tasks
{
    [ActiveTask(CommandType = typeof(InfoRequest))]
    public class SocialInfoTask : PlayerTask<InfoRequest, InfoResponse>
    {
        private SocialInfo socialInfo;
        protected override void OnCommit(Context context)
        {
            Player.SocialInfo = socialInfo;
            base.OnCommit(context);
        }
        protected override ErrorCode SafeProcessCommand(InfoResponse res)
        {

            socialInfo = new SocialInfo();
            socialInfo.Origins = SocialUtils.QueryFellowList(PublicID);
            socialInfo.List = new List<Fellow>(socialInfo.Origins.Count);
            foreach (var item in socialInfo.Origins)
            {
                var id = PublicID.Create(item.FellowGroup, item.FellowID);
                socialInfo.List.Add(new Fellow { ID = id, PublicID = id.ToString(), Complex = new ComplexI32 { Value = item.Complex } });
            }

            res.SocialInfo = socialInfo.Serialize();

            return ErrorCode.Success;
        }
    }

    [ActiveTask(CommandType = typeof(PurchaseHistoryRequest))]
    public class SocialPurchaseHistoryTask : PlayerTask<PurchaseHistoryRequest, PurchaseHistoryResponse>
    {
        private List<DBSocialPurchase> list;

        protected override void OnPrepare(Context context)
        {
            base.OnPrepare(context);
            list = Player.SocialPurchaseList;
        }
        protected override void OnCommit(Context context)
        {
            Player.SocialPurchaseList = list;
            base.OnCommit(context);
        }

        protected override ErrorCode SafeProcessCommand(PurchaseHistoryResponse res)
        {
            if (list == null) list = SocialUtils.QueryPurchaseList(PublicID, CurrentTime.Date).ToList();

            res.List = new List<Data.PurchaseHistory>();

            foreach (var item in list)
            {
                if (item.Timestamp < CurrentTime.Date) continue;
                var obj = new Data.PurchaseHistory { ID = item.ArticleID, Timestamp = item.Timestamp };
                res.List.Add(obj);
            }

            return ErrorCode.Success;
        }
    }

    [ActiveTask(CommandType = typeof(QueryRequest))]
    public class SocialQueryTask : PlayerTask<QueryRequest, QueryResponse>
    {
        protected override ErrorCode SafeProcessCommand(QueryResponse res)
        {
            var snapshots = new List<Internal.Snapshot>();
            foreach (var item in Command.List)
            {
                var snapshot = new Internal.Snapshot { ID = item };

                var id = default(PublicID);

                try
                {
                    id = PublicID.FromString(item);
                }
                catch
                {
                    id = null;
                }

                if (id != null && id.IsValid)
                {

                    using (var db = AcquireDB(id))
                    {
                        var cmd = DBCommandBuilder.SelectSnapshot(id);

                        using (var dbRes = db.DoQuery(cmd))
                        {
                            if (!dbRes.IsEmpty)
                            {
                                var obj = DBObjectConverter.ConvertCurrent<DBSnapshot>(dbRes);
                                snapshot.Data = obj.Data;
                                snapshot.UpdateTime = obj.UpdateTime;

                                snapshots.Add(snapshot);
                            }
                        }
                    }
                }
            }

            res.Snapshots = snapshots;
            return ErrorCode.Success;
        }
    }

    [ActiveTask(CommandType = typeof(AddRequest))]
    public class SocialAddTask : PlayerTask<AddRequest, AddResponse>
    {
        protected override ErrorCode SafeProcessCommand(AddResponse res)
        {
            var fellow = PublicID.FromString(Command.ID);

            using (var db = AcquireDB())
            {
                var cmd = DBCommandBuilder.UpdateFellow(PublicID, fellow, ComplexI32.Bit(0).Value, CurrentTime, false);
                db.SimpleQuery(cmd);
            }

            using (var db = AcquireDB(fellow))
            {
                var cmd = DBCommandBuilder.UpdateFellow(fellow, PublicID, ComplexI32.Bit(16).Value, CurrentTime, true);
                db.SimpleQuery(cmd);
            }

            res.ID = Command.ID;

            return ErrorCode.Success;
        }
    }

    [ActiveTask(CommandType = typeof(RemoveRequest))]
    public class SocialRemoveTask : PlayerTask<RemoveRequest, RemoveResponse>
    {
        protected override ErrorCode SafeProcessCommand(RemoveResponse res)
        {
            var fellow = PublicID.FromString(Command.ID);

            using (var db = AcquireDB())
            {
                var cmd = DBCommandBuilder.DeleteFellow(PublicID, fellow, false);
                db.SimpleQuery(cmd);
            }

            using (var db = AcquireDB(fellow))
            {
                var cmd = DBCommandBuilder.DeleteFellow(fellow, PublicID, true);
                db.SimpleQuery(cmd);
            }

            res.ID = Command.ID;

            return ErrorCode.Success;
        }
    }

    [ActiveTask(CommandType = typeof(RandomRequest))]
    public class SocialRandomTask : PlayerTask<RandomRequest, RandomResponse>
    {
        private Random rand = new Random();

        private HashSet<PublicID> list = PublicID.CreateHashSet();
        
        protected override void OnPrepare(Context context)
        {
            base.OnPrepare(context);

            var count = context.PlayerCount;

            for (int i = 0; i < Global.Tables.FellowCount + 3; ++i)
            {
                var index = rand.Next(count);
                list.Add(context.GetPlayerID(index));
            }
        }

        protected override ErrorCode SafeProcessCommand(RandomResponse res)
        {

            res.List = new List<string>(Global.Tables.FellowCount);

            foreach (var item in list)
            {
                if (item.Equals(PublicID)) continue;
                res.List.Add(item.ToString());
                if (res.List.Count >= Global.Tables.FellowCount) break;
            }

            if (res.List.Count < Global.Tables.FellowCount)
            {
                using (var db = AcquireDB())
                {
                    var cmd = DBCommandBuilder.SelectSnapshot(PublicID, Global.Tables.FellowCount);
                    using (var dbRes = db.DoQuery(cmd))
                    {
                        foreach (var item in DBObjectConverter.Convert<DBSnapshot>(dbRes))
                        {
                            var id = PublicID.Create(PublicID.Group, item.PlayerID).ToString();
                            if (!res.List.Contains(id))
                            {
                                res.List.Add(id);
                                if (res.List.Count >= Global.Tables.FellowCount) break;
                            }
                        }
                    }
                }
            }

            return ErrorCode.Success;
        }
    }


    public class SocialUtils
    {
        public static List<DBFellow> QueryFellowList(PublicID id)
        {
            var list = default(List<DBFellow>);

            using (var db = Global.DBPool.Acquire(id.Group))
            {
                var cmd = DBCommandBuilder.SelectFellowList(id);
                using (var dbRes = db.DoQuery(cmd))
                {
                    list = DBObjectConverter.Convert<DBFellow>(dbRes).ToList();
                }
            }

            list.Sort((lhs, rhs) => lhs.UpdateTime.CompareTo(rhs.UpdateTime));

            return list;
        }

        public static IEnumerable<DBSocialPurchase> QueryPurchaseList(PublicID id, DateTime today)
        {
            using (var db = Global.DBPool.Acquire(id.Group))
            {
                var cmd = DBCommandBuilder.SelectSocialPurchaseList(id);

                using (var res = db.DoQuery(cmd))
                {
                    foreach (var item in DBObjectConverter.Convert<DBSocialPurchase>(res))
                    {
                        if (item.Timestamp >= today) yield return item;
                    }
                }
            }
        }
    }
}


namespace Mikan.CSAGA.Server.Tasks
{
    public class SocialPlayer : Internal.SocialPlayerObj
    {
        public PublicID ID;
    }

    [ActiveTask(CommandType = typeof(QueryRankRequest))]
    public class SocialQueryRankTask : PlayerTask<QueryRankRequest, QueryRankResponse>
    {
        protected override ErrorCode SafeProcessCommand(QueryRankResponse res)
        {
            var list = new List<Internal.SocialPlayerObj>(Command.List.Count);

            foreach (var item in Command.List)
            {
                var id = PublicID.FromString(item);
                var obj = new SocialPlayer { PublicID = item, ID = id };

                using (var db = AcquireDB(id.Group))
                {
                    var cmd = DBCommandBuilder.SelectPlayer(id);
                    using (var dbRes = db.DoQuery(cmd))
                    {
                        var dbPlayer = DBObjectConverter.ConvertCurrent<DBPlayer>(dbRes);
                        if (dbPlayer != null)
                            obj.Name = dbPlayer.Name;
                        else
                            obj.Name = "???";

                        list.Add(obj);
                    }
                }
            }

            res.List = list;

            return ErrorCode.Success;
        }
    }
}
