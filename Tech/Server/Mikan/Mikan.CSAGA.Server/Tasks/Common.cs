﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mikan.Server;
using Mikan.CSAGA.Commands;

namespace Mikan.CSAGA.Server.Tasks
{
    public enum TokenStatus
    {
        Normal,
        Invalid,
        Expire,
        Maintaining,
    }
    public enum Priority
    {
        None,
        Immediately,
        Delay,
    }

    abstract public class KernelTask : IKernelEvent<Context>
    {
        abstract public void Execute(Context context);

        virtual public void Dispose() { }
    }

    abstract public class Task<T> : HttpTask<Context, T>
        where T : ICommand
    {
        private bool hasPrivilege = false;

        private TokenStatus tokenStatus = TokenStatus.Normal;

        private bool isInitialized = false;

        protected ICommand feedback { get; private set; }

        protected Service owner { get; private set; }

        protected DateTime CurrentTime { get; private set; }

        protected ILogger Logger { get { return Service.Host.Logger; } }

        protected ConstData.Tables Tables { get { return Global.Tables; } }

        protected override void Feedback(ICommand cmd)
        {
            OnFeedback(cmd);
            feedback = cmd;
            base.Feedback(cmd);
        }

        sealed public override void Prepare(Context context)
        {
            if (!context.IsInitialized) return;

            CurrentTime = DateTime.Now.Subtract(Global.TimeDiff);

            isInitialized = true;

            Setup(context);

            tokenStatus = GetTokenStatus(context);

            if (tokenStatus == TokenStatus.Normal)
            {
                hasPrivilege = HasPrivilege(context);

                if (hasPrivilege) OnPrepare(context);
            }
        }

        virtual protected TokenStatus GetTokenStatus(Context context) { return TokenStatus.Normal; }

        virtual protected bool HasPrivilege(Context context) { return true; }

        virtual protected void OnPrepare(Context context) { }

        virtual protected void Setup(Context context)
        {
            owner = context.Owner;
        }

        sealed public override void Commit(Context context)
        {
            // 成功的操作才會把資料回寫
            if (isInitialized && hasPrivilege)
            {
                if (feedback.IsSucc && !IsRestore)
                {
                    try
                    {
                        OnCommit(context);
                    }
                    catch
                    {
                        OnCancel(context);
                        throw;
                    }
                }
                else
                    OnCancel(context);
            }
        }

        virtual protected void OnCommit(Context context) { }
        virtual protected void OnCancel(Context context) { }
        virtual protected void OnFeedback(ICommand cmd) { }
        virtual protected bool IsRestore { get { return false; } }
        virtual protected ICommand GetRestoreCommand() { return null; }

        sealed protected override void BeginProcessCommand()
        {
            try
            {
                if (tokenStatus == TokenStatus.Maintaining)
                {
                    EndProcessCommand(new MaintenanceMessage());
                    return;
                }

                if (!isInitialized)
                    EndProcessCommand(new RegularError(ErrorCode.Initializing));
                else if (tokenStatus == TokenStatus.Expire)
                    EndProcessCommand(new RegularError(ErrorCode.TokenExpired));
                else if (tokenStatus == TokenStatus.Invalid)
                    EndProcessCommand(new RegularError(ErrorCode.InvalidToken));
                else if (!hasPrivilege)
                    EndProcessCommand(new RegularError(ErrorCode.RaceCondition));
                else if (!IsRestore)
                    SafeBeginProcessCommand();
                else
                    EndProcessCommand(GetRestoreCommand());
            }
            catch (Exception e)
            {
                Logger.Fatal(e.ToString());
                EndProcessCommand(new InvalidOperation());
            }
        }

        virtual protected void SafeBeginProcessCommand()
        {
            EndProcessCommand(SafeProcessCommand());
        }

        virtual protected ICommand SafeProcessCommand()
        {
            throw new NotImplementedException();
        }

        protected IDBConnection AcquireUniqueDB()
        {
            return AcquireDB(-1);
        }

        protected IDBConnection AcquireDB(int group)
        {
            return Global.DBPool.Acquire(group);
        }

        protected IDBConnection AcquireDB(PublicID id)
        {
            return AcquireDB(id.Group);
        }

        protected void Cancel()
        {
            hasPrivilege = false;
        }
    }

    abstract public partial class PlayerTask<T> : Task<T>
        where T : CSAGARequest
    {

        private List<IAssistObj> assists = new List<IAssistObj>();

        private bool isRestore = false;

        virtual protected Priority UpdatePriority { get { return Priority.None; } }

        protected Player Player { get; private set; }

        protected PublicID PublicID
        {
            get
            {
                if (Token == null) return null;
                return Token.ID;
            }
        }

        private Token token;

        protected Token Token
        {
            get
            {
                if (token == null)
                {
                    if (CommandContext != null) token = CommandContext.Token;

                }
                return token;
            }
        }

        protected override bool IsRestore { get { return isRestore; } }

        protected override ICommand GetRestoreCommand()
        {
            KernelService.Logger.Debug("[RESTORE] end restore, res = {0}", Player.LastResponse);
            return Player.LastResponse;
        }

        protected override void Setup(Context context)
        {
            base.Setup(context);

            Player = context.GetPlayer(Token);

            if (Player != null && Command.TransferID > 0 && Player.LastRequest != null && Player.LastRequest.TransferID == Command.TransferID && Player.LastResponse != null)
            {
                isRestore = true;
                KernelService.Logger.Debug("[RESTORE] begin restore, req = {0}", Command);
            }
        }

        protected override TokenStatus GetTokenStatus(Context context)
        {
            if (Player == null) return TokenStatus.Invalid;

            if (Token.ExpireTime < CurrentTime) return TokenStatus.Expire;

            if (context.IsMaintaining && !Player.UseSuperPrivilege)
                return TokenStatus.Maintaining;
           
            return TokenStatus.Normal;
        }

        protected override bool HasPrivilege(Context context)
        {
            // 保證同一玩家同時只能進行一個操作
            return Player != null && Player.PlayerInfo != null && !Player.IsBusy;
        }

        protected override void OnPrepare(Context context)
        {
            Player.Owner = this;

            if (!isRestore)
            {
                Player.LastUse = CurrentTime;
                Player.OnSyncPrepare(context, Command.SyncList);
            }
        }

        protected override void OnCommit(Context context)
        {
            if (!isRestore)
            {
                CommitAssists();

                Player.EndSync(context);

                Player.LastRequest = Command;
                Player.LastResponse = feedback;

                if (UpdatePriority != Priority.None) Player.LastPlayTime = CurrentTime;
            }

            Player.Owner = null;
        }

        protected override void OnCancel(Context context)
        {
            Player.Owner = null;
        }

        protected override void OnFeedback(ICommand feedback)
        {
            if (isRestore) return;

            if (feedback.IsSucc)
            {
                SyncAssists(feedback);

                Player.BeginSync();
            }

            if (!feedback.IsSucc || UpdatePriority != Priority.Delay) return;

            using (var db = AcquireDB())
            {
                var cmd = DBCommandBuilder.UpdatePlayerLastPlayTime(PublicID);

                db.SimpleQuery(cmd);
            }
            
        }

        protected IDBConnection AcquireDB()
        {
            return AcquireDB(PublicID);
        }

        #region Assists

        protected void AddAssistObj(IAssistObj obj)
        {
            assists.Add(obj);
        }

        protected void ApplyAssists(IDBConnection db)
        {
            foreach (var item in assists) item.Apply(Player, db, CurrentTime);
        }

        private void SyncAssists(ICommand cmd)
        {
            var syncTarget = cmd as CSAGACommand;
            if (syncTarget != null)
            {
                foreach (var item in assists) item.Sync(syncTarget);
            }
        }
        private void CommitAssists()
        {
            foreach (var item in assists) item.Commit(Player);
        }

        #endregion

        protected void AddTrophyTask(TrophyTask task)
        {
            Player.QueuedTrophyTasks.Enqueue(task);
            if(Player.QueuedTrophyTasks.Count == 1)   
                Player.Context.Owner.AddCommandTask(task);
        }

        protected void AddLog(LogType type, params int[] param)
        {
            Player.Context.Owner.AddTask(new AddLogTask { PublicID = PublicID, Type = type, Params = param });
        }

        /*
        #region 給予奬勵

        protected void CommitReward(IEnumerable<IReward> rewards)
        {
            throw new NotImplementedException();

            foreach (var item in rewards)
            {
                switch (item.Type)
                {
                    case RewardType.Crystal:
                        break;
                    case RewardType.Gem:
                        break;
                    case RewardType.Coin:
                        break;
                    case RewardType.Card:
                        break;
                    case RewardType.Item:
                        break;

                }
            }
        }

        protected IEnumerable<IReward> AddReward(IDBConnection db, RewardSource source, RewardType type, int value1, int value2)
        {
            throw new NotImplementedException();
            switch (type)
            {
                case RewardType.Crystal:
                    {
                        break;
                    }
                case RewardType.Gem:
                    {
                        break;
                    }
                case RewardType.Coin:
                    {
                        break;
                    }
                case RewardType.Card:
                    {
                        break;
                    }
                case RewardType.Item:
                    {
                        
                        break;
                    }
            }
        }

        #endregion
        */
    }


    abstract public class PlayerTask<TReq, TRes> : PlayerTask<TReq>
        where TReq : CSAGARequest
        where TRes : CSAGAResponse
    {
        sealed protected override ICommand SafeProcessCommand()
        {
            var res = Activator.CreateInstance<TRes>();

            res.Status = SafeProcessCommand(res);

            return res;
        }

        abstract protected ErrorCode SafeProcessCommand(TRes res);
    }

    abstract public class SimpleTask : Task<CSAGACommand> { }

    public class IllegalCommandTask : SimpleTask
    {
        protected override ICommand SafeProcessCommand()
        {
            return new InvalidOperation();
        }
    }

    
    public class NotLoginYetCommandTask : SimpleTask
    {
        protected override ICommand SafeProcessCommand()
        {
            return new RegularError(ErrorCode.NotLoginYet);
        }
    }

    public class ExternalHttpHandler : UnmanagedHttpHandler
    {
        public ExternalHttpHandler(Service service)
            : base(service)
        {
        }
        protected override UnmanagedHttpTask CreateTask()
        {
            return new ExternalCommandTask();
        }
    }

    public class ExternalCommandTask : UnmanagedHttpTask
    {
        private static List<string> white_inquire = new List<string>(new string[]
        {
            "210.61.134.145",
            "210.71.189.161",
            "210.71.189.165",
            "60.251.54.250"
        });
        private static List<string> white_restore = new List<string>(new string[]
        {
            "210.61.134.145",
            "210.71.189.161",
            "220.130.127.125",
            "60.251.54.250"
        });

        protected override void ProcessRequest(Uri url, System.Collections.Specialized.NameValueCollection queryString)
        {
            var buffers = new Buffers();

            if (url.AbsolutePath.EndsWith("mycard/inquire"))
            {
                if (!Global.MyCardSetting.Sandbox && !white_inquire.Contains(this.UserHostAddress))
                {
                    Feedback(403);
                    return;
                }

                OnInquire(buffers);
            }
            else if (url.AbsolutePath.EndsWith("mycard/restore"))
            {
                if (!Global.MyCardSetting.Sandbox && !white_restore.Contains(this.UserHostAddress))
                {
                    Feedback(403);
                    return;
                }

                OnRestore(buffers);
            }
            else if (url.AbsolutePath.EndsWith("csaga/api"))
            {

            }
            
            Feedback(new Output { Buffers = buffers });
        }

        #region Inquire

        private void OnInquire(Buffers output)
        {
            string StartDateTime = this["StartDateTime"];
            string EndDateTime = this["EndDateTime"];
            string MyCardTradeNo = this["MyCardTradeNo"];

            KernelService.Logger.Debug("[MYCARD] inquire data, StartDateTime = {0}, EndDateTime = {1}, MyCardTradeNo = {2}", StartDateTime, EndDateTime, MyCardTradeNo);

            if (string.IsNullOrEmpty(StartDateTime) && string.IsNullOrEmpty(EndDateTime) && string.IsNullOrEmpty(MyCardTradeNo))
            {

                output.Add(QueryByDate(DateTime.MinValue, DateTime.MaxValue));

                return;
            }
            if (!string.IsNullOrEmpty(StartDateTime) && !string.IsNullOrEmpty(EndDateTime))
            {
                DateTime start_dt;
                DateTime end_dt;
                try
                {
                    start_dt = Convert.ToDateTime(StartDateTime);
                    end_dt = Convert.ToDateTime(EndDateTime);
                }
                catch (Exception)
                {
                    output.Add("DateTime Cast Fail");
                    return;
                }

                output.Add(QueryByDate(start_dt, end_dt));

            }
            else if (!string.IsNullOrEmpty(MyCardTradeNo))
            {
                output.Add(QueryByMyCardTradeNo(MyCardTradeNo));
            }
            else
            {
                output.Add("No Data Found");
            }

        }

        private string QueryByDate(DateTime start_dt, DateTime end_dt)
        {
            //To Do Query Transactions Between StartDateTime and EndDateTime 

            using (var db = Global.DBPool.AcquireUnique())
            {
                var cmd = DBCommandBuilder.SelectMyCardHistory(start_dt, end_dt);
                using (var res = db.DoQuery(cmd)) return Format(res);
            }
        }

        private string QueryByMyCardTradeNo(string MyCardTradeNo)
        {
            //To Do Query Transaction By "MyCardTradeNo"

            using (var db = Global.DBPool.AcquireUnique())
            {
                var cmd = DBCommandBuilder.SelectMyCardHistory(MyCardTradeNo);
                using (var res = db.DoQuery(cmd)) return Format(res);
            }
        }

        private string Format(IDBQueryResult res)
        {
            StringBuilder sb = new StringBuilder();

            foreach (var item in DBObjectConverter.Convert<DBMyCardHistory>(res))
            {
                var id = PublicID.Create(item.GroupID, item.PlayerID);
                sb.AppendFormat("{0},{1},{2},{3},{4},{5},{6},{7}<BR>",
                    item.PaymentType, item.TradeSeq, item.MyCardTradeNo, item.FacTradeSeq, id.ToString(), item.Amount, item.Currency, item.Timestamp.ToString("yyyy-MM-ddTHH:mm:ss")
                );
            }

            return sb.ToString();
        }

        #endregion

        #region Restore

        private void OnRestore(Buffers output)
        {
            string data = this["DATA"];

            ((Service)Service).AddCommandTask(new MyCardRestoreTask { Data = data });
        }

        #endregion

        class Output : IOutputContext
        {
            public Buffers Buffers { get; set; }
        }
    }

}
