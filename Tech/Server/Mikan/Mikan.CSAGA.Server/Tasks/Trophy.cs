﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mikan.Server;
using Mikan.CSAGA.Commands.Trophy;

namespace Mikan.CSAGA.Server.Tasks
{
    [ActiveTask(CommandType = typeof(InfoRequest))]
    public class TrophyInfoTask : PlayerTask<InfoRequest, InfoResponse>
    {
        protected override ErrorCode SafeProcessCommand(InfoResponse res)
        {
            res.TrophyInfo = Player.TrophyInfo.Serialize();

            return ErrorCode.Success;
        }

    }
    abstract public class TrophyTask : ICommandTask<Context>
    {
        public PublicID PublicID;

        private int retryCount = 0;

        protected Player Player { get; private set; }
        public void Setup(IInputContext input)
        {
            throw new NotImplementedException();
        }

        public void Prepare(Context context)
        {
            var player = context.GetPlayer(PublicID);

            if (player.Owner != null) return;

            Player = player;

            Player.Owner = this;
            Player.LastUse = Global.CurrentTime;
        }

        public void Commit(Context context)
        {
            if (Player == null)
            {
                if (++retryCount < 3)
                    context.Owner.AddCommandTask(this);

                return;
            }

            Player.CommitCache();
            Player.TrophyInfo.Commit();

            Player.QueuedTrophyTasks.Dequeue();

            if(Player.QueuedTrophyTasks.Count > 0)
                context.Owner.AddCommandTask(Player.QueuedTrophyTasks.Peek());

            Player.Owner = null;
            Player = null;
        }

        public void Dispose() { }

        public void Execute()
        {
            if (Player != null) BeginCheck();
        }

        abstract protected void BeginCheck();
    }

    class QuestTrophyTask : TrophyTask
    {
        public int QuestID;
        public int ChallengeCount;
        protected override void BeginCheck()
        {
            Player.Trophy.OnQuestClear(QuestID, ChallengeCount);
            Player.Trophy.OnPlayerExpChange(Player.PlayerInfo[PropertyID.Exp]);
            Player.Trophy.OnKizunaChange();
        }
    }

    class KizunaTrophyTask : TrophyTask
    {
        protected override void BeginCheck()
        {
            Player.Trophy.OnKizunaChange();
        }
    }

    class AwakenTrophyTask : TrophyTask
    {
        public int Awaken;
        protected override void BeginCheck()
        {
            if (Awaken == 0)
                Player.Trophy.OnAwakenRarity();
            else
                Player.Trophy.OnAwakenAbility();
        }
    }

    class TalkTrophy : TrophyTask
    {
        protected override void BeginCheck()
        {
            Player.Trophy.OnTalk();
        }
    }

    class EquipTrophy : TrophyTask
    {
        protected override void BeginCheck()
        {
            Player.Trophy.OnEquip();
        }
    }

    class GiftTrophy : TrophyTask
    {
        protected override void BeginCheck()
        {
            Player.Trophy.OnGift();
        }
    }

    class GachaTrophy : TrophyTask
    {
        protected override void BeginCheck()
        {
            Player.Trophy.OnSummon();
        }
    }
}
