﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mikan.CSAGA.Commands.Gift;
using Mikan.Server;

namespace Mikan.CSAGA.Server.Tasks
{
    
    [ActiveTask(CommandType = typeof(InfoRequest))]
    public class GiftInfoTask : PlayerTask<InfoRequest, InfoResponse>
    {
        private GiftInfo giftInfo;

        protected override void OnCommit(Context context)
        {
            // 贈禮採即時取回
            Player.GiftInfo = giftInfo;
            base.OnCommit(context);
        }

        protected override ErrorCode SafeProcessCommand(InfoResponse res)
        {
            giftInfo = GiftUtils.QueryGiftInfo(PublicID);

            res.GiftInfo = giftInfo.Serialize();

            return ErrorCode.Success;
        }
    }

    [ActiveTask(CommandType = typeof(ClaimRequest))]
    public class GiftClaimTask : PlayerTask<ClaimRequest, ClaimResponse>
    {
        private GiftInfo giftInfo;
        private IGiftClaim claimObj;

        protected override void OnCommit(Context context)
        {
            Player.GiftInfo = giftInfo;

            if(claimObj != null) claimObj.Commit();

            base.OnCommit(context);
        }

        protected override ErrorCode SafeProcessCommand(ClaimResponse res)
        {
            if (Player.GiftInfo == null)
                giftInfo = GiftUtils.QueryGiftInfo(PublicID);
            else
                giftInfo = Player.GiftInfo.Clone();

            var originCount = giftInfo.List.Count;

            var count = Command.Count > 0 ? Command.Count : 1;

            var items = new List<Gift>(count);

            if (Command.Serial > 0)
            {
                var item = FilterUtils.SelectFirst(giftInfo.List, o => { return o.Serial == Command.Serial; });
                if (item == null) return ErrorCode.InvalidSerial;
                items.Add(item);
            }
            else
            {
                foreach(var item in giftInfo.List)
                {
                    items.Add(item);
                    if (items.Count >= count) break;
                }
            }

            if (items.Count == 0) return ErrorCode.Success;

            count = items.Count;

            if (count > 1)
            {
                claimObj = new GiftBatchClaimObj(Player);
            }
            else
                claimObj = new GiftClaimObj(Player);

            using (var db = AcquireDB())
            {
                foreach (var item in items)
                {
                    using (var trans = db.BeginTransaction())
                    {
                        var cmd = DBCommandBuilder.DeleteGift(PublicID, item.Serial);

                        var exists = false;

                        using (var result = db.DoQuery(cmd)) exists = result.RecordsAffected > 0;

                        if (exists)
                        {
                            var errCode = claimObj.AddReward(db, item);
                            if (errCode == ErrorCode.Success)
                            {
                                trans.Commit();
                                giftInfo.List.Remove(item);
                            }
                            else
                                trans.Rollback();

                        }
                        else
                        {
                            trans.Rollback();
                            if (count == 1) return ErrorCode.InvalidSerial;
                        }
                    }
                }

                if (originCount >= 50) giftInfo = GiftUtils.QueryGiftInfo(PublicID);
            }

            if (claimObj.Rewards.Count == 0) return ErrorCode.GiftOverflow;

            claimObj.Sync(res);

            res.GiftInfo = giftInfo.Serialize();
            res.Serial = Command.Serial;
            res.List = claimObj.Rewards;

            return ErrorCode.Success;

        }

    }

    class GiftUtils
    {
        public static GiftInfo QueryGiftInfo(PublicID id)
        {
            var cmd = DBCommandBuilder.SelectGiftList(id);

            using (var db = Global.DBPool.Acquire(id.Group))
            {
                using (var result = db.DoQuery(cmd))
                {
                    return new GiftInfo(DBObjectConverter.Convert<DBGift>(result));
                }
            }
        }
    }

    interface IGiftClaim
    {
        List<Reward> Rewards { get; }
        ErrorCode AddReward(IDBConnection db, Gift gift);
        void Sync(CSAGACommand syncTarget);
        void Commit();
    }

    class GiftClaimObjBase
    {
        private int cardCount = 0;
        private int equipmentCount = 0;
        private int cardSpace = -1;
        private int equipmentSpace = -1;
        protected Player player;
        protected void Initialize(Player player)
        {
            this.player = player;
        }

        protected bool AddCard()
        {
            if (cardSpace < 0)
            {
                cardSpace = Global.Tables.CardSpaceCalc.Count(player.PurchaseInfo.Count(ArticleID.CardSpace));
                cardCount = player.CardInfo.List.Count;
            }
            if (cardCount >= cardSpace) return false;
            ++cardCount;
            return true;
        }

        protected bool AddEquipment()
        {
            if (equipmentSpace < 0)
            {
                var lv = PlayerUtils.CalcLV(player.PlayerInfo[PropertyID.Exp]);

                equipmentSpace = Global.Tables.Player[lv].n_EQUIP_SPACE;
                foreach (var item in player.EquipmentInfo.List)
                {
                    if (item.CardSerial.IsEmpty) ++equipmentCount;
                }
            }

            if (equipmentCount >= equipmentSpace) return false;

            ++equipmentSpace;

            return true;
        }
    }

    class GiftBatchClaimObj : GiftClaimObjBase, IGiftClaim
    {
        public List<Reward> Rewards { get { return rewards; } }
        private List<Reward> rewards = new List<Reward>();

        private int originCrystal;
        private int originCoin;
        private int originGem;

        private int crystalDiff;
        private int coinDiff;
        private int gemDiff;

        private List<Card> cards = new List<Card>();
        private Dictionary<int, ValuePair> lucks = new Dictionary<int, ValuePair>();

        private List<Equipment> equipments = new List<Equipment>();
        private Dictionary<int, ValuePair> items = new Dictionary<int, ValuePair>();
        private List<int> tactics = new List<int>();

        public GiftBatchClaimObj(Player player)
        {
            Initialize(player);
            originCrystal = player.PlayerInfo[PropertyID.Crystal];
            originCoin = player.PlayerInfo[PropertyID.Coin];
            originGem = player.PurchaseInfo.Gem;
        }
        public ErrorCode AddReward(IDBConnection db, Gift gift)
        {
            switch (gift.Type)
            {
                case DropType.Crystal:
                {
                    if (originCrystal + crystalDiff + gift.Value1 > Global.Tables.CrystalMax) return ErrorCode.GiftOverflow;
                    crystalDiff += gift.Value1;
                    var cmd = DBCommandBuilder.AddCrystal(player.ID, gift.Value1);
                    db.SimpleQuery(cmd);
                    break;
                }
                case DropType.Coin:
                {
                    if (originCoin + coinDiff + gift.Value1 > Global.Tables.CoinMax) return ErrorCode.GiftOverflow;
                    coinDiff += gift.Value1;
                    var cmd = DBCommandBuilder.AddCoin(player.ID, gift.Value1);
                    db.SimpleQuery(cmd);
                    break;
                }
                case DropType.Gem:
                {
                    if (originGem + gemDiff + gift.Value1 > Global.Tables.GemMax) return ErrorCode.GiftOverflow;
                    gemDiff += gift.Value1;
                    var cmd = DBCommandBuilder.InsertGem(player.ID, gift.Source, gift.Value1, Global.CurrentTime, gift.Value2);
                    db.SimpleQuery(cmd);
                    break;
                }
                case DropType.Item:
                {
                    var data = Global.Tables.Item[gift.Value2];

                    if (data.n_TYPE == ItemType.Equipment)
                    {
                        if (!AddEquipment()) return ErrorCode.GiftOverflow;

                        var obj = new Equipment { ID = gift.Value2 };
                        Calc.Equipment.Generate(obj);
                        var cmd = DBCommandBuilder.InsertEquipment(player.ID, obj.ID, obj.Hp, obj.Atk, obj.Rev, obj.Chg, obj[PropertyID.EffectID], obj[PropertyID.Counter], obj[PropertyID.CounterType]);
                        obj.Serial = db.DoQuery<Int64>(cmd);

                        equipments.Add(obj);
                    }
                    else
                    {
                        var itemCount = items.SafeGetValue(gift.Value2);

                        if (itemCount == null)
                        {
                            itemCount = new ValuePair();
                            itemCount.OriginValue = itemCount.NewValue = player.ItemInfo.Count(gift.Value2);
                            items.Add(gift.Value2, itemCount);
                        }

                        if (itemCount.NewValue + gift.Value1 > Global.Tables.ItemLimit) return ErrorCode.GiftOverflow;

                        itemCount.NewValue += gift.Value1;

                        var cmd = DBCommandBuilder.IncreaseItemCount(player.ID, gift.Value2, gift.Value1);
                        db.SimpleQuery(cmd);
                    }
                    break;
                }
                case DropType.Card:
                {
                    if (!AddCard()) return ErrorCode.GiftOverflow;

                    var lv = gift.Value1 / 1000;
                    if (lv == 0) lv = 1;

                    var rare = (gift.Value1 % 1000) / 100;

                    var exp = CardUtils.CalcExp(gift.Value2, lv);

                    var obj = new Card
                    {
                        ID = gift.Value2,
                        Exp = exp,
                        Complex = new Complex()
                    };

                    obj.Rare = rare;

                    var cmd = DBCommandBuilder.InsertCard(player.ID, obj.ID, obj.Complex.Value, obj.Exp);
                    obj.Serial = Serial.Create(db.DoQuery<Int64>(cmd));

                    cards.Add(obj);

                    var luck = lucks.SafeGetValue(obj.ID);

                    if (luck == null)
                    {
                        luck = new ValuePair();
                        luck.OriginValue = luck.NewValue = player.CardInfo.GetLuck(obj.ID);
                        lucks.Add(obj.ID, luck);
                    }
                    // 獲得卡片+1 & 額外給予幸運值
                    var luckDff = 1 + gift.Value1 % 100;
                    var origin = luck.NewValue;
                    luck.NewValue = Math.Min(luck.NewValue + luckDff, Global.Tables.Card[obj.ID].n_LUCK_MAX);
                    luckDff = luck.NewValue - origin;

                    if (luckDff > 0)
                    {
                        cmd = DBCommandBuilder.InsertGallery(player.ID, obj.ID, luckDff);
                        db.SimpleQuery(cmd);
                    }
                    break;
                }
                case DropType.Tactics:
                {
                    if (!player.TacticsInfo.Contains(gift.Value2))
                    {
                        var cmd = DBCommandBuilder.InsertTactics(player.ID, gift.Value2, Global.CurrentTime);
                        db.SimpleQuery(cmd);
                        tactics.Add(gift.Value2);
                    }
                    break;
                }
                default:
                    return ErrorCode.GiftUnknown;
            }

            rewards.Add(new Reward { Source = gift.Source, Type = gift.Type, Value1 = gift.Value1, Value2 = gift.Value2 });
            return ErrorCode.Success;
        }

        public void Sync(CSAGACommand syncTarget)
        {
            if (crystalDiff > 0) syncTarget.PlayerSync(PropertyID.Crystal, originCrystal, originCrystal + crystalDiff);
            if (coinDiff > 0) syncTarget.PlayerSync(PropertyID.Coin, originCoin, originCoin + coinDiff);
            if (gemDiff > 0) syncTarget.PlayerSync(PropertyID.Gem, originGem, originGem + gemDiff);

            if (cards.Count > 0)
            {
                if (syncTarget.CardSync.AddList == null) syncTarget.CardSync.AddList = new List<Internal.CardObj>(cards.Count);
                foreach (var item in cards) syncTarget.CardSync.AddList.Add(item);

                if (lucks.Count > 0)
                {
                    if (syncTarget.CardSync.UpdateList == null) syncTarget.CardSync.UpdateList = new List<CardDiff>(lucks.Count);

                    foreach (var item in lucks)
                    {
                        if (item.Value.Diff == 0) continue;
                        // 這裡用的不是卡片流水號,而是卡片ID,client在同步時要注意
                        syncTarget.CardSync.UpdateList.Add(CardDiff.Create(Serial.Create(item.Key), PropertyID.Luck, item.Value.OriginValue, item.Value.NewValue));
                    }
                }
            }
            if (equipments.Count > 0)
            {
                if (syncTarget.EquipmentSync.AddList == null) syncTarget.EquipmentSync.AddList = new List<Internal.EquipmentObj>(equipments.Count);
                foreach (var item in equipments) syncTarget.EquipmentSync.AddList.Add(item);
            }

            foreach (var item in items)
            {
                if (item.Value.Diff == 0) continue;
                var syncItem = new SyncItem { Diff = Diff.Create(item.Key, item.Value.OriginValue, item.Value.NewValue) };
                syncTarget.AddSyncData(syncItem);
            }

            foreach (var item in tactics)
            {
                var syncData = new SyncTactics { TacticsID = item };
                syncTarget.AddSyncData(syncData);
            }
        }

        public void Commit()
        {
            if (crystalDiff > 0)
                player.PlayerInfo[PropertyID.Crystal] += crystalDiff;

            if (coinDiff > 0)
                player.PlayerInfo[PropertyID.Coin] += coinDiff;

            if(gemDiff > 0)
                player.PurchaseInfo.AddGem(gemDiff);

            player.CardInfo.AddRange(cards);

            foreach (var item in lucks) player.CardInfo.AddLuck(item.Key, item.Value.Diff);

            foreach (var item in equipments) player.EquipmentInfo.Add(item);

            foreach (var item in items) player.ItemInfo.Add(item.Key, item.Value.Diff);

            foreach (var item in tactics) player.TacticsInfo.Add(item);
        }
    }
    class GiftClaimObj : GiftClaimObjBase, IGiftClaim
    {
        public List<Reward> Rewards { get { return rewards; } }
        private List<Reward> rewards = new List<Reward>();

        private Gift gift;

        private PropertyID propertyId = PropertyID.Invalid;
        private int origin;

        private int diff;

        private int originLuck;
        private int luckDff;
        private Card card;
        private Equipment equipment;

        public GiftClaimObj(Player player)
        {
            Initialize(player);
        }
        public void Commit()
        {
            player.GiftInfo.List.Remove(gift);

            if (diff > 0)
            {
                switch (gift.Type)
                {
                    case DropType.Crystal:
                        {
                            player.PlayerInfo[PropertyID.Crystal] = origin + diff;
                            break;
                        }
                    case DropType.Coin:
                        {
                            player.PlayerInfo[PropertyID.Coin] = origin + diff;
                            break;
                        }
                    case DropType.Gem:
                        {
                            player.PurchaseInfo.AddGem(diff);
                            break;
                        }
                    case DropType.Item:
                        {
                            player.ItemInfo.Add(gift.Value2, diff);
                            break;
                        }
                    default:
                        break;
                }
            }

            if (card != null) player.CardInfo.Add(card, luckDff);
            if (equipment != null) player.EquipmentInfo.Add(equipment);

            if (gift.Type == DropType.Tactics) player.TacticsInfo.Add(gift.Value2);
        }

        public ErrorCode AddReward(IDBConnection db, Gift gift)
        {
            this.gift = gift;

            switch (gift.Type)
            {
                case DropType.Crystal:
                    {
                        origin = player.PlayerInfo[PropertyID.Crystal];

                        if (origin + gift.Value1 > Global.Tables.CrystalMax) return ErrorCode.GiftOverflow;

                        diff = gift.Value1;

                        var cmd = DBCommandBuilder.AddCrystal(player.ID, diff);
                        db.SimpleQuery(cmd);

                        propertyId = PropertyID.Crystal;

                        break;
                    }
                case DropType.Coin:
                    {
                        origin = player.PlayerInfo[PropertyID.Coin];

                        if (origin + gift.Value1 > Global.Tables.CoinMax) return ErrorCode.GiftOverflow;
                        diff = gift.Value1;

                        var cmd = DBCommandBuilder.AddCoin(player.ID, diff);
                        db.SimpleQuery(cmd);

                        propertyId = PropertyID.Coin;
                        break;
                    }
                case DropType.Gem:
                    {
                        origin = player.PurchaseInfo.Gem;
                        diff = gift.Value1;

                        if (origin + diff > Global.Tables.GemMax) return ErrorCode.GiftOverflow;

                        var cmd = DBCommandBuilder.InsertGem(player.ID, gift.Source, diff, Global.CurrentTime, gift.Value2);
                        db.SimpleQuery(cmd);

                        propertyId = PropertyID.Gem;
                        break;
                    }
                case DropType.Item:
                    {
                        var data = Global.Tables.Item[gift.Value2];

                        if (data.n_TYPE == ItemType.Equipment)
                        {
                            if (!AddEquipment()) return ErrorCode.GiftOverflow;

                            var obj = new Equipment { ID = gift.Value2 };
                            Calc.Equipment.Generate(obj);
                            var cmd = DBCommandBuilder.InsertEquipment(player.ID, obj.ID, obj.Hp, obj.Atk, obj.Rev, obj.Chg, obj[PropertyID.EffectID], obj[PropertyID.Counter], obj[PropertyID.CounterType]);
                            obj.Serial = db.DoQuery<Int64>(cmd);

                            equipment = obj;
                        }
                        else
                        {
                            origin = player.ItemInfo.Count(gift.Value2);

                            if (origin + gift.Value1 > Global.Tables.ItemLimit) return ErrorCode.GiftOverflow;

                            diff = gift.Value1;

                            var cmd = DBCommandBuilder.IncreaseItemCount(player.ID, gift.Value2, diff);
                            db.SimpleQuery(cmd);
                        }
                        break;
                    }
                case DropType.Card:
                    {
                        if (!AddCard()) return ErrorCode.GiftOverflow;

                        var lv = gift.Value1 / 1000;
                        if (lv == 0) lv = 1;

                        var rare = (gift.Value1 % 1000) / 100;

                        var exp = CardUtils.CalcExp(gift.Value2, lv);

                        var obj = new Card
                        {
                            ID = gift.Value2,
                            Exp = exp,
                            Complex = new Complex()
                        };

                        obj.Rare = rare;

                        originLuck = player.CardInfo.GetLuck(obj.ID);
                        // 獲得卡片+1 & 額外給予幸運值
                        luckDff = 1 + gift.Value1 % 100;
                        var newLuck = Math.Min(originLuck + luckDff, Global.Tables.Card[obj.ID].n_LUCK_MAX);
                        luckDff = newLuck - originLuck;

                        var cmd = DBCommandBuilder.InsertCard(player.ID, obj.ID, obj.Complex.Value, obj.Exp);
                        obj.Serial = Serial.Create(db.DoQuery<Int64>(cmd));

                        if (luckDff > 0)
                        {
                            cmd = DBCommandBuilder.InsertGallery(player.ID, obj.ID, luckDff);
                            db.SimpleQuery(cmd);
                        }

                        card = obj;
                        break;
                    }
                case DropType.Tactics:
                    {
                        if (!player.TacticsInfo.Contains(gift.Value2))
                        {
                            var cmd = DBCommandBuilder.InsertTactics(player.ID, gift.Value2, Global.CurrentTime);
                            db.SimpleQuery(cmd);
                        }
                        break;
                    }
                default:
                    return ErrorCode.GiftUnknown;
            }

            rewards.Add(new Reward { Source = gift.Source, Type = gift.Type, Value1 = gift.Value1, Value2 = gift.Value2 });
            return ErrorCode.Success;
        }

        public void Sync(CSAGACommand syncTarget)
        {
            if (propertyId != PropertyID.Invalid)
            {
                if(diff > 0) syncTarget.PlayerSync(propertyId, origin, origin + diff);
            }
            else
            {
                if (gift.Type == DropType.Item)
                {
                    if (equipment != null)
                    {
                        if (syncTarget.EquipmentSync.AddList == null) syncTarget.EquipmentSync.AddList = new List<Internal.EquipmentObj>(1);
                        syncTarget.EquipmentSync.AddList.Add(equipment);
                    }
                    else if(diff > 0)
                    {
                        var syncItem = new SyncItem { Diff = Diff.Create(gift.Value2, origin, origin + diff) };
                        syncTarget.AddSyncData(syncItem);
                    }
                }
                else if (gift.Type == DropType.Tactics)
                {
                    var syncData = new SyncTactics { TacticsID = gift.Value2 };
                    syncTarget.AddSyncData(syncData);
                }
                else if (card != null)
                {
                    if (syncTarget.CardSync.AddList == null) syncTarget.CardSync.AddList = new List<Internal.CardObj>(1);
                    syncTarget.CardSync.AddList.Add(card);

                    if (luckDff > 0)
                    {
                        if (syncTarget.CardSync.UpdateList == null) syncTarget.CardSync.UpdateList = new List<CardDiff>(1);
                        // 這裡用的不是卡片流水號,而是卡片ID,client在同步時要注意
                        syncTarget.CardSync.UpdateList.Add(CardDiff.Create(Serial.Create(card.ID), PropertyID.Luck, originLuck, originLuck + luckDff));
                    }
                }
            }
        }
    }

  
}
