﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mikan.CSAGA.Commands.IAP;
using Mikan.Server;
using System.Security.Cryptography;

namespace Mikan.CSAGA.Server.Tasks
{
    [ActiveTask(CommandType = typeof(InitRequest))]
    public class IAPInitTask : PlayerTask<InitRequest, InitResponse>
    {
        protected override void OnCommit(Context context)
        {
            Player.IAPName = Command.IAPName;

            base.OnCommit(context);
        }
        protected override ErrorCode SafeProcessCommand(InitResponse res)
        {
            using (var db = Global.DBPool.AcquireLog())
            {
                var cmd = DBCommandBuilder.LogSignature(PublicID, UserHostAddress, Command.IAPName, CurrentTime);
                db.SafeSimpleQuery(cmd);
            }

            return ErrorCode.Success;
        }
    }


    [ActiveTask(CommandType = typeof(InfoRequest))]
    public class IAPInfoTask : PlayerTask<InfoRequest, InfoResponse>
    {
        private IAPInfo iapInfo;
        protected override void OnPrepare(Context context)
        {
            base.OnPrepare(context);
            iapInfo = Player.IAPInfo;
        }

        protected override void OnCommit(Context context)
        {
            Player.IAPInfo = iapInfo;

            base.OnCommit(context);
        }
        protected override ErrorCode SafeProcessCommand(InfoResponse res)
        {
            if (Player.Platform != Platform.iOS)
            {
                //if (string.IsNullOrEmpty(Player.IAPName) || Player.IAPName == "NotAvailable") return ErrorCode.IAPNotAvailable;
            }

            if (iapInfo == null) iapInfo = IAPUtils.QueryIAPInfo(PublicID);

            var info = new Data.IAPInfo();

            var list = new List<Data.IAPProduct>(Player.Context.IAPProductList.Count);

            foreach (var item in Player.Context.IAPProductList)
            {
                var src = item.Value;

                if (src.IsMyCard != Player.IsMyCard) continue;
                //if (!src.IsMyCard) continue;
                var bonus = src.Bonus;

                var history = default(DBIAPHistory);

                if (iapInfo.History.TryGetValue(src.ID, out history))
                {
                    if (history.ProductID == 11 && history.UpdateTime.Date != CurrentTime.Date)
                        bonus = src.Bonus;
                    else if (history.UpdateTime.Year == CurrentTime.Year && history.UpdateTime.Month == CurrentTime.Month)
                        bonus = 0;
                }

                list.Add(new Data.IAPProduct { ProductID = src.ProductID, ProductName = src.ProductName, Price = src.Price, Bonus = bonus });
            }

            info.List = list;

            res.IAPInfo = info.Serialize();

            return ErrorCode.Success;
        }
    }

    [ActiveTask(CommandType = typeof(ClaimRequest))]
    public class IAPClaimTask : PlayerTask<ClaimRequest, ClaimResponse>
    {
        private IAPInfo iapInfo;

        private Int64 receiptId;

        private int productId;

        private int gem;

        private int bonus;

        private int mycardBonus;

        private bool isCancelled = false;

        private DBMyCardTransaction mycardTrans;

        private MyCardClaimObj mycardClaimObj;

        protected override void OnPrepare(Context context)
        {
            base.OnPrepare(context);
            iapInfo = Player.IAPInfo;
        }
        protected override void OnCommit(Context context)
        {
            if (gem > 0) Player.PurchaseInfo.AddIAPGem(gem);

            iapInfo.History[productId] = new DBIAPHistory { ProductID = productId, ReceiptID = receiptId, UpdateTime = CurrentTime };

            Player.IAPInfo = iapInfo;

            base.OnCommit(context);
        }
        protected override ErrorCode SafeProcessCommand(ClaimResponse res)
        {
            var product = default(IAPProduct);

            if (!Player.Context.IAPProductList.TryGetValue(Command.ProductID, out product))
            {
                KernelService.Logger.Debug("[IAPFAIL], product not found, id = {0}", Command.ProductID);
                return ErrorCode.IAPIllegal;
            }

            productId = product.ID;

            var errCode = ErrorCode.IAPIllegal;

            switch (Player.Platform)
            {
                case Platform.Android:
                {
                    if (Command.OrderID == "MYCARD")
                        errCode = ProcessMyCardReceipt(product);
                    else
                        errCode = ProcessGooglePlayReceipt(product);
                    break;
                }
                case Platform.iOS:
                    errCode = ProcessIOSReceipt(product);
                    break;
                default:
                    KernelService.Logger.Debug("[IAPFAIL], unknown platform, platform = {0}", Player.Platform);
                    break;
            }

            if (errCode != ErrorCode.Success) return errCode;

            if (!isCancelled)
            {
                IAPUtils.AddGem(PublicID, product, receiptId, gem, bonus, mycardBonus, CurrentTime);

                if (mycardTrans != null && mycardClaimObj != null && !mycardClaimObj.IsTransComplete)
                    mycardClaimObj.CommitTrans();

                if (gem > 0)
                {
                    res.Gem = gem;
                    res.PlayerSync(PropertyID.IAPGem, Player.PurchaseInfo.IAPGem, Player.PurchaseInfo.IAPGem + gem);
                    res.PlayerSync(PropertyID.Gem, Player.PurchaseInfo.Gem, Player.PurchaseInfo.Gem + gem);
                }
            }

            res.OrderID = Command.OrderID;
            res.ProductID = Command.ProductID;

            return ErrorCode.Success;
        }

        private ErrorCode ProcessGooglePlayReceipt(IAPProduct product)
        {
            var hash = CalcHash(Command.OrderID);

            var receipt = default(DBIAPReceipt);

            using (var db = AcquireUniqueDB())
            {
                var cmd = DBCommandBuilder.SelectGooglePlayReceipt(hash);

                using (var dbRes = db.DoQuery(cmd))
                {
                    foreach (var item in DBObjectConverter.Convert<DBIAPReceipt>(dbRes))
                    {
                        if (item.OrderID != Command.OrderID) continue;
                        receipt = item;
                        receiptId = receipt.ID;
                        productId = receipt.ProductID;

                        if (productId != product.ID)
                        {
                            product = null;

                            foreach (var productItem in Player.Context.IAPProductList)
                            {
                                if (productItem.Value.ID != productId) continue;
                                product = productItem.Value;
                                break;
                            }

                            if (product == null)
                            {
                                KernelService.Logger.Debug("[IAPFAIL], receipt exists, but product not found, platform = {0}", productId);
                                return ErrorCode.IAPIllegal;
                            }
                        }
                        break;
                    }
                }
            }

            gem = CalcGem(product, receiptId, out bonus);

            if (receipt == null)
            {
                var checkRes = Player.Context.GooglePlayIAPService.Check(Command.ProductID, Command.OrderID, Command.Token);

                if (!checkRes.IsSucc)
                {
                    KernelService.Logger.Debug("[IAPFAIL], verify failed, id = {0}, order_id={1}, token={2}", Command.ProductID, Command.OrderID, Command.Token);
                    return checkRes.Timeout ? ErrorCode.IAPTimeout : ErrorCode.IAPIllegal;
                }

                if (checkRes.PurchaseState != 0) return ErrorCode.IAPCancelled;

                // 照理說只要沒在資料庫內的收據SERVER都要給寶石才對, 就先不檢查是否消費掉了
                //if (checkResult.ConsumptionState != 0) gem = 0;

                using (var db = AcquireUniqueDB())
                {
                    var cmd = DBCommandBuilder.InsertGooglePlayReceipt(PublicID, productId, Command.OrderID, hash, Command.Token, gem, bonus, CurrentTime);
                    receiptId = db.DoQuery<Int64>(cmd);
                }
            }

            return ErrorCode.Success;
        }

        private ErrorCode ProcessMyCardReceipt(IAPProduct product)
        {
            var receiptData = Player.Context.MyCardIAPService.Convert(Command.Token);

            if (!receiptData.IsSucc) return ErrorCode.IAPIllegal;

            if (receiptData.IsCancelled)
            {
                isCancelled = true;
                return ErrorCode.Success;
            }

            mycardClaimObj = new MyCardClaimObj(Player.Context.MyCardIAPService, Player.Context.IAPProductList);

            mycardTrans = mycardClaimObj.QueryTransaction(PublicID, receiptData.Serial);

            var errCode = mycardClaimObj.ProcessTransaction(mycardTrans, Command.Token, iapInfo, product);

            if (errCode == ErrorCode.Success)
            {
                receiptId = mycardClaimObj.ReceiptID;
                gem = mycardClaimObj.Gem;
                bonus = mycardClaimObj.Bonus;
                mycardBonus = mycardClaimObj.MyCardBonus;
            }

            return errCode;
        }

        private ErrorCode ProcessIOSReceipt(IAPProduct product)
        {
            var checkRes = Player.Context.iOSIAPService.Check(Convert.ToBase64String(Encoding.UTF8.GetBytes(Command.Token)), product.ProductID);

            if (!checkRes.IsSucc)
            {
                if (checkRes.IsNetFail)
                    return ErrorCode.IAPTimeout;
                else
                {
                    KernelService.Logger.Debug("[IAP_FAIL] player_id = {0}, product_id = {1}, receipt = {2}, status = {3}", PublicID.ToString(), Command.ProductID, Command.Token, checkRes.Status);
                    return ErrorCode.IAPIllegal;
                }
            }

            var hash = CalcHash(checkRes.TransactionID);
            var receipt = default(DBIAPReceipt);

            using (var db = AcquireUniqueDB())
            {
                var cmd = DBCommandBuilder.SelectIOSReceipt(hash);

                using (var dbRes = db.DoQuery(cmd))
                {
                    foreach (var item in DBObjectConverter.Convert<DBIAPReceipt>(dbRes))
                    {
                        if (item.OrderID != Command.OrderID) continue;
                        receipt = item;
                        receiptId = receipt.ID;
                        productId = receipt.ProductID;

                        if (productId != product.ID)
                        {
                            product = null;

                            foreach (var productItem in Player.Context.IAPProductList)
                            {
                                if (productItem.Value.ID != productId) continue;
                                product = productItem.Value;
                                break;
                            }

                            if (product == null) return ErrorCode.IAPIllegal;
                        }
                        break;
                    }
                }
            }

            gem = CalcGem(product, receiptId, out bonus);

            if (receipt == null)
            {
                using (var db = AcquireUniqueDB())
                {
                    var cmd = DBCommandBuilder.InsertIOSReceipt(PublicID, productId, checkRes.TransactionID, hash, Command.Token, gem, bonus, CurrentTime);
                    receiptId = db.DoQuery<Int64>(cmd);
                }
            }

            return ErrorCode.Success;
        }

        private Int64 CalcHash(string text)
        {
            using (var md5 = MD5.Create())
            {
                var bytes = md5.ComputeHash(Encoding.UTF8.GetBytes(text));

                var hashBytes = new byte[] { bytes[1], bytes[13], bytes[9], bytes[3], bytes[2], bytes[10], bytes[6], bytes[11] };
                return BitConverter.ToInt64(hashBytes, 0);
            }      
        }

        private int CalcGem(IAPProduct product, Int64 receiptId, out int bonus)
        {
            bonus = 0;

            if (iapInfo == null) iapInfo = IAPUtils.QueryIAPInfo(PublicID);

            var history = default(DBIAPHistory);

            if (iapInfo.History.TryGetValue(productId, out history))
            {
                if (receiptId != 0 && receiptId == history.ReceiptID)
                    return 0;
                else
                {

                    if (history.UpdateTime.Year != CurrentTime.Year || history.UpdateTime.Month != CurrentTime.Month)
                        bonus = product.Bonus;
                }
            }
            else
                bonus = product.Bonus;

            return product.Gem;
        }
    }

    public class IAPUtils
    {

        public static IAPInfo QueryIAPInfo(PublicID id)
        {
            var info = new IAPInfo();
            using (var db = Global.DBPool.Acquire(id.Group))
            {
                var cmd = DBCommandBuilder.SelectIAPHistory(id);

                using (var res = db.DoQuery(cmd)) info.Setup(DBObjectConverter.Convert<DBIAPHistory>(res));
            }

            return info;
        }

        public static void AddGem(PublicID id, IAPProduct product, Int64 receiptId, int gem, int bonus, DateTime timestamp)
        {
            AddGem(id, product, receiptId, gem, bonus, 0, timestamp);
        }

        public static void AddGem(PublicID id, IAPProduct product, Int64 receiptId, int gem, int bonus, int mycardBonus, DateTime timestamp)
        {
            if (gem > 0)
            {
                using (var db = Global.DBPool.Acquire(id.Group))
                {
                    using (var trans = db.BeginTransaction())
                    {
                        var cmd = DBCommandBuilder.UpdateIAPHistory(id, product.ID, receiptId, timestamp);
                        db.SimpleQuery(cmd);
                        cmd = DBCommandBuilder.InsertGem(id, RewardSource.IAP, gem, timestamp);
                        db.SimpleQuery(cmd);

                        if (bonus > 0)
                        {
                            cmd = DBCommandBuilder.InsertGift(id, RewardSource.IAPBonus, Global.Tables.GetSystemText(SystemTextID._225_MONTHLY_IAP_BONUS), DropType.Gem, bonus, product.ID);
                            db.SimpleQuery(cmd);
                        }
                        if (mycardBonus > 0)
                        {
                            cmd = DBCommandBuilder.InsertGift(id, RewardSource.MyCardBonus, Global.Tables.GetSystemText(SystemTextID._413_MYCARD_BONUS), DropType.Gem, mycardBonus, product.ID);
                            db.SimpleQuery(cmd);
                        }
                        trans.Commit();
                    }
                }

            }
        }
    }
}
