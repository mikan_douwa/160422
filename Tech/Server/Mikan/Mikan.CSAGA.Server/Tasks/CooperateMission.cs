﻿using System.Text;
using Mikan.CSAGA.Commands.CooperateMission;
using Mikan.Server;
using System.Collections.Generic;
using System;
using Mikan.CSAGA.Commands;

namespace Mikan.CSAGA.Server.Tasks
{
    abstract public class CooperateMissionTaskBase<TReq, TRes> : PlayerTask<TReq, TRes>
        where TReq : CSAGARequest
        where TRes : CSAGAResponse
    {
        protected CooperateMissionInfo cachedInfo { get; private set; }

        protected GlobalCooperateMissionInfo globalInfo { get; private set; }

        protected override void OnPrepare(Context context)
        {
            base.OnPrepare(context);

            globalInfo = context.CooperateMissionInfo;

            cachedInfo = Player.CooperateMissionInfo;
        }

        protected override void OnCommit(Context context)
        {
            Player.CooperateMissionInfo = cachedInfo;

            base.OnCommit(context);
        }

        protected CooperateMission GetMission(Int64 serial)
        {
            var info = GetMissionInfo();

            return info.Map.SafeGetValue(serial);
        }

        protected CooperateMissionInfo GetMissionInfo()
        {
            if (cachedInfo == null || cachedInfo.IsOverdue)
                cachedInfo = CooperateMissionInfo.Query(Player.ID, globalInfo.BeginSerial);

            cachedInfo = globalInfo.Clone(cachedInfo.Origin);

            return cachedInfo;
        }
    }

    [ActiveTask(CommandType = typeof(InfoRequest))]
    public class CooperateMissionInfoTask : CooperateMissionTaskBase<InfoRequest, InfoResponse>
    {
        
        protected override ErrorCode SafeProcessCommand(InfoResponse res)
        {
            var info = GetMissionInfo();

            res.CooperateMissionInfo = info.Serialize();

            return ErrorCode.Success;
        }
    }

    [ActiveTask(CommandType = typeof(ClaimRequest))]
    public class CooperateMissionClaimTask : CooperateMissionTaskBase<ClaimRequest, ClaimResponse>
    {
        private int itemCount;
        protected override void OnCommit(Context context)
        {
            Player.ItemInfo.Add(Global.Tables.CoMissionItemID, itemCount);

            base.OnCommit(context);
        }
        protected override ErrorCode SafeProcessCommand(ClaimResponse res)
        {
            var info = GetMissionInfo();

            var mission = info.Map.SafeGetValue(Command.Serial);

            if (mission == null) return ErrorCode.CoMissionIllegal;

            if (!mission.IsReached || mission.State.Progress < Global.Tables.CoMissionThreshold) return ErrorCode.CoMissionNotReach;

            if (!mission.State.IsClaimed)
            {
                var isClaimed = false;

                foreach (var item in info.List)
                {
                    if (item.BeginTime.Date == mission.BeginTime.Date && item.State.IsClaimed)
                    {
                        isClaimed = true;
                        break;
                    }
                }

                itemCount = Global.Tables.GetVariable(isClaimed ? VariableID.CO_MISSION_REPEAT : VariableID.CO_MISSION_FIRST);

                using (var db = AcquireDB())
                {
                    using (var trans = db.BeginTransaction())
                    {
                        mission.State.IsClaimed = true;
                        var cmd = DBCommandBuilder.UpdateCoMissionState(PublicID, mission.Serial, mission.State.Value);
                        db.SimpleQuery(cmd);

                        cmd = DBCommandBuilder.IncreaseItemCount(PublicID, Global.Tables.CoMissionItemID, itemCount);
                        db.SimpleQuery(cmd);

                        trans.Commit();
                    }
                }

                res.Serial = Command.Serial;
                res.Rewards = new List<Reward>(1);
                res.Rewards.Add(new Reward { Source = RewardSource.CoMission, Type = DropType.Item, Value2 = Global.Tables.CoMissionItemID, Value1 = itemCount });

                var origin = Player.ItemInfo.Count(Global.Tables.CoMissionItemID);
                res.AddSyncData(new SyncItem { Diff = Diff.Create(Global.Tables.CoMissionItemID, origin, origin + itemCount) });

                info.IsOverdue = true;
            }

            return ErrorCode.Success;
        }
    }


    [ActiveTask(CommandType = typeof(DonateRequest))]
    public class CooperateMissionDonateTask : CooperateMissionTaskBase<DonateRequest, DonateResponse>
    {
        private int itemId;
        private int count;

        protected override void OnCommit(Context context)
        {
            Player.ItemInfo.Add(itemId, -count);
            context.MissionSubjects[Command.Mission.Serial][Command.Mission.Index] += Command.Mission.Count;
            base.OnCommit(context);
        }
        protected override ErrorCode SafeProcessCommand(DonateResponse res)
        {
            var info = GetMissionInfo();

            var mission = info.Map.SafeGetValue(Command.Mission.Serial);

            if (mission == null) return ErrorCode.CoMissionIllegal;

            if (mission.BeginTime.Date < CurrentTime.Date) return ErrorCode.CoMissionOverdue;

            var subject = mission.Subjects[Command.Mission.Index];

            if (subject.Rawdata.n_TYPE != MissionType.Item) return ErrorCode.CoMissionIllegal;

            itemId = subject.Rawdata.n_ID_[0];
            var origin = Player.ItemInfo.Count(itemId);

            if (origin < Command.Mission.Count) return ErrorCode.ItemNotEnough;

            if (Player.SyncMission(Command.Mission, info))
            {
                info.IsOverdue = true;

                count = Command.Mission.Count;

                using (var db = AcquireDB())
                {
                    var cmd = DBCommandBuilder.IncreaseItemCount(PublicID, itemId, -count);
                    db.SimpleQuery(cmd);
                }

                res.AddSyncData(new SyncItem { Diff = Diff.Create(itemId, origin, origin - count) });
                res.AddSyncData(Command.Mission);
            }
            else
                return ErrorCode.CoMissionIllegal;

            return ErrorCode.Success;
        }
    }
}
