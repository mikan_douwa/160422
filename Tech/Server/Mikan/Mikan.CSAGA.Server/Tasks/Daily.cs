﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mikan.CSAGA.Commands.Daily;
using Mikan.Server;

namespace Mikan.CSAGA.Server.Tasks
{
    [ActiveTask(CommandType = typeof(InfoRequest))]
    public class DailyInfoTask : PlayerTask<InfoRequest, InfoResponse>
    {
        private DailyInfo dailyInfo;

        protected override void OnCommit(Context context)
        {
            Player.DailyInfo = dailyInfo;

            base.OnCommit(context);
        }
        protected override ErrorCode SafeProcessCommand(InfoResponse res)
        {
            dailyInfo = Player.DailyInfo.IsOverdue ? DailyUtils.QueryDailyInfo(Player) : Player.DailyInfo;

            res.DailyInfo = DailyUtils.Clone(dailyInfo).Serialize();
            
            return ErrorCode.Success;
        }
    }

    [ActiveTask(CommandType = typeof(PickRequest))]
    public class DailyPickTask : PlayerTask<PickRequest, PickResponse>
    {
        private DailyInfo dailyInfo;

        private bool hasChange;

        private int state;

        private int consumedAdd;

        private int cached;

        protected override void OnCommit(Context context)
        {
            if (hasChange)
            {
                dailyInfo.State.Value = state;
                dailyInfo.Consumed += consumedAdd;
                dailyInfo.Cached = cached;
            }

            Player.DailyInfo = dailyInfo;

            base.OnCommit(context);
        }

        protected override ErrorCode SafeProcessCommand(PickResponse res)
        {
            if (Player.DailyInfo.IsOverdue)
            {
                dailyInfo = DailyUtils.QueryDailyInfo(Player);
                cached = -1;
            }
            else
            {
                dailyInfo = Player.DailyInfo;

                if (!dailyInfo.State[Command.Index])
                {
                    var drop = default(DropObj);

                    if (dailyInfo.Cached < 0)
                    {
                        if (dailyInfo.Remains <= 0) return ErrorCode.IllegalOpertaion;

                        // 翻開第一張,只更新次數和狀態
                        state = dailyInfo.State.Combin(ComplexI32.Bit(Command.Index));
                        consumedAdd = 1;
                        cached = Command.Index;
                    }
                    else
                    {
                        // 翻開第二張,檢查獎勵是否相同
                        var drop1 = Global.Tables.Drop[dailyInfo.List[dailyInfo.Cached]];
                        var drop2 = Global.Tables.Drop[dailyInfo.List[Command.Index]];

                        if (drop1.n_DROP_TYPE == drop2.n_DROP_TYPE && drop1.n_DROP_ID == drop2.n_DROP_ID && drop1.n_COUNT == drop2.n_COUNT)
                        {
                            state = dailyInfo.State.Combin(ComplexI32.Bit(Command.Index));
                            drop = new DropObj(Player, RewardSource.DailyActive);
                            drop.Add(drop1);
                            AddAssistObj(drop);
                            res.DropID = drop1.n_ID;
                        }
                        else
                        {
                            state = dailyInfo.State.Separate(dailyInfo.Cached);
                        }

                        consumedAdd = 0;
                        cached = -1;

                    }

                    using (var db = AcquireDB())
                    {
                        using (var trans = db.BeginTransaction())
                        {
                            var cmd = DBCommandBuilder.UpdateDaily(PublicID, state, consumedAdd, cached);

                            db.SimpleQuery(cmd);

                            ApplyAssists(db);

                            trans.Commit();
                        }
                    }

                    hasChange = true;
                }
            }

            var tmp = DailyUtils.Clone(dailyInfo);
            tmp.State.Value = state;
            tmp.Consumed += consumedAdd;
            tmp.Cached = cached;
            tmp.List[Command.Index] = dailyInfo.List[Command.Index];

            if(dailyInfo.Cached > -1)
                tmp.List[dailyInfo.Cached] = dailyInfo.List[dailyInfo.Cached];

            res.DailyInfo = tmp.Serialize();

            return ErrorCode.Success;
        }
    }
}
namespace Mikan.CSAGA.Server
{
    class DailyUtils
    {
        public static DailyInfo QueryDailyInfo(Player player)
        {
            var dailyInfo = new DailyInfo();

            using (var db = Global.DBPool.Acquire(player.ID.Group))
            {
                var cmd = DBCommandBuilder.SelectDaily(player.ID);

                using (var dbRes = db.DoQuery(cmd))
                    dailyInfo.Update(DBObjectConverter.ConvertCurrent<DBDaily>(dbRes));

                if (dailyInfo.IsOverdue)
                {
                    dailyInfo.Seed = player.Context.DailyActive.NextSeed();
                    dailyInfo.UpdateTime = Global.CurrentTime;
                    dailyInfo.State.Value = 0;
                    dailyInfo.Cached = -1;
                    dailyInfo.Add = 0;
                    dailyInfo.Consumed = 0;
                    cmd = DBCommandBuilder.ResetDaily(player.ID, dailyInfo.Seed, dailyInfo.UpdateTime);
                    db.SimpleQuery(cmd);
                }

                dailyInfo.List = player.Context.DailyActive.Select(dailyInfo.Seed).ToList();
            }

            return dailyInfo;
        }

        public static DailyInfo Clone(DailyInfo source)
        {
            var dailyInfo = new DailyInfo();
            dailyInfo.Seed = source.Seed;
            dailyInfo.State.Value = source.State.Value;
            dailyInfo.Consumed = source.Consumed;
            dailyInfo.Add = source.Add;
            dailyInfo.UpdateTime = source.UpdateTime;
            dailyInfo.Cached = source.Cached;

            dailyInfo.List = new List<int>(source.List);

            for (int i = 0; i < dailyInfo.List.Count; ++i)
            {
                if (!source.State[i]) dailyInfo.List[i] = 0;
            }

            return dailyInfo;
        }
    }
}
