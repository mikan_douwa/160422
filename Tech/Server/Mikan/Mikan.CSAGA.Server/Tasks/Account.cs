﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mikan.Server;
using Mikan.CSAGA.Commands.Account;
using Mikan.CSAGA.Data;
using System.Security.Cryptography;

namespace Mikan.CSAGA.Server.Tasks
{
    [ActiveTask(CommandType = typeof(CreateRequest))]
    public class AccountCreateTask : Task<CreateRequest>
    {
        protected override ICommand SafeProcessCommand()
        {
            var response = new CreateResponse();

            DBAccount account = null;
            DBPlayer player = null;

            #region 檢查帳號是否已存在

            using (var db = AcquireUniqueDB())
            {

                var cmd = DBCommandBuilder.SelectAccount(Command.UUID);

                using (var res = db.DoQuery(cmd))
                {
                    if (!res.IsEmpty) account = DBObjectConverter.ConvertCurrent<DBAccount>(res);
                }
            }

            #endregion

            if (account == null)
            {
                using (var db = AcquireUniqueDB())
                {
                    var key = MathUtils.Random.Next();

                    while (key <= 0) key = MathUtils.Random.Next();

                    var cmd = DBCommandBuilder.CreateAccount(Command.UUID, key);

                    using (var res = db.DoQuery(cmd))
                    {
                        account = DBObjectConverter.ConvertCurrent<DBAccount>(res);
                    }
                }
            }

            if (account.SubID != 0)
            {
                response.Status = ErrorCode.AccountExists;
                return response;
            }

            var group = Global.Groups[(int)(account.ID % Global.Groups.Count)];

            using (var db = AcquireDB(group))
            {
                var cmd = DBCommandBuilder.CreatePlayer(account.ID, group);

                using (var res = db.SafeDoQuery(cmd))
                {
                    if (res.IsSucc) player = DBObjectConverter.ConvertCurrent<DBPlayer>(res);
                }

                if (player == null)
                {
                    cmd = DBCommandBuilder.SelectPlayer(account.ID, group);

                    using (var res = db.DoQuery(cmd))
                    {
                        player = DBObjectConverter.ConvertCurrent<DBPlayer>(res);

                        if (player == null)
                        {
                            response.Status = ErrorCode.AccountInvalid;
                            return response;
                        }
                    }
                }
            }

            using (var db = AcquireUniqueDB())
            {
                var cmd = DBCommandBuilder.SettleAccount(account.ID, group, player.PlayerID);

                using (var res = db.DoQuery(cmd))
                {
                    var publicId = PublicID.Create(group, player.PlayerID);

                    var profile = Profile.Create(publicId.ToString(), account.UUID, account.Key);

                    response.Profile = profile.Serialize();
                }
            }

            return response;
        }
    }

    [ActiveTask(CommandType = typeof(LoginRequest))]
    public class AccountLoginTask : Task<LoginRequest>
    {
        private DBAccount account;
        private PublicID id;
        private Token token;
        private Platform platform;
        private bool useSuperPrivilege;
        private bool isMyCard = false;
        private Dictionary<Platform, string> appVersion;
        private Dictionary<Platform, string> signature;
        private bool isMaintaining = false;

        private PVPContext pvp;

        private PVPContextList pvpList;

        protected override void OnPrepare(Context context)
        {
            base.OnPrepare(context);
            signature = context.Signature;
            appVersion = context.AppVersion;
            isMaintaining = context.IsMaintaining;
            pvpList = context.PVPList;
        }

        protected override void OnCommit(Context context)
        {
            if (id != null)
            {
                var player = context.Login(id, token, account, platform, isMyCard, useSuperPrivilege);
            }
        }

        protected override ICommand SafeProcessCommand()
        {
            switch (Command.Platform)
            {
                case "IPhonePlayer":
                    platform = Platform.iOS;
                    break;
                case "Android":
                    platform = Platform.Android;
                    break;
                case "Android.MyCard":
                    platform = Platform.Android;
                    isMyCard = true;
                    break;
                default:
                    platform = Platform.Unknown;
                    if (Command.Platform.EndsWith("MyCard")) isMyCard = true;
                        
                    break;
            }

            var res = new LoginResponse();

            using (var db = AcquireUniqueDB())
            {
                var cmd = DBCommandBuilder.SelectAccount(Command.UUID);

                using (var result = db.DoQuery(cmd))
                {
                    if (result.IsEmpty)
                    {
                        res.Status = ErrorCode.AccountNotExists;
                        return res;
                    }
                    else
                    {
                        account = DBObjectConverter.ConvertCurrent<DBAccount>(result);
                        id = PublicID.Create(account.GroupID, account.SubID);
                        // Token只要換日就無效,client會自動重新登入
#if DEBUG

                        var expire = CurrentTime.AddHours(1);
                        expire = new DateTime(expire.Year, expire.Month, expire.Day, expire.Hour, 0, 0);
                        token = id.CreateToken(expire);
#else
                        token = id.CreateToken(CurrentTime.Date.AddDays(1));
#endif
                        pvp = pvpList[id];
                    }
                }

                if (Command.UseSuperPrivilege)
                {
                    cmd = DBCommandBuilder.SelectSuperAccount(id);
                    using (var result = db.DoQuery(cmd))
                    {
                        if (!result.IsEmpty) useSuperPrivilege = true;
                    }
                }
            }

            res.Token = token.Text;

            res.ServerTime = CurrentTime;

            res.AppVersion = appVersion.SafeGetValue(platform);
            res.Signature = signature.SafeGetValue(platform);
            res.IsMaintaining = isMaintaining;

            return res;
        }
    }

    [ActiveTask(CommandType = typeof(InitRequest))]
    public class AccountInitTask : PlayerTask<InitRequest>
    {
        private PlayerObj obj;

        private EventScheduleList schedule;

        protected override bool HasPrivilege(Context context)
        {
            return true;
        }

        protected override Priority UpdatePriority { get { return Priority.Delay; } }

        protected override void OnPrepare(Context context)
        {
            base.OnPrepare(context);

            schedule = context.EventScheduleList;
        }
        protected override void OnCommit(Context context)
        {
            obj.Feedback(Player);
            
            base.OnCommit(context);
        }
        protected override ICommand SafeProcessCommand()
        {
            //Player.AccountInfo.Key
            using (var db = AcquireDB()) obj = PlayerObj.Create(PublicID, db, schedule);

            obj.CheckBlackGem(PublicID,Player.Context.BlackGem);

            return new InitResponse();
        }
    }

    [ActiveTask(CommandType = typeof(PublishRequest))]
    public class AccountPublishTask : PlayerTask<PublishRequest, PublishResponse>
    {
        protected override ErrorCode SafeProcessCommand(PublishResponse res)
        {
            var cache = default(DBTransferCache);

            using (var db = AcquireUniqueDB())
            {
                var cmd = DBCommandBuilder.SelectTransferCache(PublicID);

                using (var dbRes = db.DoQuery(cmd)) cache = DBObjectConverter.ConvertCurrent<DBTransferCache>(dbRes);
            }

            if (cache != null && cache.Status != 0 && cache.Timestamp.Date == CurrentTime.Date) return ErrorCode.AcccountPublishCD;

            var key = 0;

            var currentTime = CurrentTime;

            if (cache == null || cache.Timestamp.Date < currentTime.Date)
            {
                key = MathUtils.Random.Next();

                while (key <= 0 || key == Player.AccountInfo.Key) key = MathUtils.Random.Next();

                using (var db = AcquireUniqueDB())
                {
                    var cmd = cache == null ? DBCommandBuilder.InsertTransferCache(PublicID, Player.AccountInfo.UUID, key, currentTime) : DBCommandBuilder.UpdateTransferCache(PublicID, key, currentTime);

                    db.SimpleQuery(cmd);
                }
            }
            else
                key = cache.Key;

            res.Token = AccountTransfer.Generate(PublicID, Player.AccountInfo.UUID, key, currentTime);

            return ErrorCode.Success;
        }
    }

    [ActiveTask(CommandType = typeof(TransferBeginRequest))]
    public class AccountTransferBeginTask : Task<TransferBeginRequest>
    {
        
        protected override ICommand SafeProcessCommand()
        {
            var res = new TransferBeginResponse();

            var id = PublicID.FromString(Command.ID);

            if (id == null || !id.IsValid)
            {
                res.Status = ErrorCode.AccountInvalid;
                return res;
            }

            var cache = default(DBTransferCache);

            using (var db = AcquireUniqueDB())
            {
                var cmd = DBCommandBuilder.SelectTransferCache(id);

                using (var dbRes = db.DoQuery(cmd)) cache = DBObjectConverter.ConvertCurrent<DBTransferCache>(dbRes);
            }

            if (cache == null || cache.Status != 0 || AccountTransfer.Generate(id, cache.UUID, cache.Key, cache.Timestamp) != Command.Token)
            {
                res.Status = ErrorCode.AccountTransferFail;
                return res;
            }

            using (var db = AcquireDB(id.Group))
            {
                var cmd = DBCommandBuilder.SelectPlayer(id);
                using (var dbRes = db.DoQuery(cmd))
                {
                    var obj = DBObjectConverter.ConvertCurrent<DBPlayer>(dbRes);
                    res.Name = obj.Name;
                    res.Rank = PlayerUtils.CalcLV(obj.Exp);
                }

                #region 取回消費相關資訊
                var purchaseInfo = new PurchaseInfo();

                cmd = DBCommandBuilder.SelectPurchase(id);
                using (var dbRes = db.DoQuery(cmd))
                {
                    var list = DBObjectConverter.Convert<DBPurchase>(dbRes);
                    purchaseInfo = new PurchaseInfo();
                    purchaseInfo.Setup(list);
                }

                cmd = DBCommandBuilder.SelectGem(id);
                using (var dbRes = db.DoQuery(cmd)) purchaseInfo.AddGem(Decimal.ToInt32(dbRes.Field<Decimal>()));

                res.Gem = purchaseInfo.Gem;
                #endregion
            }

            var key = MathUtils.Random.Next(1, int.MaxValue);
            using (var db = AcquireUniqueDB())
            {
                var cmd = DBCommandBuilder.UpdateTransferCacheTransferKey(id, key);

                db.SimpleQuery(cmd);
            }

            var profile = Profile.Create(Command.ID, cache.UUID, cache.Key);

            res.Profile = profile.Serialize();
            res.Key = key;

            return res;
        }
    }

    [ActiveTask(CommandType = typeof(TransferEndRequest))]
    public class AccountTransferEndTask : Task<TransferEndRequest>
    {
        private PublicID id;

        protected override void OnCommit(Context context)
        {
            if (id != null) context.Logout(id);
        }
        protected override ICommand SafeProcessCommand()
        {
            var res = new TransferEndResponse();

            var id = PublicID.FromString(Command.ID);

            if (id == null || !id.IsValid)
            {
                res.Status = ErrorCode.AccountTransferFail;
                return res;
            }

            var cache = default(DBTransferCache);

            using (var db = AcquireUniqueDB())
            {
                var cmd = DBCommandBuilder.SelectTransferCache(id);

                using (var dbRes = db.DoQuery(cmd)) cache = DBObjectConverter.ConvertCurrent<DBTransferCache>(dbRes);
            }

            if (cache == null || cache.TransferKey != Command.Key || AccountTransfer.Generate(id, cache.UUID, cache.Key, cache.Timestamp) != Command.Token)
            {
                res.Status = ErrorCode.AccountTransferIllegal;
                return res;
            }

            if (cache.Status == 0)
            {
                using (var db = AcquireUniqueDB())
                {
                    using (var trans = db.BeginTransaction())
                    {
                        var cmd = DBCommandBuilder.UpdateTransferCacheStatus(id, 1);
                        db.SimpleQuery(cmd);

                        cmd = DBCommandBuilder.TransferAccount(cache.UUID, cache.Key);
                        db.SimpleQuery(cmd);

                        trans.Commit();
                    }
                }
            }

            this.id = id;

            return res;
        }
    }
}

namespace Mikan.CSAGA.Server.Tasks
{
    public class PlayerObj
    {
        private PlayerInfo playerInfo;
        private PurchaseInfo purchaseInfo;
        private CardInfo cardInfo;
        private ItemInfo itemInfo;
        private EquipmentInfo equipmentInfo;
        private QuestInfo questInfo;
        private TrophyInfo trophyInfo;
        private FaithInfo faithInfo;
        private TacticsInfo tacticsInfo;
        private DateTime lastPlayTime;

        private bool isSlim = false;

        public static PlayerObj Create(PublicID id, IDBConnection db, EventScheduleList schedule)
        {
            var obj = new PlayerObj();

            obj.DoQuery(id, db, schedule);

            return obj;
        }

        public static PlayerObj Create(int group, DBPlayer dbplayer, IDBConnection db)
        {
            var obj = new PlayerObj();

            obj.DoQuery(group, dbplayer, db);

            return obj;
        }

        public void Feedback(Player target)
        {
            target.PlayerInfo = playerInfo;
            target.LastPlayTime = lastPlayTime;
            target.CardInfo = cardInfo;
            target.ItemInfo = itemInfo;
            target.EquipmentInfo = equipmentInfo;
            target.PurchaseInfo = purchaseInfo;
            target.QuestInfo = questInfo;
            target.TrophyInfo = trophyInfo;
            target.FaithInfo = faithInfo;
            target.TacticsInfo = tacticsInfo;
            target.DailyInfo = new DailyInfo();
        }

        public void CheckBlackGem(PublicID id, Dictionary<PublicID, DBBlackGem> list)
        {
            if (purchaseInfo.OriginIAPGem == 0) return;

            var blackGem = list.SafeGetValue(id);
            if (blackGem != null) purchaseInfo.SetBlackGem(blackGem.Value);
        }

        private void DoQuery(PublicID id, IDBConnection db, EventScheduleList schedule)
        {
            var currentTime = Global.CurrentTime;

            isSlim = false;

            #region 取回角色基本資訊
            var cmd = DBCommandBuilder.SelectPlayer(id);
            using (var res = db.DoQuery(cmd))
            {
                var obj = DBObjectConverter.ConvertCurrent<DBPlayer>(res);
                playerInfo = new PlayerInfo();
                playerInfo.Setup(obj);
                lastPlayTime = obj.LastPlayTime;
            }

            #endregion

            #region 取回卡片列表
            cmd = DBCommandBuilder.SelectCardList(id);
            using (var res = db.DoQuery(cmd))
            {
                var list = DBObjectConverter.Convert<DBCard>(res);
                cardInfo = new CardInfo();
                cardInfo.Setup(list);
            }
            cmd = DBCommandBuilder.SelectCardEventCacheList(id);
            using (var res = db.DoQuery(cmd))
                cardInfo.Setup(DBObjectConverter.Convert<DBCardEventCache>(res));

            cmd = DBCommandBuilder.SelectGallery(id);
            using (var res = db.DoQuery(cmd))
                cardInfo.Setup(DBObjectConverter.Convert<DBGallery>(res));

            cmd = DBCommandBuilder.SelectCardSellCache(id);
            using (var res = db.DoQuery(cmd))
            {
                if (!res.IsEmpty) cardInfo.SellCacheList = DBObjectConverter.Convert<DBSellCache>(res).ToList();
            }
            #endregion

            #region 取回道具列表
            cmd = DBCommandBuilder.SelectItemList(id);
            using (var res = db.DoQuery(cmd))
            {
                var list = DBObjectConverter.Convert<DBItem>(res);
                itemInfo = new ItemInfo();
                itemInfo.Setup(list);
            }
            #endregion

            #region 取回裝備列表
            cmd = DBCommandBuilder.SelectEquipmentList(id);
            using (var res = db.DoQuery(cmd))
            {
                var list = DBObjectConverter.Convert<DBEquipment>(res);
                equipmentInfo = new EquipmentInfo();
                equipmentInfo.Setup(list);
            }
            #endregion

            #region 取回消費相關資訊
            cmd = DBCommandBuilder.SelectPurchase(id);
            using (var res = db.DoQuery(cmd))
            {
                var list = DBObjectConverter.Convert<DBPurchase>(res);
                purchaseInfo = new PurchaseInfo();
                purchaseInfo.Setup(list);
            }

            cmd = DBCommandBuilder.SelectGem(id);
            using (var res = db.DoQuery(cmd)) purchaseInfo.AddGem(Decimal.ToInt32(res.Field<Decimal>()));
            cmd = DBCommandBuilder.SelectIAPGem(id);
            using (var res = db.DoQuery(cmd)) purchaseInfo.SetIAPGem(Decimal.ToInt32(res.Field<Decimal>()));

            #endregion

            #region 取回任務資訊
            cmd = DBCommandBuilder.SelectQuestList(id);
            using (var res = db.DoQuery(cmd))
            {
                var list = DBObjectConverter.Convert<DBQuest>(res);
                questInfo = new QuestInfo();
                questInfo.Setup(list);
            }
            cmd = DBCommandBuilder.SelectLimitedQuestList(id);
            using (var res = db.DoQuery(cmd))
            {
                var list = DBObjectConverter.Convert<DBLimitedQuest>(res);
                questInfo.Setup(list);
            }
            cmd = DBCommandBuilder.SelectQuestCache(id);
            using (var res = db.DoQuery(cmd))
            {
                var list = DBObjectConverter.Convert<DBQuestCache>(res);
                questInfo.Setup(list);
            }

            cmd = DBCommandBuilder.SelectRescue(id);
            using (var res = db.DoQuery(cmd))
                questInfo.Setup(DBObjectConverter.ConvertCurrent<DBRescue>(res));

            cmd = DBCommandBuilder.SelectInfinity(id);
            using (var res = db.DoQuery(cmd))
                questInfo.Setup(DBObjectConverter.Convert<DBInfinity>(res), schedule);

            cmd = DBCommandBuilder.SelectCrusadeCache(id, true);
            using (var res = db.DoQuery(cmd))
                questInfo.Setup(DBObjectConverter.Convert<DBCrusadeCache>(res));

            questInfo.Crusade = Crusade.Query(db, id, schedule.Crusade, currentTime);

            #endregion
            
            #region 取回成就資訊
            cmd = DBCommandBuilder.SelectTrophyList(id);
            using (var res = db.DoQuery(cmd))
            {
                trophyInfo = new TrophyInfo();
                trophyInfo.Add(DBObjectConverter.Convert<DBTrophy>(res), schedule);
            }
            cmd = DBCommandBuilder.SelectSimpleTrophyList(id);
            using (var res = db.DoQuery(cmd)) 
                trophyInfo.Add(DBObjectConverter.Convert<DBSimpleTrophy>(res));
            #endregion

            #region 取回信仰資訊
            faithInfo = new FaithInfo();

            cmd = DBCommandBuilder.SelectFaithList(id);
            using (var res = db.DoQuery(cmd))
                faithInfo.Setup(DBObjectConverter.Convert<DBFaith>(res));
            cmd = DBCommandBuilder.SelectFaithClaim(id);
            using (var res = db.DoQuery(cmd))
            {
                if (!res.IsEmpty)
                {
                    faithInfo.Origin = DBObjectConverter.ConvertCurrent<DBFaithClaim>(res);
                    faithInfo.ClaimTime = faithInfo.Origin.ClaimTime;
                }
                else
                    faithInfo.ClaimTime = DateTime.MinValue;

            }

            #endregion

            #region 取回戰術資訊
            tacticsInfo = new TacticsInfo();
            cmd = DBCommandBuilder.SelectTacticsList(id);
            using (var res = db.DoQuery(cmd))
                tacticsInfo.Setup(DBObjectConverter.Convert<DBTactics>(res));
            #endregion

            /*

            #region 取回本地資料
            cmd = DBCommandBuilder.SelectLocalDataList(id);
            using (var res = db.DoQuery(cmd)) localDataInfo = new LocalDataInfo(DBObjectConverter.Convert<DBLocalData>(res));
            #endregion

            #region 取回每日登入資訊
            cmd = DBCommandBuilder.SelectDailyLogin(id);
            using (var res = db.DoQuery(cmd)) dailyLogin = DBObjectConverter.ConvertCurrent<DBDailyLogin>(res);
            // 要等創角才會有資料
            if (dailyLogin == null) dailyLogin = new DBDailyLogin { LastLogin = Global.CurrentTime, TotalDays = 1, Days = 1, PlayerID = id.Serial };
            #endregion
            */
        }

        private void DoQuery(int group, DBPlayer dbPlayer, IDBConnection db)
        {
            isSlim = true;

            var id = PublicID.Create(group, dbPlayer.PlayerID);

            playerInfo = new PlayerInfo();
            playerInfo.Setup(dbPlayer);
            lastPlayTime = dbPlayer.LastPlayTime;
            /*
            var cmd = DBCommandBuilder.SelectTeamList(id);

            using (var res = db.DoQuery(cmd)) teamInfo = new TeamInfo(DBObjectConverter.Convert<DBTeam>(res));

            var cardSerial = teamInfo[dbPlayer.MainTeam].Members[0];

            cmd = DBCommandBuilder.SelectCard(id, cardSerial);

            using (var res = db.DoQuery(cmd)) cardInfo = new CardInfo(DBObjectConverter.Convert<DBCard>(res));

            cmd = DBCommandBuilder.SelectEquipmentList(id, cardSerial);
            using (var res = db.DoQuery(cmd)) itemInfo = new ItemInfo(null, DBObjectConverter.Convert<DBEquipment>(res));
            */
        }
    }

    public class AccountTransfer
    {
        public static string Generate(PublicID id, string uuid, int key, DateTime time)
        {
            var text = string.Format("{0}#{1}&{2}-{3}{4:00}{5:00}", id.ToString(), uuid, key ^ 563859, time.Year, time.Month, time.Day);

            using (var md5 = MD5.Create())
            {
                var hash = md5.ComputeHash(Encoding.UTF8.GetBytes(text));
                var shorten = new byte[] { hash[1], hash[9], hash[0], hash[3], hash[15], hash[10], hash[3], hash[7], hash[5] };
                return Convert.ToBase64String(shorten).Replace('+', 'u').Replace('/', '0');
            }
        }
    }
}