﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mikan.CSAGA.Server
{
    public partial class Context
    {
        public string DataPath;

        private Dictionary<PublicID, Player> players;

        public HashSet<string> AvailableAPKList = new HashSet<string>();

        public HashSet<PublicID> BlackList;

        public Service Owner;

        public Dictionary<Platform, string> Signature = new Dictionary<Platform, string>();

        public Dictionary<Platform, string> AppVersion = new Dictionary<Platform, string>();

        public Dictionary<PublicID, DBBlackGem> BlackGem;

        public bool IsMaintaining;

        private bool isInitialized = false;

        public Context()
        {
            players = PublicID.CreateDictinary<Player>();

            SetupSyncManager();
        }

        public int PlayerCount { get { return players.Count; } }

        public PublicID GetPlayerID(int index)
        {
            return players.ElementAt(index).Key;
        }

        public bool IsInitialized
        {
            get
            {
                return true;
            }
        }

        public void Setup()
        {
            BlackGem = PublicID.CreateDictinary<DBBlackGem>();

            SetupIAP();
            SetupGacha();
            SetupDataCache();
            SetupEventSchedule();

            SetupCrusade();
            SetupChallenge();
            SetupPVP();
        }

        public void Clear()
        {
            if (GooglePlayIAPService != null)
            {
                GooglePlayIAPService.Dispose();
                GooglePlayIAPService = null;
            }
        }

        public void ClearIdlePlayer()
        {
            var now = Global.CurrentTime;

            var overdueList = new List<PublicID>();

            var allowCount = players.Count - 200;

            foreach (var item in players)
            {
                if (overdueList.Count > allowCount) break;
                // 超過3小時沒動作的玩家就把資料移掉
                if (!item.Value.IsBusy && item.Value.LastUse.AddHours(3) < now) overdueList.Add(item.Key);
            }

            foreach (var item in overdueList) players.Remove(item);
        }

        public Player Login(PublicID id, Token token, DBAccount account, Platform platform, bool isMyCard, bool useSuperPrivilege)
        {
            Owner.RegisterKey(id, account.Key);

            Player oldData = null;

            if (players.TryGetValue(id, out oldData)) oldData.Owner = null;

            var player = new Player();

            player.ID = id;
            player.Context = this;
            player.Platform = platform;
            player.IsMyCard = isMyCard;
            player.AccountInfo = account;
            player.Token = token;
            player.LastUse = DateTime.Now;
            player.UseSuperPrivilege = useSuperPrivilege;
            player.Init();

            players[id] = player;

            return player;
        }

        public void Logout(PublicID id)
        {
            players.Remove(id);
        }

        public Player GetPlayer(PublicID id)
        {
            Player player = null;

            if (players.TryGetValue(id, out player)) return player;

            return null;
        }

        public Player GetPlayer(Token token)
        {
            if (token == null) return null;

            Player player = null;

            if (players.TryGetValue(token.ID, out player))
            {
                if (player.Token.Text == token.Text) return player;
            }

            return null;
        }
    }
}
