﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mikan.CSAGA.Server
{
    public partial class Context
    {
        public Dictionary<int, List<int>> ChallengeList = new Dictionary<int, List<int>>();

        public Dictionary<int, List<int>> QuestGroup = new Dictionary<int, List<int>>();

        private void SetupChallenge()
        {
            foreach (var item in Global.Tables.Challenge.Select())
            {
                var list = ChallengeList.SafeGetValue(item.n_STAGEID);
                if (list == null)
                {
                    list = new List<int>(3);
                    ChallengeList.Add(item.n_STAGEID, list);
                }
                list.Add(item.n_ID);
            }

            foreach (var item in Global.Tables.QuestDesign.Select())
            {
                var list = QuestGroup.SafeGetValue(item.n_MAIN_QUEST);
                if (list == null)
                {
                    list = new List<int>();
                    QuestGroup.Add(item.n_MAIN_QUEST, list);
                }
                list.Add(item.n_ID);
            }
        }
    }
}
