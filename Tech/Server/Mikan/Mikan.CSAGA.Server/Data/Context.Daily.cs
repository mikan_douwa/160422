﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mikan.Server;

namespace Mikan.CSAGA.Server
{
   

    public partial class Context
    {
        public DailyActive DailyActive;
        private void SetupDaily(IDBConnection db, IEnumerable<DBDataCache> list)
        {
            DailyActive = new DailyActive();

            DailyActive.Add(list);

            var dataList = Global.Tables.Drop.Select(o => { return o.n_GROUP == Global.Tables.DailyActiveDropGroup || o.n_GROUP == Global.Tables.DailyActiveResidentDropGroup; }).ToList();

            if (!DailyActive.Equals(dataList))
            {
                var cmd = DBCommandBuilder.InsertSignature(SignatureType.DropCache, Global.CurrentTime);
                var signature = db.DoQuery<int>(cmd);

                foreach (var item in dataList)
                {
                    cmd = DBCommandBuilder.InsertDataCache(signature, item.n_ID, item.n_VALUE);
                    // id+dataid=PK,失敗就表示有另一台server也正在塞,不予理會
                    db.SafeSimpleQuery(cmd);

                    DailyActive.Add(signature, item.n_ID, item.n_VALUE);
                }
            }
        }
    }

    public class DailyActive : DataCacheList
    {
        const int RESIDENT = 1;
        public override IEnumerable<int> Select(long seed)
        {
            var results = new List<int>();

            var id = (int)(seed >> 32);
            var N0 = (int)(seed & int.MaxValue);

            Cache cache = null;

            Cache resident = null;

            if (list.TryGetValue(id, out cache))
            {
                resident = cache[RESIDENT];

                var residentCount = resident != null ? resident.Default.Count : 0;
                if (cache.Default.Total > 0)
                {
                    while (true)
                    {
                        N0 = MathUtils.Seed.Next(N0);

                        var rand = N0 % cache.Default.Total;

                        var dataId = cache.Default.Get(rand);
                        if (dataId > 0)
                        {
                            results.Add(dataId);
                            if (results.Count >= (Global.Tables.DailyActivePairCount - residentCount)) break;
                        }
                    }
                }
            }
            else
                resident = cache[RESIDENT];


            if (resident != null)
            {
                foreach (var item in resident.Default.Select())
                {
                    N0 = MathUtils.Seed.Next(N0);
                    var rand = N0 % results.Count;
                    results.Insert(rand, item);

                    if (results.Count >= Global.Tables.DailyActivePairCount) break;
                }
            }

            var copy = new List<int>(results);

            foreach (var item in copy)
            {
                N0 = MathUtils.Seed.Next(N0);
                var rand = N0 % results.Count;
                results.Insert(rand, item);
            }

            return results;
        }

        public bool Equals(List<ConstData.Drop> newList)
        {
            if (CurrentID == 0) return false;

            var current = list.SafeGetValue(CurrentID);

            if (current == null) return false;

            var resident = current[RESIDENT];

            return Equals(resident != null ? resident.Default : DataCache.Empty, newList, Global.Tables.DailyActiveResidentDropGroup) && Equals(current.Default, newList, Global.Tables.DailyActiveDropGroup);
        }

        private bool Equals(DataCache current, List<ConstData.Drop> newList, int group)
        {
            var itemCount = 0;

            foreach (var item in newList)
            {
                if (item.n_GROUP == group)
                {
                    ++itemCount;
                    if (!current.Equals(item.n_ID, item.n_VALUE)) return false;
                }
            }

            return itemCount == current.Count;
        }

        protected override void Add(DataCacheList.Cache cache, int dataId, int value)
        {
            var data = Global.Tables.Drop[dataId];

            if (data.n_GROUP == Global.Tables.DailyActiveResidentDropGroup)
                Add(cache, RESIDENT, dataId, value);
            else if (data.n_GROUP == Global.Tables.DailyActiveDropGroup)
                base.Add(cache, dataId, value);
        }
    }
}
