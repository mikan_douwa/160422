﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mikan.CSAGA.Server
{
    public partial class Context
    {
        public QuestScheduleList QuestScheduleList;
    }

    public class QuestSchedule
    {
        public int QuestID;
        public DateTime BeginTime;
        public DateTime EndTime;
    }

    public class QuestScheduleList
    {
        public Dictionary<int, QuestSchedule> List = new Dictionary<int, QuestSchedule>();
        public DateTime Date;

        public bool IsAvailable(int questId, DateTime currentTime)
        {
            var quest = List.SafeGetValue(questId);
            if (quest == null) return false;
            return currentTime >= quest.BeginTime && currentTime <= quest.EndTime.AddMinutes(3);
        }

        public static QuestScheduleList Generate(DateTime currentTime)
        {
            var list = new QuestScheduleList();

            list.Date = currentTime.Date;

            var beginTime = currentTime;
            var endTime = currentTime;

            foreach (var item in Global.Tables.EventSchedule.Select(o => { return o.n_TYPE == EventScheduleType.Quest; }))
            {
                if (!Calc.Quest.ParseSchedulePeriod(item, currentTime, ref beginTime, ref endTime)) continue;

                if (currentTime.AddDays(1) < beginTime || currentTime > endTime) continue;

                foreach (var sub in Global.Tables.QuestDesign.Select(o => { return o.n_MAIN_QUEST == item.n_QUESTID; }))
                {
                    var obj = new QuestSchedule { QuestID = sub.n_ID, BeginTime = beginTime, EndTime = endTime };


                    list.List.Add(obj.QuestID, obj);
                }

            }

            return list;
        }
    }
}
