﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mikan.CSAGA.Server
{
    public partial class Context
    {
        public List<LogQuest> LogQuestList = new List<LogQuest>();

        public void LogQuest(int questId, LogQuestType type)
        {
            LogQuestList.Add(new LogQuest { QuestID = questId, Type = type });
        }
    }

    public class LogQuest
    {
        public int QuestID;
        public LogQuestType Type;

    }
    public enum LogQuestType
    {
        Play,
        Clear,
        Continue
    }


    public class LogQuestPrepareSchedule : ScheduleTask
    {
        public Service Owner;
        public override void Execute(int times)
        {
            Owner.AddKernelEvent(new LogQuestPrepareTask());
        }
    }

    public class LogQuestPrepareTask : Tasks.KernelTask
    {
        public override void Execute(Context context)
        {
            if (context.LogQuestList.Count == 0) return;
            var list = context.LogQuestList;
            context.LogQuestList = new List<LogQuest>();
            context.Owner.AddTask(new LogQuestTask { List = list });
        }
    }

    public class LogQuestTask : ITask
    {
        public List<LogQuest> List;
        public void Execute()
        {
            var quests = new Dictionary<int, Quest>();
            
            foreach (var item in List)
            {
                var quest = DictionaryUtils.SafeGetValue(quests, item.QuestID);
                switch (item.Type)
                {
                    case LogQuestType.Play:
                        ++quest.PlayCount;
                        break;
                    case LogQuestType.Clear:
                        ++quest.ClearCount;
                        break;
                    case LogQuestType.Continue:
                        ++quest.ContinueCount;
                        break;
                }
            }

            using (var db = Global.DBPool.AcquireLog())
            {
                foreach (var item in quests)
                {
                    var cmd = DBCommandBuilder.LogQuest(item.Key, item.Value.PlayCount, item.Value.ClearCount, item.Value.ContinueCount, Global.CurrentTime);
                    db.SafeSimpleQuery(cmd);
                }
            }
           
        }

        public void Dispose() { }

        class Quest
        {
            public int PlayCount = 0;
            public int ClearCount = 0;
            public int ContinueCount = 0;
        }
    }

    public class AddLogTask : ITask
    {
        public PublicID PublicID;
        public LogType Type;
        public int[] Params;

        public void Execute()
        {
            var beginTime = new DateTime(2017, 2, 8, 16, 0, 0);
            var endTime = new DateTime(2017, 2, 28, 23, 59, 59);
            
            var currentTime = Global.CurrentTime;

            if (currentTime < beginTime || currentTime > endTime) return;

            var point = 0;

            switch (Type)
            {
                case LogType.Quest:
                {
                    var questId = Params[0];
                    point = Global.Tables.QuestDesign[questId].n_LEVEL;
                    break;
                }
                case LogType.Gift:
                {
                    var kizuna = Params[0];
                    point = kizuna * 3;
                    break;
                }
                case LogType.Gacha:
                {
                    var count = Params[0];
                    point = count * 10000;
                    break;
                }
                case LogType.Shop:
                {
                    point = 300;
                    break;
                }
                case LogType.Sell:
                {
                    foreach (var id in Params)
                    {
                        var data = Global.Tables.Item[id];
                        if (data.n_RARE == 5)
                            point += 1500;
                        else if (data.n_RARE == 4)
                            point += 300;
                    }
                    break;
                }
                case LogType.Faith:
                {
                    var total = 0;
                    foreach (var count in Params) total += count;
                    point = total * 5;
                    break;
                }
            }

            if (point == 0) return;

            using (var db = Global.DBPool.AcquireLog())
            {
                var cmd = DBCommandBuilder.LogEvent(PublicID, 3, point, currentTime);
                db.SafeSimpleQuery(cmd);
            }
        }

        public void Dispose() { }
    }

    public class SyncLogSchedule : ScheduleTask
    {
        public override void Execute(int times)
        {
            /*
            using (var db = Global.DBPool.AcquireLog())
            {
                var cmd = DBCommandBuilder.SyncLogEvent(2);
                db.SafeSimpleQuery(cmd);
            }
            */
        }
    }

    public enum LogType
    {
        Quest,
        Gift,
        Gacha,
        Shop,
        Sell,
        Faith,
    }


}
