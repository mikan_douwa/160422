﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mikan.CSAGA.Server
{
    public partial class Context
    {
        public PVPContextList PVPList;

        private void SetupPVP()
        {
            PVPList = PVPContextList.Generate(EventScheduleList, Global.CurrentTime);
        }
    }

    public class PVPWave : Internal.PVPWave<Card, Equipment>
    {
        public void ChangeCard(Player player, Serial serial)
        {
            if (Card != null && Card.Serial == serial) return;

            Card = player.CardInfo[serial.Value];
            Equipments = new List<Equipment>(player.EquipmentInfo.List.FindAll((o) => o.Serial == serial.Value));
        }
        public PVPWave Clone()
        {
            var obj = new PVPWave();
            obj.Card = Card;
            obj.Equipments = Equipments;
            obj.Skills = Skills;
            obj.Selections = Selections;
            obj.Prologs = Prologs;

            return obj;
        }
    }

    public class PVPWaves : SerializableObject
    {
        public List<PVPWave> List;

        protected override void Serialize(IOutput output)
        {
            output.Write(List);
        }

        protected override void DeSerialize(IInput input)
        {
            List = input.ReadList<PVPWave>();
        }

        public PVPWaves Clone()
        {
            return new PVPWaves { List = new List<PVPWave>(List) };
        }
    }

    public class PVPContext
    {
        public DBPVPContext Origin;
        public PVPWaves Waves;

        public Int64 EventID;

        public int CardID;

        public int Point { get; private set; }

        public bool IsActive { get; private set; }

        public bool IsOverdue;

        public void Setup(DBPVPContext origin)
        {
            Origin = origin;
            Waves = new PVPWaves();
            Waves.DeSerialize(origin.QuestDesign);
            CardID = origin.CardID;
        }

        public void Setup(DBPVPIdlePlayer origin)
        {
            Waves = new PVPWaves();
            Waves.DeSerialize(origin.QuestDesign);
            CardID = origin.CardID;
        }

        public void AddPoint(Int64 eventId, int point)
        {
            if (EventID != eventId)
            {
                EventID = eventId;
                Point = point;
            }
            else
            {
                Point += point;
            }

            IsActive = true;
        }
    }

    public class PVPContextList
    {
        private object lckObj = new object();
        private Dictionary<PublicID, PVPContext> list;
        private Dictionary<PublicID, PVPContext> idleList;
        private Dictionary<PublicID, PVPContext> dummyList;
        private List<PublicID> dummys;
        private EventScheduleObj schedule;
        private List<PublicID> players;
        public Int64 EventID { get { return schedule != null ? schedule.Relative.ID : 0; } }

        public PVPContext GetUnsafe(PublicID id)
        {
            var obj = list.SafeGetValue(id);

            if (obj == null) obj = idleList.SafeGetValue(id);
            if (obj == null) obj = dummyList.SafeGetValue(id);

            return obj;
        }

        public PVPContext this[PublicID id]
        {
            get
            {
                if (id == null || !id.IsValid) return null;

                lock (lckObj) return GetUnsafe(id);
            }
            set
            {
                lock (lckObj) list[id] = value;
            }
        }

        public bool Get(PublicID id, Action<PVPContext> callback)
        {
            lock (lckObj)
            {
                var obj = GetUnsafe(id);

                if (obj == null) return false;
                
                callback(obj);
            }

            return true;
        }

        public int GetPoint(PublicID id)
        {
            lock (lckObj)
            {
                var obj = GetUnsafe(id);
                if (obj == null) return 0;
                return obj.Point;
            }
        }

        public PVPInfo Convert(PublicID id, PVPInfo refer)
        {
            var data = this[id];

            if (data == null) return null;

            var info = new PVPInfo();

            lock (lckObj) info.Setup(data);

            if (refer == null)
            {

                using (var db = Global.DBPool.Acquire(id.Group))
                {
                    var cmd = DBCommandBuilder.SelectPVPSkillList(id);
                    using (var dbRes = db.DoQuery(cmd))
                    {
                        foreach (var item in DBObjectConverter.Convert<DBPVPSkill>(dbRes)) info.AddSkill(item);
                    }
                }
            }
            else
            {
                info.Skills = refer.Skills;
                info.SkillLevels = refer.SkillLevels;
                info.BPConsumed = refer.BPConsumed;
                info.BPUpdateTime = refer.BPUpdateTime;
            }

            return info;
        }

        public void Insert(PublicID id, PVPContext context)
        {
            lock (lckObj) list[id] = context;
        }

        public void Update(PublicID id, PVPWaves waves)
        {
            Update(id, (context) => context.Waves = waves);
        }

        public void Update(PublicID id, int cardId)
        {
            Update(id, (context) => context.CardID = cardId);
        }

        public void Update(PublicID id, int bpConsumed, DateTime bpUpdateTime)
        {
            Update(id, (context) => {
                if (context.Origin != null)
                {
                    context.Origin.BPConsumed = bpConsumed;
                    context.Origin.BPUpdateTime = bpUpdateTime;
                }
            });
        }

        private void Update(PublicID id, Action<PVPContext> update)
        {
            lock (lckObj)
            {
                var context = GetUnsafe(id);

                if (context == null) return;

                update(context);

                if (context.IsOverdue)
                {
                    context.IsOverdue = false;
                    idleList.Remove(id);
                    list[id] = context;
                }
            }
        }

        public void AddPointUnsafe(PublicID id, Int64 eventId, int add)
        {
            var context = list.SafeGetValue(id);
            if (context == null) context = idleList.SafeGetValue(id);
            if (context == null) context = dummyList.SafeGetValue(id);
            if (context != null) context.AddPoint(eventId, add);
        }

        public void SafeCall(Callback callback)
        {
            lock (lckObj) callback();
        }

        public static PVPContextList Generate(EventScheduleList schedule, DateTime currentTime)
        {
            var inst = new PVPContextList();
            inst.list = PublicID.CreateDictinary<PVPContext>();
            inst.idleList = PublicID.CreateDictinary<PVPContext>();
            inst.players = new List<PublicID>();
            inst.dummyList = PublicID.CreateDictinary<PVPContext>();
            inst.dummys = new List<PublicID>();

            var ids = PublicID.CreateHashSet();

            var beginTime = currentTime.AddDays(-15);
            foreach (var group in Global.Groups)
            {
                using (var db = Global.DBPool.Acquire(group))
                {
                    var cmd = DBCommandBuilder.SelectPVPContextList(group);

                    using (var res = db.DoQuery(cmd))
                    {
                        foreach (var item in DBObjectConverter.Convert<DBPVPContext>(res))
                        {
                            var id = PublicID.Create(group, item.PlayerID);
                            var obj = new PVPContext();
                            obj.Setup(item);

                            if (item.UpdateTime > beginTime)
                                inst.list.Add(id, obj);
                            else
                            {
                                obj.IsOverdue = true;
                                inst.idleList.Add(id, obj);
                            }

                            inst.players.Add(id);

                            ids.Add(id);
                        }
                    }
                }
            }

            using (var db = Global.DBPool.AcquireUnique())
            {
                var cmd = DBCommandBuilder.DebugSelectPVPContext();
                using (var res = db.DoQuery(cmd))
                {
                    foreach (var item in DBObjectConverter.Convert<DBPVPIdlePlayer>(res))
                    {
                        var id = PublicID.Create(item.Group, item.PlayerID);

                        if (!ids.Add(id)) continue;

                        var obj = new PVPContext();
                        obj.Setup(item);
                        inst.dummyList.Add(id, obj);
                        inst.dummys.Add(id);
                    }
                }
            }

            inst.schedule = schedule.PVP;

            if (inst.schedule != null && inst.schedule.Players != null)
            {
                var eventId = inst.schedule.Relative.ID;
                foreach (var item in inst.schedule.Players)
                {
                    inst.AddPointUnsafe(item.Key, eventId, item.Value.Point);
                }
            }

            return inst;
        }

        public List<PVPPlayer> Query(PublicID id, int point)
        {
            var ids = PublicID.CreateHashSet();

            if (schedule.RankingPlayer.Count > 0)
            {
                var current = this[id];

                var pos = GetPosition(point, 0, schedule.RankingPlayer.Count - 1);

                for (int i = 0; i < 10; ++i)
                {
                    var rand = MathUtils.Restrict(pos + MathUtils.Random.Next(-10, 11), 0, schedule.RankingPlayer.Count - 1);
                    var playerId = schedule.RankingPlayer[rand].ID;
                    if (playerId.Equals(id)) continue;
                    ids.Add(playerId);
                    break;
                }

                {
                    var rand = MathUtils.Random.Next(schedule.RankingPlayer.Count);
                    var playerId = schedule.RankingPlayer[rand].ID;
                    if (!playerId.Equals(id)) ids.Add(playerId);
                }
            }

            if (dummys.Count > 0)
            {
                // 有3成機會rand到假(舊)玩家
                if (MathUtils.Random.NextDouble() < 0.3)
                {
                    var rand = MathUtils.Random.Next(dummys.Count);
                    var playerId = dummys[rand];
                    if (!playerId.Equals(id)) ids.Add(playerId);
                }
            }

            if (ids.Count < 3)
            {
                for (int i = 0; i < 10; ++i)
                {
                    var rand = MathUtils.Random.Next(players.Count);
                    var playerId = players[rand];
                    if (playerId.Equals(id) || !ids.Add(playerId)) continue;
                    if (ids.Count >= 3) break;
                }
            }

            if (ids.Count < 3 && players.Count > 3)
            {
                foreach (var item in players)
                {
                    if (item.Equals(id) || !ids.Add(item)) continue;
                    if (ids.Count >= 3) break;
                }
            }

            var list = new List<PVPPlayer>(ids.Count);

            foreach (var item in ids)
            {
                var player = new PVPPlayer { ID = item };

                var isExists = Get(item, obj=> {
                    player.Point = (obj.EventID == schedule.Relative.ID) ? obj.Point : 0;
                    player.Waves = obj.Waves.List;
                });

                if (isExists) list.Add(player);
            }

            return list;
        }

        private int GetPosition(int point, int begin, int end)
        {
            if (end - begin <= 3) return begin;
            var middle = (end - begin) / 2;
            var middlePlayer = schedule.RankingPlayer[middle];

            if (middlePlayer.Point > point) return GetPosition(point, begin, middle);
            if (middlePlayer.Point < point) return GetPosition(point, middle, end);

            return middle;
        }
    }

    public class RefreshPVPListSchedule : ScheduleTask
    {
        public Service Owner;
        public override void Execute(int times)
        {
            Owner.AddKernelEvent(new RefreshPVPListPrepareTask());
        }

        class RefreshPVPListPrepareTask : Tasks.KernelTask
        {
            public override void Execute(Context context)
            {
                context.Owner.AddTask(new RefreshPVPListTask { Owner = context.Owner, EventSchedule = context.EventScheduleList });
            }
        }

        class RefreshPVPListTask : ITask
        {
            public Service Owner;
            public EventScheduleList EventSchedule;

            public void Execute()
            {
                var pvpList = PVPContextList.Generate(EventSchedule, Global.CurrentTime);
                Owner.AddKernelEvent(new ReplacePVPListTask { NewObj = pvpList });
            }

            public void Dispose() { }
        }

        class ReplacePVPListTask : Tasks.KernelTask
        {
            public PVPContextList NewObj;
            public override void Execute(Context context)
            {
                context.PVPList = NewObj;
            }
        }
    }
}
