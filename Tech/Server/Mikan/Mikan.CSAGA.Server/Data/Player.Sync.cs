﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mikan.CSAGA.Server
{
    public partial class Player
    {
        List<ISyncData> syncList;

        private CooperateMissionInfo cachedMissionInfo;

        private GlobalCooperateMissionInfo globalMissionInfo;

        private PVPContextList globalPVPList;

        private Queue<SyncMission> missions = new Queue<SyncMission>();

        public void OnSyncPrepare(Context context, List<ISyncData> syncList)
        {
            cachedMissionInfo = CooperateMissionInfo;

            globalMissionInfo = context.CooperateMissionInfo;

            globalPVPList = context.PVPList;

            this.syncList = new List<ISyncData>();

            foreach (var item in syncList)
            {
                if (item.Type == SyncType.Advance)
                {
                    context.SyncManager.OnSyncData(this, item);

                    if (item is SyncFavorite)
                    {
                        this.syncList.Add(new SyncPVPContext());
                    }
                }
                else
                    this.syncList.Add(item);
            }
        }

        public void BeginSync()
        {
            foreach (var item in syncList) Context.SyncManager.OnSyncData(this, item);
        }

        public void EndSync(Context context)
        {
            while (missions.Count > 0)
            {
                var mission = missions.Dequeue();
                context.MissionSubjects[mission.Serial][mission.Index] += mission.Count;
                cachedMissionInfo.IsOverdue = true;
            }

            cachedMissionInfo = null;
            globalMissionInfo = null;
        }

        public void OnBackup(Backup data)
        {
            SyncUtils.WriteDB(this, data);
        }

        public bool SyncMission(SyncMission trigger, CooperateMissionInfo info)
        {
            var mission = info.Map.SafeGetValue(trigger.Serial);

            if (mission != null)
            {
                var subject = mission.Subjects[trigger.Index];

                if (trigger.SubjectID == 0) trigger.SubjectID = subject.ID;

                if (subject.ID != trigger.SubjectID) return false;

                if (subject.Rawdata.n_MAX > 0)
                {
                    // 同一批觸發的任務是不會重複的 所以只要用原始資料判斷上限就好,不用算上這次的
                    if (trigger.Count + subject.Progress > subject.Rawdata.n_MAX)
                        trigger.Count = subject.Rawdata.n_MAX - subject.Progress;
                }

                if (trigger.Count == 0) return false;

                var subjects = new int[5];

                subjects[trigger.Index] = trigger.Count;

                using (var db = Global.DBPool.Acquire(ID.Group))
                {
                    var cmd = DBCommandBuilder.UpdateCoMissionState(ID, trigger.Serial, subjects);
                    db.SimpleQuery(cmd);
                }

                return true;
            }

            return false;
        }

        public void OnSyncMission(SyncMission mission)
        {
            if (cachedMissionInfo == null || cachedMissionInfo.IsOverdue)
            {
                cachedMissionInfo = CooperateMissionInfo.Query(ID, globalMissionInfo.BeginSerial);
                cachedMissionInfo = globalMissionInfo.Clone(cachedMissionInfo.Origin);
            }

            if (SyncMission(mission, cachedMissionInfo)) missions.Enqueue(mission);
        }

        public void OnSyncFavorite(SyncFavorite favorite)
        {
            if (PlayerInfo != null && PlayerInfo.IsAvailable && CardInfo != null)
            {
                CardInfo.SetFavorite(favorite.Serial);
                //UpdateSnapshot(Global.CurrentTime);
                /*
                using (var db = Global.DBPool.Acquire(ID.Group))
                {
                    var cmd = DBCommandBuilder.UpdatePVPContextCardID(ID, CardInfo.Favorite.ID, Global.CurrentTime);
                    db.SimpleQuery(cmd);
                }
                */
            }
        }

        public void OnSyncPVPContext(SyncPVPContext syncData)
        {
            if (PlayerInfo != null && PlayerInfo.IsAvailable && CardInfo != null)
            {
                using (var db = Global.DBPool.Acquire(ID.Group))
                {
                    var cmd = DBCommandBuilder.UpdatePVPContextCardID(ID, CardInfo.Favorite.ID, Global.CurrentTime);
                    db.SimpleQuery(cmd);
                }

                globalPVPList.Update(ID, CardInfo.Favorite.ID);
            }
        }

        public void OnSyncPlatform(SyncPlatform platform)
        {
            Transaction = platform.Transaction;

            var signature = string.Format("{0}|{1}|{2}", platform.Platform, platform.AppVer, platform.Transaction);

            if (Context.AvailableAPKList.Contains(signature)) return;

            KernelService.Logger.Debug("[BLACK] ID={0}, Signature={1}", ID.ToString(), signature);

            if (UseSuperPrivilege) return;

            Forbidden = true;

            using (var db = Global.DBPool.AcquireUnique())
            {
                var cmd = DBCommandBuilder.InsertBlack(ID, platform.Platform, platform.AppVer, platform.Transaction, Global.CurrentTime);
                db.SimpleQuery(cmd);
            }

        }
    }

    public class SyncUtils
    {
        public static bool WriteDB(Player player, Backup backup)
        {
            using (var db = Global.DBPool.Acquire(player.ID.Group))
            {
                var cmd = DBCommandBuilder.UpdateBackup(player.ID, backup.Key, backup.Data);

                db.SafeSimpleQuery(cmd);

                return true;
            }
        }
    }
}
