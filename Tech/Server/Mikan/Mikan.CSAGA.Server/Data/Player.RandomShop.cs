﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mikan.CSAGA.Internal;
using Mikan.Server;

namespace Mikan.CSAGA.Server
{
    public partial class Player
    {
        public RandomShopInfo RefreshRandomShop(IDBConnection db, DateTime timestamp, bool existing)
        {
            var info = new RandomShopInfo();

            var seed = Context.RandomShop.NextSeed();

            var cmd = (existing || RandomShopInfo != null)
                        ? DBCommandBuilder.UpdateRandomShopSeed(ID, seed, timestamp)
                        : DBCommandBuilder.InsertRandomShop(ID, seed, timestamp);

            db.SimpleQuery(cmd);

            info.List = Context.RandomShop.Select(seed).ToList();
            info.UpdateTime = timestamp;
            info.State = new ComplexI32();

            return info;
        }
    }

    public class RandomShopInfo : RandomShopInfoObj
    {

    }
}
