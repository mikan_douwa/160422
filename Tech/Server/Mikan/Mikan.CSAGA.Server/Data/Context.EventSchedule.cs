﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Mikan.CSAGA.Server
{
    public partial class Context
    {
        public EventScheduleList EventScheduleList;
        public EventScheduleList EventScheduleListTmp;

        private void SetupEventSchedule()
        {
            EventScheduleList = Mikan.CSAGA.Server.EventScheduleList.Generate(Global.CurrentTime);
        }
    }

    public class EventScheduleList
    {
        const int DELAY_CLEAN_DAY = 3;

        public DateTime UpdateTime { get; private set; }

        public EventScheduleObj Rescue { get; private set; }

        public EventScheduleObj Crusade { get; private set; }

        public EventScheduleObj PVP { get; private set; }
        public EventScheduleObj PVPNext { get; private set; }

        public Int64 MinSerial { get { return minSerial; } }

        private Dictionary<int, EventScheduleObj> questOrder = new Dictionary<int, EventScheduleObj>();

        private Dictionary<Int64, EventScheduleObj> serialOrder = new Dictionary<long, EventScheduleObj>();

        private Int64 minSerial = Int64.MaxValue;

        public IEnumerable<EventScheduleObj> SelectCurrentSchedule()
        {
            foreach (var item in questOrder)
            {
                if (item.Value.Relative == null) continue;
                yield return item.Value;
            }
        }
        public IEnumerable<EventScheduleObj> SelectActiveSchedule(DateTime currentTime)
        {
            foreach (var item in serialOrder)
            {
                if (item.Value.BeginTime > currentTime || item.Value.EndTime.AddDays(1) < currentTime) continue;

                yield return item.Value;
            }
        }

        public EventScheduleObj GetCurrentInfinity(DateTime currentTime)
        {
            var obj = default(EventScheduleObj);

            foreach (var item in questOrder)
            {
                if (!item.Value.IsInfinity || item.Value.EndTime < currentTime) continue;
                if (obj == null || obj.BeginTime > item.Value.BeginTime)
                    obj = item.Value;
            }

            if (obj == null)
            {
                foreach (var item in serialOrder)
                {
                    if (!item.Value.IsInfinity || item.Value.EndTime < currentTime) continue;
                    if (obj == null || obj.BeginTime > item.Value.BeginTime)
                        obj = item.Value;
                }
            }

            return obj;
        }

        public int CalcRescueCount(int count, DateTime updateTime, DateTime currentTime)
        {
            var origin = count;

            if (updateTime < Rescue.BeginTime)
            {
                origin = Global.Tables.HelpInital * Global.Tables.HelpScale;
                updateTime = Rescue.BeginTime;
            }
            
            var minutes = currentTime.Subtract(updateTime).TotalMinutes;

            var add = (int)Math.Floor((minutes / (float)Global.Tables.HelpAdd) * Global.Tables.HelpScale);

            return Math.Min(origin + add, Global.Tables.HelpMax * Global.Tables.HelpScale);
        }

        public bool IsRescueAvailable(int count, DateTime updateTime, DateTime currentTime)
        {
            if (Rescue == null || currentTime < Rescue.BeginTime || currentTime > Rescue.EndTime) return false;

            if (updateTime < Rescue.BeginTime || count >= Global.Tables.HelpScale) return true;

            var minutes = currentTime.Subtract(updateTime).TotalMinutes;

            var add = (int)Math.Floor((minutes / (float)Global.Tables.HelpAdd) * Global.Tables.HelpScale);

            return (count + add) >= Global.Tables.HelpScale;
        }

        public int CalcCrusadeCount(int count, DateTime updateTime, DateTime currentTime)
        {
            return Calc.Quest.CalcCrusadeCount(count, updateTime, Crusade.BeginTime, currentTime);
        }

        public bool IsCrusadeAvailable(int count, DateTime updateTime, DateTime currentTime)
        {
            if (Crusade == null || currentTime < Crusade.BeginTime || currentTime > Crusade.EndTime) return false;

            if (updateTime < Crusade.BeginTime || count >= Global.Tables.CrusadeScale) return true;

            var minutes = currentTime.Subtract(updateTime).TotalMinutes;

            var add = (int)Math.Floor((minutes / (float)Global.Tables.CrusadeAdd) * Global.Tables.CrusadeScale);

            return (count + add) >= Global.Tables.CrusadeScale;
        }

        public void Add(EventScheduleObj obj, DateTime currentTime)
        {
            var origin = questOrder.SafeGetValue(obj.MainQuestID);
            if(origin == null || (origin.EndTime < currentTime || (origin.BeginTime < obj.BeginTime && obj.BeginTime < currentTime)))
                questOrder[obj.MainQuestID] = obj;
        }

        public void Add(DBEventSchedule obj, DateTime currentTime)
        {
            var data = Global.Tables.EventSchedule[obj.DataID];

            if (data == null) return;

            var current = GetCurrent(data.n_QUESTID);

            if (current == null) return;

            var endTime = obj.BeginTime.AddMinutes(data.n_DURATION);

            if (data.n_TYPE != EventScheduleType.PVP)
            {
                if (endTime.AddDays(DELAY_CLEAN_DAY) < currentTime) return;
            }
            
            var copy = new EventScheduleObj { Origin = current.Origin, Relative = obj, BeginTime = obj.BeginTime, EndTime = endTime, Complex = new ComplexI32 { Value = obj.Complex } };

            serialOrder[obj.ID] = copy;

            if (minSerial > obj.ID) minSerial = obj.ID;

            if (current.Relative == null || current.Relative.ID < obj.ID)
            {
                if(data.n_TYPE != EventScheduleType.PVP || current.Relative == null || obj.BeginTime < currentTime)
                    questOrder[data.n_QUESTID] = copy;
            }
            
            if (copy.Relative != null)
            {
                var sample = Global.Tables.QuestDesign.SelectFirst((o) => { return o.n_MAIN_QUEST == data.n_QUESTID; });

                if (sample != null && sample.n_COUNT == 0 && sample.n_QUEST_TYPE != QuestType.Crusade)
                {
                    copy.IsInfinity = true;
                    copy.InfinityBonus = Calc.Quest.CalcInfinityBonus(copy.Relative.ID);
                }
            }

            if (data.n_TYPE == EventScheduleType.Crusade)
            {
                if (Crusade == null || Crusade.Complex[0])
                    Crusade = copy;
            }

            // 為了保留上次的PVP積分 無視未開始的PVP活動
            if (data.n_TYPE == EventScheduleType.PVP)
            {
                if (copy.BeginTime > currentTime)
                {
                    if (PVPNext == null || copy.BeginTime < PVPNext.BeginTime)
                        PVPNext = copy;
                }
                else
                {
                    if (PVP == null || copy.BeginTime > PVP.BeginTime)
                        PVP = copy;
                }
            }
        }

        public void AddRescue(ConstData.EventSchedule data)
        {
            var newEvent = CalcEvent(data, Rescue);

            if (newEvent != null) Rescue = newEvent;
        }

        private EventScheduleObj CalcEvent(ConstData.EventSchedule data, EventScheduleObj current)
        {
            var beginTime = Calc.Quest.ParseBeginTime(data.n_START_DAY, data.n_START_TIME);
            var endTime = beginTime.AddMinutes(data.n_DURATION);

            if (endTime < Global.CurrentTime) return null;

            if (current != null && current.BeginTime < beginTime) return null;

            return new EventScheduleObj { Origin = data, BeginTime = beginTime, EndTime = endTime };
        }

        public void InsertNewEvent(Mikan.Server.IDBConnection db, DateTime currentTime)
        {
            var list = new List<DBEventSchedule>();

            var beginTime = default(DateTime);
            var endTime = default(DateTime);

            foreach (var item in questOrder)
            {
                var data = item.Value.Origin;

                if (data.n_WEEKLY != 0 || (data.n_POINT == 0 && data.n_RANKING == 0)) continue;

                if (!Calc.Quest.ParseSchedulePeriod(data, currentTime, ref beginTime, ref endTime)) continue;

                if (currentTime.AddDays(1) < beginTime || endTime.AddDays(DELAY_CLEAN_DAY) < currentTime) continue;

                if (item.Value.Relative != null && item.Value.BeginTime >= beginTime) continue;

                var cmd = DBCommandBuilder.InsertEventSchedule(data.n_ID, beginTime);

                var id = db.DoQuery<Int64>(cmd);

                list.Add(new DBEventSchedule { ID = id, DataID = data.n_ID, BeginTime = beginTime });
            }

            foreach (var item in list) Add(item, currentTime);
        }

        public EventScheduleObj GetCurrent(int mainQuestId)
        {
            return questOrder.SafeGetValue(mainQuestId);
        }

        public EventScheduleObj Get(Int64 serial)
        {
            return serialOrder.SafeGetValue(serial);
        }

        public void GetPlayerList()
        {
            foreach (var group in Global.Groups)
            {
                using (var db = Global.DBPool.Acquire(group))
                {
                    var cmd = DBCommandBuilder.SelectEventPoint(group, minSerial);
                    using (var res = db.DoQuery(cmd))
                    {
                        foreach (var item in DBObjectConverter.Convert<DBEventPoint>(res))
                        {
                            var obj = serialOrder.SafeGetValue(item.EventID);
                            if (obj == null) continue;
                            if (obj.Players == null) obj.Players = PublicID.CreateDictinary<RankingPlayer>();
                            
                            var id = PublicID.Create(group, item.PlayerID);
                            var player = new RankingPlayer { ID = id, Point = item.Point, CardID = item.CardID, Complex = item.Complex };
                            player.PublicID = id.ToString();

                            if (!obj.Points.TryGetValue(player.Point, out player.RankingPoint))
                            {
                                player.RankingPoint = new RankingPoint { Value = player.Point };
                                obj.Points.Add(player.Point, player.RankingPoint);
                                obj.Ranking.Add(player.RankingPoint);
                            }

                            player.RankingPoint.Players.Add(player);

                            obj.Players.Add(id, player);
                            obj.RankingPlayer.Add(player);
                        }
                    }
                }
            }

            foreach (var item in serialOrder)
            {
                var ranking = item.Value.Ranking;
                ranking.Sort(Compare);

                for (int i = 0; i < ranking.Count; ++i)
                {
                    var _ranking = ranking[i];
                    _ranking.Ranking = i;
                    foreach (var player in _ranking.Players) player.Ranking = i;
                }

                var rankingPlayer = item.Value.RankingPlayer;
                rankingPlayer.Sort(Compare);

                for (int i = 0; i < rankingPlayer.Count; ++i)
                {
                    rankingPlayer[i].SortRanking = i;
                }
            }
        }

        public void TryCommit(DateTime currentTime)
        {
            KernelService.Logger.Debug("EventSchedule.TryCommit");
            // 已結算完成
            const int COMMITED = 0;
            foreach (var item in serialOrder)
            {
                var obj = item.Value;

                if (Global.IsDevServer)
                {
                    if (obj.EndTime > currentTime || obj.Complex[COMMITED]) continue;
                }
                else
                {
                    if (obj.EndTime.AddHours(13) > currentTime || obj.Complex[COMMITED]) continue;
                }

                if(obj.Origin.n_RANKING > 0)
                {
                    var periods = Global.Tables.RankingPeriods.SafeGetValue(obj.Origin.n_RANKING);
                    
                    if (periods == null || obj.Players == null || obj.Players.Count == 0) continue;

                    foreach (var period in periods)
                    {
                        var begin = Math.Min((int)Math.Ceiling((float)period.n_EFFECT[0] / 100f * obj.Ranking.Count), obj.Ranking.Count - 1);
                        var end = Math.Min((int)Math.Floor((float)period.n_EFFECT[1] / 100f * obj.Ranking.Count), obj.Ranking.Count - 1);

                        var gifts = new List<Data.Gift>();

                        foreach (var trophy in Global.Tables.Trophy.Select(o => { return o.n_LEGACY_TYPE == TrophyType.EventRanking && (int)o.n_IDSET == obj.Origin.n_RANKING && o.n_EFFECT[0] == period.n_EFFECT[0]; }))
                        {
                            gifts.Add(Calc.Trophy.ToGift(trophy, RewardSource.EventRanking));
                        }

                        for (int i = begin; i <= end; ++i)
                        {
                            var ranking = obj.Ranking[i];

                            foreach (var player in ranking.Players)
                            {
                                var complex = new ComplexI32 { Value = player.Complex };
                                if (complex[COMMITED]) continue;

                                using (var db = Global.DBPool.Acquire(player.ID.Group))
                                {
                                    using (var trans = db.BeginTransaction())
                                    {
                                        var cmd = DBCommandBuilder.UpdateEventPointComplex(player.ID, obj.Relative.ID, ComplexI32.Bit(COMMITED).Value);
                                        db.SimpleQuery(cmd);

                                        foreach (var gift in gifts)
                                        {
                                            cmd = DBCommandBuilder.InsertGift(player.ID, RewardSource.EventRanking, gift.Description, gift.Type, gift.Value1, gift.Value2);
                                            db.SimpleQuery(cmd);
                                        }

                                        trans.Commit();
                                    }
                                }
                            }
                        }
                    }
                }
                using (var db = Global.DBPool.AcquireUnique())
                {
                    var cmd = DBCommandBuilder.UpdateEventScheduleComplex(item.Key, ComplexI32.Bit(COMMITED).Value);
                    db.SimpleQuery(cmd);
                }
            }
        }

        private static int Compare(RankingPoint lhs, RankingPoint rhs)
        {
            return rhs.Value.CompareTo(lhs.Value);
        }

        private static int Compare(RankingPlayer lhs, RankingPlayer rhs)
        {
            return rhs.Point.CompareTo(lhs.Point);
        }

        public static EventScheduleList Generate(DateTime currentTime)
        {
            var list = new EventScheduleList();

            var beginTime = currentTime;
            var endTime = currentTime;

            foreach (var item in Global.Tables.EventSchedule.Select())
            {
                switch (item.n_TYPE)
                {
                    case EventScheduleType.Quest:
                    case EventScheduleType.Crusade:
                    case EventScheduleType.PVP:
                    {
                        // 這裡會有資料和QuestSchedule重複,但QuestSchedule是需要即時資料,不能廢掉,想到方法再優化吧
                        if (!Calc.Quest.ParseSchedulePeriod(item, currentTime, ref beginTime, ref endTime)) continue;
                        var obj = new EventScheduleObj { Origin = item, BeginTime = beginTime, EndTime = endTime };

                        list.Add(obj, currentTime);

                        break;
                    }
                    case EventScheduleType.Rescue:
                    {
                        list.AddRescue(item);
                        break;
                    }
                }
            }

            lock (typeof(EventScheduleList))
            {
                using (var db = Global.DBPool.AcquireUnique())
                {
                    var cmd = DBCommandBuilder.SelectEventSchedule();
                    using (var res = db.DoQuery(cmd))
                    {
                        foreach (var item in DBObjectConverter.Convert<DBEventSchedule>(res)) list.Add(item, currentTime);
                    }

                    list.GetPlayerList();

                    list.InsertNewEvent(db, currentTime);
                }
            }

            list.UpdateTime = currentTime;

            return list;
        }
    }

    public class EventScheduleObj
    {
        public int EventID { get { return Origin.n_ID; } }
        public int MainQuestID { get { return Origin.n_QUESTID; } }
        public bool IsCommited { get { return Complex[0]; } }
        public bool IsInfinity;

        public DateTime BeginTime;
        public DateTime EndTime;

        public Dictionary<PublicID, RankingPlayer> Players;
        public List<RankingPlayer> RankingPlayer = new List<RankingPlayer>();

        public Dictionary<int, RankingPoint> Points = new Dictionary<int, RankingPoint>();
        public List<RankingPoint> Ranking = new List<RankingPoint>();
        
        public List<ValuePair> InfinityBonus;

        public ConstData.EventSchedule Origin;
        public DBEventSchedule Relative;
        public ComplexI32 Complex;
    }

    public class RankingPlayer : Internal.RankingPlayerObj
    {
        public PublicID ID;
        public int Complex;

        public int SortRanking;
        public RankingPoint RankingPoint;
    }

    public class RankingPoint
    {
        public int Value;
        public int Ranking;
        public List<RankingPlayer> Players = new List<RankingPlayer>();
    }

    public class RefreshEventScheduleSchedule : ScheduleTask
    {
        public Service Owner;
        public override void Execute(int times)
        {
            var list = EventScheduleList.Generate(Global.CurrentTime);
            list.TryCommit(Global.CurrentTime);
            var pvp = PVPContextList.Generate(list, Global.CurrentTime);
            Owner.AddKernelEvent(new ReplaceEventScheduleTask { NewObj = list, NewPVPObj = pvp });
        }
    }

    public class ReplaceEventScheduleTask : Tasks.KernelTask
    {
        public EventScheduleList NewObj;
        public PVPContextList NewPVPObj;
        public override void Execute(Context context)
        {
            context.EventScheduleList = NewObj;
            context.EventScheduleListTmp = null;
            context.PVPList = NewPVPObj;
        }
    }
}

