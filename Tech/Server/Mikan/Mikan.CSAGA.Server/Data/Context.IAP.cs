﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mikan.Server;
using System.Security.Cryptography.X509Certificates;
using System.Net;
using System.Net.Security;

namespace Mikan.CSAGA.Server
{
    public partial class Context
    {
        public Mikan.Server.IAP.GooglePlay.IAPService GooglePlayIAPService;
        public Mikan.Server.IAP.iOS.IAPService iOSIAPService;
        public Mikan.Server.IAP.MyCard.IAPService MyCardIAPService;

        public Dictionary<string, IAPProduct> IAPProductList;

        private void SetupIAP()
        {
            ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(delegate { return true; });

            iOSIAPService = new Mikan.Server.IAP.iOS.IAPService("com.mikan.csaga");

            GooglePlayIAPService = new Mikan.Server.IAP.GooglePlay.IAPService();

            var certificate = new X509Certificate2(DataPath + "csaga.iap.p12", "notasecret", X509KeyStorageFlags.Exportable);

            GooglePlayIAPService.Initialize("com.mikan.csaga", "account-1@api-7169968673650633879-343901.iam.gserviceaccount.com", certificate);

            MyCardIAPService = new Mikan.Server.IAP.MyCard.IAPService(Global.MyCardSetting);
            
            IAPProductList = new Dictionary<string, IAPProduct>();

            QueryIAPProduct(null);
            /*
            using (var db = Global.DBPool.AcquireUnique())
            {
                QueryIAPProduct(db);

                if (IAPProductList.Count == 0)
                {
                    using (var trans = db.BeginTransaction())
                    {
                        var cmd = DBCommandBuilder.InsertIAPProduct(1, "talesingames.csaga.iap01", 20, 10, "神石20個", "TWD. 30.00", 30);
                        db.SimpleQuery(cmd);
                        cmd = DBCommandBuilder.InsertIAPProduct(2, "talesingames.csaga.iap02", 100, 50, "神石100個", "TWD. 120.00", 120);
                        db.SimpleQuery(cmd);
                        cmd = DBCommandBuilder.InsertIAPProduct(3, "talesingames.csaga.iap03", 250, 125, "神石250個", "TWD. 270.00", 270);
                        db.SimpleQuery(cmd);
                        cmd = DBCommandBuilder.InsertIAPProduct(4, "talesingames.csaga.iap04", 500, 250, "神石500個", "TWD. 520.00", 520);
                        db.SimpleQuery(cmd);
                        cmd = DBCommandBuilder.InsertIAPProduct(5, "talesingames.csaga.iap05", 1000, 500, "神石1000個", "TWD. 840.00", 840);
                        db.SimpleQuery(cmd);
                        cmd = DBCommandBuilder.InsertIAPProduct(6, "talesingames.csaga.iap06", 2000, 1000, "神石2000個", "TWD. 1490.00", 1490);
                        db.SimpleQuery(cmd);
                        cmd = DBCommandBuilder.InsertIAPProduct(7, "talesingames.csaga.iap07", 4000, 2000, "神石4000個", "TWD. 2490.00", 2490);
                        db.SimpleQuery(cmd);
                        cmd = DBCommandBuilder.InsertIAPProduct(8, "talesingames.csaga.iap08", 130, 65, "神石130個", "TWD. 150.00", 150);
                        db.SimpleQuery(cmd);

                        trans.Commit();
                    }

                    QueryIAPProduct(db);
                }
            }
            */
            foreach (var item in IAPProductList)
            {
                //if(item.Value.IsMyCard)
                    MyCardIAPService.AddProduct(item.Value.ID, item.Value.ProductName, item.Value.Dollars);
            }

            foreach (var item in Global.Tables.MyCardBonus.Select())
            {
                MyCardIAPService.AddPromoCode(item.s_CODE, item.n_FEEDBACK);
            }
        }

        private void QueryIAPProduct(IDBConnection db)
        {
            foreach (var item in Global.Tables.IAPList.Select())
            {
                var obj = new IAPProduct { Origin = item };
                obj.ProductID = item.s_KEY;
                obj.ProductName = Global.Tables.GetSystemText(item.n_NAME);
                obj.Price = Global.Tables.GetSystemText(item.n_NAME_PRICE);
                IAPProductList[obj.ProductID] = obj;
                
            }
            /*
            var cmd = DBCommandBuilder.SelectIAPProduct();
            using (var res = db.DoQuery(cmd))
            {
                foreach (var item in DBObjectConverter.Convert<DBIAPProduct>(res))
                {
                    var obj = new IAPProduct { Origin = item };
                    obj.ProductID = item.ProductID;
                    obj.ProductName = item.Name;
                    obj.Price = item.Price;
                    IAPProductList[item.ProductID] = obj;
                }
            }
            */
        }
    }
}
