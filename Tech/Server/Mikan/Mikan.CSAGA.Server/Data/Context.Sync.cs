﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mikan.CSAGA.Server
{
    public partial class Context
    {
        public SyncManager<Player> SyncManager;

        private void SetupSyncManager()
        {
            SyncManager = new SyncManager<Player>();
            SyncManager.RegisterHandler<Backup>(OnBackup);
            SyncManager.RegisterHandler<SyncMission>(OnSyncMission);
            SyncManager.RegisterHandler<SyncFavorite>(OnSyncFavorite);
            SyncManager.RegisterHandler<SyncPlatform>(OnSyncPlatform);
            SyncManager.RegisterHandler<SyncPVPContext>(OnSyncPVPContext);
        }

        private void OnBackup(Player player, ISyncData data)
        {
            player.OnBackup(data as Backup);
        }

        private void OnSyncMission(Player player, ISyncData data)
        {
            player.OnSyncMission(data as SyncMission);
        }

        private void OnSyncFavorite(Player player, ISyncData data)
        {
            player.OnSyncFavorite(data as SyncFavorite);
        }

        private void OnSyncPlatform(Player player, ISyncData data)
        {
            player.OnSyncPlatform(data as SyncPlatform);
        }

        private void OnSyncPVPContext(Player player, ISyncData data)
        {
            player.OnSyncPVPContext(data as SyncPVPContext);
        }
    }

    public class SyncPVPContext : SyncDataBase, ISyncData
    {
        public void Serialize(IOutput output) { }

        public void DeSerialize(IInput input) { }
    }
}
