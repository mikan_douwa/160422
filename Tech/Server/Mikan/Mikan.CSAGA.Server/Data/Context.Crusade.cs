﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mikan.CSAGA.Server
{
    public partial class Context
    {
        public SharedQuestList SharedQuestList;

        private void SetupCrusade()
        {
            SharedQuestList = SharedQuestList.Query();

        }
    }

    public class SharedQuestList
    {
        private List<SharedQuest> list;
        private List<SharedQuest> secondary;
        private List<SharedQuest> overdue;
        private Dictionary<Int64, SharedQuest> map;

        public SharedQuest this[Int64 serial] { get { return map.SafeGetValue(serial); } }

        public void Setup(IEnumerable<DBSharedQuest> items)
        {
            var currentTime = Global.CurrentTime;

            list = new List<SharedQuest>();
            secondary = new List<SharedQuest>();
            overdue = new List<SharedQuest>();
            map = new Dictionary<long, SharedQuest>();
            foreach (var item in items)
            {
                var obj = SharedQuest.Create(item);

                if (obj.EndTime < currentTime)
                    overdue.Add(obj);
                else if (obj.PlayCount < Global.Tables.CrusadeOwnerLimit)
                    list.Add(obj);
                else
                    secondary.Add(obj);

                map.Add(obj.Serial, obj);
            }

            list.Sort((lhs, rhs) => lhs.EndTime.CompareTo(rhs.EndTime));
        }

        public IEnumerable<SharedQuest> Random(PublicID id, DateTime currentTime)
        {

            var rand = new Random();

            var items = new HashSet<SharedQuest>();

            if (list.Count > 10)
            {
                var range = Math.Min(20, list.Count);

                // 前5個先取時間快結束的
                for (int i = 0; i < 5; ++i)
                {
                    var item = list[rand.Next(range)];

                    if (!item.PublicID.Equals(id) && item.EndTime > currentTime)
                        items.Add(item);
                }

                range = list.Count;

                var retryCount = 0;
                while (items.Count < 10 && retryCount < 5)
                {
                    var item = list[rand.Next(range)];

                    if (item.PublicID.Equals(id) || item.EndTime < currentTime || !items.Add(item))
                        ++retryCount;
                }
            }

            if (items.Count < 10 && list.Count > 0)
            {
                foreach (var item in list)
                {
                    if (!item.PublicID.Equals(id) && item.EndTime > currentTime && !items.Contains(item))
                    {
                        items.Add(item);
                        if (items.Count >= 10) break;
                    }
                }
            }

            if (items.Count < 10 && secondary.Count > 0)
            {
                foreach (var item in secondary)
                {
                    if (!item.PublicID.Equals(id) && item.EndTime > currentTime)
                    {
                        items.Add(item);
                        if (items.Count >= 10) break;
                    }
                }
            }

            return items;
        }

        public static SharedQuestList Query()
        {
            var list = new SharedQuestList();

            using (var db = Global.DBPool.AcquireUnique())
            {
                var cmd = DBCommandBuilder.SelectSharedQuest();
                using (var res = db.DoQuery(cmd))
                {
                    list.Setup(DBObjectConverter.Convert<DBSharedQuest>(res));
                }
            }

            return list;
        }

        public static List<SharedQuest> Query(PublicID id)
        {
            var list = new List<SharedQuest>();

            using (var db = Global.DBPool.AcquireUnique())
            {
                var cmd = DBCommandBuilder.SelectSharedQuest(id);
                using (var res = db.DoQuery(cmd))
                {
                    foreach(var item in DBObjectConverter.Convert<DBSharedQuest>(res))
                        list.Add(SharedQuest.Create(item));
                }
            }

            return list;
        }
    }

    public class RefreshShareedQuestListSchedule : ScheduleTask
    {
        public Service Owner;

        public override void Execute(int times)
        {
            var list = SharedQuestList.Query();

            Owner.AddKernelEvent(new ReplaceShareedQuestListTask { List = list });
        }
    }

    public class ReplaceShareedQuestListTask : Tasks.KernelTask
    {
        public SharedQuestList List;
        public override void Execute(Context context)
        {
            context.SharedQuestList = List;
        }
    }

    public class ClearOverdueCrusadeSchedule : ScheduleTask
    {
        public Service Owner;

        public override bool Immediately { get { return true; } }

        public override void Execute(int times)
        {

            var isCrusadeExists = false;

            using (var db = Global.DBPool.AcquireUnique())
            {
                

                var cmd = DBCommandBuilder.SelectEventSchedule();

                using (var res = db.DoQuery(cmd))
                {
                    foreach (var item in DBObjectConverter.Convert<DBEventSchedule>(res))
                    {
                        var data = Global.Tables.EventSchedule[item.DataID];

                        if (data == null || data.n_TYPE != EventScheduleType.Crusade) continue;

                        if (item.BeginTime < Global.CurrentTime && item.BeginTime.AddMinutes(data.n_DURATION).AddDays(3) >= Global.CurrentTime)
                        {
                            isCrusadeExists = true;
                            break;
                        }

                    }
                }
            }

            if (!isCrusadeExists)
            {
                using (var db = Global.DBPool.AcquireUnique())
                {
                    var cmd = DBCommandBuilder.DeleteSharedQuest();
                    db.SimpleQuery(cmd);
                }

                foreach (var group in Global.Groups)
                {
                    using (var db = Global.DBPool.Acquire(group))
                    {
                        var cmd = DBCommandBuilder.DeleteCrusade(group);
                        db.SimpleQuery(cmd);
                    }
                }
            }
        }
    }
}
