﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mikan.CSAGA.ConstData;

namespace Mikan.CSAGA.Server
{
    public partial class Context
    {
        private void SetupDataCache()
        {
            DailyActive = new DailyActive();
            using (var db = Global.DBPool.AcquireUnique())
            {
                var signatures = new Dictionary<SignatureType, List<int>>();
                var cmd = DBCommandBuilder.SelectSignature();
                using (var res = db.DoQuery(cmd))
                {
                    foreach(var item in DBObjectConverter.Convert<DBSignature>(res))
                    {
                        var type = (SignatureType)item.Type;
                        DictionaryUtils.SafeGetValue(signatures, type).Add(item.ID);
                    }
                }

                cmd = DBCommandBuilder.SelectDataCache();
                var list = default(List<DBDataCache>);
                using (var res = db.DoQuery(cmd)) list = DBObjectConverter.Convert<DBDataCache>(res).ToList();


                var signature = DictionaryUtils.SafeGetValue(signatures, SignatureType.DropCache);

                SetupDaily(db, list.FindAll(o=>{ return signature.Contains(o.Signature); }));

                signature = DictionaryUtils.SafeGetValue(signatures, SignatureType.GachaCache);

                SetupRandomShop(db, list.FindAll(o => { return signature.Contains(o.Signature); }));

                signature = DictionaryUtils.SafeGetValue(signatures, SignatureType.MissionCache);

                SetupCooperateMission(db, list.FindAll(o => { return signature.Contains(o.Signature); }));
            }
        }
    }

    public enum SignatureType
    {
        DropCache = 1,
        GachaCache = 2,
        MissionCache = 3,
    }

    public class DataCache
    {
        public static DataCache Empty = new DataCache();
        class Item
        {
            public int ID;
            public int Rate;
        }
        private int total = 0;
        private List<Item> list = new List<Item>();

        public int Total { get { return total; } }

        public int Count { get { return list.Count; } }

        public void Add(int id, int rate)
        {
            list.Add(new Item { ID = id, Rate = rate });
            total += rate;
        }

        public bool Equals(int id, int rate)
        {
            foreach (var item in list)
            {
                if (item.ID != id) continue;
                return item.Rate == rate;
            }
            return false;
        }

        public int Get(int rand)
        {
            var weight = 0;

            foreach (var item in list)
            {
                weight += item.Rate;

                if (weight > rand) return item.ID;
            }

            return 0;
        }

        public IEnumerable<int> Select()
        {
            foreach (var item in list) yield return item.ID;
        }
    }


    abstract public class DataCacheList
    {
        protected class Cache
        {
            public DataCache Default = new DataCache();
            private Dictionary<int, Cache> list = new Dictionary<int, Cache>();
            public Cache this[int group]
            {
                get
                {
                    var cache = default(Cache);
                    if (list.TryGetValue(group, out cache)) return cache;
                    return null;
                }
            }

            public IEnumerable<int> Select()
            {
                foreach (var item in list) yield return item.Key;
            }

            public Cache TryAdd(int group)
            {
                var cache = this[group];

                if (cache == null)
                {
                    cache = new Cache();
                    list[group] = cache;
                }
                
                return cache;
            }
        }

        public int CurrentID;

        protected Dictionary<int, Cache> list = new Dictionary<int, Cache>();

        public void Add(int signature, int dataId, int value)
        {
            Add(DictionaryUtils.SafeGetValue(list, signature), dataId, value);

            if (CurrentID < signature) CurrentID = signature;
        }

        virtual public void Add(IEnumerable<DBDataCache> cache)
        {
            foreach (var item in cache) Add(item.Signature, item.DataID, item.Value);
        }

        protected void Add(Cache cache, int group, int dataId, int value)
        {
            var list = cache.TryAdd(group);
            list.Default.Add(dataId, value);
        }

        virtual protected void Add(Cache cache, int dataId, int value)
        {
            cache.Default.Add(dataId, value);
        }

        public Int64 NextSeed()
        {
            var seed1 = (Int64)CurrentID << 32;
            var seed2 = (Int64)MathUtils.Seed.Next();
            return seed1 | seed2;
        }

        abstract public IEnumerable<int> Select(Int64 seed);
    }
}
