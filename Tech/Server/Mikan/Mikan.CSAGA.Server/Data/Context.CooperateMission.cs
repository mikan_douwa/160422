﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mikan.Server;

namespace Mikan.CSAGA.Server
{
    public partial class Context
    {
        public CooperateMissionProvider CooperateMissionProvider;

        public GlobalCooperateMissionInfo CooperateMissionInfo;

        public Dictionary<Int64, int[]> MissionSubjects = new Dictionary<long, int[]>();

        private void SetupCooperateMission(IDBConnection db, IEnumerable<DBDataCache> list)
        {
            var currentTime = Global.CurrentTime;

            CooperateMissionProvider = new CooperateMissionProvider();
            CooperateMissionProvider.Add(list);

            if (!CooperateMissionProvider.Equals(Global.Tables.CoMission.Select()))
            {
                var cmd = DBCommandBuilder.InsertSignature(SignatureType.MissionCache, currentTime);
                var signature = db.DoQuery<int>(cmd);

                foreach (var item in Global.Tables.CoMission.Select())
                {
                    cmd = DBCommandBuilder.InsertDataCache(signature, item.n_ID, 100);
                    // id+dataid=PK,失敗就表示有另一台server也正在塞,不予理會
                    db.SafeSimpleQuery(cmd);

                    CooperateMissionProvider.Add(signature, item.n_ID, 100);
                }
            }

            ReplaceCooperateMissionInfo(GlobalCooperateMissionInfo.Query(CooperateMissionProvider, currentTime));
        }

        public void ReplaceCooperateMissionInfo(GlobalCooperateMissionInfo info)
        {
            CooperateMissionInfo = info;

            MissionSubjects = new Dictionary<long, int[]>(CooperateMissionInfo.List.Count);
            foreach (var mission in CooperateMissionInfo.List)
            {
                MissionSubjects.Add(mission.Serial, new int[] { 0, 0, 0, 0, 0 });
            }
        }

        public void MergeCooperateMissionInfo()
        {
            var info = CooperateMissionInfo.Clone();

            foreach (var mission in info.List)
            {
                var subjects = MissionSubjects.SafeGetValue(mission.Serial);

                if (subjects == null) continue;

                for (int i = 0; i < subjects.Length; ++i) mission.Subjects[i].Progress += subjects[i];

                GlobalCooperateMissionInfo.Subtotal(mission);
            }

            ReplaceCooperateMissionInfo(info);
        }
    }

    public class CooperateMissionProvider : DataCacheList
    {
        public override IEnumerable<int> Select(long seed)
        {
            var id = (int)(seed >> 32);
            var N0 = (int)(seed & int.MaxValue);

            Cache cache = null;

            if (!list.TryGetValue(id, out cache)) yield break;

            var groups = new List<int>(cache.Select());

            var max = Math.Min(groups.Count, 3);

            // 取出3組任務
            for (int i = 0; i < max; ++i)
            {
                N0 = MathUtils.Seed.Next(N0);

                var rand = (int)(N0 % groups.Count);

                var group = groups[rand];

                groups.Remove(group);

                var _cache = cache[group];

                // 項目群組1~5各隨機取得一項
                for (int j = 1; j <= 5; ++j)
                {
                    var _list = _cache[j];
                    rand = (int)(N0 % _list.Default.Total);

                    var dataId = _list.Default.Get(rand);
                    if (dataId != 0) yield return dataId;
                }
            }
        }

        public bool Equals(List<ConstData.CoMission> newList)
        {
            if (CurrentID == 0) return false;

            var current = list.SafeGetValue(CurrentID);

            if (current == null) return false;

            foreach (var item in newList)
            {
                if (!current.Default.Equals(item.n_ID, 100)) return false;
            }

            return current.Default.Count == newList.Count;
        }

        protected override void Add(DataCacheList.Cache cache, int dataId, int value)
        {
            cache.Default.Add(dataId, value);

            var data = Global.Tables.CoMission[dataId];

            var list = cache.TryAdd(data.n_GROUP);

            list = list.TryAdd(data.n_GROUP2);

            list.Default.Add(dataId, value);

        }
    }
    public class CooperateMissionInfo : Internal.CooperationMissionInfoObj<CooperateMission, MissionSubejct, MissionState>
    {
        public Dictionary<Int64, CooperateMission> Map;

        public Dictionary<Int64, MissionState> Origin;

        public bool IsOverdue = false;

        public static CooperateMissionInfo Query(PublicID id, Int64 beginSerial)
        {
            var info = new CooperateMissionInfo();

            info.Origin = new Dictionary<long, MissionState>();

            using (var db = Global.DBPool.Acquire(id.Group))
            {
                var cmd = DBCommandBuilder.SelectCoMissionState(id, beginSerial);

                using (var dbRes = db.DoQuery(cmd))
                {
                    foreach (var item in DBObjectConverter.Convert<DBCoMissionState>(dbRes))
                    {
                        var obj = new MissionState { Origin = item, Value = item.Complex };
                        info.Origin.Add(item.MissionID, obj);
                    }
                }
            }

            return info;
        }
    }

    public class GlobalCooperateMissionInfo : CooperateMissionInfo
    {
        public CooperateMission Newest;

        public Int64 BeginSerial { get; private set; }

        public void Setup(IEnumerable<DBCoMission> list)
        {
            foreach (var item in list) Setup(item);
        }

        public void Setup(DBCoMission dbObj)
        {
            if (List == null) List = new List<CooperateMission>();

            var obj = new CooperateMission { Origin = dbObj, Serial = dbObj.ID, BeginTime = dbObj.BeginTime };
            obj.Complex = new ComplexI32 { Value = dbObj.Complex };
            List.Add(obj);

            if (Newest == null || Newest.BeginTime < obj.BeginTime) Newest = obj;

            if (BeginSerial == 0 || BeginSerial > obj.Serial) BeginSerial = obj.Serial;
        }

        public void Setup(CooperateMissionProvider provider)
        {
            foreach (var item in List)
            {
                var offset = item.Complex.Get(0, 3) * 5;

                item.Subjects = new List<MissionSubejct>(5);

                var subjects = provider.Select(item.Origin.Seed).ToList();

                for (int i = 0; i < 5; ++i)
                {
                    var subjectId = subjects[i + offset];

                    var subject = new MissionSubejct { ID = subjectId };
                    subject.Rawdata = Global.Tables.CoMission[subjectId];

                    if (i == 0) item.Rawdata = subject.Rawdata;

                    item.Subjects.Add(subject);
                }
            }
        }

        public static GlobalCooperateMissionInfo Query(CooperateMissionProvider provider, DateTime currentTime)
        {
            var info = new GlobalCooperateMissionInfo();

            using (var db = Global.DBPool.AcquireUnique())
            {
                var cmd = DBCommandBuilder.SelectCoMission(Global.CurrentTime.AddDays(-2));
                using (var res = db.DoQuery(cmd))
                {
                    info.Setup(DBObjectConverter.Convert<DBCoMission>(res));
                }

                while (info.Newest == null || info.Newest.BeginTime < currentTime.AddDays(1).Date)
                {
                    var beginTime = (info.Newest != null && info.Newest.BeginTime.Date == currentTime.Date) ? currentTime.AddDays(1).Date : currentTime.Date;

                    var seed = provider.NextSeed();

                    var list = new List<DBCoMission>();

                    // 3個任務使用同一個種子,用偏移值來區別
                    for (int i = 0; i < 3; ++i)
                    {
                        var complex = new ComplexI32();
                        complex.Set(0, 3, i);

                        cmd = DBCommandBuilder.InsertCoMission(seed, complex.Value, beginTime);
                        var id = db.DoQuery<Int64>(cmd);

                        list.Add(new DBCoMission { ID = id, Seed = seed, Complex = complex.Value, BeginTime = beginTime });
                    }

                    info.Setup(list);
                }
            }

            info.Setup(provider);

            foreach (var mission in info.List)
            {
                if (mission.BeginTime > currentTime) continue;
                foreach (var group in Global.Groups)
                {
                    using (var playerDb = Global.DBPool.Acquire(group))
                    {
                        var cmd = DBCommandBuilder.SumCoMissionState(group, mission.Serial);
                        using (var res = playerDb.DoQuery(cmd))
                        {
                            var obj = DBObjectConverter.ConvertCurrent<DBSumCoMissionState>(res);
                            mission.Subjects[0].Progress += Decimal.ToInt32(obj.Subject1);
                            mission.Subjects[1].Progress += Decimal.ToInt32(obj.Subject2);
                            mission.Subjects[2].Progress += Decimal.ToInt32(obj.Subject3);
                            mission.Subjects[3].Progress += Decimal.ToInt32(obj.Subject4);
                            mission.Subjects[4].Progress += Decimal.ToInt32(obj.Subject5);
                        }
                    }
                }

                GlobalCooperateMissionInfo.Subtotal(mission);
            }

            return info;
        }

        public CooperateMissionInfo Clone(Dictionary<Int64, MissionState> list)
        {
            var info = new CooperateMissionInfo();
            info.Origin = list;
            info.List = new List<CooperateMission>();
            info.Map = new Dictionary<long, CooperateMission>();

            foreach (var item in List)
            {
                var obj = new CooperateMission { Origin = item.Origin, Serial = item.Serial, BeginTime = item.BeginTime, Point = item.Point, Progress = item.Progress, Rawdata = item.Rawdata };
                obj.Complex = new ComplexI32 { Value = item.Complex.Value };

                obj.Subjects = new List<MissionSubejct>(item.Subjects.Count);

                var progress = new int[5];

                var state = list.SafeGetValue(item.Serial);

                if (state != null)
                {
                    obj.State = state;

                    var _progress = new int[] { state.Origin.Subject1, state.Origin.Subject2, state.Origin.Subject3, state.Origin.Subject4, state.Origin.Subject5 };

                    for (int i = 0; i < 5; ++i)
                    {
                        var value = _progress[i];
                        state.Progress += value;
                        progress[i] = value;
                    }
                }
                else
                    obj.State = new MissionState();

                for (int i = 0; i < 5; ++i)
                {
                    var origin = item.Subjects[i];
                    var subject = new MissionSubejct { ID = origin.ID, Progress = progress[i], Rawdata = origin.Rawdata };
                    obj.Subjects.Add(subject);
                }

                info.List.Add(obj);
                info.Map.Add(obj.Serial, obj);
            }

            return info;
        }

        public GlobalCooperateMissionInfo Clone()
        {
            var info = new GlobalCooperateMissionInfo();
            
            foreach (var item in List) info.Setup(item.Origin);

            for (int i = 0; i < List.Count; ++i) List[i].CopyTo(info.List[i]);

            return info;
        }

        public static void Subtotal(CooperateMission mission)
        {
            mission.Point = 0;

            foreach (var item in mission.Subjects)
            {
                mission.Point += item.Progress * item.Rawdata.n_EFF;
            }

            mission.Progress = (int)((float)mission.Point / (float)mission.Rawdata.n_GOAL * 100);
        }
    }

    public class CooperateMission : Internal.CooperationMissionObj<MissionSubejct, MissionState>
    {
        public DBCoMission Origin;
        public ComplexI32 Complex;

        public ConstData.CoMission Rawdata;

        public bool IsReached { get { return Point >= Rawdata.n_GOAL; } }

        public void CopyTo(CooperateMission target)
        {
            target.Rawdata = Rawdata;
            target.Subjects = new List<MissionSubejct>(Subjects.Count);
            foreach (var item in Subjects) target.Subjects.Add(new MissionSubejct { ID = item.ID, Progress = item.Progress, Rawdata = item.Rawdata });
        }
    }

    public class MissionSubejct : Internal.MissionSubjectObj
    {
        public ConstData.CoMission Rawdata;
    }

    public class MissionState : Internal.MissionStateObj
    {
        public DBCoMissionState Origin;

        public int Progress;
    }

    public class RefreshCooperationMissionSchedule : ScheduleTask
    {
        public Service Owner;

        public CooperateMissionProvider Provider;

        public override void Execute(int times)
        {
            // 每3分鐘*20 全部重取一次
            if (times % 20 == 0)
            {
                var newObj = GlobalCooperateMissionInfo.Query(Provider, Global.CurrentTime);
                var task = new ReplaceCooperationMissionTask { NewObj = newObj };
                Owner.AddKernelEvent(task);
            }
            else
                Owner.AddKernelEvent(new MergeCooperationMissionTask());
        }
    }

    public class ReplaceCooperationMissionTask : Tasks.KernelTask
    {
        public GlobalCooperateMissionInfo NewObj;
        public override void Execute(Context context)
        {
            context.ReplaceCooperateMissionInfo(NewObj);
        }
    }

    public class MergeCooperationMissionTask : Tasks.KernelTask
    {
        public override void Execute(Context context)
        {
            context.MergeCooperateMissionInfo();
        }
    }
}
