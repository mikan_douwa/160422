﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mikan.CSAGA.Server
{
    public partial class Player
    {
        public Calc.Trophy Trophy;

        private List<Calc.TrophyCache> trophyCacheList = new List<Calc.TrophyCache>();

        public Queue<Tasks.TrophyTask> QueuedTrophyTasks = new Queue<Tasks.TrophyTask>();

        private void SetupTrophy()
        {
            Trophy = Calc.Trophy.Generate(new TrophySourceProvider { Player = this });
            Trophy.OnTrophyUpdate += new Action<Calc.TrophyCache>(OnTrophyUpdate);
        }

        private void OnTrophyUpdate(Calc.TrophyCache cache)
        {
            try
            {
                using (var db = Global.DBPool.Acquire(ID.Group))
                {
                    using (var trans = db.BeginTransaction())
                    {
                        var now = Global.CurrentTime;

                        var cmd = DBCommandBuilder.UpdateTrophy(ID, cache.Type, (int)cache.IDSet, cache.ID, cache.Progress, now);
                        db.SimpleQuery(cmd);

                        if (cache.IsFinish)
                        {
                            switch (cache.Type)
                            {
                                case TrophyType.Daily:
                                {
                                    var data = Global.Tables.Trophy[cache.ID];

                                    if (data.n_BONUS_LINK > 0)
                                    {
                                        if (cachedDailyInfo == null) cachedDailyInfo = DailyInfo;

                                        if (cachedDailyInfo.UpdateTime.Date != now.Date)
                                            cachedDailyInfo = DailyUtils.QueryDailyInfo(this);

                                        cmd = DBCommandBuilder.AddDailyCount(ID, data.n_BONUS_LINK);
                                        db.SimpleQuery(cmd);

                                        cachedDailyInfo.NeedUpdate = true;
                                    }
                                    break;
                                }
                                case TrophyType.DailyTrigger:
                                {
                                    var data = Global.Tables.Trophy[cache.ID];

                                    switch (data.s_CONDITION_TYPE)
                                    {
                                        case TrophyConditionType.DAILY_FAVORITE:
                                        {
                                            var card = CardInfo.Favorite;
                                            if (card != null)
                                            {
                                                var kizuna = Global.Tables.Trophy[cache.ID].n_BONUS_LINK;
                                                kizuna = Math.Min(card.Kizuna + kizuna, Global.Tables.KizunaLimit) - card.Kizuna;
                                                if (kizuna > 0)
                                                {
                                                    cmd = DBCommandBuilder.AddCardKizuna(ID, card.Serial, kizuna);
                                                    db.SimpleQuery(cmd);
                                                }
                                                Unsafe.KizunaDiff = kizuna;
                                            }
                                            break;
                                        }
                                        case TrophyConditionType.DAILY_FP_FANS:
                                        {
                                            var list = Tasks.SocialUtils.QueryFellowList(ID);
                                            var count = 0;
                                            foreach (var item in list)
                                            {
                                                if (ComplexI32.Check(item.Complex, 16)) ++count;
                                            }

                                            var origin = PlayerInfo[PropertyID.FP];
                                            var add = Math.Min(count * data.n_EFFECT[1], data.n_EFFECT[2]);
                                            var diff = Math.Min(origin + add, Global.Tables.FPMax) - origin;

                                            if (diff > 0)
                                            {
                                                cmd = DBCommandBuilder.AddFP(ID, diff);
                                                db.SimpleQuery(cmd);

                                                Unsafe.FPDiff = ValuePair.Create(origin, origin + diff);
                                            }

                                            Unsafe.FPAdd = add;
                                            
                                            break;
                                        }
                                    }

                                    
                                    break;
                                }
                                default:
                                {
                                    var gift = Calc.Trophy.ToGift(cache.ID, RewardSource.Trophy);
                                    cmd = DBCommandBuilder.InsertGift(ID, RewardSource.Trophy, gift.Description, gift.Type, gift.Value1, gift.Value2, now, gift.ExpireTime);
                                    db.SimpleQuery(cmd);
                                    break;
                                }
                            }
                            
                        }
                        
                        trans.Commit();
                    }
                }

                TrophyInfo.AddCache(cache);

            }
            catch (Exception e)
            {
                KernelService.Logger.Fatal("[Trophy] Internal Error, Group = {0}, ID = {1}, Progress = {2}, Msg = {3}", cache.IDSet, cache.ID, cache.Progress, e);
            }
        }

    }

    public class TrophyInfo : Internal.TrophyInfoObj<Trophy>
    {
        class Cache
        {
            public Calc.TrophyCache Origin;
            public DateTime Timestamp;
        }
        public HashSet<int> SimpleList;

        private List<Cache> cache;

        public TrophyInfo()
        {
            List = new Dictionary<int, Trophy>();
            SimpleList = new HashSet<int>();
        }

        public void Add(IEnumerable<DBTrophy> list, EventScheduleList schedule)
        {
            foreach (var item in list)
            {
                var data = Global.Tables.Trophy.SelectFirst(o => { return (int)o.n_IDSET == item.Group; });
                if (data == null) continue;
                var obj = new Trophy { Current = item.Current, Progress = item.Progress, UpdateTime = item.UpdateTime, Type = data.n_LEGACY_TYPE };

                if (obj.Type == TrophyType.EventPoint || obj.Type == TrophyType.EventRanking)
                {
                    // 使用同一個獎勵群組的活動,如果獎勵的領取時間小於目前進行中的活動,表示是上次活動的獎勵,就設成過期
                    foreach (var ev in schedule.SelectActiveSchedule(Global.CurrentTime))
                    {
                        if (((obj.Type == TrophyType.EventPoint && item.Group == ev.Origin.n_POINT) || (obj.Type == TrophyType.EventRanking && item.Group == ev.Origin.n_RANKING)) && ev.BeginTime > obj.UpdateTime)
                        {
                            obj.IsOverdue = true;
                            break;
                        }
                    }
                }
                
                else if (obj.Type == TrophyType.Challenge)
                {
                    if (obj.Current == 0) continue;
                }
                

                List.Add(item.Group, obj);
            }
        }

        public void Add(IEnumerable<DBSimpleTrophy> list)
        {
            foreach (var item in list) SimpleList.Add(item.TrophyID);
        }

        public void AddCache(Calc.TrophyCache obj)
        {
            if (cache == null) cache = new List<Cache>();
            cache.Add(new Cache { Origin = obj, Timestamp = Global.CurrentTime });
        }

        public void Commit()
        {
            if (cache == null) return;

            foreach (var item in cache)
            {
                if (item.Origin.Type == TrophyType.Immediately || item.Origin.Type == TrophyType.CreatePlayer)
                {
                    if (item.Origin.IsFinish) SimpleList.Add(item.Origin.ID);
                }
                else
                {
                    if (item.Origin.Type == TrophyType.Challenge && item.Origin.ID == 0) continue;
                    List[(int)item.Origin.IDSet] = new Trophy { Current = item.Origin.ID, Progress = item.Origin.Progress, UpdateTime = item.Timestamp, Type = item.Origin.Type };
                }
            }

            cache = null;
        }
    }

    public class Trophy : Internal.TrophyObj
    {
        public TrophyType Type;

        public bool IsOverdue;
    }

    class TrophySourceProvider : Calc.ITrophySourceProvider
    {
        public Player Player;

        public int PlayerExp { get { return Player.PlayerInfo[PropertyID.Exp]; } }

        public DateTime AccountCreateTime { get { return Player.AccountInfo.CreateTime; } }

        public int GalleryCount { get { return GetProgress(TrophyIDSet.GALLERY_NUM); } }

        public int AwakenRarityCount { get { return GetProgress(TrophyIDSet.AWAKEN_RARITY); } }

        public int AwakenAbilityCount { get { return GetProgress(TrophyIDSet.AWAKEN_ABILITY); } }

        public DateTime CurrentTime { get{ return Global.CurrentTime; } }

        public IEnumerable<Internal.CardObj> SelectCard()
        {
            foreach (var item in Player.CardInfo.List) yield return item;
        }

        public IEnumerable<int> SelectCardKizuna()
        {
            foreach (var item in Player.CardInfo.List) yield return item.Kizuna;

            if (Player.CardInfo.SellCacheList != null)
            {
                foreach (var item in Player.CardInfo.SellCacheList) yield return item.Kizuna;
            }
        }

        public Internal.TrophyObj GetTrophy(TrophyIDSet idset)
        {
            var item = Player.TrophyInfo.List.SafeGetValue((int)idset);

            if (item == null || item.IsOverdue) return null;
#if DEBUG
            if (item.Type == TrophyType.DailyFavorite && item.UpdateTime.Hour != CurrentTime.Hour) return null;
#endif
            if ((item.Type == TrophyType.Daily || item.Type == TrophyType.DailyTrigger) && item.UpdateTime.Date != CurrentTime.Date) return null;

            if (item.Type == TrophyType.DailyCounter)
            {
                if (item.Current == ConstData.Tables.Instance.DailyCounterLastTrophyID && item.UpdateTime.Date != CurrentTime.Date) return null;
            }

            if (item.Type == TrophyType.DailyLogin)
            {
                if (item.UpdateTime.Date != CurrentTime.Date) return null;
            }

            return item;
        }

        public bool IsTrophyFinished(int id)
        {
            return Player.TrophyInfo.SimpleList.Contains(id);
        }

        public Internal.QuestStateObj GetQuest(int id)
        {
            return Player.QuestInfo[id];
        }

        private int GetProgress(TrophyIDSet idset)
        {
            var trophy = GetTrophy(idset);
            return trophy != null ? trophy.Progress : 0;
        }

    }
}
