﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mikan.CSAGA.Server
{
    public partial class Context
    {

        public GachaMachineFactory GachaMachine;

        public GachaMachineFactory GachaJackpot;

        public Int64 GachaPromotionID;

        public Calc.GachaSchedule GachaSchedule;

        public void SetupGacha()
        {
            GachaMachine = new GachaMachineFactory();
            GachaJackpot = new GachaMachineFactory();
            foreach (var item in Global.Tables.GachaList.Select(o => { return o.n_TAG == GachaListTag.RegularGacha || o.n_TAG == GachaListTag.ActivityGacha || o.n_ID == Global.Tables.FirstGachaID; })) AddGacha(item.n_GACHAID);
            foreach (var item in Global.Tables.EventSchedule.Select(o => { return o.n_TYPE == EventScheduleType.Gacha && o.n_WEEKLY > 0; })) AddGacha(item.n_WEEKLY);

            GachaSchedule = Calc.Gacha.GetGachaSchdule();
        }

        public IGachaMachine GetGachaMachine(int gachaId)
        {
            return GachaMachine.Get(gachaId);
        }

        private void AddGacha(int group)
        {
            if (GachaMachine.Get(group) != null) return;

            var provider = new GachaDataProvider();

            var rare = new GachaDataProvider();

            var jackpot = new GachaDataProvider();

            foreach (var item in Global.Tables.Gacha.Select(o => { return o.n_GROUP == group; }))
            {
                provider.Add(item.n_ID, item.n_RATE, item.n_SHOWCARD == 2);

                if (item.n_SHOWCARD != 0)
                {
                    rare.Add(item.n_ID, item.n_RATE, item.n_SHOWCARD == 2);
                    if(item.n_SHOWCARD == 2)
                        jackpot.Add(item.n_ID, item.n_RARE, true);
                }
            }

            GachaMachine.Add(group, provider);

            // 如果有必中稀有卡設定則以群組的負數當key額外放置一台轉蛋
            if (rare.Basis > 0) GachaMachine.Add(-group, rare);

            if (jackpot.Basis > 0) GachaJackpot.Add(group, jackpot);
        }
    }
}
