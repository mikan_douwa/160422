﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mikan.CSAGA.Server
{
    public partial class Player
    {

        public Platform Platform;

        public string Transaction;

        public bool IsMyCard = false;

        public string IAPName;

        #region Game

        public UnsafeVariable Unsafe = new UnsafeVariable();

        public DBAccount AccountInfo;

        public PlayerInfo PlayerInfo;

        public CardInfo CardInfo;

        public ItemInfo ItemInfo;

        public EquipmentInfo EquipmentInfo;

        public PurchaseInfo PurchaseInfo;

        public QuestInfo QuestInfo;

        public GiftInfo GiftInfo;

        public TrophyInfo TrophyInfo;

        public DailyInfo DailyInfo;

        public RandomShopInfo RandomShopInfo;

        public IAPInfo IAPInfo;

        public CooperateMissionInfo CooperateMissionInfo;

        public SocialInfo SocialInfo;

        public FaithInfo FaithInfo;

        public TacticsInfo TacticsInfo;

        public DBMyLord MyLord;

        public DBDailyEP DailyEP;

        public List<DBSocialPurchase> SocialPurchaseList;

        /*

        public LocalDataInfo LocalDataInfo;

        public AuctionInfo AuctionInfo;

        public int PrevCharaID = 0;

        public int CharaIDRepeatCount = 0;

        public DBDailyLogin DailyLogin;

        public bool IsAPFull(DateTime currentTime)
        {
            return APConsumed(currentTime) == 0;
        }

        public int APConsumed(DateTime currentTime)
        {
            return PlayerInfo.CalcAPConsumed(currentTime, Global.APRecoverSeconds);
        }
        */

        #endregion

        public void Init()
        {
            SetupTrophy();
        }

        private DailyInfo cachedDailyInfo;

        public void CommitCache()
        {
            if (cachedDailyInfo != null)
            {
                DailyInfo = cachedDailyInfo;
                cachedDailyInfo = null;
            }
        }
        #region Internal

        public bool Forbidden = false;

        public PublicID ID;

        public Token Token;

        public DateTime LastUse;

        public DateTime LastPlayTime;

        public bool UseSuperPrivilege;

        public ICommand LastRequest;
        public ICommand LastResponse;

        public object Owner;


        public Context Context;

        public bool IsBusy { get { return Owner != null && DateTime.Now.Subtract(LastUse).TotalSeconds < 30; } }

        //public bool IsAvailable { get { return PlayerInfo != null && ((PlayerInfo.Obj.status & PlayerFlag.CREATE) != 0); } }

        /*
        public ErrorCode IsCoinEnough(CoinType coinType, int cost, out ValuePair diff)
        {
            diff = null;

            switch (coinType)
            {
                case CoinType.FP:
                    if (PlayerInfo.Obj.fp < cost) return ErrorCode.FPNotEnough;
                    diff = ValuePair.Create(PlayerInfo.Obj.fp, PlayerInfo.Obj.fp - cost);
                    break;
                case CoinType.GEM:
                    if (PurchaseInfo.Gem < cost) return ErrorCode.GemNotEnough;
                    diff = ValuePair.Create(PurchaseInfo.Gem, PurchaseInfo.Gem - cost);
                    break;
                case CoinType.MONEY:
                    if (PlayerInfo.Obj.money < cost) return ErrorCode.MoneyNotEnough;
                    diff = ValuePair.Create(PlayerInfo.Obj.money, PlayerInfo.Obj.money - cost);
                    break;
                case CoinType.COIN:
                    if (PlayerInfo.Obj.coin < cost) return ErrorCode.CoinNotEnough;
                    diff = ValuePair.Create(PlayerInfo.Obj.coin, PlayerInfo.Obj.coin - cost);
                    break;
            }

            return ErrorCode.Success;
        }

        public static int Compare(Player lhs, Player rhs)
        {
            return lhs.PlayerInfo.LV.CompareTo(rhs.PlayerInfo.LV);
        }
        */
        #endregion
    }

    public enum Platform
    {
        Unknown,
        iOS,
        Android,
    }

    /// <summary>
    /// 拿來放沒有緒安處理的變數,要確定使用時不會有多緒同時存取
    /// </summary>
    public class UnsafeVariable
    {
        public int KizunaDiff;
        public int FPAdd;
        public ValuePair FPDiff;
    }

    public class PlayerInfo : Internal.PlayerInfoObj
    {
        public string Name { get { return name; } set { name = value; } }
        public int TPConsumed { get { return tpConsumed; } }
        public bool GachaRefund { get { return complex[ComplexIndex.PlayerGachaRefund]; } }
        public Int64 Complex { get { return complex.Value; } }
        public void Setup(DBPlayer obj)
        {
            complex = new Complex { Value = obj.Complex };
            name = obj.Name;
            tpConsumed = obj.TPConsumed;
            tpChangeTime = obj.TPChangeTime;
            list[PropertyID.Exp] = obj.Exp;
            list[PropertyID.Crystal] = obj.Crystal;
            list[PropertyID.Coin] = obj.Coin;
            list[PropertyID.FP] = obj.FP;
            
        }

        public void UpdateComplex(int index, bool flag)
        {
            complex[index] = flag;
        }

        public void UpdateComplex(int index, int len, int val)
        {
            complex.Set(index, len, val);
        }

        public void UpdateTPConsumed(int consumed, DateTime changeTime)
        {
            tpConsumed = consumed;
            tpChangeTime = changeTime;
        }

        public void DebugEdit(string propty, string val)
        {
            switch (propty)
            {
                case "exp":
                    list[PropertyID.Exp] = int.Parse(val);
                    break;
                case "crystal":
                    list[PropertyID.Crystal] = int.Parse(val);
                    break;
                case "coin":
                    list[PropertyID.Coin] = int.Parse(val);
                    break;
                case "tp_consumed":
                    tpConsumed = int.Parse(val);
                    tpChangeTime = Global.CurrentTime;
                    break;
                case "name":
                    name = val;
                    break;
            }
        }

        public PlayerInfo Clone()
        {
            var copy = new PlayerInfo();
            copy.DeSerialize(this.Serialize());
            return copy;
        }
    }

    public class CardInfo : Internal.CardInfoObj<Card>
    {
        public Card Favorite
        {
            get
            {
                if (favorite != null) return favorite;

                return List.Count > 0 ? List[0] : null;
            }
        }
        public Card Header
        {
            get
            {
                if (header != null) return header;

                return List.Count > 0 ? List[0] : null;
            }
        }
        public DBCardEventCache Current;

        public List<DBSellCache> SellCacheList;

        private Dictionary<Int64, Card> map;

        private Card favorite;
        private Card header;

        public Card this[Int64 serial]
        {
            get
            {
                var card = default(Card);
                if (map.TryGetValue(serial, out card)) return card;
                return card;
            }
        }

        public void Setup(IEnumerable<DBCard> list)
        {
            List = new List<Card>();
            map = new Dictionary<long, Card>();

            foreach (var item in list)
            {
                var card = new Card
                {
                    ID = item.CardID,
                    Serial = Serial.Create(item.ID),
                    Complex = new Complex { Value = item.Complex },
                    Exp = item.Exp,
                    Kizuna = item.Kizuna
                };

                Add(card);
            }
        }

        public void Setup(IEnumerable<DBCardEventCache> list)
        {
            cacheList = new List<Internal.CardEventCache>();
            foreach (var item in list)
            {
                var cache = new Internal.CardEventCache
                {
                    Serial = Serial.Create(item.CardID),
                    InteractiveID = item.InteractiveID
                };
                cacheList.Add(cache);

                // 取得最後一個由卡片觸發的任務
                if (Current == null || item.QuestID > Current.QuestID) Current = item;
            }
        }

        public void Setup(IEnumerable<DBGallery> list)
        {
            luckList = new Dictionary<int, int>();

            foreach (var item in list) luckList[item.CardID] = item.Luck;
        }

        public void AddLuck(int cardId, int luck)
        {
            var origin = 0;
            if (luckList.TryGetValue(cardId, out origin))
                luckList[cardId] = origin + luck;
            else
                luckList[cardId] = luck;
        }

        public void Add(Card card)
        {
            Add(card, 0);
        }

        public void Add(Card card, int luckDiff)
        {
            if (card.Complex == null) card.Complex = new Complex();
            List.Add(card);
            map.Add(card.Serial.Value, card);

            if (luckDiff > 0)
            {
                var luck = 0;
                if (luckList.TryGetValue(card.ID, out luck))
                    luck += luckDiff;
                else
                    luck = luckDiff;

                luckList[card.ID] = luck;
            }
        }
        public void AddRange(List<Card> list)
        {
            foreach (var item in list) Add(item, 0);
        }

        public void RemoveEventCache(Serial serial)
        {
            for (int i = 0; i < cacheList.Count; ++i)
            {
                if (cacheList[i].Serial == serial)
                {
                    cacheList.RemoveAt(i);
                    break;
                }
            }
        }
        public void AddEventCache(Serial serial, int interactiveId)
        {
            RemoveEventCache(serial);
            cacheList.Add(new Internal.CardEventCache { Serial = serial, InteractiveID = interactiveId });
        }

        public int GetEventCache(Int64 serial)
        {
            foreach (var item in cacheList)
            {
                if (item.Serial.Value == serial) return item.InteractiveID;
            }
            return 0;
        }

        public void SetFavorite(Int64 serial)
        {
            favorite = this[serial];
        }

        public void SetHeader(Int64 serial)
        {
            header = this[serial];
        }

        public void DebugEdit(string index, string property, string val)
        {
            var card = List[int.Parse(index)];
            switch (property)
            {
                case "exp": card.Exp = int.Parse(val); break;
                case "rare": card.Rare = int.Parse(val); break;
            }
        }
    }

    public class Card : Internal.CardObj
    {
    }

    public class ItemInfo : Internal.ItemInfoObj<Item>
    {
        private Dictionary<int, Item> map;

        public void Setup(IEnumerable<DBItem> list)
        {
            List = new List<Item>();
            map = new Dictionary<int, Item>();
            foreach (var item in list)
            {
                var obj = new Item
                {
                    ID = item.ItemID,
                    Count = item.Count,
                };
                List.Add(obj);
                map.Add(obj.ID, obj);
            }
        }

        public int Count(int id)
        {
            var item = default(Item);

            if (map.TryGetValue(id, out item)) return item.Count;

            return 0;
        }

        public void Add(int id, int count)
        {
            var item = default(Item);

            if (map.TryGetValue(id, out item))
                item.Count += count;
            else
            {
                item = new Item { ID = id, Count = count };
                map.Add(item.ID, item);
                List.Add(item);
            }   
        }

        public void Delete(int id)
        {
            var item = default(Item);

            if (map.TryGetValue(id, out item)) item.Count = 0;
        }
    }

    public class Item : Internal.ItemObj
    {
    }

    public class EquipmentInfo : Internal.EquipmentInfoObj<Equipment>
    {
        private Dictionary<Int64, Equipment> map;

        public Equipment this[Int64 serial]
        {
            get { return map.SafeGetValue(serial); }
        }

        public void Setup(IEnumerable<DBEquipment> list)
        {
            List = new List<Equipment>();
            map = new Dictionary<Int64, Equipment>();
            foreach (var item in list)
            {
                var obj = Equipment.Create(item);
                Add(obj);
            }
        }


        public void Add(Equipment obj)
        {
            List.Add(obj);
            map.Add(obj.Serial, obj);
        }

        public void Remove(Equipment obj)
        {
            List.Remove(obj);
            map.Remove(obj.Serial);
        }
    }

    public class Equipment : Internal.EquipmentObj
    {
        public PropertyCollection Properties { get { return list; } }
        public static Equipment Create(DBEquipment src)
        {
            var obj = new Equipment { Serial = src.ID, CardSerial = Mikan.Serial.Create(src.CardID) };
            obj[PropertyID.ID] = src.ItemID;
            
            if (src.Hp != 0)    obj.Hp = src.Hp;
            if (src.Atk != 0)   obj.Atk = src.Atk;
            if (src.Rev != 0)   obj.Rev = src.Rev;
            if (src.Chg != 0)   obj.Chg = src.Chg; 

            if (src.Slot != 0)      obj.Slot = src.Slot;
            if (src.EffectID != 0)  obj.EffectID = src.EffectID;

            if (src.Counter != 0) obj.Counter = src.Counter;
            if (src.CounterType != 0) obj.CounterType = src.CounterType;
            return obj;
        }
    }

    public class PurchaseInfo : Internal.PurchaseInfoObj<Article>
    {
        private Dictionary<ArticleID, Article> articles;

        private int cost = 0;

        private int gem = 0;

        private int iapCost = 0;

        private int iapGem = 0;

        private int blackGem = 0;

        public int OriginIAPGem { get { return iapGem; } }

        public bool MyLord { get; private set; }

        public void Setup(IEnumerable<DBPurchase> list)
        {
            articles = new Dictionary<ArticleID, Article>();

            foreach (var item in list) AddArticle(item);

            List = new List<Article>(articles.Count);

            foreach (var item in articles) List.Add(item.Value);
        }

        public Article GetArticle(ArticleID id)
        {
            Article article;
            if (articles.TryGetValue(id, out article)) return article;

            return null;
        }

        public int Count(ArticleID id)
        {
            var article = GetArticle(id);
            return article != null ? article.Count : 0;
        }

        public void AddArticle(ArticleID id, int cost, int iapCost, DateTime timestamp)
        {
            Article article;
            if (!articles.TryGetValue(id, out article))
            {
                article = new Article { ID = id };
                articles.Add(id, article);
            }

            ++article.Count;

            this.cost += cost;

            this.iapCost += iapCost;

            if (timestamp > article.LastPurchase) article.LastPurchase = timestamp;

            Gem = gem - this.cost;

            IAPGem = iapGem - this.iapCost;
        }

        public void AddArticle(DBPurchase item)
        {
            AddArticle((ArticleID)item.ArticleID, item.Cost, item.IAPCost, item.Timestamp);
        }

        public void AddGem(int gem)
        {
            this.gem += gem;

            Gem = this.gem - cost;
        }

        public void SetIAPGem(int gem)
        {
            this.iapGem = gem;

            IAPGem = this.iapGem - iapCost;

            UpdateMyLord();
        }

        public void AddIAPGem(int gem)
        {
            AddGem(gem);

            this.iapGem += gem;

            IAPGem = this.iapGem - iapCost;

            UpdateMyLord();
        }

        public void SetBlackGem(int gem)
        {
            this.blackGem = gem;

            UpdateMyLord();
        }

        private void UpdateMyLord()
        {
            if ((this.iapGem - this.blackGem) >= 4000) MyLord = true;
        }
    }

    public class Article : Internal.ArticleObj
    {
        
    }

    public class QuestInfo : Internal.QuestInfoObj<QuestState>
    {
        public HashSet<Int64> CrusadeCacheList;

        public QuestCache Current;

        public Int64 QuestCacheBeginSerial;

        public Dictionary<Int64, Infinity> InfinityList;

        public Crusade Crusade;

        public List<SharedQuest> SharedList;

        private Dictionary<int, QuestState> map;

        private Dictionary<int, LimitedQuest> limited;

        private HashSet<PublicID> cache;

        public bool IsUsed(PublicID id)
        {
            return cache.Contains(id);
        }

        public LimitedQuest GetLimited(int questId)
        {
            return limited.SafeGetValue(questId);
        }

        public IEnumerable<Internal.LimitedQuestObj> SelectLimited()
        {
            foreach (var item in limited) yield return item.Value;
        }

        public QuestState this[int questId]
        {
            get { return Get(questId); }
        }

        public QuestState Get(int questId)
        {
            return map.SafeGetValue(questId);
        }

        public void Remove(int questId)
        {
            var item = this[questId];

            if (item != null)
            {
                List.Remove(item);
                map.Remove(item.QuestID);
            }
        }

        public void Setup(IEnumerable<DBQuest> list)
        {
            List = new List<QuestState>();
            map = new Dictionary<int, QuestState>();
            foreach (var item in list)
            {
                var obj = new QuestState
                {
                    QuestID = item.QuestID,
                    Complex = new ComplexI32 { Value = item.Complex },
                    LastPlayTime = item.LastPlayTime
                };
                Add(obj);
            }
        }

        public void Setup(IEnumerable<DBLimitedQuest> list)
        {
            limited = new Dictionary<int, LimitedQuest>();

            foreach (var item in list)
            {
                var obj = new LimitedQuest { Serial = item.ID, QuestID = item.QuestID, EndTime = item.EndTime, PlayCount = item.PlayCount };
                limited.Add(obj.QuestID, obj);
            }
        }

        public void Setup(IEnumerable<DBQuestCache> list)
        {
            cache = PublicID.CreateHashSet();

            var today = Global.CurrentTime.Date;

            var current = default(DBQuestCache);

            var tmp = new QuestCacheData();

            foreach (var item in list)
            {
                if (current == null || current.Timestamp < item.Timestamp) current = item;

                if (item.Timestamp < today) continue;

                if (QuestCacheBeginSerial == 0 || QuestCacheBeginSerial > item.ID) QuestCacheBeginSerial = item.ID;

                tmp.DeSerialize(item.Data);
                if (tmp.Fellow == null) continue;

                cache.Add(tmp.Fellow);
            }

            Current = new QuestCache(current);
        }

        public void Setup(IEnumerable<DBCrusadeCache> list)
        {
            CrusadeCacheList = new HashSet<long>();
            foreach (var item in list) CrusadeCacheList.Add(item.QuestID);
        }

        public void Setup(DBRescue rescue)
        {
            if (rescue != null)
            {
                RescueCount = rescue.Count;
                RescueUpdateTime = rescue.UpdateTime;
            }
            else
                RescueUpdateTime = DateTime.MinValue;
        }

        public void Setup(IEnumerable<DBInfinity> list, EventScheduleList eventSchedule)
        {
            InfinityList = new Dictionary<long, Infinity>();

            foreach (var item in list)
            {
                var schedule = eventSchedule.Get(item.EventID);
                if (schedule == null) continue;
                var obj = new Infinity { ID = item.ID, Schedule = schedule, EventID = schedule.EventID, Point = item.Point, UpdateTime = item.UpdateTime };
                InfinityList.Add(schedule.Relative.ID, obj);
            }
        }

        public void Commit(QuestCache cache)
        {
            if (cache.Data.Fellow != null) this.cache.Add(cache.Data.Fellow);
        }

        public void UpdateLimitedQuest(LimitedQuest obj)
        {
            limited[obj.QuestID] = obj;
        }

        public void UpdateQuest(int questId, bool isSucc, int complex, DateTime now)
        {
            var state = this[questId];

            if (state == null)
            {
                state = new QuestState { QuestID = questId, LastPlayTime = now, Complex = new ComplexI32() };
                Add(state);
            }

            if (isSucc)
            {
                if (state.Complex == null) state.Complex = new ComplexI32();
                state.Complex.Value |= complex;
            }
        }

        private void Add(QuestState state)
        {
            List.Add(state);
            map.Add(state.QuestID, state);
        }
    }

    public class QuestState : Internal.QuestStateObj
    {
    }

    public class QuestCache
    {
        public DBQuestCache Obj { get; private set; }
        public ComplexI32 Complex { get; private set; }
        public QuestCacheData Data { get; private set; }
        public QuestCache(DBQuestCache obj)
        {
            Obj = obj;
            if (Obj == null) Obj = new DBQuestCache();
            Complex = new ComplexI32 { Value = Obj.Complex };
            Data = new QuestCacheData();
            if (!string.IsNullOrEmpty(Obj.Data))
                Data.DeSerialize(Obj.Data);
        }
    }

    public class LimitedQuest : Internal.LimitedQuestObj
    {
        public Int64 Serial;
    }

    public class SharedQuest : Internal.SharedQuestObj
    {
        public PublicID PublicID;
        public DBSharedQuest Origin;

        public static SharedQuest Create(DBSharedQuest origin)
        {
            var obj = new SharedQuest { Origin = origin };
            obj.PublicID = PublicID.Create(origin.GroupID, origin.PlayerID);
            obj.PlayerID = obj.PublicID.ToString();
            obj.Serial = origin.ID;
            obj.QuestID = origin.QuestID;
            obj.EndTime = origin.EndTime;
            obj.PlayCount = origin.PlayCount;

            return obj;
        }

        public SharedQuest Clone()
        {
            var obj = new SharedQuest { Origin = Origin };
            obj.PublicID = PublicID;
            obj.PlayerID = PlayerID;
            obj.Serial = Serial;
            obj.QuestID = QuestID;
            obj.EndTime = EndTime;
            obj.PlayCount = PlayCount;

            return obj;
        }
    }

    public class Crusade : Internal.CrusadeObj<SharedQuest>
    {
        public bool IsValid { get; private set; }

        public EventScheduleObj Schedule;

        public static Crusade Query(Mikan.Server.IDBConnection db, PublicID id, EventScheduleObj schedule, DateTime currentTime)
        {
            var crusade = new Crusade();
            crusade.Schedule = schedule;

            var cmd = DBCommandBuilder.SelectCrusade(id);
            using (var res = db.DoQuery(cmd))
            {
                if (!res.IsEmpty)
                {
                    var origin = DBObjectConverter.ConvertCurrent<DBCrusade>(res);

                    crusade.Discover = origin.Discover;
                    crusade.Point = origin.Point;
                    crusade.UpdateTime = origin.UpdateTime;

                    crusade.IsValid = true;
                }
            }

            if (!crusade.IsValid && schedule != null && schedule.BeginTime <= currentTime && schedule.EndTime > currentTime)
            {
                // 討伐活動結束後會用排程把資料清掉,這裡只需要做插入新資料的動作
                
                crusade.Point = Global.Tables.CrusadeInital * Global.Tables.CrusadeScale;
                crusade.UpdateTime = currentTime;
                crusade.IsValid = true;

                cmd = DBCommandBuilder.InsertCrusade(id, crusade.Point, crusade.UpdateTime);
                db.SimpleQuery(cmd);
            }

            if (!crusade.IsValid)
                crusade.UpdateTime = currentTime;

            return crusade;
        }

        public int CalcPoint(DateTime currentTime)
        {
            return Calc.Quest.CalcCrusadeCount(Point, UpdateTime, Schedule != null ? Schedule.BeginTime : currentTime, currentTime);
        }

        public Crusade Clone()
        {
            var copy = new Crusade { Schedule = Schedule, Discover = Discover, Point = Point, UpdateTime = UpdateTime, IsValid = IsValid };

            return copy;
        }
    }

    public class GiftInfo : Internal.GiftInfoObj<Gift>
    {
        private GiftInfo() { }
        public GiftInfo(IEnumerable<DBGift> list)
        {
            var now = Global.CurrentTime;

            List = new List<Gift>();

            foreach (var item in list)
            {
                if (item.BeginTime > now) continue;

                var gift = new Gift
                {
                    Serial = item.ID,
                    Type = (DropType)item.Type,
                    Description = item.Description,
                    Value1 = item.Value1,
                    Value2 = item.Value2,
                    ExpireTime = item.EndTime,
                    Source = (RewardSource)item.Source
                };

                List.Add(gift);
            }
        }

        public GiftInfo Clone()
        {
            var giftInfo = new GiftInfo();
            giftInfo.List = new List<Gift>(List);
            return giftInfo;
        }
    }

    public class Gift : Internal.GiftObj
    {
        public RewardSource Source;

    }

    public class DailyInfo : Internal.DailyInfoObj
    {
        public Int64 Seed;

        public bool IsOverdue { get { return NeedUpdate || UpdateTime.Date != Global.CurrentTime.Date; } }

        public bool NeedUpdate = false;

        public int Remains
        {
            get
            {
                return Global.Tables.DailyActiveCount - Consumed + Add;
            }

        }
        public DailyInfo()
        {
            State = new ComplexI32();
            UpdateTime = DateTime.MinValue;
        }

        public void Update(DBDaily obj)
        {
            if (obj == null) return;
            Seed = obj.Seed;
            State = new ComplexI32 { Value = obj.State };
            Consumed = obj.Consumed;
            Add = obj.Add;
            Cached = obj.Cached;
            UpdateTime = obj.UpdateTime;
        }

        protected override void Serialize(IOutput output)
        {
            base.Serialize(output);
        }
    }

    public class RankingInfo : Internal.RankingInfoObj<Ranking>
    {
        private PublicID id;

        public RankingInfo()
        {
            List = new Dictionary<int, Ranking>();
        }
        public void Setup(EventScheduleList schedule, IEnumerable<DBEventPoint> items)
        {
            foreach (var item in items)
            {
                var current = schedule.Get(item.EventID);

                if (current == null) continue;

                if (current.EndTime < Global.CurrentTime && current != schedule.GetCurrent(current.MainQuestID)) continue;

                var obj = new Ranking { EventID = current.Relative.DataID, Point = item.Point, BeginTime = current.Relative.BeginTime, Complex = new ComplexI32 { Value = item.Complex } };

                if (current.Players != null)
                {
                    obj.PlayerCount = current.Ranking.Count;

                    var point = current.Players.SafeGetValue(id);

                    if (point != null && current.Origin.n_RANKING > 0)
                    {
                        obj.Ranking = point.Ranking;

                        var percent = (((float)obj.Ranking / (float)current.Ranking.Count) * 100);

                        var prev = default(ConstData.Trophy);

                        if (percent > 0)
                        {
                            var list = Global.Tables.RankingPeriods[current.Origin.n_RANKING];
                            foreach (var period in list)
                            {
                                if (percent > period.n_EFFECT[0] && percent <= period.n_EFFECT[1]) break;

                                prev = period;
                            }
                        }

                        if (prev != null)
                        {
                            var next = (int)((prev.n_EFFECT[1] / 100f) * current.Ranking.Count);

                            var nextPoint = current.Ranking[next];

                            obj.Next = nextPoint.Value - obj.Point;
                        }
                    }
                    else
                        obj.Ranking = -1;
                }
                else
                {
                    obj.PlayerCount = current.Relative.PlayerCount;
                    obj.Ranking = item.Ranking;
                }

                List[obj.EventID] = obj;

                KernelService.Logger.Debug("add event: ID = {0}, Serial = {1}", obj.EventID, current.Relative.ID);
                
            }

            foreach (var item in schedule.SelectCurrentSchedule())
            {
                if (List.ContainsKey(item.EventID)) continue;
                var obj = new Ranking { EventID = item.Relative.DataID, BeginTime = item.Relative.BeginTime, Ranking = -1 };
                if (item.Players != null)
                    obj.PlayerCount = item.Players.Count;
                else
                    obj.PlayerCount = item.Relative.PlayerCount;

                List[obj.EventID] = obj;

                KernelService.Logger.Debug("add placement event: ID = {0}, Serial = {1}", obj.EventID, item.Relative.ID);
            }
        }

        public static RankingInfo Query(Mikan.Server.IDBConnection db, EventScheduleList schedule, PublicID id)
        {
            var inst = new RankingInfo();
            inst.id = id;

            var cmd = DBCommandBuilder.SelectEventPoint(id, schedule.MinSerial);
            using (var res = db.DoQuery(cmd))
            {
                inst.Setup(schedule, DBObjectConverter.Convert<DBEventPoint>(res));
            }

            return inst;
        }
    }

    public class Ranking : Internal.RankingObj
    {
        public ComplexI32 Complex;
    }

    public class IAPInfo : Internal.IAPInfoObj<IAPProduct>
    {
        public Dictionary<int, DBIAPHistory> History;

        public void Setup(IEnumerable<DBIAPHistory> list)
        {
            History = new Dictionary<int, DBIAPHistory>();

            foreach (var item in list) History.Add(item.ProductID, item);
        }
    }

    public class IAPProduct : Internal.IAPProductObj
    {
        public ConstData.IAPList Origin;

        public int ID { get { return Origin.n_ID; } }

        public int Gem { get { return Origin.n_GEM; } }

        public int Bonus { get { return Origin.n_GEM_BONUS; } }

        public int Platform { get { return Origin.n_PLATFORM; } }

        public int Dollars { get { return Origin.n_PRICE; } }

        public bool IsMyCard { get { return (Platform & 0x01) == 0; } }
    }

    public class Infinity
    {
        public Int64 ID;
        public EventScheduleObj Schedule;
        public int EventID;
        public int Point;
        public DateTime UpdateTime;
    }

    public class FaithInfo : Internal.FaithInfoObj<Faith>
    {
        public DBFaithClaim Origin;

        public void Setup(IEnumerable<DBFaith> list)
        {
            List = new List<Faith>();
            foreach (var item in list)
            {
                var obj = new Faith { Origin = item };
                obj.Serial = item.ID;
                obj.Index = item.Index;
                obj.Result = item.Result;
                obj.EndTime = item.EndTime;
                List.Add(obj);
            }
        }
    }

    public class Faith : Internal.FaithObj
    {
        public DBFaith Origin;
    }

    public class TacticsInfo : Internal.TacticsInfoObj
    {
        private Dictionary<int, ConstData.Tactics> groups = new Dictionary<int, ConstData.Tactics>();
        
        public void Setup(IEnumerable<DBTactics> list)
        {
            List = new List<int>();
            foreach (var item in list) Add(item.TacticsID);
        }

        public ConstData.Tactics this[int group]
        {
            get
            {
                return groups.SafeGetValue(group);
            }
        }

        public IEnumerable<ConstData.Tactics> Select()
        {
            foreach (var item in groups) yield return item.Value;
        }

        public ConstData.Tactics Select(TacticsEffect effect)
        {
            foreach (var item in groups)
            {
                if (item.Value.n_EFFECT == effect) return item.Value;
            }

            return null;
        }

        public bool Contains(int tacticsId)
        {
            return List.Contains(tacticsId);
        }

        public void Add(int tacticsId)
        {
            var data = Global.Tables.Tactics[tacticsId];

            var origin = groups.SafeGetValue(data.n_GROUP);

            if (origin == null || origin.n_LV < data.n_LV)
                groups[data.n_GROUP] = data;

            if (!List.Contains(tacticsId)) List.Add(tacticsId);
        }
       
    }
}
