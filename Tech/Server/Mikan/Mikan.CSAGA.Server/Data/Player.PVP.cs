﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mikan.CSAGA.Server
{
    public partial class Player
    {
        public PVPInfo PVPInfo;

        public DateTime PVPQueryTime = DateTime.MinValue;
    }

    public class PVPInfo
    {
        public int BPConsumed;

        public DateTime BPUpdateTime;

        public PVPWaves Waves;

        public Int64 EventID;

        public int Point;

        public HashSet<int> Skills = new HashSet<int>();

        public Dictionary<int, int> SkillLevels = new Dictionary<int, int>();

        public void Setup(PVPContext context)
        {
            if (context.Origin != null)
            {
                BPConsumed = context.Origin.BPConsumed;
                BPUpdateTime = context.Origin.BPUpdateTime;
            }

            EventID = context.EventID;
            Point = context.Point;
            Waves = context.Waves;
        }

        public void AddSkill(DBPVPSkill skill)
        {
            Skills.Add(skill.SkillID);
            var data = Global.Tables.Skill[skill.SkillID];
            var current = SkillLevels.SafeGetValue(data.n_TYPE);
            if (current < data.n_CD) SkillLevels[data.n_TYPE] = data.n_ID;
            
        }

    }

    public class PVPPlayer : Internal.PVPPlayerObj<PVPWave>
    {
        public PublicID ID;
    }

    public class PVPTicket : SerializableObject
    {
        private const Int64 KEY = 348031734293109;

        private Int64 value;
        private Token token;
        private PublicID id;

        public Token Token { get { return token; } }
        public PublicID PlayerID { get { return id; } }
        public static PVPTicket Create(Token token, PublicID playerId)
        {
            var ticket = new PVPTicket();
            ticket.token = token;
            ticket.id = playerId;
            ticket.value = token.ID.Serial ^ playerId.Serial ^ KEY;

            return ticket;
        }

        public static PVPTicket FromString(string text)
        {
            var ticket = new PVPTicket();
            ticket.DeSerialize(text);
            return ticket;
        }

        protected override void Serialize(IOutput output)
        {
            output.Write(value);
            output.Write(id.Group ^ value);
            output.Write(id.Serial ^ value);
            output.Write(token.Text);
        }

        protected override void DeSerialize(IInput input)
        {
            value = input.ReadInt64();
            var group = (int)(input.ReadInt64() ^ value);
            var serial = input.ReadInt64() ^ value;
            id = PublicID.Create(group, serial);
            token = PublicID.ParseToken(input.ReadString());
        }
    }

    public static class PVPServerUtils
    {
        public static int Calc(int winner, int loser, int basePoint, int extraPoint)
        {
            var _winner = winner + 1000f;
            var _loser = loser + 1000f;

            var extra = 0;
            if (_winner > 0)
            {
                var diff = Math.Min(1f, (_loser / _winner) - 1f);
                var extraf = extraPoint * diff;
                if (extraf >= 0)
                    extra = (int)Math.Floor(extraf);
                else
                    extra = (int)Math.Ceiling(extraf);
            }
            else
            {
                extra = extraPoint;
            }

            return Math.Max(1, basePoint + extra);
        }

        public static DBPVPBattle QueryBattle(PublicID id, Int64 questId)
        {
            using (var db = Global.DBPool.Acquire(id.Group))
            {
                var cmd = DBCommandBuilder.SelectPVPBattle(id, questId);
                using (var dbRes = db.DoQuery(cmd))
                {
                    if (!dbRes.IsEmpty) return DBObjectConverter.ConvertCurrent<DBPVPBattle>(dbRes);
                }
            }
            return null;
        }
    }
}
