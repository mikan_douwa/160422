﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mikan.CSAGA.Server
{
    public partial class Player
    {
        public Snapshot Snapshot()
        {
            var obj = new Snapshot();

            obj.Name = PlayerInfo.Name;
            obj.Exp = PlayerInfo[PropertyID.Exp];

            var favorite = CardInfo.Header;
            obj.Favorite = new Snapshot.Card { Obj = favorite, Luck = CardInfo.GetLuck(favorite.ID) };
            obj.Equipments = new List<Snapshot.Equipment>();
            foreach (var item in EquipmentInfo.List.FindAll((o) => o.CardSerial == favorite.Serial))
            {
                obj.Equipments.Add(new Snapshot.Equipment { list = item.list });
            }
            return obj;
        }

        public void UpdateSnapshot(DateTime currentTime)
        {
            var obj = Snapshot();

            Context.Owner.AddTask(new Tasks.UpdateSnapshotTask { ID = ID, Data = obj.Serialize(), UpdateTime = currentTime });
        }
    }

    public class SocialInfo : Internal.SocialInfoObj<Fellow>
    {
        public List<DBFellow> Origins;
    }

    public class Fellow : Internal.FellowObj
    {
        public PublicID ID;
    }

    public class Snapshot : Internal.SnapshotObj<Card>
    {
    }
}

namespace Mikan.CSAGA.Server.Tasks
{
    public class UpdateSnapshotTask : ITask
    {
        public PublicID ID;
        public string Data;
        public DateTime UpdateTime;
        public void Execute()
        {
            using (var db = Global.DBPool.Acquire(ID.Group))
            {
                var cmd = string.IsNullOrEmpty(Data) ? 
                    DBCommandBuilder.UpdateSnapshot(ID, UpdateTime) :
                    DBCommandBuilder.UpdateSnapshot(ID, Data, UpdateTime);

                db.SafeSimpleQuery(cmd);
            }
        }

        public void Dispose() { }
    }
    
}
