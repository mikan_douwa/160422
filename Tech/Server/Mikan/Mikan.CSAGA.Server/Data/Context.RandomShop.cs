﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mikan.Server;

namespace Mikan.CSAGA.Server
{
    public partial class Context
    {
        public RandomShop RandomShop;

        private void SetupRandomShop(IDBConnection db, IEnumerable<DBDataCache> list)
        {
            RandomShop = new RandomShop();
            RandomShop.Add(list);


            var dataList = Global.Tables.GachaList.Select(o => { return o.n_TAG == GachaListTag.Random && o.n_TIME == 0; }).ToList();


            if (!RandomShop.Equals(dataList))
            {
                var cmd = DBCommandBuilder.InsertSignature(SignatureType.GachaCache, Global.CurrentTime);
                var signature = db.DoQuery<int>(cmd);

                foreach (var item in dataList)
                {
                    var data = Global.Tables.Drop[item.n_GOODS];
                    cmd = DBCommandBuilder.InsertDataCache(signature, item.n_ID, data.n_VALUE);
                    // id+dataid=PK,失敗就表示有另一台server也正在塞,不予理會
                    db.SafeSimpleQuery(cmd);

                    RandomShop.Add(signature, item.n_ID, data.n_VALUE);
                }
            }
        }
    }
}
