﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mikan.CSAGA.Server
{
    public static class Global
    {
        #region Sys
        /// <summary>
        /// 玩家群組列表
        /// </summary>
        public static List<int> Groups { get; private set; }

        /// <summary>
        /// 與主伺服器的時間差
        /// </summary>
        public static TimeSpan TimeDiff { get; private set; }

        /// <summary>
        /// 取得伺服器目前時間
        /// </summary>
        public static DateTime CurrentTime { get { return DateTime.Now.Subtract(TimeDiff); } }

        public static Mikan.Server.DBPool DBPool { get { return Mikan.Server.DBPool.Instance; } }

        public static ConstData.Tables Tables { get { return ConstData.Tables.Instance; } }

        public static Mikan.Server.IAP.MyCard.Setting MyCardSetting { get; private set; }

        public static bool IsDevServer { get; private set; }

        #endregion

        private static List<int> defaultCardList;
        public static IEnumerable<int> GetDefaultCardList(int category)
        {
            return defaultCardList;
        }

        public static QuestInstance GenerateQuest(int questId, int luck)
        {
            return Calc.Quest.Generate(questId, luck);
        }

        /// <summary>
        /// 初始化
        /// </summary>
        /// <param name="provider"></param>
        public static void Initialize(IConfig config, ConstDataProvider provider, List<DBGroup> groups, DateTime dbTime)
        {
            ConstData.Tables.Create(provider);

            Groups = new List<int>(groups.Count);
            foreach (var item in groups) Groups.Add(item.ID);

            var converter = new DefaultPublicIConverter();
            converter.SetGroups(Groups);
            PublicID.Setup(converter);

#warning 暫時寫死伺服器時間為+8的台灣時間
            dbTime = dbTime.AddHours(8);

            TimeDiff = DateTime.Now.Subtract(dbTime);

            MyCardSetting = new Mikan.Server.IAP.MyCard.Setting();
            MyCardSetting.ServerID = "SIF";
            MyCardSetting.Key = "com.mikan.csaga+mikan_douwa3433";
            MyCardSetting.Sandbox = bool.Parse(config["is_sandbox"]);

            IsDevServer = bool.Parse(config["is_dev"]);

            #region 取得變數表


            #endregion


            #region 取得初始要塞給玩家的卡片ID
            defaultCardList = new List<int>();
            AddDefaultCard(Tables.GetVariable(VariableID.INITIAL_CARD_01));
            AddDefaultCard(Tables.GetVariable(VariableID.INITIAL_CARD_02));
            AddDefaultCard(Tables.GetVariable(VariableID.INITIAL_CARD_03));
            AddDefaultCard(Tables.GetVariable(VariableID.INITIAL_CARD_04));
            AddDefaultCard(Tables.GetVariable(VariableID.INITIAL_CARD_05));
            #endregion

        }

        private static void AddDefaultCard(int cardId)
        {
            if (cardId > 0) defaultCardList.Add(cardId);
        }
    }
}
