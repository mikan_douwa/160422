﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Mikan;
using Mikan.Server;
using Mikan.CSAGA.Server;

public partial class _Default : System.Web.UI.Page
{
    private Mikan.CSAGA.ConstData.Tables Tables { get { return Mikan.CSAGA.ConstData.Tables.Instance; } }
    protected void Page_Load(object sender, EventArgs e)
    {
        System.Reflection.Assembly.Load("Mikan");
        System.Reflection.Assembly.Load("Mikan.Server");
        System.Reflection.Assembly.Load("Mikan.CSAGA");
        System.Reflection.Assembly.Load("Mikan.CSAGA.Server");
        DBUtils.Init();
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        var lastUse = Context.Session["wgwen32vc"];

        if (lastUse is DateTime)
        {
            var _lastUse = (DateTime)lastUse;
            if (_lastUse.AddSeconds(10) > DateTime.Now)
            {
                ShowAlert("系統忙碌中,請稍候再試!!");
                return;
            }
        }

        Context.Session["wgwen32vc"] = DateTime.Now;

        try
        {
            var id = PublicID.FromString(tbGameId.Text);

            if (id == null || !id.IsValid || !DBUtils.Groups.ContainsKey(id.Group))
            {
                ShowAlert("無效的遊戲ID!!");
                return;
            }

            if (string.IsNullOrEmpty(tbCode.Text))
            {
                ShowAlert("請輸入遊戲序號!!");
                return;
            }

            if (tbCode.Text.Length != 10)
            {
                ShowAlert("無效的遊戲序號!!");
                return;
            }

            if (Tables == null)
            {
                using (var reader = new ConstDataReader())
                {
                    var path = "/usr/csaga/";
                    reader.Load(path + "CSAGA_CONSTDATA.pak");
                    reader.Load(path + "CSAGA_TEXTDATA.pak");
                    reader.Load(path + "SERIAL_NUMBER.pak");

                    Mikan.CSAGA.ConstData.Tables.Create(reader.Combine());
                    Mikan.CSAGA.Calc.Calculator.Initialize();
                }
            }

            var group = tbCode.Text.Substring(0, 2);
            var code = tbCode.Text.Substring(2);

            var data = Tables.SerialNumber.SelectFirst(o => { return o.s_GROUP == group && o.s_NUMBER == code; });

            if (data == null)
            {
                ShowAlert("無效的遊戲序號!!");
                return;
            }

            var trophy = Tables.Trophy[data.n_TROPHY];

            if (trophy == null)
            {
                ShowAlert("無效的遊戲序號!!");
                return;
            }

            var text = Tables.TrophyText[data.n_TROPHY];

            var description = text != null ? text.s_NAME : "兌換獎勵";

            var gifts = new List<Mikan.CSAGA.Data.Gift>();

            foreach (var item in Tables.Drop.Select(o => { return o.n_GROUP == trophy.n_BONUS_LINK; }))
            {
                var gift = Mikan.CSAGA.Calc.Drop.ToGift(item);
                gifts.Add(gift);
            }

            if (gifts.Count == 0)
            {
                ShowAlert("無效的遊戲序號!!");
                return;
            }

            var once = group[0] == 'b';

            if (once)
            {
                using(var db = DBUtils.OpenUniqueDB())
                {
                    var cmd = DBUtils.SelectPlayerSerialNumber(id);

                    using (var res = db.DoQuery(cmd))
                    {
                        foreach (var item in DBObjectConverter.Convert<DBSerialNumber>(res))
                        {
                            var obj = Tables.SerialNumber[item.ID];
                            if (obj != null && obj.s_GROUP == group)
                            {
                                ShowAlert("同一遊戲ID無法重複領取!!");
                                return;
                            }
                        }
                    }
                }

            }

            using (var db = DBUtils.OpenDB(id.Group))
            {
                var cmd = DBCommandBuilder.SelectPlayer(id);
                using (var res = db.DoQuery(cmd))
                {
                    if (res.IsEmpty)
                    {
                        ShowAlert("無效的遊戲ID!!");
                        return;
                    }
                }

                using (var trans = db.BeginTransaction())
                {

                    foreach (var item in gifts)
                    {
                        cmd = DBCommandBuilder.InsertGift(id, Mikan.CSAGA.RewardSource.SerialNumber, description, item.Type, item.Value1, item.Value2);
                        db.SimpleQuery(cmd);
                    }

                    using (var uniqueDb = DBUtils.OpenUniqueDB())
                    {
                        cmd = DBUtils.InsertSerialNumber(id, data.n_ID, data.n_TROPHY, DateTime.Now);
                        if (!uniqueDb.SafeSimpleQuery(cmd))
                        {
                            ShowAlert("此遊戲序號已使用過!!");
                            return;
                        }
                    }

                    trans.Commit();
                }
            }

            Label3.ForeColor = System.Drawing.Color.Black;
            Label3.Text = "兌換成功!!";
            MultiView1.ActiveViewIndex = 1;
        }
        catch (Exception ex)
        {
            ShowAlert(ex.ToString());
        }
    }

    
    private void ShowAlert(string msg)
    {
        Label3.ForeColor = System.Drawing.Color.Red;
        Label3.Text = msg;
        MultiView1.ActiveViewIndex = 1;
        //var script = string.Format("<SCRIPT LANGUAGE=\"JavaScript\">alert(\"{0}\")</SCRIPT>", msg);

        //HttpContext.Current.Response.Write(script);
    }


    protected void Button2_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 0;
    }
}


public class DBUtils
{
    public static Dictionary<int, GroupData> Groups;

    public static void Init()
    {
        if (Groups == null)
        {
            using (var db = DBUtils.OpenUniqueDB())
            {
                var cmd = DBCommandBuilder.SelectGroups();
                using (var res = db.DoQuery(cmd))
                {
                    Groups = new Dictionary<int, GroupData>();
                    foreach (var item in DBObjectConverter.Convert<DBGroup>(res).ToList())
                    {
                        Groups.Add(item.ID, new GroupData { Origin = item });
                    }
                }
            }
        }
    }

    public static IDBConnection OpenUniqueDB()
    {
        var db = new MySqlDBConnection();
        db.Open("Server=172.31.30.18;Port=3306;Database=csaga_unique;Uid=root;Pwd=miKan.!@#;CharSet=utf8;");
        return db;
    }

    public static IDBConnection OpenDB(int group)
    {
        return OpenDB(Groups[group].ConnStr);
    }
    public static IDBConnection OpenDB(string connStr)
    {
        var db = new MySqlDBConnection();
        db.Open(connStr);
        return db;
    }
    public static IDBCommand CreateNoLockDBCommand(string statement)
    {
        return DBCommandBuilder.CreateNoLockDBCommand(statement);
    }
    public static IDBCommand InsertSerialNumber(PublicID playerId, int id, int trophyId, DateTime timestamp)
    {
        const string text = @"INSERT INTO `serial_number`(id, trophy_id, group_id, player_id, timestamp) VALUES(@id, @trophy_id, @group_id, @player_id, @timestamp);";

        var cmd = DBCommandBuilder.CreateDBCommand(text);
        cmd.AddParameter("@id", id);
        cmd.AddParameter("@trophy_id", trophyId);
        cmd.AddParameter("@group_id", playerId.Group);
        cmd.AddParameter("@player_id", playerId.Serial);
        cmd.AddParameter("@timestamp", timestamp);

        return cmd;
    }

    public static IDBCommand SelectPlayerSerialNumber(PublicID playerId)
    {
        const string text = "SELECT * FROM `serial_number` WHERE group_id=@group_id AND player_id=@player_id;";

        var cmd = DBCommandBuilder.CreateDBCommand(text);

        cmd.AddParameter("@group_id", playerId.Group);
        cmd.AddParameter("@player_id", playerId.Serial);

        return cmd;
    }
}

public class GroupData
{
    public DBGroup Origin;
    public int ID { get { return Origin.ID; } }
    public string ConnStr { get { return Origin.ConnectionString; } }
}