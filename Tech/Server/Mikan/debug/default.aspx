﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="debug._default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type="text/javascript">
        // 顯示讀取畫面
        function displayProgress() {
            divProgress.style.display = "inherit";
        }
    </script>
</head>
<body>
    <form id="form1" runat="server" enctype="multipart/form-data">
    <div>
    
        <asp:FileUpload ID="FileUpload1" runat="server" />
        <asp:Button ID="Button1" runat="server" onclick="Button1_Click" 
            OnClientClick="javascript:displayProgress()" Text="更新constdata" />
        <br />
        <br />
        <asp:FileUpload ID="FileUpload2" runat="server" />
        <asp:Button ID="Button2" runat="server" Height="22px" onclick="Button2_Click" 
            OnClientClick="javascript:displayProgress()" Text="更新textdata" Width="106px" />
        <br />
        <br />
        <asp:FileUpload ID="FileUpload3" runat="server" />
        <asp:Button ID="Button4" runat="server" Height="22px"
            OnClientClick="javascript:displayProgress()" Text="更新活動" 
            Width="106px" onclick="Button4_Click" />
        <br />
        <br />
        <asp:FileUpload ID="FileUpload4" runat="server" />
        <asp:Button ID="Button5" runat="server" Height="22px"
            OnClientClick="javascript:displayProgress()" Text="更新轉蛋" 
            Width="106px" onclick="Button5_Click" />
        <br />
        <br />
        <asp:Button ID="Button3" runat="server" onclick="Button3_Click" 
            Text="重啟Server" />
    
    </div>

    <div id="divProgress" style="text-align:center; display :none; position: fixed; top: 50%;  left: 50%;" > 
        <asp:Image ID="imgLoading" runat="server" ImageUrl="~/loading.gif" /> 
        <br /> 
        <font color="#1B3563" size="2px">資料處理中</font> 
    </div> 

    </form>
</body>
</html>
