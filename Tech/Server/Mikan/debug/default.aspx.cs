﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
namespace debug
{
    public partial class _default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
            
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            if (!CheckFile(FileUpload1)) return;

            File.WriteAllBytes("/usr/csaga/CSAGA_CONSTDATA.pak", FileUpload1.FileBytes);

            ShowAlert("操作完成!");
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            if (!CheckFile(FileUpload2)) return;

            File.WriteAllBytes("/usr/csaga/CSAGA_TEXTDATA.pak", FileUpload2.FileBytes);

            ShowAlert("操作完成!");
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            var path = "var/www/html/csaga/bin/Mikan.CSAGA.Server.dll";

            var bytes = File.ReadAllBytes(path);
            File.WriteAllBytes(path, bytes);

            ShowAlert("操作完成!");
        }

        protected void Button4_Click(object sender, EventArgs e)
        {
            if (!CheckFile(FileUpload3)) return;

            InsertConstdata(1, FileUpload3.FileBytes);
        }

        protected void Button5_Click(object sender, EventArgs e)
        {
            if (!CheckFile(FileUpload4)) return;

            InsertConstdata(2, FileUpload4.FileBytes);
        }

        private void ShowAlert(string msg)
        {
            Response.Write("<script>alert('" + msg + "')</script>"); 
        }

        private bool CheckFile(FileUpload file)
        {
            if (file.HasFile) return true;

            ShowAlert("請選擇要上傳的檔案");
            return false;
        }

        private void InsertConstdata(int type, byte[] data)
        {
            /*using (var db = new MySqlDBConnection())
            {
                db.Open("Server=127.0.0.1;Port=3306;Database=dal_unique;Uid=gamon;Pwd=gamon.dal;CharSet=utf8;");
                var cmd = new DBCommand();
                cmd.CommandText = "INSERT INTO `constdata` (type, data) VALUES (@type,@data);";
                cmd.AddParameter("@type", type);
                cmd.AddParameter("@data", data);
                db.SimpleQuery(cmd);
            }
            */
            ShowAlert("無作用!");
        }
    }
}